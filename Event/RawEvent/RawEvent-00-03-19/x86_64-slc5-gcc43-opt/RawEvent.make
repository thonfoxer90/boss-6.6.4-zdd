#-- start of make_header -----------------

#====================================
#  Library RawEvent
#
<<<<<<< HEAD
#   Generated Sat May 19 14:50:50 2018  by bgarillo
=======
#   Generated Sun May 20 13:40:07 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_RawEvent_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_RawEvent_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_RawEvent

RawEvent_tag = $(tag)

#cmt_local_tagfile_RawEvent = $(RawEvent_tag)_RawEvent.make
cmt_local_tagfile_RawEvent = $(bin)$(RawEvent_tag)_RawEvent.make

else

tags      = $(tag),$(CMTEXTRATAGS)

RawEvent_tag = $(tag)

#cmt_local_tagfile_RawEvent = $(RawEvent_tag).make
cmt_local_tagfile_RawEvent = $(bin)$(RawEvent_tag).make

endif

include $(cmt_local_tagfile_RawEvent)
#-include $(cmt_local_tagfile_RawEvent)

ifdef cmt_RawEvent_has_target_tag

cmt_final_setup_RawEvent = $(bin)setup_RawEvent.make
#cmt_final_setup_RawEvent = $(bin)RawEvent_RawEventsetup.make
cmt_local_RawEvent_makefile = $(bin)RawEvent.make

else

cmt_final_setup_RawEvent = $(bin)setup.make
#cmt_final_setup_RawEvent = $(bin)RawEventsetup.make
cmt_local_RawEvent_makefile = $(bin)RawEvent.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)RawEventsetup.make

#RawEvent :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'RawEvent'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = RawEvent/
#RawEvent::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

RawEventlibname   = $(bin)$(library_prefix)RawEvent$(library_suffix)
RawEventlib       = $(RawEventlibname).a
RawEventstamp     = $(bin)RawEvent.stamp
RawEventshstamp   = $(bin)RawEvent.shstamp

RawEvent :: dirs  RawEventLIB
	$(echo) "RawEvent ok"

#-- end of libary_header ----------------

RawEventLIB :: $(RawEventlib) $(RawEventshstamp)
	@/bin/echo "------> RawEvent : library ok"

$(RawEventlib) :: $(bin)RawData.o $(bin)RawDataUtil.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(RawEventlib) $?
	$(lib_silent) $(ranlib) $(RawEventlib)
	$(lib_silent) cat /dev/null >$(RawEventstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(RawEventlibname).$(shlibsuffix) :: $(RawEventlib) $(RawEventstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" RawEvent $(RawEvent_shlibflags)

$(RawEventshstamp) :: $(RawEventlibname).$(shlibsuffix)
	@if test -f $(RawEventlibname).$(shlibsuffix) ; then cat /dev/null >$(RawEventshstamp) ; fi

RawEventclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)RawData.o $(bin)RawDataUtil.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
RawEventinstallname = $(library_prefix)RawEvent$(library_suffix).$(shlibsuffix)

RawEvent :: RawEventinstall

install :: RawEventinstall

RawEventinstall :: $(install_dir)/$(RawEventinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(RawEventinstallname) :: $(bin)$(RawEventinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(RawEventinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(RawEventinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(RawEventinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(RawEventinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(RawEventinstallname) $(install_dir)/$(RawEventinstallname); \
	      echo `pwd`/$(RawEventinstallname) >$(install_dir)/$(RawEventinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(RawEventinstallname), no installation directory specified"; \
	  fi; \
	fi

RawEventclean :: RawEventuninstall

uninstall :: RawEventuninstall

RawEventuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(RawEventinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(RawEventinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(RawEventinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(RawEventinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),RawEventclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)RawEvent_dependencies.make :: dirs

ifndef QUICK
$(bin)RawEvent_dependencies.make : $(src)RawData.cxx $(src)RawDataUtil.cxx $(use_requirements) $(cmt_final_setup_RawEvent)
	$(echo) "(RawEvent.make) Rebuilding $@"; \
	  $(build_dependencies) RawEvent -all_sources -out=$@ $(src)RawData.cxx $(src)RawDataUtil.cxx
endif

#$(RawEvent_dependencies)

-include $(bin)RawEvent_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),RawEventclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)RawData.d

$(bin)$(binobj)RawData.d : $(use_requirements) $(cmt_final_setup_RawEvent)

$(bin)$(binobj)RawData.d : $(src)RawData.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/RawData.dep $(use_pp_cppflags) $(RawEvent_pp_cppflags) $(lib_RawEvent_pp_cppflags) $(RawData_pp_cppflags) $(use_cppflags) $(RawEvent_cppflags) $(lib_RawEvent_cppflags) $(RawData_cppflags) $(RawData_cxx_cppflags)  $(src)RawData.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/RawData.o $(src)RawData.cxx $(@D)/RawData.dep
endif
endif

$(bin)$(binobj)RawData.o : $(src)RawData.cxx
else
$(bin)RawEvent_dependencies.make : $(RawData_cxx_dependencies)

$(bin)$(binobj)RawData.o : $(RawData_cxx_dependencies)
endif
	$(cpp_echo) $(src)RawData.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(RawEvent_pp_cppflags) $(lib_RawEvent_pp_cppflags) $(RawData_pp_cppflags) $(use_cppflags) $(RawEvent_cppflags) $(lib_RawEvent_cppflags) $(RawData_cppflags) $(RawData_cxx_cppflags)  $(src)RawData.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),RawEventclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)RawDataUtil.d

$(bin)$(binobj)RawDataUtil.d : $(use_requirements) $(cmt_final_setup_RawEvent)

$(bin)$(binobj)RawDataUtil.d : $(src)RawDataUtil.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/RawDataUtil.dep $(use_pp_cppflags) $(RawEvent_pp_cppflags) $(lib_RawEvent_pp_cppflags) $(RawDataUtil_pp_cppflags) $(use_cppflags) $(RawEvent_cppflags) $(lib_RawEvent_cppflags) $(RawDataUtil_cppflags) $(RawDataUtil_cxx_cppflags)  $(src)RawDataUtil.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/RawDataUtil.o $(src)RawDataUtil.cxx $(@D)/RawDataUtil.dep
endif
endif

$(bin)$(binobj)RawDataUtil.o : $(src)RawDataUtil.cxx
else
$(bin)RawEvent_dependencies.make : $(RawDataUtil_cxx_dependencies)

$(bin)$(binobj)RawDataUtil.o : $(RawDataUtil_cxx_dependencies)
endif
	$(cpp_echo) $(src)RawDataUtil.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(RawEvent_pp_cppflags) $(lib_RawEvent_pp_cppflags) $(RawDataUtil_pp_cppflags) $(use_cppflags) $(RawEvent_cppflags) $(lib_RawEvent_cppflags) $(RawDataUtil_cppflags) $(RawDataUtil_cxx_cppflags)  $(src)RawDataUtil.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: RawEventclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(RawEvent.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(RawEvent.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(RawEvent.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(RawEvent.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_RawEvent)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawEvent.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawEvent.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawEvent.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

RawEventclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library RawEvent
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)RawEvent$(library_suffix).a $(library_prefix)RawEvent$(library_suffix).s? RawEvent.stamp RawEvent.shstamp
#-- end of cleanup_library ---------------
