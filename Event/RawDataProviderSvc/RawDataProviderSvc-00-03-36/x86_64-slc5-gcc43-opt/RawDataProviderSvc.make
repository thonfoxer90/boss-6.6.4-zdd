#-- start of make_header -----------------

#====================================
#  Library RawDataProviderSvc
#
<<<<<<< HEAD
#   Generated Sat May 19 14:52:45 2018  by bgarillo
=======
#   Generated Sun May 20 13:44:12 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_RawDataProviderSvc_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_RawDataProviderSvc_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_RawDataProviderSvc

RawDataProviderSvc_tag = $(tag)

#cmt_local_tagfile_RawDataProviderSvc = $(RawDataProviderSvc_tag)_RawDataProviderSvc.make
cmt_local_tagfile_RawDataProviderSvc = $(bin)$(RawDataProviderSvc_tag)_RawDataProviderSvc.make

else

tags      = $(tag),$(CMTEXTRATAGS)

RawDataProviderSvc_tag = $(tag)

#cmt_local_tagfile_RawDataProviderSvc = $(RawDataProviderSvc_tag).make
cmt_local_tagfile_RawDataProviderSvc = $(bin)$(RawDataProviderSvc_tag).make

endif

include $(cmt_local_tagfile_RawDataProviderSvc)
#-include $(cmt_local_tagfile_RawDataProviderSvc)

ifdef cmt_RawDataProviderSvc_has_target_tag

cmt_final_setup_RawDataProviderSvc = $(bin)setup_RawDataProviderSvc.make
#cmt_final_setup_RawDataProviderSvc = $(bin)RawDataProviderSvc_RawDataProviderSvcsetup.make
cmt_local_RawDataProviderSvc_makefile = $(bin)RawDataProviderSvc.make

else

cmt_final_setup_RawDataProviderSvc = $(bin)setup.make
#cmt_final_setup_RawDataProviderSvc = $(bin)RawDataProviderSvcsetup.make
cmt_local_RawDataProviderSvc_makefile = $(bin)RawDataProviderSvc.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)RawDataProviderSvcsetup.make

#RawDataProviderSvc :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'RawDataProviderSvc'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = RawDataProviderSvc/
#RawDataProviderSvc::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

RawDataProviderSvclibname   = $(bin)$(library_prefix)RawDataProviderSvc$(library_suffix)
RawDataProviderSvclib       = $(RawDataProviderSvclibname).a
RawDataProviderSvcstamp     = $(bin)RawDataProviderSvc.stamp
RawDataProviderSvcshstamp   = $(bin)RawDataProviderSvc.shstamp

RawDataProviderSvc :: dirs  RawDataProviderSvcLIB
	$(echo) "RawDataProviderSvc ok"

#-- end of libary_header ----------------

RawDataProviderSvcLIB :: $(RawDataProviderSvclib) $(RawDataProviderSvcshstamp)
	@/bin/echo "------> RawDataProviderSvc : library ok"

$(RawDataProviderSvclib) :: $(bin)RawDataProvider_load.o $(bin)RawDataProvider_entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(RawDataProviderSvclib) $?
	$(lib_silent) $(ranlib) $(RawDataProviderSvclib)
	$(lib_silent) cat /dev/null >$(RawDataProviderSvcstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(RawDataProviderSvclibname).$(shlibsuffix) :: $(RawDataProviderSvclib) $(RawDataProviderSvcstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" RawDataProviderSvc $(RawDataProviderSvc_shlibflags)

$(RawDataProviderSvcshstamp) :: $(RawDataProviderSvclibname).$(shlibsuffix)
	@if test -f $(RawDataProviderSvclibname).$(shlibsuffix) ; then cat /dev/null >$(RawDataProviderSvcshstamp) ; fi

RawDataProviderSvcclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)RawDataProvider_load.o $(bin)RawDataProvider_entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
RawDataProviderSvcinstallname = $(library_prefix)RawDataProviderSvc$(library_suffix).$(shlibsuffix)

RawDataProviderSvc :: RawDataProviderSvcinstall

install :: RawDataProviderSvcinstall

RawDataProviderSvcinstall :: $(install_dir)/$(RawDataProviderSvcinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(RawDataProviderSvcinstallname) :: $(bin)$(RawDataProviderSvcinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(RawDataProviderSvcinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(RawDataProviderSvcinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(RawDataProviderSvcinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(RawDataProviderSvcinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(RawDataProviderSvcinstallname) $(install_dir)/$(RawDataProviderSvcinstallname); \
	      echo `pwd`/$(RawDataProviderSvcinstallname) >$(install_dir)/$(RawDataProviderSvcinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(RawDataProviderSvcinstallname), no installation directory specified"; \
	  fi; \
	fi

RawDataProviderSvcclean :: RawDataProviderSvcuninstall

uninstall :: RawDataProviderSvcuninstall

RawDataProviderSvcuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(RawDataProviderSvcinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(RawDataProviderSvcinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(RawDataProviderSvcinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(RawDataProviderSvcinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),RawDataProviderSvcclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)RawDataProviderSvc_dependencies.make :: dirs

ifndef QUICK
$(bin)RawDataProviderSvc_dependencies.make : $(src)components/RawDataProvider_load.cxx $(src)components/RawDataProvider_entries.cxx $(use_requirements) $(cmt_final_setup_RawDataProviderSvc)
	$(echo) "(RawDataProviderSvc.make) Rebuilding $@"; \
	  $(build_dependencies) RawDataProviderSvc -all_sources -out=$@ $(src)components/RawDataProvider_load.cxx $(src)components/RawDataProvider_entries.cxx
endif

#$(RawDataProviderSvc_dependencies)

-include $(bin)RawDataProviderSvc_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),RawDataProviderSvcclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)RawDataProvider_load.d

$(bin)$(binobj)RawDataProvider_load.d : $(use_requirements) $(cmt_final_setup_RawDataProviderSvc)

$(bin)$(binobj)RawDataProvider_load.d : $(src)components/RawDataProvider_load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/RawDataProvider_load.dep $(use_pp_cppflags) $(RawDataProviderSvc_pp_cppflags) $(lib_RawDataProviderSvc_pp_cppflags) $(RawDataProvider_load_pp_cppflags) $(use_cppflags) $(RawDataProviderSvc_cppflags) $(lib_RawDataProviderSvc_cppflags) $(RawDataProvider_load_cppflags) $(RawDataProvider_load_cxx_cppflags) -I../src/components $(src)components/RawDataProvider_load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/RawDataProvider_load.o $(src)components/RawDataProvider_load.cxx $(@D)/RawDataProvider_load.dep
endif
endif

$(bin)$(binobj)RawDataProvider_load.o : $(src)components/RawDataProvider_load.cxx
else
$(bin)RawDataProviderSvc_dependencies.make : $(RawDataProvider_load_cxx_dependencies)

$(bin)$(binobj)RawDataProvider_load.o : $(RawDataProvider_load_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/RawDataProvider_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(RawDataProviderSvc_pp_cppflags) $(lib_RawDataProviderSvc_pp_cppflags) $(RawDataProvider_load_pp_cppflags) $(use_cppflags) $(RawDataProviderSvc_cppflags) $(lib_RawDataProviderSvc_cppflags) $(RawDataProvider_load_cppflags) $(RawDataProvider_load_cxx_cppflags) -I../src/components $(src)components/RawDataProvider_load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),RawDataProviderSvcclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)RawDataProvider_entries.d

$(bin)$(binobj)RawDataProvider_entries.d : $(use_requirements) $(cmt_final_setup_RawDataProviderSvc)

$(bin)$(binobj)RawDataProvider_entries.d : $(src)components/RawDataProvider_entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/RawDataProvider_entries.dep $(use_pp_cppflags) $(RawDataProviderSvc_pp_cppflags) $(lib_RawDataProviderSvc_pp_cppflags) $(RawDataProvider_entries_pp_cppflags) $(use_cppflags) $(RawDataProviderSvc_cppflags) $(lib_RawDataProviderSvc_cppflags) $(RawDataProvider_entries_cppflags) $(RawDataProvider_entries_cxx_cppflags) -I../src/components $(src)components/RawDataProvider_entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/RawDataProvider_entries.o $(src)components/RawDataProvider_entries.cxx $(@D)/RawDataProvider_entries.dep
endif
endif

$(bin)$(binobj)RawDataProvider_entries.o : $(src)components/RawDataProvider_entries.cxx
else
$(bin)RawDataProviderSvc_dependencies.make : $(RawDataProvider_entries_cxx_dependencies)

$(bin)$(binobj)RawDataProvider_entries.o : $(RawDataProvider_entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/RawDataProvider_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(RawDataProviderSvc_pp_cppflags) $(lib_RawDataProviderSvc_pp_cppflags) $(RawDataProvider_entries_pp_cppflags) $(use_cppflags) $(RawDataProviderSvc_cppflags) $(lib_RawDataProviderSvc_cppflags) $(RawDataProvider_entries_cppflags) $(RawDataProvider_entries_cxx_cppflags) -I../src/components $(src)components/RawDataProvider_entries.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: RawDataProviderSvcclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(RawDataProviderSvc.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(RawDataProviderSvc.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(RawDataProviderSvc.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(RawDataProviderSvc.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_RawDataProviderSvc)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawDataProviderSvc.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawDataProviderSvc.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawDataProviderSvc.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

RawDataProviderSvcclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library RawDataProviderSvc
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)RawDataProviderSvc$(library_suffix).a $(library_prefix)RawDataProviderSvc$(library_suffix).s? RawDataProviderSvc.stamp RawDataProviderSvc.shstamp
#-- end of cleanup_library ---------------
