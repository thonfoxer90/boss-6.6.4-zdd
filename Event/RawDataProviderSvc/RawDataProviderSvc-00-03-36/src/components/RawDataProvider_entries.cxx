#include "RawDataProviderSvc/RawDataProviderSvc.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_SERVICE_FACTORY( RawDataProviderSvc )

DECLARE_FACTORY_ENTRIES( RawDataProviderSvc ) { 
  DECLARE_SERVICE( RawDataProviderSvc );
}
