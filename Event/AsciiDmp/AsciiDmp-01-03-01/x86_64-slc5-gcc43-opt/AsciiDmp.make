#-- start of make_header -----------------

#====================================
#  Library AsciiDmp
#
<<<<<<< HEAD
#   Generated Sat May 19 14:51:11 2018  by bgarillo
=======
#   Generated Sun May 20 13:41:01 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_AsciiDmp_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_AsciiDmp_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_AsciiDmp

AsciiDmp_tag = $(tag)

#cmt_local_tagfile_AsciiDmp = $(AsciiDmp_tag)_AsciiDmp.make
cmt_local_tagfile_AsciiDmp = $(bin)$(AsciiDmp_tag)_AsciiDmp.make

else

tags      = $(tag),$(CMTEXTRATAGS)

AsciiDmp_tag = $(tag)

#cmt_local_tagfile_AsciiDmp = $(AsciiDmp_tag).make
cmt_local_tagfile_AsciiDmp = $(bin)$(AsciiDmp_tag).make

endif

include $(cmt_local_tagfile_AsciiDmp)
#-include $(cmt_local_tagfile_AsciiDmp)

ifdef cmt_AsciiDmp_has_target_tag

cmt_final_setup_AsciiDmp = $(bin)setup_AsciiDmp.make
#cmt_final_setup_AsciiDmp = $(bin)AsciiDmp_AsciiDmpsetup.make
cmt_local_AsciiDmp_makefile = $(bin)AsciiDmp.make

else

cmt_final_setup_AsciiDmp = $(bin)setup.make
#cmt_final_setup_AsciiDmp = $(bin)AsciiDmpsetup.make
cmt_local_AsciiDmp_makefile = $(bin)AsciiDmp.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)AsciiDmpsetup.make

#AsciiDmp :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'AsciiDmp'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = AsciiDmp/
#AsciiDmp::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

AsciiDmplibname   = $(bin)$(library_prefix)AsciiDmp$(library_suffix)
AsciiDmplib       = $(AsciiDmplibname).a
AsciiDmpstamp     = $(bin)AsciiDmp.stamp
AsciiDmpshstamp   = $(bin)AsciiDmp.shstamp

AsciiDmp :: dirs  AsciiDmpLIB
	$(echo) "AsciiDmp ok"

#-- end of libary_header ----------------

AsciiDmpLIB :: $(AsciiDmplib) $(AsciiDmpshstamp)
	@/bin/echo "------> AsciiDmp : library ok"

$(AsciiDmplib) :: $(bin)AsciiData.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(AsciiDmplib) $?
	$(lib_silent) $(ranlib) $(AsciiDmplib)
	$(lib_silent) cat /dev/null >$(AsciiDmpstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(AsciiDmplibname).$(shlibsuffix) :: $(AsciiDmplib) $(AsciiDmpstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" AsciiDmp $(AsciiDmp_shlibflags)

$(AsciiDmpshstamp) :: $(AsciiDmplibname).$(shlibsuffix)
	@if test -f $(AsciiDmplibname).$(shlibsuffix) ; then cat /dev/null >$(AsciiDmpshstamp) ; fi

AsciiDmpclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)AsciiData.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
AsciiDmpinstallname = $(library_prefix)AsciiDmp$(library_suffix).$(shlibsuffix)

AsciiDmp :: AsciiDmpinstall

install :: AsciiDmpinstall

AsciiDmpinstall :: $(install_dir)/$(AsciiDmpinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(AsciiDmpinstallname) :: $(bin)$(AsciiDmpinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(AsciiDmpinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(AsciiDmpinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(AsciiDmpinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(AsciiDmpinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(AsciiDmpinstallname) $(install_dir)/$(AsciiDmpinstallname); \
	      echo `pwd`/$(AsciiDmpinstallname) >$(install_dir)/$(AsciiDmpinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(AsciiDmpinstallname), no installation directory specified"; \
	  fi; \
	fi

AsciiDmpclean :: AsciiDmpuninstall

uninstall :: AsciiDmpuninstall

AsciiDmpuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(AsciiDmpinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(AsciiDmpinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(AsciiDmpinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(AsciiDmpinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),AsciiDmpclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)AsciiDmp_dependencies.make :: dirs

ifndef QUICK
$(bin)AsciiDmp_dependencies.make : $(src)AsciiData.cc $(use_requirements) $(cmt_final_setup_AsciiDmp)
	$(echo) "(AsciiDmp.make) Rebuilding $@"; \
	  $(build_dependencies) AsciiDmp -all_sources -out=$@ $(src)AsciiData.cc
endif

#$(AsciiDmp_dependencies)

-include $(bin)AsciiDmp_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),AsciiDmpclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)AsciiData.d

$(bin)$(binobj)AsciiData.d : $(use_requirements) $(cmt_final_setup_AsciiDmp)

$(bin)$(binobj)AsciiData.d : $(src)AsciiData.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/AsciiData.dep $(use_pp_cppflags) $(AsciiDmp_pp_cppflags) $(lib_AsciiDmp_pp_cppflags) $(AsciiData_pp_cppflags) $(use_cppflags) $(AsciiDmp_cppflags) $(lib_AsciiDmp_cppflags) $(AsciiData_cppflags) $(AsciiData_cc_cppflags)  $(src)AsciiData.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/AsciiData.o $(src)AsciiData.cc $(@D)/AsciiData.dep
endif
endif

$(bin)$(binobj)AsciiData.o : $(src)AsciiData.cc
else
$(bin)AsciiDmp_dependencies.make : $(AsciiData_cc_dependencies)

$(bin)$(binobj)AsciiData.o : $(AsciiData_cc_dependencies)
endif
	$(cpp_echo) $(src)AsciiData.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(AsciiDmp_pp_cppflags) $(lib_AsciiDmp_pp_cppflags) $(AsciiData_pp_cppflags) $(use_cppflags) $(AsciiDmp_cppflags) $(lib_AsciiDmp_cppflags) $(AsciiData_cppflags) $(AsciiData_cc_cppflags)  $(src)AsciiData.cc

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: AsciiDmpclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(AsciiDmp.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(AsciiDmp.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(AsciiDmp.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(AsciiDmp.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_AsciiDmp)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(AsciiDmp.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(AsciiDmp.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(AsciiDmp.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

AsciiDmpclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library AsciiDmp
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)AsciiDmp$(library_suffix).a $(library_prefix)AsciiDmp$(library_suffix).s? AsciiDmp.stamp AsciiDmp.shstamp
#-- end of cleanup_library ---------------
