
//------------------------------------------------------
// Private Application Configuration options
//--------------------------------------------------------------

ApplicationMgr.DLLs += {"DstMakerAlg", "RootCnvSvc"};
ApplicationMgr.TopAlg += { "DstEventMaker"};


EventPersistencySvc.CnvServices += {"RootCnvSvc"};
//--------------------------------------------------------------
// Event related parameters
//--------------------------------------------------------------
// items to be written out and configuration of OutStream
ApplicationMgr.OutStream    = {"RootWriter"};
RootWriter.ItemList         = {"/Event/Digi#99","/Event/Dst#99","/Event/MC#99"};
// {"/Event/Digi/CalDigiCol#99","/Event/Digi/MdcDigiCol#99","/Event/Digi/MuonDigiCol#99","/Event/Digi/TofDigiCol#99"};

RootWriter.Output         = " TYP='ROOT'";

// properties of the Root service
// RootCnvSvc.digiRootOutputFile = "digi.root";


