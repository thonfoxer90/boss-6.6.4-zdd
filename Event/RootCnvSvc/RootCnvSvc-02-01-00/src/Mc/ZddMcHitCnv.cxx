#ifndef ZddMcHitCnv_CXX
#define ZddMcHitCnv_CXX  1

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectVector.h"

#include "TClonesArray.h"

#include "EventModel/EventModel.h"
#include "McTruth/McEvent.h"   //TDS object
#include "McTruth/ZddMcHit.h"   //TDS object
#include "RootEventData/TZddMc.h" // standard root object
#include "RootEventData/TMcEvent.h"
#include "RootCnvSvc/Mc/McCnv.h"
#include "RootCnvSvc/Mc/ZddMcHitCnv.h"
#include "RootCnvSvc/RootAddress.h"


// Instantiation of a static factory class used by clients to create
// instances of this service
//static CnvFactory<ZddMcHitCnv> s_factory;
//const ICnvFactory& ZddMcHitCnvFactory = s_factory;

ZddMcHitCnv::ZddMcHitCnv(ISvcLocator* svc)
: RootEventBaseCnv(classID(), svc)
{
    // Here we associate this converter with the /Event path on the TDS.
    MsgStream log(msgSvc(), "ZddMcHitCnv");
    //log << MSG::DEBUG << "Constructor called for " << objType() << endreq;
    m_rootBranchname ="m_zddMcHitCol";
    //declareObject(EventModel::MC::ZddMcHitCol, objType(), m_rootTreename, m_rootBranchname);
    m_adresses.push_back(&m_zddMcHitCol);
    m_zddMcHitCol=0;
}

StatusCode ZddMcHitCnv::TObjectToDataObject(DataObject*& refpObject) {
  // creation of TDS object from root object

    MsgStream log(msgSvc(), "ZddMcHitCnv");
    log << MSG::DEBUG << "ZddMcHitCnv::TObjectToDataObject" << endreq;
    StatusCode sc=StatusCode::SUCCESS;

    // create the TDS location for the ZddMc Collection
    ZddMcHitCol* zddMcTdsCol = new ZddMcHitCol;
    refpObject=zddMcTdsCol;


    // now convert
    if (!m_zddMcHitCol) return sc;
    TIter zddMcIter(m_zddMcHitCol);
    TZddMc *zddMcRoot = 0;
    while ((zddMcRoot = (TZddMc*)zddMcIter.Next())) {
               unsigned int id = zddMcRoot ->getId();
	       unsigned int trackIndex = zddMcRoot ->getTrackID();

	       double preXPosition =   zddMcRoot->getPrePositionX();
	       double preYPosition =   zddMcRoot->getPrePositionY();
	       double preZPosition =   zddMcRoot->getPrePositionZ();

               double postXPosition =   zddMcRoot->getPostPositionX();
               double postYPosition =   zddMcRoot->getPostPositionY();
               double postZPosition =   zddMcRoot->getPostPositionZ();

	       double px = zddMcRoot->getPx();
	       double py = zddMcRoot->getPy();
	       double pz = zddMcRoot->getPz();
	       
	       double preTime = zddMcRoot->getPreTime();
	       double postTime = zddMcRoot->getPostTime();
	       double edep = zddMcRoot->getEDep();
	    	       
	       ZddMcHit *zddMcTds = new ZddMcHit;
	       m_common.m_rootZddMcHitMap[zddMcRoot] = zddMcTds;
	       
	       zddMcTds->setIdentifier(id);
	       zddMcTds->setTrackID(trackIndex);
	       zddMcTds->setPrePositionX(preXPosition);
	       zddMcTds->setPrePositionY(preYPosition);
	       zddMcTds->setPrePositionZ(preZPosition);
               zddMcTds->setPostPositionX(postXPosition);
               zddMcTds->setPostPositionY(postYPosition); 
               zddMcTds->setPostPositionZ(postZPosition); 
	       zddMcTds->setPx(px);
	       zddMcTds->setPy(py);
	       zddMcTds->setPz(pz);
	       zddMcTds->setPreTime(preTime);
               zddMcTds->setPostTime(postTime);
	       zddMcTds->setEDep(edep);

	       zddMcTdsCol->push_back(zddMcTds);
     }
    //m_zddMcHitCol->Delete();  // wensp add 2005/12/30
    delete m_zddMcHitCol;
    m_zddMcHitCol = 0;
   return StatusCode::SUCCESS;
}

StatusCode ZddMcHitCnv::DataObjectToTObject(DataObject* obj,RootAddress* rootaddr) {

  MsgStream log(msgSvc(), "ZddMcHitCnv");
  log << MSG::DEBUG << "ZddMcHitCnv::DataObjectToTObject" << endreq;
  StatusCode sc=StatusCode::SUCCESS;
 
 ZddMcHitCol * zddMcHitCnvTds=dynamic_cast<ZddMcHitCol *> (obj);
  if (!zddMcHitCnvTds) {
    log << MSG::ERROR << "Could not downcast to ZddMcHitCol" << endreq;
    return StatusCode::FAILURE;
  }
 
  DataObject *evt;
  m_eds->findObject(EventModel::MC::Event,evt);
  if (evt==NULL) {
    log << MSG::ERROR << "Could not get McEvent in TDS "  << endreq;
    return StatusCode::FAILURE;
  }
  McEvent * devtTds=dynamic_cast<McEvent *> (evt);
  if (!devtTds) {
    log << MSG::ERROR << "ZddMcHitCnv:Could not downcast to TDS McEvent" << endreq;
  }
  IOpaqueAddress *addr;

  m_cnvSvc->getMcCnv()->createRep(evt,addr); 
  TMcEvent *McEvt=m_cnvSvc->getMcCnv()->getWriteObject();

  const TObjArray *m_zddMcHitCol = McEvt->getZddMcHitCol();
  if (!m_zddMcHitCol) return sc;
  McEvt->clearZddMcHitCol(); //necessary in case there is I/O at the same time since array is static
  ZddMcHitCol::const_iterator zddMcTds;

  for (zddMcTds = zddMcHitCnvTds->begin(); zddMcTds != zddMcHitCnvTds->end(); zddMcTds++) {
    		UInt_t id  = ((*zddMcTds)->identify()).get_value() ;
		UInt_t trackIndex = (*zddMcTds) ->getTrackID();
		Double_t preXPosition =   (*zddMcTds) ->getPrePositionX() ;
		Double_t preYPosition =   (*zddMcTds) ->getPrePositionY() ;
		Double_t preZPosition =   (*zddMcTds) ->getPrePositionZ() ;
                Double_t postXPosition =   (*zddMcTds) ->getPostPositionX() ;
                Double_t postYPosition =   (*zddMcTds) ->getPostPositionY() ;
                Double_t postZPosition =   (*zddMcTds) ->getPostPositionZ() ;
		Double_t px=   (*zddMcTds) ->getPx() ;
		Double_t py =   (*zddMcTds) ->getPy() ;
		Double_t pz =   (*zddMcTds) ->getPz() ;
		Double_t preTime = (*zddMcTds) ->getPreTime() ;
                Double_t postTime = (*zddMcTds) ->getPostTime() ;
		Double_t edep = (*zddMcTds) ->getEDep() ;


		TZddMc *zddMcRoot = new TZddMc();
		//m_common.m_zddMcHitMap[(*zddMcTds)] = zddMcRoot;

		zddMcRoot->setId(id);
		zddMcRoot->setTrackID(trackIndex);
		zddMcRoot->setPrePositionX(preZPosition);
		zddMcRoot->setPrePositionY(preYPosition);
		zddMcRoot->setPrePositionZ(preZPosition);
                zddMcRoot->setPostPositionX(postZPosition);
                zddMcRoot->setPostPositionY(postYPosition);
                zddMcRoot->setPostPositionZ(postZPosition);
		zddMcRoot->setPx(px);
		zddMcRoot->setPy(py);
		zddMcRoot->setPz(pz);
		zddMcRoot->setPreTime(preTime);
		zddMcRoot->setPostTime(postTime);
		zddMcRoot->setEDep(edep);

		McEvt->addZddMc(zddMcRoot); 
  }

  return StatusCode::SUCCESS;
}
#endif










