#ifndef ZddDigiCnv_CXX
#define ZddDigiCnv_CXX  1

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectVector.h"

#include "TClonesArray.h"

#include "EventModel/EventModel.h"
#include "RawEvent/DigiEvent.h"   //TDS object
#include "ZddRawEvent/ZddDigi.h"   //TDS object
#include "RootEventData/TZddDigi.h" // standard root object
#include "RootEventData/TDigiEvent.h"
#include "RootCnvSvc/Digi/DigiCnv.h"
#include "RootCnvSvc/Digi/ZddDigiCnv.h"
#include "RootCnvSvc/RootAddress.h"


// Instantiation of a static factory class used by clients to create
// instances of this service
//static CnvFactory<ZddDigiCnv> s_factory;
//const ICnvFactory& ZddDigiCnvFactory = s_factory;

ZddDigiCnv::ZddDigiCnv(ISvcLocator* svc)
: RootEventBaseCnv(classID(), svc)
{
    // Here we associate this converter with the /Event path on the TDS.
    MsgStream log(msgSvc(), "ZddDigiCnv");
    //log << MSG::DEBUG << "Constructor called for " << objType() << endreq;
    m_rootBranchname ="m_zddDigiCol";
    //declareObject(EventModel::Digi::ZddDigiCol, objType(), m_rootTreename, m_rootBranchname);
    m_adresses.push_back(&m_zddDigiCol);
    m_zddDigiCol=0;

}

StatusCode ZddDigiCnv::TObjectToDataObject(DataObject*& refpObject) {
  // creation of TDS object from root object

    MsgStream log(msgSvc(), "ZddDigiCnv");
    log << MSG::DEBUG << "ZddDigiCnv::TObjectToDataObject" << endreq;
    StatusCode sc=StatusCode::SUCCESS;

    // create the TDS location for the ZddDigi Collection
    ZddDigiCol* zddDigiTdsCol = new ZddDigiCol;
    refpObject=zddDigiTdsCol;


    // now convert
    if (!m_zddDigiCol) return sc;
    TIter zddDigiIter(m_zddDigiCol);
    TZddDigi *zddDigiRoot = 0;
    while ((zddDigiRoot = (TZddDigi*)zddDigiIter.Next())) {
        unsigned int  id  = zddDigiRoot->getIntId();
	//        G4cout << "RootCnvSvc::ZddDigiCnv.cxx1 - Identifier:    " << id << G4endl;
	unsigned int  time = zddDigiRoot->getTimeChannel();
	//        G4cout << "RootCnvSvc::ZddDigiCnv.cxx1 - timeChannel:   " << time << G4endl;
	unsigned int  charge = zddDigiRoot->getChargeChannel(); 
	//	G4cout << "RootCnvSvc::ZddDigiCnv.cxx1 - chargeChannel: " << charge << G4endl;
        unsigned int measure = zddDigiRoot->getMeasure(); // how was the output of the ADC? (3 = saturation, 2 = high measure, 1 = mid measure, 0 = low measure) 
	                                                  // also depends on available adc channels, not correctly implemented yet
	                                                  // source: RawDataUtil.h

        ZddDigi *zddDigiTds = new ZddDigi(id);
        m_common.m_rootZddDigiMap[zddDigiRoot] = zddDigiTds;
	
	zddDigiTds->setTimeChannel(time);
	zddDigiTds->setChargeChannel(charge);
	zddDigiTds->setMeasure(measure);

	zddDigiTdsCol->push_back(zddDigiTds);
      
     }
    //m_zddDigiCol->Delete();  // wensp add 2005/12/30
    delete m_zddDigiCol;
    m_zddDigiCol = 0; 
   return StatusCode::SUCCESS;
}

StatusCode ZddDigiCnv::DataObjectToTObject(DataObject* obj,RootAddress* rootaddr) {

  MsgStream log(msgSvc(), "ZddDigiCnv");
  log << MSG::DEBUG << "ZddDigiCnv::DataObjectToTObject" << endreq;
  StatusCode sc=StatusCode::SUCCESS;
 
  ZddDigiCol * zddDigiColTds=dynamic_cast<ZddDigiCol *> (obj);
  if (!zddDigiColTds) {
    log << MSG::ERROR << "Could not downcast to ZddDigiCol" << endreq;
    return StatusCode::FAILURE;
  }
 
  DataObject *evt;
  m_eds->findObject(EventModel::Digi::Event,evt);
  if (evt==NULL) {
    log << MSG::ERROR << "Could not get DigiEvent in TDS "  << endreq;
    return StatusCode::FAILURE;
  }
  DigiEvent * devtTds=dynamic_cast<DigiEvent *> (evt);
  if (!devtTds) {
    log << MSG::ERROR << "ZddDigiCnv:Could not downcast to TDS DigiEvent" << endreq;
  }
  IOpaqueAddress *addr;

  m_cnvSvc->getDigiCnv()->createRep(evt,addr); 
  TDigiEvent *recEvt=m_cnvSvc->getDigiCnv()->getWriteObject();

  const TObjArray *m_zddDigiCol = recEvt->getZddDigiCol();
  if (!m_zddDigiCol) return sc;
  recEvt->clearZddDigiCol(); //necessary in case there is I/O at the same time since array is static
  ZddDigiCol::const_iterator zddDigiTds;

  for (zddDigiTds = zddDigiColTds->begin(); zddDigiTds != zddDigiColTds->end(); zddDigiTds++) {
 
    UInt_t measure = (*zddDigiTds)->getMeasure();
    //       G4cout << "RootCnvSvc::ZddDigiCnv.cxx2 - measure " << measure << G4endl;
    UInt_t time      = (*zddDigiTds)->getTimeChannel(); 
    //       G4cout << "RootCnvSvc::ZddDigiCnv.cxx2 - timeChannel:   " << time << G4endl;
    UInt_t charge    = (*zddDigiTds)->getChargeChannel(); 
    //       G4cout << "RootCnvSvc::ZddDigiCnv.cxx2 - chargeChannel: " << charge << G4endl;
    UInt_t id        = (*zddDigiTds)->getIntId();
    //       G4cout << "RootCnvSvc::ZddDigiCnv.cxx2 - Identifier:    " << id << G4endl << "-------------------------------" << G4endl;

    TZddDigi *zddDigiRoot = new TZddDigi();
    //m_common.m_zddDigiMap[(*zddDigiTds)] = zddDigiRoot;

    zddDigiRoot->initialize(id, time ,charge);
    zddDigiRoot->setMeasure(measure);

    recEvt->addZddDigi(zddDigiRoot); 
  }

  return StatusCode::SUCCESS;
}
#endif










