#ifndef ZddMcHitCnv_H
#define ZddMcHitCnv_H 1

#include "RootCnvSvc/RootEventBaseCnv.h"
#include "RootCnvSvc/commonData.h"

class RootAddress;

extern const CLID& CLID_ZddMcHitCol;

// Abstract factory to create the converter
template <class TYPE> class CnvFactory;

/** @class ZddMcCnv
 * @brief Concrete converter for the ZddMc collection
 *
 */ 

class ZddMcHitCnv : public RootEventBaseCnv { 

  friend class CnvFactory<ZddMcHitCnv>;

public: 
  static const CLID& classID()   
  {
       return CLID_ZddMcHitCol; 
  }

  virtual ~ZddMcHitCnv() { };

protected:
  ZddMcHitCnv(ISvcLocator* svc);

  /// transformation to root
  virtual StatusCode DataObjectToTObject(DataObject* obj, RootAddress* addr);

  /// transformation from root
  virtual StatusCode TObjectToDataObject(DataObject*& obj);

private:
  /// relational maps
  commonData m_common;

  /// root object to be read
  TObjArray *m_zddMcHitCol;
};



#endif // ZddMcCnv_H









