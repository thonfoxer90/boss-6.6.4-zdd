#ifndef ZddDigiCnv_H
#define ZddDigiCnv_H 1

#include "RootCnvSvc/RootEventBaseCnv.h"
#include "RootCnvSvc/commonData.h"

class RootAddress;

extern const CLID& CLID_ZddDigiCol;

// Abstract factory to create the converter
template <class TYPE> class CnvFactory;


class ZddDigiCnv : public RootEventBaseCnv { 

  friend class CnvFactory<ZddDigiCnv>;

public: 
  static const CLID& classID()   
  {
       return CLID_ZddDigiCol; 
  }

  virtual ~ZddDigiCnv() { };

protected:
  ZddDigiCnv(ISvcLocator* svc);

  /// transformation to root
  virtual StatusCode DataObjectToTObject(DataObject* obj, RootAddress* addr);

  /// transformation from root
  virtual StatusCode TObjectToDataObject(DataObject*& obj);

private:
  /// relational maps
  commonData m_common;

  /// root object to be read
  TObjArray *m_zddDigiCol;
};



#endif // ZddDigiCnv_H









