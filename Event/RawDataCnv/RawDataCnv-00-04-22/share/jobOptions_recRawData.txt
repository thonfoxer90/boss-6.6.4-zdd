//############################################################## -*-C++-*-
//
// Boss job options file for ReadAsciiData
//
//==============================================================
#include "$RAWDATACNVROOT/share/ReadRawDatajobOptions.txt"

//--------------------------------------------------------------
// Private Application Configuration options
//--------------------------------------------------------------
ApplicationMgr.DLLs += {"MdcDummyAlg", "MdcGeomSvc"};
ApplicationMgr.TopAlg = { "MdcDummy" };
ApplicationMgr.ExtSvc += { "MdcGeomSvc" };

//--------------------------------------------------------------
// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
//--------------------------------------------------------------
MessageSvc.OutputLevel      = 3;

//--------------------------------------------------------------
// Algorithms Private Options
//--------------------------------------------------------------
RawDataInputSvc.InputFiles ={ "data_dir/sim_test2_a2r.rdt"};

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 1;
//==============================================================
//
// End of job options file
//
//##############################################################
