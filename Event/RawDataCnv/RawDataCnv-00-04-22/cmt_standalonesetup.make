----------> uses
#
# Selection :
use CMT v1r20p20081118 (/afs/.ihep.ac.cn/bes3/offline/ExternalLib/contrib)
----------> tags
CMTv1 (from CMTVERSION)
CMTr20 (from CMTVERSION)
CMTp20081118 (from CMTVERSION)
Linux (from uname) package CMT implies [Unix]
x86_64-slc5-gcc43-opt (from CMTCONFIG)
mwerner_no_config (from PROJECT) excludes [mwerner_config]
mwerner_root (from PROJECT) excludes [mwerner_no_root]
mwerner_cleanup (from PROJECT) excludes [mwerner_no_cleanup]
mwerner_prototypes (from PROJECT) excludes [mwerner_no_prototypes]
mwerner_with_installarea (from PROJECT) excludes [mwerner_without_installarea]
mwerner_with_version_directory (from PROJECT) excludes [mwerner_without_version_directory]
mwerner (from PROJECT)
BOSS_no_config (from PROJECT) excludes [BOSS_config]
BOSS_root (from PROJECT) excludes [BOSS_no_root]
BOSS_cleanup (from PROJECT) excludes [BOSS_no_cleanup]
BOSS_prototypes (from PROJECT) excludes [BOSS_no_prototypes]
BOSS_with_installarea (from PROJECT) excludes [BOSS_without_installarea]
BOSS_with_version_directory (from PROJECT) excludes [BOSS_without_version_directory]
GAUDI_no_config (from PROJECT) excludes [GAUDI_config]
GAUDI_root (from PROJECT) excludes [GAUDI_no_root]
GAUDI_cleanup (from PROJECT) excludes [GAUDI_no_cleanup]
GAUDI_prototypes (from PROJECT) excludes [GAUDI_no_prototypes]
GAUDI_with_installarea (from PROJECT) excludes [GAUDI_without_installarea]
GAUDI_without_version_directory (from PROJECT) excludes [GAUDI_with_version_directory]
LCGCMT_no_config (from PROJECT) excludes [LCGCMT_config]
LCGCMT_no_root (from PROJECT) excludes [LCGCMT_root]
LCGCMT_cleanup (from PROJECT) excludes [LCGCMT_no_cleanup]
LCGCMT_prototypes (from PROJECT) excludes [LCGCMT_no_prototypes]
LCGCMT_without_installarea (from PROJECT) excludes [LCGCMT_with_installarea]
LCGCMT_with_version_directory (from PROJECT) excludes [LCGCMT_without_version_directory]
x86_64 (from package CMT)
slc55 (from package CMT)
gcc432 (from package CMT)
Unix (from package CMT) excludes [WIN32 Win32]
----------> CMTPATH
# Add path /besfs/users/mwerner/workarea_b662 from initialization
# Add path /afs/ihep.ac.cn/bes3/offline/Boss/6.6.2 from initialization
# Add path /afs/ihep.ac.cn/bes3/offline/ExternalLib/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5 from initialization
# Add path /afs/ihep.ac.cn/bes3/offline/ExternalLib/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5 from initialization
