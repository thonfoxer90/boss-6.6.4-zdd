#include <iostream>
#include <fstream>
#include "Identifier/ZddID.h"
#include "RawDataCnv/EventManagement/ZddMcHitBuilder.h"
using namespace std;

ZddMcHitBuilder::ZddMcHitBuilder()
   :Builder()
{
  initialize (Builder::m_confFile);
}

void ZddMcHitBuilder::unPack(vector<uint32_t>::const_iterator& iter,
             vector<uint32_t>::const_iterator& eiter, ZddTruth_t& mt)
{
  /*  
uint32_t helpVal = *(iter++); assert(iter != eiter);
  mt.x          = *(iter++); assert(iter != eiter);
  mt.y          = *(iter++); assert(iter != eiter);
  mt.z          = *(iter++); assert(iter != eiter);
  mt.px         = *(iter++); assert(iter != eiter);
  mt.py         = *(iter++); assert(iter != eiter);
  mt.pz         = *(iter++);
  
  mt.trackIndex = (helpVal&m_trackIDMask) >> m_trackIDIndex;
  mt.partID     = (helpVal&m_partIDMask) >> m_partIDIndex;
  mt.detectorID = (helpVal&m_detectorIDMask) >> m_detectorIDIndex;
  mt.crystalNo  = (helpVal&m_crystalNoMask) >> m_crystalNoIndex;
*/
  return;
}


StatusCode ZddMcHitBuilder::pack(ZddMcHitCol* zddMcHitCol, WriteRawEvent*& re)
{

  
  if (zddMcHitCol == NULL) {
    cout << "ZddMcHitBuilder::pack cant get ZddMcHitCol" << endl;
    return StatusCode::FAILURE;
  }

  vector<uint32_t> *zddReMcHitVec = new vector<uint32_t>;
  ZddTruth_t m_ZddTruth;

  ZddMcHitCol::const_iterator pZddMcHit = zddMcHitCol->begin();
  for ( ; pZddMcHit != zddMcHitCol->end(); pZddMcHit++) {
    /*   //make the ZddTruth data
    makeZddTruth(pZddMcHit, m_ZddTruth);
    //pack integers in zddTruth
    uint32_t helpVal = (m_ZddTruth.trackIndex<<m_trackIDIndex) & m_trackIDMask;
    helpVal |= ((m_ZddTruth.partID<<m_partIDIndex) & m_partIDMask);
    helpVal |= ((m_ZddTruth.detectorID<<m_detectorIDIndex) & m_detectorIDMask);
    helpVal |= ((m_ZddTruth.crystalNo<<m_crystalNoIndex) & m_crystalNoMask);
    //fill the McHit vector
    zddReMcHitVec->push_back(helpVal);
    zddReMcHitVec->push_back(m_ZddTruth.x);
    zddReMcHitVec->push_back(m_ZddTruth.y);
    zddReMcHitVec->push_back(m_ZddTruth.z);
    zddReMcHitVec->push_back(m_ZddTruth.px);
    zddReMcHitVec->push_back(m_ZddTruth.py);
    zddReMcHitVec->push_back(m_ZddTruth.pz);
    */ }
  /*
  OfflineEventFormat::SubDetectorHeader sh(OfflineEventFormat::ZDDTRUTH);
  SubRawEvent *sub = new SubRawEvent(sh, zddReMcHitVec);
  re->append(sub);
  */  

  return StatusCode::SUCCESS;
}

// initialize re2te tables

StatusCode  ZddMcHitBuilder::initialize(string& initFile)
{
  ifstream f;

  //read init file
  f.open(initFile.c_str());

  if ( f.bad() ) {
    cerr   << "Error: could not open file " << initFile << endl;
    return StatusCode::FAILURE;
  }

  if (!Builder::find( f, "##ZddTruthConf", initFile)) {
    cerr << "Error: could not find '##ZddTruthConf' in file " << initFile << endl;
    return StatusCode::FAILURE;
  }
  /*
  if ( !Builder::expect( f, "#ZddTruthShift", initFile) ||
       !Builder::expectInt( f, "trackIndex", initFile, m_trackIndexIndex, m_trackIndexMask) ||
       !Builder::expectInt( f, "", initFile, m_partIdIndex, m_partIdMask) ||
       !Builder::expectInt( f, "segId", initFile, m_segIdIndex, m_segIdMask) ||
       !Builder::expectInt( f, "gapId", initFile, m_gapIdIndex, m_gapIdMask) ||
       !Builder::expectInt( f, "stripId", initFile, m_stripIdIndex, m_stripIdMask) ||
       !Builder::expect( f, "#ZddTruthCoeff", initFile) ||
       !Builder::expectLong( f, "x", initFile, m_xCoeff) ||
       !Builder::expectLong( f, "y", initFile, m_yCoeff) ||
       !Builder::expectLong( f, "z", initFile, m_zCoeff) ||
       !Builder::expectLong( f, "px", initFile, m_pxCoeff) ||
       !Builder::expectLong( f, "py", initFile, m_pyCoeff) ||
       !Builder::expectLong( f, "pz", initFile, m_pzCoeff)    )
    return StatusCode::FAILURE;
  */
  f.close();

  return StatusCode::SUCCESS;
}


uint32_t ZddMcHitBuilder::getTEID(uint32_t reid)
{
  return 0;
}

 
uint32_t ZddMcHitBuilder::getREID(uint32_t teid)
{
  return 0;
}

void ZddMcHitBuilder::makeZddTruth
(ZddMcHitCol::const_iterator& pZddMcHit, ZddTruth_t& mt)
{
  Identifier ident = (*pZddMcHit)->identify();

  mt.trackIndex      = (*pZddMcHit)->getTrackID() % 1000;
  mt.partID          = ZddID::partID(ident);
  mt.detectorID      = ZddID::detectorID(ident);
  mt.preCrystalNo    = ZddID::crystalNo(ident);
  mt.eDep            = int((*pZddMcHit)->getEDep());
  mt.pdg             = int((*pZddMcHit)->getPDGCode());
  mt.preTime         = int((*pZddMcHit)->getPreTime());
  mt.postTime        = int((*pZddMcHit)->getPostTime());
  mt.preX            = int((*pZddMcHit)->getPrePositionX());
  mt.preY            = int((*pZddMcHit)->getPrePositionY());
  mt.preZ            = int((*pZddMcHit)->getPrePositionZ());
  mt.postX           = int((*pZddMcHit)->getPrePositionX());
  mt.postY           = int((*pZddMcHit)->getPrePositionY());
  mt.postZ           = int((*pZddMcHit)->getPrePositionZ());
  mt.px              = int((*pZddMcHit)->getPx());
  mt.py              = int((*pZddMcHit)->getPy());
  mt.pz              = int((*pZddMcHit)->getPz());
  return;
}

