//====================================================================
//      RawDataZddMcHitCnv.cxx
//====================================================================
//
// Description: A converter class to unpack Event Filter packed raw   
//              event persistent data and place it in the Transient    
//              Data Store of Athena.
//
//--------------------------------------------------------------------

// Include files.
#include <assert.h>
#include <vector>
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h" 
// for Mutil-thread by tianhl
#include "GaudiKernel/ThreadGaudi.h"
// for Mutil-thread by tianhl

#include "EventModel/EventModel.h"
#include "Identifier/Identifier.h"
#include "Identifier/ZddID.h"
#include "McTruth/ZddMcHit.h"
#include "RawDataCnv/EventManagement/RAWEVENT.h" 
#include "RawDataCnv/EventManagement/RawEvent.h" 
#include "RawDataCnv/RawDataInputSvc.h" 
#include "RawDataCnv/RawDataZddMcHitCnv.h"


using Event::ZddMcHit;
using Event::ZddMcHitCol;

// REMOVE THIS LINE AFTER "PACKEDRAWEVENT_StorageType" IS ADDED TO
// THE FILE "GaudiKernel/ClassID.h"
extern const CLID& CLID_ZddMcHitCol;

// Constructor.
RawDataZddMcHitCnv::RawDataZddMcHitCnv(ISvcLocator* svc) : 
RawDataBaseCnv(PACKEDRAWEVENT_StorageType, classID(), svc) 
{
  //init();
}

// Return the identification number of this converter to the 
// persistency service.
const CLID& RawDataZddMcHitCnv::classID()
{
  return CLID_ZddMcHitCol;
}

StatusCode RawDataZddMcHitCnv::initialize()
{
    std::string PackedRawDataCnvSvc_Name("PackedRawDataCnvSvc");
    std::string RawDataInputSvc_Name("RawDataInputSvc");
    std::string RawDataZddMcHitCnv_Name("RawDataZddMcHitCnv");
    
    // for Mutil-thread by tianhl
    //    ConversionSvc*  pCnvSvc = 0;
    //    if (pCnvSvc = dynamic_cast<ConversionSvc*>(conversionSvc())){
      SmartIF<IService> pCnvSvc(conversionSvc());
      if  (isGaudiThreaded(pCnvSvc->name())){
        PackedRawDataCnvSvc_Name += getGaudiThreadIDfromName(pCnvSvc->name());
        RawDataInputSvc_Name += getGaudiThreadIDfromName(pCnvSvc->name());
        RawDataZddMcHitCnv_Name += getGaudiThreadIDfromName(pCnvSvc->name());
      }
      //    }
    

  MsgStream log(messageService(), RawDataZddMcHitCnv_Name.c_str());

  StatusCode sc = RawDataBaseCnv::initialize();
  if (StatusCode::SUCCESS != sc) {
    log << MSG::ERROR << "RawDataBaseCnv: Cant initialize PackedRawDataCnvSvc" << endreq;
    return sc;
  }

  // Check RawDataCnvSvc
  IService* isvc = 0;
  sc = serviceLocator()->service(PackedRawDataCnvSvc_Name.c_str(), isvc, true);
  if (sc != StatusCode::SUCCESS) {
    log << MSG::ERROR << "Cant get PackedRawDataCnvSvc" << endreq;
  }

  m_RawDataAccess = dynamic_cast<PackedRawDataCnvSvc*> (isvc); 
  if (m_RawDataAccess  == 0 ) {
    log << MSG::ERROR << "RawDataZddCnv: Cant cast to RawDataCnvSvc " << endreq; 
    return StatusCode::FAILURE;
  }

  sc = serviceLocator()->getService(RawDataInputSvc_Name.c_str(), isvc);
  if (sc != StatusCode::SUCCESS ) {
    log << MSG::WARNING << "Cant get RawDataInputSvc " << endreq;
    return sc ;
  }

  m_inputSvc = dynamic_cast<RawDataInputSvc*> (isvc);
  if (m_inputSvc == 0 ) {
    log << MSG::WARNING << "Cant cast to RawDataInputSvc " << endreq;
    return StatusCode::FAILURE ;
  }

  return StatusCode::SUCCESS; 
}


StatusCode RawDataZddMcHitCnv::updateObj(IOpaqueAddress* pAddr, DataObject* pObj) {
  // Purpose and Method:  This method does nothing other than announce it has
  //   been called.

  //MsgStream log(msgSvc(), "RawDataZddMcHitCnv");
  //log << MSG::DEBUG << "RawDataZddMcHitCnv::updateObj" << endreq;
  return Converter::updateObj(pAddr, pObj);
}

// Create a converted object in the Transient Data Store.
StatusCode RawDataZddMcHitCnv::createObj(IOpaqueAddress* pAddr, DataObject*& pObj)
{
  //MsgStream log(msgSvc(), "RawDataZddMcHitCnv");

  //This converter will create an empty ZddMcHitCol on the TDS
  ZddMcHitCol *zddMcHitCol = new ZddMcHitCol;
  pObj = zddMcHitCol;
   
  RAWEVENT *evt = m_inputSvc->currentEvent(); 
  if (evt == NULL) {
      //log << MSG::ERROR << "RawDataCnv has no event!" << endreq; 
      return StatusCode::FAILURE;
  }
  
  assert((evt->getZddTruth().size()%7) == 0);

  ZddMcHit* zddMcHit;
  ZddTruth_t m_ZddTruth;

  std::vector<uint32_t>::const_iterator iter = evt->getZddTruth().begin();
  std::vector<uint32_t>::const_iterator eiter = evt->getZddTruth().end();

  for (int zddMcHitId = 0; iter != eiter; zddMcHitId++) {
    //retrieve the ZddTruth data
    m_zddMcHitBuilder.unPack(iter, eiter, m_ZddTruth);
    //construct the identifier

    int partID;
    if(m_ZddTruth.preCrystalNo < 0) partID = 1;
    else{ partID = 2;}

    int detectorID;
    if(m_ZddTruth.preCrystalNo < 200) detectorID= 1;
    else{ detectorID = 2;}


    const Identifier ident = ZddID::cell_id
      (partID, detectorID, abs(m_ZddTruth.preCrystalNo) );
    //construct the ZddMcHit
    zddMcHit = new ZddMcHit(ident, m_ZddTruth.trackIndex%1000,
			    m_ZddTruth.eDep,
			    m_ZddTruth.pdg,
			    m_ZddTruth.preTime,
			    m_ZddTruth.postTime,
			    m_ZddTruth.preX,
			    m_ZddTruth.postX,
			    m_ZddTruth.preY,
			    m_ZddTruth.postY,
			    m_ZddTruth.preZ,
			    m_ZddTruth.postZ,
			    m_ZddTruth.px,
			    m_ZddTruth.py,
			    m_ZddTruth.pz);
    // And add the stuff to the container
    zddMcHitCol->push_back(zddMcHit);
  }

  return StatusCode::SUCCESS; 

}

StatusCode RawDataZddMcHitCnv::createRep(DataObject* pObj, IOpaqueAddress*& pAddr)  
{
   // convert PixelRaw in the container into ByteStream
  //MsgStream log(messageService(), "RawDataZddMcHitCnv");
  
  WriteRawEvent*& re = m_RawDataAccess->getRawEvent(); 
  
  if (re == 0) {
     //log << " get RawEvent failed !" << endreq;
     return StatusCode::FAILURE; 
  }

  SmartDataPtr<ZddMcHitCol> mcHitCol(dataProvider(), EventModel::MC::ZddMcHitCol);

  if (mcHitCol == 0) {
     //log << "no ZddMcHitCol found" << endreq;
     return StatusCode::FAILURE;
  }
  
  StatusCode sc = m_zddMcHitBuilder.pack(mcHitCol, re);

  return sc; 

}

