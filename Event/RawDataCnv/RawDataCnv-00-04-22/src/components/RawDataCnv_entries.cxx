//====================================================================
//  RawDataCnv_entries.cxx
//--------------------------------------------------------------------
//
//  Package    : RawDataCnv
//
//  Description: Implementation of <Package>_load routine.
//               This routine is needed for forcing the linker
//               to load all the components of the library. 
//
//====================================================================

#include "GaudiKernel/DeclareFactoryEntries.h" 
#include "RawDataCnv/RawDataSelector.h"
#include "RawDataCnv/PackedRawDataCnvSvc.h"
#include "RawDataCnv/RawDataInputSvc.h"
#include "RawDataCnv/RawDataOutputSvc.h"
#include "RawDataCnv/RawDataCnv.h"
#include "RawDataCnv/RawDataEvtHeaderCnv.h"
#include "RawDataCnv/RawDataDigiCnv.h" 
#include "RawDataCnv/RawDataMdcDigiCnv.h"
#include "RawDataCnv/RawDataEmcDigiCnv.h"
#include "RawDataCnv/RawDataTofDigiCnv.h"
#include "RawDataCnv/RawDataLumiDigiCnv.h"
#include "RawDataCnv/RawDataMucDigiCnv.h"
#include "RawDataCnv/RawDataZddDigiCnv.h"
#include "RawDataCnv/RawDataTrigCnv.h"
#include "RawDataCnv/RawDataTrigGTDCnv.h"
#include "RawDataCnv/RawDataHltCnv.h"
#include "RawDataCnv/RawDataHltRawCnv.h"
#include "RawDataCnv/RawDataMcCnv.h" 
#include "RawDataCnv/RawDataMcParticleCnv.h" 
#include "RawDataCnv/RawDataMdcMcHitCnv.h" 
#include "RawDataCnv/RawDataEmcMcHitCnv.h" 
#include "RawDataCnv/RawDataTofMcHitCnv.h" 
#include "RawDataCnv/RawDataMucMcHitCnv.h" 
#include "RawDataCnv/RawDataZddMcHitCnv.h"
//#include "RawDataCnv/MixerAlg.h" 

DECLARE_CONVERTER_FACTORY( RawDataCnv )
DECLARE_CONVERTER_FACTORY( RawDataEvtHeaderCnv )
DECLARE_CONVERTER_FACTORY( RawDataDigiCnv )
DECLARE_CONVERTER_FACTORY( RawDataMdcDigiCnv )
DECLARE_CONVERTER_FACTORY( RawDataEmcDigiCnv )
DECLARE_CONVERTER_FACTORY( RawDataTofDigiCnv )
DECLARE_CONVERTER_FACTORY( RawDataLumiDigiCnv )
DECLARE_CONVERTER_FACTORY( RawDataMucDigiCnv )
DECLARE_CONVERTER_FACTORY( RawDataZddDigiCnv )
DECLARE_CONVERTER_FACTORY( RawDataTrigCnv)
DECLARE_CONVERTER_FACTORY( RawDataTrigGTDCnv)
DECLARE_CONVERTER_FACTORY( RawDataHltCnv)
DECLARE_CONVERTER_FACTORY( RawDataHltRawCnv)
DECLARE_CONVERTER_FACTORY( RawDataMcCnv )
DECLARE_CONVERTER_FACTORY( RawDataMcParticleCnv )
DECLARE_CONVERTER_FACTORY( RawDataMdcMcHitCnv )
DECLARE_CONVERTER_FACTORY( RawDataEmcMcHitCnv )
DECLARE_CONVERTER_FACTORY( RawDataTofMcHitCnv )
DECLARE_CONVERTER_FACTORY( RawDataMucMcHitCnv )
DECLARE_CONVERTER_FACTORY( RawDataZddMcHitCnv )
DECLARE_SERVICE_FACTORY( PackedRawDataCnvSvc )
DECLARE_SERVICE_FACTORY( RawDataInputSvc )
DECLARE_SERVICE_FACTORY( RawDataOutputSvc )
DECLARE_SERVICE_FACTORY( RawDataSelector )
//DECLARE_ALGORITHM_FACTORY( MixerAlg )

DECLARE_FACTORY_ENTRIES(RawDataCnv) 
{
   DECLARE_CONVERTER(RawDataCnv)
   DECLARE_CONVERTER(RawDataEvtHeaderCnv)
   DECLARE_CONVERTER(RawDataDigiCnv)
   DECLARE_CONVERTER(RawDataMdcDigiCnv)
   DECLARE_CONVERTER(RawDataEmcDigiCnv)
   DECLARE_CONVERTER(RawDataTofDigiCnv)
   DECLARE_CONVERTER(RawDataLumiDigiCnv)
   DECLARE_CONVERTER(RawDataMucDigiCnv)
   DECLARE_CONVERTER(RawDataZddDigiCnv) 
   DECLARE_CONVERTER(RawDataTrigCnv)
   DECLARE_CONVERTER(RawDataTrigGTDCnv)
   DECLARE_CONVERTER(RawDataHltCnv)
   DECLARE_CONVERTER(RawDataHltRawCnv)
   DECLARE_CONVERTER(RawDataMcCnv)
   DECLARE_CONVERTER(RawDataMcParticleCnv)
   DECLARE_CONVERTER(RawDataMdcMcHitCnv)
   DECLARE_CONVERTER(RawDataEmcMcHitCnv)
   DECLARE_CONVERTER(RawDataTofMcHitCnv)
   DECLARE_CONVERTER(RawDataMucMcHitCnv)
   DECLARE_CONVERTER(RawDataZddMcHitCnv)
   DECLARE_SERVICE(PackedRawDataCnvSvc)
   DECLARE_SERVICE(RawDataInputSvc)
   DECLARE_SERVICE(RawDataOutputSvc)
   DECLARE_SERVICE(RawDataSelector )
   //DECLARE_ALGORITHM( MixerAlg )
}
