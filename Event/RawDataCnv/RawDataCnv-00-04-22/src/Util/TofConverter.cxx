#include "RawDataCnv/Util/TofConverter.h"

TofConverter* TofConverter::s_instance = 0;

TofConverter* TofConverter::instance()
{
   if ( s_instance == 0 ) {
      s_instance = new TofConverter();
   }

   return s_instance;
}

void TofConverter::destroy()
{
   if ( s_instance != 0 ) {
      delete s_instance;
      s_instance = 0;
   }
}

StatusCode TofConverter::convert(const BufferHolder& src, TofDigiCol* des, LumiDigiCol* des2)
{
   uint32_t REId, TEId, TEData, TEOverflow, TETorQ;
   uint32_t nbuf = src.nBuf();

   TofDigi* tofDigi;
   typedef std::multimap<uint32_t, TofDigi*>::iterator my_iter;
   std::multimap<uint32_t, TofDigi*> mapIdData;
   my_iter it;

   for (uint32_t i = 0; i < nbuf; ++i) {
      uint32_t* buf = src(i);
      uint32_t  bufSize = src.bufSize(i);
      for (uint32_t j = 0; j < bufSize; ++j) {
	 m_builder.unPack(buf[j], REId, TEData, TEOverflow, TETorQ);
	 TEId = m_builder.getTEID( REId);
	 if ( TEId == 0xFFFFFFFF ) {
	    uint32_t data_unit = buf[j];
	    if ( (data_unit >> 25) == 0x7F ) {
	       tofDigi = new TofDigi(Identifier(0xFFFFFFFF), 0x7FFFFFFF, 0x7FFFFFFF);
	       tofDigi->setOverflow(data_unit);
	       des->push_back(tofDigi);
	    }
	    continue;
	 }

	 uint32_t count = mapIdData.count(TEId);

	 if (count == 0) {
	    if (TETorQ) {  //Q
	       tofDigi = new TofDigi(Identifier(TEId), 0x7FFFFFFF, TEData);
	       tofDigi->setOverflow(0x10 | (TEOverflow<<1));
	    }
	    else {  //T
	       tofDigi = new TofDigi(Identifier(TEId), TEData, 0x7FFFFFFF);
	       tofDigi->setOverflow(0x20 | TEOverflow);
	    }
	    mapIdData.insert(make_pair(TEId, tofDigi));
	 }
	 else {
	    pair<my_iter, my_iter> range = mapIdData.equal_range(TEId);
	    it = range.first;
	    tofDigi = it->second;
	    if (TETorQ) {  //Q
	       if (tofDigi->getChargeChannel() == 0x7FFFFFFF) {  //matched Q and T, first Q
		  tofDigi->setChargeChannel(TEData);
		  tofDigi->setOverflow((tofDigi->getOverflow() | (TEOverflow<<1)) & 0xF);
		  while ((++it) != range.second) {  //multiT
		     tofDigi = it->second;
		     tofDigi->setOverflow(tofDigi->getOverflow() & 0xF);
		  }
	       }
	       else {  //multiQ
		  uint32_t flag = (tofDigi->getOverflow() & 0x3C) | 8;
		  while (it != range.second) {
		     tofDigi = (it++)->second;
		     tofDigi->setOverflow((tofDigi->getOverflow()&0x3) | flag);
		  }
		  tofDigi = new TofDigi(Identifier(TEId), 0x7FFFFFFF, TEData);
		  tofDigi->setOverflow(flag | (TEOverflow<<1));
		  mapIdData.insert(make_pair(TEId, tofDigi));
	       }
	    }
	    else {  //T
	       if (tofDigi->getTimeChannel() == 0x7FFFFFFF) {  //matched T and Q, firstT
		  tofDigi->setTimeChannel(TEData);
		  tofDigi->setOverflow((tofDigi->getOverflow() | TEOverflow) & 0xF);
		  while ((++it) != range.second) {  //multiQ
		     tofDigi = it->second;
		     tofDigi->setOverflow(tofDigi->getOverflow() & 0xF);
		  }
	       }
	       else {  //multiT
		  uint32_t flag = (tofDigi->getOverflow() & 0x3C) | 4;
		  while (it != range.second) {
		     tofDigi = (it++)->second;
		     tofDigi->setOverflow((tofDigi->getOverflow()&0x3) | flag);
		  }
		  tofDigi = new TofDigi(Identifier(TEId), TEData, 0x7FFFFFFF);
		  tofDigi->setOverflow(flag | TEOverflow);
		  mapIdData.insert(make_pair(TEId, tofDigi));
	       }
	    }
	 }
      }
   }

   my_iter end = mapIdData.end();
   for (it = mapIdData.begin(); it != end; ++it) {
      TEId = it->first;
      tofDigi = it->second;
      if ( (TEId&0xFFFF7FFF) != 0x20000060 ) {
	 des->push_back(tofDigi);
      }
      //zoujh: for luminosity
      else {
	 if (des2 != 0) {
	    des2->push_back(new LumiDigi(tofDigi));
	 }
	 delete tofDigi;
      }
   }

   return StatusCode::SUCCESS;
}

StatusCode TofConverter::convert(TofDigiCol* src, WriteRawEvent*& des)
{
   return m_builder.pack(src, des);
}

TofConverter::TofConverter()
{
}

TofConverter::~TofConverter()
{
}
