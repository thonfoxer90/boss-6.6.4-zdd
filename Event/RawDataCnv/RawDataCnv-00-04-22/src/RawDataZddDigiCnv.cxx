//====================================================================
//      RawDataZddDigiCnv.cxx
//====================================================================
//
// Description: A converter class to unpack Event Filter packed raw   
//              event persistent data and place it in the Transient    
//              Data Store of Athena.
//
//--------------------------------------------------------------------

// Include files.
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h" 
// for Mutil-thread by tianhl
#include "GaudiKernel/ThreadGaudi.h"
// for Mutil-thread by tianhl

#include "EventModel/EventModel.h"
#include "ZddRawEvent/ZddDigi.h"
#include "RawDataCnv/EventManagement/RAWEVENT.h" 
#include "RawDataCnv/EventManagement/RawEvent.h" 
#include "RawDataCnv/RawDataInputSvc.h" 
#include "RawDataCnv/RawDataZddDigiCnv.h"



extern const CLID& CLID_ZddDigiCol;

// Constructor.
RawDataZddDigiCnv::RawDataZddDigiCnv(ISvcLocator* svc) : 
RawDataBaseCnv(PACKEDRAWEVENT_StorageType, classID(), svc) 
{
}

RawDataZddDigiCnv::~RawDataZddDigiCnv()
{
   ZddConverter::destroy();
}

// Return the identification number of this converter to the 
// persistency service.
const CLID& RawDataZddDigiCnv::classID()
{
   return CLID_ZddDigiCol;
}

StatusCode RawDataZddDigiCnv::initialize()
{
   std::string PackedRawDataCnvSvc_Name("PackedRawDataCnvSvc");
   std::string RawDataInputSvc_Name("RawDataInputSvc");
   std::string RawDataZddDigiCnv_Name("RawDataZddDigiCnv");

   // for Mutil-thread by tianhl
   //   ConversionSvc*  pCnvSvc = 0;
   //   if (pCnvSvc = dynamic_cast<ConversionSvc*>(conversionSvc())){
     SmartIF<IService> pCnvSvc(conversionSvc());
      if  (isGaudiThreaded(pCnvSvc->name())){
	 PackedRawDataCnvSvc_Name += getGaudiThreadIDfromName(pCnvSvc->name());
	 RawDataInputSvc_Name += getGaudiThreadIDfromName(pCnvSvc->name());
	 RawDataZddDigiCnv_Name += getGaudiThreadIDfromName(pCnvSvc->name());
      }
      //   }

   MsgStream log(messageService(), RawDataZddDigiCnv_Name.c_str());

   StatusCode sc = RawDataBaseCnv::initialize();
   if ( sc.isFailure() ) {
      return sc;
   }

   // Check RawDataCnvSvc
   IService* isvc = 0;
   StatusCode status = serviceLocator()->service(PackedRawDataCnvSvc_Name.c_str(), isvc, true);
   m_RawDataAccess = dynamic_cast<PackedRawDataCnvSvc*> (isvc); 
   if(m_RawDataAccess  == 0 ) {
      log<<MSG::ERROR<< "  ZddRawDataCnv: Cant cast to  RawDataCnvSvc " <<endreq; 
      return StatusCode::FAILURE ;
   }

   IService* svc ;
   sc = serviceLocator()->getService(RawDataInputSvc_Name.c_str(), svc);
   //if ( sc.isFailure() ) {
   //   log<<MSG::WARNING << " Cant get RawDataInputSvc " <<endreq;
   //   return sc ;
   //}
   m_inputSvc = dynamic_cast<RawDataInputSvc*> (svc);
   if ( m_inputSvc == 0 ) {
      log<< MSG::WARNING << " Cant cast to  RawDataInputSvc " <<endreq;
      return StatusCode::FAILURE ;
   }

   m_cnv = ZddConverter::instance();

   return StatusCode::SUCCESS; 
}


StatusCode RawDataZddDigiCnv::updateObj(IOpaqueAddress* pAddr, DataObject* pObj) {
    // Purpose and Method:  This method does nothing other than announce it has
    //   been called.

    //MsgStream log(msgSvc(), "RawDataZddDigiCnv");
    //log << MSG::DEBUG << "RawDataZddDigiCnv::updateObj" << endreq;
    return Converter::updateObj(pAddr, pObj);
}

// Create a converted object in the Transient Data Store.
StatusCode RawDataZddDigiCnv::createObj(IOpaqueAddress* pAddr, DataObject*& pObj)
{
  //MsgStream log(msgSvc(), "RawDataZddDigiCnv");
  // Purpose and Method:  This converter will create an empty ZddDigiCol on
  //   the TDS.
  ZddDigiCol *digiCol = new ZddDigiCol;
  pObj = digiCol;

  RAWEVENT* evt = m_inputSvc->currentEvent(); 
  if (evt == NULL) {
    cout << "RawDataZddDigiCnv::createObj has event!" << endl; 
    return StatusCode::FAILURE;
  }

  const BufferHolder& zddBuf = evt->getZddBuf();

  return m_cnv->convert(zddBuf, digiCol); 
}

StatusCode RawDataZddDigiCnv::createRep(DataObject* pObj, IOpaqueAddress*& pAddr)  
{
  // convert PixelRaw in the container into ByteStream
  //MsgStream log(messageService(), "RawDataZddDigiCnv");

  WriteRawEvent*& re = m_RawDataAccess->getRawEvent(); 
  if (re == 0) {
    //log << " get RawEvent failed !" << endreq;
    return StatusCode::FAILURE; 
  }

  SmartDataPtr<ZddDigiCol> digiCol(dataProvider(), EventModel::Digi::ZddDigiCol);
  if (digiCol == 0) {
    //log << "no ZddDigiCol found" << endreq;
    return StatusCode::FAILURE;
  }
  /*
  else {
    log << MSG::INFO << endreq << "Detailed dump of 1st event: " << endreq << endreq;
    int ndigi = 0;
    ZddDigiCol::const_iterator pZddDigi = digiCol->begin();
    for (pZddDigi; pZddDigi!= digiCol->end(); pZddDigi++) {
      log <<MSG::INFO << "Digi " << ndigi++ << " ";
      // output the digi proper:  " << digi " no longer works because
      // "digi" now returns an integer sort order
      (**pZddDigi).fillStream(log.stream());
      log << endreq;
    }
  }
  */

  return m_cnv->convert(digiCol, re);
}
