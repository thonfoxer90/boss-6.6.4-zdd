// Include files.
#include <vector>
#include "WriteRawData.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/PropertyMgr.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "RawDataCnv/EventManagement/RAWEVENT.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IDataManagerSvc.h"
 
// Event Model related classes
#include "EventModel/Event.h"
#include "RawEvent/DigiEvent.h"
#include "MdcRawEvent/MdcDigi.h"
#include "EmcRawEvent/EmcDigi.h"
#include "TofRawEvent/TofDigi.h"
#include "MucRawEvent/MucDigi.h"
#include "ZddRawEvent/ZddDigi.h"

#include "EventModel/EventModel.h"

using namespace std;

// Constructor.
WriteRawData::WriteRawData(const string& name, ISvcLocator* pSvcLocator)
   : Algorithm(name, pSvcLocator)
{}

// WriteRawData::initialize().
StatusCode WriteRawData::initialize()
{

   MsgStream log(msgSvc(), name());
   m_count = 0; 

   log << MSG::INFO << "in initialize()" << endreq;

   return StatusCode::SUCCESS;
}

// WriteRawData::execute().
StatusCode WriteRawData::execute() 
{
    StatusCode sc;   
    MsgStream log( msgSvc(), name() );

    log << MSG::INFO << "================================================" << endreq;
    log << MSG::INFO << "In execute()" << endreq;
    log << MSG::INFO << "++++++++++++++++++++++++++++++++++++++++++++++++" << endreq;
 
    static int evtnum = 0;
    static int runnum = 999;
 
    // Create the Event header and set it as "root" of the event store
    Event::EventHeader* event = new Event::EventHeader;
    event->setEventNumber(evtnum++);
    event->setRunNumber(runnum);
 
    IDataManagerSvc* evtmgr = dynamic_cast<IDataManagerSvc*>(eventSvc());
    sc = evtmgr->setRoot(EventModel::EventHeader , event);
    if( sc.isFailure() ) {
       log << MSG::ERROR << "Unable to register /Event object" << endreq;
       return sc;
    }
    
    DigiEvent* digi = new DigiEvent;
    log << MSG::INFO << " DigiEvent clID :: " << digi->clID() << endreq;
    sc = eventSvc()->registerObject(EventModel::Digi::Event, digi);
    if( sc.isFailure() ) {
       log << MSG::ERROR << "Unable to register /Event/Digi object" << endreq;
       return sc;
    }
  
    sc = writeMdcDigiData();
    if( sc.isFailure() ) {
       log << MSG::ERROR << "Unable to register /Event/Digi/MdcDigiCol object" << endreq;
       return sc;
    }

    sc = writeEmcDigiData();
    if( sc.isFailure() ) {
       log << MSG::ERROR << "Unable to register /Event/Digi/EmcDigiCol object" << endreq;
       return sc;
    }

    sc = writeTofDigiData();
    if( sc.isFailure() ) {
       log << MSG::ERROR << "Unable to register /Event/Digi/TofDigiCol object" << endreq;
       return sc;
    }

    sc = writeMucDigiData();
    if( sc.isFailure() ) {
       log << MSG::ERROR << "Unable to register /Event/Digi/MucDigiCol object" << endreq;
       return sc;
    }
    
    sc = writeZddDigiData();
    if( sc.isFailure() ) {
      log << MSG::ERROR << "Unable to register /Event/Digi/ZddDigiCol object" << endreq;
      return sc;
    }

    m_count++;

   return StatusCode::SUCCESS;
}

StatusCode WriteRawData::writeMdcDigiData()
{
    int n = 10;
    MdcDigiCol* mdcCol = new MdcDigiCol;
    for (int i=0; i < n; i++) {
        // Create new MdcDigi
        MdcDigi* mdcDigi = new MdcDigi(m_count);
        mdcDigi->setTimeChannel(8);
        mdcDigi->setChargeChannel(10);
        mdcDigi->setOverflow(0);
        // And add the stuff to the container
        mdcCol->push_back ( mdcDigi );
    }
 
    StatusCode sc = StatusCode::SUCCESS;
    sc = eventSvc()->registerObject(EventModel::Digi::MdcDigiCol, mdcCol);
    return sc;
} 

StatusCode WriteRawData::writeEmcDigiData()
{
    int n = 10;
    EmcDigiCol* emcCol = new EmcDigiCol;
    for (int i=0; i < n; i++) {
        // Create new EmcDigi
        EmcDigi* emcDigi = new EmcDigi(m_count);
        emcDigi->setTimeChannel(9);
        emcDigi->setChargeChannel(9);
        emcDigi->setMeasure(0);
        // And add the stuff to the container
        emcCol->push_back ( emcDigi );
    }
 
    StatusCode sc = StatusCode::SUCCESS;
    sc = eventSvc()->registerObject(EventModel::Digi::EmcDigiCol, emcCol);
    return sc;
}

StatusCode WriteRawData::writeTofDigiData()
{
    int n = 10;
    TofDigiCol* tofCol = new TofDigiCol;
    for (int i=0; i < n; i++) {
        // Create new TofDigi
        TofDigi* tofDigi = new TofDigi(m_count);
        tofDigi->setTimeChannel(10);
        tofDigi->setChargeChannel(8);
        tofDigi->setOverflow(0);
        // And add the stuff to the container
        tofCol->push_back ( tofDigi );
    }
 
    StatusCode sc = StatusCode::SUCCESS; 
    sc = eventSvc()->registerObject(EventModel::Digi::TofDigiCol, tofCol);
    return sc;
}

StatusCode WriteRawData::writeMucDigiData()
{
    int n = 10;
    MucDigiCol* mucCol = new MucDigiCol;
    for (int i=0; i < n; i++) {
        // Create new MucDigi
        MucDigi* mucDigi = new MucDigi(m_count);
        // And add the stuff to the container
        mucCol->push_back ( mucDigi );
    }
 
    StatusCode sc = StatusCode::SUCCESS; 
    sc = eventSvc()->registerObject(EventModel::Digi::MucDigiCol, mucCol);

    return sc;
}

StatusCode WriteRawData::writeZddDigiData()
{
  int n = 10;
  ZddDigiCol* zddCol = new ZddDigiCol;
  for (int i=0; i < n; i++) {
    // Create new ZddDigi                                                                                                                                                                                                                                                  
    ZddDigi* zddDigi = new ZddDigi(m_count);
    // And add the stuff to the container                                                                                                                                                                                                                                  
    zddCol->push_back ( zddDigi );
  }

  StatusCode sc = StatusCode::SUCCESS;
  sc = eventSvc()->registerObject(EventModel::Digi::ZddDigiCol, zddCol);

  return sc;
}


// WriteRawData::finalize().
StatusCode WriteRawData::finalize() 
{

   MsgStream log(msgSvc(), name());

   log << MSG::INFO << "In finalize()" << endreq;

   return StatusCode::SUCCESS;
}
