// Include files.
#include <vector>
#include "ReadRawData.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/PropertyMgr.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "RawDataCnv/EventManagement/RAWEVENT.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IDataManagerSvc.h"
 
// Event Model related classes
#include "EventModel/Event.h"
#include "RawEvent/DigiEvent.h"
#include "MdcRawEvent/MdcDigi.h"
#include "EmcRawEvent/EmcDigi.h"
#include "TofRawEvent/TofDigi.h"
#include "MucRawEvent/MucDigi.h"
#include "ZddRawEvent/ZddDigi.h"
#include "EventModel/EventModel.h"

using namespace std;

// Constructor.
ReadRawData::ReadRawData(const string& name, ISvcLocator* pSvcLocator)
   : Algorithm(name, pSvcLocator)
{}

// ReadRawData::initialize().
StatusCode ReadRawData::initialize()
{

   MsgStream log(msgSvc(), name());
   m_count = 0; 

   log << MSG::INFO << "in initialize()" << endreq;

   return StatusCode::SUCCESS;
}

// ReadRawData::execute().
StatusCode ReadRawData::execute() 
{
   StatusCode sc;   
   MsgStream log( msgSvc(), name() );

   log << MSG::INFO << "================================================" << endreq;
   log << MSG::INFO << "In execute()" << endreq;
   log << MSG::INFO << "++++++++++++++++++++++++++++++++++++++++++++++++" << endreq;

   SmartDataPtr<Event::EventHeader> evt(eventSvc(), EventModel::EventHeader);
   if (!evt) {
       log << MSG::ERROR << "Did not retrieve event" << endreq;
       return StatusCode::FAILURE;
   }
   //Check event header
 
   int eventNo = evt->eventNumber();
   int runNo   = evt->runNumber();

   log << MSG::INFO << "ReadRawData: retrieved event: " << eventNo << "  run: " << runNo << endreq;

   log << MSG::INFO << "ReadRawData: start to read MdcDigiData" << endreq;
   sc = readMdcDigiData(); 

   log << MSG::INFO << "ReadRawData: start to read EmcDigiData" << endreq;
   sc = readEmcDigiData();

   log << MSG::INFO << "ReadRawData: start to read TofDigiData" << endreq;
   sc = readTofDigiData();

   log << MSG::INFO << "ReadRawData: start to read MucDigiData" << endreq;
   sc = readMucDigiData();
 
   log << MSG::INFO << "ReadRawData: start to read ZddDigiData" << endreq;
   sc = readZddDigiData();


  if (sc.isFailure()) {
       return sc;
   }

   m_count++;
 

  return StatusCode::SUCCESS;
}

StatusCode ReadRawData::readMdcDigiData() {
    MsgStream log(msgSvc(), name());
    StatusCode sc = StatusCode::SUCCESS;

    SmartDataPtr<DigiEvent> digiEvt(eventSvc(), EventModel::Digi::Event);
    if (!digiEvt) {
        log << MSG::ERROR << "Did not retrieve DigiEvent" << endreq;
        return StatusCode::FAILURE;
    }

    SmartDataPtr<MdcDigiCol> digiCol(eventSvc(), EventModel::Digi::MdcDigiCol);

    if (digiCol == 0) {
        log << "no MdcDigiCol found" << endreq;
        return sc;
    } else {
        log << "Event No. " << m_count << " --->   " << digiCol->size() << " Mdc digis found " << endreq;

        log << MSG::INFO << "Detailed dump of this event: " << endreq;
        int ndigi = 0;
        MdcDigiCol::const_iterator pMdcDigi = digiCol->begin();
        for ( ; pMdcDigi!= digiCol->end(); pMdcDigi++) {
            log << MSG::INFO << "Digi " << ndigi++ << " ";
            // output the digi proper:  " << digi " no longer works because
            // "digi" now returns an integer sort order
            (**pMdcDigi).fillStream(log.stream());
            log << endreq;
        }
    }
    

    return StatusCode::SUCCESS;
}

StatusCode ReadRawData::readEmcDigiData() {
    MsgStream log(msgSvc(), name());
    StatusCode sc = StatusCode::SUCCESS;

 
    SmartDataPtr<EmcDigiCol> digiCol(eventSvc(), EventModel::Digi::EmcDigiCol);

 
    if (digiCol == 0) {
        log << "no EmcDigiCol found" << endreq;
        return sc;
    } else {
        log << "Event No. " << m_count << " --->  " << digiCol->size() << " Emc digis found " << endreq;
        log << MSG::INFO << "Detailed dump of this event: " << endreq;
        int ndigi = 0;
        EmcDigiCol::const_iterator pEmcDigi = digiCol->begin();
        for ( ; pEmcDigi != digiCol->end(); pEmcDigi++) {
            log <<MSG::INFO << "Digi " << ndigi++ << " ";
            (**pEmcDigi).fillStream(log.stream());
            log << endreq;
        }
    }
 
 
    return StatusCode::SUCCESS;
}

StatusCode ReadRawData::readTofDigiData() {
    MsgStream log(msgSvc(), name());
    StatusCode sc = StatusCode::SUCCESS;
 
    SmartDataPtr<TofDigiCol> digiCol(eventSvc(), EventModel::Digi::TofDigiCol);
 
    if (digiCol == 0) {
        log << "no TofDigiCol found" << endreq;
        return sc;
    } else {
        log << "Event No. " << m_count << " --->  " << digiCol->size() << " Tof digis found " << endreq;
        log << MSG::INFO << "Detailed dump of this event: " << endreq;
        int ndigi = 0;
        TofDigiCol::const_iterator pTofDigi = digiCol->begin();
        for ( ; pTofDigi!= digiCol->end(); pTofDigi++) {
            log <<MSG::INFO << "Digi " << ndigi++ << " ";
            (**pTofDigi).fillStream(log.stream());
            log << endreq;
        }
    }
 
 
    return StatusCode::SUCCESS;
}

StatusCode ReadRawData::readMucDigiData() {
    MsgStream log(msgSvc(), name());
    StatusCode sc = StatusCode::SUCCESS;
 
    SmartDataPtr<MucDigiCol> digiCol(eventSvc(), EventModel::Digi::MucDigiCol);
 
    if (digiCol == 0) {
        log << "no MucDigiCol found" << endreq;
        return sc;
    } else {
        log << "Event No. " << m_count << " --->  " << digiCol->size() << " Muc digis found " << endreq;
        log << MSG::INFO << "Detailed dump of this event: " << endreq;
        int ndigi = 0;
        MucDigiCol::const_iterator pMucDigi = digiCol->begin();
        for ( ; pMucDigi!= digiCol->end(); pMucDigi++) {
            log <<MSG::INFO << "Digi " << ndigi++ << " ";
            (**pMucDigi).fillStream(log.stream());
            log << endreq;
        }
    }
 
 
    return StatusCode::SUCCESS;
}

StatusCode ReadRawData::readZddDigiData() {
  MsgStream log(msgSvc(), name());
  StatusCode sc = StatusCode::SUCCESS;

  SmartDataPtr<ZddDigiCol> digiCol(eventSvc(), EventModel::Digi::ZddDigiCol);

  if (digiCol == 0) {
    log << "no ZddDigiCol found" << endreq;
    return sc;
  } else {
    log << "Event No. " << m_count << " --->  " << digiCol->size() << " Zdd digis found " << endreq;
    log << MSG::INFO << "Detailed dump of this event: " << endreq;
    int ndigi = 0;
    ZddDigiCol::const_iterator pZddDigi = digiCol->begin();
    for ( ; pZddDigi!= digiCol->end(); pZddDigi++) {
      log <<MSG::INFO << "Digi " << ndigi++ << " ";
      (**pZddDigi).fillStream(log.stream());
      log << endreq;
    }
  }


  return StatusCode::SUCCESS;
}
  
// ReadRawData::finalize().
StatusCode ReadRawData::finalize() 
{

   MsgStream log(msgSvc(), name());

   log << MSG::INFO << "In finalize()" << endreq;

   return StatusCode::SUCCESS;
}
