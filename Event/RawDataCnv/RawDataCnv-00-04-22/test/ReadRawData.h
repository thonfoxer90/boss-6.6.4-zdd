#include "GaudiKernel/Algorithm.h"

class ReadRawData:public Algorithm {

public:

  ReadRawData (const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();

private:
  StatusCode readMdcDigiData();
  StatusCode readEmcDigiData();
  StatusCode readTofDigiData();
  StatusCode readMucDigiData();
  StatusCode readZddDigiData();

  int m_count;

};
