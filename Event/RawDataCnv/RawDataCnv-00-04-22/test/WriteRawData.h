#include "GaudiKernel/Algorithm.h"

class WriteRawData:public Algorithm {

public:

  WriteRawData (const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();

private:
  StatusCode writeMdcDigiData();
  StatusCode writeEmcDigiData();
  StatusCode writeTofDigiData();
  StatusCode writeMucDigiData();
  StatusCode writeZddDigiData();

  int m_count;

};
