#include "GaudiKernel/DeclareFactoryEntries.h"
#include "test/ReadRawData.h"
#include "test/WriteRawData.h"

DECLARE_ALGORITHM_FACTORY(ReadRawData);
DECLARE_ALGORITHM_FACTORY(WriteRawData);

DECLARE_FACTORY_ENTRIES(TestRawDataCnv) 
{
  DECLARE_ALGORITHM(ReadRawData);
  DECLARE_ALGORITHM(WriteRawData);
}
