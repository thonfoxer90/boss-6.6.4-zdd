----------> uses
# use BesPolicy BesPolicy-* 
#   use BesCxxPolicy BesCxxPolicy-* 
#     use GaudiPolicy v*  (no_version_directory)
#       use LCG_Settings *  (no_version_directory)
#         use LCG_SettingsCompat *  (no_version_directory)
#       use Python * LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.5.4p2)
#         use LCG_Configuration v*  (no_version_directory)
#         use LCG_Settings v*  (no_version_directory)
#       use tcmalloc v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=1.4)
#         use LCG_Configuration v*  (no_version_directory)
#         use LCG_Settings v*  (no_version_directory)
#   use BesFortranPolicy BesFortranPolicy-* 
#     use LCG_Settings v*  (no_version_directory)
# use GaudiInterface GaudiInterface-01-* External
#   use GaudiKernel *  (no_version_directory)
#     use GaudiPolicy v*  (no_version_directory)
#     use ROOT v* LCG_Interfaces (no_version_directory) (native_version=5.24.00b)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#       use GCCXML v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=0.9.0_20090601)
#         use LCG_Configuration v*  (no_version_directory)
#         use LCG_Settings v*  (no_version_directory)
#       use Python v* LCG_Interfaces (no_auto_imports) (no_version_directory)
#     use Reflex v* LCG_Interfaces (no_version_directory)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#       use ROOT v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=5.24.00b)
#     use Boost v* LCG_Interfaces (no_version_directory) (native_version=1.39.0_python2.5)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#       use Python v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.5.4p2)
#     use CppUnit v* LCG_Interfaces (private) (no_auto_imports) (no_version_directory) (native_version=1.12.1_p1)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#   use GaudiSvc *  (no_version_directory)
#     use GaudiKernel v*  (no_version_directory)
#     use Reflex v* LCG_Interfaces (no_auto_imports) (no_version_directory)
#     use CLHEP v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.0.4.5)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#     use AIDA v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=3.2.1)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#     use Boost v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=1.39.0_python2.5)
#     use ROOT v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=5.24.00b)
#     use PCRE v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=4.4)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
# use RawFile RawFile-* Event
#   use BesPolicy BesPolicy-* 
#   use BesExternalArea BesExternalArea-* External
#   use IRawFile IRawFile-* Event
#     use BesPolicy BesPolicy-01-* 
#   use XmlRpcC XmlRpcC-* External
#     use BesExternalArea BesExternalArea-* External
# use EventModel EventModel-* Event
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-* External
# use RawDataCnvBase RawDataCnvBase-* Event
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-01-* External
#   use EventModel EventModel-* Event
# use RawEvent RawEvent-00-* Event
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-01-* External
#   use Identifier Identifier-* DetectorDescription
#     use BesPolicy BesPolicy-* 
#   use EventModel EventModel-* Event
# use McTruth McTruth-* Event
#   use BesPolicy BesPolicy-01-* 
#   use EventModel EventModel-* Event
#   use GaudiInterface GaudiInterface-01-* External
#   use Identifier Identifier-* DetectorDescription
#   use RelTable RelTable-* Event
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-01-* External
#   use BesCLHEP BesCLHEP-* External (private)
#     use CLHEP v* LCG_Interfaces (no_version_directory) (native_version=2.0.4.5)
#     use HepMC * LCG_Interfaces (no_version_directory) (native_version=2.03.11)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#     use HepPDT * LCG_Interfaces (no_version_directory) (native_version=2.05.04)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#     use BesExternalArea BesExternalArea-* External
# use Identifier Identifier-* DetectorDescription
# use MdcRawEvent MdcRawEvent-* Mdc
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-01-* External
#   use RawEvent RawEvent-* Event
#   use EventModel EventModel-* Event
# use TofRawEvent TofRawEvent-* Tof
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-01-* External
#   use RawEvent RawEvent-* Event
#   use EventModel EventModel-* Event
# use EmcRawEvent EmcRawEvent-* Emc
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-01-* External
#   use RawEvent RawEvent-* Event
#   use EventModel EventModel-* Event
#   use Identifier Identifier-* DetectorDescription
#   use EmcWaveform EmcWaveform-* Emc
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-* External
# use MucRawEvent MucRawEvent-* Muc
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-01-* External
#   use RawEvent RawEvent-* Event
#   use EventModel EventModel-* Event
# use ZddRawEvent ZddRawEvent-* Zdd
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-01-* External
#   use RawEvent RawEvent-* Event
#   use EventModel EventModel-* Event
# use TrigEvent TrigEvent-* Event
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-01-* External
# use HltEvent HltEvent-* Event
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-01-* External
#   use RawEvent RawEvent-* Event
#   use EventModel EventModel-* Event
#   use Identifier Identifier-* DetectorDescription
# use LumiDigi LumiDigi-* Event
#   use BesPolicy BesPolicy-* 
#   use GaudiInterface GaudiInterface-01-* External
#   use RawEvent RawEvent-* Event
#   use TofRawEvent TofRawEvent-* Tof
# use eformat eformat-* Event
#   use BesPolicy BesPolicy-* 
#   use ers ers-* Event
#     use BesPolicy BesPolicy-* 
# use RawDataProviderSvc RawDataProviderSvc-* Event
#   use BesPolicy BesPolicy-01-* 
#   use RootPolicy RootPolicy-* 
#     use BesPolicy BesPolicy-* 
#     use BesROOT BesROOT-00-* External
#       use CASTOR v* LCG_Interfaces (no_version_directory) (native_version=2.1.8-10)
#         use LCG_Configuration v*  (no_version_directory)
#         use LCG_Settings v*  (no_version_directory)
#       use ROOT v* LCG_Interfaces (no_version_directory) (native_version=5.24.00b)
#   use GaudiInterface GaudiInterface-01-* External
#   use BesCLHEP BesCLHEP-* External
#   use MdcRawEvent MdcRawEvent-* Mdc
#   use MdcRecEvent MdcRecEvent-* Mdc
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use MdcGeomSvc MdcGeomSvc-* Mdc
#       use BesPolicy BesPolicy-01-* 
#       use GaudiInterface GaudiInterface-* External
#       use calibUtil * Calibration
#         use GaudiInterface GaudiInterface-01-* External
#         use facilities * Calibration
#           use BesPolicy BesPolicy-* 
#         use xmlBase * Calibration
#           use BesPolicy * 
#           use XercesC * LCG_Interfaces (no_version_directory) (native_version=2.8.0)
#             use LCG_Configuration v*  (no_version_directory)
#             use LCG_Settings v*  (no_version_directory)
#           use facilities * Calibration
#         use rdbModel * Calibration
#           use BesPolicy * 
#           use facilities * Calibration
#           use xmlBase * Calibration
#           use MYSQL * External
#             use mysql * LCG_Interfaces (no_version_directory) (native_version=5.0.18)
#               use LCG_Configuration v*  (no_version_directory)
#               use LCG_Settings v*  (no_version_directory)
#         use BesROOT BesROOT-00-* External
#         use DatabaseSvc DatabaseSvc-* Database
#           use BesPolicy BesPolicy-* 
#           use GaudiInterface GaudiInterface-* External
#           use mysql * LCG_Interfaces (no_version_directory) (native_version=5.0.18)
#           use sqlite * LCG_Interfaces (no_version_directory) (native_version=3.6.11)
#             use LCG_Configuration v*  (no_version_directory)
#             use LCG_Settings v*  (no_version_directory)
#           use BesROOT * External
#       use CalibData * Calibration
#         use facilities facilities-* Calibration
#         use GaudiInterface * External
#         use BesROOT BesROOT-00-* External
#       use EventModel EventModel-* Event
#       use BesCLHEP BesCLHEP-00-* External (private)
#     use RelTable RelTable-* Event
#     use EventModel EventModel-* Event
#     use Identifier Identifier-* DetectorDescription
#     use DstEvent DstEvent-* Event
#       use BesPolicy BesPolicy-* 
#       use GaudiInterface GaudiInterface-* External
#       use BesCLHEP BesCLHEP-* External
#       use EventModel EventModel-* Event
#     use BesCLHEP BesCLHEP-* External (private)
#   use TofRawEvent TofRawEvent-* Tof
#   use TofCaliSvc TofCaliSvc-* Tof
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-* External
#     use calibUtil * Calibration
#     use CalibData * Calibration
#     use CalibSvc * Calibration
#       use CalibMySQLCnv * Calibration/CalibSvc
#         use BesPolicy * 
#         use calibUtil * Calibration
#         use CalibData * Calibration
#         use GaudiInterface * External
#         use MYSQL MYSQL-00-* External
#         use CalibDataSvc * Calibration/CalibSvc
#           use BesPolicy * 
#           use BesROOT * External
#           use calibUtil * Calibration
#           use CalibData * Calibration
#           use DstEvent DstEvent-* Event
#           use EventModel EventModel-* Event
#           use GaudiInterface * External
#       use CalibROOTCnv * Calibration/CalibSvc
#         use BesPolicy * 
#         use calibUtil * Calibration
#         use CalibData * Calibration
#         use GaudiInterface * External
#         use BesROOT BesROOT-00-* External
#         use EventModel EventModel-* Event
#         use CalibDataSvc * Calibration/CalibSvc
#         use CalibMySQLCnv * Calibration/CalibSvc
#       use CalibDataSvc * Calibration/CalibSvc
#       use CalibXmlCnvSvc * Calibration/CalibSvc
#         use BesPolicy * 
#         use calibUtil * Calibration
#         use GaudiInterface * External
#         use XercesC * LCG_Interfaces (no_version_directory) (native_version=2.8.0)
#         use CalibDataSvc * Calibration/CalibSvc
#       use CalibTreeCnv * Calibration/CalibSvc
#         use BesPolicy * 
#         use calibUtil * Calibration
#         use CalibData * Calibration
#         use MYSQL MYSQL-00-* External
#         use GaudiInterface * External
#         use BesROOT BesROOT-00-* External
#         use DstEvent DstEvent-* Event
#         use EventModel EventModel-* Event
#         use CalibDataSvc * Calibration/CalibSvc
#         use CalibMySQLCnv * Calibration/CalibSvc
#         use DatabaseSvc DatabaseSvc-* Database
#     use XercesC * LCG_Interfaces (no_version_directory) (native_version=2.8.0)
#     use MYSQL MYSQL-00-00-* External
#     use BesROOT BesROOT-* External
#     use BesCLHEP BesCLHEP-* External (private)
#   use TofQCorrSvc TofQCorrSvc-* Tof
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-* External
#     use CalibData * Calibration
#     use EventModel EventModel-* Event
#     use DatabaseSvc DatabaseSvc-* Database
#     use XercesC * LCG_Interfaces (no_version_directory) (native_version=2.8.0)
#     use MYSQL MYSQL-00-00-* External
#     use BesROOT BesROOT-* External
#     use BesCLHEP BesCLHEP-* External (private)
#   use TofQElecSvc TofQElecSvc-* Tof
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-* External
#     use calibUtil * Calibration
#     use CalibData * Calibration
#     use CalibSvc * Calibration
#     use XercesC * LCG_Interfaces (no_version_directory) (native_version=2.8.0)
#     use MYSQL MYSQL-00-00-* External
#     use BesROOT BesROOT-* External
#     use BesCLHEP BesCLHEP-* External (private)
#   use EmcRawEvent EmcRawEvent-* Emc
#   use EmcCalibConstSvc EmcCalibConstSvc-* Emc
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-* External
#     use CalibData CalibData-* Calibration
#     use CalibDataSvc CalibDataSvc-* Calibration/CalibSvc
#     use CalibROOTCnv CalibROOTCnv-* Calibration/CalibSvc
#     use EmcRecGeoSvc EmcRecGeoSvc-* Emc
#       use BesPolicy BesPolicy-* 
#       use Identifier Identifier-* DetectorDescription
#       use ROOTGeo ROOTGeo-* DetectorDescription
#         use BesPolicy BesPolicy-01-* 
#         use GaudiInterface GaudiInterface-* External
#         use BesCLHEP BesCLHEP-* External
#         use BesROOT BesROOT-* External
#         use XercesC * LCG_Interfaces (no_version_directory) (native_version=2.8.0)
#         use GdmlToRoot GdmlToRoot-* External
#           use BesExternalArea BesExternalArea-* External
#           use BesROOT BesROOT-* External
#         use GdmlManagement GdmlManagement-* DetectorDescription
#           use BesExternalArea BesExternalArea-* External
#       use BesCLHEP BesCLHEP-* External
#       use GaudiInterface GaudiInterface-* External
#     use EmcGeneralClass EmcGeneralClass-* Emc
#       use BesPolicy BesPolicy-* 
#       use Identifier Identifier-* DetectorDescription
#     use BesCLHEP BesCLHEP-* External
#   use MdcCalibFunSvc MdcCalibFunSvc-* Mdc
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use CalibData CalibData-* Calibration
#     use CalibSvc CalibSvc-* Calibration
#     use MdcGeomSvc MdcGeomSvc-* Mdc
#     use BesCLHEP BesCLHEP-* External
#   use EvTimeEvent EvTimeEvent-* Event
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use BesCLHEP BesCLHEP-* External
#     use MdcGeomSvc MdcGeomSvc-* Mdc
#     use RelTable RelTable-* Event
#     use EventModel EventModel-* Event
#     use Identifier Identifier-* DetectorDescription
# use NetDataReader NetDataReader-* DistBoss
#   use BesPolicy BesPolicy-* 
#   use IRawFile IRawFile-* Event
#   use DimSetup DimSetup-* DistBoss
#     use BesExternalArea BesExternalArea-* External
#   use DistBossUtil DistBossUtil-* DistBoss
#     use BesPolicy BesPolicy-* 
#     use IRawFile IRawFile-* Event
#   use ClientErrHandler ClientErrHandler-* DistBoss
#     use BesPolicy BesPolicy-* 
#     use DimSetup DimSetup-* DistBoss
#     use DistBossUtil DistBossUtil-* DistBoss
#
# Selection :
use CMT v1r20p20090520 (/cluster/him/bes3)
use BesExternalArea BesExternalArea-00-00-21 External (/cluster/him/bes3/dist/6.6.4/)
use DimSetup DimSetup-00-00-05 DistBoss (/cluster/him/bes3/dist/6.6.4/)
use GdmlManagement GdmlManagement-00-00-31 DetectorDescription (/home/bgarillo/boss-6.6.4-ZDD/)
use XmlRpcC XmlRpcC-00-00-01 External (/cluster/him/bes3/dist/6.6.4/)
use LCG_Configuration v1  (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5)
use LCG_SettingsCompat v1  (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5)
use LCG_Settings v1  (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5)
use sqlite v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use mysql v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use MYSQL MYSQL-00-00-09 External (/cluster/him/bes3/dist/6.6.4/)
use XercesC v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use CASTOR v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use HepPDT v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use HepMC v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use PCRE v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use AIDA v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use CLHEP v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use BesCLHEP BesCLHEP-00-00-09 External (/cluster/him/bes3/dist/6.6.4/)
use CppUnit v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use GCCXML v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use BesFortranPolicy BesFortranPolicy-00-01-03  (/cluster/him/bes3/dist/6.6.4)
use tcmalloc v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use Python v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use Boost v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use ROOT v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use BesROOT BesROOT-00-00-07 External (/cluster/him/bes3/dist/6.6.4/)
use GdmlToRoot GdmlToRoot-00-00-13 External (/cluster/him/bes3/dist/6.6.4/)
use Reflex v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use GaudiPolicy v10r4  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
use GaudiKernel v27r6  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
use GaudiSvc v18r6  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
use GaudiInterface GaudiInterface-01-03-07 External (/cluster/him/bes3/dist/6.6.4/)
use BesCxxPolicy BesCxxPolicy-00-01-01  (/cluster/him/bes3/dist/6.6.4)
use BesPolicy BesPolicy-01-05-03  (/cluster/him/bes3/dist/6.6.4)
use ROOTGeo ROOTGeo-00-00-15 DetectorDescription (/home/bgarillo/boss-6.6.4-ZDD/)
<<<<<<< HEAD
use DatabaseSvc DatabaseSvc-00-00-24-p01 Database (/cluster/him/bes3/dist/6.6.4/)
use facilities facilities-00-00-03 Calibration (/cluster/him/bes3/dist/6.6.4/)
use CalibData CalibData-00-01-09 Calibration (/cluster/him/bes3/dist/6.6.4/)
use xmlBase xmlBase-00-00-02 Calibration (/cluster/him/bes3/dist/6.6.4/)
use rdbModel rdbModel-00-01-00 Calibration (/cluster/him/bes3/dist/6.6.4/)
use calibUtil calibUtil-00-00-38 Calibration (/cluster/him/bes3/dist/6.6.4/)
use RootPolicy RootPolicy-00-01-02  (/cluster/him/bes3/dist/6.6.4)
use ers ers-00-00-02 Event (/cluster/him/bes3/dist/6.6.4/)
use eformat eformat-00-00-04 Event (/cluster/him/bes3/dist/6.6.4/)
use TrigEvent TrigEvent-00-01-01 Event (/cluster/him/bes3/dist/6.6.4/)
use EmcWaveform EmcWaveform-00-00-03 Emc (/cluster/him/bes3/dist/6.6.4/)
use RelTable RelTable-00-00-02 Event (/cluster/him/bes3/dist/6.6.4/)
use Identifier Identifier-00-02-13 DetectorDescription (/home/bgarillo/boss-6.6.4-ZDD/)
use EmcGeneralClass EmcGeneralClass-00-00-03 Emc (/cluster/him/bes3/dist/6.6.4/)
use EmcRecGeoSvc EmcRecGeoSvc-01-01-07 Emc (/cluster/him/bes3/dist/6.6.4/)
=======
use DatabaseSvc DatabaseSvc-00-00-22 Database (/cluster/bes3/dist/6.6.4.p01/)
use facilities facilities-00-00-03 Calibration (/cluster/bes3/dist/6.6.4.p01/)
use CalibData CalibData-00-01-09 Calibration (/cluster/bes3/dist/6.6.4.p01/)
use xmlBase xmlBase-00-00-02 Calibration (/cluster/bes3/dist/6.6.4.p01/)
use rdbModel rdbModel-00-01-00 Calibration (/cluster/bes3/dist/6.6.4.p01/)
use calibUtil calibUtil-00-00-38 Calibration (/cluster/bes3/dist/6.6.4.p01/)
use RootPolicy RootPolicy-00-01-02  (/cluster/bes3/dist/6.6.4.p01)
use ers ers-00-00-02 Event (/cluster/bes3/dist/6.6.4.p01/)
use eformat eformat-00-00-04 Event (/cluster/bes3/dist/6.6.4.p01/)
use TrigEvent TrigEvent-00-01-01 Event (/cluster/bes3/dist/6.6.4.p01/)
use EmcWaveform EmcWaveform-00-00-03 Emc (/cluster/bes3/dist/6.6.4.p01/)
use RelTable RelTable-00-00-02 Event (/cluster/bes3/dist/6.6.4.p01/)
use Identifier Identifier-00-02-13 DetectorDescription (/home/bgarillo/boss-6.6.4-ZDD/)
use EmcGeneralClass EmcGeneralClass-00-00-03 Emc (/cluster/bes3/dist/6.6.4.p01/)
use EmcRecGeoSvc EmcRecGeoSvc-01-01-07 Emc (/cluster/bes3/dist/6.6.4.p01/)
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
use EventModel EventModel-01-05-31 Event (/home/bgarillo/boss-6.6.4-ZDD/)
use TofQCorrSvc TofQCorrSvc-00-00-09 Tof (/cluster/him/bes3/dist/6.6.4/)
use DstEvent DstEvent-00-02-50-p01 Event (/cluster/him/bes3/dist/6.6.4/)
use CalibDataSvc CalibDataSvc-00-01-03 Calibration/CalibSvc (/cluster/him/bes3/dist/6.6.4/)
use CalibXmlCnvSvc CalibXmlCnvSvc-00-01-01 Calibration/CalibSvc (/cluster/him/bes3/dist/6.6.4/)
use CalibMySQLCnv CalibMySQLCnv-00-01-09 Calibration/CalibSvc (/cluster/him/bes3/dist/6.6.4/)
use CalibTreeCnv CalibTreeCnv-00-01-12 Calibration/CalibSvc (/cluster/him/bes3/dist/6.6.4/)
use CalibROOTCnv CalibROOTCnv-00-01-06 Calibration/CalibSvc (/cluster/him/bes3/dist/6.6.4/)
use EmcCalibConstSvc EmcCalibConstSvc-00-00-10 Emc (/cluster/him/bes3/dist/6.6.4/)
use CalibSvc CalibSvc-00-01-92 Calibration (/cluster/him/bes3/dist/6.6.4/)
use TofQElecSvc TofQElecSvc-00-00-04 Tof (/cluster/him/bes3/dist/6.6.4/)
use TofCaliSvc TofCaliSvc-00-01-05 Tof (/cluster/him/bes3/dist/6.6.4/)
use MdcGeomSvc MdcGeomSvc-00-01-37 Mdc (/cluster/him/bes3/dist/6.6.4/)
use EvTimeEvent EvTimeEvent-00-00-08 Event (/cluster/him/bes3/dist/6.6.4/)
use MdcCalibFunSvc MdcCalibFunSvc-00-03-15 Mdc (/cluster/him/bes3/dist/6.6.4/)
use MdcRecEvent MdcRecEvent-00-05-14 Mdc (/cluster/him/bes3/dist/6.6.4/)
use McTruth McTruth-00-02-19 Event (/home/bgarillo/boss-6.6.4-ZDD/)
use RawEvent RawEvent-00-03-19 Event (/home/bgarillo/boss-6.6.4-ZDD/)
use HltEvent HltEvent-00-02-06 Event (/cluster/him/bes3/dist/6.6.4/)
use ZddRawEvent ZddRawEvent-00-02-02 Zdd (/home/bgarillo/boss-6.6.4-ZDD/)
use MucRawEvent MucRawEvent-00-02-02 Muc (/cluster/him/bes3/dist/6.6.4/)
use EmcRawEvent EmcRawEvent-00-02-05 Emc (/cluster/him/bes3/dist/6.6.4/)
use TofRawEvent TofRawEvent-00-02-07 Tof (/cluster/him/bes3/dist/6.6.4/)
use LumiDigi LumiDigi-00-00-02 Event (/cluster/him/bes3/dist/6.6.4/)
use MdcRawEvent MdcRawEvent-00-03-08 Mdc (/cluster/him/bes3/dist/6.6.4/)
use RawDataProviderSvc RawDataProviderSvc-00-03-36 Event (/home/bgarillo/boss-6.6.4-ZDD/)
use RawDataCnvBase RawDataCnvBase-01-00-03 Event (/cluster/him/bes3/dist/6.6.4/)
use IRawFile IRawFile-00-00-03 Event (/cluster/him/bes3/dist/6.6.4/)
use DistBossUtil DistBossUtil-00-00-02 DistBoss (/cluster/him/bes3/dist/6.6.4/)
use ClientErrHandler ClientErrHandler-00-00-01 DistBoss (/cluster/him/bes3/dist/6.6.4/)
use NetDataReader NetDataReader-00-00-04 DistBoss (/cluster/him/bes3/dist/6.6.4/)
use RawFile RawFile-00-00-07 Event (/cluster/him/bes3/dist/6.6.4/)
----------> tags
CMTv1 (from CMTVERSION)
CMTr20 (from CMTVERSION)
CMTp20090520 (from CMTVERSION)
Linux (from uname) package BesPolicy implies [Unix host-linux]
x86_64-slc5-gcc43-opt (from CMTCONFIG) package LCG_Settings implies [Linux slc5-amd64 gcc43 optimized target-linux target-x86_64 target-slc5 target-gcc43 target-opt]
LOCAL (from CMTSITE)
bgarillo_no_config (from PROJECT) excludes [bgarillo_config]
bgarillo_root (from PROJECT) excludes [bgarillo_no_root]
bgarillo_cleanup (from PROJECT) excludes [bgarillo_no_cleanup]
bgarillo_no_prototypes (from PROJECT) excludes [bgarillo_prototypes]
bgarillo_with_installarea (from PROJECT) excludes [bgarillo_without_installarea]
bgarillo_with_version_directory (from PROJECT) excludes [bgarillo_without_version_directory]
bgarillo (from PROJECT)
BOSS_no_config (from PROJECT) excludes [BOSS_config]
BOSS_root (from PROJECT) excludes [BOSS_no_root]
BOSS_cleanup (from PROJECT) excludes [BOSS_no_cleanup]
BOSS_no_prototypes (from PROJECT) excludes [BOSS_prototypes]
BOSS_with_installarea (from PROJECT) excludes [BOSS_without_installarea]
BOSS_with_version_directory (from PROJECT) excludes [BOSS_without_version_directory]
GAUDI_no_config (from PROJECT) excludes [GAUDI_config]
GAUDI_root (from PROJECT) excludes [GAUDI_no_root]
GAUDI_cleanup (from PROJECT) excludes [GAUDI_no_cleanup]
GAUDI_prototypes (from PROJECT) excludes [GAUDI_no_prototypes]
GAUDI_with_installarea (from PROJECT) excludes [GAUDI_without_installarea]
GAUDI_without_version_directory (from PROJECT) excludes [GAUDI_with_version_directory]
LCGCMT_no_config (from PROJECT) excludes [LCGCMT_config]
LCGCMT_no_root (from PROJECT) excludes [LCGCMT_root]
LCGCMT_cleanup (from PROJECT) excludes [LCGCMT_no_cleanup]
LCGCMT_prototypes (from PROJECT) excludes [LCGCMT_no_prototypes]
LCGCMT_without_installarea (from PROJECT) excludes [LCGCMT_with_installarea]
LCGCMT_with_version_directory (from PROJECT) excludes [LCGCMT_without_version_directory]
x86_64 (from package CMT) package LCG_Settings implies [host-x86_64]
slc73 (from package CMT)
gcc432 (from package CMT) package LCG_Settings implies [gcc43 host-gcc43]
Unix (from package CMT) package LCG_Settings implies [host-unix] package LCG_Settings excludes [WIN32 Win32]
gcc43 (from package LCG_SettingsCompat)
amd64 (from package LCG_SettingsCompat)
slc5 (from package LCG_SettingsCompat)
slc5-amd64 (from package LCG_SettingsCompat) package LCG_SettingsCompat implies [slc5 amd64]
optimized (from package LCG_SettingsCompat) package BesPolicy implies [opt]
target-unix (from package LCG_Settings)
host-x86_64 (from package LCG_Settings)
host-gcc4 (from package LCG_Settings) package LCG_Settings implies [host-gcc]
host-gcc43 (from package LCG_Settings) package LCG_Settings implies [host-gcc4]
host-gcc (from package LCG_Settings)
host-linux (from package LCG_Settings)
host-unix (from package LCG_Settings)
target-linux (from package LCG_Settings) package LCG_Settings implies [target-unix]
target-slc5 (from package LCG_Settings) package LCG_Settings implies [target-slc]
target-opt (from package LCG_Settings)
target-gcc43 (from package LCG_Settings) package LCG_Settings implies [target-gcc4]
target-x86_64 (from package LCG_Settings)
target-slc (from package LCG_Settings)
target-gcc4 (from package LCG_Settings) package LCG_Settings implies [target-gcc]
target-gcc (from package LCG_Settings)
ROOT_GE_5_15 (from package LCG_Configuration)
ROOT_GE_5_19 (from package LCG_Configuration)
opt (from package BesCxxPolicy) package BesPolicy implies [optimized]
HasAthenaRunTime (from package BesPolicy)
ROOTBasicLibs (from package BesROOT)
----------> CMTPATH
# Add path /home/bgarillo/boss-6.6.4-ZDD from initialization
# Add path /cluster/him/bes3/dist/6.6.4 from initialization
# Add path /cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5 from initialization
# Add path /cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5 from initialization
