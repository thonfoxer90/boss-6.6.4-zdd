# echo "Setting RawDataCnv RawDataCnv-00-04-22 in /home/bgarillo/boss-6.6.4-ZDD/Event"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cluster/him/bes3/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh

tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=RawDataCnv -version=RawDataCnv-00-04-22 -path=/home/bgarillo/boss-6.6.4-ZDD/Event  -no_cleanup $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

