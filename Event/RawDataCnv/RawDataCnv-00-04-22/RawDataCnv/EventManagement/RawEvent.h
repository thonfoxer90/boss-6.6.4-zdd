#ifndef RAWEVENT_H
#define RAWEVENT_H

#include "eformat/FullEventFragment.h"
#include "eformat/write/FullEventFragment.h"
#include "eformat/SubDetectorFragment.h"

typedef eformat::FullEventFragment<const uint32_t*> RawEvent;
typedef eformat::write::FullEventFragment WriteRawEvent;
typedef eformat::SubDetectorFragment<const uint32_t*> SubRawEvent;

#endif  

