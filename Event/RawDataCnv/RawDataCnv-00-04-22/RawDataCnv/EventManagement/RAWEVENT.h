#ifndef _RAWEVENT_h_
#define _RAWEVENT_h_
 
#include <stdint.h>
#include <vector>
#include "RawDataCnv/Util/BufferHolder.h"

class RAWEVENT 
{
public:
   class RawEventHeader
   {
      public:
	 RawEventHeader() {};
	 ~RawEventHeader() {};
	 void setRunNo(uint32_t runno) { m_run_number = runno; }
	 void setEventNo(uint32_t eventno) { m_event_number = eventno; }
	 void setTime(uint32_t time) { m_time = time; }
	 void setFlag1(uint32_t flag1) { m_flag1 = flag1; }
	 void setFlag2(uint32_t flag2) { m_flag2 = flag2; }

	 uint32_t run_number() const { return m_run_number; }
	 uint32_t event_number() const { return m_event_number; }
	 uint32_t time() const { return m_time; }
	 uint32_t event_type() const { return m_event_type; }
	 uint32_t flag1() const { return m_flag1; }
	 uint32_t flag2() const { return m_flag2; }

      private:
	 uint32_t m_run_number;
	 uint32_t m_event_number;
	 uint32_t m_time;
	 uint32_t m_event_type;

	 uint32_t m_flag1;
	 uint32_t m_flag2;
   };

  RAWEVENT();
  virtual ~RAWEVENT() {}

  typedef std::vector<uint32_t> int_vector;

  //modifier
  void reset();
  void setRunNo(uint32_t run_no) { m_eventHeader.setRunNo(run_no); }
  void setEventNo(uint32_t event_no) { m_eventHeader.setEventNo(event_no); }
  void setTime(uint32_t time) { m_eventHeader.setTime(time); }
  void setFlag1(uint32_t flag1) { m_eventHeader.setFlag1(flag1); }
  void setFlag2(uint32_t flag2) { m_eventHeader.setFlag2(flag2); }

  // add digi
  void addReMdcDigi(uint32_t* digi, uint32_t size) {
    m_bufMdcDigi.addBuffer(digi, size);
  }
  void addReEmcDigi(uint32_t* digi, uint32_t size) {
    m_bufEmcDigi.addBuffer(digi, size);
  }
  void addReTofDigi(uint32_t* digi, uint32_t size) {
    m_bufTofDigi.addBuffer(digi, size);
  }
  void addReMucDigi(uint32_t* digi, uint32_t size) {
    m_bufMucDigi.addBuffer(digi, size);
  }
  // zdd
  void addReZddDigi(uint32_t* digi, uint32_t size) {
    m_bufZddDigi.addBuffer(digi, size);
  }
  // end of zdd

  void addReTrigGTD(uint32_t* digi, uint32_t size) {
    m_bufTrigGTD.addBuffer(digi, size);
  }
  void addReHltRaw(uint32_t* digi, uint32_t size) {
    m_bufHltRaw.addBuffer(digi, size);
  }

  // add MC data
  void addMcParticle(uint32_t* buf, uint32_t size) {
    m_bufMcPar.addBuffer(buf, size);
  }

  // should be replaced in future
  void setReMdcTruth(int_vector& ReMdcTruth) { m_ReMdcTruth = ReMdcTruth; }
  void setReEmcTruth(int_vector& ReEmcTruth) { m_ReEmcTruth = ReEmcTruth; }
  void setReTofTruth(int_vector& ReTofTruth) { m_ReTofTruth = ReTofTruth; }
  void setReMucTruth(int_vector& ReMucTruth) { m_ReMucTruth = ReMucTruth; }
  void setReZddTruth(int_vector& ReZddTruth) { m_ReZddTruth = ReZddTruth; }


  //visitor
  const RawEventHeader&  eventHeader() const { return m_eventHeader; }

  const BufferHolder& getMdcBuf() const { return m_bufMdcDigi; }
  const BufferHolder& getTofBuf() const { return m_bufTofDigi; }
  const BufferHolder& getEmcBuf() const { return m_bufEmcDigi; }
  const BufferHolder& getMucBuf() const { return m_bufMucDigi; }
  const BufferHolder& getZddBuf() const { return m_bufZddDigi; }

  const BufferHolder& getGTDBuf() const { return m_bufTrigGTD; }
  const BufferHolder& getHltBuf() const { return m_bufHltRaw; }

  const BufferHolder& getMcParBuf() const { return m_bufMcPar; }

  // should be replaced in future
  const int_vector& getMdcTruth() const { return m_ReMdcTruth; }
  const int_vector& getEmcTruth() const { return m_ReEmcTruth; }
  const int_vector& getTofTruth() const { return m_ReTofTruth; }
  const int_vector& getMucTruth() const { return m_ReMucTruth; }
  const int_vector& getZddTruth() const { return m_ReZddTruth; }
private:
  RawEventHeader   m_eventHeader; 

  // real data
  BufferHolder m_bufMdcDigi;
  BufferHolder m_bufTofDigi;
  BufferHolder m_bufEmcDigi;
  BufferHolder m_bufMucDigi;
  BufferHolder m_bufZddDigi;
  BufferHolder m_bufTrigGTD;
  BufferHolder m_bufHltRaw;

  // mc data
  BufferHolder m_bufMcPar;

  // should be replaced in future
  int_vector    m_ReMdcTruth;
  int_vector    m_ReEmcTruth;
  int_vector    m_ReTofTruth;
  int_vector    m_ReMucTruth;
  int_vector    m_ReZddTruth;

};

#endif // _RAWEVENT_h_
