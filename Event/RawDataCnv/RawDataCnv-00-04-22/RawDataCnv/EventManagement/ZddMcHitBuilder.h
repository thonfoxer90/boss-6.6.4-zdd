#ifndef ZDD_MCHIT_BUILDER_H
#define ZDD_MCHIT_BUILDER_H

#include "RawDataCnv/EventManagement/Builder.h"
#include "McTruth/ZddMcHit.h" 
#include "RawDataCnv/EventManagement/McTruthDataStructure.h"

using namespace std;
using Event::ZddMcHit;
using Event::ZddMcHitCol;

class ZddMcHitBuilder: public Builder
{                                           

public:
   ZddMcHitBuilder ();
   virtual ~ZddMcHitBuilder(){};
   virtual StatusCode  initialize(string &initFile);
   virtual uint32_t getTEID(uint32_t teid);
   virtual uint32_t getREID(uint32_t reid);
   
   virtual void unPack(vector<uint32_t>::const_iterator&,
                       vector<uint32_t>::const_iterator&,
		       ZddTruth_t&);
   virtual StatusCode pack(ZddMcHitCol* zddMcHitCol, WriteRawEvent*& re);

private:
   
  void makeZddTruth(ZddMcHitCol::const_iterator& pZddMcHit, ZddTruth_t& mt);

  TE2REMAP m_te2reMap;
  //string initFile;
  vector <int> re2te;
  PropertyMgr m_propMgr;
 
  uint32_t m_trackIDIndex, m_partIDIndex, m_detectorIDIndex, m_crystalNoIndex;
  uint32_t m_trackIDMask, m_partIDMask, m_detectorIDMask, m_crystalNoMask;

};
#endif
