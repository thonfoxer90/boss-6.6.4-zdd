#ifndef ZDD_CONVERTER_H
#define ZDD_CONVERTER_H

#include "GaudiKernel/StatusCode.h"
#include "RawDataCnv/EventManagement/ZddBuilder.h"
#include "RawDataCnv/Util/BufferHolder.h"
#include "ZddRawEvent/ZddDigi.h"

class ZddConverter {

   public:

      // static interfaces
      static ZddConverter* instance();
      static void destroy();

      // public interfaces
      StatusCode convert(const BufferHolder& src, ZddDigiCol* des);
      StatusCode convert(ZddDigiCol* src, WriteRawEvent*& des);


   private:

      // private methods
      ZddConverter();  //get its intance via the static interface!
      ~ZddConverter();

      // private data members
      ZddBuilder m_builder;

      // static data members
      static ZddConverter* s_instance;
};

#endif
