//=========================================================================
//       ClassID_temp.h
//=========================================================================
//
//  Description: A temporary file that defines some type definitions 
//               specific to this package and that will eventually be 
//               added to the standard Athena file "GaudiKernel/ClassID.h"
//               once this package is placed in the CVS repository.
//
//-------------------------------------------------------------------------

#ifndef CLASSID_TEMP_H
#define CLASSID_TEMP_H

#include "GaudiKernel/ClassID.h"

unsigned const long PACKEDRAWEVENT_StorageType = 0xAB;

#endif  // CLASSID_TEMP_H
