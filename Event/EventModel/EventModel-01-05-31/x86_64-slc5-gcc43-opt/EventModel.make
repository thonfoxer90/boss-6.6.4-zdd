#-- start of make_header -----------------

#====================================
#  Library EventModel
#
<<<<<<< HEAD
#   Generated Sat May 19 14:51:15 2018  by bgarillo
=======
#   Generated Sun May 20 13:41:12 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_EventModel_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_EventModel_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_EventModel

EventModel_tag = $(tag)

#cmt_local_tagfile_EventModel = $(EventModel_tag)_EventModel.make
cmt_local_tagfile_EventModel = $(bin)$(EventModel_tag)_EventModel.make

else

tags      = $(tag),$(CMTEXTRATAGS)

EventModel_tag = $(tag)

#cmt_local_tagfile_EventModel = $(EventModel_tag).make
cmt_local_tagfile_EventModel = $(bin)$(EventModel_tag).make

endif

include $(cmt_local_tagfile_EventModel)
#-include $(cmt_local_tagfile_EventModel)

ifdef cmt_EventModel_has_target_tag

cmt_final_setup_EventModel = $(bin)setup_EventModel.make
#cmt_final_setup_EventModel = $(bin)EventModel_EventModelsetup.make
cmt_local_EventModel_makefile = $(bin)EventModel.make

else

cmt_final_setup_EventModel = $(bin)setup.make
#cmt_final_setup_EventModel = $(bin)EventModelsetup.make
cmt_local_EventModel_makefile = $(bin)EventModel.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)EventModelsetup.make

#EventModel :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'EventModel'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = EventModel/
#EventModel::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

EventModellibname   = $(bin)$(library_prefix)EventModel$(library_suffix)
EventModellib       = $(EventModellibname).a
EventModelstamp     = $(bin)EventModel.stamp
EventModelshstamp   = $(bin)EventModel.shstamp

EventModel :: dirs  EventModelLIB
	$(echo) "EventModel ok"

#-- end of libary_header ----------------

EventModelLIB :: $(EventModellib) $(EventModelshstamp)
	@/bin/echo "------> EventModel : library ok"

$(EventModellib) :: $(bin)EventModel.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(EventModellib) $?
	$(lib_silent) $(ranlib) $(EventModellib)
	$(lib_silent) cat /dev/null >$(EventModelstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(EventModellibname).$(shlibsuffix) :: $(EventModellib) $(EventModelstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" EventModel $(EventModel_shlibflags)

$(EventModelshstamp) :: $(EventModellibname).$(shlibsuffix)
	@if test -f $(EventModellibname).$(shlibsuffix) ; then cat /dev/null >$(EventModelshstamp) ; fi

EventModelclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)EventModel.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
EventModelinstallname = $(library_prefix)EventModel$(library_suffix).$(shlibsuffix)

EventModel :: EventModelinstall

install :: EventModelinstall

EventModelinstall :: $(install_dir)/$(EventModelinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(EventModelinstallname) :: $(bin)$(EventModelinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(EventModelinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(EventModelinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(EventModelinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(EventModelinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(EventModelinstallname) $(install_dir)/$(EventModelinstallname); \
	      echo `pwd`/$(EventModelinstallname) >$(install_dir)/$(EventModelinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(EventModelinstallname), no installation directory specified"; \
	  fi; \
	fi

EventModelclean :: EventModeluninstall

uninstall :: EventModeluninstall

EventModeluninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(EventModelinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(EventModelinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(EventModelinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(EventModelinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),EventModelclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)EventModel_dependencies.make :: dirs

ifndef QUICK
$(bin)EventModel_dependencies.make : $(src)EventModel.cxx $(use_requirements) $(cmt_final_setup_EventModel)
	$(echo) "(EventModel.make) Rebuilding $@"; \
	  $(build_dependencies) EventModel -all_sources -out=$@ $(src)EventModel.cxx
endif

#$(EventModel_dependencies)

-include $(bin)EventModel_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),EventModelclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EventModel.d

$(bin)$(binobj)EventModel.d : $(use_requirements) $(cmt_final_setup_EventModel)

$(bin)$(binobj)EventModel.d : $(src)EventModel.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/EventModel.dep $(use_pp_cppflags) $(EventModel_pp_cppflags) $(lib_EventModel_pp_cppflags) $(EventModel_pp_cppflags) $(use_cppflags) $(EventModel_cppflags) $(lib_EventModel_cppflags) $(EventModel_cppflags) $(EventModel_cxx_cppflags)  $(src)EventModel.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/EventModel.o $(src)EventModel.cxx $(@D)/EventModel.dep
endif
endif

$(bin)$(binobj)EventModel.o : $(src)EventModel.cxx
else
$(bin)EventModel_dependencies.make : $(EventModel_cxx_dependencies)

$(bin)$(binobj)EventModel.o : $(EventModel_cxx_dependencies)
endif
	$(cpp_echo) $(src)EventModel.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(EventModel_pp_cppflags) $(lib_EventModel_pp_cppflags) $(EventModel_pp_cppflags) $(use_cppflags) $(EventModel_cppflags) $(lib_EventModel_cppflags) $(EventModel_cppflags) $(EventModel_cxx_cppflags)  $(src)EventModel.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: EventModelclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(EventModel.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(EventModel.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(EventModel.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(EventModel.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_EventModel)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(EventModel.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(EventModel.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(EventModel.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

EventModelclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library EventModel
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)EventModel$(library_suffix).a $(library_prefix)EventModel$(library_suffix).s? EventModel.stamp EventModel.shstamp
#-- end of cleanup_library ---------------
