# echo "Setting EventModel EventModel-01-05-31 in /home/bgarillo/boss-6.6.4-ZDD/Event"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cluster/him/bes3/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=EventModel -version=EventModel-01-05-31 -path=/home/bgarillo/boss-6.6.4-ZDD/Event  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

