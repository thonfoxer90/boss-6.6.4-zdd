----------> uses
# use GaudiKernel *  (no_version_directory)
#   use GaudiPolicy v*  (no_version_directory)
#     use LCG_Settings *  (no_version_directory)
#       use LCG_SettingsCompat *  (no_version_directory)
#     use Python * LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.5.4p2)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#     use tcmalloc v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=1.4)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#   use ROOT v* LCG_Interfaces (no_version_directory) (native_version=5.24.00b)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
#     use GCCXML v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=0.9.0_20090601)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#     use Python v* LCG_Interfaces (no_auto_imports) (no_version_directory)
#   use Reflex v* LCG_Interfaces (no_version_directory)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
#     use ROOT v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=5.24.00b)
#   use Boost v* LCG_Interfaces (no_version_directory) (native_version=1.39.0_python2.5)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
#     use Python v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.5.4p2)
#   use CppUnit v* LCG_Interfaces (private) (no_auto_imports) (no_version_directory) (native_version=1.12.1_p1)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
# use Gaudi *  (no_version_directory)
#   use GaudiSys v*  (no_version_directory)
#     use GaudiKernel v*  (no_version_directory)
#     use GaudiSvc v*  (no_auto_imports) (no_version_directory)
#       use GaudiKernel v*  (no_version_directory)
#       use Reflex v* LCG_Interfaces (no_auto_imports) (no_version_directory)
#       use CLHEP v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.0.4.5)
#         use LCG_Configuration v*  (no_version_directory)
#         use LCG_Settings v*  (no_version_directory)
#       use AIDA v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=3.2.1)
#         use LCG_Configuration v*  (no_version_directory)
#         use LCG_Settings v*  (no_version_directory)
#       use Boost v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=1.39.0_python2.5)
#       use ROOT v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=5.24.00b)
#       use PCRE v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=4.4)
#         use LCG_Configuration v*  (no_version_directory)
#         use LCG_Settings v*  (no_version_directory)
#     use GaudiAud v*  (no_auto_imports) (no_version_directory)
#       use GaudiKernel v*  (no_version_directory)
#       use GaudiSvc v*  (private) (no_auto_imports) (no_version_directory)
#     use GaudiAlg v*  (no_auto_imports) (no_version_directory)
#       use GaudiKernel v*  (no_version_directory)
#       use GaudiUtils v*  (no_version_directory)
#         use GaudiKernel v*  (no_version_directory)
#         use ROOT v* LCG_Interfaces (no_version_directory) (native_version=5.24.00b)
#         use AIDA v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=3.2.1)
#         use Boost v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=1.39.0_python2.5)
#         use uuid v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=1.38)
#           use LCG_Configuration v*  (no_version_directory)
#           use LCG_Settings v*  (no_version_directory)
#         use Reflex v* LCG_Interfaces (no_auto_imports) (no_version_directory)
#         use XercesC v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.8.0)
#           use LCG_Configuration v*  (no_version_directory)
#           use LCG_Settings v*  (no_version_directory)
#         use GaudiSvc v*  (private) (no_auto_imports) (no_version_directory)
#       use ROOT v* LCG_Interfaces (no_version_directory) (native_version=5.24.00b)
#       use AIDA v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=3.2.1)
#       use Boost v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=1.39.0_python2.5)
#       use GaudiSvc v*  (private) (no_auto_imports) (no_version_directory)
#     use GaudiPython v*  (no_auto_imports) (no_version_directory)
#       use GaudiKernel v*  (no_version_directory)
#       use GaudiAlg v*  (no_version_directory)
#       use GaudiUtils v*  (no_version_directory)
#       use AIDA v* LCG_Interfaces (no_version_directory) (native_version=3.2.1)
#       use CLHEP v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.0.4.5)
#       use Python v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.5.4p2)
#       use Reflex v* LCG_Interfaces (no_auto_imports) (no_version_directory)
#       use GaudiSvc v*  (private) (no_auto_imports) (no_version_directory)
#       use GaudiSvc v*  (private) (no_auto_imports) (no_version_directory)
# use ReconEvent ReconEvent-* Event
#   use BesPolicy BesPolicy-* 
#     use BesCxxPolicy BesCxxPolicy-* 
#       use GaudiPolicy v*  (no_version_directory)
#     use BesFortranPolicy BesFortranPolicy-* 
#       use LCG_Settings v*  (no_version_directory)
#   use GaudiInterface GaudiInterface-* External
#     use GaudiKernel *  (no_version_directory)
#     use GaudiSvc *  (no_version_directory)
#   use BesCLHEP BesCLHEP-* External
#     use CLHEP v* LCG_Interfaces (no_version_directory) (native_version=2.0.4.5)
#     use HepMC * LCG_Interfaces (no_version_directory) (native_version=2.03.11)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#     use HepPDT * LCG_Interfaces (no_version_directory) (native_version=2.05.04)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#     use BesExternalArea BesExternalArea-* External
#   use EventModel EventModel-* Event
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-* External
#   use ExtEvent ExtEvent-* Event
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use BesCLHEP BesCLHEP-* External
#     use EventModel EventModel-* Event
#     use DstEvent DstEvent-* Event
#       use BesPolicy BesPolicy-* 
#       use GaudiInterface GaudiInterface-* External
#       use BesCLHEP BesCLHEP-* External
#       use EventModel EventModel-* Event
# use RootCnvSvc RootCnvSvc-* Event
#   use BesCLHEP BesCLHEP-* External
#   use BesPolicy BesPolicy-01-* 
#   use DataInfoSvc DataInfoSvc-* Control
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-01-* External
#   use BesROOT BesROOT-00-* External
#     use CASTOR v* LCG_Interfaces (no_version_directory) (native_version=2.1.8-10)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#     use ROOT v* LCG_Interfaces (no_version_directory) (native_version=5.24.00b)
#   use GaudiInterface GaudiInterface-* External
#   use Identifier Identifier-* DetectorDescription
#     use BesPolicy BesPolicy-* 
#   use RawEvent RawEvent-* Event
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use Identifier Identifier-* DetectorDescription
#     use EventModel EventModel-* Event
#   use RootEventData RootEventData-* Event
#     use RootPolicy RootPolicy-* 
#       use BesPolicy BesPolicy-* 
#       use BesROOT BesROOT-00-* External
#     use BesROOT BesROOT-* External
#     use MucRecEvent MucRecEvent-* Muc
#       use BesPolicy BesPolicy-01-* 
#       use GaudiInterface GaudiInterface-01-* External
#       use Identifier Identifier-* DetectorDescription
#       use EventModel EventModel-* Event
#       use ExtEvent ExtEvent-* Event
#       use MucGeomSvc MucGeomSvc-* Muc
#         use BesPolicy BesPolicy-01-* 
#         use GaudiInterface GaudiInterface-* External
#         use Identifier Identifier-* DetectorDescription
#         use ROOTGeo ROOTGeo-* DetectorDescription
#           use BesPolicy BesPolicy-01-* 
#           use GaudiInterface GaudiInterface-* External
#           use BesCLHEP BesCLHEP-* External
#           use BesROOT BesROOT-* External
#           use XercesC * LCG_Interfaces (no_version_directory)
#           use GdmlToRoot GdmlToRoot-* External
#             use BesExternalArea BesExternalArea-* External
#             use BesROOT BesROOT-* External
#           use GdmlManagement GdmlManagement-* DetectorDescription
#             use BesExternalArea BesExternalArea-* External
#         use BesCLHEP BesCLHEP-* External
#         use BesROOT BesROOT-* External
#         use XercesC * LCG_Interfaces (no_version_directory) (native_version=2.8.0)
#         use GdmlToRoot GdmlToRoot-* External
#         use G4Geo G4Geo-* DetectorDescription
#           use BesPolicy BesPolicy-01-* 
#           use GaudiInterface GaudiInterface-* External
#           use BesCLHEP BesCLHEP-* External
#           use BesGeant4 BesGeant4-* External
#             use BesExternalArea BesExternalArea-00-* External
#             use BesCLHEP BesCLHEP-00-* External
#           use XercesC * LCG_Interfaces (no_version_directory) (native_version=2.8.0)
#           use GdmlToG4 GdmlToG4-* External
#             use BesExternalArea BesExternalArea-* External
#             use BesGeant4 BesGeant4-* External
#             use XercesC * LCG_Interfaces (no_version_directory) (native_version=2.8.0)
#           use GdmlManagement GdmlManagement-* DetectorDescription
#           use Identifier Identifier-* DetectorDescription
#       use DstEvent DstEvent-* Event
#       use BesCLHEP BesCLHEP-* External (private)
#     use Identifier Identifier-* DetectorDescription
#   use MdcRawEvent MdcRawEvent-* Mdc
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use RawEvent RawEvent-* Event
#     use EventModel EventModel-* Event
#   use EventModel EventModel-* Event
#   use EmcRawEvent EmcRawEvent-* Emc
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use RawEvent RawEvent-* Event
#     use EventModel EventModel-* Event
#     use Identifier Identifier-* DetectorDescription
#     use EmcWaveform EmcWaveform-* Emc
#       use BesPolicy BesPolicy-* 
#       use GaudiInterface GaudiInterface-* External
#   use TofRawEvent TofRawEvent-* Tof
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use RawEvent RawEvent-* Event
#     use EventModel EventModel-* Event
#   use TofRecEvent TofRecEvent-* Tof
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use Identifier Identifier-* DetectorDescription
#     use EventModel EventModel-* Event
#     use DstEvent * Event
#   use MucRawEvent MucRawEvent-* Muc
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use RawEvent RawEvent-* Event
#     use EventModel EventModel-* Event
#   use ZddRawEvent ZddRawEvent-* Zdd
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use RawEvent RawEvent-* Event
#     use EventModel EventModel-* Event
#   use MucRecEvent MucRecEvent-* Muc
#   use EmcRecEventModel EmcRecEventModel-* Emc
#     use BesPolicy BesPolicy-* 
#     use Identifier Identifier-* DetectorDescription
#     use BesCLHEP BesCLHEP-* External
#     use EventModel EventModel-* Event
#     use DstEvent DstEvent-* Event
#     use EmcRecGeoSvc EmcRecGeoSvc-* Emc
#       use BesPolicy BesPolicy-* 
#       use Identifier Identifier-* DetectorDescription
#       use ROOTGeo ROOTGeo-* DetectorDescription
#       use BesCLHEP BesCLHEP-* External
#       use GaudiInterface GaudiInterface-* External
#   use MdcRecEvent MdcRecEvent-* Mdc
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use MdcGeomSvc MdcGeomSvc-* Mdc
#       use BesPolicy BesPolicy-01-* 
#       use GaudiInterface GaudiInterface-* External
#       use calibUtil * Calibration
#         use GaudiInterface GaudiInterface-01-* External
#         use facilities * Calibration
#           use BesPolicy BesPolicy-* 
#         use xmlBase * Calibration
#           use BesPolicy * 
#           use XercesC * LCG_Interfaces (no_version_directory) (native_version=2.8.0)
#           use facilities * Calibration
#         use rdbModel * Calibration
#           use BesPolicy * 
#           use facilities * Calibration
#           use xmlBase * Calibration
#           use MYSQL * External
#             use mysql * LCG_Interfaces (no_version_directory) (native_version=5.0.18)
#               use LCG_Configuration v*  (no_version_directory)
#               use LCG_Settings v*  (no_version_directory)
#         use BesROOT BesROOT-00-* External
#         use DatabaseSvc DatabaseSvc-* Database
#           use BesPolicy BesPolicy-* 
#           use GaudiInterface GaudiInterface-* External
#           use mysql * LCG_Interfaces (no_version_directory) (native_version=5.0.18)
#           use sqlite * LCG_Interfaces (no_version_directory) (native_version=3.6.11)
#             use LCG_Configuration v*  (no_version_directory)
#             use LCG_Settings v*  (no_version_directory)
#           use BesROOT * External
#       use CalibData * Calibration
#         use facilities facilities-* Calibration
#         use GaudiInterface * External
#         use BesROOT BesROOT-00-* External
#       use EventModel EventModel-* Event
#       use BesCLHEP BesCLHEP-00-* External (private)
#     use RelTable RelTable-* Event
#       use BesPolicy BesPolicy-01-* 
#       use GaudiInterface GaudiInterface-01-* External
#     use EventModel EventModel-* Event
#     use Identifier Identifier-* DetectorDescription
#     use DstEvent DstEvent-* Event
#     use BesCLHEP BesCLHEP-* External (private)
#   use EvTimeEvent EvTimeEvent-* Event
#     use BesPolicy BesPolicy-01-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use BesCLHEP BesCLHEP-* External
#     use MdcGeomSvc MdcGeomSvc-* Mdc
#     use RelTable RelTable-* Event
#     use EventModel EventModel-* Event
#     use Identifier Identifier-* DetectorDescription
#   use EvtRecEvent EvtRecEvent-* Event
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-* External
#     use BesCLHEP BesCLHEP-* External
#     use EventModel EventModel-* Event
#     use EvTimeEvent EvTimeEvent-* Event
#     use MdcRecEvent MdcRecEvent-* Mdc
#     use TofRecEvent TofRecEvent-* Tof
#     use EmcRecEventModel EmcRecEventModel-* Emc
#     use MucRecEvent MucRecEvent-* Muc
#     use ExtEvent ExtEvent-* Event
#     use DstEvent DstEvent-* Event
#   use EventNavigator EventNavigator-* Event
#     use BesPolicy * 
#     use GaudiInterface * External
#     use McTruth McTruth-* Event
#       use BesPolicy BesPolicy-01-* 
#       use EventModel EventModel-* Event
#       use GaudiInterface GaudiInterface-01-* External
#       use Identifier Identifier-* DetectorDescription
#       use RelTable RelTable-* Event
#       use BesCLHEP BesCLHEP-* External (private)
#     use EmcRecEventModel * Emc
#     use MdcRecEvent * Mdc
#     use MdcRawEvent * Mdc
#     use MucRecEvent * Muc
#     use TofRecEvent * Tof
#   use McTruth McTruth-* Event
#   use ExtEvent ExtEvent-* Event
#   use DstEvent DstEvent-* Event
#   use ReconEvent ReconEvent-* Event
#   use TrigEvent TrigEvent-* Event
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-01-* External
#   use HltEvent HltEvent-* Event
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use RawEvent RawEvent-* Event
#     use EventModel EventModel-* Event
#     use Identifier Identifier-* DetectorDescription
#   use LumiDigi LumiDigi-* Event
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-01-* External
#     use RawEvent RawEvent-* Event
#     use TofRawEvent TofRawEvent-* Tof
# use BesROOT BesROOT-00-* External
# use BesCLHEP BesCLHEP-* External
# use BesPolicy BesPolicy-01-* 
# use GaudiInterface GaudiInterface-01-* External
# use MdcRawEvent MdcRawEvent-* Mdc
# use TofRawEvent TofRawEvent-* Tof
# use EmcRawEvent EmcRawEvent-* Emc
# use ZddRawEvent ZddRawEvent-* Zdd
# use DstEvent DstEvent-* Event
# use EventModel EventModel-* Event
# use McTruth McTruth-* Event
# use Identifier Identifier-* DetectorDescription
# use RootPolicy RootPolicy-* 
# use RootEventData RootEventData-* Event
#
# Selection :
use CMT v1r20p20090520 (/cluster/him/bes3)
use BesExternalArea BesExternalArea-00-00-21 External (/cluster/him/bes3/dist/6.6.4/)
use GdmlManagement GdmlManagement-00-00-31 DetectorDescription (/home/bgarillo/boss-6.6.4-ZDD/)
use LCG_Configuration v1  (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5)
use LCG_SettingsCompat v1  (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5)
use LCG_Settings v1  (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5)
use sqlite v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use mysql v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use MYSQL MYSQL-00-00-09 External (/cluster/him/bes3/dist/6.6.4/)
use CASTOR v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use HepPDT v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use HepMC v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use BesFortranPolicy BesFortranPolicy-00-01-03  (/cluster/him/bes3/dist/6.6.4)
use XercesC v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use uuid v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use PCRE v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use AIDA v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use CLHEP v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use BesCLHEP BesCLHEP-00-00-09 External (/cluster/him/bes3/dist/6.6.4/)
use BesGeant4 BesGeant4-00-00-09 External (/home/bgarillo/boss-6.6.4-ZDD/)
<<<<<<< HEAD
use GdmlToG4 GdmlToG4-00-00-10 External (/cluster/him/bes3/dist/6.6.4/)
use CppUnit v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use GCCXML v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use tcmalloc v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use Python v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use Boost v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use ROOT v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use BesROOT BesROOT-00-00-07 External (/cluster/him/bes3/dist/6.6.4/)
use GdmlToRoot GdmlToRoot-00-00-13 External (/cluster/him/bes3/dist/6.6.4/)
use Reflex v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use GaudiPolicy v10r4  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
use BesCxxPolicy BesCxxPolicy-00-01-01  (/cluster/him/bes3/dist/6.6.4)
use BesPolicy BesPolicy-01-05-03  (/cluster/him/bes3/dist/6.6.4)
use facilities facilities-00-00-03 Calibration (/cluster/him/bes3/dist/6.6.4/)
use xmlBase xmlBase-00-00-02 Calibration (/cluster/him/bes3/dist/6.6.4/)
use rdbModel rdbModel-00-01-00 Calibration (/cluster/him/bes3/dist/6.6.4/)
use RootPolicy RootPolicy-00-01-02  (/cluster/him/bes3/dist/6.6.4)
use Identifier Identifier-00-02-13 DetectorDescription (/home/bgarillo/boss-6.6.4-ZDD/)
use GaudiKernel v27r6  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
use GaudiSvc v18r6  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
use GaudiInterface GaudiInterface-01-03-07 External (/cluster/him/bes3/dist/6.6.4/)
use TrigEvent TrigEvent-00-01-01 Event (/cluster/him/bes3/dist/6.6.4/)
use RelTable RelTable-00-00-02 Event (/cluster/him/bes3/dist/6.6.4/)
use CalibData CalibData-00-01-09 Calibration (/cluster/him/bes3/dist/6.6.4/)
use DatabaseSvc DatabaseSvc-00-00-24-p01 Database (/cluster/him/bes3/dist/6.6.4/)
use calibUtil calibUtil-00-00-38 Calibration (/cluster/him/bes3/dist/6.6.4/)
use EmcWaveform EmcWaveform-00-00-03 Emc (/cluster/him/bes3/dist/6.6.4/)
=======
use GdmlToG4 GdmlToG4-00-00-10 External (/cluster/bes3/dist/6.6.4.p01/)
use CppUnit v1 LCG_Interfaces (/cluster/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use GCCXML v1 LCG_Interfaces (/cluster/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use tcmalloc v1 LCG_Interfaces (/cluster/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use Python v1 LCG_Interfaces (/cluster/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/) (no_auto_imports)
use Boost v1 LCG_Interfaces (/cluster/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use ROOT v1 LCG_Interfaces (/cluster/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use BesROOT BesROOT-00-00-07 External (/cluster/bes3/dist/6.6.4.p01/)
use GdmlToRoot GdmlToRoot-00-00-13 External (/cluster/bes3/dist/6.6.4.p01/)
use Reflex v1 LCG_Interfaces (/cluster/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use GaudiPolicy v10r4  (/cluster/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
use BesCxxPolicy BesCxxPolicy-00-01-01  (/cluster/bes3/dist/6.6.4.p01)
use BesPolicy BesPolicy-01-05-03  (/cluster/bes3/dist/6.6.4.p01)
use facilities facilities-00-00-03 Calibration (/cluster/bes3/dist/6.6.4.p01/)
use xmlBase xmlBase-00-00-02 Calibration (/cluster/bes3/dist/6.6.4.p01/)
use rdbModel rdbModel-00-01-00 Calibration (/cluster/bes3/dist/6.6.4.p01/)
use RootPolicy RootPolicy-00-01-02  (/cluster/bes3/dist/6.6.4.p01)
use Identifier Identifier-00-02-13 DetectorDescription (/home/bgarillo/boss-6.6.4-ZDD/)
use GaudiKernel v27r6  (/cluster/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
use GaudiSvc v18r6  (/cluster/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
use GaudiInterface GaudiInterface-01-03-07 External (/cluster/bes3/dist/6.6.4.p01/)
use TrigEvent TrigEvent-00-01-01 Event (/cluster/bes3/dist/6.6.4.p01/)
use RelTable RelTable-00-00-02 Event (/cluster/bes3/dist/6.6.4.p01/)
use CalibData CalibData-00-01-09 Calibration (/cluster/bes3/dist/6.6.4.p01/)
use DatabaseSvc DatabaseSvc-00-00-22 Database (/cluster/bes3/dist/6.6.4.p01/)
use calibUtil calibUtil-00-00-38 Calibration (/cluster/bes3/dist/6.6.4.p01/)
use EmcWaveform EmcWaveform-00-00-03 Emc (/cluster/bes3/dist/6.6.4.p01/)
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
use G4Geo G4Geo-00-00-10 DetectorDescription (/home/bgarillo/boss-6.6.4-ZDD/)
use ROOTGeo ROOTGeo-00-00-15 DetectorDescription (/home/bgarillo/boss-6.6.4-ZDD/)
use EmcRecGeoSvc EmcRecGeoSvc-01-01-07 Emc (/cluster/him/bes3/dist/6.6.4/)
use MucGeomSvc MucGeomSvc-00-02-25 Muc (/cluster/him/bes3/dist/6.6.4/)
use DataInfoSvc DataInfoSvc-00-00-03 Control (/cluster/him/bes3/dist/6.6.4/)
use EventModel EventModel-01-05-31 Event (/home/bgarillo/boss-6.6.4-ZDD/)
use McTruth McTruth-00-02-19 Event (/home/bgarillo/boss-6.6.4-ZDD/)
use MdcGeomSvc MdcGeomSvc-00-01-37 Mdc (/cluster/him/bes3/dist/6.6.4/)
use EvTimeEvent EvTimeEvent-00-00-08 Event (/cluster/him/bes3/dist/6.6.4/)
use RawEvent RawEvent-00-03-19 Event (/home/bgarillo/boss-6.6.4-ZDD/)
use HltEvent HltEvent-00-02-06 Event (/cluster/him/bes3/dist/6.6.4/)
use ZddRawEvent ZddRawEvent-00-02-02 Zdd (/home/bgarillo/boss-6.6.4-ZDD/)
use MucRawEvent MucRawEvent-00-02-02 Muc (/cluster/him/bes3/dist/6.6.4/)
use TofRawEvent TofRawEvent-00-02-07 Tof (/cluster/him/bes3/dist/6.6.4/)
use LumiDigi LumiDigi-00-00-02 Event (/cluster/him/bes3/dist/6.6.4/)
use EmcRawEvent EmcRawEvent-00-02-05 Emc (/cluster/him/bes3/dist/6.6.4/)
use MdcRawEvent MdcRawEvent-00-03-08 Mdc (/cluster/him/bes3/dist/6.6.4/)
use DstEvent DstEvent-00-02-50-p01 Event (/cluster/him/bes3/dist/6.6.4/)
use MdcRecEvent MdcRecEvent-00-05-14 Mdc (/cluster/him/bes3/dist/6.6.4/)
use EmcRecEventModel EmcRecEventModel-01-01-18 Emc (/cluster/him/bes3/dist/6.6.4/)
use TofRecEvent TofRecEvent-00-02-14 Tof (/cluster/him/bes3/dist/6.6.4/)
use ExtEvent ExtEvent-00-00-13 Event (/cluster/him/bes3/dist/6.6.4/)
use MucRecEvent MucRecEvent-00-02-50-p01 Muc (/cluster/him/bes3/dist/6.6.4/)
use EventNavigator EventNavigator-00-01-03 Event (/cluster/him/bes3/dist/6.6.4/)
use EvtRecEvent EvtRecEvent-00-01-04 Event (/cluster/him/bes3/dist/6.6.4/)
use RootEventData RootEventData-00-03-75 Event (/home/bgarillo/boss-6.6.4-ZDD/)
use ReconEvent ReconEvent-00-00-04 Event (/cluster/him/bes3/dist/6.6.4/)
use RootCnvSvc RootCnvSvc-02-01-00 Event (/home/bgarillo/boss-6.6.4-ZDD/)
use GaudiUtils v3r5  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5) (no_auto_imports)
use GaudiAlg v12r4  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5) (no_auto_imports)
use GaudiPython v10r6  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5) (no_auto_imports)
use GaudiAud v9r1  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5) (no_auto_imports)
use GaudiSys v21r6  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
use Gaudi v21r6  (/cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5)
----------> tags
CMTv1 (from CMTVERSION)
CMTr20 (from CMTVERSION)
CMTp20090520 (from CMTVERSION)
Linux (from uname) package BesPolicy implies [Unix host-linux]
x86_64-slc5-gcc43-opt (from CMTCONFIG) package LCG_Settings implies [Linux slc5-amd64 gcc43 optimized target-linux target-x86_64 target-slc5 target-gcc43 target-opt]
LOCAL (from CMTSITE)
bgarillo_no_config (from PROJECT) excludes [bgarillo_config]
bgarillo_root (from PROJECT) excludes [bgarillo_no_root]
bgarillo_cleanup (from PROJECT) excludes [bgarillo_no_cleanup]
bgarillo_no_prototypes (from PROJECT) excludes [bgarillo_prototypes]
bgarillo_with_installarea (from PROJECT) excludes [bgarillo_without_installarea]
bgarillo_with_version_directory (from PROJECT) excludes [bgarillo_without_version_directory]
bgarillo (from PROJECT)
BOSS_no_config (from PROJECT) excludes [BOSS_config]
BOSS_root (from PROJECT) excludes [BOSS_no_root]
BOSS_cleanup (from PROJECT) excludes [BOSS_no_cleanup]
BOSS_no_prototypes (from PROJECT) excludes [BOSS_prototypes]
BOSS_with_installarea (from PROJECT) excludes [BOSS_without_installarea]
BOSS_with_version_directory (from PROJECT) excludes [BOSS_without_version_directory]
GAUDI_no_config (from PROJECT) excludes [GAUDI_config]
GAUDI_root (from PROJECT) excludes [GAUDI_no_root]
GAUDI_cleanup (from PROJECT) excludes [GAUDI_no_cleanup]
GAUDI_prototypes (from PROJECT) excludes [GAUDI_no_prototypes]
GAUDI_with_installarea (from PROJECT) excludes [GAUDI_without_installarea]
GAUDI_without_version_directory (from PROJECT) excludes [GAUDI_with_version_directory]
LCGCMT_no_config (from PROJECT) excludes [LCGCMT_config]
LCGCMT_no_root (from PROJECT) excludes [LCGCMT_root]
LCGCMT_cleanup (from PROJECT) excludes [LCGCMT_no_cleanup]
LCGCMT_prototypes (from PROJECT) excludes [LCGCMT_no_prototypes]
LCGCMT_without_installarea (from PROJECT) excludes [LCGCMT_with_installarea]
LCGCMT_with_version_directory (from PROJECT) excludes [LCGCMT_without_version_directory]
x86_64 (from package CMT) package LCG_Settings implies [host-x86_64]
slc73 (from package CMT)
gcc432 (from package CMT) package LCG_Settings implies [gcc43 host-gcc43]
Unix (from package CMT) package LCG_Settings implies [host-unix] package LCG_Settings excludes [WIN32 Win32]
gcc43 (from package LCG_SettingsCompat)
amd64 (from package LCG_SettingsCompat)
slc5 (from package LCG_SettingsCompat)
slc5-amd64 (from package LCG_SettingsCompat) package LCG_SettingsCompat implies [slc5 amd64]
optimized (from package LCG_SettingsCompat) package BesPolicy implies [opt]
target-unix (from package LCG_Settings)
host-x86_64 (from package LCG_Settings)
host-gcc4 (from package LCG_Settings) package LCG_Settings implies [host-gcc]
host-gcc43 (from package LCG_Settings) package LCG_Settings implies [host-gcc4]
host-gcc (from package LCG_Settings)
host-linux (from package LCG_Settings)
host-unix (from package LCG_Settings)
target-linux (from package LCG_Settings) package LCG_Settings implies [target-unix]
target-slc5 (from package LCG_Settings) package LCG_Settings implies [target-slc]
target-opt (from package LCG_Settings)
target-gcc43 (from package LCG_Settings) package LCG_Settings implies [target-gcc4]
target-x86_64 (from package LCG_Settings)
target-slc (from package LCG_Settings)
target-gcc4 (from package LCG_Settings) package LCG_Settings implies [target-gcc]
target-gcc (from package LCG_Settings)
ROOT_GE_5_15 (from package LCG_Configuration)
ROOT_GE_5_19 (from package LCG_Configuration)
opt (from package BesCxxPolicy) package BesPolicy implies [optimized]
HasAthenaRunTime (from package BesPolicy)
ROOTBasicLibs (from package BesROOT)
----------> CMTPATH
# Add path /home/bgarillo/boss-6.6.4-ZDD from initialization
# Add path /cluster/him/bes3/dist/6.6.4 from initialization
# Add path /cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5 from initialization
# Add path /cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5 from initialization
