#include "GaudiKernel/DeclareFactoryEntries.h"
#include "RootIO/RootIoAlg.h"

DECLARE_ALGORITHM_FACTORY( RootIoAlg )

DECLARE_FACTORY_ENTRIES( RootIO) {
  DECLARE_ALGORITHM(RootIoAlg);
}

