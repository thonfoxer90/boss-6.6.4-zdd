
ApplicationMgr.DLLs += {"RootCnvSvc"};

EventPersistencySvc.CnvServices += {"RootCnvSvc"};

// items to be written out and configuration of OutStream

ApplicationMgr.OutStream = {"RootWriter"};
RootWriter.Output  = " TYP='ROOT'";

RootWriter.ItemList = { "/Event/EventHeader#1",
                        "/Event/Digi/MdcDigiCol#1",
                        "/Event/Digi/TofDigiCol#1",
                        "/Event/Digi/EmcDigiCol#1",
                        "/Event/Digi/MucDigiCol#1",
                        "/Event/Digi/LumiDigiCol#1",
                        "/Event/Recon#99",
                        "/Event/EvtRec#99",
                        "/Event/Trig/TrigData#1",
                        "/Event/Hlt#99"
};


