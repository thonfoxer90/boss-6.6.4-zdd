#-- start of make_header -----------------

#====================================
#  Library McTruth
#
<<<<<<< HEAD
#   Generated Sat May 19 14:51:19 2018  by bgarillo
=======
#   Generated Sun May 20 13:41:24 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_McTruth_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_McTruth_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_McTruth

McTruth_tag = $(tag)

#cmt_local_tagfile_McTruth = $(McTruth_tag)_McTruth.make
cmt_local_tagfile_McTruth = $(bin)$(McTruth_tag)_McTruth.make

else

tags      = $(tag),$(CMTEXTRATAGS)

McTruth_tag = $(tag)

#cmt_local_tagfile_McTruth = $(McTruth_tag).make
cmt_local_tagfile_McTruth = $(bin)$(McTruth_tag).make

endif

include $(cmt_local_tagfile_McTruth)
#-include $(cmt_local_tagfile_McTruth)

ifdef cmt_McTruth_has_target_tag

cmt_final_setup_McTruth = $(bin)setup_McTruth.make
#cmt_final_setup_McTruth = $(bin)McTruth_McTruthsetup.make
cmt_local_McTruth_makefile = $(bin)McTruth.make

else

cmt_final_setup_McTruth = $(bin)setup.make
#cmt_final_setup_McTruth = $(bin)McTruthsetup.make
cmt_local_McTruth_makefile = $(bin)McTruth.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)McTruthsetup.make

#McTruth :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'McTruth'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = McTruth/
#McTruth::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

McTruthlibname   = $(bin)$(library_prefix)McTruth$(library_suffix)
McTruthlib       = $(McTruthlibname).a
McTruthstamp     = $(bin)McTruth.stamp
McTruthshstamp   = $(bin)McTruth.shstamp

McTruth :: dirs  McTruthLIB
	$(echo) "McTruth ok"

#-- end of libary_header ----------------

McTruthLIB :: $(McTruthlib) $(McTruthshstamp)
	@/bin/echo "------> McTruth : library ok"

$(McTruthlib) :: $(bin)McParticle.o $(bin)MdcMcHit.o $(bin)TofMcHit.o $(bin)EmcMcHit.o $(bin)MucMcHit.o $(bin)ZddMcHit.o $(bin)McPrimaryParticle.o $(bin)DecayMode.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(McTruthlib) $?
	$(lib_silent) $(ranlib) $(McTruthlib)
	$(lib_silent) cat /dev/null >$(McTruthstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(McTruthlibname).$(shlibsuffix) :: $(McTruthlib) $(McTruthstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" McTruth $(McTruth_shlibflags)

$(McTruthshstamp) :: $(McTruthlibname).$(shlibsuffix)
	@if test -f $(McTruthlibname).$(shlibsuffix) ; then cat /dev/null >$(McTruthshstamp) ; fi

McTruthclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)McParticle.o $(bin)MdcMcHit.o $(bin)TofMcHit.o $(bin)EmcMcHit.o $(bin)MucMcHit.o $(bin)ZddMcHit.o $(bin)McPrimaryParticle.o $(bin)DecayMode.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
McTruthinstallname = $(library_prefix)McTruth$(library_suffix).$(shlibsuffix)

McTruth :: McTruthinstall

install :: McTruthinstall

McTruthinstall :: $(install_dir)/$(McTruthinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(McTruthinstallname) :: $(bin)$(McTruthinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(McTruthinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(McTruthinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(McTruthinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(McTruthinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(McTruthinstallname) $(install_dir)/$(McTruthinstallname); \
	      echo `pwd`/$(McTruthinstallname) >$(install_dir)/$(McTruthinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(McTruthinstallname), no installation directory specified"; \
	  fi; \
	fi

McTruthclean :: McTruthuninstall

uninstall :: McTruthuninstall

McTruthuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(McTruthinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(McTruthinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(McTruthinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(McTruthinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),McTruthclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)McTruth_dependencies.make :: dirs

ifndef QUICK
$(bin)McTruth_dependencies.make : $(src)McParticle.cxx $(src)MdcMcHit.cxx $(src)TofMcHit.cxx $(src)EmcMcHit.cxx $(src)MucMcHit.cxx $(src)ZddMcHit.cxx $(src)McPrimaryParticle.cxx $(src)DecayMode.cxx $(use_requirements) $(cmt_final_setup_McTruth)
	$(echo) "(McTruth.make) Rebuilding $@"; \
	  $(build_dependencies) McTruth -all_sources -out=$@ $(src)McParticle.cxx $(src)MdcMcHit.cxx $(src)TofMcHit.cxx $(src)EmcMcHit.cxx $(src)MucMcHit.cxx $(src)ZddMcHit.cxx $(src)McPrimaryParticle.cxx $(src)DecayMode.cxx
endif

#$(McTruth_dependencies)

-include $(bin)McTruth_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),McTruthclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)McParticle.d

$(bin)$(binobj)McParticle.d : $(use_requirements) $(cmt_final_setup_McTruth)

$(bin)$(binobj)McParticle.d : $(src)McParticle.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/McParticle.dep $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(McParticle_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(McParticle_cppflags) $(McParticle_cxx_cppflags)  $(src)McParticle.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/McParticle.o $(src)McParticle.cxx $(@D)/McParticle.dep
endif
endif

$(bin)$(binobj)McParticle.o : $(src)McParticle.cxx
else
$(bin)McTruth_dependencies.make : $(McParticle_cxx_dependencies)

$(bin)$(binobj)McParticle.o : $(McParticle_cxx_dependencies)
endif
	$(cpp_echo) $(src)McParticle.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(McParticle_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(McParticle_cppflags) $(McParticle_cxx_cppflags)  $(src)McParticle.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),McTruthclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MdcMcHit.d

$(bin)$(binobj)MdcMcHit.d : $(use_requirements) $(cmt_final_setup_McTruth)

$(bin)$(binobj)MdcMcHit.d : $(src)MdcMcHit.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MdcMcHit.dep $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(MdcMcHit_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(MdcMcHit_cppflags) $(MdcMcHit_cxx_cppflags)  $(src)MdcMcHit.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MdcMcHit.o $(src)MdcMcHit.cxx $(@D)/MdcMcHit.dep
endif
endif

$(bin)$(binobj)MdcMcHit.o : $(src)MdcMcHit.cxx
else
$(bin)McTruth_dependencies.make : $(MdcMcHit_cxx_dependencies)

$(bin)$(binobj)MdcMcHit.o : $(MdcMcHit_cxx_dependencies)
endif
	$(cpp_echo) $(src)MdcMcHit.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(MdcMcHit_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(MdcMcHit_cppflags) $(MdcMcHit_cxx_cppflags)  $(src)MdcMcHit.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),McTruthclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TofMcHit.d

$(bin)$(binobj)TofMcHit.d : $(use_requirements) $(cmt_final_setup_McTruth)

$(bin)$(binobj)TofMcHit.d : $(src)TofMcHit.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/TofMcHit.dep $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(TofMcHit_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(TofMcHit_cppflags) $(TofMcHit_cxx_cppflags)  $(src)TofMcHit.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/TofMcHit.o $(src)TofMcHit.cxx $(@D)/TofMcHit.dep
endif
endif

$(bin)$(binobj)TofMcHit.o : $(src)TofMcHit.cxx
else
$(bin)McTruth_dependencies.make : $(TofMcHit_cxx_dependencies)

$(bin)$(binobj)TofMcHit.o : $(TofMcHit_cxx_dependencies)
endif
	$(cpp_echo) $(src)TofMcHit.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(TofMcHit_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(TofMcHit_cppflags) $(TofMcHit_cxx_cppflags)  $(src)TofMcHit.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),McTruthclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EmcMcHit.d

$(bin)$(binobj)EmcMcHit.d : $(use_requirements) $(cmt_final_setup_McTruth)

$(bin)$(binobj)EmcMcHit.d : $(src)EmcMcHit.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/EmcMcHit.dep $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(EmcMcHit_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(EmcMcHit_cppflags) $(EmcMcHit_cxx_cppflags)  $(src)EmcMcHit.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/EmcMcHit.o $(src)EmcMcHit.cxx $(@D)/EmcMcHit.dep
endif
endif

$(bin)$(binobj)EmcMcHit.o : $(src)EmcMcHit.cxx
else
$(bin)McTruth_dependencies.make : $(EmcMcHit_cxx_dependencies)

$(bin)$(binobj)EmcMcHit.o : $(EmcMcHit_cxx_dependencies)
endif
	$(cpp_echo) $(src)EmcMcHit.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(EmcMcHit_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(EmcMcHit_cppflags) $(EmcMcHit_cxx_cppflags)  $(src)EmcMcHit.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),McTruthclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MucMcHit.d

$(bin)$(binobj)MucMcHit.d : $(use_requirements) $(cmt_final_setup_McTruth)

$(bin)$(binobj)MucMcHit.d : $(src)MucMcHit.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MucMcHit.dep $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(MucMcHit_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(MucMcHit_cppflags) $(MucMcHit_cxx_cppflags)  $(src)MucMcHit.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MucMcHit.o $(src)MucMcHit.cxx $(@D)/MucMcHit.dep
endif
endif

$(bin)$(binobj)MucMcHit.o : $(src)MucMcHit.cxx
else
$(bin)McTruth_dependencies.make : $(MucMcHit_cxx_dependencies)

$(bin)$(binobj)MucMcHit.o : $(MucMcHit_cxx_dependencies)
endif
	$(cpp_echo) $(src)MucMcHit.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(MucMcHit_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(MucMcHit_cppflags) $(MucMcHit_cxx_cppflags)  $(src)MucMcHit.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),McTruthclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ZddMcHit.d

$(bin)$(binobj)ZddMcHit.d : $(use_requirements) $(cmt_final_setup_McTruth)

$(bin)$(binobj)ZddMcHit.d : $(src)ZddMcHit.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ZddMcHit.dep $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(ZddMcHit_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(ZddMcHit_cppflags) $(ZddMcHit_cxx_cppflags)  $(src)ZddMcHit.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ZddMcHit.o $(src)ZddMcHit.cxx $(@D)/ZddMcHit.dep
endif
endif

$(bin)$(binobj)ZddMcHit.o : $(src)ZddMcHit.cxx
else
$(bin)McTruth_dependencies.make : $(ZddMcHit_cxx_dependencies)

$(bin)$(binobj)ZddMcHit.o : $(ZddMcHit_cxx_dependencies)
endif
	$(cpp_echo) $(src)ZddMcHit.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(ZddMcHit_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(ZddMcHit_cppflags) $(ZddMcHit_cxx_cppflags)  $(src)ZddMcHit.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),McTruthclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)McPrimaryParticle.d

$(bin)$(binobj)McPrimaryParticle.d : $(use_requirements) $(cmt_final_setup_McTruth)

$(bin)$(binobj)McPrimaryParticle.d : $(src)McPrimaryParticle.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/McPrimaryParticle.dep $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(McPrimaryParticle_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(McPrimaryParticle_cppflags) $(McPrimaryParticle_cxx_cppflags)  $(src)McPrimaryParticle.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/McPrimaryParticle.o $(src)McPrimaryParticle.cxx $(@D)/McPrimaryParticle.dep
endif
endif

$(bin)$(binobj)McPrimaryParticle.o : $(src)McPrimaryParticle.cxx
else
$(bin)McTruth_dependencies.make : $(McPrimaryParticle_cxx_dependencies)

$(bin)$(binobj)McPrimaryParticle.o : $(McPrimaryParticle_cxx_dependencies)
endif
	$(cpp_echo) $(src)McPrimaryParticle.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(McPrimaryParticle_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(McPrimaryParticle_cppflags) $(McPrimaryParticle_cxx_cppflags)  $(src)McPrimaryParticle.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),McTruthclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)DecayMode.d

$(bin)$(binobj)DecayMode.d : $(use_requirements) $(cmt_final_setup_McTruth)

$(bin)$(binobj)DecayMode.d : $(src)DecayMode.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/DecayMode.dep $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(DecayMode_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(DecayMode_cppflags) $(DecayMode_cxx_cppflags)  $(src)DecayMode.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/DecayMode.o $(src)DecayMode.cxx $(@D)/DecayMode.dep
endif
endif

$(bin)$(binobj)DecayMode.o : $(src)DecayMode.cxx
else
$(bin)McTruth_dependencies.make : $(DecayMode_cxx_dependencies)

$(bin)$(binobj)DecayMode.o : $(DecayMode_cxx_dependencies)
endif
	$(cpp_echo) $(src)DecayMode.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(McTruth_pp_cppflags) $(lib_McTruth_pp_cppflags) $(DecayMode_pp_cppflags) $(use_cppflags) $(McTruth_cppflags) $(lib_McTruth_cppflags) $(DecayMode_cppflags) $(DecayMode_cxx_cppflags)  $(src)DecayMode.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: McTruthclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(McTruth.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(McTruth.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(McTruth.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(McTruth.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_McTruth)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(McTruth.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(McTruth.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(McTruth.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

McTruthclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library McTruth
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)McTruth$(library_suffix).a $(library_prefix)McTruth$(library_suffix).s? McTruth.stamp McTruth.shstamp
#-- end of cleanup_library ---------------
