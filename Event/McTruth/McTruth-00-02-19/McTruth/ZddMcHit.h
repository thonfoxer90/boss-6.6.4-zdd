#ifndef ZddMcHit_H
#define ZddMcHit_H 
#include <algorithm>
#include "GaudiKernel/ContainedObject.h"
#include "Identifier/Identifier.h"
#include "GaudiKernel/ObjectVector.h"
#include "EventModel/EventModel.h"

using namespace EventModel;
extern const CLID& CLID_ZddMcHit;

namespace Event {  // NameSpace

class ZddMcHit : virtual public ContainedObject { 
public:
   virtual const CLID& clID() const 
   {
	return ZddMcHit::classID();
   }
		   
   static const CLID& classID()
    {
	return CLID_ZddMcHit;
    } 
  // Constructor
  ZddMcHit(){};
  ZddMcHit(const Identifier& id, int trackID, double eDep, int pdg, double preTime, double postTime, double preXPosition, double postXPosition,
           double preYPosition, double postYPosition, double preZPosition, double postZPosition, double px, double py, double pz);

  // Get associated id
  Identifier identify() const;

  // Get the associated track id 
  int getTrackID() const;

  double getEDep() const;

  int getPDGCode() const;

  double getPreTime() const;
  double getPostTime() const;

  // Get the position x 
  double getPrePositionX() const;
  double getPostPositionX() const;
  // Get the position y 
  double getPrePositionY() const;
  double getPostPositionY() const;
  // Get the position z 
  double getPrePositionZ() const;
  double getPostPositionZ() const;
  // Get momentum Px 
  double getPx() const;

  // Get momentum Py  
  double getPy() const;

  // Get momentum Pz  
  double getPz() const;

//Set
  void setIdentifier(unsigned id) {m_id = id;}
  void setTrackID(int trackID){ m_trackID = trackID;}
  void setEDep(double eDep) { m_eDep = eDep;}
  void setPDGCode(int pdg) { m_pdg = pdg;}
  void setPreTime(double time) { m_preTime = time;} 
  void setPostTime(double time) { m_postTime = time;}
  void setPrePositionX(double positionX) {m_preXPosition = positionX;}
  void setPrePositionY(double positionY) {m_preYPosition = positionY;}
  void setPrePositionZ(double positionZ) {m_preZPosition = positionZ;}
  void setPostPositionX(double positionX) {m_postXPosition = positionX;}
  void setPostPositionY(double positionY) {m_postYPosition = positionY;}
  void setPostPositionZ(double positionZ) {m_postZPosition = positionZ;}
  void setPx(double px) {m_px = px;}
  void setPy(double py) {m_py = py;}
  void setPz(double pz) {m_pz = pz;}

    
private:
  ZddMcHit(const ZddMcHit &);

  ZddMcHit &operator=(const ZddMcHit &);

  // const Identifier m_id;  //2005/12/29 wensp modified

  Identifier m_id;
  
  int m_trackID;

  double m_eDep;

  int m_pdg;

  double m_preTime;
  double m_postTime;

  double m_preXPosition;
  double m_postXPosition;

  double m_preYPosition;
  double m_postYPosition;

  double m_preZPosition;
  double m_postZPosition;
 
  double m_px;
  
  double m_py;

  double m_pz;
};

typedef ObjectVector<ZddMcHit> ZddMcHitCol;

} // NameSpace Event
#endif 
