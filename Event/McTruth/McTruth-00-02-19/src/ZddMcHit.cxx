#include "McTruth/ZddMcHit.h"

Event::ZddMcHit::ZddMcHit(const Identifier& id, int trackID, double eDep, int pdg, double preTime, double postTime, double preXPosition, double postXPosition, double preYPosition, double postYPosition, double preZPosition, double postZPosition, double px, double py, double pz) 
  : m_id(id), m_trackID(trackID), m_eDep(eDep), m_pdg(pdg), m_preTime(preTime), m_postTime(postTime),
           m_preXPosition(preXPosition), m_postXPosition(postXPosition), m_preYPosition(preYPosition), m_postYPosition(postYPosition), m_preZPosition(preZPosition), m_postZPosition(postZPosition), m_px(px), m_py(py), m_pz(pz){}

// Get associated id
Identifier Event::ZddMcHit::identify() const {
  return  m_id;
}
 
// Get the associated track id 
int Event::ZddMcHit::getTrackID() const {
  return m_trackID;
}

double Event::ZddMcHit::getEDep() const {
  return m_eDep;
}

int Event::ZddMcHit::getPDGCode() const {
  return m_pdg;
}

double Event::ZddMcHit::getPreTime() const {
  return m_preTime;
}

double Event::ZddMcHit::getPostTime() const {
  return m_postTime;
}

// Get the position x 
double Event::ZddMcHit::getPrePositionX() const {
  return m_preXPosition;
}
// Get the position x                                                                                                                               
double Event::ZddMcHit::getPostPositionX() const {
  return m_postXPosition;
}

// Get the position y 
double Event::ZddMcHit::getPrePositionY() const {
  return m_preYPosition;
}
// Get the position y
double Event::ZddMcHit::getPostPositionY() const {
  return m_postYPosition;
}

// Get the position z 
double Event::ZddMcHit::getPrePositionZ() const {
  return m_preZPosition;
}
// Get the position z                                                                                                                                 
double Event::ZddMcHit::getPostPositionZ() const {
  return m_postZPosition;
}
  
// Get momentum Px 
double Event::ZddMcHit::getPx() const {
  return m_px;
}

// Get momentum Py  
double Event::ZddMcHit::getPy() const {
  return m_py; 
}

// Get momentum Pz  
double Event::ZddMcHit::getPz() const {
  return m_pz; 
}
  
