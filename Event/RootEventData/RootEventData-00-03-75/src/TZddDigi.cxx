#include "RootEventData/TZddDigi.h"

#include <iostream>

ClassImp(TZddDigi)

//**************************************
TZddDigi::TZddDigi() {
  Clear();
}

//*************************************

TZddDigi::~TZddDigi() {
	  Clear();
} 

/************************************

void TZddDigi::Clear(Option_t *option) {
}


void TZddDigi::Print(Option_t *option) const {
     TObject::Print(option);
     std::cout.precision(2);
     std::cout << "Identifier:    " << m_intId         << std::endl;
     std::cout << "TimeChannel:  " << m_timeChannel    <<std::endl;
     std::cout << "ChargeChannel:" << m_chargeChannel <<std::endl;
}
 ******************************/



void TZddDigi::setMeasure(const UInt_t measure) {
	 m_measure = measure;
}

UInt_t TZddDigi::getMeasure() const {
	return m_measure;
}


