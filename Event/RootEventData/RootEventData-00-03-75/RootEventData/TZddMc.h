
#ifndef RootEventData_TZddMc_H
#define RootEventData_TZddMc_H 1

#include "TObject.h"
#include <map>

//#include <vector>
//using namespace std;

class TZddMc : public TObject {

public:
    
   TZddMc();
   ~TZddMc();
//Get

   // Get Hit_Map
   std::map<Int_t, Double_t> getHitMap() const { return m_hitMap; }

   // Get whether or not hit on Zdd
   int  getHitZdd() const {return m_hitZdd;}

   int  getPreCrystalNo() const {return m_preCrystalNo;}
   int  getPostCrystalNo() const {return m_postCrystalNo;}

   // Get pdg id
   int getPDGCode() const {return m_PDGCode;}

   // Get crystal or dead time
   double getPreTime() const {return m_preTime;}
   double getPostTime() const {return m_postTime;}

   // Get associated id
   UInt_t getId() const {return m_id;}

  // Get the associated track id 
  Int_t getTrackID() const {return m_trackID; }

  // Get the position x 
  Double_t getPrePositionX() const {return m_preXPosition;}
  Double_t getPostPositionX() const {return m_postXPosition;}

  // Get the position y 
  Double_t getPrePositionY() const {return m_preYPosition;}
  Double_t getPostPositionY() const {return m_postYPosition;}

  // Get the position z 
  Double_t getPrePositionZ() const {return m_preZPosition;}
  Double_t getPostPositionZ() const {return m_postZPosition;}

  // Get momentum Px 
  Double_t getPx() const {return m_px ;}

  // Get momentum Py  
  Double_t getPy() const {return m_py ;}

  // Get momentum Pz  
  Double_t getPz() const {return m_pz ;}
  
  // Get the total deposited energy 
  Double_t getEDep() const {return m_eDep;}

//Set

  void setHitMap(std::map<Int_t, Double_t>  hitMap) {m_hitMap = hitMap; }
  void setHitZdd(int hitZdd) {m_hitZdd = hitZdd ;}
  void setPDGCode(int PDGCode) {m_PDGCode = PDGCode ;}
  void setPreCrystalNo( int crystalNo) {m_preCrystalNo =crystalNo; }
  void setPostCrystalNo ( int crystalNo) {m_postCrystalNo = crystalNo; }
  void setPreTime(double time) {m_preTime = time ;}
  void setPostTime(double time) {m_postTime = time ;}
  void setId(UInt_t id) {m_id = id ;}
  void setTrackID(UInt_t trackIndex){ m_trackID = trackIndex;}
  void setPrePositionX(Double_t positionX) {m_preXPosition = positionX;}
  void setPrePositionY(Double_t positionY) {m_preYPosition = positionY;}
  void setPrePositionZ(Double_t positionZ) {m_preZPosition = positionZ;}
  void setPostPositionX(Double_t positionX) {m_postXPosition = positionX;}
  void setPostPositionY(Double_t positionY) {m_postYPosition = positionY;}
  void setPostPositionZ(Double_t positionZ) {m_postZPosition = positionZ;}
  void setPx(Double_t px) {m_px = px;}
  void setPy(Double_t py) {m_py = py;}
  void setPz(Double_t pz) {m_pz = pz;}
  void setEDep(Double_t depositEnergy) {m_eDep = depositEnergy;}

private:

  //whether hit Zdd
  int m_hitZdd;
  //particle ID
  int m_PDGCode;

  int m_preCrystalNo;
  int m_postCrystalNo;

  //hit crystal time or dead time
  double m_preTime;
  double m_postTime;

  UInt_t m_id; 

  UInt_t m_trackID;

  Double_t m_preXPosition;
  Double_t m_postXPosition;

  Double_t m_preYPosition;
  Double_t m_postYPosition;

  Double_t m_preZPosition;
  Double_t m_postZPosition;

  Double_t m_px;

  Double_t m_py;

  Double_t m_pz;

  Double_t m_eDep;

  std::map<Int_t, Double_t> m_hitMap;

  ClassDef(TZddMc,1)

};

#endif //TrackRootData_TZddMc_H

