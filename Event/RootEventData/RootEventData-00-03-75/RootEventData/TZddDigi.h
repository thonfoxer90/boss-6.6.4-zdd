#ifndef DigiRootData_TZddDigi_H
#define DigiRootData_TZddDigi_H 1

#include "TRawData.h"

class TZddDigi : public TRawData {

public:
    
   TZddDigi();
   ~TZddDigi();


//   void Clear(Option_t *option ="");

//   void Print(Option_t *option="") const;

   // Set Measure Word
   void setMeasure(const UInt_t measure);

  // Get Measure Word
   UInt_t  getMeasure() const; 

private:

    UInt_t  m_measure;

    ClassDef(TZddDigi,2)
};


#endif //DigiRootData_TZddDigi_H

