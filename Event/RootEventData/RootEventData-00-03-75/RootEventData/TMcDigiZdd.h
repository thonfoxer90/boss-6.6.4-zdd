#ifndef DigiRootData_TMcDigiZdd_H
#define DigiRootData_TMcDigiZdd_H 1

#include "TObject.h"
#include "TString.h"
#include "TVector3.h"

class TMcDigiZdd: public TObject {
    
public:
    
    TMcDigiZdd();
    
    ~TMcDigiZdd ();
  
    void SetPartID(Int_t id)            { m_partID = id; } ;
    void SetDetectorID(Int_t detID)     { m_detectorID = detID; };
    //    void SetTrackID( Int_t trackID)     { m_trackID = trackID; };
    void SetCrystalNo (Int_t crystalNo) { m_crystalNo = crystalNo; };
    void SetChargeChannel(Double_t chargeChannel)        { m_chargeChannel = chargeChannel; };
    void SetTimeChannel(Double_t timeChannel)         { m_timeChannel = timeChannel; };
 
    Int_t GetPartID()           const      {return m_partID;};
    Int_t GetDetectorID()       const      {return m_detectorID;};
    //    Int_t GetTrackID()       const      {return m_trackID;};  
    Int_t GetCrystalNo()        const      {return m_crystalNo;};
    Double_t GetChargeChannel() const      { return m_chargeChannel; };
    Double_t GetTimeChannel()   const      { return m_timeChannel; };


private:

    Int_t m_partID;
    Int_t m_detectorID;

    Int_t m_crystalNo;
    Double_t  m_chargeChannel;
    Double_t  m_timeChannel;
 
    ClassDef(TMcDigiZdd,1)
};

#endif 
