# echo "Setting RootEventData RootEventData-00-03-75 in /home/bgarillo/boss-6.6.4-ZDD/Event"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cluster/him/bes3/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh

tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=RootEventData -version=RootEventData-00-03-75 -path=/home/bgarillo/boss-6.6.4-ZDD/Event  -no_cleanup $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

