#-- start of make_header -----------------

#====================================
#  Library PhokharaLib
#
#   Generated Mon Jun 29 12:22:02 2015  by wittke
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_PhokharaLib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_PhokharaLib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_PhokharaLib

Phokhara_tag = $(tag)

#cmt_local_tagfile_PhokharaLib = $(Phokhara_tag)_PhokharaLib.make
cmt_local_tagfile_PhokharaLib = $(bin)$(Phokhara_tag)_PhokharaLib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Phokhara_tag = $(tag)

#cmt_local_tagfile_PhokharaLib = $(Phokhara_tag).make
cmt_local_tagfile_PhokharaLib = $(bin)$(Phokhara_tag).make

endif

include $(cmt_local_tagfile_PhokharaLib)
#-include $(cmt_local_tagfile_PhokharaLib)

ifdef cmt_PhokharaLib_has_target_tag

cmt_final_setup_PhokharaLib = $(bin)setup_PhokharaLib.make
#cmt_final_setup_PhokharaLib = $(bin)Phokhara_PhokharaLibsetup.make
cmt_local_PhokharaLib_makefile = $(bin)PhokharaLib.make

else

cmt_final_setup_PhokharaLib = $(bin)setup.make
#cmt_final_setup_PhokharaLib = $(bin)Phokharasetup.make
cmt_local_PhokharaLib_makefile = $(bin)PhokharaLib.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Phokharasetup.make

#PhokharaLib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'PhokharaLib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = PhokharaLib/
#PhokharaLib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

PhokharaLiblibname   = $(bin)$(library_prefix)PhokharaLib$(library_suffix)
PhokharaLiblib       = $(PhokharaLiblibname).a
PhokharaLibstamp     = $(bin)PhokharaLib.stamp
PhokharaLibshstamp   = $(bin)PhokharaLib.shstamp

PhokharaLib :: dirs  PhokharaLibLIB
	$(echo) "PhokharaLib ok"

#-- end of libary_header ----------------

PhokharaLibLIB :: $(PhokharaLiblib) $(PhokharaLibshstamp)
	@/bin/echo "------> PhokharaLib : library ok"

$(PhokharaLiblib) :: $(bin)Phokhara.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(PhokharaLiblib) $?
	$(lib_silent) $(ranlib) $(PhokharaLiblib)
	$(lib_silent) cat /dev/null >$(PhokharaLibstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(PhokharaLiblibname).$(shlibsuffix) :: $(PhokharaLiblib) $(PhokharaLibstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" PhokharaLib $(PhokharaLib_shlibflags)

$(PhokharaLibshstamp) :: $(PhokharaLiblibname).$(shlibsuffix)
	@if test -f $(PhokharaLiblibname).$(shlibsuffix) ; then cat /dev/null >$(PhokharaLibshstamp) ; fi

PhokharaLibclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)Phokhara.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
PhokharaLibinstallname = $(library_prefix)PhokharaLib$(library_suffix).$(shlibsuffix)

PhokharaLib :: PhokharaLibinstall

install :: PhokharaLibinstall

PhokharaLibinstall :: $(install_dir)/$(PhokharaLibinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(PhokharaLibinstallname) :: $(bin)$(PhokharaLibinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(PhokharaLibinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(PhokharaLibinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(PhokharaLibinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(PhokharaLibinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(PhokharaLibinstallname) $(install_dir)/$(PhokharaLibinstallname); \
	      echo `pwd`/$(PhokharaLibinstallname) >$(install_dir)/$(PhokharaLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(PhokharaLibinstallname), no installation directory specified"; \
	  fi; \
	fi

PhokharaLibclean :: PhokharaLibuninstall

uninstall :: PhokharaLibuninstall

PhokharaLibuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(PhokharaLibinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(PhokharaLibinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(PhokharaLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(PhokharaLibinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),PhokharaLibclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)PhokharaLib_dependencies.make :: dirs

ifndef QUICK
$(bin)PhokharaLib_dependencies.make : $(src)Phokhara.cxx $(use_requirements) $(cmt_final_setup_PhokharaLib)
	$(echo) "(PhokharaLib.make) Rebuilding $@"; \
	  $(build_dependencies) PhokharaLib -all_sources -out=$@ $(src)Phokhara.cxx
endif

#$(PhokharaLib_dependencies)

-include $(bin)PhokharaLib_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),PhokharaLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Phokhara.d

$(bin)$(binobj)Phokhara.d : $(use_requirements) $(cmt_final_setup_PhokharaLib)

$(bin)$(binobj)Phokhara.d : $(src)Phokhara.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Phokhara.dep $(use_pp_cppflags) $(PhokharaLib_pp_cppflags) $(lib_PhokharaLib_pp_cppflags) $(Phokhara_pp_cppflags) $(use_cppflags) $(PhokharaLib_cppflags) $(lib_PhokharaLib_cppflags) $(Phokhara_cppflags) $(Phokhara_cxx_cppflags)  $(src)Phokhara.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Phokhara.o $(src)Phokhara.cxx $(@D)/Phokhara.dep
endif
endif

$(bin)$(binobj)Phokhara.o : $(src)Phokhara.cxx
else
$(bin)PhokharaLib_dependencies.make : $(Phokhara_cxx_dependencies)

$(bin)$(binobj)Phokhara.o : $(Phokhara_cxx_dependencies)
endif
	$(cpp_echo) $(src)Phokhara.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(PhokharaLib_pp_cppflags) $(lib_PhokharaLib_pp_cppflags) $(Phokhara_pp_cppflags) $(use_cppflags) $(PhokharaLib_cppflags) $(lib_PhokharaLib_cppflags) $(Phokhara_cppflags) $(Phokhara_cxx_cppflags)  $(src)Phokhara.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: PhokharaLibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(PhokharaLib.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(PhokharaLib.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(PhokharaLib.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(PhokharaLib.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_PhokharaLib)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(PhokharaLib.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(PhokharaLib.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(PhokharaLib.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

PhokharaLibclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library PhokharaLib
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)PhokharaLib$(library_suffix).a $(library_prefix)PhokharaLib$(library_suffix).s? PhokharaLib.stamp PhokharaLib.shstamp
#-- end of cleanup_library ---------------
