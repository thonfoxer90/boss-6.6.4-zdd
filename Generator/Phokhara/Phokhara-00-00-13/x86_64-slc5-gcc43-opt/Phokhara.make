#-- start of make_header -----------------

#====================================
#  Library Phokhara
#
#   Generated Mon Jun 29 12:22:08 2015  by wittke
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_Phokhara_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_Phokhara_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_Phokhara

Phokhara_tag = $(tag)

#cmt_local_tagfile_Phokhara = $(Phokhara_tag)_Phokhara.make
cmt_local_tagfile_Phokhara = $(bin)$(Phokhara_tag)_Phokhara.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Phokhara_tag = $(tag)

#cmt_local_tagfile_Phokhara = $(Phokhara_tag).make
cmt_local_tagfile_Phokhara = $(bin)$(Phokhara_tag).make

endif

include $(cmt_local_tagfile_Phokhara)
#-include $(cmt_local_tagfile_Phokhara)

ifdef cmt_Phokhara_has_target_tag

cmt_final_setup_Phokhara = $(bin)setup_Phokhara.make
#cmt_final_setup_Phokhara = $(bin)Phokhara_Phokharasetup.make
cmt_local_Phokhara_makefile = $(bin)Phokhara.make

else

cmt_final_setup_Phokhara = $(bin)setup.make
#cmt_final_setup_Phokhara = $(bin)Phokharasetup.make
cmt_local_Phokhara_makefile = $(bin)Phokhara.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Phokharasetup.make

#Phokhara :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'Phokhara'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = Phokhara/
#Phokhara::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

Phokharalibname   = $(bin)$(library_prefix)Phokhara$(library_suffix)
Phokharalib       = $(Phokharalibname).a
Phokharastamp     = $(bin)Phokhara.stamp
Phokharashstamp   = $(bin)Phokhara.shstamp

Phokhara :: dirs  PhokharaLIB
	$(echo) "Phokhara ok"

#-- end of libary_header ----------------

PhokharaLIB :: $(Phokharalib) $(Phokharashstamp)
	@/bin/echo "------> Phokhara : library ok"

$(Phokharalib) :: $(bin)Phokhara__load.o $(bin)Phokhara__entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(Phokharalib) $?
	$(lib_silent) $(ranlib) $(Phokharalib)
	$(lib_silent) cat /dev/null >$(Phokharastamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(Phokharalibname).$(shlibsuffix) :: $(Phokharalib) $(Phokharastamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" Phokhara $(Phokhara_shlibflags)

$(Phokharashstamp) :: $(Phokharalibname).$(shlibsuffix)
	@if test -f $(Phokharalibname).$(shlibsuffix) ; then cat /dev/null >$(Phokharashstamp) ; fi

Phokharaclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)Phokhara__load.o $(bin)Phokhara__entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
Phokharainstallname = $(library_prefix)Phokhara$(library_suffix).$(shlibsuffix)

Phokhara :: Phokharainstall

install :: Phokharainstall

Phokharainstall :: $(install_dir)/$(Phokharainstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(Phokharainstallname) :: $(bin)$(Phokharainstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(Phokharainstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(Phokharainstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(Phokharainstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(Phokharainstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(Phokharainstallname) $(install_dir)/$(Phokharainstallname); \
	      echo `pwd`/$(Phokharainstallname) >$(install_dir)/$(Phokharainstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(Phokharainstallname), no installation directory specified"; \
	  fi; \
	fi

Phokharaclean :: Phokharauninstall

uninstall :: Phokharauninstall

Phokharauninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(Phokharainstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(Phokharainstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(Phokharainstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(Phokharainstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),Phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)Phokhara_dependencies.make :: dirs

ifndef QUICK
$(bin)Phokhara_dependencies.make : $(src)components/Phokhara__load.cxx $(src)components/Phokhara__entries.cxx $(use_requirements) $(cmt_final_setup_Phokhara)
	$(echo) "(Phokhara.make) Rebuilding $@"; \
	  $(build_dependencies) Phokhara -all_sources -out=$@ $(src)components/Phokhara__load.cxx $(src)components/Phokhara__entries.cxx
endif

#$(Phokhara_dependencies)

-include $(bin)Phokhara_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Phokhara__load.d

$(bin)$(binobj)Phokhara__load.d : $(use_requirements) $(cmt_final_setup_Phokhara)

$(bin)$(binobj)Phokhara__load.d : $(src)components/Phokhara__load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Phokhara__load.dep $(use_pp_cppflags) $(Phokhara_pp_cppflags) $(lib_Phokhara_pp_cppflags) $(Phokhara__load_pp_cppflags) $(use_cppflags) $(Phokhara_cppflags) $(lib_Phokhara_cppflags) $(Phokhara__load_cppflags) $(Phokhara__load_cxx_cppflags) -I../src/components $(src)components/Phokhara__load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Phokhara__load.o $(src)components/Phokhara__load.cxx $(@D)/Phokhara__load.dep
endif
endif

$(bin)$(binobj)Phokhara__load.o : $(src)components/Phokhara__load.cxx
else
$(bin)Phokhara_dependencies.make : $(Phokhara__load_cxx_dependencies)

$(bin)$(binobj)Phokhara__load.o : $(Phokhara__load_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/Phokhara__load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Phokhara_pp_cppflags) $(lib_Phokhara_pp_cppflags) $(Phokhara__load_pp_cppflags) $(use_cppflags) $(Phokhara_cppflags) $(lib_Phokhara_cppflags) $(Phokhara__load_cppflags) $(Phokhara__load_cxx_cppflags) -I../src/components $(src)components/Phokhara__load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Phokhara__entries.d

$(bin)$(binobj)Phokhara__entries.d : $(use_requirements) $(cmt_final_setup_Phokhara)

$(bin)$(binobj)Phokhara__entries.d : $(src)components/Phokhara__entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Phokhara__entries.dep $(use_pp_cppflags) $(Phokhara_pp_cppflags) $(lib_Phokhara_pp_cppflags) $(Phokhara__entries_pp_cppflags) $(use_cppflags) $(Phokhara_cppflags) $(lib_Phokhara_cppflags) $(Phokhara__entries_cppflags) $(Phokhara__entries_cxx_cppflags) -I../src/components $(src)components/Phokhara__entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Phokhara__entries.o $(src)components/Phokhara__entries.cxx $(@D)/Phokhara__entries.dep
endif
endif

$(bin)$(binobj)Phokhara__entries.o : $(src)components/Phokhara__entries.cxx
else
$(bin)Phokhara_dependencies.make : $(Phokhara__entries_cxx_dependencies)

$(bin)$(binobj)Phokhara__entries.o : $(Phokhara__entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/Phokhara__entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Phokhara_pp_cppflags) $(lib_Phokhara_pp_cppflags) $(Phokhara__entries_pp_cppflags) $(use_cppflags) $(Phokhara_cppflags) $(lib_Phokhara_cppflags) $(Phokhara__entries_cppflags) $(Phokhara__entries_cxx_cppflags) -I../src/components $(src)components/Phokhara__entries.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: Phokharaclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(Phokhara.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(Phokhara.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(Phokhara.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(Phokhara.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_Phokhara)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Phokhara.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Phokhara.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Phokhara.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

Phokharaclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library Phokhara
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)Phokhara$(library_suffix).a $(library_prefix)Phokhara$(library_suffix).s? Phokhara.stamp Phokhara.shstamp
#-- end of cleanup_library ---------------
