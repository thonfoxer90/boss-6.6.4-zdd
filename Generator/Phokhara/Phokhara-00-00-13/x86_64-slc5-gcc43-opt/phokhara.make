#-- start of make_header -----------------

#====================================
#  Library phokhara
#
#   Generated Mon Jun 29 12:21:54 2015  by wittke
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_phokhara_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_phokhara_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_phokhara

Phokhara_tag = $(tag)

#cmt_local_tagfile_phokhara = $(Phokhara_tag)_phokhara.make
cmt_local_tagfile_phokhara = $(bin)$(Phokhara_tag)_phokhara.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Phokhara_tag = $(tag)

#cmt_local_tagfile_phokhara = $(Phokhara_tag).make
cmt_local_tagfile_phokhara = $(bin)$(Phokhara_tag).make

endif

include $(cmt_local_tagfile_phokhara)
#-include $(cmt_local_tagfile_phokhara)

ifdef cmt_phokhara_has_target_tag

cmt_final_setup_phokhara = $(bin)setup_phokhara.make
#cmt_final_setup_phokhara = $(bin)Phokhara_phokharasetup.make
cmt_local_phokhara_makefile = $(bin)phokhara.make

else

cmt_final_setup_phokhara = $(bin)setup.make
#cmt_final_setup_phokhara = $(bin)Phokharasetup.make
cmt_local_phokhara_makefile = $(bin)phokhara.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Phokharasetup.make

#phokhara :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'phokhara'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = phokhara/
#phokhara::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

phokharalibname   = $(bin)$(library_prefix)phokhara$(library_suffix)
phokharalib       = $(phokharalibname).a
phokharastamp     = $(bin)phokhara.stamp
phokharashstamp   = $(bin)phokhara.shstamp

phokhara :: dirs  phokharaLIB
	$(echo) "phokhara ok"

#-- end of libary_header ----------------

phokharaLIB :: $(phokharalib) $(phokharashstamp)
	@/bin/echo "------> phokhara : library ok"

$(phokharalib) :: $(bin)phokhara_7.0.o $(bin)ranlux_fort.o $(bin)ranlxd.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(phokharalib) $?
	$(lib_silent) $(ranlib) $(phokharalib)
	$(lib_silent) cat /dev/null >$(phokharastamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(phokharalibname).$(shlibsuffix) :: $(phokharalib) $(phokharastamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" phokhara $(phokhara_shlibflags)

$(phokharashstamp) :: $(phokharalibname).$(shlibsuffix)
	@if test -f $(phokharalibname).$(shlibsuffix) ; then cat /dev/null >$(phokharashstamp) ; fi

phokharaclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)phokhara_7.0.o $(bin)ranlux_fort.o $(bin)ranlxd.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
phokharainstallname = $(library_prefix)phokhara$(library_suffix).$(shlibsuffix)

phokhara :: phokharainstall

install :: phokharainstall

phokharainstall :: $(install_dir)/$(phokharainstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(phokharainstallname) :: $(bin)$(phokharainstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(phokharainstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(phokharainstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(phokharainstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(phokharainstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(phokharainstallname) $(install_dir)/$(phokharainstallname); \
	      echo `pwd`/$(phokharainstallname) >$(install_dir)/$(phokharainstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(phokharainstallname), no installation directory specified"; \
	  fi; \
	fi

phokharaclean :: phokharauninstall

uninstall :: phokharauninstall

phokharauninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(phokharainstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(phokharainstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(phokharainstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(phokharainstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)phokhara_dependencies.make :: dirs

ifndef QUICK
$(bin)phokhara_dependencies.make : $(src)phokhara_7.0.f $(src)ranlux_fort.c $(src)ranlxd.c $(use_requirements) $(cmt_final_setup_phokhara)
	$(echo) "(phokhara.make) Rebuilding $@"; \
	  $(build_dependencies) phokhara -all_sources -out=$@ $(src)phokhara_7.0.f $(src)ranlux_fort.c $(src)ranlxd.c
endif

#$(phokhara_dependencies)

-include $(bin)phokhara_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(phokhara_7.0_f_dependencies)
 
$(bin)$(binobj)phokhara_7.0.o : $(phokhara_7.0_f_dependencies)
	$(fortran_echo) $(src)phokhara_7.0.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(phokhara_7.0_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(phokhara_7.0_fflags) $(phokhara_7.0_f_fflags)  $(src)phokhara_7.0.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)phokhara_7.0.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(phokhara_7.0_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(phokhara_7.0_fflags) $(phokhara_7.0_f_fflags)  $(src)phokhara_7.0.f

#-- end of fortran_library ------
#-- start of c_library ------

ifneq ($(cdepflags),)

ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ranlux_fort.d

$(bin)$(binobj)ranlux_fort.d : $(use_requirements) $(cmt_final_setup_phokhara)

$(bin)$(binobj)ranlux_fort.d : $(src)ranlux_fort.c
	$(dep_echo) $@
	$(c_silent) $(ccomp) $(cdepflags) -o $(@D)/ranlux_fort.dep $(use_pp_cflags) $(phokhara_pp_cflags) $(lib_phokhara_pp_cflags) $(ranlux_fort_pp_cflags) $(use_cflags) $(phokhara_cflags) $(lib_phokhara_cflags) $(ranlux_fort_cflags) $(ranlux_fort_c_cflags)  $(src)ranlux_fort.c
	$(c_silent) $(format_dependencies) $@ $(@D)/ranlux_fort.o $(src)ranlux_fort.c $(@D)/ranlux_fort.dep
endif
endif

$(bin)$(binobj)ranlux_fort.o : $(src)ranlux_fort.c
else
$(bin)phokhara_dependencies.make : $(ranlux_fort_c_dependencies)

$(bin)$(binobj)ranlux_fort.o : $(ranlux_fort_c_dependencies)
endif
	$(c_echo) $(src)ranlux_fort.c
	$(c_silent) $(ccomp) -o $@ $(use_pp_cflags) $(phokhara_pp_cflags) $(lib_phokhara_pp_cflags) $(ranlux_fort_pp_cflags) $(use_cflags) $(phokhara_cflags) $(lib_phokhara_cflags) $(ranlux_fort_cflags) $(ranlux_fort_c_cflags)  $(src)ranlux_fort.c

#-- end of c_library ------
#-- start of c_library ------

ifneq ($(cdepflags),)

ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ranlxd.d

$(bin)$(binobj)ranlxd.d : $(use_requirements) $(cmt_final_setup_phokhara)

$(bin)$(binobj)ranlxd.d : $(src)ranlxd.c
	$(dep_echo) $@
	$(c_silent) $(ccomp) $(cdepflags) -o $(@D)/ranlxd.dep $(use_pp_cflags) $(phokhara_pp_cflags) $(lib_phokhara_pp_cflags) $(ranlxd_pp_cflags) $(use_cflags) $(phokhara_cflags) $(lib_phokhara_cflags) $(ranlxd_cflags) $(ranlxd_c_cflags)  $(src)ranlxd.c
	$(c_silent) $(format_dependencies) $@ $(@D)/ranlxd.o $(src)ranlxd.c $(@D)/ranlxd.dep
endif
endif

$(bin)$(binobj)ranlxd.o : $(src)ranlxd.c
else
$(bin)phokhara_dependencies.make : $(ranlxd_c_dependencies)

$(bin)$(binobj)ranlxd.o : $(ranlxd_c_dependencies)
endif
	$(c_echo) $(src)ranlxd.c
	$(c_silent) $(ccomp) -o $@ $(use_pp_cflags) $(phokhara_pp_cflags) $(lib_phokhara_pp_cflags) $(ranlxd_pp_cflags) $(use_cflags) $(phokhara_cflags) $(lib_phokhara_cflags) $(ranlxd_cflags) $(ranlxd_c_cflags)  $(src)ranlxd.c

#-- end of c_library ------
#-- start of cleanup_header --------------

clean :: phokharaclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(phokhara.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(phokhara.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(phokhara.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(phokhara.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_phokhara)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(phokhara.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(phokhara.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(phokhara.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

phokharaclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library phokhara
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)phokhara$(library_suffix).a $(library_prefix)phokhara$(library_suffix).s? phokhara.stamp phokhara.shstamp
#-- end of cleanup_library ---------------
