//====================================================================
//  BHWIDE_entries.cxx
//--------------------------------------------------------------------
//
//  Package    : Generator/Bhwide
//
//  Description: Declaration of the components (factory entries)
//               specified by this component library.
//
//====================================================================

#include "Phokhara/Phokhara.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_ALGORITHM_FACTORY( Phokhara )

DECLARE_FACTORY_ENTRIES( Phokhara ) {
    DECLARE_ALGORITHM( Phokhara );
}
