#-- start of make_header -----------------

#====================================
#  Library phokhara
#
#   Generated Tue May  2 09:16:08 2017  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_phokhara_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_phokhara_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_phokhara

Phokhara_tag = $(tag)

#cmt_local_tagfile_phokhara = $(Phokhara_tag)_phokhara.make
cmt_local_tagfile_phokhara = $(bin)$(Phokhara_tag)_phokhara.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Phokhara_tag = $(tag)

#cmt_local_tagfile_phokhara = $(Phokhara_tag).make
cmt_local_tagfile_phokhara = $(bin)$(Phokhara_tag).make

endif

include $(cmt_local_tagfile_phokhara)
#-include $(cmt_local_tagfile_phokhara)

ifdef cmt_phokhara_has_target_tag

cmt_final_setup_phokhara = $(bin)setup_phokhara.make
#cmt_final_setup_phokhara = $(bin)Phokhara_phokharasetup.make
cmt_local_phokhara_makefile = $(bin)phokhara.make

else

cmt_final_setup_phokhara = $(bin)setup.make
#cmt_final_setup_phokhara = $(bin)Phokharasetup.make
cmt_local_phokhara_makefile = $(bin)phokhara.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Phokharasetup.make

#phokhara :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'phokhara'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = phokhara/
#phokhara::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

phokharalibname   = $(bin)$(library_prefix)phokhara$(library_suffix)
phokharalib       = $(phokharalibname).a
phokharastamp     = $(bin)phokhara.stamp
phokharashstamp   = $(bin)phokhara.shstamp

phokhara :: dirs  phokharaLIB
	$(echo) "phokhara ok"

#-- end of libary_header ----------------

phokharaLIB :: $(phokharalib) $(phokharashstamp)
	@/bin/echo "------> phokhara : library ok"

$(phokharalib) :: $(bin)phokhara_9.1.o $(bin)ranlxd.o $(bin)ranlux_fort.o $(bin)sqampeemmg-1_5.o $(bin)sqampeemmg-2_5.o $(bin)sqampeemmg-3_5.o $(bin)sqampeemmg-4_5.o $(bin)sqampeemmg-5_5.o $(bin)sqampeemmg-main_5.o $(bin)sqampeemmg-wrap_5.o $(bin)sqampeemmg-1_isr.o $(bin)sqampeemmg-main_isr.o $(bin)sqampeemmg-wrap_isr.o $(bin)sqampeemmg-1_isrfsr.o $(bin)sqampeemmg-main_isrfsr.o $(bin)sqampeemmg-wrap_isrfsr.o $(bin)sqampeemmg-1_mix.o $(bin)sqampeemmg-main_mix.o $(bin)sqampeemmg-wrap_mix.o $(bin)ffabcd.o $(bin)ffxd0.o $(bin)ffcxs4.o $(bin)ff2dl2.o $(bin)ffcxr.o $(bin)ffcrr.o $(bin)ffxc0i.o $(bin)ffcxyz.o $(bin)ffcc0p.o $(bin)ffxd0h.o $(bin)ffcxs3.o $(bin)ffxd0i.o $(bin)ffdcxs.o $(bin)ffxc0p.o $(bin)ffdel4.o $(bin)ffxli2.o $(bin)ffxc0.o $(bin)ffcli2.o $(bin)ffdel3.o $(bin)ffcel2.o $(bin)ffxd0p.o $(bin)fftran.o $(bin)ffcel3.o $(bin)ffdxc0.o $(bin)ffxdbd.o $(bin)ffxxyz.o $(bin)ffinit_mine.o $(bin)ffdel2.o $(bin)ffdcc0.o $(bin)auxCD.o $(bin)qlbox10.o $(bin)qlbox11.o $(bin)qlbox12.o $(bin)qlbox13.o $(bin)qlbox14.o $(bin)qlbox15.o $(bin)qlbox16.o $(bin)qlbox1.o $(bin)qlbox2.o $(bin)qlbox3.o $(bin)qlbox4.o $(bin)qlbox5.o $(bin)qlbox6.o $(bin)qlbox7.o $(bin)qlbox8.o $(bin)qlbox9.o $(bin)qlcLi2omx2.o $(bin)qlcLi2omx3.o $(bin)qlfndd.o $(bin)qlfunctions.o $(bin)qlI1.o $(bin)qlI2.o $(bin)qlI2fin.o $(bin)qlI3.o $(bin)qlI3fin.o $(bin)qlI3sub.o $(bin)qlI4array.o $(bin)qlI4DNS41.o $(bin)qlI4.o $(bin)qlI4fin.o $(bin)qlI4sub0m.o $(bin)qlI4sub1m.o $(bin)qlI4sub2ma.o $(bin)qlI4sub2m.o $(bin)qlI4sub2mo.o $(bin)qlI4sub3m.o $(bin)qlinit.o $(bin)qlkfn.o $(bin)qlLi2omprod.o $(bin)qlLi2omrat.o $(bin)qlLi2omx2.o $(bin)qlLi2omx.o $(bin)qllnrat.o $(bin)qlratgam.o $(bin)qlratreal.o $(bin)qlsnglsort.o $(bin)qlspencer.o $(bin)qltri1.o $(bin)qltri2.o $(bin)qltri3.o $(bin)qltri4.o $(bin)qltri5.o $(bin)qltri6.o $(bin)qltrisort.o $(bin)qlxpicheck.o $(bin)qlYcalc.o $(bin)qlzero.o $(bin)qlddilog.o $(bin)pjfry.o $(bin)minor.o $(bin)integral.o $(bin)minoreval.o $(bin)cache.o $(bin)minorex.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(phokharalib) $?
	$(lib_silent) $(ranlib) $(phokharalib)
	$(lib_silent) cat /dev/null >$(phokharastamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(phokharalibname).$(shlibsuffix) :: $(phokharalib) $(phokharastamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" phokhara $(phokhara_shlibflags)

$(phokharashstamp) :: $(phokharalibname).$(shlibsuffix)
	@if test -f $(phokharalibname).$(shlibsuffix) ; then cat /dev/null >$(phokharashstamp) ; fi

phokharaclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)phokhara_9.1.o $(bin)ranlxd.o $(bin)ranlux_fort.o $(bin)sqampeemmg-1_5.o $(bin)sqampeemmg-2_5.o $(bin)sqampeemmg-3_5.o $(bin)sqampeemmg-4_5.o $(bin)sqampeemmg-5_5.o $(bin)sqampeemmg-main_5.o $(bin)sqampeemmg-wrap_5.o $(bin)sqampeemmg-1_isr.o $(bin)sqampeemmg-main_isr.o $(bin)sqampeemmg-wrap_isr.o $(bin)sqampeemmg-1_isrfsr.o $(bin)sqampeemmg-main_isrfsr.o $(bin)sqampeemmg-wrap_isrfsr.o $(bin)sqampeemmg-1_mix.o $(bin)sqampeemmg-main_mix.o $(bin)sqampeemmg-wrap_mix.o $(bin)ffabcd.o $(bin)ffxd0.o $(bin)ffcxs4.o $(bin)ff2dl2.o $(bin)ffcxr.o $(bin)ffcrr.o $(bin)ffxc0i.o $(bin)ffcxyz.o $(bin)ffcc0p.o $(bin)ffxd0h.o $(bin)ffcxs3.o $(bin)ffxd0i.o $(bin)ffdcxs.o $(bin)ffxc0p.o $(bin)ffdel4.o $(bin)ffxli2.o $(bin)ffxc0.o $(bin)ffcli2.o $(bin)ffdel3.o $(bin)ffcel2.o $(bin)ffxd0p.o $(bin)fftran.o $(bin)ffcel3.o $(bin)ffdxc0.o $(bin)ffxdbd.o $(bin)ffxxyz.o $(bin)ffinit_mine.o $(bin)ffdel2.o $(bin)ffdcc0.o $(bin)auxCD.o $(bin)qlbox10.o $(bin)qlbox11.o $(bin)qlbox12.o $(bin)qlbox13.o $(bin)qlbox14.o $(bin)qlbox15.o $(bin)qlbox16.o $(bin)qlbox1.o $(bin)qlbox2.o $(bin)qlbox3.o $(bin)qlbox4.o $(bin)qlbox5.o $(bin)qlbox6.o $(bin)qlbox7.o $(bin)qlbox8.o $(bin)qlbox9.o $(bin)qlcLi2omx2.o $(bin)qlcLi2omx3.o $(bin)qlfndd.o $(bin)qlfunctions.o $(bin)qlI1.o $(bin)qlI2.o $(bin)qlI2fin.o $(bin)qlI3.o $(bin)qlI3fin.o $(bin)qlI3sub.o $(bin)qlI4array.o $(bin)qlI4DNS41.o $(bin)qlI4.o $(bin)qlI4fin.o $(bin)qlI4sub0m.o $(bin)qlI4sub1m.o $(bin)qlI4sub2ma.o $(bin)qlI4sub2m.o $(bin)qlI4sub2mo.o $(bin)qlI4sub3m.o $(bin)qlinit.o $(bin)qlkfn.o $(bin)qlLi2omprod.o $(bin)qlLi2omrat.o $(bin)qlLi2omx2.o $(bin)qlLi2omx.o $(bin)qllnrat.o $(bin)qlratgam.o $(bin)qlratreal.o $(bin)qlsnglsort.o $(bin)qlspencer.o $(bin)qltri1.o $(bin)qltri2.o $(bin)qltri3.o $(bin)qltri4.o $(bin)qltri5.o $(bin)qltri6.o $(bin)qltrisort.o $(bin)qlxpicheck.o $(bin)qlYcalc.o $(bin)qlzero.o $(bin)qlddilog.o $(bin)pjfry.o $(bin)minor.o $(bin)integral.o $(bin)minoreval.o $(bin)cache.o $(bin)minorex.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
phokharainstallname = $(library_prefix)phokhara$(library_suffix).$(shlibsuffix)

phokhara :: phokharainstall

install :: phokharainstall

phokharainstall :: $(install_dir)/$(phokharainstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(phokharainstallname) :: $(bin)$(phokharainstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(phokharainstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(phokharainstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(phokharainstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(phokharainstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(phokharainstallname) $(install_dir)/$(phokharainstallname); \
	      echo `pwd`/$(phokharainstallname) >$(install_dir)/$(phokharainstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(phokharainstallname), no installation directory specified"; \
	  fi; \
	fi

phokharaclean :: phokharauninstall

uninstall :: phokharauninstall

phokharauninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(phokharainstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(phokharainstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(phokharainstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(phokharainstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)phokhara_dependencies.make :: dirs

ifndef QUICK
$(bin)phokhara_dependencies.make : $(src)phokhara_9.1.f $(src)ranlxd.c $(src)ranlux_fort.c $(src)eemmg-lib/eemmg5/sqampeemmg-1_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-2_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-3_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-4_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-5_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-main_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-wrap_5.f $(src)eemmg-lib/eemmgisr/sqampeemmg-1_isr.f $(src)eemmg-lib/eemmgisr/sqampeemmg-main_isr.f $(src)eemmg-lib/eemmgisr/sqampeemmg-wrap_isr.f $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-1_isrfsr.f $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-main_isrfsr.f $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-wrap_isrfsr.f $(src)eemmg-lib/eemmgmix/sqampeemmg-1_mix.f $(src)eemmg-lib/eemmgmix/sqampeemmg-main_mix.f $(src)eemmg-lib/eemmgmix/sqampeemmg-wrap_mix.f $(src)eemmg-lib/ff/ffabcd.f $(src)eemmg-lib/ff/ffxd0.f $(src)eemmg-lib/ff/ffcxs4.f $(src)eemmg-lib/ff/ff2dl2.f $(src)eemmg-lib/ff/ffcxr.f $(src)eemmg-lib/ff/ffcrr.f $(src)eemmg-lib/ff/ffxc0i.f $(src)eemmg-lib/ff/ffcxyz.f $(src)eemmg-lib/ff/ffcc0p.f $(src)eemmg-lib/ff/ffxd0h.f $(src)eemmg-lib/ff/ffcxs3.f $(src)eemmg-lib/ff/ffxd0i.f $(src)eemmg-lib/ff/ffdcxs.f $(src)eemmg-lib/ff/ffxc0p.f $(src)eemmg-lib/ff/ffdel4.f $(src)eemmg-lib/ff/ffxli2.f $(src)eemmg-lib/ff/ffxc0.f $(src)eemmg-lib/ff/ffcli2.f $(src)eemmg-lib/ff/ffdel3.f $(src)eemmg-lib/ff/ffcel2.f $(src)eemmg-lib/ff/ffxd0p.f $(src)eemmg-lib/ff/fftran.f $(src)eemmg-lib/ff/ffcel3.f $(src)eemmg-lib/ff/ffdxc0.f $(src)eemmg-lib/ff/ffxdbd.f $(src)eemmg-lib/ff/ffxxyz.f $(src)eemmg-lib/ff/ffinit_mine.f $(src)eemmg-lib/ff/ffdel2.f $(src)eemmg-lib/ff/ffdcc0.f $(src)eemmg-lib/ql/auxCD.f $(src)eemmg-lib/ql/qlbox10.f $(src)eemmg-lib/ql/qlbox11.f $(src)eemmg-lib/ql/qlbox12.f $(src)eemmg-lib/ql/qlbox13.f $(src)eemmg-lib/ql/qlbox14.f $(src)eemmg-lib/ql/qlbox15.f $(src)eemmg-lib/ql/qlbox16.f $(src)eemmg-lib/ql/qlbox1.f $(src)eemmg-lib/ql/qlbox2.f $(src)eemmg-lib/ql/qlbox3.f $(src)eemmg-lib/ql/qlbox4.f $(src)eemmg-lib/ql/qlbox5.f $(src)eemmg-lib/ql/qlbox6.f $(src)eemmg-lib/ql/qlbox7.f $(src)eemmg-lib/ql/qlbox8.f $(src)eemmg-lib/ql/qlbox9.f $(src)eemmg-lib/ql/qlcLi2omx2.f $(src)eemmg-lib/ql/qlcLi2omx3.f $(src)eemmg-lib/ql/qlfndd.f $(src)eemmg-lib/ql/qlfunctions.f $(src)eemmg-lib/ql/qlI1.f $(src)eemmg-lib/ql/qlI2.f $(src)eemmg-lib/ql/qlI2fin.f $(src)eemmg-lib/ql/qlI3.f $(src)eemmg-lib/ql/qlI3fin.f $(src)eemmg-lib/ql/qlI3sub.f $(src)eemmg-lib/ql/qlI4array.f $(src)eemmg-lib/ql/qlI4DNS41.f $(src)eemmg-lib/ql/qlI4.f $(src)eemmg-lib/ql/qlI4fin.f $(src)eemmg-lib/ql/qlI4sub0m.f $(src)eemmg-lib/ql/qlI4sub1m.f $(src)eemmg-lib/ql/qlI4sub2ma.f $(src)eemmg-lib/ql/qlI4sub2m.f $(src)eemmg-lib/ql/qlI4sub2mo.f $(src)eemmg-lib/ql/qlI4sub3m.f $(src)eemmg-lib/ql/qlinit.f $(src)eemmg-lib/ql/qlkfn.f $(src)eemmg-lib/ql/qlLi2omprod.f $(src)eemmg-lib/ql/qlLi2omrat.f $(src)eemmg-lib/ql/qlLi2omx2.f $(src)eemmg-lib/ql/qlLi2omx.f $(src)eemmg-lib/ql/qllnrat.f $(src)eemmg-lib/ql/qlratgam.f $(src)eemmg-lib/ql/qlratreal.f $(src)eemmg-lib/ql/qlsnglsort.f $(src)eemmg-lib/ql/qlspencer.f $(src)eemmg-lib/ql/qltri1.f $(src)eemmg-lib/ql/qltri2.f $(src)eemmg-lib/ql/qltri3.f $(src)eemmg-lib/ql/qltri4.f $(src)eemmg-lib/ql/qltri5.f $(src)eemmg-lib/ql/qltri6.f $(src)eemmg-lib/ql/qltrisort.f $(src)eemmg-lib/ql/qlxpicheck.f $(src)eemmg-lib/ql/qlYcalc.f $(src)eemmg-lib/ql/qlzero.f $(src)eemmg-lib/ql/qlddilog.f $(src)eemmg-lib/src/pjfry.cpp $(src)eemmg-lib/src/minor.cpp $(src)eemmg-lib/src/integral.cpp $(src)eemmg-lib/src/minoreval.cpp $(src)eemmg-lib/src/cache.cpp $(src)eemmg-lib/src/minorex.cpp $(use_requirements) $(cmt_final_setup_phokhara)
	$(echo) "(phokhara.make) Rebuilding $@"; \
	  $(build_dependencies) phokhara -all_sources -out=$@ $(src)phokhara_9.1.f $(src)ranlxd.c $(src)ranlux_fort.c $(src)eemmg-lib/eemmg5/sqampeemmg-1_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-2_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-3_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-4_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-5_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-main_5.f $(src)eemmg-lib/eemmg5/sqampeemmg-wrap_5.f $(src)eemmg-lib/eemmgisr/sqampeemmg-1_isr.f $(src)eemmg-lib/eemmgisr/sqampeemmg-main_isr.f $(src)eemmg-lib/eemmgisr/sqampeemmg-wrap_isr.f $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-1_isrfsr.f $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-main_isrfsr.f $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-wrap_isrfsr.f $(src)eemmg-lib/eemmgmix/sqampeemmg-1_mix.f $(src)eemmg-lib/eemmgmix/sqampeemmg-main_mix.f $(src)eemmg-lib/eemmgmix/sqampeemmg-wrap_mix.f $(src)eemmg-lib/ff/ffabcd.f $(src)eemmg-lib/ff/ffxd0.f $(src)eemmg-lib/ff/ffcxs4.f $(src)eemmg-lib/ff/ff2dl2.f $(src)eemmg-lib/ff/ffcxr.f $(src)eemmg-lib/ff/ffcrr.f $(src)eemmg-lib/ff/ffxc0i.f $(src)eemmg-lib/ff/ffcxyz.f $(src)eemmg-lib/ff/ffcc0p.f $(src)eemmg-lib/ff/ffxd0h.f $(src)eemmg-lib/ff/ffcxs3.f $(src)eemmg-lib/ff/ffxd0i.f $(src)eemmg-lib/ff/ffdcxs.f $(src)eemmg-lib/ff/ffxc0p.f $(src)eemmg-lib/ff/ffdel4.f $(src)eemmg-lib/ff/ffxli2.f $(src)eemmg-lib/ff/ffxc0.f $(src)eemmg-lib/ff/ffcli2.f $(src)eemmg-lib/ff/ffdel3.f $(src)eemmg-lib/ff/ffcel2.f $(src)eemmg-lib/ff/ffxd0p.f $(src)eemmg-lib/ff/fftran.f $(src)eemmg-lib/ff/ffcel3.f $(src)eemmg-lib/ff/ffdxc0.f $(src)eemmg-lib/ff/ffxdbd.f $(src)eemmg-lib/ff/ffxxyz.f $(src)eemmg-lib/ff/ffinit_mine.f $(src)eemmg-lib/ff/ffdel2.f $(src)eemmg-lib/ff/ffdcc0.f $(src)eemmg-lib/ql/auxCD.f $(src)eemmg-lib/ql/qlbox10.f $(src)eemmg-lib/ql/qlbox11.f $(src)eemmg-lib/ql/qlbox12.f $(src)eemmg-lib/ql/qlbox13.f $(src)eemmg-lib/ql/qlbox14.f $(src)eemmg-lib/ql/qlbox15.f $(src)eemmg-lib/ql/qlbox16.f $(src)eemmg-lib/ql/qlbox1.f $(src)eemmg-lib/ql/qlbox2.f $(src)eemmg-lib/ql/qlbox3.f $(src)eemmg-lib/ql/qlbox4.f $(src)eemmg-lib/ql/qlbox5.f $(src)eemmg-lib/ql/qlbox6.f $(src)eemmg-lib/ql/qlbox7.f $(src)eemmg-lib/ql/qlbox8.f $(src)eemmg-lib/ql/qlbox9.f $(src)eemmg-lib/ql/qlcLi2omx2.f $(src)eemmg-lib/ql/qlcLi2omx3.f $(src)eemmg-lib/ql/qlfndd.f $(src)eemmg-lib/ql/qlfunctions.f $(src)eemmg-lib/ql/qlI1.f $(src)eemmg-lib/ql/qlI2.f $(src)eemmg-lib/ql/qlI2fin.f $(src)eemmg-lib/ql/qlI3.f $(src)eemmg-lib/ql/qlI3fin.f $(src)eemmg-lib/ql/qlI3sub.f $(src)eemmg-lib/ql/qlI4array.f $(src)eemmg-lib/ql/qlI4DNS41.f $(src)eemmg-lib/ql/qlI4.f $(src)eemmg-lib/ql/qlI4fin.f $(src)eemmg-lib/ql/qlI4sub0m.f $(src)eemmg-lib/ql/qlI4sub1m.f $(src)eemmg-lib/ql/qlI4sub2ma.f $(src)eemmg-lib/ql/qlI4sub2m.f $(src)eemmg-lib/ql/qlI4sub2mo.f $(src)eemmg-lib/ql/qlI4sub3m.f $(src)eemmg-lib/ql/qlinit.f $(src)eemmg-lib/ql/qlkfn.f $(src)eemmg-lib/ql/qlLi2omprod.f $(src)eemmg-lib/ql/qlLi2omrat.f $(src)eemmg-lib/ql/qlLi2omx2.f $(src)eemmg-lib/ql/qlLi2omx.f $(src)eemmg-lib/ql/qllnrat.f $(src)eemmg-lib/ql/qlratgam.f $(src)eemmg-lib/ql/qlratreal.f $(src)eemmg-lib/ql/qlsnglsort.f $(src)eemmg-lib/ql/qlspencer.f $(src)eemmg-lib/ql/qltri1.f $(src)eemmg-lib/ql/qltri2.f $(src)eemmg-lib/ql/qltri3.f $(src)eemmg-lib/ql/qltri4.f $(src)eemmg-lib/ql/qltri5.f $(src)eemmg-lib/ql/qltri6.f $(src)eemmg-lib/ql/qltrisort.f $(src)eemmg-lib/ql/qlxpicheck.f $(src)eemmg-lib/ql/qlYcalc.f $(src)eemmg-lib/ql/qlzero.f $(src)eemmg-lib/ql/qlddilog.f $(src)eemmg-lib/src/pjfry.cpp $(src)eemmg-lib/src/minor.cpp $(src)eemmg-lib/src/integral.cpp $(src)eemmg-lib/src/minoreval.cpp $(src)eemmg-lib/src/cache.cpp $(src)eemmg-lib/src/minorex.cpp
endif

#$(phokhara_dependencies)

-include $(bin)phokhara_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(phokhara_9.1_f_dependencies)
 
$(bin)$(binobj)phokhara_9.1.o : $(phokhara_9.1_f_dependencies)
	$(fortran_echo) $(src)phokhara_9.1.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(phokhara_9.1_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(phokhara_9.1_fflags) $(phokhara_9.1_f_fflags)  $(src)phokhara_9.1.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)phokhara_9.1.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(phokhara_9.1_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(phokhara_9.1_fflags) $(phokhara_9.1_f_fflags)  $(src)phokhara_9.1.f

#-- end of fortran_library ------
#-- start of c_library ------

ifneq ($(cdepflags),)

ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ranlxd.d

$(bin)$(binobj)ranlxd.d : $(use_requirements) $(cmt_final_setup_phokhara)

$(bin)$(binobj)ranlxd.d : $(src)ranlxd.c
	$(dep_echo) $@
	$(c_silent) $(ccomp) $(cdepflags) -o $(@D)/ranlxd.dep $(use_pp_cflags) $(phokhara_pp_cflags) $(lib_phokhara_pp_cflags) $(ranlxd_pp_cflags) $(use_cflags) $(phokhara_cflags) $(lib_phokhara_cflags) $(ranlxd_cflags) $(ranlxd_c_cflags)  $(src)ranlxd.c
	$(c_silent) $(format_dependencies) $@ $(@D)/ranlxd.o $(src)ranlxd.c $(@D)/ranlxd.dep
endif
endif

$(bin)$(binobj)ranlxd.o : $(src)ranlxd.c
else
$(bin)phokhara_dependencies.make : $(ranlxd_c_dependencies)

$(bin)$(binobj)ranlxd.o : $(ranlxd_c_dependencies)
endif
	$(c_echo) $(src)ranlxd.c
	$(c_silent) $(ccomp) -o $@ $(use_pp_cflags) $(phokhara_pp_cflags) $(lib_phokhara_pp_cflags) $(ranlxd_pp_cflags) $(use_cflags) $(phokhara_cflags) $(lib_phokhara_cflags) $(ranlxd_cflags) $(ranlxd_c_cflags)  $(src)ranlxd.c

#-- end of c_library ------
#-- start of c_library ------

ifneq ($(cdepflags),)

ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ranlux_fort.d

$(bin)$(binobj)ranlux_fort.d : $(use_requirements) $(cmt_final_setup_phokhara)

$(bin)$(binobj)ranlux_fort.d : $(src)ranlux_fort.c
	$(dep_echo) $@
	$(c_silent) $(ccomp) $(cdepflags) -o $(@D)/ranlux_fort.dep $(use_pp_cflags) $(phokhara_pp_cflags) $(lib_phokhara_pp_cflags) $(ranlux_fort_pp_cflags) $(use_cflags) $(phokhara_cflags) $(lib_phokhara_cflags) $(ranlux_fort_cflags) $(ranlux_fort_c_cflags)  $(src)ranlux_fort.c
	$(c_silent) $(format_dependencies) $@ $(@D)/ranlux_fort.o $(src)ranlux_fort.c $(@D)/ranlux_fort.dep
endif
endif

$(bin)$(binobj)ranlux_fort.o : $(src)ranlux_fort.c
else
$(bin)phokhara_dependencies.make : $(ranlux_fort_c_dependencies)

$(bin)$(binobj)ranlux_fort.o : $(ranlux_fort_c_dependencies)
endif
	$(c_echo) $(src)ranlux_fort.c
	$(c_silent) $(ccomp) -o $@ $(use_pp_cflags) $(phokhara_pp_cflags) $(lib_phokhara_pp_cflags) $(ranlux_fort_pp_cflags) $(use_cflags) $(phokhara_cflags) $(lib_phokhara_cflags) $(ranlux_fort_cflags) $(ranlux_fort_c_cflags)  $(src)ranlux_fort.c

#-- end of c_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-1_5_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-1_5.o : $(sqampeemmg-1_5_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmg5/sqampeemmg-1_5.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-1_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-1_5_fflags) $(sqampeemmg-1_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-1_5.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-1_5.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-1_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-1_5_fflags) $(sqampeemmg-1_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-1_5.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-2_5_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-2_5.o : $(sqampeemmg-2_5_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmg5/sqampeemmg-2_5.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-2_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-2_5_fflags) $(sqampeemmg-2_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-2_5.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-2_5.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-2_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-2_5_fflags) $(sqampeemmg-2_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-2_5.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-3_5_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-3_5.o : $(sqampeemmg-3_5_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmg5/sqampeemmg-3_5.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-3_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-3_5_fflags) $(sqampeemmg-3_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-3_5.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-3_5.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-3_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-3_5_fflags) $(sqampeemmg-3_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-3_5.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-4_5_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-4_5.o : $(sqampeemmg-4_5_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmg5/sqampeemmg-4_5.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-4_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-4_5_fflags) $(sqampeemmg-4_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-4_5.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-4_5.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-4_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-4_5_fflags) $(sqampeemmg-4_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-4_5.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-5_5_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-5_5.o : $(sqampeemmg-5_5_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmg5/sqampeemmg-5_5.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-5_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-5_5_fflags) $(sqampeemmg-5_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-5_5.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-5_5.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-5_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-5_5_fflags) $(sqampeemmg-5_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-5_5.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-main_5_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-main_5.o : $(sqampeemmg-main_5_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmg5/sqampeemmg-main_5.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-main_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-main_5_fflags) $(sqampeemmg-main_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-main_5.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-main_5.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-main_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-main_5_fflags) $(sqampeemmg-main_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-main_5.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-wrap_5_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-wrap_5.o : $(sqampeemmg-wrap_5_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmg5/sqampeemmg-wrap_5.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-wrap_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-wrap_5_fflags) $(sqampeemmg-wrap_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-wrap_5.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-wrap_5.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-wrap_5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-wrap_5_fflags) $(sqampeemmg-wrap_5_f_fflags) $(ppcmd)../src/eemmg-lib/eemmg5 $(src)eemmg-lib/eemmg5/sqampeemmg-wrap_5.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-1_isr_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-1_isr.o : $(sqampeemmg-1_isr_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmgisr/sqampeemmg-1_isr.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-1_isr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-1_isr_fflags) $(sqampeemmg-1_isr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisr $(src)eemmg-lib/eemmgisr/sqampeemmg-1_isr.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-1_isr.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-1_isr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-1_isr_fflags) $(sqampeemmg-1_isr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisr $(src)eemmg-lib/eemmgisr/sqampeemmg-1_isr.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-main_isr_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-main_isr.o : $(sqampeemmg-main_isr_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmgisr/sqampeemmg-main_isr.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-main_isr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-main_isr_fflags) $(sqampeemmg-main_isr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisr $(src)eemmg-lib/eemmgisr/sqampeemmg-main_isr.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-main_isr.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-main_isr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-main_isr_fflags) $(sqampeemmg-main_isr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisr $(src)eemmg-lib/eemmgisr/sqampeemmg-main_isr.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-wrap_isr_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-wrap_isr.o : $(sqampeemmg-wrap_isr_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmgisr/sqampeemmg-wrap_isr.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-wrap_isr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-wrap_isr_fflags) $(sqampeemmg-wrap_isr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisr $(src)eemmg-lib/eemmgisr/sqampeemmg-wrap_isr.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-wrap_isr.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-wrap_isr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-wrap_isr_fflags) $(sqampeemmg-wrap_isr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisr $(src)eemmg-lib/eemmgisr/sqampeemmg-wrap_isr.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-1_isrfsr_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-1_isrfsr.o : $(sqampeemmg-1_isrfsr_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-1_isrfsr.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-1_isrfsr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-1_isrfsr_fflags) $(sqampeemmg-1_isrfsr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisrfsr $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-1_isrfsr.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-1_isrfsr.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-1_isrfsr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-1_isrfsr_fflags) $(sqampeemmg-1_isrfsr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisrfsr $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-1_isrfsr.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-main_isrfsr_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-main_isrfsr.o : $(sqampeemmg-main_isrfsr_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-main_isrfsr.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-main_isrfsr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-main_isrfsr_fflags) $(sqampeemmg-main_isrfsr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisrfsr $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-main_isrfsr.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-main_isrfsr.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-main_isrfsr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-main_isrfsr_fflags) $(sqampeemmg-main_isrfsr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisrfsr $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-main_isrfsr.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-wrap_isrfsr_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-wrap_isrfsr.o : $(sqampeemmg-wrap_isrfsr_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-wrap_isrfsr.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-wrap_isrfsr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-wrap_isrfsr_fflags) $(sqampeemmg-wrap_isrfsr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisrfsr $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-wrap_isrfsr.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-wrap_isrfsr.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-wrap_isrfsr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-wrap_isrfsr_fflags) $(sqampeemmg-wrap_isrfsr_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgisrfsr $(src)eemmg-lib/eemmgisrfsr/sqampeemmg-wrap_isrfsr.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-1_mix_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-1_mix.o : $(sqampeemmg-1_mix_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmgmix/sqampeemmg-1_mix.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-1_mix_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-1_mix_fflags) $(sqampeemmg-1_mix_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgmix $(src)eemmg-lib/eemmgmix/sqampeemmg-1_mix.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-1_mix.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-1_mix_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-1_mix_fflags) $(sqampeemmg-1_mix_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgmix $(src)eemmg-lib/eemmgmix/sqampeemmg-1_mix.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-main_mix_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-main_mix.o : $(sqampeemmg-main_mix_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmgmix/sqampeemmg-main_mix.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-main_mix_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-main_mix_fflags) $(sqampeemmg-main_mix_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgmix $(src)eemmg-lib/eemmgmix/sqampeemmg-main_mix.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-main_mix.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-main_mix_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-main_mix_fflags) $(sqampeemmg-main_mix_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgmix $(src)eemmg-lib/eemmgmix/sqampeemmg-main_mix.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(sqampeemmg-wrap_mix_f_dependencies)
 
$(bin)$(binobj)sqampeemmg-wrap_mix.o : $(sqampeemmg-wrap_mix_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/eemmgmix/sqampeemmg-wrap_mix.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-wrap_mix_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-wrap_mix_fflags) $(sqampeemmg-wrap_mix_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgmix $(src)eemmg-lib/eemmgmix/sqampeemmg-wrap_mix.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sqampeemmg-wrap_mix.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(sqampeemmg-wrap_mix_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(sqampeemmg-wrap_mix_fflags) $(sqampeemmg-wrap_mix_f_fflags) $(ppcmd)../src/eemmg-lib/eemmgmix $(src)eemmg-lib/eemmgmix/sqampeemmg-wrap_mix.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffabcd_f_dependencies)
 
$(bin)$(binobj)ffabcd.o : $(ffabcd_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffabcd.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffabcd_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffabcd_fflags) $(ffabcd_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffabcd.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffabcd.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffabcd_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffabcd_fflags) $(ffabcd_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffabcd.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffxd0_f_dependencies)
 
$(bin)$(binobj)ffxd0.o : $(ffxd0_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffxd0.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxd0_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxd0_fflags) $(ffxd0_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxd0.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffxd0.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxd0_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxd0_fflags) $(ffxd0_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxd0.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffcxs4_f_dependencies)
 
$(bin)$(binobj)ffcxs4.o : $(ffcxs4_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffcxs4.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcxs4_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcxs4_fflags) $(ffcxs4_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcxs4.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffcxs4.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcxs4_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcxs4_fflags) $(ffcxs4_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcxs4.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ff2dl2_f_dependencies)
 
$(bin)$(binobj)ff2dl2.o : $(ff2dl2_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ff2dl2.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ff2dl2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ff2dl2_fflags) $(ff2dl2_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ff2dl2.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ff2dl2.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ff2dl2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ff2dl2_fflags) $(ff2dl2_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ff2dl2.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffcxr_f_dependencies)
 
$(bin)$(binobj)ffcxr.o : $(ffcxr_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffcxr.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcxr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcxr_fflags) $(ffcxr_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcxr.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffcxr.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcxr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcxr_fflags) $(ffcxr_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcxr.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffcrr_f_dependencies)
 
$(bin)$(binobj)ffcrr.o : $(ffcrr_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffcrr.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcrr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcrr_fflags) $(ffcrr_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcrr.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffcrr.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcrr_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcrr_fflags) $(ffcrr_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcrr.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffxc0i_f_dependencies)
 
$(bin)$(binobj)ffxc0i.o : $(ffxc0i_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffxc0i.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxc0i_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxc0i_fflags) $(ffxc0i_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxc0i.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffxc0i.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxc0i_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxc0i_fflags) $(ffxc0i_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxc0i.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffcxyz_f_dependencies)
 
$(bin)$(binobj)ffcxyz.o : $(ffcxyz_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffcxyz.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcxyz_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcxyz_fflags) $(ffcxyz_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcxyz.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffcxyz.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcxyz_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcxyz_fflags) $(ffcxyz_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcxyz.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffcc0p_f_dependencies)
 
$(bin)$(binobj)ffcc0p.o : $(ffcc0p_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffcc0p.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcc0p_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcc0p_fflags) $(ffcc0p_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcc0p.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffcc0p.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcc0p_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcc0p_fflags) $(ffcc0p_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcc0p.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffxd0h_f_dependencies)
 
$(bin)$(binobj)ffxd0h.o : $(ffxd0h_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffxd0h.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxd0h_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxd0h_fflags) $(ffxd0h_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxd0h.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffxd0h.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxd0h_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxd0h_fflags) $(ffxd0h_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxd0h.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffcxs3_f_dependencies)
 
$(bin)$(binobj)ffcxs3.o : $(ffcxs3_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffcxs3.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcxs3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcxs3_fflags) $(ffcxs3_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcxs3.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffcxs3.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcxs3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcxs3_fflags) $(ffcxs3_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcxs3.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffxd0i_f_dependencies)
 
$(bin)$(binobj)ffxd0i.o : $(ffxd0i_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffxd0i.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxd0i_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxd0i_fflags) $(ffxd0i_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxd0i.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffxd0i.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxd0i_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxd0i_fflags) $(ffxd0i_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxd0i.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffdcxs_f_dependencies)
 
$(bin)$(binobj)ffdcxs.o : $(ffdcxs_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffdcxs.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdcxs_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdcxs_fflags) $(ffdcxs_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdcxs.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffdcxs.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdcxs_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdcxs_fflags) $(ffdcxs_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdcxs.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffxc0p_f_dependencies)
 
$(bin)$(binobj)ffxc0p.o : $(ffxc0p_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffxc0p.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxc0p_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxc0p_fflags) $(ffxc0p_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxc0p.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffxc0p.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxc0p_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxc0p_fflags) $(ffxc0p_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxc0p.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffdel4_f_dependencies)
 
$(bin)$(binobj)ffdel4.o : $(ffdel4_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffdel4.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdel4_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdel4_fflags) $(ffdel4_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdel4.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffdel4.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdel4_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdel4_fflags) $(ffdel4_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdel4.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffxli2_f_dependencies)
 
$(bin)$(binobj)ffxli2.o : $(ffxli2_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffxli2.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxli2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxli2_fflags) $(ffxli2_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxli2.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffxli2.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxli2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxli2_fflags) $(ffxli2_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxli2.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffxc0_f_dependencies)
 
$(bin)$(binobj)ffxc0.o : $(ffxc0_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffxc0.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxc0_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxc0_fflags) $(ffxc0_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxc0.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffxc0.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxc0_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxc0_fflags) $(ffxc0_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxc0.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffcli2_f_dependencies)
 
$(bin)$(binobj)ffcli2.o : $(ffcli2_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffcli2.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcli2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcli2_fflags) $(ffcli2_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcli2.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffcli2.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcli2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcli2_fflags) $(ffcli2_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcli2.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffdel3_f_dependencies)
 
$(bin)$(binobj)ffdel3.o : $(ffdel3_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffdel3.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdel3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdel3_fflags) $(ffdel3_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdel3.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffdel3.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdel3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdel3_fflags) $(ffdel3_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdel3.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffcel2_f_dependencies)
 
$(bin)$(binobj)ffcel2.o : $(ffcel2_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffcel2.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcel2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcel2_fflags) $(ffcel2_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcel2.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffcel2.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcel2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcel2_fflags) $(ffcel2_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcel2.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffxd0p_f_dependencies)
 
$(bin)$(binobj)ffxd0p.o : $(ffxd0p_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffxd0p.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxd0p_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxd0p_fflags) $(ffxd0p_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxd0p.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffxd0p.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxd0p_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxd0p_fflags) $(ffxd0p_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxd0p.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(fftran_f_dependencies)
 
$(bin)$(binobj)fftran.o : $(fftran_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/fftran.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(fftran_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(fftran_fflags) $(fftran_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/fftran.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)fftran.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(fftran_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(fftran_fflags) $(fftran_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/fftran.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffcel3_f_dependencies)
 
$(bin)$(binobj)ffcel3.o : $(ffcel3_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffcel3.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcel3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcel3_fflags) $(ffcel3_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcel3.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffcel3.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffcel3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffcel3_fflags) $(ffcel3_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffcel3.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffdxc0_f_dependencies)
 
$(bin)$(binobj)ffdxc0.o : $(ffdxc0_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffdxc0.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdxc0_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdxc0_fflags) $(ffdxc0_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdxc0.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffdxc0.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdxc0_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdxc0_fflags) $(ffdxc0_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdxc0.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffxdbd_f_dependencies)
 
$(bin)$(binobj)ffxdbd.o : $(ffxdbd_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffxdbd.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxdbd_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxdbd_fflags) $(ffxdbd_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxdbd.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffxdbd.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxdbd_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxdbd_fflags) $(ffxdbd_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxdbd.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffxxyz_f_dependencies)
 
$(bin)$(binobj)ffxxyz.o : $(ffxxyz_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffxxyz.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxxyz_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxxyz_fflags) $(ffxxyz_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxxyz.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffxxyz.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffxxyz_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffxxyz_fflags) $(ffxxyz_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffxxyz.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffinit_mine_f_dependencies)
 
$(bin)$(binobj)ffinit_mine.o : $(ffinit_mine_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffinit_mine.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffinit_mine_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffinit_mine_fflags) $(ffinit_mine_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffinit_mine.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffinit_mine.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffinit_mine_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffinit_mine_fflags) $(ffinit_mine_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffinit_mine.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffdel2_f_dependencies)
 
$(bin)$(binobj)ffdel2.o : $(ffdel2_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffdel2.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdel2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdel2_fflags) $(ffdel2_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdel2.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffdel2.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdel2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdel2_fflags) $(ffdel2_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdel2.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(ffdcc0_f_dependencies)
 
$(bin)$(binobj)ffdcc0.o : $(ffdcc0_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ff/ffdcc0.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdcc0_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdcc0_fflags) $(ffdcc0_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdcc0.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)ffdcc0.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(ffdcc0_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(ffdcc0_fflags) $(ffdcc0_f_fflags) $(ppcmd)../src/eemmg-lib/ff $(src)eemmg-lib/ff/ffdcc0.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(auxCD_f_dependencies)
 
$(bin)$(binobj)auxCD.o : $(auxCD_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/auxCD.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(auxCD_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(auxCD_fflags) $(auxCD_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/auxCD.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)auxCD.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(auxCD_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(auxCD_fflags) $(auxCD_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/auxCD.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox10_f_dependencies)
 
$(bin)$(binobj)qlbox10.o : $(qlbox10_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox10.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox10_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox10_fflags) $(qlbox10_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox10.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox10.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox10_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox10_fflags) $(qlbox10_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox10.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox11_f_dependencies)
 
$(bin)$(binobj)qlbox11.o : $(qlbox11_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox11.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox11_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox11_fflags) $(qlbox11_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox11.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox11.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox11_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox11_fflags) $(qlbox11_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox11.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox12_f_dependencies)
 
$(bin)$(binobj)qlbox12.o : $(qlbox12_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox12.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox12_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox12_fflags) $(qlbox12_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox12.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox12.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox12_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox12_fflags) $(qlbox12_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox12.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox13_f_dependencies)
 
$(bin)$(binobj)qlbox13.o : $(qlbox13_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox13.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox13_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox13_fflags) $(qlbox13_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox13.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox13.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox13_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox13_fflags) $(qlbox13_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox13.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox14_f_dependencies)
 
$(bin)$(binobj)qlbox14.o : $(qlbox14_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox14.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox14_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox14_fflags) $(qlbox14_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox14.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox14.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox14_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox14_fflags) $(qlbox14_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox14.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox15_f_dependencies)
 
$(bin)$(binobj)qlbox15.o : $(qlbox15_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox15.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox15_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox15_fflags) $(qlbox15_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox15.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox15.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox15_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox15_fflags) $(qlbox15_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox15.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox16_f_dependencies)
 
$(bin)$(binobj)qlbox16.o : $(qlbox16_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox16.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox16_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox16_fflags) $(qlbox16_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox16.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox16.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox16_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox16_fflags) $(qlbox16_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox16.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox1_f_dependencies)
 
$(bin)$(binobj)qlbox1.o : $(qlbox1_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox1.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox1_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox1_fflags) $(qlbox1_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox1.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox1.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox1_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox1_fflags) $(qlbox1_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox1.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox2_f_dependencies)
 
$(bin)$(binobj)qlbox2.o : $(qlbox2_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox2.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox2_fflags) $(qlbox2_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox2.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox2.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox2_fflags) $(qlbox2_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox2.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox3_f_dependencies)
 
$(bin)$(binobj)qlbox3.o : $(qlbox3_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox3.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox3_fflags) $(qlbox3_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox3.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox3.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox3_fflags) $(qlbox3_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox3.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox4_f_dependencies)
 
$(bin)$(binobj)qlbox4.o : $(qlbox4_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox4.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox4_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox4_fflags) $(qlbox4_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox4.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox4.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox4_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox4_fflags) $(qlbox4_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox4.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox5_f_dependencies)
 
$(bin)$(binobj)qlbox5.o : $(qlbox5_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox5.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox5_fflags) $(qlbox5_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox5.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox5.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox5_fflags) $(qlbox5_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox5.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox6_f_dependencies)
 
$(bin)$(binobj)qlbox6.o : $(qlbox6_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox6.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox6_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox6_fflags) $(qlbox6_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox6.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox6.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox6_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox6_fflags) $(qlbox6_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox6.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox7_f_dependencies)
 
$(bin)$(binobj)qlbox7.o : $(qlbox7_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox7.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox7_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox7_fflags) $(qlbox7_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox7.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox7.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox7_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox7_fflags) $(qlbox7_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox7.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox8_f_dependencies)
 
$(bin)$(binobj)qlbox8.o : $(qlbox8_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox8.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox8_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox8_fflags) $(qlbox8_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox8.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox8.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox8_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox8_fflags) $(qlbox8_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox8.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlbox9_f_dependencies)
 
$(bin)$(binobj)qlbox9.o : $(qlbox9_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlbox9.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox9_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox9_fflags) $(qlbox9_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox9.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlbox9.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlbox9_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlbox9_fflags) $(qlbox9_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlbox9.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlcLi2omx2_f_dependencies)
 
$(bin)$(binobj)qlcLi2omx2.o : $(qlcLi2omx2_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlcLi2omx2.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlcLi2omx2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlcLi2omx2_fflags) $(qlcLi2omx2_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlcLi2omx2.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlcLi2omx2.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlcLi2omx2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlcLi2omx2_fflags) $(qlcLi2omx2_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlcLi2omx2.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlcLi2omx3_f_dependencies)
 
$(bin)$(binobj)qlcLi2omx3.o : $(qlcLi2omx3_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlcLi2omx3.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlcLi2omx3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlcLi2omx3_fflags) $(qlcLi2omx3_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlcLi2omx3.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlcLi2omx3.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlcLi2omx3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlcLi2omx3_fflags) $(qlcLi2omx3_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlcLi2omx3.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlfndd_f_dependencies)
 
$(bin)$(binobj)qlfndd.o : $(qlfndd_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlfndd.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlfndd_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlfndd_fflags) $(qlfndd_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlfndd.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlfndd.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlfndd_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlfndd_fflags) $(qlfndd_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlfndd.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlfunctions_f_dependencies)
 
$(bin)$(binobj)qlfunctions.o : $(qlfunctions_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlfunctions.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlfunctions_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlfunctions_fflags) $(qlfunctions_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlfunctions.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlfunctions.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlfunctions_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlfunctions_fflags) $(qlfunctions_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlfunctions.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI1_f_dependencies)
 
$(bin)$(binobj)qlI1.o : $(qlI1_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI1.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI1_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI1_fflags) $(qlI1_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI1.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI1.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI1_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI1_fflags) $(qlI1_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI1.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI2_f_dependencies)
 
$(bin)$(binobj)qlI2.o : $(qlI2_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI2.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI2_fflags) $(qlI2_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI2.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI2.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI2_fflags) $(qlI2_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI2.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI2fin_f_dependencies)
 
$(bin)$(binobj)qlI2fin.o : $(qlI2fin_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI2fin.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI2fin_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI2fin_fflags) $(qlI2fin_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI2fin.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI2fin.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI2fin_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI2fin_fflags) $(qlI2fin_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI2fin.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI3_f_dependencies)
 
$(bin)$(binobj)qlI3.o : $(qlI3_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI3.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI3_fflags) $(qlI3_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI3.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI3.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI3_fflags) $(qlI3_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI3.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI3fin_f_dependencies)
 
$(bin)$(binobj)qlI3fin.o : $(qlI3fin_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI3fin.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI3fin_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI3fin_fflags) $(qlI3fin_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI3fin.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI3fin.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI3fin_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI3fin_fflags) $(qlI3fin_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI3fin.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI3sub_f_dependencies)
 
$(bin)$(binobj)qlI3sub.o : $(qlI3sub_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI3sub.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI3sub_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI3sub_fflags) $(qlI3sub_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI3sub.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI3sub.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI3sub_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI3sub_fflags) $(qlI3sub_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI3sub.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI4array_f_dependencies)
 
$(bin)$(binobj)qlI4array.o : $(qlI4array_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI4array.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4array_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4array_fflags) $(qlI4array_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4array.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI4array.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4array_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4array_fflags) $(qlI4array_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4array.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI4DNS41_f_dependencies)
 
$(bin)$(binobj)qlI4DNS41.o : $(qlI4DNS41_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI4DNS41.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4DNS41_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4DNS41_fflags) $(qlI4DNS41_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4DNS41.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI4DNS41.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4DNS41_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4DNS41_fflags) $(qlI4DNS41_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4DNS41.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI4_f_dependencies)
 
$(bin)$(binobj)qlI4.o : $(qlI4_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI4.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4_fflags) $(qlI4_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI4.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4_fflags) $(qlI4_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI4fin_f_dependencies)
 
$(bin)$(binobj)qlI4fin.o : $(qlI4fin_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI4fin.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4fin_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4fin_fflags) $(qlI4fin_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4fin.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI4fin.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4fin_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4fin_fflags) $(qlI4fin_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4fin.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI4sub0m_f_dependencies)
 
$(bin)$(binobj)qlI4sub0m.o : $(qlI4sub0m_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI4sub0m.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub0m_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub0m_fflags) $(qlI4sub0m_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub0m.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI4sub0m.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub0m_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub0m_fflags) $(qlI4sub0m_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub0m.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI4sub1m_f_dependencies)
 
$(bin)$(binobj)qlI4sub1m.o : $(qlI4sub1m_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI4sub1m.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub1m_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub1m_fflags) $(qlI4sub1m_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub1m.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI4sub1m.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub1m_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub1m_fflags) $(qlI4sub1m_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub1m.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI4sub2ma_f_dependencies)
 
$(bin)$(binobj)qlI4sub2ma.o : $(qlI4sub2ma_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI4sub2ma.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub2ma_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub2ma_fflags) $(qlI4sub2ma_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub2ma.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI4sub2ma.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub2ma_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub2ma_fflags) $(qlI4sub2ma_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub2ma.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI4sub2m_f_dependencies)
 
$(bin)$(binobj)qlI4sub2m.o : $(qlI4sub2m_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI4sub2m.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub2m_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub2m_fflags) $(qlI4sub2m_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub2m.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI4sub2m.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub2m_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub2m_fflags) $(qlI4sub2m_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub2m.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI4sub2mo_f_dependencies)
 
$(bin)$(binobj)qlI4sub2mo.o : $(qlI4sub2mo_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI4sub2mo.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub2mo_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub2mo_fflags) $(qlI4sub2mo_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub2mo.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI4sub2mo.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub2mo_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub2mo_fflags) $(qlI4sub2mo_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub2mo.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlI4sub3m_f_dependencies)
 
$(bin)$(binobj)qlI4sub3m.o : $(qlI4sub3m_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlI4sub3m.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub3m_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub3m_fflags) $(qlI4sub3m_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub3m.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlI4sub3m.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlI4sub3m_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlI4sub3m_fflags) $(qlI4sub3m_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlI4sub3m.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlinit_f_dependencies)
 
$(bin)$(binobj)qlinit.o : $(qlinit_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlinit.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlinit_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlinit_fflags) $(qlinit_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlinit.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlinit.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlinit_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlinit_fflags) $(qlinit_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlinit.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlkfn_f_dependencies)
 
$(bin)$(binobj)qlkfn.o : $(qlkfn_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlkfn.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlkfn_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlkfn_fflags) $(qlkfn_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlkfn.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlkfn.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlkfn_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlkfn_fflags) $(qlkfn_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlkfn.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlLi2omprod_f_dependencies)
 
$(bin)$(binobj)qlLi2omprod.o : $(qlLi2omprod_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlLi2omprod.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlLi2omprod_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlLi2omprod_fflags) $(qlLi2omprod_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlLi2omprod.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlLi2omprod.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlLi2omprod_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlLi2omprod_fflags) $(qlLi2omprod_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlLi2omprod.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlLi2omrat_f_dependencies)
 
$(bin)$(binobj)qlLi2omrat.o : $(qlLi2omrat_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlLi2omrat.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlLi2omrat_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlLi2omrat_fflags) $(qlLi2omrat_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlLi2omrat.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlLi2omrat.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlLi2omrat_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlLi2omrat_fflags) $(qlLi2omrat_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlLi2omrat.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlLi2omx2_f_dependencies)
 
$(bin)$(binobj)qlLi2omx2.o : $(qlLi2omx2_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlLi2omx2.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlLi2omx2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlLi2omx2_fflags) $(qlLi2omx2_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlLi2omx2.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlLi2omx2.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlLi2omx2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlLi2omx2_fflags) $(qlLi2omx2_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlLi2omx2.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlLi2omx_f_dependencies)
 
$(bin)$(binobj)qlLi2omx.o : $(qlLi2omx_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlLi2omx.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlLi2omx_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlLi2omx_fflags) $(qlLi2omx_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlLi2omx.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlLi2omx.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlLi2omx_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlLi2omx_fflags) $(qlLi2omx_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlLi2omx.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qllnrat_f_dependencies)
 
$(bin)$(binobj)qllnrat.o : $(qllnrat_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qllnrat.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qllnrat_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qllnrat_fflags) $(qllnrat_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qllnrat.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qllnrat.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qllnrat_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qllnrat_fflags) $(qllnrat_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qllnrat.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlratgam_f_dependencies)
 
$(bin)$(binobj)qlratgam.o : $(qlratgam_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlratgam.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlratgam_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlratgam_fflags) $(qlratgam_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlratgam.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlratgam.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlratgam_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlratgam_fflags) $(qlratgam_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlratgam.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlratreal_f_dependencies)
 
$(bin)$(binobj)qlratreal.o : $(qlratreal_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlratreal.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlratreal_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlratreal_fflags) $(qlratreal_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlratreal.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlratreal.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlratreal_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlratreal_fflags) $(qlratreal_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlratreal.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlsnglsort_f_dependencies)
 
$(bin)$(binobj)qlsnglsort.o : $(qlsnglsort_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlsnglsort.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlsnglsort_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlsnglsort_fflags) $(qlsnglsort_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlsnglsort.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlsnglsort.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlsnglsort_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlsnglsort_fflags) $(qlsnglsort_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlsnglsort.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlspencer_f_dependencies)
 
$(bin)$(binobj)qlspencer.o : $(qlspencer_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlspencer.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlspencer_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlspencer_fflags) $(qlspencer_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlspencer.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlspencer.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlspencer_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlspencer_fflags) $(qlspencer_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlspencer.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qltri1_f_dependencies)
 
$(bin)$(binobj)qltri1.o : $(qltri1_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qltri1.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri1_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri1_fflags) $(qltri1_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri1.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qltri1.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri1_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri1_fflags) $(qltri1_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri1.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qltri2_f_dependencies)
 
$(bin)$(binobj)qltri2.o : $(qltri2_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qltri2.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri2_fflags) $(qltri2_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri2.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qltri2.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri2_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri2_fflags) $(qltri2_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri2.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qltri3_f_dependencies)
 
$(bin)$(binobj)qltri3.o : $(qltri3_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qltri3.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri3_fflags) $(qltri3_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri3.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qltri3.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri3_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri3_fflags) $(qltri3_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri3.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qltri4_f_dependencies)
 
$(bin)$(binobj)qltri4.o : $(qltri4_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qltri4.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri4_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri4_fflags) $(qltri4_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri4.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qltri4.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri4_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri4_fflags) $(qltri4_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri4.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qltri5_f_dependencies)
 
$(bin)$(binobj)qltri5.o : $(qltri5_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qltri5.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri5_fflags) $(qltri5_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri5.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qltri5.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri5_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri5_fflags) $(qltri5_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri5.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qltri6_f_dependencies)
 
$(bin)$(binobj)qltri6.o : $(qltri6_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qltri6.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri6_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri6_fflags) $(qltri6_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri6.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qltri6.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltri6_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltri6_fflags) $(qltri6_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltri6.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qltrisort_f_dependencies)
 
$(bin)$(binobj)qltrisort.o : $(qltrisort_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qltrisort.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltrisort_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltrisort_fflags) $(qltrisort_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltrisort.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qltrisort.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qltrisort_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qltrisort_fflags) $(qltrisort_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qltrisort.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlxpicheck_f_dependencies)
 
$(bin)$(binobj)qlxpicheck.o : $(qlxpicheck_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlxpicheck.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlxpicheck_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlxpicheck_fflags) $(qlxpicheck_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlxpicheck.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlxpicheck.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlxpicheck_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlxpicheck_fflags) $(qlxpicheck_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlxpicheck.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlYcalc_f_dependencies)
 
$(bin)$(binobj)qlYcalc.o : $(qlYcalc_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlYcalc.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlYcalc_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlYcalc_fflags) $(qlYcalc_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlYcalc.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlYcalc.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlYcalc_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlYcalc_fflags) $(qlYcalc_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlYcalc.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlzero_f_dependencies)
 
$(bin)$(binobj)qlzero.o : $(qlzero_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlzero.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlzero_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlzero_fflags) $(qlzero_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlzero.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlzero.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlzero_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlzero_fflags) $(qlzero_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlzero.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)phokhara_dependencies.make : $(qlddilog_f_dependencies)
 
$(bin)$(binobj)qlddilog.o : $(qlddilog_f_dependencies)
	$(fortran_echo) $(src)eemmg-lib/ql/qlddilog.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlddilog_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlddilog_fflags) $(qlddilog_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlddilog.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)qlddilog.o $(use_pp_fflags) $(phokhara_pp_fflags) $(lib_phokhara_pp_fflags) $(qlddilog_pp_fflags) $(use_fflags) $(phokhara_fflags) $(lib_phokhara_fflags) $(qlddilog_fflags) $(qlddilog_f_fflags) $(ppcmd)../src/eemmg-lib/ql $(src)eemmg-lib/ql/qlddilog.f

#-- end of fortran_library ------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)pjfry.d

$(bin)$(binobj)pjfry.d : $(use_requirements) $(cmt_final_setup_phokhara)

$(bin)$(binobj)pjfry.d : $(src)eemmg-lib/src/pjfry.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/pjfry.dep $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(pjfry_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(pjfry_cppflags) $(pjfry_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/pjfry.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/pjfry.o $(src)eemmg-lib/src/pjfry.cpp $(@D)/pjfry.dep
endif
endif

$(bin)$(binobj)pjfry.o : $(src)eemmg-lib/src/pjfry.cpp
else
$(bin)phokhara_dependencies.make : $(pjfry_cpp_dependencies)

$(bin)$(binobj)pjfry.o : $(pjfry_cpp_dependencies)
endif
	$(cpp_echo) $(src)eemmg-lib/src/pjfry.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(pjfry_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(pjfry_cppflags) $(pjfry_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/pjfry.cpp

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)minor.d

$(bin)$(binobj)minor.d : $(use_requirements) $(cmt_final_setup_phokhara)

$(bin)$(binobj)minor.d : $(src)eemmg-lib/src/minor.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/minor.dep $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(minor_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(minor_cppflags) $(minor_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/minor.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/minor.o $(src)eemmg-lib/src/minor.cpp $(@D)/minor.dep
endif
endif

$(bin)$(binobj)minor.o : $(src)eemmg-lib/src/minor.cpp
else
$(bin)phokhara_dependencies.make : $(minor_cpp_dependencies)

$(bin)$(binobj)minor.o : $(minor_cpp_dependencies)
endif
	$(cpp_echo) $(src)eemmg-lib/src/minor.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(minor_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(minor_cppflags) $(minor_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/minor.cpp

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)integral.d

$(bin)$(binobj)integral.d : $(use_requirements) $(cmt_final_setup_phokhara)

$(bin)$(binobj)integral.d : $(src)eemmg-lib/src/integral.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/integral.dep $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(integral_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(integral_cppflags) $(integral_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/integral.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/integral.o $(src)eemmg-lib/src/integral.cpp $(@D)/integral.dep
endif
endif

$(bin)$(binobj)integral.o : $(src)eemmg-lib/src/integral.cpp
else
$(bin)phokhara_dependencies.make : $(integral_cpp_dependencies)

$(bin)$(binobj)integral.o : $(integral_cpp_dependencies)
endif
	$(cpp_echo) $(src)eemmg-lib/src/integral.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(integral_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(integral_cppflags) $(integral_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/integral.cpp

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)minoreval.d

$(bin)$(binobj)minoreval.d : $(use_requirements) $(cmt_final_setup_phokhara)

$(bin)$(binobj)minoreval.d : $(src)eemmg-lib/src/minoreval.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/minoreval.dep $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(minoreval_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(minoreval_cppflags) $(minoreval_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/minoreval.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/minoreval.o $(src)eemmg-lib/src/minoreval.cpp $(@D)/minoreval.dep
endif
endif

$(bin)$(binobj)minoreval.o : $(src)eemmg-lib/src/minoreval.cpp
else
$(bin)phokhara_dependencies.make : $(minoreval_cpp_dependencies)

$(bin)$(binobj)minoreval.o : $(minoreval_cpp_dependencies)
endif
	$(cpp_echo) $(src)eemmg-lib/src/minoreval.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(minoreval_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(minoreval_cppflags) $(minoreval_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/minoreval.cpp

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)cache.d

$(bin)$(binobj)cache.d : $(use_requirements) $(cmt_final_setup_phokhara)

$(bin)$(binobj)cache.d : $(src)eemmg-lib/src/cache.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/cache.dep $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(cache_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(cache_cppflags) $(cache_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/cache.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/cache.o $(src)eemmg-lib/src/cache.cpp $(@D)/cache.dep
endif
endif

$(bin)$(binobj)cache.o : $(src)eemmg-lib/src/cache.cpp
else
$(bin)phokhara_dependencies.make : $(cache_cpp_dependencies)

$(bin)$(binobj)cache.o : $(cache_cpp_dependencies)
endif
	$(cpp_echo) $(src)eemmg-lib/src/cache.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(cache_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(cache_cppflags) $(cache_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/cache.cpp

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),phokharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)minorex.d

$(bin)$(binobj)minorex.d : $(use_requirements) $(cmt_final_setup_phokhara)

$(bin)$(binobj)minorex.d : $(src)eemmg-lib/src/minorex.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/minorex.dep $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(minorex_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(minorex_cppflags) $(minorex_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/minorex.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/minorex.o $(src)eemmg-lib/src/minorex.cpp $(@D)/minorex.dep
endif
endif

$(bin)$(binobj)minorex.o : $(src)eemmg-lib/src/minorex.cpp
else
$(bin)phokhara_dependencies.make : $(minorex_cpp_dependencies)

$(bin)$(binobj)minorex.o : $(minorex_cpp_dependencies)
endif
	$(cpp_echo) $(src)eemmg-lib/src/minorex.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(phokhara_pp_cppflags) $(lib_phokhara_pp_cppflags) $(minorex_pp_cppflags) $(use_cppflags) $(phokhara_cppflags) $(lib_phokhara_cppflags) $(minorex_cppflags) $(minorex_cpp_cppflags) -I../src/eemmg-lib/src $(src)eemmg-lib/src/minorex.cpp

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: phokharaclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(phokhara.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(phokhara.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(phokhara.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(phokhara.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_phokhara)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(phokhara.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(phokhara.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(phokhara.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

phokharaclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library phokhara
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)phokhara$(library_suffix).a $(library_prefix)phokhara$(library_suffix).s? phokhara.stamp phokhara.shstamp
#-- end of cleanup_library ---------------
