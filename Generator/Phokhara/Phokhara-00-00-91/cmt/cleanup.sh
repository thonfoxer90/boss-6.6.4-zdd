if test "${CMTROOT}" = ""; then
  CMTROOT=/cluster/bes3/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt cleanup -sh -pack=Phokhara -version=Phokhara-00-00-91 -path=/home/bgarillo/boss-6.6.4-ZDD/Generator $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

