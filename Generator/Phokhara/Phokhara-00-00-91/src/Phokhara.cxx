//*****************************************************************************
//
//Phokhara.cxx
//
//Algorithm runs event generator Phokhara (hep-ph/0710.4227v1)
//and stores output to transient store
//
//Nov. 2007, Alexey Zhemchugov: initial version written for BES3
//Jun. 2013, Sven Schumann:     implementation of PHOKHARA 8.0
//Oct. 2013, Sven Schumann:     implementation of PHOKHARA 9.1
//Aug. 2014, Sven Schumann:     implementation of PHOKHARA 9.1
//
//*****************************************************************************

//Header for this module
#include "Phokhara/Phokhara.h"
#include "Phokhara/PhokharaDef.h"

//Framework Related Headers
#include "HepMC/GenEvent.h"
#include "HepMC/GenVertex.h"
#include "HepMC/GenParticle.h"
#include "CLHEP/Vector/LorentzVector.h"

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/DataSvc.h"
#include "GaudiKernel/SmartDataPtr.h"

#include "GeneratorObject/McGenEvent.h"
#include "BesKernel/IBesRndmGenSvc.h"
#include "CLHEP/Random/RandomEngine.h"

#include "cfortran/cfortran.h"

#include <stdlib.h>
#include <iostream>
#include <fstream>

//-----------------------------------------------------------------------------

using namespace std;
using namespace CLHEP;

//-----------------------------------------------------------------------------

Phokhara::Phokhara(const string& name, ISvcLocator* pSvcLocator):Algorithm(name, pSvcLocator)
{
  m_initSeed = (0);

  declareProperty("InitialSeed",        m_initSeed = 0);
  declareProperty("InitialEvents",      m_nm = 50000);        //Number of events to determine the maximum
  declareProperty("ScanMode",           m_ph0 = 0);           //Normal ISR(0), Scan mode(1), Scan mode, Born only(-1)
  declareProperty("NLO",                m_nlo = 1);           //Born(0), NLO(1)
  declareProperty("SoftPhotonCutoff",   m_w = 0.0001);        //Soft photon cutoff
  declareProperty("Channel",            m_pion = 0);          //mu+mu-(0), pi+pi-(1), 2pi0pi+pi-(2), 2pi+2pi-(3), ppbar(4), nnbar(5),K+K-(6),
                                                              //K0K0bar(7), pi+pi-pi0(8), Lambda Lambdabar->pi-pi+ppbar(9), pi+pi-eta(10)
  declareProperty("FSR",                m_fsr = 1);           //ISR only(0), ISR+FSR(1), ISR+INT+FSR(2)
  declareProperty("FSRNLO",             m_fsrnlo = 1);        //Yes(1), no(0)
  declareProperty("NarrowRes",          m_NarrowRes = 1);     //None(0) J/Psi(1) Psi(2S)(2)
  declareProperty("KaonFormfactor",     m_FF_Kaon = 1);       //Constrained(0), unconstrained(1), Kuhn-Khodjamirian-Bruch(2)
  declareProperty("VacuumPolarization", m_ivac = 0);          //No(0), yes [by Fred Jegerlehner](1), yes [by Thomas Teubner](2)
  declareProperty("PionFormfactor",     m_FF_Pion = 0);       //KS Pion form factor(0), GS Pion form factor, old(1), GS Pion form factor, new(2)
  declareProperty("ProtonFormfactor",   m_FF_Proton = 0);     //Proton form factor, old(0), Proton form factor, new(1)
  declareProperty("F0model",            m_f0_model = 0);      //f0+f0(600): KK model(0), no structure(1), no f0+f0(600)(2), f0 KLOE(3)
  declareProperty("Ecm",                m_E = 3.097);         //CMS energy (GeV)
  declareProperty("CutTotalInvMass",    m_q2min = 0.0);       //Minimal hadrons(muons)-gamma-inv mass squared (GeV^2)
  declareProperty("CutHadInvMass",      m_q2_min_c = 0.0447); //Minimal inv. mass squared of the hadrons(muons) (GeV^2)
  declareProperty("MaxHadInvMass",      m_q2_max_c = 1.06);   //Maximal inv. mass squared of the hadrons(muons) (GeV^2)
  declareProperty("MinPhotonEnergy",    m_gmin = 0.05);       //Minimal photon energy/missing energy (GeV)
  declareProperty("MinPhotonAngle",     m_phot1cut = 0.0);    //Minimal photon angle/missing momentum angle (deg)
  declareProperty("MaxPhotonAngle",     m_phot2cut = 180.0);  //Maximal photon angle/missing momentum angle (deg)
  declareProperty("MinHadronsAngle",    m_pi1cut = 0.0);      //Minimal hadrons(muons) angle (deg)
  declareProperty("MaxHadronsAngle",    m_pi2cut = 180.0);    //Maximal hadrons(muons) angle (deg)
  declareProperty("PassLambdas",        m_pass_llbar = false);//Pass Lambda, Lambdabar directly to GEANT, instead of performing the decay to p pi in Phokhara
  declareProperty("SloppyConfig",       m_sloppy = false);    //Ignore inconsistencies in parameter configuration (NOT RECOMMENDED)

  ievent = 0;
}

//-----------------------------------------------------------------------------

StatusCode Phokhara::initialize()
{
  MsgStream log(messageService(), name());

  log << MSG::INFO  << "Phokhara initialize" << endreq;

  int s_seed[105];
  int i, k, j;

  if(m_initSeed==0) //if seed is not set in the jobptions file, set it using BesRndmGenSvc
  {
    IBesRndmGenSvc* p_BesRndmGenSvc;
    static const bool CREATEIFNOTTHERE(true);
    StatusCode RndmStatus = service("BesRndmGenSvc", p_BesRndmGenSvc, CREATEIFNOTTHERE);
    if(!RndmStatus.isSuccess() || 0==p_BesRndmGenSvc)
    {
      log << MSG::ERROR << " Could not initialize Random Number Service" << endreq;
      return RndmStatus;
    }

    //Save the random number seeds in the event
    HepRandomEngine* engine = p_BesRndmGenSvc->GetEngine("PHOKHARA");
    const long* s = engine->getSeeds();
    m_initSeed = s[0];
  }

  log << MSG::INFO << "Set initial seed " << m_initSeed << endreq;

  /*ifstream seeds("seed.dat");
  if(seeds.is_open())
  {
    int ii=0;
    while(!seeds.eof())
      seeds >> s_seed[ii++];
  }
  else
    cout << "Cannot open file seed.dat for reading" << endl;*/

  RLXDINIT(1, m_initSeed);
  //RLXDRESETF(s_seed);
  //rlxd_reset(s_seed);

  MAXIMA.iprint = 0;

  FLAGS.ph0  = m_ph0;
  FLAGS.nlo  = m_nlo;
  FLAGS.pion = m_pion;
  FLAGS.fsr    = m_fsr;
  FLAGS.fsrnlo = m_fsrnlo;
  FLAGS.ivac   = m_ivac;
  FLAGS.FF_pion  = m_FF_Pion;
  FLAGS.FF_kaon  = m_FF_Kaon;
  FLAGS.FF_pp    = m_FF_Proton;
  FLAGS.f0_model = m_f0_model;
  FLAGS.narr_res = m_NarrowRes;

  CTES.Sp = m_E*m_E;

  CUTS.w = m_w;
  CUTS.q2min = m_q2min;
  CUTS.q2_min_c = m_q2_min_c;
  CUTS.q2_max_c = m_q2_max_c;
  CUTS.gmin = m_gmin;
  CUTS.phot1cut = m_phot1cut;
  CUTS.phot2cut = m_phot2cut;
  CUTS.pi1cut = m_pi1cut;
  CUTS.pi2cut = m_pi2cut;

  INPUT();

  //Process parameter input and exit, if any bad configuration is found
  if(!parseParameters()) return 0;

  //Find the maximum
  for(i=0; i<3; i++)
  {
    MAXIMA.Mmax[i] = 1.0;
    MAXIMA.gross[i] = 0.0;
    MAXIMA.klein[i] = 0.0;
  }
  if(FLAGS.nlo==0)  //2-photon events are not generated in LO
    MAXIMA.Mmax[2] = 0.0;
  if(FLAGS.ph0==0)  //0-photon events are not generated in ISR mode
    MAXIMA.Mmax[0] = 0.0;
  if(FLAGS.ph0==-1) //Only 0 photon events in LO are generated
  {
    MAXIMA.Mmax[1] = 0.0;
    MAXIMA.Mmax[2] = 0.0;
  }

  MAXIMA.tr[0] = 0.0;
  MAXIMA.tr[1] = 0.0;
  MAXIMA.tr[2] = 0.0;
  MAXIMA.count[0] = 0.0;
  MAXIMA.count[1] = 0.0;
  MAXIMA.count[2] = 0.0;

  //Beginning the MC loop event generation to mind maximum
  for(j=1; j<=m_nm; j++)
  {
    RANLXDF(Ar_r, 1);
    Ar[1] = Ar_r[0];
    if(Ar[1]<=MAXIMA.Mmax[0]/(MAXIMA.Mmax[1]+MAXIMA.Mmax[2]+MAXIMA.Mmax[0]))
    {
      MAXIMA.count[0] = MAXIMA.count[0]+1.0;
      GEN_0PH(1, qqmin, CTES.Sp, cos3min, cos3max);
    }
    else if(Ar[1]<=(MAXIMA.Mmax[0]+MAXIMA.Mmax[1])/(MAXIMA.Mmax[1]+MAXIMA.Mmax[2]+MAXIMA.Mmax[0]))
    {
      MAXIMA.count[1] = MAXIMA.count[1]+1.0;
      GEN_1PH(1, qqmin, qqmax, cos1min, cos1max, cos3min, cos3max);
    }
    else
    {
      MAXIMA.count[2] = MAXIMA.count[2]+1.0;
      GEN_2PH(1, qqmin, cos1min, cos1max, cos2min, cos2max, cos3min, cos3max);
    }
  }
  //End of the MC loop

  //For the production run in Phokhara::execute()
  MAXIMA.Mmax[0] = MAXIMA.gross[0]+0.10*fabs(MAXIMA.gross[0]);
  MAXIMA.Mmax[1] = MAXIMA.gross[1]+0.05*fabs(MAXIMA.gross[1]);
  MAXIMA.Mmax[2] = MAXIMA.gross[2]+(0.03+0.02*CTES.Sp)*fabs(MAXIMA.gross[2]);
  //The original implementation of MAXIMA.Mmax[n] = ... above was using the expression
  //sqrt(MAXIMA.gross[n]*MAXIMA.gross[n]) instead of fabs(MAXIMA.gross[n]). I'm using
  //fabs() here, as it should be faster and also returns always a positive number, as
  //sqrt() would do. However, it seems that MAXIMA.gross[n] is always positive, so
  //fabs() shouldn't be necessary after all, too. I don't see any reasonable explanation
  //for the original sqrt() construct...
  if((FLAGS.pion==1) && (FLAGS.fsrnlo==1))
    MAXIMA.Mmax[2]*=1.50;
  if((FLAGS.pion==0) && (FLAGS.fsrnlo==1))
    MAXIMA.Mmax[2]*=1.50;
  if((FLAGS.pion==0) && (FLAGS.fsr==1) && (FLAGS.fsrnlo==0))
    MAXIMA.Mmax[2]*=1.20;
  if((FLAGS.pion==2) || (FLAGS.pion==3))
  {
    MAXIMA.Mmax[1]*=1.10;
    MAXIMA.Mmax[2]*=1.10;
  }
  if(FLAGS.pion==8)
  {
    MAXIMA.Mmax[1]*=1.08;
    MAXIMA.Mmax[2]*=1.10;
  }

  MAXIMA.tr[0] = 0.0;
  MAXIMA.tr[1] = 0.0;
  MAXIMA.tr[2] = 0.0;
  MAXIMA.count[0] = 0.0;
  MAXIMA.count[1] = 0.0;
  MAXIMA.count[2] = 0.0;

  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------

StatusCode Phokhara::execute()
{
  MsgStream log(messageService(), name());

  int ntrials = 0;
  int tr_old[3];
  tr_old[0] = (int)MAXIMA.tr[0];
  tr_old[1] = (int)MAXIMA.tr[1];
  tr_old[2] = (int)MAXIMA.tr[2];

  while(ntrials < 1e6)
  {
    ievent++;

    RANLXDF(Ar_r, 1);
    Ar[1] = Ar_r[0];
    if(Ar[1]<=MAXIMA.Mmax[0]/(MAXIMA.Mmax[1]+MAXIMA.Mmax[2]+MAXIMA.Mmax[0]))
    {
      MAXIMA.count[0] = MAXIMA.count[0]+1.0;
      GEN_0PH(2, qqmin, CTES.Sp, cos3min, cos3max);
    }
    else if(Ar[1]<=(MAXIMA.Mmax[0]+MAXIMA.Mmax[1])/(MAXIMA.Mmax[1]+MAXIMA.Mmax[2]+MAXIMA.Mmax[0]))
    {
      MAXIMA.count[1] = MAXIMA.count[1]+1.0;
      GEN_1PH(2, qqmin, qqmax, cos1min, cos1max, cos3min, cos3max);
    }
    else
    {
      MAXIMA.count[2] = MAXIMA.count[2]+1.0;
      GEN_2PH(2, qqmin, cos1min, cos1max, cos2min, cos2max, cos3min, cos3max);
    }
    if(((int)MAXIMA.tr[0]+(int)MAXIMA.tr[1]+(int)MAXIMA.tr[2]) > (tr_old[0]+tr_old[1]+tr_old[2])) //Event accepted after cuts
    {
      storeParticles();
      return StatusCode::SUCCESS;
    }
    ntrials++;
  }

  log << MSG::WARNING << "Could not satisfy cuts after " << ntrials << "trials. Continue ..." << endreq;

  //When the loop above is failing, store current particle dataset. It might contain nonsense,
  //but a single bad event is better than a crashing program...
  storeParticles();
  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------

StatusCode Phokhara::storeParticles()
{
  MsgStream log(messageService(), name());
  int npart = 0;

  //Fill event information
  GenEvent* evt = new GenEvent(1, ievent);
  GenVertex* prod_vtx = new GenVertex();
  evt->add_vertex(prod_vtx);
  GenParticle* p;

  //Incoming beam e+
  p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][0], CTES.momenta[2][0], CTES.momenta[3][0], CTES.momenta[0][0]), -11, 3);
  p->suggest_barcode(++npart);
  prod_vtx->add_particle_in(p);

  //Incoming beam e-
  p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][1], CTES.momenta[2][1], CTES.momenta[3][1], CTES.momenta[0][1]), +11, 3);
  p->suggest_barcode(++npart);
  prod_vtx->add_particle_in(p);

  //First outgoing photon
  if(CTES.momenta[0][2]!=0)
  {
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][2], CTES.momenta[2][2], CTES.momenta[3][2], CTES.momenta[0][2]), 22, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(CTES.momenta[0][3]!=0) //Second outgoing photon
  {
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][3], CTES.momenta[2][3], CTES.momenta[3][3], CTES.momenta[0][3]), 22, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  //Other products depending on channel
  if(FLAGS.pion==0) //mu+ mu-
  {
    //mu+
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5], CTES.momenta[2][5], CTES.momenta[3][5], CTES.momenta[0][5]), -13, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //mu -
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6], CTES.momenta[2][6], CTES.momenta[3][6], CTES.momenta[0][6]), +13, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(FLAGS.pion==1) //pi+ pi-
  {
    //pi+
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5], CTES.momenta[2][5], CTES.momenta[3][5], CTES.momenta[0][5]), +211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //pi-
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6], CTES.momenta[2][6], CTES.momenta[3][6], CTES.momenta[0][6]), -211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(FLAGS.pion==2) //pi+ pi- pi0 pi0
  {
    //pi0
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5], CTES.momenta[2][5], CTES.momenta[3][5], CTES.momenta[0][5]), 111, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //pi0
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6], CTES.momenta[2][6], CTES.momenta[3][6], CTES.momenta[0][6]), 111, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //pi-
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][7], CTES.momenta[2][7], CTES.momenta[3][7], CTES.momenta[0][7]), -211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //pi+
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][8], CTES.momenta[2][8], CTES.momenta[3][8], CTES.momenta[0][8]), +211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(FLAGS.pion==3) //pi+ pi- pi+ pi-
  {
    //pi+
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5], CTES.momenta[2][5], CTES.momenta[3][5], CTES.momenta[0][5]), +211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //pi-
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6], CTES.momenta[2][6], CTES.momenta[3][6], CTES.momenta[0][6]), -211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //pi-
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][7], CTES.momenta[2][7], CTES.momenta[3][7], CTES.momenta[0][7]), -211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //pi+
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][8], CTES.momenta[2][8], CTES.momenta[3][8], CTES.momenta[0][8]), +211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(FLAGS.pion==4) //p pbar
  {
    //pbar
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5], CTES.momenta[2][5], CTES.momenta[3][5], CTES.momenta[0][5]), -2212, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //p
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6], CTES.momenta[2][6], CTES.momenta[3][6], CTES.momenta[0][6]), +2212, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(FLAGS.pion==5) //n nbar
  {
    //nbar
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5], CTES.momenta[2][5], CTES.momenta[3][5], CTES.momenta[0][5]), -2112, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //n
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6], CTES.momenta[2][6], CTES.momenta[3][6], CTES.momenta[0][6]), +2112, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(FLAGS.pion==6) //K+ K-
  {
    //K+
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5], CTES.momenta[2][5], CTES.momenta[3][5], CTES.momenta[0][5]), +321, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //K-
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6], CTES.momenta[2][6], CTES.momenta[3][6], CTES.momenta[0][6]), -321, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(FLAGS.pion==7) //K0 K0bar
  {
    //K0
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5], CTES.momenta[2][5], CTES.momenta[3][5], CTES.momenta[0][5]), +311, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //K0bar
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6], CTES.momenta[2][6], CTES.momenta[3][6], CTES.momenta[0][6]), -311, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(FLAGS.pion==8) //pi+ pi- pi0
  {
    //pi+
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5], CTES.momenta[2][5], CTES.momenta[3][5], CTES.momenta[0][5]), +211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //pi-
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6], CTES.momenta[2][6], CTES.momenta[3][6], CTES.momenta[0][6]), -211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //pi0
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][7], CTES.momenta[2][7], CTES.momenta[3][7], CTES.momenta[0][7]), 111, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(FLAGS.pion==9) //Lambda Lambdabar -> pi+ pi- p pbar
  {
    if(m_pass_llbar) //Pass only Lambda, Lambdabar to GEANT and do not use the decay products from Phokhara
    {
      //Lambdabar
      p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5],  CTES.momenta[2][5],  CTES.momenta[3][5],  CTES.momenta[0][5]),  -3122, 1);
      p->suggest_barcode(++npart);
      prod_vtx->add_particle_out(p);
      //Lambda
      p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6],  CTES.momenta[2][6],  CTES.momenta[3][6],  CTES.momenta[0][6]),  +3122, 1);
      p->suggest_barcode(++npart);
      prod_vtx->add_particle_out(p);
    }
    else //Pass Lambda, Lambdar bar (as intermediate particles) and p,pbar, pi+,pi- as final state particles to GEANT (i.e. let Phokhara handle Lambda decays)
    {
      //Lambdabar
      p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5],  CTES.momenta[2][5],  CTES.momenta[3][5],  CTES.momenta[0][5]),  -3122, 2);
      p->suggest_barcode(++npart);
      prod_vtx->add_particle_out(p);
      //Lambda
      p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6],  CTES.momenta[2][6],  CTES.momenta[3][6],  CTES.momenta[0][6]),  +3122, 2);
      p->suggest_barcode(++npart);
      prod_vtx->add_particle_out(p);
      //pi+
      p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][7],  CTES.momenta[2][7],  CTES.momenta[3][7],  CTES.momenta[0][7]),  +211, 1);
      p->suggest_barcode(++npart);
      prod_vtx->add_particle_out(p);
      //pbar
      p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][8],  CTES.momenta[2][8],  CTES.momenta[3][8],  CTES.momenta[0][8]),  -2212, 1);
      p->suggest_barcode(++npart);
      prod_vtx->add_particle_out(p);
      //pi-
      p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][9],  CTES.momenta[2][9],  CTES.momenta[3][9],  CTES.momenta[0][9]),  -211, 1);
      p->suggest_barcode(++npart);
      prod_vtx->add_particle_out(p);
      //p
      p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][10], CTES.momenta[2][10], CTES.momenta[3][10], CTES.momenta[0][10]), +2212, 1);
      p->suggest_barcode(++npart);
      prod_vtx->add_particle_out(p);
    }
  }

  if(FLAGS.pion==10) //pi+ pi- eta
  {
    //pi+
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][5], CTES.momenta[2][5], CTES.momenta[3][5], CTES.momenta[0][5]), +211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //pi-
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][6], CTES.momenta[2][6], CTES.momenta[3][6], CTES.momenta[0][6]), -211, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
    //eta
    p = new GenParticle(CLHEP::HepLorentzVector(CTES.momenta[1][7], CTES.momenta[2][7], CTES.momenta[3][7], CTES.momenta[0][7]), 221, 1);
    p->suggest_barcode(++npart);
    prod_vtx->add_particle_out(p);
  }

  if(log.level() < MSG::INFO) evt->print();

  //Check if the McCollection already exists
  SmartDataPtr<McGenEventCol> anMcCol(eventSvc(), "/Event/Gen");
  if(anMcCol!=0)
  {
    //Add event to existing collection
    MsgStream log(messageService(), name());
    log << MSG::INFO << "Add McGenEvent to existing collection" << endreq;
    McGenEvent* mcEvent = new McGenEvent(evt);
    anMcCol->push_back(mcEvent);
  }
  else
  {
    //Create Collection and add  to the transient store
    McGenEventCol *mcColl = new McGenEventCol;
    McGenEvent* mcEvent = new McGenEvent(evt);
    mcColl->push_back(mcEvent);
    StatusCode sc = eventSvc()->registerObject("/Event/Gen",mcColl);
    if(sc!=StatusCode::SUCCESS)
    {
      log << MSG::ERROR << "Could not register McGenEvent" << endreq;
      delete mcColl;
      delete evt;
      delete mcEvent;
      return StatusCode::FAILURE;
    }
    else
      log << MSG::INFO << "McGenEventCol created and " << npart <<" particles stored in McGenEvent" << endreq;
  }

  return StatusCode::SUCCESS;

}

//-----------------------------------------------------------------------------

StatusCode Phokhara::finalize()
{
  MsgStream log(messageService(), name());

  if(FLAGS.pion==9)
  {
    MAXIMA.Mmax[0] = MAXIMA.Mmax[0]*(1.0+LAMBDA_PAR.alpha_lamb)*(1.0+LAMBDA_PAR.alpha_lamb)*LAMBDA_PAR.ratio_lamb*LAMBDA_PAR.ratio_lamb;
    MAXIMA.Mmax[1] = MAXIMA.Mmax[1]*(1.0+LAMBDA_PAR.alpha_lamb)*(1.0+LAMBDA_PAR.alpha_lamb)*LAMBDA_PAR.ratio_lamb*LAMBDA_PAR.ratio_lamb;
  }

  //Values of the cross section
  sigma0 = 0.0;
  sigma1 = 0.0;
  sigma2 = 0.0;
  dsigm0 = 0.0;
  dsigm1 = 0.0;
  dsigm2 = 0.0;

  if((FLAGS.ph0==0) && (FLAGS.nlo==0))
  {
    sigma1 = MAXIMA.Mmax[1]/MAXIMA.count[1]*MAXIMA.tr[1];
    dsigm1 = MAXIMA.Mmax[1]*sqrt((MAXIMA.tr[1]/MAXIMA.count[1]-MAXIMA.tr[1]/MAXIMA.count[1]*MAXIMA.tr[1]/MAXIMA.count[1])/MAXIMA.count[1]);
  }
  else if((FLAGS.ph0==0) && (FLAGS.nlo==1))
  {
    sigma1 = MAXIMA.Mmax[1]/MAXIMA.count[1]*MAXIMA.tr[1];
    dsigm1 = MAXIMA.Mmax[1]*sqrt((MAXIMA.tr[1]/MAXIMA.count[1]-MAXIMA.tr[1]/MAXIMA.count[1]*MAXIMA.tr[1]/MAXIMA.count[1])/MAXIMA.count[1]);
    dsigm1 = MAXIMA.Mmax[1]*sqrt((MAXIMA.tr[1]/MAXIMA.count[1]-MAXIMA.tr[1]/MAXIMA.count[1]*MAXIMA.tr[1]/MAXIMA.count[1])/MAXIMA.count[1]);
    sigma2 = MAXIMA.Mmax[2]/MAXIMA.count[2]*MAXIMA.tr[2];
    dsigm2 = MAXIMA.Mmax[2]*sqrt((MAXIMA.tr[2]/MAXIMA.count[2]-MAXIMA.tr[2]/MAXIMA.count[2]*MAXIMA.tr[2]/MAXIMA.count[2])/MAXIMA.count[2]);
  }
  else if((FLAGS.ph0==1) && (FLAGS.nlo==0))
  {
    sigma0 = MAXIMA.Mmax[0]/MAXIMA.count[0]*MAXIMA.tr[0];
    dsigm0 = MAXIMA.Mmax[0]*sqrt((MAXIMA.tr[0]/MAXIMA.count[0]-MAXIMA.tr[0]/MAXIMA.count[0]*MAXIMA.tr[0]/MAXIMA.count[0])/MAXIMA.count[0]);
    sigma1 = MAXIMA.Mmax[1]/MAXIMA.count[1]*MAXIMA.tr[1];
    dsigm1 = MAXIMA.Mmax[1]*sqrt((MAXIMA.tr[1]/MAXIMA.count[1]-MAXIMA.tr[1]/MAXIMA.count[1]*MAXIMA.tr[1]/MAXIMA.count[1])/MAXIMA.count[1]);
  }
  else if(FLAGS.ph0==-1)
  {
    sigma0 = MAXIMA.Mmax[0]/MAXIMA.count[0]*MAXIMA.tr[0];
    dsigm0 = MAXIMA.Mmax[0]*sqrt((MAXIMA.tr[0]/MAXIMA.count[0]-MAXIMA.tr[0]/MAXIMA.count[0]*MAXIMA.tr[0]/MAXIMA.count[0])/MAXIMA.count[0]);
  }
  else
  {
    sigma0 = MAXIMA.Mmax[0]/MAXIMA.count[0]*MAXIMA.tr[0];
    dsigm0 = MAXIMA.Mmax[0]*sqrt((MAXIMA.tr[0]/MAXIMA.count[0]-MAXIMA.tr[0]/MAXIMA.count[0]*MAXIMA.tr[0]/MAXIMA.count[0])/MAXIMA.count[0]);
    sigma1 = MAXIMA.Mmax[1]/MAXIMA.count[1]*MAXIMA.tr[1];
    dsigm1 = MAXIMA.Mmax[1]*sqrt((MAXIMA.tr[1]/MAXIMA.count[1]-MAXIMA.tr[1]/MAXIMA.count[1]*MAXIMA.tr[1]/MAXIMA.count[1])/MAXIMA.count[1]);
    sigma2 = MAXIMA.Mmax[2]/MAXIMA.count[2]*MAXIMA.tr[2];
    dsigm2 = MAXIMA.Mmax[2]*sqrt((MAXIMA.tr[2]/MAXIMA.count[2]-MAXIMA.tr[2]/MAXIMA.count[2]*MAXIMA.tr[2]/MAXIMA.count[2])/MAXIMA.count[2]);
  }
  sigma = sigma0+sigma1+sigma2;
  dsigm = sqrt(dsigm0*dsigm0+dsigm1*dsigm1+dsigm2*dsigm2);

  //Print run statistics
  cout << "-------------------------------------------------------------" << endl;
  cout << "     PHOKHARA 9.1 Final Statistics                           " << endl;
  cout << "-------------------------------------------------------------" << endl;
  cout << int(MAXIMA.tr[0]+MAXIMA.tr[1]+MAXIMA.tr[2]) << " total events accepted of " << endl;
  cout << int(ievent)            << " total events generated" << endl;
  cout << int(MAXIMA.tr[0])    << " zero photon events accepted of " << endl;
  cout << int(MAXIMA.count[0]) << " events generated" << endl;
  cout << int(MAXIMA.tr[1])    << " one photon events accepted of "  << endl;
  cout << int(MAXIMA.count[1]) << " events generated" << endl;
  cout << int(MAXIMA.tr[2])    << " two photon events accepted of "  << endl;
  cout << int(MAXIMA.count[2]) << " events generated" << endl;
  cout << endl;
  cout << "sigma0(nbarn) = " << sigma0 << " +- " << dsigm0 << endl;
  cout << "sigma1(nbarn) = " << sigma1 << " +- " << dsigm1 << endl;
  cout << "sigma2(nbarn) = " << sigma2 << " +- " << dsigm2 << endl;
  cout << "sigma(nbarn)  = " << sigma  << " +- " << dsigm  << endl;
  cout << endl;
  cout << "maximum0 = " << MAXIMA.gross[0] << "  minimum0 = " << MAXIMA.klein[0] << endl;
  cout << "Mmax0    = " << MAXIMA.Mmax[0]  << endl;
  cout << "maximum1 = " << MAXIMA.gross[1] << "  minimum1 = " << MAXIMA.klein[1] << endl;
  cout << "Mmax1    = " << MAXIMA.Mmax[1]  << endl;
  cout << "maximum2 = " << MAXIMA.gross[2] << "  minimum2 = " << MAXIMA.klein[2] << endl;
  cout << "Mmax2    = " << MAXIMA.Mmax[2]  << endl;
  cout << "-------------------------------------------------------------" << endl;

  log << MSG::INFO << "Phokhara finalized" << endreq;

  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------

StatusCode Phokhara::parseParameters()
{
  MsgStream log(messageService(), name());

  //Print run data
  cout << "-------------------------------------------------------------" << endl;
  if(FLAGS.ph0==0)
  {
    if(FLAGS.pion==0)
      cout << "PHOKHARA 9.1 : e^+ e^- -> mu^+ mu^- gamma" << endl;
    else if(FLAGS.pion==1)
      cout << "PHOKHARA 9.1: e^+ e^- -> pi^+ pi^- gamma" << endl;
    else if(FLAGS.pion==2)
      cout << "PHOKHARA 9.1: e^+ e^- -> pi^+ pi^- 2pi^0 gamma" << endl;
    else if(FLAGS.pion==3)
      cout << "PHOKHARA 9.1: e^+ e^- -> 2pi^+ 2pi^- gamma" << endl;
    else if(FLAGS.pion==4)
      cout << "PHOKHARA 9.1: e^+ e^- -> p pbar gamma" << endl;
    else if(FLAGS.pion==5)
      cout << "PHOKHARA 9.1: e^+ e^- -> n nbar gamma" << endl;
    else if(FLAGS.pion==6)
      cout << "PHOKHARA 9.1: e^+ e^- -> K^+ K^- gamma" << endl;
    else if(FLAGS.pion==7)
      cout << "PHOKHARA 9.1: e^+ e^- -> K_0 K_0bar gamma" << endl;
    else if(FLAGS.pion==8)
      cout << "PHOKHARA 9.1: e^+ e^- -> pi^+ pi^- pi^0 gamma" << endl;
    else if(FLAGS.pion==9)
      cout << "PHOKHARA 9.1 : e^+ e^- -> Lambda (-> pi^- p) Lambda bar (-> pi^+ pbar) gamma" << endl;
    else if(FLAGS.pion==10)
      cout << "PHOKHARA 9.1: e^+ e^- -> pi^+ pi^- eta gamma" << endl;
    else
      cout << "PHOKHARA 9.1: not yet implemented" << endl;
  }
  else
  {
    if(FLAGS.pion==0)
      cout << "PHOKHARA 9.1 : e^+ e^- -> mu^+ mu^-" << endl;
    else if(FLAGS.pion==1)
      cout << "PHOKHARA 9.1: e^+ e^- -> pi^+ pi^-" << endl;
    else if(FLAGS.pion==2)
      cout << "PHOKHARA 9.1: e^+ e^- -> pi^+ pi^- 2pi^0" << endl;
    else if(FLAGS.pion==3)
      cout << "PHOKHARA 9.1: e^+ e^- -> 2pi^+ 2pi^-" << endl;
    else if(FLAGS.pion==4)
      cout << "PHOKHARA 9.1: e^+ e^- -> p pbar" << endl;
    else if(FLAGS.pion==5)
      cout << "PHOKHARA 9.1: e^+ e^- -> n nbar" << endl;
    else if(FLAGS.pion==6)
      cout << "PHOKHARA 9.1: e^+ e^- -> K^+ K^-" << endl;
    else if(FLAGS.pion==7)
      cout << "PHOKHARA 9.1: e^+ e^- -> K_0 K_0bar" << endl;
    else if(FLAGS.pion==8)
      cout << "PHOKHARA 9.1: e^+ e^- -> pi^+ pi^- pi^0" << endl;
    else if(FLAGS.pion==9)
      cout << "PHOKHARA 9.1 : e^+ e^- -> Lambda (-> pi^- p) Lambda bar (-> pi^+ pbar)" << endl;
    else if(FLAGS.pion==10)
      cout << "PHOKHARA 9.1: e^+ e^- -> pi^+ pi^- eta" << endl;
    else
      cout << "PHOKHARA 9.1: not yet implemented" << endl;
  }
  cout << "--------------------------------------------------------------" << endl;

  cout << "CM total energy = " << sqrt(CTES.Sp) << " GeV" << endl;
  if(FLAGS.ph0==0) //Scan mode not enabled?
  {
    if((0.5*CUTS.gmin/CTES.ebeam) < 0.0098)
    {
      cout << "Minimal missing energy set to small" << endl;
      if(!m_sloppy) return 0;
    }
    cout << "Minimal tagged photon energy  = " << CUTS.gmin << " GeV"  << endl;
    cout << "Angular cuts on tagged photon = " << CUTS.phot1cut << "," << CUTS.phot2cut << endl;
  }

  //Print angular cuts
  if(FLAGS.pion==0)
    cout << "Angular cuts on muons             = " << CUTS.pi1cut << "," << CUTS.pi2cut << endl;
  else if(FLAGS.pion==4)
    cout << "Angular cuts on protons           = " << CUTS.pi1cut << "," << CUTS.pi2cut << endl;
  else if(FLAGS.pion==5)
    cout << "Angular cuts on neutrons          = " << CUTS.pi1cut << "," << CUTS.pi2cut << endl;
  else if((FLAGS.pion==6) || (FLAGS.pion==7))
    cout << "Angular cuts on kaons             = " << CUTS.pi1cut << "," << CUTS.pi2cut << endl;
  else if(FLAGS.pion==9)
    cout << "Angular cuts on pions and protons = " << CUTS.pi1cut << "," << CUTS.pi2cut << endl;
  else if(FLAGS.pion==10)
    cout << "Angular cuts on pions and eta     = " << CUTS.pi1cut << "," << CUTS.pi2cut << endl;
  else
    cout << "Angular cuts on pions             = " << CUTS.pi1cut << "," << CUTS.pi2cut << endl;

  if(FLAGS.ph0==0) //Scan mode not enabled?
  {
    if(FLAGS.pion==0)
      cout << "Min. muons-tagged photon inv.mass^2     = " << CUTS.q2min << " GeV^2" << endl;
    else if(FLAGS.pion==4)
      cout << "Min. protons-tagged photon inv.mass^2   = " << CUTS.q2min << " GeV^2" << endl;
    else if(FLAGS.pion==5)
      cout << "Min. neutrons-tagged photon inv.mass^2  = " << CUTS.q2min << " GeV^2" << endl;
    else if((FLAGS.pion==6) || (FLAGS.pion==7))
      cout << "Min. kaons-tagged photon inv.mass^2     = " << CUTS.q2min << " GeV^2" << endl;
    else if(FLAGS.pion==9)
      cout << "Min. lambdas-tagged photon inv.mass^2   = " << CUTS.q2min << " GeV^2" << endl;
    else if(FLAGS.pion==10)
      cout << "Min. pi-pi-eta-tagged photon inv.mass^2 = " << CUTS.q2min << " GeV^2" << endl;
    else
      cout << "Min. pions-tagged photon inv.mass^2     = " << CUTS.q2min << " GeV^2" << endl;
  }

  //Set cuts
  cos1min = cos(CUTS.phot2cut*CTES.pi/180.0); //Photon1 angle cuts in lab rest frame
  cos1max = cos(CUTS.phot1cut*CTES.pi/180.0); //Photon1 angle cuts in lab rest frame
  cos2min = -1.0; //Photon2 angle limits
  cos2max =  1.0; //Photon2 angle limits
  cos3min = -1.0; //Hadrons/muons angle limits in their rest frame
  cos3max =  1.0; //Hadrons/muons angle limits in their rest frame

  if(FLAGS.pion==0)                   //Virtual photon energy cut
    qqmin = 4.0*CTES.mmu*CTES.mmu;
  else if(FLAGS.pion==1)
    qqmin = 4.0*CTES.mpi*CTES.mpi;
  else if(FLAGS.pion==2)
    qqmin = 4.0*(CTES.mpi+CTES.mpi0)*(CTES.mpi+CTES.mpi0);
  else if(FLAGS.pion==3)
    qqmin = 16.0*CTES.mpi*CTES.mpi;
  else if(FLAGS.pion==4)
    qqmin = 4.0*CTES.mp*CTES.mp;
  else if(FLAGS.pion==5)
    qqmin = 4.0*CTES.mnt*CTES.mnt;
  else if(FLAGS.pion==6)
    qqmin = 4.0*CTES.mKp*CTES.mKp;
  else if(FLAGS.pion==7)
    qqmin = 4.0*CTES.mKn*CTES.mKn;
  else if(FLAGS.pion==8)
    qqmin = (2.0*CTES.mpi+CTES.mpi0)*(2.0*CTES.mpi+CTES.mpi0);
  else if(FLAGS.pion==9)
    qqmin = 4.0*CTES.mlamb*CTES.mlamb;
  else if(FLAGS.pion==10)
    qqmin = (2.0*CTES.mpi+CTES.meta)*(2.0*CTES.mpi+CTES.meta);
  qqmax = CTES.Sp-2.0*sqrt(CTES.Sp)*CUTS.gmin; //If only one photon

  //Check upper bound for maximum q^2 cut in ISR mode
  if((CUTS.q2_max_c < qqmax) && (FLAGS.ph0==0))
    qqmax = CUTS.q2_max_c; //External cut for maximum q^2

  //Check lower bound for minimum q^2 cut
  if(FLAGS.ph0==0) //In ISR mode
  {
    if((CUTS.q2_min_c > qqmin) && (CUTS.q2_min_c < (CTES.Sp*(1.0-2.0*(CUTS.gmin/sqrt(CTES.Sp)+CUTS.w)))))
      qqmin = CUTS.q2_min_c; //External cut for minimum q^2
    else
    {
      cout << " Q^2_min out of range" << endl;
      cout << " Q^2_min changed by PHOKHARA to = " << qqmin << " GeV^2" << endl;
    }
  }
  else if((FLAGS.ph0==1) || (FLAGS.ph0==-1)) //In Scan mode
  {
    if((CUTS.q2_min_c > qqmin) && (CUTS.q2_min_c < (CTES.Sp-2.0*sqrt(CTES.Sp)*CUTS.gmin)))
      qqmin = CUTS.q2_min_c; //External cut for minimum q^2
    else
    {
      cout << " Q^2_min out of range" << endl;
      cout << " Q^2_min changed by PHOKHARA to = " << qqmin << " GeV^2" << endl;
    }
  }

  //Check lower and upper q^2 cut bounds for consistency
  if(qqmax<=qqmin)
  {
    cout << " Q^2_max too small" << endl;
    cout << " Q^2_max = " << qqmax << endl;
    cout << " Q^2_min = " << qqmin << endl;
    if(!m_sloppy) return 0;
  }

  //Print invariant mass range cuts
  if(FLAGS.ph0==0) //In ISR mode
  {
    if(FLAGS.pion==0)
    {
      cout << "Minimal muon-pair invariant mass^2    = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal muon-pair invariant mass^2    = "<< qqmax << " GeV^2" << endl;
    }
    else if(FLAGS.pion==1)
    {
      cout << "Minimal pion-pair invariant mass^2    = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal pion-pair invariant mass^2    = "<< qqmax << " GeV^2" << endl;
    }
    else if(FLAGS.pion==4)
    {
      cout << "Minimal proton-pair invariant mass^2  = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal proton-pair invariant mass^2  = "<< qqmax << " GeV^2" << endl;
    }
    else if(FLAGS.pion==5)
    {
      cout << "Minimal neutron-pair invariant mass^2 = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal neutron-pair invariant mass^2 = "<< qqmax << " GeV^2" << endl;
    }
    else if((FLAGS.pion==6) || (FLAGS.pion==7))
    {
      cout << "Minimal kaon-pair invariant mass^2    = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal kaon-pair invariant mass^2    = "<< qqmax << " GeV^2" << endl;
    }
    else if(FLAGS.pion==8)
    {
      cout << "Minimal three-pion invariant mass^2   = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal three-pion invariant mass^2   = "<< qqmax << " GeV^2" << endl;
    }
    else if(FLAGS.pion==9)
    {
      cout << "Minimal lambda-pair invariant mass^2  = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal lambda-pair invariant mass^2  = "<< qqmax << " GeV^2" << endl;
    }
    else if(FLAGS.pion==10)
    {
      cout << "Minimal pi-pi-eta invariant mass^2    = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal pi-pi-eta invariant mass^2    = "<< qqmax << " GeV^2" << endl;
    }
    else
    {
      cout << "Minimal four-pion invariant mass^2    = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal four-pion invariant mass^2    = "<< qqmax << " GeV^2" << endl;
    }
  }
  else if((FLAGS.ph0==1) || (FLAGS.ph0==-1)) //In Scan mode
  {
    if(FLAGS.pion==0)
    {
      cout << "Minimal muon-pair invariant mass^2    = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal muon-pair invariant mass^2    = "<< CTES.Sp << " GeV^2" << endl;
    }
    else if(FLAGS.pion==1)
    {
      cout << "Minimal pion-pair invariant mass^2    = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal pion-pair invariant mass^2    = "<< CTES.Sp << " GeV^2" << endl;
    }
    else if(FLAGS.pion==4)
    {
      cout << "Minimal proton-pair invariant mass^2  = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal proton-pair invariant mass^2  = "<< CTES.Sp << " GeV^2" << endl;
    }
    else if(FLAGS.pion==5)
    {
      cout << "Minimal neutron-pair invariant mass^2 = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal neutron-pair invariant mass^2 = "<< CTES.Sp << " GeV^2" << endl;
    }
    else if((FLAGS.pion==6) || (FLAGS.pion==7))
    {
      cout << "Minimal kaon-pair invariant mass^2    = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal kaon-pair invariant mass^2    = "<< CTES.Sp << " GeV^2" << endl;
    }
    else if(FLAGS.pion==8)
    {
      cout << "Minimal three-pion invariant mass^2   = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal three-pion invariant mass^2   = "<< CTES.Sp << " GeV^2" << endl;
    }
    else if(FLAGS.pion==9)
    {
      cout << "Minimal lambda-pair invariant mass^2  = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal lambda-pair invariant mass^2  = "<< CTES.Sp << " GeV^2" << endl;
    }
    else if(FLAGS.pion==10)
    {
      cout << "Minimal pi-pi-eta invariant mass^2    = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal pi-pi-eta invariant mass^2    = "<< CTES.Sp << " GeV^2" << endl;
    }
    else
    {
      cout << "Minimal four-pion invariant mass^2    = "<< qqmin << " GeV^2" << endl;
      cout << "Maximal four-pion invariant mass^2    = "<< CTES.Sp << " GeV^2" << endl;
    }
  }

  //Handle Born/NLO switches for ISR and Scan modes
  if(FLAGS.ph0==-1) //Scan mode, Born only
    cout << "Born" << endl;
  else if(FLAGS.ph0==0) //ISR mode
  {
     if(FLAGS.nlo==0)
     {
       cout << "Born" << endl;
       if(FLAGS.fsrnlo!=0)
       {
         cout << "Wrong FSRNLO switch: only FSRNLO = 0 allowed for Born" << endl;
         if(!m_sloppy) return 0;
       }
     }
  }
  else //Scan mode
  {
    if(FLAGS.nlo==0)
      cout << "ISR NLO" << endl;
    else if(FLAGS.nlo==1)
      cout << "ISR NNLO" << endl;
    else
    {
      cout << "Wrong NLO switch" << endl;
      if(!m_sloppy) return 0;
    }
  }

  //Handle NLO for Lambda Lambdabar in ISR and Scan modes
  if((FLAGS.pion==9) && (FLAGS.nlo!=0))
  {
    cout << "Wrong NLO switch";
    if(FLAGS.ph0==1)  cout << ": only NLO allowed for Lambdas" << endl;
    if(FLAGS.ph0==0)  cout << ": only Born allowed for Lambdas" << endl;
    cout << "If you feel that you need better precision, please contact the authors" << endl;
    if(!m_sloppy) return 0;
  }

  //In Scan mode, handle soft photon cuts and FSR Coulomb factor
  if(FLAGS.ph0==1)
  {
    if(FLAGS.nlo==0) cout << "NLO:  soft photon cutoff w = " << CUTS.w << endl;
    if(FLAGS.nlo==1) cout << "NNLO: soft photon cutoff w = " << CUTS.w << endl;
    if(FLAGS.fsr!=0) cout << "Coulomb factor included" << endl;
  }

  //In ISR mode, first handle mu+mu-...
  if((FLAGS.ph0==0) || (FLAGS.pion==0))
  {
    if(FLAGS.nlo==0)
    {
      if(FLAGS.fsrnlo!=0)
      {
        cout << "Wrong combination of FSR, FSRNLO switches" << endl;
        if(!m_sloppy) return 0;
      }
      else
      {
        if(FLAGS.fsr==0)
          cout << "ISR only" << endl;
        else if(FLAGS.fsr==1)
          cout << "ISR+FSR" << endl;
        else if(FLAGS.fsr==2)
          cout << "ISR+INT+FSR" << endl;
        else
        {
          cout << "Wrong FSR switch" << endl;
          if(!m_sloppy) return 0;
        }
      }
    }
    else if(FLAGS.nlo==1)
    {
      if(FLAGS.fsrnlo==1)
      {
        if(FLAGS.fsr==2)
          cout << "ISR+INT+FSR" << endl;
        else
        {
          cout << "Wrong switches" << endl;
          cout << "Combinations for mu+mu- NLO mode:" << endl;
          cout << "NLO = 1 then" << endl;
          cout << "FSR = 0, IFSNLO = 0" << endl;
          cout << "FSR = 2, IFSNLO = 1" << endl;
          if(!m_sloppy) return 0;
        }
      }
    }
  }

  //...and then other final states in ISR mode
  if(FLAGS.ph0==0)
  {
    //... handle soft photon cuts, ...
    if(FLAGS.nlo==1) cout << "NLO:  soft photon cutoff w = " << CUTS.w << endl;

    //... and check various FSR and FSRNLO combinations for validity in implemented modes
    if((FLAGS.pion==1) || (FLAGS.pion==4)|| (FLAGS.pion==6))
    {
      if(((FLAGS.fsr==1) || (FLAGS.fsr==2)) && (FLAGS.fsrnlo==0))
      {
        ; //NOP
      }
      else if((FLAGS.fsr==1) && (FLAGS.fsrnlo==1))
      {
        ; //NOP
      }
      else if((FLAGS.fsr==0) && (FLAGS.fsrnlo==0))
      {
        ; //NOP
      }
      else
      {
        cout << "Wrong combination of FSR, FSRNLO switches" << endl;
        if(!m_sloppy) return 0;
      }

      //Check FSR, ISR flags
      if(FLAGS.fsr==0)
        cout << "ISR only" << endl;
      else if(FLAGS.fsr==1)
        cout << "ISR+FSR" << endl;
      else if(FLAGS.fsr==2)
      {
        if(FLAGS.nlo==0)
          cout << "ISR+INT+FSR" << endl;
        else
        {
          cout << "Wrong FSR switch: interference is included only for NLO = 0" << endl;
          if(!m_sloppy) return 0;
        }
      }
      else
      {
        cout << "Wrong FSR switch" << FLAGS.fsr << endl;
        if(!m_sloppy) return 0;
      }

      if(FLAGS.fsrnlo==1)
        cout << "IFSNLO included" << endl;
    }
    else //in some modes, FSR is not available
    {
      if((FLAGS.fsr==0) && (FLAGS.fsrnlo==0))
        cout << "ISR only" << endl;
      else if(FLAGS.pion==0)
        ; //NOP
      else
      {
        cout << "FSR is implemented only for pi+pi-, mu+mu- and K+K- modes" << endl;
        if(!m_sloppy) return 0;
      }
    }
  }

  //Handle vacuum polarisation
  if(FLAGS.ivac==0)
    cout << "Vacuum polarization is NOT included" << endl;
  else if(FLAGS.ivac==1)
    cout << "Vacuum polarization by Fred Jegerlehner (http://www-com.physik.hu-berlin.de/fjeger/alphaQEDn.uu)" << endl;
  else if(FLAGS.ivac==2)
    cout << "Vacuum polarization by Daisuke Nomura and Thomas Teubner (VP_HLMNT_v1_3nonr)" << endl;
  else
  {
    cout << "Wrong vacuum polarization switch" << endl;
    if(!m_sloppy) return 0;
  }

  //Handle pion form factor
  if(FLAGS.pion==1)
  {
    if(FLAGS.FF_pion==0)
      cout << "Kuhn-Santamaria pion form factor" << endl;
    else if(FLAGS.FF_pion==1)
      cout << "Gounaris-Sakurai pion form factor (old)" << endl;
    else if(FLAGS.FF_pion==2)
      cout << "Gounaris-Sakurai pion form factor (new)" << endl;
    else
    {
      cout << "Wrong pion form factor switch" << endl;
      if(!m_sloppy) return 0;
    }

    if(FLAGS.fsr!=0)
    {
      if(FLAGS.f0_model==0)
        cout << "f0+f0(600): K+K- model" << endl;
      else if(FLAGS.f0_model==1)
        cout << "f0+f0(600): \"No structure\" model" << endl;
      else if(FLAGS.f0_model==2)
        cout << "No f0+f0(600)" << endl;
      else if(FLAGS.f0_model==3)
        cout << "Only f0, KLOE (Cesare Bini, private communication)" << endl;
      else
      {
        cout << "Wrong f0+f0(600) switch" << endl;
        if(!m_sloppy) return 0;
      }
    }
  }

  //Handle Kaon form factor
  if((FLAGS.pion==6) || (FLAGS.pion==7))
  {
    if(FLAGS.FF_kaon==0)
      cout << "Constrained Kaon form factor" << endl;
    else if(FLAGS.FF_kaon==1)
      cout << "Unconstrained Kaon form factor" << endl;
    else if(FLAGS.FF_kaon==2)
      cout << "Kuhn-Khodjamirian-Bruch Kaon form factor" << endl;
    else
    {
      cout << "Wrong Kaon form factor switch" << endl;
      if(!m_sloppy) return 0;
    }
  }

  //Handle proton form factor
  if((FLAGS.pion==4) || (FLAGS.pion==5))
  {
    if(FLAGS.FF_pp==0)
      cout << "Proton form factor (old)" << endl;
    else if(FLAGS.FF_pp==1)
      cout << "Proton form factor (new)" << endl;
    else
    {
      cout << "Wrong Proton form factor switch" << endl;
      if(!m_sloppy) return 0;
    }
  }

  //Handle J/Psi resonances
  if((FLAGS.pion==0) || (FLAGS.pion==1) || (FLAGS.pion==6) || (FLAGS.pion==7))
  {
    if(FLAGS.narr_res==1)
      cout << "Narrow resonance J/Psi included" << endl;
    else if(FLAGS.narr_res==2)
      cout << "Narrow resonance Psi(2S) included" << endl;
    else if(FLAGS.narr_res!=0)
    {
      cout << "Wrong NarrowRes switch" << endl;
      if(!m_sloppy) return 0;
    }
  }
  cout << "--------------------------------------------------------------" << endl;

  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------

