//*****************************************************************************
//
// Generator/Phokhara/Phokhara.h
//
// Algorithm runs event generator Phokhara (hep-ph/0710.4227v1)
// and stores output to transient store
//
//Nov. 2007, Alexey Zhemchugov: initial version written for BES3
//Jun. 2013, Sven Schumann:     implementation of PHOKHARA 8.0
//Oct. 2013, Sven Schumann:     implementation of PHOKHARA 9.0
//Aug. 2014, Sven Schumann:     implementation of PHOKHARA 9.1
//
//*****************************************************************************

#ifndef GENERATORMODULESPHOKHARA_H
#define GENERATORMODULESPHOKHARA_H

#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"

#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/ISvcLocator.h"

#include <vector>

using namespace std;

class Phokhara:public Algorithm
{
public:
 Phokhara(const string& name, ISvcLocator* pSvcLocator);

 StatusCode initialize();
 StatusCode execute();
 StatusCode finalize();
 StatusCode storeParticles();
 StatusCode parseParameters();

private:
  //jobOption parameters
 int m_nm;          //Number of events to determine the maximum
 int m_ph0;         //Normal ISR(0), Scan mode(1), Scan mode, Born only(-1)
 int m_nlo;         //Born(0), NLO(1)
 int m_pion;        //mu+mu-(0), pi+pi-(1), 2pi0pi+pi-(2), 2pi+2pi-(3), ppbar(4), nnbar(5),K+K-(6),
                    //K0K0bar(7), pi+pi-pi0(8), Lambda Lambdabar->pi-pi+ppbar(9), pi+pi-eta(10)
 int m_fsr;         //ISR only(0), ISR+FSR(1), ISR+INT+FSR(2)
 int m_fsrnlo;      //Yes(1), no(0)
 int m_ivac;        //No(0), yes [by Fred Jegerlehner](1), yes [by Thomas Teubner](2)
 int m_NarrowRes;   //None(0), JPsi (1), Psi(2S)(2)
 int m_FF_Kaon;     //Kaon form factor: constrained (0), unconstrained (1), Kuhn-Khodjamirian-Bruch (2)
 int m_FF_Pion;     //KS pion form factor(0), GS pion form factor, old(1), GS pion form factor, new(2)
 int m_FF_Proton;   //Proton form factor, old(0), proton form factor, new(1)
 int m_f0_model;    //f0+f0(600): KK model(0), no structure(1), no f0+f0(600)(2), f0 KLOE(3)
 double m_w;        //Soft photon cutoff
 double m_E;        //CMS energy
 double m_q2min;    //Minimal hadrons(muons)-gamma-inv mass squared (GeV^2)
 double m_q2_min_c; //Minimal inv. mass squared of the hadrons(muons) (GeV^2)
 double m_q2_max_c; //Maximal inv. mass squared of the hadrons(muons) (GeV^2)
 double m_gmin;     //Minimal photon energy/missing energy (GeV)
 double m_phot1cut; //Minimal photon angle/missing momentum angle (deg)
 double m_phot2cut; //Maximal photon angle/missing momentum angle (deg)
 double m_pi1cut;   //Minimal hadrons(muons) angle (deg)
 double m_pi2cut;   //Maximal hadrons(muons) angle (deg)
 bool m_pass_llbar; //Pass Lambda, Lambdabar directly to GEANT, instead of performing the decay to p pi in Phokhara
 bool m_sloppy;     //Ignore inconsistencies in parameter configuration (NOT RECOMMENDED)

 double qqmin, qqmax;
 double cos1min, cos1max, cos2min, cos2max, cos3min, cos3max;
 double dsigm0, dsigm1, dsigm2, dsigm, sigma0, sigma1, sigma2, sigma;
 double Ar[14], Ar_r[14];
 int ievent;

 //Initial Seed
 long int m_initSeed;
};

#endif
