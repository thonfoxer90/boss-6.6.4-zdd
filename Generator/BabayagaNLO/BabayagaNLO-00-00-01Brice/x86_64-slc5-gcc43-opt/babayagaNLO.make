#-- start of make_header -----------------

#====================================
#  Library babayagaNLO
#
#   Generated Thu Jun 21 15:42:09 2018  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_babayagaNLO_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_babayagaNLO_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_babayagaNLO

BabayagaNLO_tag = $(tag)

#cmt_local_tagfile_babayagaNLO = $(BabayagaNLO_tag)_babayagaNLO.make
cmt_local_tagfile_babayagaNLO = $(bin)$(BabayagaNLO_tag)_babayagaNLO.make

else

tags      = $(tag),$(CMTEXTRATAGS)

BabayagaNLO_tag = $(tag)

#cmt_local_tagfile_babayagaNLO = $(BabayagaNLO_tag).make
cmt_local_tagfile_babayagaNLO = $(bin)$(BabayagaNLO_tag).make

endif

include $(cmt_local_tagfile_babayagaNLO)
#-include $(cmt_local_tagfile_babayagaNLO)

ifdef cmt_babayagaNLO_has_target_tag

cmt_final_setup_babayagaNLO = $(bin)setup_babayagaNLO.make
#cmt_final_setup_babayagaNLO = $(bin)BabayagaNLO_babayagaNLOsetup.make
cmt_local_babayagaNLO_makefile = $(bin)babayagaNLO.make

else

cmt_final_setup_babayagaNLO = $(bin)setup.make
#cmt_final_setup_babayagaNLO = $(bin)BabayagaNLOsetup.make
cmt_local_babayagaNLO_makefile = $(bin)babayagaNLO.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)BabayagaNLOsetup.make

#babayagaNLO :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'babayagaNLO'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = babayagaNLO/
#babayagaNLO::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

babayagaNLOlibname   = $(bin)$(library_prefix)babayagaNLO$(library_suffix)
babayagaNLOlib       = $(babayagaNLOlibname).a
babayagaNLOstamp     = $(bin)babayagaNLO.stamp
babayagaNLOshstamp   = $(bin)babayagaNLO.shstamp

babayagaNLO :: dirs  babayagaNLOLIB
	$(echo) "babayagaNLO ok"

#-- end of libary_header ----------------

babayagaNLOLIB :: $(babayagaNLOlib) $(babayagaNLOshstamp)
	@/bin/echo "------> babayagaNLO : library ok"

$(babayagaNLOlib) :: $(bin)mtx_eeenudbb.o $(bin)mapmomenta.o $(bin)interface.o $(bin)Aint.o $(bin)userroutines.o $(bin)Asu3.o $(bin)sv.o $(bin)hadr5n.o $(bin)intpl.o $(bin)Rteubner.o $(bin)routines.o $(bin)phasespace.o $(bin)userinterface.o $(bin)Acp.o $(bin)sampling.o $(bin)distributions.o $(bin)matrix_model.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(babayagaNLOlib) $?
	$(lib_silent) $(ranlib) $(babayagaNLOlib)
	$(lib_silent) cat /dev/null >$(babayagaNLOstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(babayagaNLOlibname).$(shlibsuffix) :: $(babayagaNLOlib) $(babayagaNLOstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" babayagaNLO $(babayagaNLO_shlibflags)

$(babayagaNLOshstamp) :: $(babayagaNLOlibname).$(shlibsuffix)
	@if test -f $(babayagaNLOlibname).$(shlibsuffix) ; then cat /dev/null >$(babayagaNLOshstamp) ; fi

babayagaNLOclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)mtx_eeenudbb.o $(bin)mapmomenta.o $(bin)interface.o $(bin)Aint.o $(bin)userroutines.o $(bin)Asu3.o $(bin)sv.o $(bin)hadr5n.o $(bin)intpl.o $(bin)Rteubner.o $(bin)routines.o $(bin)phasespace.o $(bin)userinterface.o $(bin)Acp.o $(bin)sampling.o $(bin)distributions.o $(bin)matrix_model.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
babayagaNLOinstallname = $(library_prefix)babayagaNLO$(library_suffix).$(shlibsuffix)

babayagaNLO :: babayagaNLOinstall

install :: babayagaNLOinstall

babayagaNLOinstall :: $(install_dir)/$(babayagaNLOinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(babayagaNLOinstallname) :: $(bin)$(babayagaNLOinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(babayagaNLOinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(babayagaNLOinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(babayagaNLOinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(babayagaNLOinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(babayagaNLOinstallname) $(install_dir)/$(babayagaNLOinstallname); \
	      echo `pwd`/$(babayagaNLOinstallname) >$(install_dir)/$(babayagaNLOinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(babayagaNLOinstallname), no installation directory specified"; \
	  fi; \
	fi

babayagaNLOclean :: babayagaNLOuninstall

uninstall :: babayagaNLOuninstall

babayagaNLOuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(babayagaNLOinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(babayagaNLOinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(babayagaNLOinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(babayagaNLOinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),babayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)babayagaNLO_dependencies.make :: dirs

ifndef QUICK
$(bin)babayagaNLO_dependencies.make : $(src)Babayaga_NLO/mtx_eeenudbb.f $(src)Babayaga_NLO/mapmomenta.f $(src)Babayaga_NLO/interface.f $(src)Babayaga_NLO/Aint.f $(src)Babayaga_NLO/userroutines.f $(src)Babayaga_NLO/Asu3.f $(src)Babayaga_NLO/sv.f $(src)Babayaga_NLO/hadr5n.f $(src)Babayaga_NLO/intpl.f $(src)Babayaga_NLO/Rteubner.f $(src)Babayaga_NLO/routines.f $(src)Babayaga_NLO/phasespace.f $(src)Babayaga_NLO/userinterface.f $(src)Babayaga_NLO/Acp.f $(src)Babayaga_NLO/sampling.f $(src)Babayaga_NLO/distributions.f $(src)Babayaga_NLO/matrix_model.f $(use_requirements) $(cmt_final_setup_babayagaNLO)
	$(echo) "(babayagaNLO.make) Rebuilding $@"; \
	  $(build_dependencies) babayagaNLO -all_sources -out=$@ $(src)Babayaga_NLO/mtx_eeenudbb.f $(src)Babayaga_NLO/mapmomenta.f $(src)Babayaga_NLO/interface.f $(src)Babayaga_NLO/Aint.f $(src)Babayaga_NLO/userroutines.f $(src)Babayaga_NLO/Asu3.f $(src)Babayaga_NLO/sv.f $(src)Babayaga_NLO/hadr5n.f $(src)Babayaga_NLO/intpl.f $(src)Babayaga_NLO/Rteubner.f $(src)Babayaga_NLO/routines.f $(src)Babayaga_NLO/phasespace.f $(src)Babayaga_NLO/userinterface.f $(src)Babayaga_NLO/Acp.f $(src)Babayaga_NLO/sampling.f $(src)Babayaga_NLO/distributions.f $(src)Babayaga_NLO/matrix_model.f
endif

#$(babayagaNLO_dependencies)

-include $(bin)babayagaNLO_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(mtx_eeenudbb_f_dependencies)
 
$(bin)$(binobj)mtx_eeenudbb.o : $(mtx_eeenudbb_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/mtx_eeenudbb.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(mtx_eeenudbb_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(mtx_eeenudbb_fflags) $(mtx_eeenudbb_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/mtx_eeenudbb.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)mtx_eeenudbb.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(mtx_eeenudbb_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(mtx_eeenudbb_fflags) $(mtx_eeenudbb_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/mtx_eeenudbb.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(mapmomenta_f_dependencies)
 
$(bin)$(binobj)mapmomenta.o : $(mapmomenta_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/mapmomenta.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(mapmomenta_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(mapmomenta_fflags) $(mapmomenta_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/mapmomenta.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)mapmomenta.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(mapmomenta_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(mapmomenta_fflags) $(mapmomenta_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/mapmomenta.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(interface_f_dependencies)
 
$(bin)$(binobj)interface.o : $(interface_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/interface.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(interface_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(interface_fflags) $(interface_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/interface.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)interface.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(interface_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(interface_fflags) $(interface_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/interface.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(Aint_f_dependencies)
 
$(bin)$(binobj)Aint.o : $(Aint_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/Aint.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(Aint_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(Aint_fflags) $(Aint_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Aint.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)Aint.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(Aint_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(Aint_fflags) $(Aint_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Aint.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(userroutines_f_dependencies)
 
$(bin)$(binobj)userroutines.o : $(userroutines_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/userroutines.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(userroutines_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(userroutines_fflags) $(userroutines_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/userroutines.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)userroutines.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(userroutines_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(userroutines_fflags) $(userroutines_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/userroutines.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(Asu3_f_dependencies)
 
$(bin)$(binobj)Asu3.o : $(Asu3_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/Asu3.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(Asu3_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(Asu3_fflags) $(Asu3_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Asu3.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)Asu3.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(Asu3_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(Asu3_fflags) $(Asu3_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Asu3.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(sv_f_dependencies)
 
$(bin)$(binobj)sv.o : $(sv_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/sv.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(sv_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(sv_fflags) $(sv_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/sv.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sv.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(sv_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(sv_fflags) $(sv_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/sv.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(hadr5n_f_dependencies)
 
$(bin)$(binobj)hadr5n.o : $(hadr5n_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/hadr5n.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(hadr5n_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(hadr5n_fflags) $(hadr5n_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/hadr5n.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)hadr5n.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(hadr5n_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(hadr5n_fflags) $(hadr5n_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/hadr5n.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(intpl_f_dependencies)
 
$(bin)$(binobj)intpl.o : $(intpl_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/intpl.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(intpl_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(intpl_fflags) $(intpl_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/intpl.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)intpl.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(intpl_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(intpl_fflags) $(intpl_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/intpl.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(Rteubner_f_dependencies)
 
$(bin)$(binobj)Rteubner.o : $(Rteubner_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/Rteubner.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(Rteubner_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(Rteubner_fflags) $(Rteubner_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Rteubner.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)Rteubner.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(Rteubner_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(Rteubner_fflags) $(Rteubner_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Rteubner.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(routines_f_dependencies)
 
$(bin)$(binobj)routines.o : $(routines_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/routines.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(routines_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(routines_fflags) $(routines_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/routines.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)routines.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(routines_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(routines_fflags) $(routines_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/routines.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(phasespace_f_dependencies)
 
$(bin)$(binobj)phasespace.o : $(phasespace_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/phasespace.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(phasespace_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(phasespace_fflags) $(phasespace_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/phasespace.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)phasespace.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(phasespace_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(phasespace_fflags) $(phasespace_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/phasespace.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(userinterface_f_dependencies)
 
$(bin)$(binobj)userinterface.o : $(userinterface_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/userinterface.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(userinterface_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(userinterface_fflags) $(userinterface_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/userinterface.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)userinterface.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(userinterface_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(userinterface_fflags) $(userinterface_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/userinterface.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(Acp_f_dependencies)
 
$(bin)$(binobj)Acp.o : $(Acp_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/Acp.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(Acp_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(Acp_fflags) $(Acp_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Acp.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)Acp.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(Acp_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(Acp_fflags) $(Acp_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Acp.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(sampling_f_dependencies)
 
$(bin)$(binobj)sampling.o : $(sampling_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/sampling.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(sampling_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(sampling_fflags) $(sampling_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/sampling.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sampling.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(sampling_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(sampling_fflags) $(sampling_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/sampling.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(distributions_f_dependencies)
 
$(bin)$(binobj)distributions.o : $(distributions_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/distributions.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(distributions_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(distributions_fflags) $(distributions_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/distributions.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)distributions.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(distributions_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(distributions_fflags) $(distributions_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/distributions.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)babayagaNLO_dependencies.make : $(matrix_model_f_dependencies)
 
$(bin)$(binobj)matrix_model.o : $(matrix_model_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/matrix_model.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(matrix_model_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(matrix_model_fflags) $(matrix_model_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/matrix_model.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)matrix_model.o $(use_pp_fflags) $(babayagaNLO_pp_fflags) $(lib_babayagaNLO_pp_fflags) $(matrix_model_pp_fflags) $(use_fflags) $(babayagaNLO_fflags) $(lib_babayagaNLO_fflags) $(matrix_model_fflags) $(matrix_model_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/matrix_model.f

#-- end of fortran_library ------
#-- start of cleanup_header --------------

clean :: babayagaNLOclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(babayagaNLO.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(babayagaNLO.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(babayagaNLO.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(babayagaNLO.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_babayagaNLO)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(babayagaNLO.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(babayagaNLO.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(babayagaNLO.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

babayagaNLOclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library babayagaNLO
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)babayagaNLO$(library_suffix).a $(library_prefix)babayagaNLO$(library_suffix).s? babayagaNLO.stamp babayagaNLO.shstamp
#-- end of cleanup_library ---------------
