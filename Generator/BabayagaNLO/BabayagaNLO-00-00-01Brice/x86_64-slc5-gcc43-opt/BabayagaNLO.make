#-- start of make_header -----------------

#====================================
#  Library BabayagaNLO
#
#   Generated Thu Jun 21 15:42:32 2018  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_BabayagaNLO_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_BabayagaNLO_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_BabayagaNLO

BabayagaNLO_tag = $(tag)

#cmt_local_tagfile_BabayagaNLO = $(BabayagaNLO_tag)_BabayagaNLO.make
cmt_local_tagfile_BabayagaNLO = $(bin)$(BabayagaNLO_tag)_BabayagaNLO.make

else

tags      = $(tag),$(CMTEXTRATAGS)

BabayagaNLO_tag = $(tag)

#cmt_local_tagfile_BabayagaNLO = $(BabayagaNLO_tag).make
cmt_local_tagfile_BabayagaNLO = $(bin)$(BabayagaNLO_tag).make

endif

include $(cmt_local_tagfile_BabayagaNLO)
#-include $(cmt_local_tagfile_BabayagaNLO)

ifdef cmt_BabayagaNLO_has_target_tag

cmt_final_setup_BabayagaNLO = $(bin)setup_BabayagaNLO.make
#cmt_final_setup_BabayagaNLO = $(bin)BabayagaNLO_BabayagaNLOsetup.make
cmt_local_BabayagaNLO_makefile = $(bin)BabayagaNLO.make

else

cmt_final_setup_BabayagaNLO = $(bin)setup.make
#cmt_final_setup_BabayagaNLO = $(bin)BabayagaNLOsetup.make
cmt_local_BabayagaNLO_makefile = $(bin)BabayagaNLO.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)BabayagaNLOsetup.make

#BabayagaNLO :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'BabayagaNLO'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = BabayagaNLO/
#BabayagaNLO::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

BabayagaNLOlibname   = $(bin)$(library_prefix)BabayagaNLO$(library_suffix)
BabayagaNLOlib       = $(BabayagaNLOlibname).a
BabayagaNLOstamp     = $(bin)BabayagaNLO.stamp
BabayagaNLOshstamp   = $(bin)BabayagaNLO.shstamp

BabayagaNLO :: dirs  BabayagaNLOLIB
	$(echo) "BabayagaNLO ok"

#-- end of libary_header ----------------

BabayagaNLOLIB :: $(BabayagaNLOlib) $(BabayagaNLOshstamp)
	@/bin/echo "------> BabayagaNLO : library ok"

$(BabayagaNLOlib) :: $(bin)BabayagaNLORandom.o $(bin)Load.o $(bin)Entries.o $(bin)BabayagaNLO.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(BabayagaNLOlib) $?
	$(lib_silent) $(ranlib) $(BabayagaNLOlib)
	$(lib_silent) cat /dev/null >$(BabayagaNLOstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(BabayagaNLOlibname).$(shlibsuffix) :: $(BabayagaNLOlib) $(BabayagaNLOstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" BabayagaNLO $(BabayagaNLO_shlibflags)

$(BabayagaNLOshstamp) :: $(BabayagaNLOlibname).$(shlibsuffix)
	@if test -f $(BabayagaNLOlibname).$(shlibsuffix) ; then cat /dev/null >$(BabayagaNLOshstamp) ; fi

BabayagaNLOclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)BabayagaNLORandom.o $(bin)Load.o $(bin)Entries.o $(bin)BabayagaNLO.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
BabayagaNLOinstallname = $(library_prefix)BabayagaNLO$(library_suffix).$(shlibsuffix)

BabayagaNLO :: BabayagaNLOinstall

install :: BabayagaNLOinstall

BabayagaNLOinstall :: $(install_dir)/$(BabayagaNLOinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(BabayagaNLOinstallname) :: $(bin)$(BabayagaNLOinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(BabayagaNLOinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(BabayagaNLOinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(BabayagaNLOinstallname) $(install_dir)/$(BabayagaNLOinstallname); \
	      echo `pwd`/$(BabayagaNLOinstallname) >$(install_dir)/$(BabayagaNLOinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(BabayagaNLOinstallname), no installation directory specified"; \
	  fi; \
	fi

BabayagaNLOclean :: BabayagaNLOuninstall

uninstall :: BabayagaNLOuninstall

BabayagaNLOuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(BabayagaNLOinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(BabayagaNLOinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),BabayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)BabayagaNLO_dependencies.make :: dirs

ifndef QUICK
$(bin)BabayagaNLO_dependencies.make : $(src)BabayagaNLORandom.cxx $(src)Load.cxx $(src)Entries.cxx $(src)BabayagaNLO.cxx $(use_requirements) $(cmt_final_setup_BabayagaNLO)
	$(echo) "(BabayagaNLO.make) Rebuilding $@"; \
	  $(build_dependencies) BabayagaNLO -all_sources -out=$@ $(src)BabayagaNLORandom.cxx $(src)Load.cxx $(src)Entries.cxx $(src)BabayagaNLO.cxx
endif

#$(BabayagaNLO_dependencies)

-include $(bin)BabayagaNLO_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BabayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BabayagaNLORandom.d

$(bin)$(binobj)BabayagaNLORandom.d : $(use_requirements) $(cmt_final_setup_BabayagaNLO)

$(bin)$(binobj)BabayagaNLORandom.d : $(src)BabayagaNLORandom.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BabayagaNLORandom.dep $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(BabayagaNLORandom_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(BabayagaNLORandom_cppflags) $(BabayagaNLORandom_cxx_cppflags)  $(src)BabayagaNLORandom.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BabayagaNLORandom.o $(src)BabayagaNLORandom.cxx $(@D)/BabayagaNLORandom.dep
endif
endif

$(bin)$(binobj)BabayagaNLORandom.o : $(src)BabayagaNLORandom.cxx
else
$(bin)BabayagaNLO_dependencies.make : $(BabayagaNLORandom_cxx_dependencies)

$(bin)$(binobj)BabayagaNLORandom.o : $(BabayagaNLORandom_cxx_dependencies)
endif
	$(cpp_echo) $(src)BabayagaNLORandom.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(BabayagaNLORandom_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(BabayagaNLORandom_cppflags) $(BabayagaNLORandom_cxx_cppflags)  $(src)BabayagaNLORandom.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BabayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Load.d

$(bin)$(binobj)Load.d : $(use_requirements) $(cmt_final_setup_BabayagaNLO)

$(bin)$(binobj)Load.d : $(src)Load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Load.dep $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(Load_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(Load_cppflags) $(Load_cxx_cppflags)  $(src)Load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Load.o $(src)Load.cxx $(@D)/Load.dep
endif
endif

$(bin)$(binobj)Load.o : $(src)Load.cxx
else
$(bin)BabayagaNLO_dependencies.make : $(Load_cxx_dependencies)

$(bin)$(binobj)Load.o : $(Load_cxx_dependencies)
endif
	$(cpp_echo) $(src)Load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(Load_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(Load_cppflags) $(Load_cxx_cppflags)  $(src)Load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BabayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Entries.d

$(bin)$(binobj)Entries.d : $(use_requirements) $(cmt_final_setup_BabayagaNLO)

$(bin)$(binobj)Entries.d : $(src)Entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Entries.dep $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(Entries_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(Entries_cppflags) $(Entries_cxx_cppflags)  $(src)Entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Entries.o $(src)Entries.cxx $(@D)/Entries.dep
endif
endif

$(bin)$(binobj)Entries.o : $(src)Entries.cxx
else
$(bin)BabayagaNLO_dependencies.make : $(Entries_cxx_dependencies)

$(bin)$(binobj)Entries.o : $(Entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)Entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(Entries_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(Entries_cppflags) $(Entries_cxx_cppflags)  $(src)Entries.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BabayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BabayagaNLO.d

$(bin)$(binobj)BabayagaNLO.d : $(use_requirements) $(cmt_final_setup_BabayagaNLO)

$(bin)$(binobj)BabayagaNLO.d : $(src)BabayagaNLO.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BabayagaNLO.dep $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(BabayagaNLO_cppflags) $(BabayagaNLO_cxx_cppflags)  $(src)BabayagaNLO.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BabayagaNLO.o $(src)BabayagaNLO.cxx $(@D)/BabayagaNLO.dep
endif
endif

$(bin)$(binobj)BabayagaNLO.o : $(src)BabayagaNLO.cxx
else
$(bin)BabayagaNLO_dependencies.make : $(BabayagaNLO_cxx_dependencies)

$(bin)$(binobj)BabayagaNLO.o : $(BabayagaNLO_cxx_dependencies)
endif
	$(cpp_echo) $(src)BabayagaNLO.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(BabayagaNLO_cppflags) $(BabayagaNLO_cxx_cppflags)  $(src)BabayagaNLO.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: BabayagaNLOclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(BabayagaNLO.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(BabayagaNLO.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(BabayagaNLO.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(BabayagaNLO.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_BabayagaNLO)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BabayagaNLO.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BabayagaNLO.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BabayagaNLO.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

BabayagaNLOclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library BabayagaNLO
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)BabayagaNLO$(library_suffix).a $(library_prefix)BabayagaNLO$(library_suffix).s? BabayagaNLO.stamp BabayagaNLO.shstamp
#-- end of cleanup_library ---------------
