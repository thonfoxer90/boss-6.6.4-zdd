      subroutine initbabayaga
! written by CMCC, last modified 29/8/2010
      implicit double precision (a-h,o-z)
      integer*8 npoints,k,naccepted,iwriteout
      parameter (mph=40)
      dimension sump(0:mph-1),sum2p(0:mph-1)
      dimension fractions(0:mph-1)
      dimension pin(0:3),p1(0:3),p2(0:3),qph(mph,0:3),q(0:3)
      dimension pin1(0:3),pin2(0:3),ptmp(0:3),pbeam1(0:3),pbeam2(0:3)
      dimension p1b(0:3),p2b(0:3)
      dimension p1fsr(0:3),p2fsr(0:3),pcm(0:3)
      character*6   ord
      character*10  model
      character*100 outfile,storefile
      character*3   eventlimiter,store
*** filled in the subroutine userinterface
      character*2 fs
      common/finalstate/fs
      common/ecms/ecms
      common/nphot_mode/nphotmode
      common/expcuts/thmin,thmax,emin,zmax,egmin,thgmin,thgmax
      common/zparameters/zm,gz,ve,ae,rv,ra,wm,s2th,gfermi,sqrt2,um
      common/epssoft/eps
      common/energiabeam/ebeam
      common/parameters/ame,ammu,convfac,alpha,pi
      common/intinput/iwriteout,iseed,nsearch,iverbose
      common/qedORDER/ord
      common/charinput/model,eventlimiter,store,storefile,outfile
      common/realinput/anpoints,sdifmax
      common/nmaxphalpha/nphmaxalpha
      common/ialpharunning/iarun
      common/idebugging/idebug
      common/regolatori/regulator1,regulator2
*********************************************
      common/ggreg/ipair
      common/coseni/cmax,cmin
      common/channelref/iref
      common/momentainitial/pin1,pin2
      common/radpattern/nph(4)
      common/forborncrosssection/phsp2b,flux2b,bornme,bornmeq2,bornmez
      common/reducedtoborn/p1b,p2b
      common/samplelimits/spmin,omaxext
      common/various/beta,betafs
      common/for_debug/ppp(0:3),denphsp,dir3(0:3),dir4(0:3),br,bq,onm
      common/funvalues/funa,funb,func,funm
      common/enableifirst/ifrstenbld

      common/idarkon/idarkon
      common/darkmatter/amassU,gammaU,gvectU,gaxU

******
      common/babayagainit/am1,am2,sum,sum2,sump,sum2p,xsec,var,subm1,
     .     subm2,bornmax,phspmax,wmax,prodmax,elmmax,hitpmiss,hit,
     .     sumover,sum2over,sumneg,sum2neg,wnpoints
      common/babayagainitint/npoints,in_conf_spin,nphmax,naccepted,
     .     nwhenmax,nover,istopsearch,nneg,ng
*****
!      call userinterface
!      if (store.eq.'yes') call initstorage(storefile)

      if (idarkon.eq.1) then
        if (gammaU.lt.0.) then
          gammaU=gammafunc(amassU,gvectU)
        endif
        if (amassU.eq.0.) then
          amassU=1.d-13
        endif
        gvectU=gvectU*sqrt(4.*pi*alpha)
        gaxU=gaxU*sqrt(4.*pi*alpha)
        call init_apar
      endif

      idebug = 0
      cmax   = dcos(thmin)
      cmin   = dcos(thmax)
      npoints = anpoints

      am1 = ame
      am2 = ame
      if (fs.eq.'gg') then
         am1 = 0.d0
         am2 = 0.d0
      endif
      if (fs.eq.'mm') then
         am1 = ammu
         am2 = ammu
      endif
      in_conf_spin = 4

      sum = 0.d0
      sum2 = 0.d0
      do ki = 0,mph-1
         sump(ki) = 0.d0
         sum2p(ki) = 0.d0
      enddo

      nphmax       = 0
      xsec         = 0.d0
      var          = 0.1d0
      naccepted    = 0
      sumb1        = 0.d0
      sumb2        = 0.d0
      bornmax      = 0.d0
      phspmax      = 0.d0
      nwhenmax     = 0
      wmax         = 0.d0
      prodmax      = 0.d0
      elmmax       = 0.d0
      nover        = 0
      hitpmiss     = 0.d0
      hit          = 0.d0
      istopsearch  = 0
      nneg         = 0
      sumover      = 0.d0
      sum2over     = 0.d0
      sumneg       = 0.d0
      sum2neg      = 0.d0

      wnpoints = npoints
      ng = 0

      return
      end
*
      subroutine babayaga(p1out,p2out,p3out,p4out,qphout)
! derived from the previous main routine
! written by CMCC, last modified 31/8/2010
! p1out(0..3)         = initial state e- momentum in the CM system
! p2out(0..3)         = initial state e+ momentum in the CM system
! p3out(0..3)         = final state e- momentum (fs=ee) in the CM system
!                    mu- momentum (fs=mm)  in the CM system
!                    most energetic photon momentum (fs=gg)
! p4out(0..3)         = final state e+ momentum (fs=ee) in the CM system
!                    mu+ momentum (fs=mm) in the CM system
!                    next-to-most energetic photon momentum (fs=gg)  in the CM system
! qphout(1...40,0..3) = extra photon momenta in the CM system. The photon momenta
!                    are sorted in energy, from the most energetic to the less
      implicit double precision (a-h,o-z)
      integer*8 npoints,k,naccepted,iwriteout
      parameter (mph=40)
      real*8 csi(1)
      dimension sump(0:mph-1),sum2p(0:mph-1)
      dimension fractions(0:mph-1)
      dimension p1out(0:3),p2out(0:3),p3out(0:3),p4out(0:3),
     .     qphout(mph,0:3)       ! output momenta
      dimension xsecp(0:mph-1),varp(0:mph-1)
      dimension pin(0:3),p1(0:3),p2(0:3),qph(mph,0:3),q(0:3)
      dimension pin1(0:3),pin2(0:3),ptmp(0:3),pbeam1(0:3),pbeam2(0:3)
      dimension p1b(0:3),p2b(0:3)
      dimension p1fsr(0:3),p2fsr(0:3),pcm(0:3)
      integer isvec(25)
      character*6   ord
      character*10  model
      character*100 outfile,storefile
      character*3   eventlimiter,store
      common/beforesort/p1o(0:3),p2o(0:3),qpho(40,0:3)
      common/testsdebug/elmat2cmn,e2approx,s12,s34,t24,t13,u14,u23
      common/debug2/p1mod,odv
      common/eg_branchsampling/ibranch
*** filled in the subroutine userinterface
      character*2 fs
      common/finalstate/fs
      common/ecms/ecms
      common/nphot_mode/nphotmode
      common/expcuts/thmin,thmax,emin,zmax,egmin,thgmin,thgmax
      common/zparameters/zm,gz,ve,ae,rv,ra,wm,s2th,gfermi,sqrt2,um
      common/epssoft/eps
      common/parameters/ame,ammu,convfac,alpha,pi
      common/intinput/iwriteout,iseed,nsearch,iverbose
      common/qedORDER/ord
      common/charinput/model,eventlimiter,store,storefile,outfile
      common/realinput/anpoints,sdifmax
      common/nmaxphalpha/nphmaxalpha
      common/ialpharunning/iarun
      common/idebugging/idebug
      common/regolatori/regulator1,regulator2
*********************************************
      common/ggreg/ipair
      common/coseni/cmax,cmin
      common/channelref/iref
      common/momentainitial/pin1,pin2
      common/radpattern/nph(4)
      common/forborncrosssection/phsp2b,flux2b,bornme,bornmeq2,bornmez
      common/reducedtoborn/p1b,p2b
      common/samplelimits/spmin,omaxext
      common/various/beta,betafs
      common/for_debug/ppp(0:3),denphsp,dir3(0:3),dir4(0:3),br,bq,onm
      common/funvalues/funa,funb,func,funm
      common/enableifirst/ifrstenbld

      common/idarkon/idarkon
      common/darkmatter/amassU,gammaU,gvectU,gaxU

******
      common/babayagainit/am1,am2,sum,sum2,sump,sum2p,xsec,var,subm1,
     .     subm2,bornmax,phspmax,wmax,prodmax,elmmax,hitpmiss,hit,
     .     sumover,sum2over,sumneg,sum2neg,wnpoints
      common/babayagainitint/npoints,in_conf_spin,nphmax,naccepted,
     .     nwhenmax,nover,istopsearch,nneg,ng
      common/numberofphotons/numofphotinqph
*****
      common/fmaxcommon/fmax
      common/evtcounter/k
      common/alreadyfound/imaxfound,kloopunw
      data imaxfound /0/
      data k /-1/
      logical getnew,event_is_unw
      common/eventinfo/getnew,event_is_unw

*** this routine returns only when the event is UNWEIGHTED

** [[begin initialization]]
      if (k.eq.-1) then
         print*,' '
         print*,'Looking for the maximum before'//
     .        ' starting events generation...'
         k = 0

         kloopunw = 0

      endif

      event_is_unw = .false.

c:::::::::::::::::::::::::::::::::::::::
      kunw = 0
      do while(kunw.eq.0)
      ie = 0
c:::::::::::::::::::::::::::::::::::::::

**** THE FOLLOWING LINES BETWEEN "*[[ start" AND "* end ]]" MUST
**** BE RECALCULATED FOR EACH EVENT IF ECMS CHANGES EVENT BY EVENT.
**** FURTHERMORE, IF ECMS CHANGES EVENT BY EVENT ifrstenbld MUST 
**** BE SET TO 0
*[[ start

!! spreading center of mass energy!!
      call spreadecms
!!!

      ifrstenbld = 0
      esoft = eps * ecms/2.d0
      pin(0) = ecms
      pin(1) = 0.d0
      pin(2) = 0.d0
      pin(3) = 0.d0
      betafs = sqrt(1.d0 - 4.d0*am1**2/pin(0)**2)
      beta   = sqrt(1.d0 - 4.d0*ame**2/pin(0)**2)
      pin1(0) = pin(0)/2.d0
      pin1(1) = 0.d0
      pin1(2) = 0.d0
      pin1(3) = -beta * pin1(0)
      pin2(0) = pin(0)/2.d0
      pin2(1) = 0.d0
      pin2(2) = 0.d0
      pin2(3) = beta * pin2(0)
      do ki = 0,3
         pbeam1(ki) = pin1(ki)
         pbeam2(ki) = pin2(ki)
         ptmp(ki)   = pin1(ki)+pin2(ki)
      enddo
      s = dot(ptmp,ptmp)

*     end ]]
      
*     * [[end initialization]]
      
      k = k + 1
      kloopunw = kloopunw + 1

      flux = 8.d0 * (ecms/2.d0)**2

      if (fs.eq.'ee'.or.fs.eq.'mm') call get_cos_fer(cth,wcos)
      
      call multiplicity(eps,ecms,cth,ng,wnphot)
      
      numofphotinqph = ng

      if (fs.eq.'gg') then
         if (ng.eq.0) then 
            call get_cos_2g_born(cth,wcos)
         else
            call get_cos_2g(cth,wcos)
         endif
      endif
      sdif = wnphot*wcos

      if (ord.eq.'struct') then
         call emission(pbeam1,pbeam2,wisr,'e')
         sdif = sdif * wisr
      endif
     
      call phasespace(pbeam1,pbeam2,p1,p2,qph,ng,am1,am2,
     .     esoft,cth,w,phsp,ie)

ccc      print*,'printphsp ',phsp
      
      if (ord.eq.'struct') then
         do kk = 0,3
            p1fsr(kk) = p1(kk)
            p2fsr(kk) = p2(kk)
            q(kk) = qph(1,kk)
            pcm(kk) = pbeam1(kk)+pbeam2(kk)
         enddo
         call new_boost(pcm,p1fsr,p1fsr,-1)
         call new_boost(pcm,p2fsr,p2fsr,-1)
         call new_boost(pcm,q,q,-1)
         if (fs.eq.'mm') call emission(p1fsr,p2fsr,wfsr,'m')
         if (fs.eq.'ee') call emission(p1fsr,p2fsr,wfsr,'e')
      endif
      
      if (ie.ge.1) ie = 1
      if (fs.eq.'gg'.and.ie.eq.0) then
         call mixandsortmomenta(ng,p1,p2,qph)
      endif
      
      sdif = sdif * phsp * w

      if (ie.lt.1) then 
         if (idarkon.eq.1) then
            if (ord.eq.'struct') then
               call cutsdm(p1fsr,p2fsr,qph,icut,ng)
            else
               call cutsdm(p1,p2,qph,icut,ng)
            endif
         elseif (idarkon.eq.0) then
            if (fs.eq.'ee'.or.fs.eq.'mm') then
               call cuts(p1,p2,qph,icut)
            elseif (fs.eq.'gg') then
               call cutsgg(ng,p1,p2,qph,icut)
            endif
         endif
      else
         icut = 1
      endif
      ie = ie + icut
      
      if (ng.gt.nphmax.and.ie.eq.0) nphmax = ng
      if (icut.eq.0.and.imaxfound.eq.1) naccepted = naccepted + 1

!     !         ie = 1   ! uncomment  for phase space integral

      call squared_matrix(model,ng,ecms,p1,p2,pbeam1,pbeam2,qph,
     >     ie,icalc,emtx,prod)

      bck  = emtx
      emtx = emtx/in_conf_spin  ! divided by initial spin conf. 
      emtx = emtx*convfac/flux  ! divided by the flux
      
      if (fs.eq.'gg') emtx = emtx/(ng+2)/(ng+1)
      
      if (ie.eq.0) then
         call svfactor(model,ng,ecms,p1,p2,eps,sv,deltasv)
         sdif = sdif * sv
      else
         sdif = 0.d0
      endif
      
      sdif = sdif * emtx
      
      if (sdif.lt.0.d0) then
         sdif = 0.d0
         w    = 0.d0
         ie   = 1
      endif
      
      if (ord.eq.'struct'.and.ie.eq.0) then
         do kk = 0,3
            p1(kk) = p1fsr(kk)
            p2(kk) = p2fsr(kk)
         enddo
         sdif = sdif * wfsr
      endif

! rescaling for Z exchange (bornmez = 0 for gg...)
      if (abs(bornme).gt.0.d0) sdif = sdif/bornme*(bornme + bornmez)

!!         emtx = 1.d0 ! uncomment  for phase space integral
      iii = 0
      if (sdif.gt.sdifmax) then 
         sdifmax = sdif
         nwhenmax = ng
         iii = 1
      endif
      if (istopsearch.eq.0) then            
         if (iverbose.gt.0.and.iii.eq.1) then
            call printstatus(1,k,p1,p2,qph,xsec,var,varbefore,sdif,
     .           sdifmax,fmax)
         endif
      endif
      
!! unweightening for unweighted events...
      if (k.gt.nsearch) then
         if (imaxfound.eq.0) then
            k = k - nsearch     !!!! ONLY ONCE!!!
         endif
         imaxfound = 1
         istopsearch = 1
      endif

      if (imaxfound.eq.1) then
         if (hitpmiss.lt.1.d0) then
            fmax = 1.05d0*sdifmax
            print*,'... finished looking for the maximum.'//
     .           ' Starting events generation!'
            print*,' '
         endif
         hitpmiss = hitpmiss + 1.d0
         call wraprng(csi,1)

         if (fmax*csi(1).lt.sdif) then
            hit = hit + 1.d0
! it happens only when k is > nsearch !! cool...
            event_is_unw = .true. 
            do kkk = 0,3
               p1out(kkk) = pbeam1(kkk) 
               p2out(kkk) = pbeam2(kkk) 
               p3out(kkk) = p1(kkk)
               p4out(kkk) = p2(kkk)
               if (fs.ne.'gg') then
                  call orderqm(ng,qph,qphout)
               else
                  do iii = 1,mph
                     qphout(iii,kkk)=qph(iii,kkk)
                  enddo
               endif
            enddo
!            if (store.eq.'yes') call eventstorage(p1,p2,qph)
         endif
         if (sdif.lt.-1.d-20) then 
            nneg  = nneg  + 1
            sumneg  = sumneg + abs(sdif)
            sum2neg = sum2neg + sdif**2
         endif
         if (sdif.gt.fmax) then 
            nover = nover + 1
            sumover  = sumover  + sdif - fmax
            sum2over = sum2over + (sdif - fmax)**2

            if (iverbose.gt.0) then
               call printstatus(2,k,p1,p2,qph,xsec,var,varbefore,
     .              sdif,sdifmax,fmax)
            endif
         endif
      endif
      
      if (imaxfound.eq.0) sdif = 0.d0

      sum  = sum  + sdif
      sum2 = sum2 + sdif**2
      
      if (ibranch.eq.1) sumb1 = sumb1 + sdif
      if (ibranch.eq.2) sumb2 = sumb2 + sdif
      
      sump(ng)  = sump(ng)  + sdif
      sum2p(ng) = sum2p(ng) + sdif**2
      
      varbefore = var
      xsec = sum/k
      var  = sqrt(abs((sum2/k-xsec**2)/k))
      tollerate = 2.d0
      if (var.gt.tollerate*varbefore
     .     .and.varbefore.gt.0.d0.and.ie.eq.0) then
         if (iverbose.gt.0) then
            call printstatus(3,k,p1,p2,qph,xsec,var,varbefore,sdif,
     .           sdifmax,fmax)
         endif
!---------- Nullifying this event ------------!
         ratio = var/varbefore
c     if (k.gt.5000000.and.ratio.gt.3) then
         if (k.gt.5000000.and.ratio.gt.10) then
            print*,' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
            print*,' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
            print*,' >>>>>>>> REJECTING THE EVENT <<<<<<<<'
            print*,' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
            print*,' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
            if (iverbose.gt.0) then
               call printstatus(4,k,p1,p2,qph,xsec,var,varbefore,
     .              sdif,sdifmax,fmax)
            endif
            sum = sum - sdif
            sum2 = sum2 - (sdif)**2
            sump(ng) = sump(ng) - sdif
            sum2p(ng) = sum2p(ng) - (sdif**2)
            xsec = sum/k
            var  = sqrt((sum2/k-xsec**2)/k)
            sdif = 0.d0
         endif
      endif
*
      if (icalc.eq.1.and.ie.eq.0.and.imaxfound.eq.1) then
         call distributions(sdif,k,p1,p2,qph)
      endif
*
**[[WRITING OUTPUT]]
      if (mod(k,iwriteout).eq.0.or.k.eq.npoints.or.hit.eq.npoints)
     .     then
c         if (abs(sum).gt.0.d0) call writedistributions
c         call printoutput
      endif
**[[END WRITING OUTPUT]]
      if (eventlimiter.eq.'unw') wnpoints = 1.d13
      if (eventlimiter.eq.'unw'.and.hit.ge.npoints) then 
         istop = 1
         getnew = .false.
!         if (store.eq.'yes') call finalizestorage
         print*,'Generation finished'
      endif
      
      if (k.eq.wnpoints) then 
         getnew = .false.
         print*,'Generation finished'
      endif

c:::::::::::::::::::::::::::::::::::::::
      if (event_is_unw) kunw = 1
      enddo
c:::::::::::::::::::::::::::::::::::::::
      return
      end
*
      subroutine printoutput
      implicit double precision (a-h,o-z)
      integer*8 npoints,k,naccepted,iwriteout
      parameter (mph=40)
      dimension sump(0:mph-1),sum2p(0:mph-1)
      dimension fractions(0:mph-1)
      dimension xsecp(0:mph-1),varp(0:mph-1)
      character*6   ord
      character*10  model
      character*100 outfile,storefile
      character*100 stringa
      character*3   eventlimiter,store
*** filled in the subroutine userinterface
      character*2 fs
      common/finalstate/fs
      common/ecms/ecms
      common/ecmsininput/ecmsinput,beamspread
      common/nphot_mode/nphotmode
      common/expcuts/thmin,thmax,emin,zmax,egmin,thgmin,thgmax
      common/zparameters/zm,gz,ve,ae,rv,ra,wm,s2th,gfermi,sqrt2,um
      common/epssoft/eps
      common/energiabeam/ebeam
      common/parameters/ame,ammu,convfac,alpha,pi
      common/intinput/iwriteout,iseed,nsearch,iverbose
      common/qedORDER/ord
      common/charinput/model,eventlimiter,store,storefile,outfile
      common/realinput/anpoints,sdifmax
      common/nmaxphalpha/nphmaxalpha
      common/ialpharunning/iarun
*********************************************
      common/ggreg/ipair
      common/coseni/cmax,cmin
      common/channelref/iref

      common/idarkon/idarkon
      common/darkmatter/amassU,gammaU,gvectU,gaxU

      common/babayagainit/am1,am2,sum,sum2,sump,sum2p,xsec,var,subm1,
     .     subm2,bornmax,phspmax,wmax,prodmax,elmmax,hitpmiss,hit,
     .     sumover,sum2over,sumneg,sum2neg,wnpoints
      common/babayagainitint/npoints,in_conf_spin,nphmax,naccepted,
     .     nwhenmax,nover,istopsearch,nneg,ng

      common/fmaxcommon/fmax
      common/evtcounter/k


!! hit or miss cross section...
      hmxsect = 0.d0
      hmerr   = 0.d0
      if (hitpmiss.gt.0.d0)  then 
         hmeff   = hit/hitpmiss
         hmxsect = fmax*hmeff
         hmerr   = fmax * sqrt(hmeff*(1-hmeff)/hitpmiss)
      endif
!     !            
      xsec = sum/k
      var  = sqrt((abs(sum2/k-xsec**2))/k)            
      write(stringa,*)' '
      print*,stringa(1:79)
      write(stringa,'(1x,A,A,A)')
     >     'final state = ',fs,' '
      print*,stringa(1:79)
      write(stringa,'(1x,A,f12.4,A)')
     >     'ecms   =',ecmsinput,' GeV'
      print*,stringa(1:79)
      write(stringa,'(1x,A,f12.4,A)')
     >     'beam spread   =',beamspread,' GeV'
      print*,stringa(1:79)
      write(stringa,'(1x,A,f12.4,A)')
     >     'thmin  =',thmin*180.d0/pi,' deg'
      print*,stringa(1:79)
      write(stringa,'(1x,A,f12.4,A)')
     >     'thmax  =',thmax*180.d0/pi,' deg'
      print*,stringa(1:79)
      write(stringa,'(1x,A,f12.4,A)')
     >     'acoll. =',zmax*180.d0/pi,' deg'
      print*,stringa(1:79)
      write(stringa,'(1x,A,f12.4,A)')
     >     'emin   = ',emin,' GeV'
      print*,stringa(1:79)
      write(stringa,'(1x,A)')
     >     'ord    = '//ord
      print*,stringa(1:79)
      write(stringa,'(1x,A)')
     >     'model  = '//model
      print*,stringa(1:79)
      write(stringa,'(1x,A,i5)')
     >     'nphot mode =',nphotmode
      print*,stringa(1:79)
      write(stringa,'(1x,A,i5)')
     >     'iarun  =',iarun
      print*,stringa(1:79)
      write(stringa,'(1x,A,f10.9)')
     >     'eps    = ',eps
      print*,stringa(1:79)
      write(stringa,'(1x,A,i5)')
     >     'darkmod =',idarkon
      print*,stringa(1:79)
      if(idarkon.eq.1) then
         write(stringa,'(1x,A,es14.7,A)')
     >        'U mass  = ',amassU, ' GeV'
         print*,stringa(1:79)
         write(stringa,'(1x,A,es14.7,A)')
     >        'U width = ',gammaU, ' GeV'
         print*,stringa(1:79)
         write(stringa,'(1x,A,e14.7,A)')
     >        'k       = ',gvectU/sqrt(4.*pi*alpha), 
     >        ' * electric charge'
         print*,stringa(1:79)
         write(stringa,'(1x,A,f7.3,A)')
     >        'egmin   = ', egmin,  ' GeV'
         print*,stringa(1:79)
         write(stringa,'(1x,A,f7.3,A)')
     >        'thgmin  = ', thgmin*180.d0/pi, ' deg'
         print*,stringa(1:79)
         write(stringa,'(1x,A,f7.3,A)')
     >        'thgmax  = ', thgmax*180.d0/pi, ' deg'
         print*,stringa(1:79)
      endif
      write(stringa,*)' '
      print*,stringa(1:79)
c      if (eventlimiter.eq.'w') then
c         write(stringa,'(A,f12.0,A)')'~ Generating ',wnpoints,
c     .        ' weighted events ~'
c         print*,stringa(1:79)
c      else
c         write(stringa,'(A,i12,A)')'~ Generating ',npoints,
c     .        ' unweighted events ~'
c         print*,stringa(1:79)
c      endif
      write(stringa,*)' '
      print*,stringa(1:79)
      write(stringa,*)'::::::>>>>>> weighted events <<<<<<::::::'
      print*,stringa(1:79)
      do i = 0,nphmax
         xsecp(i) = sump(i)/k
         varp(i)  = sqrt((abs(sum2p(i)/k-xsecp(i)**2))/k)
         fractions(i) = 0.d0
         if (abs(xsec).gt.0.d0) fractions(i) = xsecp(i)/xsec * 100.d0
         iii = i
         if (fs.eq.'gg') iii = i + 2
         write(stringa,'(i2,A,f16.8,A,f16.8,A,f8.4,A)')
     .        iii,' photons: ',xsecp(i),' +-',varp(i),
     .        '     (',fractions(i),' %)'
         print*,stringa(1:79)
      enddo
      write(stringa,'(1x,A,f16.8,A,f16.8,A)')
     .     'total:   ',xsec,' +-',var,' nb'
      print*,stringa(1:79)
      write(stringa,*)' '
      print*,stringa(1:79)
      if (eventlimiter.eq.'w') then
         write(stringa,'(1x,A,i12,A,f20.0,A,i10.0,A)')
     .        'weighted event n. ',k,' of ',wnpoints,
     .        '     (nsearch =',nsearch,')'
         print*,stringa(1:79)
      else
         write(stringa,'(1x,A,i12,A,i10.0,A)')
     .        'weighted event n. ',k,!' of ',wnpoints
     .        '     (nsearch =',nsearch,')'
         print*,stringa(1:79)
      endif
      eff = (1.d0*naccepted)/k
      write(stringa,'(1x,A,f6.2,A)')
     .     'cut points ',100.d0 - eff*100,' %'
      print*,stringa(1:79)
      write(stringa,*)'::::::>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<::::::'
      print*,stringa(1:79)
      write(stringa,*)' '
      print*,stringa(1:79)
      write(stringa,*)'::::::>>>>>> unweighted events <<<<<<::::::'
      print*,stringa(1:79)
      write(stringa,'(1x,A,f6.2,A)')
     :     'hit or miss efficiency ',hmeff*100,' %'
      print*,stringa(1:79)
      write(stringa,'(1x,A,f12.0)')
     :     'unweighted events generated ',hit
      print*,stringa(1:79)
      biashit        = 0.d0
      biashitpmiss   = 0.d0
      biasneghit     = 0.d0
      biasneghitmiss = 0.d0
      sezover        = 0.d0
      errsezover     = 0.d0
      sezneg         = 0.d0
      errsezneg      = 0.d0
      if (hit.gt.0.d0) then 
         biashit      = 1.d0*nover/hit
         biashitpmiss = 1.d0*nover/hitpmiss
         biasneghit   = 1.d0*nneg/hit
         biasneghitmiss = 1.d0*nneg/hitpmiss
         sezover    = sumover/hitpmiss
         errsezover = (sum2over/hitpmiss - sezover**2)/hitpmiss
         errsezover = sqrt(abs(errsezover))
         sezneg    = sumneg/hitpmiss
         errsezneg = (sum2neg/hitpmiss - sezneg**2)/hitpmiss
         errsezneg = sqrt(abs(errsezneg))
      endif
      write(stringa,*)' '
      print*,stringa(1:79)
      write(stringa,'(1x,A,f16.8,A,f16.8,A)')
     :     'total (nb):   ',hmxsect,' +-',hmerr,' +'
      print*,stringa(1:79)
      write(stringa,'(1x,A,f16.8,A,f16.8,A)')
     :     '       !!!!   ',sezover,' +-',
     :     errsezover,' (bias over fmax) +'
      print*,stringa(1:79)
      write(stringa,'(1x,A,f16.8,A,f16.8,A)')
     :     '       !!!!   ',-sezneg,' +-',
     :     errsezneg,' (bias negative)'
      print*,stringa(1:79)
      write(stringa,'(1x,A,f16.8,A,f16.8)')
     :     'total + biases: ',hmxsect
     :     +sezover-sezneg,' +-',hmerr+errsezover+errsezneg
      print*,stringa(1:79)
      write(stringa,*)' '
      print*,stringa(1:79)
      write(stringa,'(1x,A,i12)')
     :     'N. points with w > fmax (bias): ',nover
      print*,stringa(1:79)
      write(stringa,'(1x,A,f10.7,A,f10.7,A)')
     :     'bias/hit and bias/(hit+missed):',
     :     biashit*100,' % and ',biashitpmiss*100,' %'
      print*,stringa(1:79)
      write(stringa,'(1x,A,i12)')
     :     'N. points with w < 0:',nneg
      print*,stringa(1:79)
      write(stringa,'(1x,A,f10.5,A,f10.5,A)')
     :     'biases for w < 0:   ',
     :     biasneghit*100,' % and ',biasneghitmiss*100,' %'
      print*,stringa(1:79)
      write(stringa,*)' '
      print*,stringa(1:79)
      write(stringa,'(1x,A,f14.6,A,f14.6)')
     :     'Upper limits fmax and sdifmax ',fmax,' ',sdifmax
      print*,stringa(1:79)
      nwhenprint = nwhenmax
      if (fs.eq.'gg') nwhenprint = nwhenmax+2
      write(stringa,'(1x,A,i3,A)')
     :     'when there were ',nwhenprint,' photons'
      print*,stringa(1:79)
      write(stringa,*)'::::::>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<::::::'
      print*,stringa(1:79)
      write(stringa,*)' '
      print*,stringa(1:79)
      
      return
      end
* 
      subroutine printstatus(icode,kl,p1,p2,qph,xs,var,varb,sd,sdm,fm)
      implicit double precision (a-h,o-z)
      integer*8 kl,iwriteout
      character*12 col(20)
      common/colors/col
      dimension p1(0:3),p2(0:3),qph(40,0:3),q(0:3)
      dimension pin1(0:3),pin2(0:3),ptmp(0:3)
      common/ifirstprst/ifirst,icount
      data ifirst /1/
      data icount /0/
      character*2 fs
      common/finalstate/fs
      common/ecms/ecms
      common/nphot_mode/nphotmode
      common/expcuts/thmin,thmax,emin,zmax,egmin,thgmin,thgmax
      common/zparameters/zm,gz,ve,ae,rv,ra,wm,s2th,gfermi,sqrt2,um
      common/energiabeam/ebeam
      common/parameters/ame,ammu,convfac,alpha,pi
      common/intinput/iwriteout,iseed,nsearch,iverbose
      common/ialpharunning/iarun
      common/idebugging/idebug
      common/momentainitial/pin1,pin2
      common/radpattern/nph(4)
      icount = icount + 1
      nfot = nph(1)+nph(2)+nph(3)+nph(4)
      amfs = 0.d0
      if (fs.eq.'ee') amfs = ame
      if (fs.eq.'mm') amfs = ammu
      print*,'>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
      print*,'printing point & event n.: ',col(2),icode,col(1),' ',kl
      print*,'final state: ',fs
      write(*,'(1x,A,f16.8,A,f16.8)')'cross section: ',xs,' +-',var
      write(*,'(1x,A,f16.8,A,f16.8)')'var now and before: ',var,',',varb
      write(*,'(1x,A,f25.8)')         'diff. xsect: ',sd
      write(*,'(1x,A,f25.8)')         'max xsect:   ',sdm
      write(*,'(1x,A,f16.8)')         'fmax (should be < max. xs): ',fm
      print*,'n. phot. ',nfot,' and rad. pattern ',nph
      write(*,'(1x,A,f16.8,f16.8,f16.8,f16.8)')'  p1 ',pin1
      write(*,'(1x,A,f16.8,f16.8,f16.8,f16.8)')'  p2 ',pin2
      write(*,'(1x,A,f16.8,f16.8,f16.8,f16.8)')'  p3 ',p1
      write(*,'(1x,A,f16.8,f16.8,f16.8,f16.8)')'  p4 ',p2
      if (nfot.gt.0) then
         do k = 1,nfot
            q(0) = qph(k,0)
            q(1) = qph(k,1)
            q(2) = qph(k,2)
            q(3) = qph(k,3)
            write(*,'(1x,i2,A,f16.8,f16.8,f16.8,f16.8)')k,' q ',q
         enddo
      endif
      write(*,'(1x,A,f16.8)')'th. angle p3-p1',angledeg(p1,pin1)
      write(*,'(1x,A,f16.8)')'th. angle p4-p1',angledeg(p2,pin1)
      write(*,'(1x,A,f16.8)')'th. angle p3-p4',angledeg(p1,p2)
      write(*,'(1x,A,f16.8)')'acoll. p3-p4   ',acollinearity(p1,p2)
      if (nfot.gt.0) then
         do k = 1,nfot
            q(0) = qph(k,0)
            q(1) = qph(k,1)
            q(2) = qph(k,2)
            q(3) = qph(k,3)
            write(*,'(1x,i2,A,f16.8)')k,'  angle q-p1 ',angledeg(q,pin1)
            write(*,'(1x,i2,A,f16.8)')k,'  angle q-p2 ',angledeg(q,pin2)
            write(*,'(1x,i2,A,f16.8)')k,'  angle q-p3 ',angledeg(q,p1)
            write(*,'(1x,i2,A,f16.8)')k,'  angle q-p4 ',angledeg(q,p2)
         enddo
      endif
      print*,'invariants:'
      do k = 0,3
         ptmp(k) = pin1(k)+pin2(k)
      enddo
      s12 = dot(ptmp,ptmp)
      do k = 0,3
         ptmp(k) = p1(k)+p2(k)
      enddo
      s34 = dot(ptmp,ptmp)
      do k = 0,3
         ptmp(k) = pin1(k)-p1(k)
      enddo
      t13 = dot(ptmp,ptmp)
      do k = 0,3
         ptmp(k) = pin2(k)-p2(k)
      enddo
      t24 = dot(ptmp,ptmp)
      do k = 0,3
         ptmp(k) = pin1(k)-p2(k)
      enddo
      u14 = dot(ptmp,ptmp)
      do k = 0,3
         ptmp(k) = pin2(k)-p1(k)
      enddo
      u23 = dot(ptmp,ptmp)
      write(*,'(1x,A,f16.8,f16.8,f16.8)')'s12 s34 t13',s12,s34,t13
      write(*,'(1x,A,f16.8,f16.8,f16.8)')'t24 u14 u23',t24,u14,u23
      if (nfot.gt.0) then
         do k = 1,nfot
            q(0) = qph(k,0)
            q(1) = qph(k,1)
            q(2) = qph(k,2)
            q(3) = qph(k,3)
            write(*,'(1x,i2,A,f16.8)')k,' p1q',dot(pin1,q)
            write(*,'(1x,i2,A,f16.8)')k,' p2q',dot(pin2,q)
            write(*,'(1x,i2,A,f16.8)')k,' p3q',dot(p1,q)
            write(*,'(1x,i2,A,f16.8)')k,' p4q',dot(p2,q)
         enddo
      endif
      print*,'<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
      return
      end

      subroutine printvector(ip,p1,p2,qph)
      implicit double precision (a-h,o-z)
      dimension p1(0:3),p2(0:3),qph(40,0:3),q(0:3)
      common/channelref/iref
      common/ifirstprv/ifirst,icount
      data ifirst /1/
      data icount /0/
      icount = icount + 1
      if (ifirst.eq.1) then
         open(33,file='momenta',status='unknown')
         ifirst = 0
      else
         open(33,file='momenta',status='unknown',access='append')
      endif
c$$$      print*,'set arrow 1 from 0,0,0 to ',p1(1),',',p1(2),',',p1(3)
c$$$      print*,'set arrow 2 from 0,0,0 to ',p2(1),',',p2(2),',',p2(3)
c$$$      print*,'set arrow 3 from 0,0,0 to ',q(1) ,',', q(2),',',q(3)
c$$$      print*,'set'//
c$$$     .' xrange [',min(p1(1),p2(1),q(1)),':',max(p1(1),p2(1),q(1)),'];'//
c$$$     .' set '//
c$$$     .'yrange [',min(p1(2),p2(2),q(2)),':\\',max(p1(2),p2(2),q(2)),'];'
c$$$     .//
c$$$     .' set'//
c$$$     .' zrange [',min(p1(3),p2(3),q(3)),':',max(p1(3),p2(3),q(3)),']'
      write(33,*)'point, count, iref ',ip,icount,iref
c
      write(33,*)'set arrow 1 from 0,0,0 to',p1(3),',',p1(1),',',p1(2),
     .' lt 1'
      write(33,*)'set arrow 2 from 0,0,0 to',p2(3),',',p2(1),',',p2(2),
     .' lt 2'
      n = 0
      do k = 1,40
         if (qph(k,0).gt.0.d0) n = n+1
      enddo
      if (n.gt.0) then
         do k = 3,n+2
            q(0) = qph(k-2,0)
            q(1) = qph(k-2,1)
            q(2) = qph(k-2,2)
            q(3) = qph(k-2,3)
      write(33,*)'set arrow',k
     .,' from 0,0,0 to',q(3),',',q(1),',',q(2),''//
     .' lt 3'
        enddo
      endif

      close(33)

      return
      end


      subroutine sortmomenta(ng,qm)
      implicit double precision (a-h,o-z)
      dimension pa(0:3),pb(0:3),qm(40,0:3),q1(0:3),q2(0:3),qmord(40,0:3)
      if (ng.eq.0) return
      ngl = ng
      call orderinverseqm(ngl,qm,qmord)
! in qmord photons are from the less energetic (k=1) to the most energetic 
! (k=ng). In qm I want to sort from the most to the less energetic...
      do k = 1,ng
         qm(ng+1-k,0) = qmord(k,0)
         qm(ng+1-k,1) = qmord(k,1)
         qm(ng+1-k,2) = qmord(k,2)
         qm(ng+1-k,3) = qmord(k,3)
      enddo
      return
      end
***
      subroutine mixandsortmomenta(ng,pa,pb,qm)
      implicit double precision (a-h,o-z)
      dimension pa(0:3),pb(0:3),qm(40,0:3),q1(0:3),q2(0:3),qmord(40,0:3)
      common/beforesort/p1o(0:3),p2o(0:3),qpho(40,0:3)

      if (ng.eq.0) return

      do k = 0,3
         p1o(k) = pa(k)
         p2o(k) = pb(k)
         do i = 1,ng
            qpho(i,k) = qm(i,k)
         enddo
      enddo
      ngl = ng + 2
      do k = 0,3
         qm(ng+1,k) = pa(k)
         qm(ng+2,k) = pb(k)
      enddo
      call orderinverseqm(ngl,qm,qmord)
      do k = 0,3
         pa(k)= qmord(ng+2,k)
         pb(k)= qmord(ng+1,k)
         qm(ng+1,k) = 0.d0
         qm(ng+2,k) = 0.d0
      enddo
! in qmord photons are from the less energetic (k=1) to the most energetic 
! (k=ng). In qm I want to sort from the most to the less energetic...
      do k = 1,ng
         qm(ng+1-k,0) = qmord(k,0)
         qm(ng+1-k,1) = qmord(k,1)
         qm(ng+1-k,2) = qmord(k,2)
         qm(ng+1-k,3) = qmord(k,3)
      enddo
      return
      end
c
      subroutine orderinverseqm(nph,qm,qmord)
      implicit double precision (a-h,o-z)
      parameter (nmax = 40)
      dimension qm(nmax,0:3),qmord(nmax,0:3),en(nph),sen(nph)
      dimension indexes(nph)
      do k = 1,nph
         en(k) = qm(k,0)
      enddo
      call sort_vector_inverse(en,sen,indexes,nph)
      do k = 1,nph
         qmord(k,0) = qm(indexes(k),0)
         qmord(k,1) = qm(indexes(k),1)
         qmord(k,2) = qm(indexes(k),2)
         qmord(k,3) = qm(indexes(k),3)
      enddo
      do k = nph+1,nmax
         qmord(k,0) = 0.d0
         qmord(k,1) = 0.d0
         qmord(k,2) = 0.d0
         qmord(k,3) = 0.d0
      enddo
      return
      end
**
      subroutine orderqm(nph,qm,qmord)
      implicit double precision (a-h,o-z)
      parameter (nmax = 40)
      dimension qm(nmax,0:3),qmord(nmax,0:3),en(nph),sen(nph)
      dimension indexes(nph)
      do k = 1,nph
         en(k) = qm(k,0)
      enddo
      call sort_vector(en,sen,indexes,nph)
      do k = 1,nph
         qmord(k,0) = qm(indexes(k),0)
         qmord(k,1) = qm(indexes(k),1)
         qmord(k,2) = qm(indexes(k),2)
         qmord(k,3) = qm(indexes(k),3)
      enddo
      do k = nph+1,nmax
         qmord(k,0) = 0.d0
         qmord(k,1) = 0.d0
         qmord(k,2) = 0.d0
         qmord(k,3) = 0.d0
      enddo
      return
      end


      subroutine sort_vector(v,sv,iold,n)
! pensare quick sort!!
      implicit double precision (a-h,o-z)
      dimension iold(n)
      dimension v(n),sv(n)
      do k = 1,n
         iold(k) = k
         sv(k) = v(k)
      enddo
      do i = 1,n
         vmax = sv(i)
         do j=i+1,n
            if (v(j).gt.vmax) then
               vmax = sv(j)
               vtmp = sv(i)
               sv(i) = sv(j)
               sv(j) = vtmp
               ktmp = iold(i)
               iold(i) = iold(j)
               iold(j) = ktmp
            endif
         enddo
      enddo
      return
      end

      subroutine sort_vector_inverse(v,sv,iold,n)
! pensare quick sort!!
      implicit double precision (a-h,o-z)
      dimension iold(n)
      dimension v(n),sv(n)
C this uses the quick sort algorithm reported at the end of the file
c      call SORTRX(n,v,iold)
c      do k = 1,n
c         sv(k) = v(iold(k))
c      enddo
c      return
      do k = 1,n
         iold(k) = k
         sv(k) = v(k)
      enddo
      do i = 1,n
         vmin = sv(i)
         do j=i+1,n
            if (v(j).lt.vmin) then
               vmin = sv(j)
               vtmp = sv(i)
               sv(i) = sv(j)
               sv(j) = vtmp
               ktmp = iold(i)
               iold(i) = iold(j)
               iold(j) = ktmp
            endif
         enddo
      enddo
      return
      end

      subroutine twomostenergetic(q,q1,q2,i1,i2)
      implicit real*8 (a-h,o-z)
      dimension q(40,0:3),q1(0:3),q2(0:3)
      do i = 0,3
         q1(i) = 0.d0
         q2(i) = 0.d0
      enddo
      ENPHOT = Q(1,0)
      ENHARD = ENPHOT
      J=1
* 
      DO I = 1,40
         ENPHOT = Q(I,0)
         IF (ENHARD.GE.ENPHOT) THEN
            ENHARD = ENHARD
         ELSE
            J = I
            ENHARD = ENPHOT
         ENDIF
      ENDDO 
*     
      ENHARDL = ENHARD
      JL = J
*     
      if (j.gt.0) then
         DO I = 0,3
            Q1(I) = Q(J,I)
         ENDDO
      endif
      i1 = j
      j1 = j
*     NEXT-TO-LEADING ENERGETIC PHOTON IS EXTRACTED
      ENHARD = 0.D0
      J = 0
      DO I = 1,40
         IF (I.NE.JL) THEN
            ENPHOT = Q(I,0)
            IF (ENHARD.GE.ENPHOT) THEN
               ENHARD = ENHARD
            ELSE
               J = I
               ENHARD = ENPHOT
            ENDIF
         ENDIF
      ENDDO 
      ENHARD = ENHARD
      if (j.gt.0) then
         DO I = 0,3
            Q2(I) = Q(J,I)
         ENDDO
      endif
      i2 = j
      j2 = j
      return
      end
*** 
      subroutine randperm(n,irand)
      implicit double precision (a-h,o-z)
      dimension irand(n),ind(n)
      real*8 csi(1)
      do k = 1,n
         ind(k) = k
      enddo
      do j = 0,n-1
         k = n-j
         call wraprng(csi,1)
         ii = k * csi(1) + 1
         if (ii.gt.k) ii = k
         irand(j+1) = ind(ii)
         itmp = ind(k)
         ind(k) = ind(ii)
         ind(ii) = itmp
      enddo
      return
      end

      function acollinearity(p1,p2)
! written by CMCC, last modified 5/3/2006
      implicit double precision (a-h,o-z)
      dimension p1(0:3),p2(0:3)
      parameter (pi = 3.1415926535897932384626433832795029D0)
      a1 = acos(p1(3)/sqrt(tridot(p1,p1)))*180.d0/pi
      a2 = acos(p2(3)/sqrt(tridot(p2,p2)))*180.d0/pi
      acollinearity = abs(180.d0 - a1 - a2)
      return
      end

      function acollinearityrad(p1,p2)
! written by CMCC, last modified 5/3/2006
      implicit double precision (a-h,o-z)
      dimension p1(0:3),p2(0:3)
      parameter (pi = 3.1415926535897932384626433832795029D0)
      a1 = acos(p1(3)/sqrt(tridot(p1,p1)))
      a2 = acos(p2(3)/sqrt(tridot(p2,p2)))
      acollinearityrad = abs(pi - a1 - a2)
      return
      end

      function cosine(p1,p2)
      implicit double precision (a-h,o-z)
      dimension p1(0:3),p2(0:3)
      p1modm1 = 1.d0/sqrt(tridot(p1,p1))
      p2modm1 = 1.d0/sqrt(tridot(p2,p2))
      cosine = tridot(p1,p2)*p1modm1*p2modm1
      return
      end

      function angledeg(p1,p2)
      implicit double precision (a-h,o-z)
      dimension p1(0:3),p2(0:3)
      parameter (pi = 3.1415926535897932384626433832795029D0)
      c = cosine(p1,p2)
      if (c.ge. 1.d0) c =  1.d0
      if (c.le.-1.d0) c = -1.d0
      angledeg = acos(c)*180.d0/pi
      return
      end

      function anglerad(p1,p2)
      implicit double precision (a-h,o-z)
      dimension p1(0:3),p2(0:3)
      parameter (pi = 3.1415926535897932384626433832795029D0)
      c = cosine(p1,p2)
      if (c.ge. 1.d0) c =  1.d0
      if (c.le.-1.d0) c = -1.d0
      anglerad = acos(c)
      return
      end

      subroutine rotbb(idir,vect,pin,pout)
! written by CMCC, last modified 9/10/2005
      implicit double precision (a-h,o-z)       
      double precision pin(0:3),pout(0:3),pp(0:3),r(3,3),
     >     vers(3),vect(0:3)
* This subroutine rotates the 4-vector pin in the frame where the z-axis is
* directed along the 4-vector vect(0,1,2,3). The rotated vector is stored
* in pout
* idir =  1 ---> direct rotation matrix
* idir = -1 ---> inverse rotation matrix
      pp(0) = pin(0)
      pp(1) = pin(1)
      pp(2) = pin(2)
      pp(3) = pin(3)

      vmo = 1.d0/sqrt(vect(1)**2+vect(2)**2+vect(3)**2)
      vers(1) = vect(1)*vmo
      vers(2) = vect(2)*vmo
      vers(3) = vect(3)*vmo
      vt = sqrt(vers(1)**2+vers(2)**2)

!   BUG - pointed out by CLEO people
!      v1ovt = vers(1)/vt
!      if (vt.eq.0.d0) v1ovt = 0.d0
!      v2ovt = vers(2)/vt
!      if (vt.eq.0.d0) v2ovt = 1.d0
 
      v1ovt = 0.d0
      v2ovt = 1.d0
      if (vt.gt.0.d0) then
         v1ovt = vers(1)/vt
         v2ovt = vers(2)/vt
      endif
      
      if (idir.eq.(-1)) then    !! INVERSE rotation matrix
         r(1,1) =  vers(3)*v1ovt
         r(1,2) = -v2ovt
         r(1,3) =  vers(1)      
         r(2,1) =  vers(3)*v2ovt
         r(2,2) =  v1ovt
         r(2,3) =  vers(2)
         r(3,1) = -vt
         r(3,2) =  0.d0
         r(3,3) =  vers(3)
      else  ! if (idir.eq.1) !! DIRECT rotation matrix
         r(1,1) =  vers(3)*v1ovt
         r(2,1) = -v2ovt
         r(3,1) =  vers(1)
         r(1,2) =  vers(3)*v2ovt
         r(2,2) =  v1ovt
         r(3,2) =  vers(2)
         r(1,3) = -vt
         r(2,3) =  0.d0
         r(3,3) =  vers(3)
      endif
      pout(0) = pp(0)
      pout(1) = r(1,1)*pp(1) + r(1,2)*pp(2) + r(1,3)*pp(3)
      pout(2) = r(2,1)*pp(1) + r(2,2)*pp(2) + r(2,3)*pp(3)
      pout(3) = r(3,1)*pp(1) + r(3,2)*pp(2) + r(3,3)*pp(3)
      return
      end
*
      subroutine getphi(rnd,ph,w)
! written by CMCC, last modified 9/10/2005
      implicit double precision (a-h,o-z)
      parameter (pi = 3.1415926535897932384626433832795029D0)
      real*8 rnd
      ph = 2.d0*pi*rnd
      w  = 2.d0*pi
      return
      end
*
      function getphiangle(ppp)
      implicit double precision (a-h,o-z)
      dimension ppp(0:3)
      parameter (pi = 3.1415926535897932384626433832795029D0)
      pm = sqrt(ppp(1)**2+ppp(2)**2+ppp(3)**2)
      c  = ppp(3)/pm
      s  = sqrt(1.d0-c**2)
      if (s.eq.0.d0) then
         getphiangle = 0.d0
         return
      else   
         arg = ppp(1)/pm/s
*  avoiding numerical problems......
         if (abs(arg).ge.1.d0) then
            iarg = arg
            arg  = iarg
         endif   
         if (ppp(2).ge.0.d0) getphiangle = acos(arg)
         if (ppp(2).lt.0.d0) getphiangle = 2.d0*pi-acos(arg)
      endif             
      return
      end
*     
      function factorial(n)
!     written by CMCC, last modified 21/1/2006
      integer n,nl      
      double precision factorial
      nl = n
      factorial = 1.d0
      do while(nl.gt.0)
         factorial = 1.d0*nl * factorial
         nl = nl - 1
      enddo
      return
      end
*
      function eikonalintegral(p1,p2,p3,p4,ch,masses)
! written by CMCC, last modified 9/10/2005
      implicit double precision (a-h,l,m,o-z)
      parameter (npart = 4)
      dimension eta(npart),p(0:3),q(0:3),pmat(npart,0:3)
      dimension masses(npart),ch(npart)
      dimension p1(0:3),p2(0:3),p3(0:3),p4(0:3)
! ch are the charges of the field (not anti-field!!)
! Use this convention: the integer factor in front of the charge
! must be
! -1 --> for incoming particle
! -1 --> for outgoing anti-particle
! +1 --> for outgoing particle
! +1 --> for incoming anti-particle
      eta(1) = -1.d0 * ch(1)
      eta(2) =  1.d0 * ch(2)
      eta(3) =  1.d0 * ch(3)
      eta(4) = -1.d0 * ch(4)
*
      do k = 0,3
         pmat(1,k) = p1(k)
         pmat(2,k) = p2(k)
         pmat(3,k) = p3(k)
         pmat(4,k) = p4(k)
      enddo

! off-diagonal contributions
      softint = 0.d0
      do i = 1,npart-1
         do j = i+1,npart
            etaij = eta(i) * eta(j)
            if (abs(etaij).gt.1.d-3) then
               call rescale_momenta(npart,i,j,pmat,masses,p,q)
               q2 = dot(q,q)
               vl = dot(p,p) - q2
               vl = vl/2.d0
               v  = vl/(p(0) - q(0)) 
               arglog = 1.d0+2.d0*vl/q2
               terminfra = log(arglog)         

               tot = terminfra ! + termfinite(p,q,v)
               tot = - tot *2.d0*dot(p,q)/vl* etaij
               tot = tot * 2.d0 ! this is the double product when
                                ! squaring the eikonal
               softint  = softint + tot
            endif
         enddo
      enddo      
      soft_integral = softint
      softint = -4.d0 * (eta(1)**2+eta(2)**2+eta(3)**2+eta(4)**2)
      soft_integral = soft_integral + softint
      soft_integral = soft_integral/16.d0
      eikonalintegral = soft_integral
      return
      end
**
      subroutine rescale_momenta(npart,i,j,pmat,masses,p,q)
! written by CMCC, last modified 9/10/2005
      implicit double precision (a-h,m,o-z)
      dimension p(0:3),q(0:3),pmat(npart,0:3),masses(npart)
      dimension p1(0:3),p2(0:3)
      do k = 0,3
         p1(k) = pmat(i,k)
         p2(k) = pmat(j,k)
      enddo
      m12 = masses(i)**2
      m22 = masses(j)**2
      p1p2 = dot(p1,p2)
      rho1 = p1p2 + sqrt(p1p2**2 - m12*m22)
      rho1 = rho1 / m12
      rho2 = p1p2 - sqrt(p1p2**2 - m12*m22)
      rho2 = rho2 / m12
      if ( (rho1*p1(0)-p2(0)) .gt. 0.d0) rho = rho1
      if ( (rho2*p1(0)-p2(0)) .gt. 0.d0) rho = rho2
      do k = 0,3
         p(k) = rho * p1(k)
         q(k) = p2(k)
      enddo
      return
      end
***
      function termfinite(p,q,v)
! written by CMCC, last modified 9/10/2005
      implicit double precision (a-h,o-z)
      dimension p(0:3),q(0:3)

      u0   = p(0)
      umod = sqrt(tridot(p,p))
      pp = u0 + umod
      pm = u0 - umod

      u0   = q(0)
      umod = sqrt(tridot(q,q))
      qp = u0 + umod
      qm = u0 - umod

! dilog from TOPAZ0
      arg1 = (v-pm)/v
      arg2 = (v-pp)/v
      arg3 = (v-qm)/v
      arg4 = (v-qp)/v

c      ddlog1 = ddilog(arg1)
c      ddlog2 = ddilog(arg2)
c      ddlog3 = ddilog(arg3)
c      ddlog3 = ddilog(arg4)

      call tspence(arg1,0.d0,1.d0-arg1,ddlog1,ddim)
      call tspence(arg2,0.d0,1.d0-arg2,ddlog2,ddim)
      call tspence(arg3,0.d0,1.d0-arg3,ddlog3,ddim)
      call tspence(arg4,0.d0,1.d0-arg4,ddlog4,ddim)

      termfin = log(pp/pm)**2/4.d0 + ddlog1 + ddlog2
     >        - log(qp/qm)**2/4.d0 - ddlog3 - ddlog4

      termfinite = termfin
      return
      end
*
      double precision function lambda(x,y,z)
! written by CMCC, last modified 9/10/2005
      double precision x,y,z
      lambda = x**2+y**2+z**2 - 2.d0*x*y - 2.d0*x*z - 2.d0*y*z
      return
      end
*
      subroutine exchange_mom(p1,p2)
! written by CMCC, last modified 9/10/2005
      implicit none 
      double precision p1(0:3),p2(0:3),p4(0:3)      
      integer i
      do i=0,3
         p4(i)=p1(i)
      enddo      
      do i=0,3
         p1(i)=p2(i)
      enddo      
      do i=0,3
         p2(i)=p4(i)
      enddo      
      return
      end
*
      subroutine new_boost(p,q,qq,idir)
! written by CMCC, last modified 9/10/2005
      implicit none
      integer idir
      double precision p(0:3),q(0:3),qq(0:3),vboost(0:3)
      double precision b,g,p0mu
! idir =  1: q is boosted where p is at rest and put in qq     
! idir = -1: q, in the frame where p is at rest, is boosted where p is not
!            at rest. The boosted q is put in qq.
      p0mu = 1.d0/p(0)
      if (idir.eq.1) then
         vboost(0) = 0.d0
         vboost(1) = p(1)*p0mu!/p(0)
         vboost(2) = p(2)*p0mu!/p(0)
         vboost(3) = p(3)*p0mu!/p(0)
      else
         vboost(0) = 0.d0
         vboost(1) = -p(1)*p0mu!/p(0)
         vboost(2) = -p(2)*p0mu!/p(0)
         vboost(3) = -p(3)*p0mu!/p(0)
      endif
      b=dsqrt(vboost(1)**2+vboost(2)**2+vboost(3)**2)
      g=1.d0/dsqrt(1.d0-b**2)
      call boost(g,vboost,q,qq)
      return
      end
**
      function dot(p,q)
! written by CMCC, last modified 9/10/2005
      implicit double precision (a-h,o-z)
      double precision p(0:3),q(0:3)
      dot=p(0)*q(0)-p(1)*q(1)-p(2)*q(2)-p(3)*q(3)
      return
      end
*
      function tridot(p,q)
! written by CMCC, last modified 9/10/2005
      implicit double precision (a-h,o-z)
      double precision p(0:3),q(0:3)
      tridot=p(1)*q(1)+p(2)*q(2)+p(3)*q(3)
      return
      end
*
      subroutine boost(g,v,p,q)
! written by CMCC, last modified 24/10/2005 (from old BABAYAGA)
      implicit double precision (a-h,o-z)
      double precision p(0:3),q(0:3),pp(0:3),v(0:3)
      if (g.eq.1.d0) then 
         do i=0,3
            q(i)=p(i)
         enddo
      else     
! this is essential if boost is called with the same momenta as arguments!!!!
         do i=0,3
            pp(i)=p(i)
         enddo
         ppdv = tridot(pp,v)
         v2   = v(1)**2+v(2)**2+v(3)**2
*     
         q(0)=g*(pp(0)-ppdv)
         do i=1,3
            q(i)=pp(i)+(g-1.d0)*ppdv/v2*v(i)-g*v(i)*pp(0)
         enddo
      endif
      return
      end
***
*=========== DILOG FROM TOPAZ0 =====================================
*-----SPENCE--------------------------------------------------------
*     COMPUTES  LI_2(X). ACCURACY IS ABOUT 16 DIGITS               
      SUBROUTINE TSPENCE(XR,XI,OMXR,CLI2R,CLI2I)
! from TOPAZ0
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
*
      COMMON/TQPARAM/QPI,QPIS,QEPS,QDELTA
*
      DIMENSION B(0:14),BF(0:14)
      DIMENSION CLNX(2),CLNOMX(2),CLNOY(2),CLNZ(2),CLNOMZ(2)
      DIMENSION ADD1(2),ADD2(2),ADD3(2),PAR(2),RES(2),CT(15),SN(15)
      DIMENSION X(2),OMX(2),Y(2),OY(2),OMY(2),Z(2),OMZ(2),T(2),OMT(2)
      common/storedtspence/b,bf,ifirst
      data ifirst /0/
c CARLO
      if (ifirst.eq.0) then
         QDELTA= 9.025809333D0
         QEPS= 1.D-30
         QPI= 3.141592653589793238462643D0
         QPIS= QPI*QPI
      endif
c CARLO
      X(1)= XR
      X(2)= XI
      OMX(1)= OMXR
      OMX(2)= -XI
      IF(XR.LT.0.D0) THEN
          Y(1)= OMXR
          Y(2)= -XI
          SIGN1= -1.D0
          CALL TCQLNX(X,CLNX)
          CALL TCQLNOMX(X,OMX,CLNOMX)
          ADD1(1)= QPIS/6.D0-CLNX(1)*CLNOMX(1)+CLNX(2)*CLNOMX(2)
          ADD1(2)= -CLNX(1)*CLNOMX(2)-CLNX(2)*CLNOMX(1)
      ELSE
          Y(1)= X(1)
          Y(2)= X(2)
          SIGN1= 1.D0
          ADD1(1)= 0.D0
          ADD1(2)= 0.D0
      ENDIF
      OMY(1)= 1.D0-Y(1)
      OMY(2)= -Y(2)
      YM2= Y(1)*Y(1)+Y(2)*Y(2)
      YM= SQRT(YM2)
      IF(YM.GT.1.D0) THEN
          Z(1)= Y(1)/YM2
          Z(2)= -Y(2)/YM2
          SIGN2= -1.D0
          OY(1)= -Y(1)
          OY(2)= -Y(2)
          CALL TCQLNX(OY,CLNOY)
          ADD2(1)= -QPIS/6.D0-0.5D0*((CLNOY(1))**2-(CLNOY(2))**2)
          ADD2(2)= -CLNOY(1)*CLNOY(2)
      ELSE
          Z(1)= Y(1)
          Z(2)= Y(2)
          SIGN2= 1.D0
          ADD2(1)= 0.D0
          ADD2(2)= 0.D0
      ENDIF
      OMZ(1)= 1.D0-Z(1)
      OMZ(2)= -Z(2)
      ZR= Z(1)
      IF(ZR.GT.0.5D0) THEN
          T(1)= 1.D0-Z(1)
          T(2)= -Z(2)
          OMT(1)= 1.D0-T(1)
          OMT(2)= -T(2)
          SIGN3= -1.D0
          CALL TCQLNX(Z,CLNZ)
          CALL TCQLNOMX(Z,OMZ,CLNOMZ)
          ADD3(1)= QPIS/6.D0-CLNZ(1)*CLNOMZ(1)+CLNZ(2)*CLNOMZ(2)
          ADD3(2)= -CLNZ(1)*CLNOMZ(2)-CLNZ(2)*CLNOMZ(1)
      ELSE
          T(1)= Z(1)
          T(2)= Z(2)
          OMT(1)= 1.D0-T(1)
          OMT(2)= -T(2)
          SIGN3= 1.D0
          ADD3(1)= 0.D0
          ADD3(2)= 0.D0
      ENDIF
      CALL TCQLNOMX(T,OMT,PAR)

      if (ifirst.eq.0) then
         B(0)= 1.D0
         B(1)= -1.D0/2.D0
         B(2)= 1.D0/6.D0
         B(4)= -1.D0/30.D0
         B(6)= 1.D0/42.D0
         B(8)= -1.D0/30.D0
         B(10)= 5.D0/66.D0
         B(12)= -691.D0/2730.D0
         B(14)= 7.D0/6.D0
         FACT= 1.D0
         DO N=0,14
            BF(N)= B(N)/FACT
            FACT= FACT*(N+2.D0)
         ENDDO
      endif
      PARR= PAR(1)
      PARI= PAR(2)
      PARM2= PARR*PARR+PARI*PARI
      PARM= SQRT(PARM2)
      CT(1)= PARR/PARM
      SN(1)= PARI/PARM
      DO N=2,15
          CT(N)= CT(1)*CT(N-1)-SN(1)*SN(N-1)
          SN(N)= SN(1)*CT(N-1)+CT(1)*SN(N-1)
      ENDDO
*      
      RES(1)= -((((((((BF(14)*CT(15)*PARM2+BF(12)*CT(13))*PARM2+
     #                 BF(10)*CT(11))*PARM2+BF(8)*CT(9))*PARM2+
     #                 BF(6)*CT(7))*PARM2+BF(4)*CT(5))*PARM2+
     #                 BF(2)*CT(3))*(-PARM)+BF(1)*CT(2))*(-PARM)+
     #                 BF(0)*CT(1))*PARM 
      RES(2)= -((((((((BF(14)*SN(15)*PARM2+BF(12)*SN(13))*PARM2+
     #                 BF(10)*SN(11))*PARM2+BF(8)*SN(9))*PARM2+
     #                 BF(6)*SN(7))*PARM2+BF(4)*SN(5))*PARM2+
     #                 BF(2)*SN(3))*(-PARM)+BF(1)*SN(2))*(-PARM)+
     #                 BF(0)*SN(1))*PARM 
      CLI2R= SIGN1*(SIGN2*(SIGN3*RES(1)+ADD3(1))+ADD2(1))+ADD1(1)
      CLI2I= SIGN1*(SIGN2*(SIGN3*RES(2)+ADD3(2))+ADD2(2))+ADD1(2)
*
      ifirst = 1
      RETURN
      END
*
*-----CQLNX---------------------------------------------
*     COMPUTES  LN(Z)                              
*
      SUBROUTINE TCQLNX(ARG,RES)
! from TOPAZ0
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
*
      DIMENSION ARG(2),AARG(2),RES(2)
*
      QPI= 3.141592653589793238462643D0
      DO I= 1,2
          AARG(I)= ABS(ARG(I))
      ENDDO
      ZM2= (ARG(1))**2+(ARG(2))**2
      ZM= SQRT(ZM2)
      RES(1)= LOG(ZM)
      IF(ARG(1).EQ.0.D0) THEN
          IF(ARG(2).GT.0.D0) THEN
              TETA= QPI/2.D0
          ELSE
              TETA= -QPI/2.D0
          ENDIF
          RES(2)= TETA
          RETURN
      ELSE IF(ARG(2).EQ.0.D0) THEN 
               IF(ARG(1).GT.0.D0) THEN
                   TETA= 0.D0
               ELSE
                   TETA= QPI
               ENDIF
          RES(2)= TETA
          RETURN
      ELSE
          TNTETA= AARG(2)/AARG(1)
          TETA= ATAN(TNTETA)
          SR= ARG(1)/AARG(1)
          SI= ARG(2)/AARG(2)
          IF(SR.GT.0.D0) THEN
              RES(2)= SI*TETA
          ELSE
              RES(2)= SI*(QPI-TETA)
          ENDIF
          RETURN
      ENDIF
      END
*
*-----CQLNOMX---------------------------------------
*     COMPUTES LN(1-X)                 
*     USUALLY |X| << 1                 
      SUBROUTINE TCQLNOMX(ARG,OMARG,RES)
! from TOPAZ0
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION ARG(2),OMARG(2),RES(2),ARES(2),CT(10),SN(10)
*
      ZR= ARG(1)
      ZI= ARG(2)
      ZM2= ZR*ZR+ZI*ZI
      ZM= SQRT(ZM2)
      IF(ZM.LT.1.D-7) THEN
          CT(1)= ZR/ZM
          SN(1)= ZI/ZM
          DO N=2,10
              CT(N)= CT(1)*CT(N-1)-SN(1)*SN(N-1)
              SN(N)= SN(1)*CT(N-1)+CT(1)*SN(N-1)
          ENDDO
          ARES(1)= CT(10)/10.D0
          ARES(2)= SN(10)/10.D0
          DO K=9,1,-1
              ARES(1)= ARES(1)*ZM+CT(K)/K
              ARES(2)= ARES(2)*ZM+SN(K)/K
          ENDDO
          ARES(1)= -ARES(1)*ZM
          ARES(2)= -ARES(2)*ZM
      ELSE
          CALL TCQLNX(OMARG,ARES)
      ENDIF
      RES(1)= ARES(1)
      RES(2)= ARES(2)
      RETURN
      END


c==================================================================
C this sort subroutine works very well...
c
C From Leonard J. Moss of SLAC:
C Here's a hybrid QuickSort I wrote a number of years ago.  It's
C based on suggestions in Knuth, Volume 3, and performs much better
C than a pure QuickSort on short or partially ordered input arrays.
      SUBROUTINE SORTRX(N,DATA,INDEX)
C===================================================================
C     SORTRX -- SORT, Real*8 input, indeX output
C     Input:  N     INTEGER
C             DATA  REAL
C     Output: INDEX INTEGER (DIMENSION N)
C This routine performs an in-memory sort of the first N elements of
C array DATA, returning into array INDEX the indices of elements of
C DATA arranged in ascending order.  Thus,
C    DATA(INDEX(1)) will be the smallest number in array DATA;
C    DATA(INDEX(N)) will be the largest number in DATA.
C The original data is not physically rearranged.  The original order
C of equal input values is not necessarily preserved.
C===================================================================
C SORTRX uses a hybrid QuickSort algorithm, based on several
C suggestions in Knuth, Volume 3, Section 5.2.2.  In particular, the
C "pivot key" [my term] for dividing each subsequence is chosen to be
C the median of the first, last, and middle values of the subsequence;
C and the QuickSort is cut off when a subsequence has 9 or fewer
C elements, and a straight insertion sort of the entire array is done
C at the end.  The result is comparable to a pure insertion sort for
C very short arrays, and very fast for very large arrays (of order 12
C micro-sec/element on the 3081K for arrays of 10K elements).  It is
C also not subject to the poor performance of the pure QuickSort on
C partially ordered data.
C Created:  15 Jul 1986  Len Moss
C===================================================================
      INTEGER   N,INDEX(N)
      DOUBLE PRECISION      DATA(N)
      INTEGER   LSTK(31),RSTK(31),ISTK
      INTEGER   L,R,I,J,P,INDEXP,INDEXT
      DOUBLE PRECISION      DATAP
C     QuickSort Cutoff
C     Quit QuickSort-ing when a subsequence contains M or fewer
C     elements and finish off at end with straight insertion sort.
C     According to Knuth, V.3, the optimum value of M is around 9.
      INTEGER   M
CARLO
cc      PARAMETER (M=30)
CARLO
      PARAMETER (M=9)
 
C===================================================================
C
C     Make initial guess for INDEX
 
      DO 50 I=1,N
         INDEX(I)=I
   50    CONTINUE
 
C     If array is short, skip QuickSort and go directly to
C     the straight insertion sort.
 
      IF (N.LE.M) GOTO 900
 
C===================================================================
C
C     QuickSort
C
C     The "Qn:"s correspond roughly to steps in Algorithm Q,
C     Knuth, V.3, PP.116-117, modified to select the median
C     of the first, last, and middle elements as the "pivot
C     key" (in Knuth's notation, "K").  Also modified to leave
C     data in place and produce an INDEX array.  To simplify
C     comments, let DATA[I]=DATA(INDEX(I)).
 
C Q1: Initialize
      ISTK=0
      L=1
      R=N
 
  200 CONTINUE
 
C Q2: Sort the subsequence DATA[L]..DATA[R].
C
C     At this point, DATA[l] <= DATA[m] <= DATA[r] for all l < L,
C     r > R, and L <= m <= R.  (First time through, there is no
C     DATA for l < L or r > R.)
 
      I=L
      J=R
 
C Q2.5: Select pivot key
C
C     Let the pivot, P, be the midpoint of this subsequence,
C     P=(L+R)/2; then rearrange INDEX(L), INDEX(P), and INDEX(R)
C     so the corresponding DATA values are in increasing order.
C     The pivot key, DATAP, is then DATA[P].
 
      P=(L+R)/2
      INDEXP=INDEX(P)
      DATAP=DATA(INDEXP)
 
      IF (DATA(INDEX(L)) .GT. DATAP) THEN
         INDEX(P)=INDEX(L)
         INDEX(L)=INDEXP
         INDEXP=INDEX(P)
         DATAP=DATA(INDEXP)
      ENDIF
 
      IF (DATAP .GT. DATA(INDEX(R))) THEN
         IF (DATA(INDEX(L)) .GT. DATA(INDEX(R))) THEN
            INDEX(P)=INDEX(L)
            INDEX(L)=INDEX(R)
         ELSE
            INDEX(P)=INDEX(R)
         ENDIF
         INDEX(R)=INDEXP
         INDEXP=INDEX(P)
         DATAP=DATA(INDEXP)
      ENDIF
 
C     Now we swap values between the right and left sides and/or
C     move DATAP until all smaller values are on the left and all
C     larger values are on the right.  Neither the left or right
C     side will be internally ordered yet; however, DATAP will be
C     in its final position.
 
  300 CONTINUE
 
C Q3: Search for datum on left >= DATAP
C
C     At this point, DATA[L] <= DATAP.  We can therefore start scanning
C     up from L, looking for a value >= DATAP (this scan is guaranteed
C     to terminate since we initially placed DATAP near the middle of
C     the subsequence).
 
         I=I+1
         IF (DATA(INDEX(I)).LT.DATAP) GOTO 300
 
  400 CONTINUE
 
C Q4: Search for datum on right <= DATAP
C
C     At this point, DATA[R] >= DATAP.  We can therefore start scanning
C     down from R, looking for a value <= DATAP (this scan is guaranteed
C     to terminate since we initially placed DATAP near the middle of
C     the subsequence).
 
         J=J-1
         IF (DATA(INDEX(J)).GT.DATAP) GOTO 400
 
C Q5: Have the two scans collided?
 
      IF (I.LT.J) THEN
 
C Q6: No, interchange DATA[I] <--> DATA[J] and continue
 
         INDEXT=INDEX(I)
         INDEX(I)=INDEX(J)
         INDEX(J)=INDEXT
         GOTO 300
      ELSE
 
C Q7: Yes, select next subsequence to sort
C
C     At this point, I >= J and DATA[l] <= DATA[I] == DATAP <= DATA[r],
C     for all L <= l < I and J < r <= R.  If both subsequences are
C     more than M elements long, push the longer one on the stack and
C     go back to QuickSort the shorter; if only one is more than M
C     elements long, go back and QuickSort it; otherwise, pop a
C     subsequence off the stack and QuickSort it.
 
         IF (R-J .GE. I-L .AND. I-L .GT. M) THEN
            ISTK=ISTK+1
            LSTK(ISTK)=J+1
            RSTK(ISTK)=R
            R=I-1
         ELSE IF (I-L .GT. R-J .AND. R-J .GT. M) THEN
            ISTK=ISTK+1
            LSTK(ISTK)=L
            RSTK(ISTK)=I-1
            L=J+1
         ELSE IF (R-J .GT. M) THEN
            L=J+1
         ELSE IF (I-L .GT. M) THEN
            R=I-1
         ELSE
C Q8: Pop the stack, or terminate QuickSort if empty
            IF (ISTK.LT.1) GOTO 900
            L=LSTK(ISTK)
            R=RSTK(ISTK)
            ISTK=ISTK-1
         ENDIF
         GOTO 200
      ENDIF
  900 CONTINUE
C===================================================================
C Q9: Straight Insertion sort
      DO 950 I=2,N
         IF (DATA(INDEX(I-1)) .GT. DATA(INDEX(I))) THEN
            INDEXP=INDEX(I)
            DATAP=DATA(INDEXP)
            P=I-1
  920       CONTINUE
               INDEX(P+1) = INDEX(P)
               P=P-1
               IF (P.GT.0) THEN
                  IF (DATA(INDEX(P)).GT.DATAP) GOTO 920
               ENDIF
            INDEX(P+1) = INDEXP
         ENDIF
  950    CONTINUE
C===================================================================
C     All done 
      END

*********************************************************************
* Function to calculate the width of the U boson knowing its mass and
* its vectorial coupling
*********************************************************************
      double precision function gammafunc(mass,k)
      implicit none
      double precision mass,k

      double precision ame,ammu,convfac,alpha,pi
      common/parameters/ame,ammu,convfac,alpha,pi

      double precision me, mmu, r, dr

      me=ame
      mmu=ammu

      gammafunc=0.

      if(mass.gt.2.*me) then
         gammafunc = gammafunc + 1./3.*alpha*k**2*mass*
     >               sqrt(1-4.*me**2/mass**2)*(1+2.*me**2/mass**2)
      endif
      if(mass.gt.2.*mmu) then
         gammafunc = gammafunc + 1./3.*alpha*k**2*mass*
     >               sqrt(1-4.*mmu**2/mass**2)*(1+2.*mmu**2/mass**2)
         call r_intpl(mass,r,dr)
         gammafunc = gammafunc + 1./3.*alpha*k**2*mass*
     >               sqrt(1-4.*mmu**2/mass**2)*(1+2.*mmu**2/mass**2)*r
      endif

      return

      end

**********************************************************************
*
* Sceglie il fotone da calcolare con l'elemento di matrice a caso fra 
* quelli che rientrano nei tagli angolari
*********************************************************************
      subroutine sceglifot(ifscelto,ng)
      implicit none
      integer ifscelto,ng,nrilev,nphotscelto
      integer k,i
      real*8 csi(1)
      integer ifnontagliati(40)
      common/fnontagliati/ifnontagliati

      ifscelto = 1
      if (ng.eq.1) return

      nrilev = 0
      do k=1,ng
        nrilev = nrilev + ifnontagliati(k) 
      enddo
      if (nrilev.eq.1) then
          nphotscelto = 1
      else
          call wraprng(csi,1)
          nphotscelto = csi(1)*nrilev + 1
      endif
      i = 0
      k = 1
      do while (k.le.ng)
        if (ifnontagliati(k).eq.1) i = i + 1
        if (i.eq.nphotscelto) then 
            ifscelto = k
            return 
        endif
        k=k+1
      enddo

      return
      end
*
      double precision function trinorma(p)
      implicit none
      double precision p(0:3), tridot

      trinorma=sqrt(tridot(p,p))

      return
      end


*************************************************************
*     FROM nunugpv
*
*.....FUNCTION DOP: D(X)/P(X)/ANORMD(XMIN,XMAX). D(X) IS THE 
*                   ELECTRON STRUCTURE FUNCTION
*
*************************************************************

      double precision function dop(x,am,p1,p2)
      implicit none
      double precision x, omx, opx, am
      double precision s
      double precision alx, alomx, alox
      double precision beta, betam, sdelta
      double precision dfunh, pm1
      double precision dot
      integer kk

      double precision ame, ammu, convfac, alpha, pi
      common/parameters/ame,ammu,convfac,alpha,pi

      double precision p1(0:3),p2(0:3),ptmp(0:3) 

      save beta, betam, sdelta


      do kk = 0,3
        ptmp(kk)   = p1(kk)+p2(kk)
      enddo
      s = dot(ptmp,ptmp)
*
      beta = 2.d0*alpha/pi*(dlog(s/am**2) - 1.d0)
      betam = beta/2.d0
*
      sdelta = 1+3.d0/8.d0*beta+(9.d0/128.d0-pi**2/48.d0)*beta**2
*
      omx = 1.D0 - x
      opx = 1.D0 + x
*
      alx = dlog(x)
*
      if (omx.LT.1D-28) then
         alomx = 0.D0
         alox = -1.D0
      else
         alomx = dlog(omx)
         alox  = alx/omx
      endif
*
      pm1 = 1.d0/betam*(1.d0 - x)**(1.d0 - betam)

      dfunh = - beta/4.D0*opx
     #        + beta**2/32.D0*(
     #        - 4.d0*opx*alomx + 3.d0*opx*alx - 4.d0*alox
     #        - 5.d0 - x)
      dop = sdelta + dfunh*pm1
*
      return
      end

****************************************************************************
      subroutine emission (p1, p2, w, particle)
      implicit none
      double precision p1(0:3), p2(0:3), ptmp(0:3)
      double precision dir1(1:3), dir2(1:3)
      real*8 csi(2)
      double precision x1,x2
      double precision s
      double precision w
      double precision x1max, x2max, x1min, x2min
      character*1 particle
      integer kk
      double precision dop
      double precision tridot, dot, am
      double precision and1, and2
      double precision ame, ammu, convfac, alpha, pi
      common/parameters/ame,ammu,convfac,alpha,pi
      double precision beta,betam
      double precision amassainv
      character*6   ord
      common/qedORDER/ord


      if (ord.eq.'born') then
        w = 1.d0
        return
      endif

      if (ord.eq.'struct') then

        do kk=0,3
        ptmp(kk)=p1(kk)+p2(kk)
        enddo

        s = dot(ptmp,ptmp)

        if (particle.eq.'e') then
          am = ame
        elseif (particle.eq.'m') then
          am = ammu
        endif

        beta  = 2.d0*alpha/pi * (dlog(s/(am**2))-1.d0)
        betam = beta/2.d0

        x1min = am/p1(0)
        x2min = am/p2(0)
        x1max = 1.d0
        x2max = 1.d0

        call wraprng(csi,2)

        x1=1.d0-((1.D0 - x1min)**(betam)*(1.D0-csi(1)*1.d0)
     >         +(1.D0 - x1max)**(betam)*csi(1))**(1.d0/betam)
*
        x2=1.d0-((1.D0 - x2min)**(betam)*(1.D0-csi(2)*1.d0)
     >         +(1.D0 - x2max)**(betam)*csi(2))**(1.d0/betam)

        w = dop(x1,am,p1,p2) * dop(x2,am,p1,p2)

        p1(0) = x1*p1(0)
        p2(0) = x2*p2(0)

        do kk=1,3
          dir1(kk)=p1(kk)/dsqrt(tridot(p1,p1))
          dir2(kk)=p2(kk)/dsqrt(tridot(p2,p2))
        enddo

        do kk=1,3
          p1(kk) = dir1(kk)*dsqrt(p1(0)**2-am**2)
          p2(kk) = dir2(kk)*dsqrt(p2(0)**2-am**2)
        enddo

        and1 = (1.D0-x1min)**(betam)-(1.D0-x1max)**(betam)
        and2 = (1.D0-x2min)**(betam)-(1.D0-x2max)**(betam)

        w = w * and1 * and2

        return
      endif

      end

      subroutine gaussspread(xmed,sigma,xga)
! Gaussian smearing......
! FAST, Box-Mueller algorithm
      implicit double precision (a-h,o-z)
      parameter (dpi = 6.2831853071795864769252867665590058d0)
      real*8 random(2),g(2)
      common/gboxmueller/g,iother
      data iother /0/
      if (iother.eq.0) then
         call wraprng(random,2)
         g(1) = sqrt(-2.d0*log(random(1)))*sin(dpi*random(2))
         g(2) = sqrt(-2.d0*log(random(1)))*cos(dpi*random(2))
         ga = g(1)
         iother = 1
      else
         ga = g(2)
         iother = 0
      endif
      xga = xmed + sigma*ga
      if (xga.lt.0.d0) xga = xmed
      return
      end

      subroutine gaussspreadSLOW(xmed,sigma,xga)
! Gaussian smearing......
      implicit double precision (a-h,o-z)
      real*8 random(2)
      istop = 0
      do while(istop.eq.0)
         call wraprng(random,2)
         v1 = 2.d0*random(1) - 1.d0
         v2 = 2.d0*random(2) - 1.d0
         s = v1**2+v2**2
         if(s.lt.1) istop = 1
      enddo
      fac =sqrt(-2.d0*log(s)/s)
      gaus = v1*fac
      xga = xmed+sigma*gaus
      if (xga.lt.0.d0) xga = xmed
      return
      end
