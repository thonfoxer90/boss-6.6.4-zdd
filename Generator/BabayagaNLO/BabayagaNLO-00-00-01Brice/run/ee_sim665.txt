#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"

//*************job options for Babayaga ***************
ApplicationMgr.DLLs += {"BabayagaNLO","BesServices"};
ApplicationMgr.TopAlg += {"BabayagaNLO"};
////////options for BABAYAGANLO
BabayagaNLO.Channel=0;              // 0: e+e-->e+e-;1:e+e_->mu+mu-;2:e+e-->gamma gamma
BabayagaNLO.CMSEnergy=3.770;
BabayagaNLO.BeamEnergySpread=0.001;
//BabayagaNLO.ThetaMin=0;
//BabayagaNLO.ThetaMax=180;
BabayagaNLO.ThetaMin=20;
BabayagaNLO.ThetaMax=160;
BabayagaNLO.AcollMax=180.0;
BabayagaNLO.Emin=0.04;
BabayagaNLO.Nsearch=10000;
BabayagaNLO.Verbose=0;
BabayagaNLO.RunningAlpha=1;
BabayagaNLO.PhotonNumber=-1;

////**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

////**************job options for detector simulation******************

#include "$BESSIMROOT/share/G4Svc_BesSim.txt"
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"
#include "$REALIZATIONSVCROOT/share/jobOptions_Realization.txt"

/// run ID
RealizationSvc.RunIdList = {-41729,0,-41909};//2.000
//RealizationSvc.RunIdList = {-39355, 0 ,-39618}; //data of R-scan 3.08 R-scan
//RealizationSvc.RunIdList = {-23463,0,-24141};
//RealizationSvc.RunIdList = {-20448};//, 0, -23454}; // 2011 data of psi''

#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "ee_3_770.rtraw";

//// OUTPUT PRINTOUT LEVEL
//// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 5;

#include "$BESSIMROOT/share/Bes_Gen.txt"

//// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 40000;


