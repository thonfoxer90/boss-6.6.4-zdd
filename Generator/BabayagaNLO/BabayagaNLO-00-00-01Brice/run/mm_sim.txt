//DENG Zi-yan 2008-03-17

#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"

//*************job options for Babayaga ***************
ApplicationMgr.DLLs += {"BabayagaNLO"};
ApplicationMgr.TopAlg += {"BabayagaNLO"};
////////options for BABAYAGANLO
BabayagaNLO.Channel=1;              // 0: e+e-->e+e-;1:e+e_->mu+mu-;2:e+e-->gamma gamma
BabayagaNLO.CMSEnergy=3.097;
BabayagaNLO.BeamEnergySpread=0.0008;
BabayagaNLO.ThetaMin=20;
BabayagaNLO.ThetaMax=160;
BabayagaNLO.AcollMax=10.0;
BabayagaNLO.Emin=1.0;
BabayagaNLO.Nsearch=4000000;
BabayagaNLO.Verbose=0;
BabayagaNLO.RunningAlpha=0;
BabayagaNLO.PhotonNumber=-1;

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

//**************job options for detector simulation******************
#include "$BESSIMROOT/share/G4Svc_BesSim.txt"

//Default: beam energy = 1.89GeV, MagneticFieldSvc.RunMode=3;
//If beam energy = 1.55GeV,   set MagneticFieldSvc.RunMode=2;
//If beam energy = 2.1GeV,    set MagneticFieldSvc.RunMode=4;
//MagneticFieldSvc.RunMode=2;

#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

// run ID
RealizationSvc.RunIdList = {-8093};

#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "mm.rtraw";

// OUTPUT PRINTOUT LEVEL
// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 6;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 10;

