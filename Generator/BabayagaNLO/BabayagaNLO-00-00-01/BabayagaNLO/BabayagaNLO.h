//************************************************************
//
//   BabayagaNLO generator to BES3
//  
//  Author: Wang Boqun
//  Date:  2010/09/06, created
//
//***********************************************************

#ifndef Generator_BabayagaNLO_H
#define Generator_BabayagaNLO_H

#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/ISvcLocator.h"

class IBesRndmGenSvc;
class BabayagaNLO: public Algorithm
{
  public:
  BabayagaNLO(const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();

 private:
  IBesRndmGenSvc* p_BesRndmGenSvc;
  int m_ch;
  int m_nsearch;
  int m_iverbose;
  int m_arun;
  int m_photmode;

  double m_ecmsinput;
  double m_beamspread;
  double m_thmin;
  double m_thmax;
  double m_zmax;
  double m_emin;
};

#endif
