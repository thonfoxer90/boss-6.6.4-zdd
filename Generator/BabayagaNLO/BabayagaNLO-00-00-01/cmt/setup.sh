# echo "Setting BabayagaNLO BabayagaNLO-00-00-01 in /home/bgarillo/boss-6.6.4/Generator"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cluster/bes3/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh

tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=BabayagaNLO -version=BabayagaNLO-00-00-01 -path=/home/bgarillo/boss-6.6.4/Generator  -no_cleanup $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

