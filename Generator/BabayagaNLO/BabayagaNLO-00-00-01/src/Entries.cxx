#include "GaudiKernel/DeclareFactoryEntries.h"
#include "BabayagaNLO/BabayagaNLO.h"

DECLARE_ALGORITHM_FACTORY( BabayagaNLO )

DECLARE_FACTORY_ENTRIES( BabayagaNLO) {
  DECLARE_ALGORITHM(BabayagaNLO );
}
