//#include "HepMC/HEPEVT_Wrapper.h"
#include "HepMC/IO_HEPEVT.h"
//#include "HepMC/IO_Ascii.h"
#include "HepMC/GenEvent.h"

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/DataSvc.h"
#include "GaudiKernel/SmartDataPtr.h"

#include "BabayagaNLO/BabayagaNLO.h"
#include "BabayagaNLO/BabayagaNLORandom.h"
#include "GeneratorObject/McGenEvent.h"
#include "BesKernel/IBesRndmGenSvc.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "BabayagaNLO/cfortran.h"
#include <iostream>

using std::cout;
using std::endl;
using namespace HepMC;

typedef struct {
	int getnew;
	int event_is_unw;
} EVENTINFO_DEF;
#define Eventinfo COMMON_BLOCK(EVENTINFO,eventinfo)
COMMON_BLOCK_DEF(EVENTINFO_DEF,Eventinfo);
EVENTINFO_DEF Eventinfo;

PROTOCCALLSFSUB2(USERINTERFACE,userinterface,INTV,DOUBLEV)
#define USERINTERFACE(xpari,xpard) CCALLSFSUB2(USERINTERFACE,userinterface,INTV,DOUBLEV,xpari,xpard)

PROTOCCALLSFSUB5(BABAYAGA,babayaga,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV)
#define BABAYAGA(p1,p2,p3,p4,qph) CCALLSFSUB5(BABAYAGA,babayaga,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV,p1,p2,p3,p4,qph)

PROTOCCALLSFSUB0(PRINTOUTPUT,printoutput)
#define PRINTOUTPUT() CCALLSFSUB0(PRINTOUTPUT,printoutput)

BabayagaNLO::BabayagaNLO(const std::string& name, ISvcLocator* pSvcLocator) : Algorithm(name, pSvcLocator)
{
	declareProperty("Channel", m_ch = 0);
	declareProperty("CMSEnergy", m_ecmsinput = 3.686);
	declareProperty("BeamEnergySpread", m_beamspread = 0.0013); 
	declareProperty("ThetaMin", m_thmin = 10);
	declareProperty("ThetaMax", m_thmax = 170);
	declareProperty("AcollMax", m_zmax = 10.0);
	declareProperty("Emin", m_emin = 1.0);
	declareProperty("Nsearch", m_nsearch = 4000000);
	declareProperty("Verbose", m_iverbose = 0);
	declareProperty("RunningAlpha", m_arun = 0);
	declareProperty("PhotonNumber", m_photmode = -1);
}

StatusCode BabayagaNLO::initialize() {
	MsgStream log(msgSvc(), name());
	log << MSG::INFO << "BabayagaNLO in initialize()" << endreq;

	//set Bes unified random engine
	static const bool CREATEIFNOTTHERE(true);
	StatusCode RndmStatus = service("BesRndmGenSvc", p_BesRndmGenSvc, CREATEIFNOTTHERE);
	if (!RndmStatus.isSuccess() || 0 == p_BesRndmGenSvc)
	{
		log << MSG::ERROR << " Could not initialize Random Number Service" << endreq;
		return RndmStatus;
	}
	CLHEP::HepRandomEngine* engine  = p_BesRndmGenSvc->GetEngine("BabayagaNLO");
	BabayagaNLORandom::setRandomEngine(engine);


	//start initialize
	int xpari[10];
	double xpard[10];

	xpari[0]=m_ch;
	xpari[1]=m_iverbose;
	xpari[2]=m_nsearch;
	xpari[3]=m_arun;
	xpari[4]=m_photmode;

	xpard[0]=m_ecmsinput;
	xpard[1]=m_beamspread;
	xpard[2]=m_thmin;
	xpard[3]=m_thmax;
	xpard[4]=m_zmax;
	xpard[5]=m_emin;

	USERINTERFACE(xpari,xpard);

	//HepMC::HEPEVT_Wrapper::set_sizeof_real(8);
	//HepMC::HEPEVT_Wrapper::set_sizeof_int(4);
	//HepMC::HEPEVT_Wrapper::set_max_number_entries(4000);
	//  std::cout << "max entries = " << HepMC::HEPEVT_Wrapper::max_number_entries() <<std::endl;
	//  std::cout << "size of real = " << HepMC::HEPEVT_Wrapper::sizeof_real() <<std::endl;


	log <<MSG::INFO<< "Finish BabayagaNLO initialize()" <<endreq;
	return StatusCode::SUCCESS;
}

StatusCode BabayagaNLO::execute() {
	MsgStream log(msgSvc(), name());
	log << MSG::INFO << "BabayagaNLO in execute()" << endreq;

	// Fill event information
	GenEvent* evt = new GenEvent(1,1);
	GenVertex* prod_vtx = new GenVertex();
	evt->add_vertex( prod_vtx );

	log << MSG::INFO << "check point 1" << endreq;
	double p1[4],p2[4],p3[4],p4[4],qph[4*40];
	for (int i=0;i<4*40;i++)
		qph[i]=0.0;

	do {
		BABAYAGA(p1,p2,p3,p4,qph);
	} while (!Eventinfo.event_is_unw);

	log << MSG::INFO << "check point 2" << endreq;
	// incoming beam e+
	GenParticle* p = 
		new GenParticle(CLHEP::HepLorentzVector(p2[1],p2[2],p2[3],p2[0]),-11, 3);
	p->suggest_barcode(1);
	prod_vtx->add_particle_in(p);

	// incoming beam e-
	p = 
		new GenParticle(CLHEP::HepLorentzVector(p1[1],p1[2],p1[3],p1[0]), 11, 3);
	p->suggest_barcode(2);
	prod_vtx->add_particle_in(p);

	log << MSG::INFO << "check point 3" << endreq;
	int finalpidm,finalpidp;
	if (m_ch == 0) {
		finalpidm=11;
		finalpidp=-11;
	} else if (m_ch == 1) {
		finalpidm=13;
		finalpidp=-13;
	} else if (m_ch == 2) {
		finalpidm=22;
		finalpidp=22;
	} else {
		finalpidm=11;
		finalpidp=-11;
	}

	log << MSG::INFO << "check point 4" << endreq;
	//outgoing particle -
	p = 
		new GenParticle(CLHEP::HepLorentzVector(p3[1],p3[2],p3[3],p3[0]), finalpidm, 1);
	p->suggest_barcode(3);
	prod_vtx->add_particle_out(p);

	//outgoing particle +
	p = 
		new GenParticle(CLHEP::HepLorentzVector(p4[1],p4[2],p4[3],p4[0]), finalpidp, 1);
	p->suggest_barcode(4);
	prod_vtx->add_particle_out(p);
	log << MSG::WARNING << "BABAYAGANLO Pz(e+) = " <<  p4[3] << endreq;
	log << MSG::INFO << "check point 5" << endreq;
	//photons
	for (int i=0;i<40;i++) {
		double px=qph[i+40];
		double py=qph[i+40*2];
		double pz=qph[i+40*3];
		double eph=sqrt(px*px+py*py+pz*pz);
		if (eph<1e-3) break;
		p = 
			new GenParticle(CLHEP::HepLorentzVector(px,py,pz,eph), 22, 1);
		p->suggest_barcode(5+i);
		prod_vtx->add_particle_out(p);
	}

	log << MSG::INFO << "check point 6" << endreq;
	if( log.level() <= MSG::INFO ){
		evt->print();  
	}

	// Check if the McCollection already exists
	SmartDataPtr<McGenEventCol> anMcCol(eventSvc(), "/Event/Gen");
	if (anMcCol!=0){
		// Add event to existing collection
		log<<MSG::WARNING<<"add event"<<endreq;
		MsgStream log(messageService(), name());
		log << MSG::INFO << "Add McGenEvent to existing collection" << endreq;
		McGenEvent* mcEvent = new McGenEvent(evt);
		anMcCol->push_back(mcEvent);
	} else {
		// Create Collection and add  to the transient store
		log<<MSG::WARNING<<"create collection"<<endreq;
		McGenEventCol *mcColl = new McGenEventCol;
		McGenEvent* mcEvent = new McGenEvent(evt);
		mcColl->push_back(mcEvent);
		StatusCode sc = eventSvc()->registerObject("/Event/Gen",mcColl);
		if (sc != StatusCode::SUCCESS) {
			log << MSG::ERROR << "Could not register McGenEvent" << endreq;
			delete mcColl;
			delete evt;
			delete mcEvent;
			return StatusCode::FAILURE;
		} else {
			log << MSG::INFO << "McGenEventCol created!" << endreq;
		}
	}


	log<<MSG::INFO<< "before execute() return"<<endreq;
	return StatusCode::SUCCESS;

}

StatusCode BabayagaNLO::finalize() {
	MsgStream log(msgSvc(), name());
	log << MSG::INFO << "BabayagaNLO in finalize()" << endreq;

	PRINTOUTPUT();
	return StatusCode::SUCCESS;
}


