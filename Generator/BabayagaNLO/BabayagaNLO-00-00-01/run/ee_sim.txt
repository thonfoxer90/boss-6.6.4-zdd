#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"

//*************job options for Babayaga ***************
ApplicationMgr.DLLs += {"BabayagaNLO","BesServices"};
ApplicationMgr.TopAlg += {"BabayagaNLO"};
////////options for BABAYAGANLO
BabayagaNLO.Channel=0;              // 0: e+e-->e+e-;1:e+e_->mu+mu-;2:e+e-->gamma gamma
BabayagaNLO.CMSEnergy=3.08;
BabayagaNLO.BeamEnergySpread=0.001;
//BabayagaNLO.ThetaMin=0;
//BabayagaNLO.ThetaMax=180;
BabayagaNLO.ThetaMin=20;
BabayagaNLO.ThetaMax=160;
BabayagaNLO.AcollMax=180.0;
BabayagaNLO.Emin=0.04;
BabayagaNLO.Nsearch=10000;
BabayagaNLO.Verbose=1;
BabayagaNLO.RunningAlpha=1;
BabayagaNLO.PhotonNumber=-1;

////**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

////**************job options for detector simulation******************

#include "$BESSIMROOT/share/Bes_Gen.txt"
#include "$REALIZATIONSVCROOT/share/jobOptions_Realization.txt"
//#include "$BESSIMROOT/share/G4Svc_BesSim.txt"
//#include "$CALIBSVCROOT/share/job-CalibData.txt"
//#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"

//// run ID
RealizationSvc.RunIdList = {-39355, 0 ,-39618}; //data of R-scan 3.08 R-scan
//RealizationSvc.RunIdList = {-23463,0,-24141};

//DatabaseSvc.DbType="MySql";
RootCnvSvc.digiRootOutputFile = "ee.rtraw";

//// OUTPUT PRINTOUT LEVEL
//// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 5;

//// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 1000;

//ApplicationMgr.HistogramPersistency = "ROOT";
//NTupleSvc.Output={"FILE1 FILE='genOnly.root' OPT='NEW' TYP='ROOT'"};
