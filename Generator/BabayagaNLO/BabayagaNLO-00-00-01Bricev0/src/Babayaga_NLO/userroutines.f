      subroutine besspread(en1,en2,s1,s2,th1,th2,ecm)
      implicit double precision (a-h,o-z)
      dimension p1bes(0:3),p2bes(0:3),plab(0:3),p1(0:3),p2(0:3)
      dimension pl2(0:3),p1tmp(0:3),p2tmp(0:3)
      common/parameters/ame,ammu,convfac,alpha,pi

      call gaussspread(en1,s1,e1)
      call gaussspread(en2,s2,e2)

      p1bes(0) =  e1
      p1bes(1) =  0.d0
      p1bes(2) =  0.d0
      p1bes(3) =  e1*sqrt(1.d0-ame*ame/e1/e1)
      p1besm   = sqrt(tridot(p1bes,p1bes)) 

      p2bes(0) =  e2
      p2bes(1) =  0.d0
      p2bes(2) =  0.d0
      p2bes(3) = -e2*sqrt(1.d0-ame*ame/e2/e2)
      p2besm   = sqrt(tridot(p2bes,p2bes)) 
      
      p1bes(1) = p1besm*sin(th1)
      p1bes(2) = 0.d0
      p1bes(3) = p1besm*cos(th1)

      p2bes(1) =  p2besm*sin(th2)
      p2bes(2) =  0.d0
      p2bes(3) = -p2besm*cos(th2)

      do k = 0,3
         plab(k) = p1bes(k)+p2bes(k)
      enddo

ccc test ok!  call new_boost(plab,plab,pl2,1)
      call new_boost(plab,p1bes,p1,1)
      call new_boost(plab,p2bes,p2,1)
! now p1 and p2 are back-to-back!! (although not along the beam axis...)
! rotate along the beam axis
      call rotbb(1,p1,p1,p1tmp)
      call rotbb(1,p1,p2,p2tmp)
      do k = 0,3
         p1(k) = p1tmp(k)
         p2(k) = p2tmp(k)
      enddo

cc      print*,' '
cc      print*,p1
cc      print*,p2
    
      ecm = p1(0)+p2(0)

      return
      end

c      subroutine initrng(iseed)
!
! wrapper to initialise the random number generator
! iseed (integer) = seed for the random number generator
!
c      integer iseed,lux,k1,k2
c      parameter (lux = 4,k1 = 0,k2 = 0) ! for RANLUX
c      call rluxgo(lux,iseed,k1,k2)
c      return
c      end
*
      subroutine wraprng(out,n)
!
! wrapper for random number generator
! n      (integer) = # of random numbers to be generated
! out(n) (real*8)  = vector of random numbers
!  
      double precision out(n)
      real*4 csi(n)
      integer k,n
      call flatarray(out,n)
c      do k=1,n
c         out(k) = 1.d0*csi(k)
c      enddo
      return
      end
*
********
      subroutine spreadecms
      implicit double precision (a-h,o-z)
      common/ecms/ecms
      common/frstspread/ecmsnominal,i1st
      common/ecmsininput/ecmsinput,beamspread
      data i1st /1/
! doing nothing...
c      sprecms = ecms
c      return

      if (i1st.eq.1) then
         ecmsnominal = ecms
         i1st = 0
      endif

      ecms = ecmsnominal

c      en1 = 3.5d0
c      en2 = 7.99650002d0
c      s1  = 0.00249999994d0
c      s2  = 0.00540000014d0
cc      call fakebellespread(en1,en2,s1,s2,ecms)

      en1 = ecmsinput/2.d0
      en2 = ecmsinput/2.d0
      s1  = beamspread
      s2  = beamspread
      th1 = 0.011d0
      th2 = 0.011d0
      call besspread(en1,en2,s1,s2,th1,th2,ecms)

c      sigma = 2d0
c      call gaussspread(ecmsnominal,sigma,ecms)

      return
      end
**************
      subroutine cuts(p1,p2,qph,icut)
! written by CMCC, last modified 7/4/2006
! p1(0...3): final state electron four-momentum
! p2(0...3): final state positron four-momentum
! qph(0...40,0...3): four-momenta of 40 emitted photons
! icut: icut = 1 event rejected, icut = 0 event accepted
      implicit double precision (a-h,o-z)
      dimension p1(0:3),p2(0:3),qph(40,0:3),q(0:3)
      parameter (pi = 3.1415926535897932384626433832795029D0)
      common/expcuts/thmin,thmax,emin,zmax,egmin,thgmin,thgmax
      common/angularranges/thmine,thmaxe,thminp,thmaxp
      common/momentainitial/pin1(0:3),pin2(0:3)

      icut = 1 ! event rejected

      if(p1(0).lt.emin.or.p2(0).lt.emin) return

      z = acollinearityrad(p1,p2)
      if (z.gt.zmax) return

      c1 = p1(3)/sqrt(tridot(p1,p1))
      c2 = p2(3)/sqrt(tridot(p2,p2))
      th1 = acos(c1)
      th2 = acos(c2)

      thmine = thmin
      thmaxe = thmax
      thminp = thmin
      thmaxp = thmax
***********************************
*      additional cuts
c      drad = 3.d0*pi/180.d0
c      thmaxe = thmine+drad
c      thminp = thmaxp-drad
***********************************
      if (th1.lt.thmine.or.th1.gt.thmaxe) return
      if (th2.lt.thminp.or.th2.gt.thmaxp) return
      
      icut = 0 ! event accepted
      return
      end
**************************************************************************
*
*
**************************************************************************
      subroutine cutsgg(ng,p1,p2,qph,icut)
      implicit double precision (a-h,o-z)
      dimension p1(0:3),p2(0:3),qph(40,0:3),q(0:3),q1(0:3),q2(0:3)
      dimension qphtot(40,0:3)
      dimension icutmat(1:ng+1,2:ng+2)
      dimension icutvec((ng+2)*(ng+1)/2)
      parameter (pi = 3.1415926535897932384626433832795029D0)
      common/expcuts/thmin,thmax,emin,zmax,egmin,thgmin,thgmax
      character*2 fs
      common/finalstate/fs
      icut = 1 ! event rejected
      nph    = ng + 2
      npairs = (nph)*(nph-1)/2
      do i=0,3
         qphtot(1,i) = p1(i)
         qphtot(2,i) = p2(i)
      enddo
      if (ng.gt.0) then
         do i = 0,3
            do k = 1,ng
               qphtot(k+2,i) = qph(k,i)
            enddo
         enddo
      endif
      ip = 0
      icuttot = 0
      do ka = 1,nph-1
         do kb = ka+1,nph
            ip = ip + 1
            do i = 0,3
               q1(i) = qphtot(ka,i)
               q2(i) = qphtot(kb,i)
            enddo
            call cuts(q1,q2,qph,icuttmp)
            icuttot = icuttot + icuttmp
c            icutvec(ip) = icuttmp

c            if (icuttmp.eq.0) then
c               icut = 0
c               return
c            endif

        enddo
      enddo
c      if (ng.gt.0.and.icuttot.lt.npairs.and.icutvec(1).ne.0) 
c     .     print*,icutvec
      if (icuttot.lt.npairs) icut = 0
      return
      end
************************************************************************
*
*
************************************************************************
      subroutine cutsdm(p1,p2,qph,icut)
! written by LB, shamelessly copied by CMCC's one, last modified 19/3/2010
! p1(0...3): final state electron four-momentum
! p2(0...3): final state positron four-momentum
! qph(0...40,0...3): four-momenta of 40 emitted photons
! icut: icut = 1 event rejected, icut = 0 event accepted
      implicit double precision (a-h,o-z)
      dimension p1(0:3),p2(0:3),qph(40,0:3),q(0:3),ptmp(0:3)
      parameter (pi = 3.1415926535897932384626433832795029D0)
      parameter (imaxph=40)
      common/expcuts/thmin,thmax,emin,zmax,egmin,thgmin,thgmax
      common/angularranges/thmine,thmaxe,thminp,thmaxp
      common/momentainitial/pin1(0:3),pin2(0:3)
      common/massainv/amassainvmin,amassainvmax
      integer ifnontagliati(imaxph)
      common/fnontagliati/ifnontagliati
      common/darkmatter/amassU,gammaU,gvectU,gaxU

      icut = 1 ! event rejected

      if (p1(0).lt.emin.or.p2(0).lt.emin) return

      z = acollinearityrad(p1,p2)
      if (z.gt.zmax) return

      c1 = p1(3)/sqrt(tridot(p1,p1))
      c2 = p2(3)/sqrt(tridot(p2,p2))
      th1 = acos(c1)
      th2 = acos(c2)

      thmine = thmin
      thmaxe = thmax
      thminp = thmin
      thmaxp = thmax
***********************************
*      additional cuts
c      drad = 3.d0*pi/180.d0
c      thmaxe = thmine+drad
c      thminp = thmaxp-drad
***********************************
      if (th1.lt.thmine.or.th1.gt.thmaxe) return
      if (th2.lt.thminp.or.th2.gt.thmaxp) return
*
** Mi restringo alla regione della distribuzione intorno al picco della
** BW che voglio osservare nel dettaglio
*
      if (p1(0).lt.emin.or.p2(0).lt.emin) return
      do k=0,3
        ptmp(k) = p1(k) + p2(k)
      enddo
      amassainv = dsqrt(dabs(dot(ptmp,ptmp)))

      if (amassainv.lt.amassainvmin) return
      if (amassainv.gt.amassainvmax) return

      do i=0,3
          q(i) = qph(1,i)
      enddo

      c  = q(3)/sqrt(tridot(q,q))
      th = dacos(c)

      if (q(0).lt.egmin.or.th.lt.thgmin.or.th.gt.thgmax) return
      
      icut = 0 ! event accepted
      return
      end
