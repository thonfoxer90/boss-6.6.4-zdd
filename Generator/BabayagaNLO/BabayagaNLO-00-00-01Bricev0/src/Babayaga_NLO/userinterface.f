      subroutine userinterface(xpari,xpard)
! written by CMCC, last modified 7/4/2006
! modified by Wang Boqun, last modified 2010/9/6
! xpari for integer parameters, xpard for double parameters
      implicit double precision (a-b,d-h,o-z)
      implicit character*10 (c)
      integer xpari(10)
      double precision xpard(10)
      logical istop
      character*20  c,ctmp
      character*100 cinput
      character*6   ord,arun,darkmod
      character*10  model,menu2,menud
      character*100 outfile,storefile
      character*30  cx
      character*100 stringa
      character*3   eventlimiter,store
      double precision convfac
      integer*8 iwriteout
      character*12 col(20)
      character*2 fs
      common/finalstate/fs
      common/colors/col
      common/ecms/ecms
      common/ecmsininput/ecmsinput,beamspread
      common/nphot_mode/nphotmode
      common/expcuts/thmin,thmax,emin,zmax,egmin,thgmin,thgmax
      common/zparameters/zm,gz,ve,ae,rv,ra,wm,s2th,gfermi,sqrt2,um
      common/epssoft/eps
      common/energiabeam/ebeam
      common/parameters/ame,ammu,convfac,alpha,pi
      common/intinput/iwriteout,iseed,nsearch,iverbose
      common/qedORDER/ord
      common/charinput/model,eventlimiter,store,storefile,outfile
      common/realinput/anpoints,sdifmax
      common/nmaxphalpha/nphmaxalpha
      common/ialpharunning/iarun
      common/idarkon/idarkon
      common/darkmatter/amassU,gammaU,gvectU,gaxU
      common/massainv/amassainvmin,amassainvmax
c$$$
c$$$ for teubner routine, 2010/05/19
c$$$
      integer iteubn
      common/teubner/iteubn
c$$$
cc      parameter (lux=4,k1=0,k2=0) ! for RANLUX
      character*62 welcome(23)
      data welcome/
     +'12345678901234567890123456789012345678901234567890123456789012',!1
     +' *************************************************************',!2
     +' **********                                         **********',!3
     +' ********                                             ********',!4
     +' ******             Welcome to BabaYaga                 ******',!5
     +' ****             ^^^^^^^^^^^^^^^^^^^^^^^                 ****',!6
     +' **       It is an event generator for QED processes        **',!7
     +' ****      at low energies, matching a QED PS with        ****',!8
     +' ******           exact order alpha corrections         ******',!9
     +' ********                                             ********',!10
     +' **********                                         **********',!11
     +' *************************************************************',!12
     +'  [[ it simulates: e+e- -->>  g  -->> e+e- or mu+mu- or gg ]] ',!13
     +'  [[             : e+e- -->> g,U -->> e+e-g or mu+mu-g     ]] ',!14
     +' *****     *      *****     *           *****   ******    *   ',!15
     +' *        * *     *        * *         *    *   *        * *  ',!16
     +' *        * *     *        * *         *    *   *        * *  ',!17
     +' ****    *   *    ****    *   *         *****   *       *   * ',!18
     +' *   *   *****    *   *   *****          *  *   *       ***** ',!19
     +' *   *  *     *   *   *  *     *        *   *   *      *     *',!20
     +' ****   *     *   ****   *     *        *   *   *      *     *',!21
     +' ',
     +' '/

      do k = 2,14
         print*,welcome(k)
      enddo


cc      open(11,file='input.dat',status='unknown')

      convfac = 0.389379660D6
      pi      = 3.1415926535897932384626433832795029D0 
      do i = 1,10
         model(i:i) = ' '
      enddo

      ame  =   0.51099906d-3
      ammu = 105.65836900d-3 

      alpha = 1.d0/137.0359895d0

** Z parameters...
      zm  = 91.1867D0
      wm  = 80.35d0 
      gz  = 2.4952D0
      s2th = 1.d0 - wm**2/zm**2

      gfermi = 1.16639d-5
      sqrt2  = sqrt(2.d0)
      gfermi = pi*alpha/2.d0/s2th/wm**2*sqrt2

      ve = -1.d0 + 4.d0*s2th
      ae = -1.d0
      rv = ve**2/(ae**2+ve**2)
      ra = ae**2/(ae**2+ve**2)
      um = sqrt(pi*alpha/sqrt2/gfermi)
** ...Z parameters

***** default input values
      if (xpari(1).eq.0) then
          fs = 'ee'
      else if (xpari(1).eq.1) then
          fs = 'mm'
      else if (xpari(1).eq.2) then
          fs = 'gg'
      else 
          fs = 'ee'
      endif

      ecmsinput = xpard(1)
      beamspread = xpard(2)
      ord  = 'exp'
      nphotmode = xpari(5)
      model  = 'matched'
      thmin  =  xpard(3)
      thmax  = xpard(4)
      zmax   =  xpard(5)
      emin   =   xpard(6)
      anpoints = 10000000.d0
      eps    = 5.d-4
      egmin  = 0.02d0
      thgmin = thmin 
      thgmax = thmax

      iseed        = 0
      iwriteout    = 1000000
      iverbose     = xpari(2)
      nsearch      = xpari(3)
!      eventlimiter = 'w'   ! 'unw' or 'w'
      eventlimiter = 'unw'  ! 'unw' or 'w'
      store        = 'no'
      sdifmax      = 1.d-18
      arun         = 'off'
      darkmod      = 'off'
      amassU       = 0.4d0
      gammaU       = -1. 
      gvectU       = 1.d-3
      gaxU         = 0.d0
      menu2        = 'off'
      menud        = 'off'
      amassainvmin = 0.d0
      amassainvmax = ecmsinput
      idarkon = 0
      iarun=xpari(4)
c      iarun = 0
c      if (arun.eq.'on') iarun = 1
      if (darkmod.eq.'on') idarkon = 1
***********

      ebeam = ecmsinput/2.d0
      thmin = thmin * pi/180.d0
      thmax = thmax * pi/180.d0
      thgmin = thgmin * pi/180.d0
      thgmax = thgmax * pi/180.d0
      zmax  = zmax * pi/180.d0
c
      if (fs.eq.'gg'.or.fs.eq.'ee'.or.fs.eq.'mm') then
         continue
      else
         print*,'  '
         print*,'Wrong final-state selected (',fs,')'
         print*,'Select one of ''ee'',''gg'' or ''mm'''
         print*,'  '
         stop
      endif
c
cc      call rluxgo(lux,iseed,k1,k2)

      if (ecmsinput.lt.0.d0) then
         print*,' '
         print*,'ERROR: ecmsinput must be specified explicitly!'
         print*,'Stopping BabaYaga'
         print*,' '
         stop
      endif



ccc      call initrng(iseed)
      nphmaxalpha = 6


c
      ecms = ecmsinput 
      amassainvmin = 0.d0
      amassainvmax = ecms

cc      close(11)


      call initbabayaga
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccc
