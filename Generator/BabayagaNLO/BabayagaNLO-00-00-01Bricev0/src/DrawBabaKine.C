using namespace std;

void DrawBabaKine(const char* filename,const char* treename, const char* outname)
{
TFile* file = TFile::Open(filename);
TTree* tree = (TTree*) file->Get(treename);

TFile* outfile= new TFile(outname,"RECREATE");

if(strcmp(treename,"leoTree")==0)
	{
	cout << "leo tree" << endl;
	double Px,Py,Pz;
	double Pxe,Pye,Pze,Pe,thetae,phie;
	double Pxp,Pyp,Pzp,Pp,thetap,phip;
	int pid;
	
	
	
	tree->SetBranchAddress("iniMomX",&Px);
	tree->SetBranchAddress("iniMomY",&Py);
	tree->SetBranchAddress("iniMomZ",&Pz);
	tree->SetBranchAddress("partProp",&pid);
	
	TH1D* hPe = new TH1D("Pe","Pe",128,0,3);
	TH1D* hThetae = new TH1D("Thetae","Thetae ",128,0.,1.);
	TH1D* hPhie = new TH1D("Phie","Phie ",128,-180.,180.);
	TH1D* hPp = new TH1D("Pp","Pp",128,0,3);
	TH1D* hThetap = new TH1D("Thetap","Thetap",128,0.,1.);
	TH1D* hPhip = new TH1D("Phip","Phip",128,-180.,180.);
	
	TTree* outtree = new TTree("Jean-Louis","Tu veux un titre?");
	outtree->Branch("Pe",&Pe,"Pe/D");
	outtree->Branch("Thetae",&thetae);
	outtree->Branch("Phie",&phie);
	outtree->Branch("Pp",&Pp);
	outtree->Branch("Thetap",&thetap);
	outtree->Branch("Phip",&phip);
	
	
	TCanvas *c = new TCanvas();
	c->Divide(3,2);
	for(int entry=0; entry<tree->GetEntries();entry++)
		{
		tree->GetEntry(entry);
			if(pid==11)
				{
				TVector3* v3 = new TVector3(Px,Py,Pz);
				Pe=v3->Mag();
				thetae=(v3->Theta())*(TMath::RadToDeg());
				phie=(v3->Phi())*(TMath::RadToDeg());
				
				hPe->Fill(Pe);
				hThetae->Fill(thetae);
				hPhie->Fill(phie);
				}
				
			if(pid==-11)
				{
				TVector3* v3 = new TVector3(Px,Py,Pz);
				Pp=v3->Mag();
				thetap=(v3->Theta())*(TMath::RadToDeg());
				phip=(v3->Phi())*(TMath::RadToDeg());
				
				hPp->Fill(Pp);
				hThetap->Fill(thetap);
				hPhip->Fill(phip);
				}
		outtree->Fill();
		}
		
	for(int i=0;i<9;i++)
		{c->cd(i+1);
		if(i==0) hPe->Draw();
		if(i==1) hThetae->Draw();
		if(i==2) hPhie->Draw();
		if(i==3) hPp->Draw();
		if(i==4) hThetap->Draw();
		if(i==5) hPhip->Draw();
		}
	outfile->Write();
	}

}
