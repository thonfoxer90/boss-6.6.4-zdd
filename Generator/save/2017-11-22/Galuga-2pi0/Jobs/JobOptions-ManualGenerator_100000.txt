#include "$KKMCROOT/share/jobOptions_KKMC.txt"

//*************job options for generator (KKMC)***************
KKMC.CMSEnergy = 3.773; //BOSS gives warning in case of inconsistency with .out file above
KKMC.BeamEnergySpread=0.0;
KKMC.GeneratePsi3770=true;
KKMC.ParticleDecayThroughEvtGen = true;
KKMC.ThresholdCut=0.0;
KKMC.RadiationCorrection = false;

#include "$MANUALGENERATORROOT/share/ManualGenerator.txt"
ManualGenerator.InputFile ="$MyLocalWorkDir/galuga.out";

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

//**************job options for detector simulation******************
#include "$BESSIMROOT/share/G4Svc_BesSim.txt"

//configure for calibration constants
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

RealizationSvc.RunIdList = {-11414, 0, -13988, -14395, 0, -14604, -20448, 0, -23454};//RUN Number for psipp data

#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "$MyLocalWorkDir/galuga.rtraw";

MessageSvc.OutputLevel  = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 400000 ; //Number of events to be simulated
#include "$KKMCROOT/share/jobOptions_KKMC.txt"

//*************job options for generator (KKMC)***************
KKMC.CMSEnergy = 3.773; //BOSS gives warning in case of inconsistency with .out file above
KKMC.BeamEnergySpread=0.0;
KKMC.GeneratePsi3770=true;
KKMC.ParticleDecayThroughEvtGen = true;
KKMC.ThresholdCut=0.0;
KKMC.RadiationCorrection = false;

#include "$MANUALGENERATORROOT/share/ManualGenerator.txt"
ManualGenerator.InputFile ="$MyLocalWorkDir/galuga.out";

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

//**************job options for detector simulation******************
#include "$BESSIMROOT/share/G4Svc_BesSim.txt"

//configure for calibration constants
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

RealizationSvc.RunIdList = {-11414, 0, -13988, -14395, 0, -14604, -20448, 0, -23454};//RUN Number for psipp data

#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "$MyLocalWorkDir/galuga.rtraw";

MessageSvc.OutputLevel  = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 400000 ; //Number of events to be simulated
#include "$KKMCROOT/share/jobOptions_KKMC.txt"

//*************job options for generator (KKMC)***************
KKMC.CMSEnergy = 3.773; //BOSS gives warning in case of inconsistency with .out file above
KKMC.BeamEnergySpread=0.0;
KKMC.GeneratePsi3770=true;
KKMC.ParticleDecayThroughEvtGen = true;
KKMC.ThresholdCut=0.0;
KKMC.RadiationCorrection = false;

#include "$MANUALGENERATORROOT/share/ManualGenerator.txt"
ManualGenerator.InputFile ="$MyLocalWorkDir/galuga.out";

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

//**************job options for detector simulation******************
#include "$BESSIMROOT/share/G4Svc_BesSim.txt"

//configure for calibration constants
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

RealizationSvc.RunIdList = {-11414, 0, -13988, -14395, 0, -14604, -20448, 0, -23454};//RUN Number for psipp data

#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "$MyLocalWorkDir/galuga.rtraw";

MessageSvc.OutputLevel  = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 400000 ; //Number of events to be simulated
#include "$KKMCROOT/share/jobOptions_KKMC.txt"

//*************job options for generator (KKMC)***************
KKMC.CMSEnergy = 3.773; //BOSS gives warning in case of inconsistency with .out file above
KKMC.BeamEnergySpread=0.0;
KKMC.GeneratePsi3770=true;
KKMC.ParticleDecayThroughEvtGen = true;
KKMC.ThresholdCut=0.0;
KKMC.RadiationCorrection = false;

#include "$MANUALGENERATORROOT/share/ManualGenerator.txt"
ManualGenerator.InputFile ="$MyLocalWorkDir/galuga.out";

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

//**************job options for detector simulation******************
#include "$BESSIMROOT/share/G4Svc_BesSim.txt"

//configure for calibration constants
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

RealizationSvc.RunIdList = {-11414, 0, -13988, -14395, 0, -14604, -20448, 0, -23454};//RUN Number for psipp data

#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "$MyLocalWorkDir/galuga.rtraw";

MessageSvc.OutputLevel  = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 50000 ; //Number of events to be simulated
