//DENG Zi-yan 2008-03-17

#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"

//*************job options for generator (KKMC)***************
#include "$KKMCROOT/share/jobOptions_KKMC.txt"
KKMC.CMSEnergy = 3.773;
KKMC.BeamEnergySpread=0.0;
KKMC.NumberOfEventPrinted=10;
KKMC.GeneratePsi3770=true;
KKMC.ParticleDecayThroughEvtGen = true;
KKMC.ThresholdCut=0.0;
KKMC.RadiationCorrection = false;

//*************job options for EvtGen***************
#include "$MANUALGENERATORROOT/share/ManualGenerator.txt"
ManualGenerator.InputFile =
"/data/work/kphpbb/guo/Galuga2.0/full-space/output/3770/galuga.out";//input file contains momentum information

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

//**************job options for detector simulation******************
#include "$BESSIMROOT/share/G4Svc_BesSim.txt"

//configure for calibration constants
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

// run ID
RealizationSvc.RunIdList = {-11414, 0, -13988, -14395, 0, -14604, -20448, 0, -23454};//RUN Number

#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "/data/work/kphpbb/guo/Galuga2.0/full-space/output/3770/galuga_sel.rtraw";


// OUTPUT PRINTOUT LEVEL
// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 50000; //Number of events to be simulated
