#include <stdlib.h>
#include <iostream>
#include <fstream> 
#include <string>
#include <vector>
#include <TLorentzVector.h>
#include <cstdio>
using namespace std;

int main(int argc, char** argv)
{

string inputfile(argv[1]);
string output(argv[2]);
output=output+".out";

ifstream readFile(inputfile.c_str());
//ofstream writeFile(output.c_str());
FILE* writeFile = fopen(output.c_str(),"w");

if(readFile.is_open()){
	while(readFile.good()){
		//Load informations from input file
		int nPart=0;
		readFile >> nPart;
		vector<int> vecpartID;
		vecpartID.resize(nPart);
		vector<vector<double> > vecLV;
		vecLV.resize(nPart);
		for(int k=0;k<nPart;k++){
		int partID;
		double Px,Py,Pz,E;
		vector<double> LV; LV.resize(4);
		
		readFile >> partID >> Px >> Py >> Pz >> E;
		vecpartID[k]=partID;
		LV[0]=Px; LV[1]=Py; LV[2]=Pz; LV[3]=E;
		vecLV[k]=LV;
		}
		
		//Change the e+e->e+e-gam kinematics so that the photon is emitted by the electron instead of the positron
		//1 : Swap e-/e+ particle ID
		vecpartID[0]=11;
		vecpartID[1]=-11;
		
		//2 : Parity transformation to the 4 vectors so that positron is still emitted towards Z>0 direction
		
		for(int i=0;i<vecLV.size();i++){
			for(int j=0;j<3;j++){
				vecLV[i][j]=-vecLV[i][j];
			}
		}
		
		fprintf(writeFile,"\t\t\t\t\t\t%d\n",nPart);
		for(int i=0;i<nPart;i++){
			fprintf(writeFile,"\t\t\t%-3d\t\t%.6E\t\t%.6E\t\t%.6E\t\t%.6E\n",
			vecpartID[i],
			vecLV[i][0],vecLV[i][1],vecLV[i][2],vecLV[i][3]
			);
		}
	}
	
}
return 0;
}