      program bbbrem                                                    ACTW0001
      implicit double precision(a-h,o-z)                                ACTW0002
      common / experc / roots,rk0                                       ACTW0003
      common / labmom / p1(4),p2(4),q1(4),q2(4),qk(4),d1,d2,t,weight    ACTW0004
      common / ranchn / nran                                            ACTW0005
                                                                        ACTW0006
* read input parameters                                                 ACTW0007
      read(*,*) roots                                                   ACTW0008
      read(*,*) rk0                                                     ACTW0009
      read(*,*) nevent                                                  ACTW0010
      read(*,*) nran                                                    ACTW0011
                                                                        ACTW0012
* iitialize bookkeeping                                                 ACTW0013
      wmax = 0                                                          ACTW0014
      wnil = 0                                                          ACTW0015
      wneg = 0                                                          ACTW0016
      w0 = 0                                                            ACTW0017
      w1 = 0                                                            ACTW0018
      w2 = 0                                                            ACTW0019
                                                                        ACTW0020
* start of event generation loop                                        ACTW0021
      do 9999 k = 1,nevent                                              ACTW0022
                                                                        ACTW0023
* generate an event                                                     ACTW0024
        call bcube                                                      ACTW0025
                                                                        ACTW0026
* bookkeeping on the weight                                             ACTW0027
        if(weight.gt.wmax) wmax = weight                                ACTW0028
        if(weight.eq.0d0) wnil = wnil+1                                 ACTW0029
        if(weight.lt.0d0) wneg = wneg+1                                 ACTW0030
        w0 = w0+1                                                       ACTW0031
        w1 = w1+weight                                                  ACTW0032
        w2 = w2+weight*weight                                           ACTW0033
                                                                        ACTW0034
* end of event generation loop                                          ACTW0035
 9999 continue                                                          ACTW0036
                                                                        ACTW0037
* analyze weight distribution, and print results                        ACTW0038
      smc = w1/w0                                                       ACTW0039
      ser = dsqrt(w2-w1*w1/w0)/w0                                       ACTW0040
      ranchk = random(0d0)                                              ACTW0041
      write(*,1) ranchk,w0,w1,w2,wmax,wnil,wneg,smc,ser                 ACTW0042
    1 format(' ******** cross section evaluation ********'/,            ACTW0043
     .       ' random number check       ',f15.13/,                     ACTW0044
     .       ' total # of events         ',f15.0/,                      ACTW0045
     .       ' sum of weights            ',d15.6/,                      ACTW0046
     .       ' sum of (weight**2)s       ',d15.6/,                      ACTW0047
     .       ' max weight occurred       ',d15.6/,                      ACTW0048
     .       ' # of zero weights         ',f15.0/,                      ACTW0049
     .       ' # of negative weights     ',f15.0/,                      ACTW0050
     .       ' computed  cross section   ',d15.6,' millibarn'/,         ACTW0051
     .       '                   +/-     ',d15.6,' millibarn'/,         ACTW0052
     .       ' ******************************************')             ACTW0053
      end                                                               ACTW0054
                                                                        ACTW0055
      subroutine bcube                                                  ACTW0056
      implicit logical(a-z)                                             ACTW0057
      double precision                                                  ACTW0058
     . roots,rk0,p1,p2,q1,q2,qk,alpha,rme,tomb,pi,twopi,                ACTW0059
     . s,rme2,rme2s,rls,z0,a1,a2,ac,sigapp,eb,pb,rin2pb,random,         ACTW0060
     . z,y,q0,temp1,tmin,tmax,sy,w2,rlamx,b,t,rlam,eps,rl,vgam,         ACTW0061
     . cgam,sgam,phi,phig,ql,qt,q(4),r0,w,rin2w,rinr0w,eta,phat1,       ACTW0062
     . phat2,phat3,phatt,phatl,sfhat,cfhat,sthat,vthat,cthat,sfg,       ACTW0063
     . cfg,temp2,veg,qkhat(4),c1,vnumx,vdenx,vnumn,vdenn,c2,rlabl,      ACTW0064
     . rhat4,etar,rhat1,rhat2,rhat3,zz,s1,d1,d2,rind1,rind12,           ACTW0065
     . rind2,rind22,aa0,aa1,aa2,aa3,aa4,rmex,rmap,weight                ACTW0066
      integer init                                                      ACTW0067
      save                                                              ACTW0068
                                                                        ACTW0069
* experimental constants                                                ACTW0070
      common / experc / roots,rk0                                       ACTW0071
* momenta in the lab frame                                              ACTW0072
      common / labmom / p1(4),p2(4),q1(4),q2(4),qk(4),d1,d2,t,weight    ACTW0073
                                                                        ACTW0074
      data init/0/                                                      ACTW0075
* start of initialization                                               ACTW0076
      if(init.eq.0) then                                                ACTW0077
        init = 1                                                        ACTW0078
        write(*,901)                                                    ACTW0079
  901   format(' ******************************************'/,          ACTW0080
     .         ' *** bbbrem : beam-beam bremsstrahlung  ***'/,          ACTW0081
     .         ' *** authors r. kleiss and h. burkhardt ***'/,          ACTW0082
     .         ' ******************************************')           ACTW0083
                                                                        ACTW0084
* physical constants                                                    ACTW0085
        alpha = 1d0/137.036d0                                           ACTW0086
        rme = 0.51099906d-03                                            ACTW0087
        tomb = 3.8937966d05 /1d6                                        ACTW0088
                                                                        ACTW0089
* mathematical constants                                                ACTW0090
        pi = 4*datan(1d0)                                               ACTW0091
        twopi = 2*pi                                                    ACTW0092
                                                                        ACTW0093
* derived constants                                                     ACTW0094
        write(*,902) roots,rk0                                          ACTW0095
  902   format(' total energy              ',f15.6,' gev'/,             ACTW0096
     .         ' minimum photon energy     ',f15.6,' * beam energy')    ACTW0097
        if(rk0.le.0d0.or.rk0.ge.1d0) then                               ACTW0098
          write(*,*) 'wrong value for rk0'                              ACTW0099
          stop                                                          ACTW0100
        endif                                                           ACTW0101
        s = roots**2                                                    ACTW0102
        rme2 = rme**2                                                   ACTW0103
        rme2s = rme2/s                                                  ACTW0104
        rls = -dlog(rme2s)                                              ACTW0105
        z0 = rk0/(1-rk0)                                                ACTW0106
                                                                        ACTW0107
* approximate total cross section                                       ACTW0108
        a1 = dlog((1+z0)/z0)                                            ACTW0109
        a2 = (dlog(1+z0))/z0                                            ACTW0110
        ac = a1/(a1+a2)                                                 ACTW0111
        sigapp = 8*alpha**3/rme2*(-dlog(rme2s))*(a1+a2)*tomb            ACTW0112
        write(*,903) sigapp                                             ACTW0113
  903   format(' approximate cross section ',d15.6,' millibarn')        ACTW0114
                                                                        ACTW0115
* the initial-state momenta                                             ACTW0116
        eb = roots*0.5d0                                                ACTW0117
        pb = dsqrt(eb*eb-rme2)                                          ACTW0118
        rin2pb = 0.5d0/pb                                               ACTW0119
        p1(1) = 0                                                       ACTW0120
        p1(2) = 0                                                       ACTW0121
        p1(3) = -pb                                                     ACTW0122
        p1(4) = eb                                                      ACTW0123
        q1(1) = 0                                                       ACTW0124
        q1(2) = 0                                                       ACTW0125
        q1(3) = pb                                                      ACTW0126
        q1(4) = eb                                                      ACTW0127
                                                                        ACTW0128
* end of initialization                                                 ACTW0129
      endif                                                             ACTW0130
                                                                        ACTW0131
* generate z                                                            ACTW0132
      if(random(1).lt.ac) then                                          ACTW0133
         temp1=random(2)                                                ACTW0134
         z = 1d0/(temp1*(dexp(a1*random(3))-1))                         ACTW0135
      else                                                              ACTW0136
        z = z0/random(4)                                                ACTW0137
      endif                                                             ACTW0138
                                                                        ACTW0139
* bounds on t                                                           ACTW0140
      y = rme2s*z                                                       ACTW0141
      q0 = eb*y                                                         ACTW0142
      temp1 = pb*pb-eb*q0                                               ACTW0143
      temp2 = temp1*temp1-rme2-q0*q0                                    ACTW0144
* exit if temp2<0 (very very rare): put weight to 0                     ACTW0145
* the `else' clause extends to the end of the routine                   ACTW0146
      if(temp2.lt.0d0) then                                             ACTW0147
        write(*,904) temp2                                              ACTW0148
  904   format(' y too large: delta_t^2 = ',d15.6)                      ACTW0149
        weight = 0d0                                                    ACTW0150
      else                                                              ACTW0151
        tmin = -2*(temp1+dsqrt(temp2))                                  ACTW0152
        tmax = rme2*s*y*y/tmin                                          ACTW0153
                                                                        ACTW0154
* generate t                                                            ACTW0155
        sy = s*y                                                        ACTW0156
        w2 = sy+rme2                                                    ACTW0157
        temp1 = sy+tmax                                                 ACTW0158
        rlamx = dsqrt(temp1*temp1-4*w2*tmax)                            ACTW0159
        if(temp1.le.0d0) then                                           ACTW0160
          temp1 = rlamx-temp1                                           ACTW0161
        else                                                            ACTW0162
          temp1 = -4*w2*tmax/(rlamx+temp1)                              ACTW0163
        endif                                                           ACTW0164
    1   continue                                                        ACTW0165
          b = dexp(random(5)*dlog(1+2*sy/temp1))                        ACTW0166
          t = -b*z*z*rme2/((b-1)*(b*z+b-1))                             ACTW0167
          if(t.lt.tmin) then                                            ACTW0168
            write(*,905) t,tmin                                         ACTW0169
  905       format(' t = ',d15.6,'   < t_min =',d15.6)                  ACTW0170
            goto 1                                                      ACTW0171
          endif                                                         ACTW0172
        continue                                                        ACTW0173
                                                                        ACTW0174
* generate cgam                                                         ACTW0175
        rlam = dsqrt((sy-t)*(sy-t)-4*rme2*t)                            ACTW0176
        eps = 4*rme2*w2/(rlam*(rlam+w2+rme2-t))                         ACTW0177
        rl = dlog((2+eps)/eps)                                          ACTW0178
        vgam = eps*(dexp(random(6)*rl)-1)                               ACTW0179
        cgam = 1-vgam                                                   ACTW0180
        sgam = dsqrt(vgam*(2-vgam))                                     ACTW0181
                                                                        ACTW0182
* generate azimuthal angles                                             ACTW0183
        phi = twopi*random(7)                                           ACTW0184
        phig = twopi*random(8)                                          ACTW0185
                                                                        ACTW0186
* construct momentum transfer q(mu)                                     ACTW0187
        ql = (2*eb*q0-t)*rin2pb                                         ACTW0188
        qt = dsqrt((tmax-t)*(t-tmin))*rin2pb                            ACTW0189
        q(1) = qt*dsin(phi)                                             ACTW0190
        q(2) = qt*dcos(phi)                                             ACTW0191
        q(3) = ql                                                       ACTW0192
        q(4) = q0                                                       ACTW0193
                                                                        ACTW0194
* construct momentum of outgoing positron in lab frame                  ACTW0195
        q2(1) = q1(1)-q(1)                                              ACTW0196
        q2(2) = q1(2)-q(2)                                              ACTW0197
        q2(3) = q1(3)-q(3)                                              ACTW0198
        q2(4) = q1(4)-q(4)                                              ACTW0199
                                                                        ACTW0200
* find euler angles of p1(mu) in cm frame                               ACTW0201
        r0 = eb+q0                                                      ACTW0202
        w = dsqrt(w2)                                                   ACTW0203
        rin2w = 0.5d0/w                                                 ACTW0204
        rinr0w = 1d0/(r0+w)                                             ACTW0205
        eta = -(sy+2*w*q0+t)*rin2w*rinr0w                               ACTW0206
        phat1 = -q(1)*(1+eta)                                           ACTW0207
        phat2 = -q(2)*(1+eta)                                           ACTW0208
        phat3 = pb*eta-ql*(1+eta)                                       ACTW0209
        phatl = rlam*rin2w                                              ACTW0210
        phatt = dsqrt(phat1*phat1+phat2*phat2)                          ACTW0211
        sfhat = phat1/phatt                                             ACTW0212
        cfhat = phat2/phatt                                             ACTW0213
        sthat = phatt/phatl                                             ACTW0214
        if(phat3.gt.0d0) then                                           ACTW0215
          vthat = sthat*sthat/(1-dsqrt(1-sthat*sthat))                  ACTW0216
        else                                                            ACTW0217
          vthat = sthat*sthat/(1+dsqrt(1-sthat*sthat))                  ACTW0218
        endif                                                           ACTW0219
        cthat = vthat-1                                                 ACTW0220
                                                                        ACTW0221
* rotate using these euler angles to get the qk direction in the cm     ACTW0222
        sfg = dsin(phig)                                                ACTW0223
        cfg = dcos(phig)                                                ACTW0224
        temp1 = sgam*sfg                                                ACTW0225
        temp2 = cthat*sgam*cfg+sthat*cgam                               ACTW0226
        veg = vthat+vgam-vthat*vgam-sthat*sgam*cfg                      ACTW0227
        qkhat(4) = sy*rin2w                                             ACTW0228
        qkhat(1) = qkhat(4)*( cfhat*temp1+sfhat*temp2)                  ACTW0229
        qkhat(2) = qkhat(4)*(-sfhat*temp1+cfhat*temp2)                  ACTW0230
        qkhat(3) = qkhat(4)*(veg-1)                                     ACTW0231
                                                                        ACTW0232
* boost the photon momentum to the lab frame                            ACTW0233
        temp1 = pb*qkhat(3)                                             ACTW0234
        if(temp1.gt.0d0) then                                           ACTW0235
          temp2 = (rme2*qkhat(4)*qkhat(4)                               ACTW0236
     .            +pb*pb*(qkhat(1)*qkhat(1)+qkhat(2)*qkhat(2)))         ACTW0237
     .            /(eb*qkhat(4)+temp1)                                  ACTW0238
        else                                                            ACTW0239
          temp2 = eb*qkhat(4)-temp1                                     ACTW0240
        endif                                                           ACTW0241
        qk(4) = (temp2+qkhat(4)*q(4)+qkhat(1)*q(1)                      ACTW0242
     .                +qkhat(2)*q(2)+qkhat(3)*q(3))/w                   ACTW0243
        temp1 = (qk(4)+qkhat(4))*rinr0w                                 ACTW0244
        qk(1) = qkhat(1)+temp1*q(1)                                     ACTW0245
        qk(2) = qkhat(2)+temp1*q(2)                                     ACTW0246
        qk(3) = qkhat(3)+temp1*(-pb+ql)                                 ACTW0247
                                                                        ACTW0248
* construct p2 by momentum conservation                                 ACTW0249
        p2(1) = -q2(1)-qk(1)                                            ACTW0250
        p2(2) = -q2(2)-qk(2)                                            ACTW0251
        p2(3) = -q2(3)-qk(3)                                            ACTW0252
        p2(4) = -q2(4)-qk(4)+roots                                      ACTW0253
                                                                        ACTW0254
* impose cut on the photon energy: qk(4)>eb*rk0                         ACTW0255
        if(qk(4).lt.eb*rk0) then                                        ACTW0256
          weight = 0d0                                                  ACTW0257
        else                                                            ACTW0258
                                                                        ACTW0259
* the event is now accepted: compute matrix element and weight          ACTW0260
* compute fudge factor c1                                               ACTW0261
          c1 = dlog(1+z)/dlog((2+eps)/eps)                              ACTW0262
                                                                        ACTW0263
* compute fudge factor c2                                               ACTW0264
          temp1 = sy-tmax                                               ACTW0265
          vnumx = dsqrt(temp1*temp1-4*rme2*tmax)+temp1                  ACTW0266
          temp1 = sy+tmax                                               ACTW0267
          if(temp1.lt.0d0) then                                         ACTW0268
            vdenx = dsqrt(temp1*temp1-4*w2*tmax)-temp1                  ACTW0269
          else                                                          ACTW0270
            vdenx = -4*w2*tmax/(dsqrt(temp1*temp1-4*w2*tmax)+temp1)     ACTW0271
          endif                                                         ACTW0272
          temp1 = sy-tmin                                               ACTW0273
          vnumn = dsqrt(temp1*temp1-4*rme2*tmin)+temp1                  ACTW0274
          temp1 = sy+tmin                                               ACTW0275
          if(temp1.lt.0d0) then                                         ACTW0276
            vdenn = dsqrt(temp1*temp1-4*w2*tmin)-temp1                  ACTW0277
          else                                                          ACTW0278
            vdenn = -4*w2*tmin/(dsqrt(temp1*temp1-4*w2*tmin)+temp1)     ACTW0279
          endif                                                         ACTW0280
          c2 = 2*rls/dlog((vnumx*vdenn)/(vdenx*vnumn))                  ACTW0281
                                                                        ACTW0282
* compute vector (small) r in cm frame, and (big) z                     ACTW0283
          rlabl = (t-2*rme2*y)*rin2pb                                   ACTW0284
          rhat4 = -(2*rme2*y+(1-y)*t)*rin2w                             ACTW0285
          etar = rhat4*rinr0w                                           ACTW0286
          rhat1 = -q(1)*(1+etar)                                        ACTW0287
          rhat2 = -q(2)*(1+etar)                                        ACTW0288
          rhat3 = rlabl+(pb-ql)*etar                                    ACTW0289
          zz = s*(rhat4*qkhat(4)-rhat1*qkhat(1)                         ACTW0290
     .         -rhat2*qkhat(2)-rhat3*qkhat(3))                          ACTW0291
                                                                        ACTW0292
* the other invariants                                                  ACTW0293
          s1 = 4*eb*(eb-qk(4))                                          ACTW0294
          d1 = sy*rlam*(eps+vgam)*rin2w*rin2w                           ACTW0295
          d2 = 0.5d0*sy                                                 ACTW0296
                                                                        ACTW0297
* the exact matrix element                                              ACTW0298
          rind1 = 1d0/d1                                                ACTW0299
          rind12 = rind1*rind1                                          ACTW0300
          rind2 = 1d0/d2                                                ACTW0301
          rind22 = rind2*rind2                                          ACTW0302
c* kleiss-burkhardt cross section multiplied by t**2                    ACTW0303
          temp1 = s+t-2*d2                                              ACTW0304
          temp2 = s1+t+2*d1                                             ACTW0305
          aa0 = (s*s+s1*s1+temp1*temp1+temp2*temp2)*rind1*rind2*(-t)    ACTW0306
          aa1 = -4*rme2*zz*zz*rind12*rind22                             ACTW0307
          aa2 = -8*rme2*(d1*d1+d2*d2)*rind1*rind2                       ACTW0308
          aa3 = 16*rme2*rme2*(d1-d2)*zz*rind12*rind22                   ACTW0309
          aa4 = -16*rme2*rme2*rme2*(d1-d2)*(d1-d2)*rind12*rind22        ACTW0310
          rmex = aa0+aa1+aa2+aa3+aa4                                    ACTW0311
                                                                        ACTW0312
* the approximate matrix element without c1,2, multiplied by t**2       ACTW0313
          rmap = 4*s*s*rind1*rind2*(-t)*c1*c2                           ACTW0314
                                                                        ACTW0315
* the weight                                                            ACTW0316
          weight = rmex/rmap*sigapp                                     ACTW0317
                                                                        ACTW0318
* the weight is now defined for both accepted and rejected events       ACTW0319
        endif                                                           ACTW0320
      endif                                                             ACTW0321
 1001 format(3(a10,d15.6))                                              ACTW0322
      end                                                               ACTW0323
                                                                        ACTW0324
      function random(r)                                                ACTW0325
      implicit double precision(a-h,o-z)                                ACTW0326
      common / ranchn / nran                                            ACTW0327
      save                                                              ACTW0328
      data init/0/                                                      ACTW0329
      if(init.eq.0) then                                                ACTW0330
        init = 1                                                        ACTW0331
        if(nran.eq.1) then                                              ACTW0332
          write(*,*) ' random: very crude random numbers chosen'        ACTW0333
        elseif(nran.eq.2) then                                          ACTW0334
          write(*,*) ' random: quasi-random numbers chosen'             ACTW0335
        else                                                            ACTW0336
          write(*,*) ' random: error: nran = ',nran,' not allowed'      ACTW0337
          stop                                                          ACTW0338
        endif                                                           ACTW0339
      endif                                                             ACTW0340
      if(nran.eq.1) then                                                ACTW0341
        random = rando1(r)                                              ACTW0342
      elseif(nran.eq.2) then                                            ACTW0343
        random = rando2(r)                                              ACTW0344
      endif                                                             ACTW0345
      end                                                               ACTW0346
                                                                        ACTW0347
      function rando1(r)                                                ACTW0348
      implicit double precision(a-h,o-z)                                ACTW0349
      save                                                              ACTW0350
      data init/0/                                                      ACTW0351
      if(init.eq.0) then                                                ACTW0352
        init = 1                                                        ACTW0353
        m = 2**20                                                       ACTW0354
        rm = 1d0/(1d0*m)                                                ACTW0355
        ia = 2**9+5                                                     ACTW0356
        ic = 1                                                          ACTW0357
        iseed = 100461                                                  ACTW0358
      endif                                                             ACTW0359
    1 iseed = mod(ia*iseed+ic,m)                                        ACTW0360
      if(iseed.eq.0) goto 1                                             ACTW0361
      rando1 = iseed*rm                                                 ACTW0362
      end                                                               ACTW0363
                                                                        ACTW0364
      function rando2(r)                                                ACTW0365
      implicit double precision(a-h,o-z)                                ACTW0366
      save                                                              ACTW0367
      parameter(n = 3)                                                  ACTW0368
      dimension nprime(10),s(10)                                        ACTW0369
      data init/0/                                                      ACTW0370
      data nprime/2,3,5,7,11,13,17,19,23,29/                            ACTW0371
      if(init.eq.0) then                                                ACTW0372
        init = 1                                                        ACTW0373
        do 1 k = 1,n                                                    ACTW0374
          s(k) = dsqrt(nprime(k)*1d0)                                   ACTW0375
    1   continue                                                        ACTW0376
      endif                                                             ACTW0377
      do 3 k = 1,n                                                      ACTW0378
        ss = 0                                                          ACTW0379
        do 2 j=1,n                                                      ACTW0380
          ss = ss+s(j)                                                  ACTW0381
    2   continue                                                        ACTW0382
        s(k) = dmod(ss,1d0)                                             ACTW0383
    3 continue                                                          ACTW0384
      rando2 = s(1)                                                     ACTW0385
      end                                                               ACTW0386
                                                                        ACTW****
