//************************************************************
//
//   Ekhara generator for BES3
//  
//***********************************************************

#ifndef Generator_Ekhara_H
#define Generator_Ekhara_H

#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/ISvcLocator.h"
#include "Ekhara/EkharaRandom.h"


class IBesRndmGenSvc;
class Ekhara: public Algorithm
{
 public:
  Ekhara(const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();
  
 private:
  IBesRndmGenSvc* p_BesRndmGenSvc;
  double m_cmsEnergy;      // CMS-energy                        
  int m_ngUpperBound;      // # of events to determine the maximum                  
  int m_finalState;        // pi+pi-(1),pi0(2), eta(3), eta' (4)
  int m_mesonAmplitudes;   // Single Pseudoscalar: s=(1); t=(2); s+t=(3);    t = signal 

  int m_TFF_ID;            // WZWconst(1); rho pole(2); LMD(3); LMD+V(4); LMD+Vnew(5); 1-Octet(7); 2-Octet(8)
  double m_posiThetaMin;   //!- e+ minimum angle
  double m_posiThetaMax;   //!- e+ maximum angle
  double m_posiEnergyMin;  //!- e+ minimum Energy
  double m_posiEnergyMax;  //!- e+ maximum Energy
  double m_elecThetaMin;   //!- e- maximum angle
  double m_elecThetaMax;   //!- e- maximum angle
  double m_elecEnergyMin;  //!- e- minimum Energy
  double m_elecEnergyMax;  //!- e- maximum Energy
  double m_mesonThetaMin;  //!- pi0, eta, eta' minimum angle
  double m_mesonThetaMax;  //!- pi0, eta, eta' maximum angle
  double m_mesonEnergyMin; //!- pi0, eta, eta' minimum Energy
  double m_mesonEnergyMax; //!- pi0, eta, eta' maximum Energy

  int m_twoPionAmplitudes;   //!- pi+pi-: s=(1); t=(2); s+t+2g=(3); s = signal
  double m_twoPionThetaMin;     //!- pi+pi-: min theta cut
  double m_twoPionThetaMax;     //!- pi+pi-: max theta cut
  double m_twoPionMissThetaMin; //!- pi+pi-: min missing theta cut
  double m_twoPionMissThetaMax; //!- pi+pi-: min missing theta cut

  int m_taggingMode;        //!- Tagging mode 0:no tagging; 1/11:untagged; 2/12:single tagging; 3/13:double tagging
  double m_taggingAngle;    //!- Tagging angle (symmetric) in degrees      
  double m_taggingQsquare;  //!- Tagging angle (symmetric) in degrees      

  int m_totalEvents;

};

#endif
