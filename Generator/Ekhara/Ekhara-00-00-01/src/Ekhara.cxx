#include "HepMC/HEPEVT_Wrapper.h"
#include "HepMC/IO_HEPEVT.h"
#include "HepMC/IO_Ascii.h"
//#include "CLHEP/HepMC/GenEvent.h"
#include "HepMC/GenEvent.h"


#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/DataSvc.h"
#include "GaudiKernel/SmartDataPtr.h"

#include "Ekhara/Ekhara.h"
#include "Ekhara/EkharaRandom.h"
#include "GeneratorObject/McGenEvent.h"
#include "BesKernel/IBesRndmGenSvc.h"
#include "Ekhara/cfortran.h"
#include <iostream>

#include "CLHEP/Matrix/Vector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Matrix/SymMatrix.h"
#include "CLHEP/Matrix/Matrix.h"

#include "TMath.h"

using namespace HepMC;
using namespace CLHEP;
using CLHEP::HepVector;
using CLHEP::HepLorentzVector;
using CLHEP::Hep3Vector;
using CLHEP::HepMatrix;
using CLHEP::HepSymMatrix;




typedef struct
{
  int channel_id;
} CHANNELSEL_DEF;
#define CHANNELSEL COMMON_BLOCK(CHANNELSEL_DEF, channelsel)

COMMON_BLOCK_DEF(CHANNELSEL_DEF, CHANNELSEL);

typedef struct
{
  int sw_2pi,sw_1pi;
} SWDIAG_DEF;
#define SWDIAG COMMON_BLOCK(SWDIAG_DEF, swdiag)

COMMON_BLOCK_DEF(SWDIAG_DEF, SWDIAG);

typedef struct
{
  int piggFFsw;
} PIONFFSW_DEF;
#define PIONFFSW COMMON_BLOCK(PIONFFSW_DEF, pionffsw)

COMMON_BLOCK_DEF(PIONFFSW_DEF, PIONFFSW);

typedef struct
{
  int tagmode;
} TAGGINGMODE_DEF;
#define TAGGINGMODE COMMON_BLOCK(TAGGINGMODE_DEF, taggingmode)

COMMON_BLOCK_DEF(TAGGINGMODE_DEF, TAGGINGMODE);


PROTOCCALLSFSUB1(EKHARA,ekhara,INT)
#define EKHARA(i) CCALLSFSUB1(EKHARA,ekhara,INT,i)

PROTOCCALLSFSUB1(BOSS_INIT_READ,boss_init_read,DOUBLEV)
#define BOSS_INIT_READ(xpar) CCALLSFSUB1(BOSS_INIT_READ,boss_init_read,DOUBLEV,xpar)

PROTOCCALLSFSUB0(DIAGNOSE,diagnose)
#define DIAGNOSE() CCALLSFSUB0(DIAGNOSE,diagnose)

PROTOCCALLSFSUB7(GET_MOMENTUM,get_momentum,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV)
#define GET_MOMENTUM(p1,p2,q1,q2,pi1,pi2,qpion) CCALLSFSUB7(GET_MOMENTUM,get_momentum,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV,DOUBLEV,p1,p2,q1,q2,pi1,pi2,qpion)

PROTOCCALLSFSUB1(GET_FINAL_MESON_INFO,get_final_meson_info,DOUBLEV)
#define GET_FINAL_MESON_INFO(mesfinpar) CCALLSFSUB1(GET_FINAL_MESON_INFO,get_final_meson_info,DOUBLEV,mesfinpar)

PROTOCCALLSFSUB1(GET_FINAL_TWOPI_INFO,get_final_twopi_info,DOUBLEV)
#define GET_FINAL_TWOPI_INFO(mesfinpar) CCALLSFSUB1(GET_FINAL_TWOPI_INFO,get_final_twopi_info,DOUBLEV,mesfinpar)





Ekhara::Ekhara(const std::string& name, ISvcLocator* pSvcLocator) : Algorithm(name, pSvcLocator)
{
  declareProperty( "Ecm",                 m_cmsEnergy = 3.773 );      //!- CMS-energy (GeV) 
  declareProperty( "InitialEvents",       m_ngUpperBound = 50000 );   //!- # of events to determine the max xs
  declareProperty( "FinalStateID",        m_finalState = 2 );         //!- 1:pi+pi-, 2:pi0, 3:eta, 4:eta'

  declareProperty( "MesonProdAmplitudes", m_mesonAmplitudes = 2 );    //!- pi0,eta,eta': s(1); t(2); s+t(3); t:signal
  declareProperty( "MesonFormfactor",     m_TFF_ID = 8 );             //!- pi0: WZWconst(1); rho pole(2);
                                                                      //!-      LMD(3); LMD+V(4); LMD+Vnew(5);
                                                                      //!-      1-Octet(7); 2-Octet(8);
                                                                      //!- eta/eta': WZWconst(1); VMD(3);
                                                                      //!-           1-Octet(7); 2-Octet(8);
  declareProperty( "PositronThetaMin",    m_posiThetaMin   = 0.0 );   //!- e+ minimum angle 
  declareProperty( "PositronThetaMax",    m_posiThetaMax   = 180.0 ); //!- e+ maximum angle
  declareProperty( "PositronEnergyMin",   m_posiEnergyMin  = 0.0 );   //!- e+ minimum Energy
  declareProperty( "PositronEnergyMax",   m_posiEnergyMax  = 110.0 ); //!- e+ maximum Energy
  declareProperty( "ElectronThetaMin",    m_elecThetaMin   = 0.0 );   //!- e- minimum angle
  declareProperty( "ElectronThetaMax",    m_elecThetaMax   = 180.0 ); //!- e- maximum angle
  declareProperty( "ElectronEnergyMin",   m_elecEnergyMin  = 0.0 );   //!- e- minimum Energy
  declareProperty( "ElectronEnergyMax",   m_elecEnergyMax  = 110.0 ); //!- e- maximum Energy
  declareProperty( "MesonThetaMin",       m_mesonThetaMin  = 0.0 );   //!- pi0, eta, eta' minimum angle
  declareProperty( "MesonThetaMax",       m_mesonThetaMax  = 180.0 ); //!- pi0, eta, eta' maximum angle
  declareProperty( "MesonEnergyMin",      m_mesonEnergyMin = 0.0 );   //!- pi0, eta, eta' minimum Energy
  declareProperty( "MesonEnergyMax",      m_mesonEnergyMax = 100.0 ); //!- pi0, eta, eta' maximum Energy

  declareProperty( "TwoPionProdAmplitudes", m_twoPionAmplitudes = 2 );    //!- pi+pi-:  s(1); s+t(2); s+t+2g(3); s:signal
  declareProperty( "TwoPionThetaMin",     m_twoPionThetaMin     = 50.0);  //!- pion angle cut
  declareProperty( "TwoPionThetaMax",     m_twoPionThetaMax     = 130.0);
  declareProperty( "TwoPionMissThetaMin", m_twoPionMissThetaMin = 15.0);  //!- missing momentum angle cut
  declareProperty( "TwoPionMissThetaMax", m_twoPionMissThetaMax = 165.0);

  declareProperty( "TaggingAngle",        m_taggingAngle   = 20. );   //!- Tagging angle (symmetric) in degrees 
  declareProperty( "TaggingQsquare",      m_taggingQsquare  = 0.3 );   //!- minimum Q^2 of tagged lepton in GeV^2 
  declareProperty( "TaggingMode",         m_taggingMode    = 0 );     //!- Tagging mode 0:no tagging; 1/11:untagged
                                                                      //!-              2/12: single tagging; 3/13:double tagging  


}

StatusCode Ekhara::initialize() {
  MsgStream log(msgSvc(), name());
  log << MSG::DEBUG << "Ekhara in initialize()" << endreq;

  //!- Set Bes unified random engine
  static const bool CREATEIFNOTTHERE(true);
  StatusCode RndmStatus = service("BesRndmGenSvc", p_BesRndmGenSvc, CREATEIFNOTTHERE);
  if (!RndmStatus.isSuccess() || 0 == p_BesRndmGenSvc)
    {
      log << MSG::ERROR << " Could not initialize Random Number Service" << endreq;
      return RndmStatus;
    }
  HepRandomEngine* engine  = p_BesRndmGenSvc->GetEngine("Ekhara");
  EkharaRandom::setRandomEngine(engine);


  log << MSG::INFO << "=============================================================" << endreq;
  log << MSG::INFO << "This is EKHARA, Version 2.1" << endreq;
  switch(m_finalState) {
  case 1:
    log << MSG::INFO << "\tProcess:\te+e- -> e+e- pi+pi-" << endreq;
    break;
  case 2:
    log << MSG::INFO << "\tProcess:\te+e- -> e+e- pi0" << endreq;
    break;
  case 3:
    log << MSG::INFO << "\tProcess:\te+e- -> e+e- eta" << endreq;
    break;
  case 4:
    log << MSG::INFO << "\tProcess:\te+e- -> e+e- etaPRIME" << endreq;
    break;
  default:
    break;
  }

  if(m_finalState !=1) { //!-Single Pseudoscalar Meson
   log << MSG::INFO << "The cross section will be calculated" << endreq;
   log << MSG::INFO << "\tMatrix element: ";
   switch(m_mesonAmplitudes){ //!-  1 - s, 2 - t, 3 - s+t
   case 1:
     log << MSG::INFO << "\t|M_s|^2" << endreq;
     break;
   case 2:
     log << MSG::INFO << "\t|M_t|^2" << endreq;
     break;
   case 3:
     log << MSG::INFO << "\t|M_s + M_t|^2" << endreq;
     break;
   default:
     break;
   }

   log << MSG::INFO << "The following conditions are applied:" << endreq;
   log << MSG::INFO << "\te+   : " << m_posiThetaMin << " < Theta [deg] < " << m_posiThetaMax << endreq; 
   log << MSG::INFO << "\t       " << m_posiEnergyMin << " < Energy [GeV] < " << m_posiEnergyMin << endreq;
   log << MSG::INFO << "\te-   : " << m_elecThetaMin << " < Theta [deg] < " << m_elecThetaMax  << endreq;
   log << MSG::INFO << "\t       " << m_elecEnergyMin << " < Energy [GeV] < " << m_elecEnergyMin << endreq;
   
   double cosTagAngleRad = fabs(cos(m_taggingAngle*TMath::DegToRad()));
   switch(m_taggingMode){ //!-  1:Untagged , 2:Single, 3: double
   case 1:
     log << MSG::INFO << "\t Generating Untagged event configuration!" << endreq;
     log << MSG::INFO << "\t Accepting only events with: |cos(Theta Lepton)| > " << cosTagAngleRad << "!" << endreq;
     break;
   case 2:
     log << MSG::INFO << "\t Generating Single Tagged event configuration!" << endreq;
     log << MSG::INFO << "\t Accepting only events with:" << endreq;
     log << MSG::INFO << "\t\t |cos(Theta e+/-)| > " << cosTagAngleRad  
	 << " and |cos(Theta e-/+)| < " << cosTagAngleRad << endreq;
     break;
   case 3:
     log << MSG::INFO << "\t Generating Double Tagged event configuration!" << endreq;
     log << MSG::INFO << "\t Accepting only events with: |cos(Theta Lepton)| < " << cosTagAngleRad << "!" << endreq;
     break;
   case 11:
     log << MSG::INFO << "\t Generating Untagged event configuration!" << endreq;
     log << MSG::INFO << "\t Accepting only events with: Q^2 (Lepton)| < " << m_taggingQsquare << "!" << endreq;
     break;
   case 12:
     log << MSG::INFO << "\t Generating Single Tagged event configuration!" << endreq;
     log << MSG::INFO << "\t Accepting only events with:" << endreq;
     log << MSG::INFO << "\t\t Q^2 (e+/-) > " << m_taggingQsquare  
	 << " and Q^2 (e-/+) < " << m_taggingQsquare << endreq;
     break;
   case 13:
     log << MSG::INFO << "\t Generating Double Tagged event configuration!" << endreq;
     log << MSG::INFO << "\t Accepting only events with: Q^2 (Lepton) > " << m_taggingQsquare << "!" << endreq;
     break;
   default:
     if(m_taggingMode>0) {
       log << MSG::INFO << "\t Unknown tagging mode selected!" << endreq;
       log << MSG::INFO << "\t Generating events without tagging configuration!" << endreq;
       m_taggingMode = 0;
     }
     break;
   }
   
   switch(m_finalState) {
   case 2:
     log << MSG::INFO << "\tpi0  : ";
     break;
   case 3:
     log << MSG::INFO << "\teta  : ";
     break;
   case 4:
     log << MSG::INFO << "\teta' : ";
     break;
   default:
     break;
   }
   log << MSG::INFO << m_mesonThetaMin << " < Theta [deg] < " << m_mesonThetaMax  << endreq;
   log << MSG::INFO << "\t       " << m_mesonEnergyMin << " < Energy [GeV] < " << m_mesonEnergyMin << endreq;
  } else {  //!- pi+pi-
    log << MSG::INFO << "\tMatrix element: ";
    switch(m_twoPionAmplitudes){ //!-  1 - s, 2 - s+t, 3 - s+t+g
    case 1:
      log << MSG::INFO << "\t|M_s|^2" << endreq;
      break;
    case 2:
      log << MSG::INFO << "\t|M_s + M_t|^2" << endreq;
      break;
    case 3:
      log << MSG::INFO << "\t|M_s + M_t + M_2g|^2" << endreq;
      break;
    default:
      break;
    }
    log << MSG::INFO << "The following conditions are applied:" << endreq;
    log << MSG::INFO << "\tPion Momentum:    " << m_twoPionThetaMin << " < Theta [deg] < " 
	<< m_twoPionThetaMax << endreq;
    log << MSG::INFO << "\tMissing Momentum: " << m_twoPionMissThetaMin << " < Theta [deg] < "
	<< m_twoPionMissThetaMax << endreq;
  }
  log << MSG::INFO << "=============================================================" << endreq;

  //!- Transfer integer parameters to FORTRAN common blocks
  CHANNELSEL.channel_id = m_finalState;
  SWDIAG.sw_2pi = m_twoPionAmplitudes;
  SWDIAG.sw_1pi = m_mesonAmplitudes;
  PIONFFSW.piggFFsw = m_TFF_ID;
  TAGGINGMODE.tagmode = m_taggingMode;

  //!- Prepare floating point parameters to be transfered to quadruple precision
  double xpar[20];
  xpar[0]  = m_cmsEnergy;
  xpar[1]  = m_ngUpperBound;
  xpar[2]  = m_posiThetaMin;
  xpar[3]  = m_posiThetaMax;
  xpar[4]  = m_posiEnergyMin;
  xpar[5]  = m_posiEnergyMax;
  xpar[6]  = m_elecThetaMin;
  xpar[7]  = m_elecThetaMax;
  xpar[8]  = m_elecEnergyMin;
  xpar[9]  = m_elecEnergyMax;
  xpar[10] = m_mesonThetaMin;
  xpar[11] = m_mesonThetaMax;
  xpar[12] = m_mesonEnergyMin;
  xpar[13] = m_mesonEnergyMax;
  xpar[14] = m_twoPionThetaMin;
  xpar[15] = m_twoPionThetaMax;
  xpar[16] = m_twoPionMissThetaMin;
  xpar[17] = m_twoPionMissThetaMax;
  xpar[18] = m_taggingAngle;
  xpar[19] = m_taggingQsquare;

  //!- Transfer floating point parameters to FORTAN routines
  BOSS_INIT_READ(xpar);

  //!- Initialize FORTAN routines
  EKHARA(-1);

  //!- Initialize event counter
  //should be replaced by reques to ApplicationManager for EvtMax
  m_totalEvents = 0;
  
  return StatusCode::SUCCESS;
}



StatusCode Ekhara::execute() {
  MsgStream log(msgSvc(), name());
  log << MSG::DEBUG << "Ekhara in execute()" << endreq;
  
  EKHARA(0);

  double p1[4],p2[4],q1[4],q2[4];
  double pi1[4],pi2[4],qpion[4];
  
  for (int i=0;i<4;i++) {
    p1[i]=0.0;
    p2[i]=0.0;
    q1[i]=0.0;
    q2[i]=0.0;
    pi1[i]=0.0;
    pi2[i]=0.0;
    qpion[i]=0.0;
  }

  GET_MOMENTUM(p1,p2,q1,q2,pi1,pi2,qpion);


  // Fill event information
  GenEvent* evt = new GenEvent(1,1);

  GenVertex* prod_vtx = new GenVertex();
  evt->add_vertex( prod_vtx );

  // incoming beam e+
  GenParticle* p = new GenParticle( HepLorentzVector(p1[1],p1[2],p1[3],p1[0]), -11, 3); 
  p->suggest_barcode(1 );
  prod_vtx->add_particle_in(p);

  // incoming beam e-
  p = new GenParticle(HepLorentzVector(p2[1],p2[2],p2[3],p2[0]), 11, 3); 
  p->suggest_barcode( 2 );
  prod_vtx->add_particle_in(p);

  // outcoming beam e+
  p = new GenParticle(HepLorentzVector(q1[1],q1[2],q1[3],q1[0]), -11, 1); 
  p->suggest_barcode(3 );
  prod_vtx->add_particle_out(p);

  // outcoming beam e-
  p = new GenParticle( HepLorentzVector(q2[1],q2[2],q2[3],q2[0] ), 11, 1); 
  p->suggest_barcode( 4 );
  prod_vtx->add_particle_out(p);

  switch (m_finalState) { 
  case 1: // e+e-pi+ pi-
    p = new GenParticle( HepLorentzVector(pi1[1],pi1[2],pi1[3],pi1[0]), 211,1); 
    p->suggest_barcode( 5 );
    prod_vtx->add_particle_out(p);
    p = new GenParticle( HepLorentzVector(pi2[1],pi2[2],pi2[3],pi2[0]),-211,1); 
    p->suggest_barcode( 6 );
    prod_vtx->add_particle_out(p);
    break;
  case 2: //e+e- pi0
    p = new GenParticle( HepLorentzVector( qpion[1],qpion[2],qpion[3],qpion[0]), 111, 1); 
    p->suggest_barcode( 5 );
    prod_vtx->add_particle_out(p);
    break;
  case 3: //e+e- eta
    p = new GenParticle( HepLorentzVector( qpion[1],qpion[2],qpion[3],qpion[0]), 221, 1); 
    p->suggest_barcode( 5 );
    prod_vtx->add_particle_out(p);
    break;
  case 4: //e+e- etaPrime
    p = new GenParticle( HepLorentzVector( qpion[1],qpion[2],qpion[3],qpion[0]), 331, 1); 
    p->suggest_barcode( 5 );
    prod_vtx->add_particle_out(p);
    break;
  default:
    break;
  }


  if( log.level() <= MSG::INFO ) {
    evt->print();
  }
  
  // Check if the McCollection already exists
  SmartDataPtr<McGenEventCol> anMcCol(eventSvc(), "/Event/Gen");
  if (anMcCol!=0) {
    log << MSG::DEBUG << "Adding McGenEvent to existing collection" << endreq;
    McGenEvent* mcEvent = new McGenEvent(evt);
    anMcCol->push_back(mcEvent);
  } else {
    McGenEventCol *mcColl = new McGenEventCol;
    McGenEvent* mcEvent = new McGenEvent(evt);
    mcColl->push_back(mcEvent);
    StatusCode sc = eventSvc()->registerObject("/Event/Gen",mcColl);
    if (sc != StatusCode::SUCCESS) {
      log << MSG::ERROR << "Could not register McGenEvent" << endreq;
      delete mcColl;
      delete evt;
      delete mcEvent;
      return StatusCode::FAILURE;
    } else {
      log << MSG::INFO << "McGenEventCol created " << endreq;
    }
  }

  m_totalEvents++;

  log<<MSG::DEBUG<< "before execute() return"<<endreq;
  return StatusCode::SUCCESS;
}



StatusCode Ekhara::finalize() {
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "Ekhara in finalize()" << endreq;

  if( log.level() <= MSG::INFO ) {
    DIAGNOSE();
  }

  EKHARA(1);
  
  char xsect[100];
  if(m_finalState!=1) {
    double mesfinpar[10];
    for(int i=0; i<10; i++) {
      mesfinpar[i]=0.0;
    }
    GET_FINAL_MESON_INFO(mesfinpar);

    log << MSG::INFO << "Total number of iterations = " << mesfinpar[0] << endreq;
    log << MSG::INFO << "Number of MC trials = " << mesfinpar[1] << endreq;
    log << MSG::INFO << "ContribMax_MCloop  = " << mesfinpar[2] << endreq;
    log << MSG::INFO << "MC efficiency (evt/eff.iter) = " << m_totalEvents/mesfinpar[1] << endreq;

    log << MSG::INFO << "================================" << endreq;
    log << MSG::INFO << "From all weights:" << endreq;
    double CS  = mesfinpar[3]/mesfinpar[0];
    double CSerr = sqrt( fabs(mesfinpar[4]/mesfinpar[0]-CS*CS) / (mesfinpar[0]-1.) );
    sprintf(xsect," Integral Cross Section = %1.5E +- %1.5E  [GeV^-2]",CS,CSerr);
    log << MSG::INFO << xsect << endreq;
    sprintf(xsect,"                        = %1.5E +- %1.5E  [nb]",CS*mesfinpar[6],CSerr*mesfinpar[6]);
    log << MSG::INFO << xsect << endreq;
    log << MSG::INFO << endreq;
    log << MSG::INFO << "From UNWEIGHTED events:" << endreq;
    if (mesfinpar[1] <= 0.) {
      CS = 0.;
      CSerr = 0.;
    } else {
      CS = mesfinpar[5] * m_totalEvents / mesfinpar[1];
      CSerr = sqrt(CS*(mesfinpar[5] - CS)) / sqrt(mesfinpar[1]);
    }
    sprintf(xsect," Integral Cross Section = %1.5E +- %1.5E  [GeV^-2]",CS,CSerr);
    log << MSG::INFO << xsect << endreq;
    sprintf(xsect,"                        = %1.5E +- %1.5E  [nb]",CS*mesfinpar[6],CSerr*mesfinpar[6]);
    log << MSG::INFO << xsect << endreq;
    log << MSG::INFO << "================================" << endreq;

    if (mesfinpar[7] > 0.) {
      log << MSG::WARNING << "There were overshootings." << endreq;
      log << MSG::WARNING << "The events, corresponding to them were ignored." << endreq;
      log << MSG::WARNING << "The unweighted sample may be not reliable!" << endreq;
      log << MSG::WARNING << "\nContribution from overshooted events:" << endreq;
      sprintf(xsect," Integral Cross Section = %1.5E +- %1.5E  [GeV^-2]",mesfinpar[8],mesfinpar[9]);
      log << MSG::WARNING << xsect << endreq;
      sprintf(xsect,"                        = %1.5E +- %1.5E  [nb]",mesfinpar[8]*mesfinpar[6],mesfinpar[9]*mesfinpar[6]);
      log << MSG::WARNING << xsect << endreq;
      log << MSG::INFO << "\n================================" << endreq;
    }
  } else {
    double twopifinpar[2];
    for(int i=0; i<2; i++) {
      twopifinpar[i]=0.0;
    }
    GET_FINAL_TWOPI_INFO(twopifinpar);

    double CS = twopifinpar[0]/m_totalEvents;
    double CSerr = sqrt((twopifinpar[1]/m_totalEvents-CS*CS)/(m_totalEvents-1.));
    log << MSG::INFO << "================================" << endreq;
    sprintf(xsect," Cross Section = %1.5E +- %1.5E  [nb]",CS,CSerr);
    log << MSG::INFO << xsect << endreq;
    log << MSG::INFO << "================================" << endreq;
  }

  log << MSG::DEBUG << "Ekhara has terminated successfully" << endreq;

  return StatusCode::SUCCESS;
}


