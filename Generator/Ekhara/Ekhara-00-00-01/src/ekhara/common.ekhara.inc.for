c-----------------------------------------------------------------------
c
c     EKHARA. include file <common.ekhara.inc.for>.
c     All the COMMON blocks are collected here.
c
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------
c     general purpose commons 
c-----------------------------------------------------------------------
      real*16 me2,me,mpi2,mpi,alpha,gev2nbarn,
     &        mpi02,mpi0,meta2,meta,metaP2,metaP
      real*16 pi

      common /mass_and_coupl/mpi2,me2,me,mpi,alpha,
     &        mpi02,mpi0,meta2,meta,metaP2,metaP

      common /const/pi,gev2nbarn

      real*16 epsilon_const(0:3,0:3,0:3,0:3)
      common /eps/epsilon_const
      
      common /FactorInCS/ FluxFactor
      real*16 FluxFactor

      common /NumGenerEvts/ nges
      real*16 nges              ! number of generated events
c-----------------------------------------------------------------------
c     channel and mode selections commons
c-----------------------------------------------------------------------
      ! general channel selection (2 pion, 1 pion, 1 eta, ...)
      common /channelsel/channel_id
      integer channel_id

      ! included contributions to the matrix element
      common /swdiag/sw_2pi, sw_1pi
      integer sw_2pi, sw_1pi

      ! option to write events into file or not.
      common /WriteEvents/WriteEvents
      integer WriteEvents

      ! option to write (0) to stdout or not (1).
      common /sw_silent/sw_silent
      integer sw_silent

      ! option to use seed
      common /SeedMode/SeedMode
      integer SeedMode

      integer EvtsFile
      common /EventsFileHandling/EvtsFile

c-----------------------------------------------------------------------
c     parameters (e+e- ->  e+e- pi pi) 
c-----------------------------------------------------------------------
      real*16 c_0_pion,c_1_pion,c_2_pion,c_3_pion,c_n_pion,c_om_pion
     &       ,m_rho0_pion,g_rho0_pion,m_rho1_pion,g_rho1_pion
     &       ,m_rho2_pion,g_rho2_pion,m_rho3_pion,g_rho3_pion
     &       ,m_om0_pion,g_om0_pion
      integer FF_pion
      real*16 mrho,mrhol,momega,al,be
      real*16 constant

      common /ffparam/ 
     &        c_0_pion,c_1_pion,c_2_pion,c_3_pion,c_n_pion,c_om_pion
     &       ,m_rho0_pion,g_rho0_pion,m_rho1_pion,g_rho1_pion
     &       ,m_rho2_pion,g_rho2_pion,m_rho3_pion,g_rho3_pion
     &       ,m_om0_pion,g_om0_pion,FF_pion
      common /ffparam_t/mrho,mrhol,momega,al,be 
      common /const_2pi/constant

c-----------------------------------------------------------------------
c     parameters for (e+e- ->  e+e- pi0 )  
c-----------------------------------------------------------------------
      ! option for the change of variables
      common /changesel/change_id_s, change_id_g
      integer change_id_s, change_id_g

      real*16 FetaP,Feta,Fpi,pggCoupling
      common /param_1pi_couplings/FetaP,Feta,Fpi,pggCoupling

      real*16 ngUpperBound, ignoreUB, UBEnlargeFactor
      common /param_1pi_other/ngUpperBound, ignoreUB, UBEnlargeFactor

      ! option calculate the volume of the phase space
      integer PhSpOnly
      common /PhSpOnly/PhSpOnly

      ! indicates which routine is used to generate the phase space
      integer WorkingGeneratorId
      common /GeneratorLed/WorkingGeneratorId

      ! option to symmetrize the PhSp generation wrt (e+ <-> e-)
      integer DoSymmetrize
      common /SymmPSParameters/DoSymmetrize


      ! selects the formula for pion transition form factor
      integer piggFFsw
      common /pionFFsw/ piggFFsw

c-----------------------------------------------------------------------
c     pion form factor ("2pi" mode)
c-----------------------------------------------------------------------
      common /PFFcom/ PFF,PFFS,gamma
      complex*32 PFF,PFFS,gamma(0:3)

c-----------------------------------------------------------------------
c     initial spinors
c-----------------------------------------------------------------------
      common/spinory_ini/vp1,vp2,up1,up2
      complex*32  vp1(2,2),vp2(2,2),up1(2,2),up2(2,2)
      common/gammu_in/gammuin
      complex*32  gammuin(0:3,2,2)

c-----------------------------------------------------------------------
c     final spinors
c-----------------------------------------------------------------------
      common/spinory_fin/v1,v2,ub1,ub2,gammu
      complex*32 gammu(0:3,2,2),v1(2,2),v2(2,2),ub1(2,2),ub2(2,2)

      common/spinory_int/gammuT1, gammuT2
      complex*32 gammuT1(0:3,2,2), gammuT2(0:3,2,2)

c-----------------------------------------------------------------------
c     KLOE cuts  ("2pi" mode)
c-----------------------------------------------------------------------
      common /cuts/ pi1lab_min,pi1lab_max,pipicut1_min,pipicut1_max
      real*16 pi1lab_min,pi1lab_max,pipicut1_min,pipicut1_max

c-----------------------------------------------------------------------
c     Kinematic cuts  ("1pi" mode)
c-----------------------------------------------------------------------
      common /KinCuts/
     &        CUT_t1max, CUT_t1min, CUT_t2max, CUT_t2min,
     &        CUT_E1min, CUT_E1max, CUT_th1min, CUT_th1max,
     &        CUT_E2min, CUT_E2max, CUT_th2min, CUT_th2max,
     &          CUT_Epionmin, CUT_Epionmax, 
     &          CUT_thpionmin, CUT_thpionmax

      real*16 CUT_t1max, CUT_t1min, CUT_t2max, CUT_t2min

      real*16 CUT_E1min, CUT_E1max, CUT_th1min, CUT_th1max
      real*16 CUT_E2min, CUT_E2max, CUT_th2min, CUT_th2max

      real*16 CUT_Epionmin, CUT_Epionmax
      real*16 CUT_thpionmin, CUT_thpionmax

c-----------------------------------------------------------------------
c     old-style histogramming
c-----------------------------------------------------------------------
      common /histo/ag,bg,ng,lh
      integer lh               ! total number of histograms (1 to 9)
      integer ng(0:9)          ! number of bins for each histogram (1 to 100)
      real*16 ag(0:9)          ! lower limit for each histogram
      real*16 bg(0:9)          ! upper limit for each histogram

      ! option to do histogramming or to ignore
      common /NeedHisto/NeedHisto
      integer NeedHisto


c----------------------------------------------------------------------
c     new-style histograming
c----------------------------------------------------------------------
      common /HistoParams/ loLimH, upLimH, normH, nHistosH, nBinsH

      real*16 loLimH(1:20)   ! lower limit for each histogram
      real*16 upLimH(1:20)   ! upper limit for each histogram

      real*16 normH(1:20)    ! extra normalization factor for each histogram
                             ! (e.g., put here the conversion from GeV^2 to nb)

      integer nHistosH !lh   ! total number of histograms (1 to 20)
      integer nBinsH(1:20)   ! number of bins for each histogram (1 to 500)


      ! name of the files to save the histograms
      common /HistoFileOut/ histofilename
      Character*51 histofilename(1:20)

      ! option to do histogramming or to ignore
      common /DoHisto/ DoHisto
      integer DoHisto

      ! data collected in histograms
      common/HistoData/ bins,bins2, loBin,upBin, sum1,sum2
      real*16  sum1,sum2,                  ! sum of the ALL contributions, sum of the squares of those.
     &   bins(1:20,1:500),bins2(1:20,1:500), ! each bin's value and sum of squares of contributions to each bin.
     &   loBin(1:20,1:500),upBin(1:20,1:500) ! lower and upper boundary for each bin.

      common/HistoData3D/ bins_3d, bins2_3d, 
     & loBin_1_3d, upBin_1_3d, loBin_2_3d, upBin_2_3d
      real*16  
     &   bins_3d(1:500,1:500),bins2_3d(1:500,1:500), ! each bin's value and sum of squares of contributions to each bin.
     &   loBin_1_3d(1:500,1:500),upBin_1_3d(1:500,1:500), ! lower and upper boundary for each bin.
     &   loBin_2_3d(1:500,1:500),upBin_2_3d(1:500,1:500) ! lower and upper boundary for each bin.

c-----------------------------------------------------------------------
c     kinematics - general
c-----------------------------------------------------------------------
      complex*32 sigpl(0:3,2,2),sigmi(0:3,2,2)
      common /sigmatrix/sigpl,sigmi

      real*16 tau(0:3),p1(0:3),p2(0:3),q1(0:3),q2(0:3)
      common /inoutmomenta/tau,p1,p2,q1,q2

      real*16 s, ss          ! s, sqrt(s)
      common /energy/ s, ss

      logical OutFlag        ! to be set to .true. only
                             !  - within the generator-routine
                             !  - when event is out of the phase space

      common /flags/ OutFlag

      integer UnstableCounter
      common /AuxCounters/ UnstableCounter

c
c  4-momenta:
c     p1 --- initial positron
c     p2 --- initial electron
c     q1 --- final positron
c     q2 --- final electron
c
c     tau --- ?

c-----------------------------------------------------------------------
c     kinematics (e+e- ->  e+e- pi pi) 
c-----------------------------------------------------------------------
      real*16 ququ,qutau,qupqup,p1qu,p1pi1,quppi1,taupi1,p1qup,ququp
      real*16 k1k1,gamma2
      complex*32 taugamma,qupgamma,p1gamma,taugammab,qupgammab
     1       ,p1gammab
      real*16 t,t1,mian9b,mian10b,mian11b,mian12b,qupppi1,p2pi1
      real*16 mian7b,mian8b
      real*16 quppqupp,sququ
      real*16 qu(0:3),qup(0:3)
     1 ,pi1(0:3),pi2(0:3),k1(0:3)
      common /vector/qu,qup,pi1,pi2,k1
      common /sc_pr/ququ,qutau,qupqup,p1qu,p1pi1,quppi1,taupi1,p1qup
     1             ,ququp,p2pi1,qupppi1,quppqupp,sququ,k1k1
     2             ,taugamma,qupgamma,p1gamma,taugammab,qupgammab
     3             ,p1gammab,gamma2
      common /den/t,t1,mian9b,mian10b,mian11b,mian12b,mian7b,mian8b


      real*16 ququmax,dququdr2,logg,aaamin,stala,bbbmin,wwmin,zzmax
     &        ,czynnik,czyn_pi
      common /stale_phsp/ququmax,dququdr2,logg,aaamin,stala,bbbmin
     &       ,wwmin,zzmax,czynnik,czyn_pi 


c-----------------------------------------------------------------------
c     kinematics - only related to ( e+e- ->  e+e- pi0 )
c-----------------------------------------------------------------------
      common /Jacobians/
     &       JacobianFactor, PhaseVol, Dab
      real*16 JacobianFactor, PhaseVol, Dab
c-----------------------------------------------------------------------
      common /PhaseSpaceConfiguration/ 
     &       qpion, qpionabs,
     &       q1abs, q2abs, 
     &       l1, l2, k, g1

      real*16 q1abs, q2abs
      real*16 qpionabs ! 3-momentum abs value
      real*16 qpion(0:3) 
      ! qpion(0:3)  is the final pseudoscalar's 4-momentum.
      ! in the code it is supposed that this particle is on-shell,
      ! namely it is PION, ETA or ETA' -- the explicit choice is selected by 
      ! <channel_id> parameter.
!                channel <3>             'e+e- -> e+e-eta'
!                channel <2>             'e+e- -> e+e-pi0'
!                channel  <default>=<1>  'e+e- -> e+e-pi pi'
      real*16 k(0:3)
            ! k(0:3) is 4-vector of final electron-positron pair
            !      (i.e. second virtual photon)
      real*16 g1(0:3) 
            !

      real*16 l1(0:3), l2(0:3)
        !  l1 and l2 notation is used to label the virtual-photon momenta
        !            which appear in the t-channel diagram:
        !            l1 = p1 - q1
        !            l2 = p2 - q2

      ! (undocumented features..)

      ! decay products (photons) of the PS
      common /PSgg_decay_mom/  p_gam_1, p_gam_2, THrel, cTHrel
      real*16 p_gam_1(0:3) ! 4-momentum of the first  photon
      real*16 p_gam_2(0:3) ! 4-momentum of the second photon
      real*16 THrel,cTHrel ! angle between these two photons  and its cosine

c-----------------------------------------------------------------------
      common /KinFactors/
     &        beta, KW, nuKIN, W, W2
      real*16 beta, KW, nuKIN, W, W2
c-----------------------------------------------------------------------
      common /KinInvariants/
     &        it1, it2, s1, s2, kk, t_taged
      real*16 it1, it2, s1, s2, kk, t_taged

            ! kk= k**2 is the invariant mass squared used for
            !      phase space factorization in s-specific generator
c-----------------------------------------------------------------------
      common /KinLims/
     &        t2max, t2min, t1max, t1min
      real*16 t2max, t2min, t1max, t1min
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------
c     Monte Carlo - ( e+e- ->  e+e- pi0 )
c-----------------------------------------------------------------------
      common /MCloopCOMMONS/
     & totIterations, MCTrialsCount,
     & Msq, CS, CSerr, contrib, 
     &     Integralcontrib, Integralcontrib2, 
     &     IntegralcontribSAMPLE, CSerr2,
     &     UpperBound,
     & IntegralOS,Integral2OS,
     &  OSCounter, CS_OS, CSerr_OS,
     &  ContribMax_MCloop
     
      real*16 totIterations, MCTrialsCount ! integer may be not enough for long runs
      real*16 Msq, CS, CSerr, contrib, 
     &     Integralcontrib, Integralcontrib2, 
     &     IntegralcontribSAMPLE, CSerr2,
     &     UpperBound
      real*16 IntegralOS,Integral2OS         ! for overshooted events
      real*16 OSCounter, CS_OS, CSerr_OS     ! for overshooted events
      real*16 ContribMax_MCloop


c-----------------------------------------------------------------------
c     RxT parameters for the form factor  ( e+e- ->  e+e- pi0 )
c-----------------------------------------------------------------------
      real*16               Mk
      common /masses_kaon/  Mk
      real*16            Fk, Fchir, F8, F0, EPSomph, EPSprime, 
     &                   Hv, Fsmv, SIGv, Fv, Gv,
     &                   Hv1, Fsmv1, SIGv1, 
     &                   Fsmv2Hv2, Fsmv2sqSIGv2 
      common /misc_c/    Fk, Fchir, F8, F0, EPSomph, EPSprime,
     &                   Hv, Fsmv, SIGv, Fv, Gv,
     &                   Hv1, Fsmv1, SIGv1, 
     &                   Fsmv2Hv2, Fsmv2sqSIGv2 

      real*16            Mrho_c, Momega_c, Mphi_c
      common /masses_c/  Mrho_c, Momega_c, Mphi_c

      real*16            Mrho_pr, Momega_pr, Mphi_pr
      common /masses_pr/  Mrho_pr, Momega_pr, Mphi_pr

      real*16            GammaTotalrho_c, GammaTotalomega_c, 
     &                   GammaTotalphi_c
      common /widths_c/  GammaTotalrho_c, GammaTotalomega_c, 
     &                   GammaTotalphi_c

      real*16            GammaTotalrho_pr, GammaTotalomega_pr,	 
     &                   GammaTotalphi_pr
      common /widths_c/  GammaTotalrho_pr, GammaTotalomega_pr, 
     &                   GammaTotalphi_pr

      real*16               theta0, theta8, Cs_c, Cq_c   
     &                      , Cs_P, Cq_P   
      common /etamixing/    theta0, theta8, Cs_c, Cq_c   
     &                      , Cs_P, Cq_P   


c-----------------------------------------------------------------------
c    parameters for tagged event configuration cuts at BESIII
c-----------------------------------------------------------------------
      integer tagmode
      common /taggingmode/ tagmode
      real*16 tagangle, tagqsqr
      common /taggingangle/ tagangle, tagqsqr


c    8>< ------------------  End of file  ------------------------- ><8
