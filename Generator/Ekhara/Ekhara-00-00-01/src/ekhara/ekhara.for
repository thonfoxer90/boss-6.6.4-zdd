
c----------------------------------------------------------------------c
c                                                                      c
c     E K H A R A       M o n t e  C a r l o    G e n e r a t o r      c
c                                                                      c
c     version 2.1                                                      c
c                                                                      c
c                  http://uranos.cto.us.edu.pl/~ekhara/                c
c                                                                      c
c         Form factors for [gamma gamma ->  pi0, eta, eta']            c
c     (c) 2012 H.Czyz, S.Ivashyn, A.Korchin, O.Shekhovtsova            c
c                                                                      c
c                   e+e- -> e+e- pi0, eta, eta'                        c
c     (c) 2010 Henryk Czyz, Sergiy Ivashyn                             c
c                                                                      c
c                   e+e- -> e+e- pi+ pi-                               c
c     (c) 2006 Henryk Czyz, Elzbieta Nowak-Kubat                       c
c                                                                      c
c----------------------------------------------------------------------c

c----------------------------------------------------------------------c
c                                                                      c
c  IMPORTANT:                                                          c
c  routines, which are supposed to be changed by user are              c
c            located in   "./ekhara-routines/routines-user.inc.for"    c
c                                                                      c
c----------------------------------------------------------------------c

c----------------------------------------------------------------------c
! REFERENCES: Form factors for [gamma gamma ->  pi0, eta, eta']        c
!                                                                      c
! H.Czyz, S.Ivashyn, A.Korchin, O.Shekhovtsova (2012),                 c
!             preprint IFIC/11-63, FTUV/2011-1110                      c
!             arXiv: look up in INSPIRE                                c
c----------------------------------------------------------------------c
! REFERENCES: e+e- -> e+e- pi0                                         c
!                                                                      c
! Henryk Czyz, Sergiy Ivashyn (2010),                                  c
!             Comput.Phys.Commun. 182 (2011) 1338-1349                 c
c----------------------------------------------------------------------c
! REFERENCES: e+e- -> e+e- pi+ pi-                                     c
!                                                                      c
! Henryk Czyz, Elzbieta Nowak-Kubat, Phys. Lett. B 634, 493 (2006),    c
!                                    [hep-ph/0601169].                 c
! Henryk Czyz, Elzbieta Nowak-Kubat, Acta Phys. Polon. B36, 3425 (2005), 
!                                    [hep-ph/0510287].                 c
! Henryk Czyz, Elzbieta Nowak, Acta Phys. Polon. B34, 5231 (2003),     c
!                                    [hep-ph/0310335].                 c
c----------------------------------------------------------------------c

c**********************************************************************
      subroutine EKHARA(stage)
      implicit none
      include 'common.ekhara.inc.for'
      integer  stage
      select case(stage) 
            case (-1)
               ! initialize ekhara
               !call EKHARA_INIT_read
               call EKHARA_INIT_set
               call EKHARA_INIT_UB
            case (0)
               ! generate an event
               call EKHARA_RUN
            case (1)
               ! finalize ekhara
               call EKHARA_FIN
      end select
      return
      end

c**********************************************************************
      subroutine EKHARA_INIT_read
      implicit none
      include 'common.ekhara.inc.for'
      CHARACTER msg*78         ! for logging
      integer st(0:104)         ! random seed
      logical ex                ! to check the existence of a file

      call opfiles              ! open all output files

      call runlog(' ')
      call runlog('=========================================')
      call runlog('This is EKHARA, version 2.1')
      call std_out(' ')
      call std_out('=========================================')
      call std_out('This is EKHARA, version 2.1')

      call runlog('Loading parameters')
      ! input reads the input file with selection of mode and parameters 
      call input

      call opfiles_events              ! open the output files for events
                                       ! (if necessary)

      ! reading the random seed
      call runlog('Prepare random number generator..')

      if (SeedMode.eq.2) then    ! <1> = Use constant seed, <2> = use (consequently) variable seed
         call runlog('mode: variable seed from <seed-v.dat>')
         inquire(file="seed-v.dat", exist=ex)
         if (ex) then
            open(9,file='seed-v.dat',status='old')
            read(9,*)st
            close(9)
         else
            call warnlog('mode: variable seed from <seed-v.dat>')
            call warnlog('file <seed-v.dat> does not exist.')
            call warnlog('Trying to read <seed.dat>')
            inquire(file="seed.dat", exist=ex)
            if (ex) then
              open(9,file='seed.dat',status='old')
              read(9,*)st
              close(9)
            else
              call errlog('file <seed.dat> does not exist.')
              call errlog('Try running <seed_prod.exe>')
              call errlog('    to create a seed from scratch.')
              call prg_abort
            endif
         endif
      endif

      if (SeedMode.eq.1) then   !Use constant seed =>1, 
                                !use variable seed =>2
         call runlog('mode: constant seed from <seed.dat>')
         inquire(file="seed.dat", exist=ex)
         if (ex) then
            open(9,file='seed.dat',status='old')
            read(9,*)st
            close(9)
         else
            call errlog('file <seed.dat> does not exist.')
            call errlog('Try running <seed_prod.exe>')
            call errlog('    to create a seed from scratch.')
            call prg_abort
         endif
      endif

      call runlog('Random seed loaded')

      ! RANLUX initialization
      !call rlxdresetf(st) 
      call runlog('Ranlux is initialized')

      return
      end

c**********************************************************************
      subroutine EKHARA_INIT_set
      implicit none
      include 'common.ekhara.inc.for'

      ! set the momenta of initial e+ and e- (they will never change)
      call init_in_momenta
      if ((channel_id .eq. 1)) then
             call runlog('subroutine mc_loop_selector: channel <1>')
             call runlog('                      e+e- -> e+e-pi+pi-')
             call std_out('                     e+e- -> e+e-pi+pi-')

             ! initialize..
             call init_2pi

             ! initialization of histograming tool
             call runlog('Initialization of histograms.')
             call hist_init_2pi('histo-settings_2pi.dat')

      elseif ((channel_id .eq. 2).or.(channel_id .eq. 3)
     &         .or. (channel_id .eq. 4) ) then 
             if (channel_id .eq. 2) then 
                call runlog('subroutine mc_loop_selector: channel <2>')
                call runlog('                         e+e- -> e+e-pi0')
                call std_out('Process:                e+e- -> e+e-pi0')
                ! initialize the pseudoscalar mass etc..
                call init_1pi
             endif
             if (channel_id .eq. 3) then 
                call runlog('subroutine mc_loop_selector: channel <3>')
                call runlog('                         e+e- -> e+e-eta')
                call std_out('Process:                e+e- -> e+e-eta')
                ! initialize the pseudoscalar mass etc..
                call init_1eta
             endif
             if (channel_id .eq. 4) then 
                call runlog('subroutine mc_loop_selector: channel <4>')
                call runlog('                    e+e- -> e+e-etaPRIME')
                call std_out('Process:           e+e- -> e+e-etaPRIME')
                ! initialize the pseudoscalar mass etc..
                call init_1etaP
             endif


             ! control the cuts to be cosistent (simple check)
             call control_cuts_simple_1pi
             
             ! initialization of histograming tool
             call runlog('Initialization of histograms.')
             call hist_init_1pi('histo-settings_1pi.dat')
                        ! Warning: number of histograms <=20 !!

             ! Tell what we are going to calculate
             if (PhSpOnly .ne. 1) then
                  call runlog('The cross section will be calculated')
                  call runlog('Matrix element: ')
                  call std_out('The cross section will be calculated')
                  call std_out('Matrix element: ')
                  select case(sw_1pi) !  1 - s, 2 - t, 3 - s+t
                    case (1)
                        call runlog('               |M_s|^2')
                        call std_out('               |M_s|^2')
                    case (2)
                        call runlog('               |M_t|^2')
                        call std_out('               |M_t|^2')
                    case (3)
                        call runlog('         |M_s + M_t|^2')
                        call std_out('         |M_s + M_t|^2')
                  end select
             else
                  call runlog('Estimating the phase space volume only')
                  call std_out('Estimating the phase space volume only')
             endif

             if (WorkingGeneratorId .eq. 1) then
                  call runlog('Generator type: s-specific')

                  select case(change_id_s) ! change of variables
                  case (4)
                        call runlog('change of variables: [ log ]')
                  case (3)
                        call runlog('change of variables: [ log^3 ]')
                  case (2)
                        call runlog('change of variables: [ plain ]')
                  case default ! default is also (1)
                    change_id_s = 2
                    call warnlog('change_id_s has non-supported value.')
                    call warnlog('                     -> changed to 2')
                    call runlog('change of variables: [ plain ]')
                  end select
            elseif (WorkingGeneratorId .eq. 2) then 
                  call runlog('Generator type: t-specific')

                  ! initialize the t-specific generator (cuts etc.)
                  !!!!!
                  call t_init
                  !!!!!

                  select case(change_id_g) ! change of variables
                      case (3)
                          call runlog('change of variables: [ t1 t2 ]')
                      case (2)
                          call runlog('change of variables: [ plain ]')
                      case default ! default is also (1)
                          change_id_g = 2
                    call warnlog('change_id_g has non-supported value.')
                    call warnlog('                     -> changed to 2')
                    call runlog('change of variables: [ plain ]')
                  end select
            elseif (WorkingGeneratorId .eq. 3) then 
                  call runlog('Generator type: (s+t)-specific')

                  ! initialize the t-specific generator (cuts etc.)
                  !!!!!
                  call t_init
                  !!!!!

                  change_id_s = 4 ! (s+t)-specific supports ONLY
                                  ! <4> log (most efficient)
                  call runlog('change of variables: [ log ]')
                  change_id_g = 3 ! (s+t)-specific supports ONLY
                                  ! <3> t1 t2
                  call runlog('change of variables: [ t1 t2 ]')

            else
              call errlog('WorkingGeneratorId has non-supported value.')
             write(*,*)'This is where I failed you'
              call prg_abort
            endif

            ! initialization of MC
            call mc_init_1pi
      else
             call errlog('channel_id not supported')
             call prg_abort
      endif
      return
      end

c**********************************************************************
      subroutine EKHARA_INIT_UB
      implicit none
      include 'common.ekhara.inc.for'
      ! Evaluate the Upper Bound for the integrand

      if ((channel_id .eq. 1)) then
             ! 2pi mode -> do nothing.
      elseif (channel_id .eq. 2) then 
             ! 1pi mode -> 
             !---------------------------------------------------------------
             ! First of all, we need to evaluate the Upper Bound of an integrand
             ! This routine is also used for production of the WEIGHTED events

             call evalUpperBound_1pi(ngUpperBound)
             ! as far as <subroutine evalUpperBound_1pi> does the mc integration
             ! itself, we have to account for its contribution into <totIterations>
             totIterations = totIterations + ngUpperBound
      elseif ((channel_id .eq. 3) .or. (channel_id .eq. 4)) then 
             ! 1eta and 1etaP mode -> 
             !---------------------------------------------------------------
             ! First of all, we need to evaluate the Upper Bound of an integrand
             ! This routine is also used for production of the WEIGHTED events

             call evalUpperBound_1pi(ngUpperBound)
             ! as far as <subroutine evalUpperBound_1pi> does the mc integration
             ! itself, we have to account for its contribution into <totIterations>
             totIterations = totIterations + ngUpperBound
      endif
      return
      end


c**********************************************************************
      subroutine EKHARA_RUN
      implicit none
      include 'common.ekhara.inc.for'
      real*16 norm_cs,norm_n    ! for histogram normalization.

      if ((channel_id .eq. 1)) then
             ! execute the MC loop
             call mc_loop_2pi
      elseif (channel_id .eq. 2) then 
             ! execute the MC loop
             call mc_loop_1pi
      elseif (channel_id .eq. 3) then 
             ! execute the MC loop
             call mc_loop_1pi
      elseif (channel_id .eq. 4) then 
             ! execute the MC loop
             call mc_loop_1pi
      endif
      return
      end


c**********************************************************************
      subroutine EKHARA_FIN
      implicit none
      include 'common.ekhara.inc.for'
      CHARACTER msg*78         ! for logging
      integer st(0:104)         ! random seed
      real*16 norm_cs,norm_n    ! for histogram normalization.

      if ((channel_id .eq. 1)) then
             ! finialization of histograming tool
             call runlog('Finialization of histograms')
             call hist_fin_2pi
      elseif (channel_id .eq. 2) then 
             ! finalization of MC
             call mc_fin_1pi(norm_cs,norm_n)

             ! finialization of histograming tool
             call runlog('Finialization of histograms')
             call hist_fin_1pi(norm_n,norm_cs)
      elseif (channel_id .eq. 3) then 
             ! finalization of MC
             call mc_fin_1pi(norm_cs,norm_n)

             ! finialization of histograming tool
             call runlog('Finialization of histograms')
             call hist_fin_1pi(norm_n,norm_cs)
      elseif (channel_id .eq. 4) then 
             ! finalization of MC
             call mc_fin_1pi(norm_cs,norm_n)

             ! finialization of histograming tool
             call runlog('Finialization of histograms')
             call hist_fin_1pi(norm_n,norm_cs)
      endif
      !
      call runlog('[ MC done ]')

      ! saving seed (RANLUX)
      if (SeedMode.eq.2) then    ! Use constant seed =1, use variable (consequently) seed =2
         call rlxdgetf(st) 
         call runlog('EKHARA: Saving random seed to <seed-v.dat>')
         open (9,file='seed-v.dat',status='UNKNOWN')
         write(9,*)st
         close(9)
      endif

      call runlog('Ekhara finished.')
      call std_out('Ekhara finished.')
      call clfiles    ! close all output files

      return
      end

c**********************************************************************
! one-event mode (e.g., for integration into another big MC)
      subroutine EKHARA_SET_ONE_EVENT_MODE
      implicit none
      include 'common.ekhara.inc.for'
      nges = 1.Q0
      return
      end

c**********************************************************************
! re-adjust beam
      subroutine EKHARA_SET_S(sz)
      implicit none
      include 'common.ekhara.inc.for'
      real*8 sz
      s  = sz*1.Q0
      ss = qsqrt(sz*1.Q0)
      call EKHARA_INIT_set
      return
      end

c**********************************************************************
! suppress writing to stdout and histo
      subroutine EKHARA_SET_SILENT 
      implicit none
      include 'common.ekhara.inc.for'
      WriteEvents = 0 ! do not to write to the file
      NeedHisto  = 0  ! ignore histogramming  
      DoHisto = NeedHisto   ! <- option for "1pi" mode
      sw_silent = 1   ! suppress output to stdout
      return
      end

c**********************************************************************
      subroutine readparams_2pi
      implicit none
      include 'common.ekhara.inc.for'

      !---- BEGIN READING THE DATACARD

      ! load the parameters and settings for 2pi mode
      if (channel_id .eq. 1) then
          call read_card_2pi
      endif
      !---- END READING THE DATACARD

      call runlog('subroutine readparams_2pi: done')
      return
      end

c**********************************************************************
      subroutine readparams_1pi
      implicit none
      include 'common.ekhara.inc.for'

      read(7,*)
      read(7,*)
      read(7,*)
      read(7,*) ngUpperBound ! number of iterations to find upper bound
      read(7,*) DoSymmetrize ! symmetrize the PhSp generation wrt (e+ <-> e-)
      read(7,*) PhSpOnly     ! (0)=normal, (1)=calculate the phase space volume only
      read(7,*) ignoreUB     ! handle UB underestim <0> = halt, <positive> = use new UB if overshoot, <negative> = ignore 
      read(7,*) UBEnlargeFactor ! Enlarge factor for upper bound
      read(7,*) 
      read(7,*) CUT_t1max
      read(7,*) CUT_t1min
      read(7,*) CUT_t2max
      read(7,*) CUT_t2min
      read(7,*) 
      read(7,*) Fpi          ! pion decay constant (in the chiral limit)
      read(7,*) 
      read(7,*) 


      !---- BEGIN READING THE DATACARD
      if (channel_id .eq. 2) then ! pi0
          call read_card_1pi
      endif
      if (channel_id .eq. 3) then ! eta
          call read_card_1pi
      endif
      if (channel_id .eq. 4) then ! eta'
          call read_card_1pi
      endif
      !---- END READING THE DATACARD

      ! which routine is used for phase space.
      WorkingGeneratorId = sw_1pi

      change_id_s = 4 ! option for the change of variables in s-channel
                      ! mapping <2> flat, <3> log^3,   <4> log (most efficient!)

      change_id_g = 3 ! option for the change of variables in t-channel
                      ! mapping <2> flat, <3> t1 t2
      call runlog('subroutine readparams_1pi: done')

      return
      end

c**********************************************************************
      subroutine input
      implicit none
      include 'common.ekhara.inc.for'

      open(7,file='input.dat',status='old')
      read(7,*)
      read(7,*)
      read(7,*)
      read(7,*) nges       ! number of generated events
      read(7,*) channel_id ! general channel selection 
                           !(<1> = 2 pion, <2> = 1 pion, <3> = 1 eta, <4> = 1 eta', ...)
      read(7,*) WriteEvents ! 1 to write events, other - not to write to the file
      read(7,*) NeedHisto   ! 1 to write histograms, 0 - to ignore histogramming  
                            ! ^- option for "2pi" mode

c~       sw_silent = 0   ! default value. 0=write to stdout,
c~                       !                1=suppress output

      if (sw_silent .eq. 1) then
            NeedHisto = 0
            WriteEvents = 0
      endif
      DoHisto = NeedHisto   ! <- option for "1pi" mode

      read(7,*) SeedMode    ! Use constant seed =1, 
                            ! use variable (consequently) seed =2

      read(7,*)
      read(7,*)

      call readparams_1pi

      ! load the parameters and settings for 2pi mode
      call readparams_2pi

      ! load General parameters (all modes)
      read(7,*)
      read(7,*)
      read(7,*)
      read(7,*) alpha      ! 1/alpha (QED)
      alpha = 1.Q0/alpha   !

      read(7,*) me         ! Electron mass
      read(7,*) mpi        ! Charged pion mass

      read(7,*) mpi0       ! Neutral pion mass
      read(7,*) meta       ! Eta meson mass
      read(7,*) metaP      ! Eta' meson mass

c-----------------------------------------------------------------------
c     RxT parameters for the form factor  ( e+e- ->  e+e- pi,eta,eta' )

      Fchir = Fpi             ![GeV]
      Fk    = 1.22Q0 * Fchir  ![GeV]
      F8 = 1.26Q0 * Fchir     ![GeV]
      F0 = 1.17Q0 * Fchir     ![GeV]

      Mrho_c = 0.7755Q0          ![GeV]
      Momega_c = 0.78265Q0       ![GeV]
      Mphi_c = 1.019456Q0        ![GeV]

      Mrho_pr   = 1.465Q0 ![GeV]
      Momega_pr = 1.42Q0  ![GeV]
      Mphi_pr   = 1.680Q0 ![GeV]
 
c      Mko  = 0.49765Q0      ![GeV]
      Mk   = 0.49368Q0      ![GeV]

      GammaTotalrho_c = 0.1494Q0
      GammaTotalomega_c = 0.00849Q0
      GammaTotalphi_c = 0.00426Q0

      GammaTotalrho_pr = 0.400Q0
      GammaTotalomega_pr = 0.215Q0
      GammaTotalphi_pr = 0.150Q0

      pi = 4.Q0*qatan(1.Q0)
      theta0 = -9.2Q0/180.0Q0*pi
      theta8 = -21.2Q0/180.0Q0*pi

       Cq_c = Fchir/(sqrt(3.Q0)*cos(theta8 - theta0)) *
     &     (cos(theta0)/F8 - sqrt(2.Q0)*sin(theta8)/F0) 
       Cs_c = Fchir/(sqrt(3.Q0)*cos(theta8 - theta0)) *
     &     (sqrt(2.Q0)*cos(theta0)/F8 + sin(theta8)/F0) 

       Cq_P = Fchir/(sqrt(3.Q0)*cos(theta8 - theta0)) *
     &     (sqrt(2.Q0)*cos(theta0)/F8 + sin(theta8)/F0) 
       Cs_P = Fchir/(sqrt(3.Q0)*cos(theta8 - theta0)) *
     &     (cos(theta0)/F8 - sqrt(2.Q0)*sin(theta8)/F0) 

      EPSomph = 0.058Q0           !       0.058 = Our EPJ C49 p.699
      EPSprime = -0.0026Q0        ! phi-omega-pi0, 0.0026 = see drafts

c-----1 octet scheme--------
      Fv = 7.04Q-6*12.Q0*pi*Mrho_c
     &     /(16.Q0*pi**2/137.0359895Q0/137.0359895Q0)
      Fv = sqrt(Fv)

      Fsmv = Fv/Mrho_c
      Gv = Fv/2.Q0 ! HIGH ENERGY CONSTRAINT

      Hv = 3.Q0/32.Q0/sqrt(2.Q0)/pi/pi/Fsmv   !*1.03Q0 ??? ! HIGH ENERGY CONSTRAINT

      SIGv = 3.Q0/32.Q0/pi/pi/Fsmv**2 ! HIGH ENERGY CONSTRAINT
      
c      write(*,*) 'Fsmv = ',Fsmv
c      write(*,*) 'Hv = ',Hv
c      write(*,*) 'SIGv = ',SIGv
c      stop

c-----2 octets scheme--------
      Fsmv1 = Fsmv       ! from width

      if (channel_id .ne. 1) then
        read(7,*)
        read(7,*)
        read(7,*)
        read(7,*)
        read(7,*)
        read(7,*) Hv1      ! from fit
      endif

      SIGv1 = sqrt(2.Q0)*Hv1/Fsmv1 ! HIGH ENERGY CONSTRAINT

      Fsmv2Hv2 = (3.Q0/4.Q0/pi/pi -8.Q0*sqrt(2.Q0)*Hv1*Fsmv1)
     &          /8.Q0/sqrt(2.Q0)          ! HIGH ENERGY CONSTRAINT

      Fsmv2sqSIGv2 = sqrt(2.Q0)*Fsmv2Hv2  ! HIGH ENERGY CONSTRAINT
c-----------------------------------------------------------------------

      close(7)
      call runlog('subroutine input: options read from <input.dat>')

      !-----constants---------------------------------------------
      me2 = me**2
      mpi2 = mpi**2
      mpi02 = mpi0**2

c s.i. 07.12.2010: now masses moved to input.dat
c      meta  = 0.57853Q0     !GeV ! Eta meson mass    ! PDG 08
c      metaP = 0.95766Q0     !GeV ! Eta' meson mass   ! PDG 08
c -

      meta2 = meta**2
      metaP2 = metaP**2

      gev2nbarn = .389379292Q6
      !-----
      t_taged = 0.Q0

      return
      end


c**********************************************************************
      subroutine opfiles
      implicit none
      !-----------------------------------------------------
      open(66,file='output/runflow.log',status='UNKNOWN')
      open(67,file='output/warnings.log',status='UNKNOWN')
      open(68,file='output/errors.log',status='UNKNOWN')
      !-----------------------------------------------------
      return
      end

c**********************************************************************
      subroutine opfiles_events
      implicit none
      include 'common.ekhara.inc.for'
      logical ex                ! to check the existence of a file
      if (WriteEvents.eq.0) then
       call runlog('Generated events will NOT be written')
      else
       inquire(file="output/events.out", exist=ex)
       if (ex) then
          call warnlog('File <events.out> already exists.')
          open(8,file='output/events-2.out',status='UNKNOWN')
          call runlog('Generated events ->  <events-2.out>')
          call warnlog('Generated events ->  <events-2.out>')
          EvtsFile = 2
       else
          open(8,file='output/events.out',status='UNKNOWN')
          call runlog('Generated events ->  <events.out>')
          EvtsFile = 1
       endif
      endif

      return
      end


c**********************************************************************
      subroutine clfiles
      implicit none
      include 'common.ekhara.inc.for'
      !-----------------------------------------------------
      if (WriteEvents.ne.0) then
         close(8)
      endif
      !-----------------------------------------------------
      close(66) ! usual      log file
      close(67) ! warning    log file
      close(68) ! errors     log file

      return
      end

c**********************************************************************
! writing "0" into stdout at every 10% completed
! "progress bar"

      subroutine animate(n,ntot)
      implicit none
      include 'common.ekhara.inc.for'
      CHARACTER msg*78         ! for logging
      real*16 n,ntot
      if (sw_silent .eq. 1) then
          return
      endif

      if ( FLOOR(10.Q0*n/ntot) .eq. CEILING(10.Q0*n/ntot) ) then
c            write(*,'(I2,\)') 0
            write(msg,*) (100 - 10*CEILING(10.Q0*n/ntot)), 
     &                   '%  events collected..' 

            call std_out(msg)
            call runlog(msg)
      endif
      return
      end

c-----------------------------------------------------------------------
      subroutine runlog(msg)
      implicit none
      CHARACTER msg*(*)
      !write(66,*) msg
!      write(*,*) msg
      return
      end

c-----------------------------------------------------------------------
      subroutine warnlog(msg)
      implicit none
      CHARACTER msg*(*)
      !write(67,*) msg
      return
      end

c-----------------------------------------------------------------------
      subroutine errlog(msg)
      implicit none
      CHARACTER msg*(*)
      !write(68,*) msg
!      write(*,*) msg
      return
      end

c-----------------------------------------------------------------------
      subroutine std_out(msg)
      implicit none
      include 'common.ekhara.inc.for'
      CHARACTER msg*(*)
      if (sw_silent .eq. 1) then
          return
      endif

      write(*,*) msg
      return
      end

c-----------------------------------------------------------------------
      subroutine prg_abort
      implicit none
      call runlog('Aborted.')
      call warnlog('Aborted.')
      call errlog('Aborted.')
      call std_out('Aborted.')
      call prg_exit
      return
      end

c-----------------------------------------------------------------------
      subroutine prg_exit
      implicit none
      call clfiles    ! close all output files
      stop
      end

c-----------------------------------------------------------------------
      subroutine tell_efficiency(evts, eff_iter)
      implicit none
      CHARACTER msg*78         ! for logging
      real*16 evts, eff_iter

      write(msg,550) ' MC efficiency (evt/eff.iter)  = ',evts/eff_iter
      call runlog(msg)
      call std_out(msg)
      return
 550  format(A,E13.3,A,E13.3)
      end

c-----------------------------------------------------------------------
      subroutine tell_integral(title)
      implicit none
      include 'common.ekhara.inc.for'
      CHARACTER title*(*) 
      CHARACTER msg*78         ! for logging

      write(msg,601)

      call std_out('================================')
      call  runlog('================================')
      call std_out(title)
      call  runlog(title)

      if (PhSpOnly .eq. 0) then
            msg = ' '
            write(msg,601)' Integral Cross Section = ', CS, 
     &                  ' +- ', CSerr, ' [GeV^-2]'
            call runlog(msg)
            call std_out(msg)
            msg = ' '
            write(msg,601)'                        = ', 
     &                  gev2nbarn * CS, 
     &                  ' +- ', gev2nbarn * CSerr, ' [nb]'
            call runlog(msg)
            call std_out(msg)
      else
            msg = ' '
            write(msg,601)' Phase space volume     = ', CS, 
     &                  ' +- ', CSerr, ' [GeV^2]'
            call runlog(msg)
            call std_out(msg)
      endif
      call std_out('================================')
      call  runlog('================================')
      return

  601 format(A,1pd12.5,A,1pd9.2,A,1pd9.2,A)
      end


c-----------------------------------------------------------------------
c
c     Include files with supplementary routines:
c
      ! User's functions:
      !   -- reporting of events
      !   -- pi-gamma-gamma form factors
      !   -- additional cuts
      include 'routines-user.inc.for'
c-----------------------------------------------------------------------

      ! mathematical routines -- matrix manipulations,
      ! Lorentz boosts etc.
      include 'routines-math.inc.for'
      include 'routines-helicity-aux.inc.for'

      ! routines for two-pion mode
      include 'routines_2pi.inc.for'
      include 'routines-histograms_2pi.inc.for'

      ! routines for one-pion mode
      include 'routines_1pi.inc.for'
      include 'routines-histograms_1pi.inc.for'

      ! routines to interface with BOSS framework
      include 'routines-boss.inc.for'

c-----------------------------------------------------------------------
c     8>< ---          End of file 'ekhara.for'           --- ><8
c-----------------------------------------------------------------------


