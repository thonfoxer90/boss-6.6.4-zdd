c----------------------------------------------------------------------c
c                                                                      c
c     E K H A R A       M o n t e  C a r l o    G e n e r a t o r      c
c                                                                      c
c                 H O W - T O       E X A M P L E                      c
c                                                                      c
c                  http://uranos.cto.us.edu.pl/~ekhara/                c
c                  Comput.Phys.Commun. 182 (2011) 1338-1349            c
c                                                                      c
c     (c) 2010-2012 Henryk Czyz, Sergiy Ivashyn                        c
c----------------------------------------------------------------------c

      program CALLEXAMPLE
      implicit none
      external EKHARA
      !----------------------------------------------------------------
      ! pre-initialize (optional)
       call EKHARA_SET_SILENT    ! suppress writing to stdout and histo
      !----------------------------------------------------------------


      !----------------------------------------------------------------
      ! initialize (obligatory)
       call EKHARA(-1)

      ! do other pre-run re-adjustments, if you need
      ! (optional)
       call EKHARA_SET_ONE_EVENT_MODE ! the effect of this
                                      ! is that EKHARA(0) will give only one event,
                                      ! otherwise, the number of generated events
                                      ! is driven by input file
      !----------------------------------------------------------------


      !----------------------------------------------------------------
      ! If you want to change the total energy after initialization, you should use:
      ! call EKHARA_SET_S(1.0404D0)    ! this re-adjusts the total energy squared [GeV^2]
                                       ! and re-initializes the event generator parameters

      ! generate event  (obligatory)
       call EKHARA(0)
      !----------------------------------------------------------------


      !----------------------------------------------------------------
      ! access the generated event
       call ReadEventExample 
      !----------------------------------------------------------------


      !----------------------------------------------------------------
      ! finalize  (obligatory)
      call EKHARA(1)
      !----------------------------------------------------------------
      stop
      end


c**********************************************************************
      subroutine ReadEventExample
      implicit none

c----------------------------------------------------------------------c
c Important: please do not change the values of
c            the variables in these COMMON blocks
c
      common /inoutmomenta/tau,p1,p2,q1,q2
      real*16 tau(0:3),p1(0:3),p2(0:3),q1(0:3),q2(0:3)
c  4-momenta:
c     p1 --- initial positron
c     p2 --- initial electron
c     q1 --- final positron
c     q2 --- final electron

      common /PhaseSpaceConfiguration/ 
     &       qpion, qpionabs,
     &       q1abs, q2abs, 
     &       l1, l2, k, g1

      real*16 q1abs, q2abs
      real*16 qpionabs ! 3-momentum abs value
      real*16 qpion(0:3) 
      ! qpion(0:3)  is the final pseudoscalar's 4-momentum.
      ! in the code it is supposed that this particle is on-shell
      real*16 k(0:3)
            ! k(0:3) is 4-vector of final electron-positron pair
            !      (i.e. second virtual photon)
      real*16 g1(0:3) 
      real*16 l1(0:3), l2(0:3)
        !  l1 and l2 notation is used to label the virtual-photon momenta
        !            which appear in the t-channel diagram:
        !            l1 = p1 - q1
        !            l2 = p2 - q2

c----------------------------------------------------------------------c
      ! Summary
      !
      ! Important variables: final state 4-momenta in center-of-mass frame
      ! pi0        e+      e-
      ! qpion(0:3),q1(0:3),q2(0:3)
      !       0   - energy
      !       1,2,- x,y components of 3-momenta
      !       3   - z-component. z is along the initial e+
c----------------------------------------------------------------------c

      return
      end

c-----------------------------------------------------------------------
c     8>< ---          End of file                          --- ><8
c-----------------------------------------------------------------------
