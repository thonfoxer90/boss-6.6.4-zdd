c-----------------------------------------------------------------------
c     EKHARA. include file.     e+e- -> e+e-Pi0
c
c     Here:      - Main MC loop
c                - basic phase space cuts
c                - some auxiliary routines
c                - Matrix element
c                - Ph.Sp. generators
c-----------------------------------------------------------------------

c**********************************************************************
! MC initialization for   e+e- -> e+e-pi0/e+e-eta
!
      subroutine mc_init_1pi
      implicit none
      include 'common.ekhara.inc.for'
      CHARACTER msg*78         ! for logging
      IntegralOS  = 0.Q0
      Integral2OS = 0.Q0
      OSCounter   = 0.Q0

      Integralcontrib  = 0.Q0
      Integralcontrib2 = 0.Q0

      UpperBound    = 0.Q0
      totIterations = 0.Q0
      MCTrialsCount = 0.Q0

      ContribMax_MCloop = 0.Q0

      UnstableCounter = 0
      !---------------------------------------------------------------
      ! FluxFactor  will be needed further to get the final integral value.
      if (PhSpOnly .eq. 0) then
            FluxFactor = 0.5Q0/s 
      else
            FluxFactor = 1.Q0
      endif

      return
      end

c**********************************************************************
! Main MC loop for   e+e- -> e+e-pi0/e+e-eta
!
      subroutine mc_loop_1pi
      implicit none
      include 'common.ekhara.inc.for'
      logical accepted
      logical acceptevent ! function
      real*8 rrc(0:6)  ! random numbers
      real*16 loopiterator
      CHARACTER msg*78         ! for logging
      !---------------------------------------------------------------
      loopiterator  = nges

      call runlog(' ')
      call std_out(' ')

      msg = ' '
      write(msg,149)'Wait for [',nges,' ] events to be accepted..'
      call std_out(msg)

      msg = ' '
      write(msg,149)'Events to be accepted=',nges
      call runlog(msg)

c ======================================================================
c ----------------------------------------------------------------------
c ----                        MC loop                               ----

      do while (loopiterator .gt. 0.Q0)
            ! create random variables (RANLUX)
            call ranlxdf(rrc, 7)

            ! generate the point in the phase space
            OutFlag = .false.
            call phasespace_1pi(rrc)
            ! if for some reason the momenta are out of the phase space,
            ! then OutFlag is set to .true.

            ! We will just set the weights of the wrong events (if any) to 0.
            if (OutFlag) then
                Msq  =  0.Q0
            else
                ! only if the event is within the phase space...

                ! apply kinematic cuts
                call eventselection_1pi(accepted)
                if (accepted) then
                    ! calculate the value of the matrix element,
                    ! if the cuts are satisfied
                    call m_el_1pi(Msq)
                else
                    Msq  =  0.Q0
                endif
            endif

            if(Msq.lt.0.Q0) then
               ! We always abort, if the |M|^2 is less then zero
               call errlog('subroutine mc_loop_1pi: [ Msq < 0 ]')
               msg = ' '
               write(msg,*) ' Msq = ',Msq
               call errlog(msg)
               call std_out('subroutine mc_loop_1pi: [ Msq < 0 ]')
               call runlog('[ Msq < 0 ]')
               call prg_abort
               return
            else
               ! Multiply |M|^2 by necessary factors, to obtain
               ! the contribution to MC integral
               contrib = Msq * PhaseVol * JacobianFactor * Dab

               ! report the weight of the event, if necessary..
               call reportcontrib

               ! calculate the actaul maximum during the MC loop
               if (contrib .gt. ContribMax_MCloop) then
                      ContribMax_MCloop = contrib
               endif

               if (contrib .gt. UpperBound) then
                     ! OVERSHOOTING
                     call std_out('UpperBound underestimated')
                     call runlog('UpperBound underestimated')
                     msg = ' '
                     write(msg,149)' Obtained ', contrib,
     &                             ' while UpperBound = ', UpperBound
                     call runlog(msg)
                     call tell_efficiency((nges-loopiterator),
     &                                    MCTrialsCount)

                     UnstableCounter = UnstableCounter + 1

                     if (ignoreUB .gt. 0.Q0) then
      ! ---------------------------------------------------------------
      ! - RETRY if UB underestimated
                              call runlog('ignoreUB flag is set on')

                              ! tell which point of PhSp caused the overshoot
                              call ReportOvershootEvent

                              call runlog('Trying once more... ')
                           call runlog('Using new UpperBound estimate')
                              call std_out('Trying once more... ')
                           call std_out('Using new UpperBound estimate')

                             ! All MC generation has to be cleaned
                             loopiterator=nges
                             totIterations=totIterations+MCTrialsCount
                             MCTrialsCount = 0.Q0
                             call hist_FLUSH_1pi
                             call FLUSHreportevent_1pi

                             ! and we take a new Upper Bound 
                             ! including the _same_ enlarge factor
                             UpperBound = contrib * UBEnlargeFactor

                             msg = ' '
                             write(msg,149)'New UpperBound: ',UpperBound
                             call runlog(msg)
                             msg = ' '
                             write(msg,149)'(incl.) UBEnlargeFactor : ',
     &                                     UBEnlargeFactor
                             call runlog(msg)

                             call runlog(' ')
                             msg = ' '
                             write(msg,149)
     &                              'Events to be accepted =',nges
     &                              , '. Please wait...' 
                             call runlog(msg)
                             call std_out(msg)

                     elseif (ignoreUB .lt. 0.Q0) then
      ! ---------------------------------------------------------------
      ! - IGNORE if UB underestimated
                              call runlog(
     &         ' ignoreUB flag is set to negative <params_1pi.dat>')

                              ! tell which point of PhSp caused the overshoot
                              call ReportOvershootEvent

                              ! count the contribution of overshooted events to cs.
                              IntegralOS  = IntegralOS  + contrib
                              Integral2OS = Integral2OS + contrib**2
                              OSCounter   = OSCounter + 1.Q0

                              ! ignore overshooting
                              contrib = 0.Q0
                              call runlog('  Ignoring overshoot. ')
                              call std_out('  Ignoring overshoot. ')
                     else
      ! ---------------------------------------------------------------
      ! - ABORT if UB underestimated
                              call errlog('UpperBound underestimated')
                              call prg_abort
                              return
                     endif
      ! ---------------------------------------------------------------
                  else 
                      ! Here (contrib < or = UpperBound), i.e. correct.

                      ! Integrate it
                      Integralcontrib = Integralcontrib + contrib
                      Integralcontrib2 = Integralcontrib2 + contrib**2
                      MCTrialsCount = MCTrialsCount + 1.Q0

                      ! Do accept-reject
                      if (acceptevent(contrib,rrc(0),UpperBound)) then
                            ! add accepted event to histograms
                            call histo_event_1pi ! UNWEIGHTED histogramming

                            call reportevent_1pi

                            ! count accepted event
                            loopiterator = loopiterator - 1.Q0

                            ! update progress bar in the screen
                            call animate(loopiterator,nges)
                      endif
                  endif
            endif

            if (UnstableCounter .ge. 10) then 
                  call std_out('10 numerical instabilities occured.')
                  call std_out('Program will be aborted.')
                  call std_out('Please, send a report to the authors!')

                  call prg_abort
            endif
      enddo
c ----                      end of MC loop                          ----
c ----------------------------------------------------------------------
c ======================================================================
 149  format(A,E13.3,A,E13.3)
      return
      end

c**********************************************************************
! MC finalization for   e+e- -> e+e-pi0/e+e-eta
!
      subroutine mc_fin_1pi(norm_cs,norm_n)
      implicit none
      include 'common.ekhara.inc.for'
      CHARACTER msg*78         ! for logging
      real*16 norm_cs,norm_n

      totIterations = totIterations + MCTrialsCount
      call runlog(' ')
      call runlog('MC loop complete.')

      call runlog(' ')
      msg = ' '
      write(msg,150)'Total number of iterations = ',(totIterations)
      call runlog(msg)
      msg = ' '
      write(msg,150)'Number of MC tials = ',(MCTrialsCount)
      call runlog(msg)
      msg = ' '
      write(msg,150)' ContribMax_MCloop  = ', ContribMax_MCloop
      call runlog(msg)

      call std_out(' ')
      call runlog(' ')

      call tell_efficiency(nges, MCTrialsCount)

      call std_out(' ')
      call runlog(' ')

      !----------------------------------------------------------------
      ! INTEGRAL from the weights collected in our MC loop 
      ! and upperbound estimation loop.
      Integralcontrib =  FluxFactor    * Integralcontrib
      Integralcontrib2 = FluxFactor**2 * Integralcontrib2 

      CS  = Integralcontrib/totIterations
      CSerr = qsqrt( qabs(Integralcontrib2/totIterations-CS**2)
     &             /(totIterations-1.Q0) )

      call tell_integral('From all weights')

      ! below values will be used for the histogram normalization
!       norm_cs = FluxFactor*UpperBound
!       norm_n = MCTrialsCount
      norm_cs = CS
      norm_n = nges

      !----------------------------------------------------------------
      ! INTEGRAL 
      ! from the unweighted events, accepted in our MC loop.
      if (MCTrialsCount .le. 0.Q0) then
            CS = 0.Q0
            CSerr = 0.Q0
      else
            IntegralcontribSAMPLE = UpperBound *  FluxFactor 
            CS    = IntegralcontribSAMPLE
     &         * (nges) / MCTrialsCount

            CSerr = sqrt( CS*(IntegralcontribSAMPLE - CS) )
     &              / sqrt( MCTrialsCount )
      endif

      call tell_integral('From UNWEIGHTED events')

      !----------------------------------------------------------------
      ! report the contribution of the overshooted events to cross section
      ! (only if there were overshootings)
      if (OSCounter .gt. 0.Q0) then
         call runlog(' ')
         call runlog('There were overshootings.')
         call runlog('The events, corresponding to them were ignored.')
         call runlog(' ')
         call runlog('The unweighted sample may be not reliable!')
         call runlog(' see also warnings.log')
         IntegralOS  =    FluxFactor * Integralcontrib 
         Integral2OS = FluxFactor**2 * Integralcontrib2 
         CS_OS    = IntegralOS/OSCounter
         CSerr_OS = qsqrt( qabs(Integral2OS/OSCounter
     &              -CS_OS**2)
     &             /(OSCounter) )
         call tell_integral('Contribution from overshooted events')
      endif


 150  format(A,E13.3,A,E13.3)
      return
      end

c**********************************************************************
      ! Evaluation of the Upper Bound.
      ! This routine is also used for production of the WEIGHTED events

      subroutine evalUpperBound_1pi(UBiter)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 UBiter
      logical accepted
      real*16 loopiterator

      real*8 rrc(0:6)
      integer idum

      CHARACTER msg*78         ! for logging
      msg = ' '
      write(msg,202)
     & 'Estimating UpperBound with [', UBiter, ' ] iterations...'
      call runlog(msg)
      call std_out(msg)

      loopiterator=UBiter

c ======================================================================
c ----------------------------------------------------------------------
c ----                     UpperBound loop                          ----
      do while (loopiterator .gt.  0.Q0) 
            loopiterator=loopiterator - 1.Q0
            OutFlag = .false.
            call ranlxdf(rrc, 7)         ! creating random variables (RANLUX)
            call phasespace_1pi(rrc)
            ! if for some reason the momenta are out of the phase space,
            ! then OutFlag is set to .true.

            if (OutFlag) then
                Msq  =  0.Q0
            else
                ! only if the event is within the phase space...

                ! apply kinematic cuts
                call eventselection_1pi(accepted)  
                if (accepted) then
                    ! calculate the value of the matrix element,
                    ! if the cuts are satisfied
                    call m_el_1pi(Msq)
                    if (Msq .lt. 0.Q0 ) then
                        call errlog('Negative matrix element squared')
                        call prg_abort
                    endif
                else
                    Msq  =  0.Q0
                endif
            endif

            call animate(loopiterator,ngUpperBound)
            contrib = Msq * PhaseVol * JacobianFactor * Dab
            call reportcontrib

            Integralcontrib = Integralcontrib + contrib
            Integralcontrib2 = Integralcontrib2 + contrib**2

            if (contrib .gt. UpperBound) then
                  UpperBound = contrib
            endif
      enddo
c ----                   end of UpperBound loop                     ----
c ----------------------------------------------------------------------
c ======================================================================

      UpperBound = UpperBound * UBEnlargeFactor

      msg = ' '
      write(msg,202)'UpperBound estimated    : ',UpperBound
      call runlog(msg)
      msg = ' '
      write(msg,202)'(incl.) UBEnlargeFactor : ',UBEnlargeFactor
      call runlog(msg)

 202  format(A,E13.3,A,E13.3)
      return
      end


c**********************************************************************
c     Hit-or-miss MC rejection, in order to generate UNWEIGHTED events
c
      logical function acceptevent(contrib, randomvar, UpperBound)
      ! this function implements Monte Carlo rejection
      implicit none
      real*16 contrib, UpperBound
      real*8 randomvar
      acceptevent = (contrib .ge. (randomvar * UpperBound))
      ! It is important to accept events ONLY IF calculated MC contribution
      ! is GREATER or equal to the UpperBound (estimated earlier) TIMES the RANDOM number
      !     (!! independent random number !!)
      return
      end
c**********************************************************************




c-----------------------------------------------------------------------
c     8>< ---          Basic phase space cuts.              --- ><8
c-----------------------------------------------------------------------

      subroutine eventselection_1pi(accepted) 
      !                   here the cuts due to input file are done
      implicit none
      include 'common.ekhara.inc.for'

      logical accepted
      logical accepted_extra

      real*16 cthpion, sthpion, cphipion, sphipion ! pion
      real*16 cphi1, sphi1, cth1, sth1             ! final positron
      real*16 cphi2, sphi2, cth2, sth2             ! final elektron
      real*16 deg

      deg = 180.Q0/pi

      ! electron
      cth2 = q2(3)/q2abs 
      sth2 = qsqrt(1.Q0 - cth2**2)
      cphi2 = q2(2)/(q2abs * sth2)
      sphi2 = q2(1)/(q2abs * sth2)

      ! positron
      cth1 = q1(3)/q1abs
      sth1 = qsqrt(1.Q0 - cth1**2)
      cphi1 = q1(2)/(q1abs * sth1)
      sphi1 = q1(1)/(q1abs * sth1)

      ! pion
      cthpion = qpion(3)/qpionabs
      sthpion = qsqrt(1.Q0 - cthpion**2)
      cphipion = qpion(2)/(qpionabs * sthpion)
      sphipion = qpion(1)/(qpionabs * sthpion)

      accepted = .true.

      ! ----------------- do basic cuts
      ! cut pion angles
      accepted = ( (accepted) .and.
     &    ((acos(cthpion) * deg) .le. CUT_thpionmax ) .and. 
     &    ((acos(cthpion) * deg) .ge. CUT_thpionmin )
     &                  )

      ! cut positron angles
      accepted = ( (accepted)  .and.
     &    ((acos(cth1) * deg) .le. CUT_th1max ) .and. 
     &    ((acos(cth1) * deg) .ge. CUT_th1min )
     &                  )

      ! cut electron angles
      accepted = ( (accepted)  .and.
     &    ((acos(cth2) * deg) .le. CUT_th2max ) .and. 
     &    ((acos(cth2) * deg) .ge. CUT_th2min )
     &                  )

      ! cut pion energy
      accepted = ( (accepted) .and.
     &    (qpion(0) .le. CUT_Epionmax ) .and. 
     &    (qpion(0) .ge. CUT_Epionmin )
     &                  )

      ! cut positron energy
      accepted = ( (accepted) .and.
     &    (q1(0) .le. CUT_E1max ) .and. 
     &    (q1(0) .ge. CUT_E1min )
     &                  )

      ! cut electron energy
      accepted = ( (accepted) .and.
     &    (q2(0) .le. CUT_E2max ) .and. 
     &    (q2(0) .ge. CUT_E2min )
     &                  )

      ! ----------------- do more cuts
      if (accepted) then
       call ExtraCuts_1pi(accepted_extra)  ! => see routines-user.inc.for
       accepted = (accepted .and. accepted_extra)
      endif

      return
      end
c**********************************************************************




c-----------------------------------------------------------------------
c     8>< ---          Some auxiliary routines              --- ><8
c-----------------------------------------------------------------------

      subroutine init_1pi
      implicit none
      include 'common.ekhara.inc.for'

      ! pi0 gamma gamma coupling SQUARED
      pggCoupling = (4.Q0 * alpha**2 / Fpi)**2

      W  = mpi0
      W2 = w**2

      return
      end

c**********************************************************************
      subroutine init_1eta
      implicit none
      include 'common.ekhara.inc.for'

      ! Feta = 0.0975Q0  ![GeV]! eprint:0912.5278 How the recent BABAR data..

      Feta = 0.09448Q0 ![GeV]! recalculated from Phys.Rev. D85 (2012) 094010

      ! eta gamma gamma coupling SQUARED
      pggCoupling = (4.Q0 * alpha**2 / Feta)**2

      w  = meta
      W2 = w**2
      return
      end

c**********************************************************************
      subroutine init_1etaP
      implicit none
      include 'common.ekhara.inc.for'

      ! FetaP = 0.0744Q0 ![GeV]! eprint:0912.5278 How the recent BABAR data..

      FetaP = 0.07363Q0 ![GeV]! recalculated from Phys.Rev. D85 (2012) 094010

      ! eta gamma gamma coupling SQUARED
      pggCoupling = (4.Q0 * alpha**2 / FetaP)**2

      w  = metaP
      W2 = w**2
      return
      end

c**********************************************************************
c     Test the consistency between the user-defined cut parameters
c     (simplified edition)
c
      subroutine control_cuts_simple_1pi
      implicit none
      include 'common.ekhara.inc.for'

      if (CUT_E1min .lt. me) then
          CUT_E1min = me
      endif
      if (CUT_E2min .lt. me) then
          CUT_E2min = me
      endif
      if (CUT_E1max .gt. (s-W2-2.Q0*me*W)/(2.Q0*ss)) then
          CUT_E1max = (s-W2-2.Q0*me*W)/(2.Q0*ss)
      endif
      if (CUT_E2max .gt. (s-W2-2.Q0*me*W)/(2.Q0*ss)) then
          CUT_E2max = (s-W2-2.Q0*me*W)/(2.Q0*ss)
      endif

      if (CUT_th1min .lt. 0.Q0) then
          CUT_th1min = 0.Q0
          call warnlog('Cuts corrected. Check input file!')
      endif
      if (CUT_th2min .lt. 0.Q0) then
          CUT_th2min = 0.Q0
          call warnlog('Cuts corrected. Check input file!')
      endif
      if (CUT_th1max .gt. 180.Q0) then
          CUT_th1max = 180.Q0
          call warnlog('Cuts corrected. Check input file!')
      endif
      if (CUT_th2max .gt. 180.Q0) then
          CUT_th2max = 180.Q0
          call warnlog('Cuts corrected. Check input file!')
      endif

      if ( (CUT_E1min .gt. CUT_E1max) .or.
     &     (CUT_E2min .gt. CUT_E2max) .or.
     &     (CUT_Epionmin .gt. CUT_Epionmax) .or.
     &     (CUT_t1min .gt. CUT_t1max) .or.
     &     (CUT_t2min .gt. CUT_t2max) .or.
     &     (CUT_th1min .gt. CUT_th1max) .or.
     &     (CUT_th2min .gt. CUT_th2max) .or.
     &     (CUT_thpionmin .gt. CUT_thpionmax) 
     &    ) then
          call runlog('Wrong cuts')
          call errlog('Wrong cuts')
          call prg_abort
      endif
      return
      end

c**********************************************************************
      ! Tests the numerical momentum conservation and 
      ! whether the final particles are on-shell
      subroutine CheckMomentConserv 
      implicit none
      include 'common.ekhara.inc.for'

      ! values that physically have to be zero
      real*16 pionZ, finalZ, compZ
      real*16 cosbeta
      integer icc

      CHARACTER msg*78         ! for logging

      ! if for some reason the momenta were out of the phase space,
      ! then OutFlag was set to .true.
      !
      ! Obviously, in this case we do not need to check momenta conservation
      if (OutFlag) then
            return
      endif

      icc = 0
      !numerical zero
      compZ = 1.Q-15

      ! pion on-shell
      pionZ=qabs((qpion(0)**2-qpion(1)**2-qpion(2)**2-qpion(3)**2)
     &        - W2)
      if (pionZ  .ge. compZ) then
         call warnlog('Pion momentum is off-shell')
         msg = ' '
         write (msg,220)'|Qpion^2 - m^2| = ',pionZ
         call warnlog(msg)
         icc = icc + 1
       endif

      ! final positron on-shell
      finalZ=qabs((q1(0)**2 - q1(1)**2 - q1(2)**2 - q1(3)**2)
     &        - me2)
      if (finalZ  .ge. compZ) then
         call warnlog('e+ momentum is off-shell')
         msg = ' '
         write (msg,220)'|q1^2 - m^2| = ',finalZ
         call warnlog(msg)
         icc = icc +1
      endif

      ! final electron on-shell
      finalZ=qabs((q2(0)**2 - q2(1)**2 - q2(2)**2 - q2(3)**2)
     &        - me2)
      if (finalZ  .ge. compZ) then
         call warnlog('e- momentum is off-shell')
         msg = ' '
         write (msg,220)'|q2^2 - m^2| = ',finalZ
         call warnlog(msg)
         icc = icc +1
      endif

      ! (ss, 0, 0 ,0 ) = qpion + q1 + q2
      finalZ=qabs( qpion(0) + q1(0) + q2(0) - ss)
      if (finalZ  .ge. compZ) then
         call warnlog('Energy is not conserved')
         msg = ' '
         write (msg,220)'|Epion + E1 + E2 - ss| => ',finalZ
         call warnlog(msg)
         icc = icc +1
      endif

      finalZ=qabs( qpion(1) + q1(1) + q2(1) ) 
     &     + qabs( qpion(2) + q1(2) + q2(2) ) 
     &     + qabs( qpion(3) + q1(3) + q2(3) ) 
      if (finalZ  .ge. compZ) then
         call warnlog('3-momentum is not conserved')
         msg = ' '
         write (msg,220)'Qpion + q1 + q2 => ',finalZ
         call warnlog(msg)
        icc = icc +1
      endif

      if (icc .gt. 0) then

         ! Some fancy stuff in log file, to make reading easier
         call warnlog(' ------------------------------ ')


         call warnlog('Failure of kinematick check!')
         call warnlog('Will be aborted from <CheckMomentConserv>')
         call warnlog('Please, send a report to the authors!')

         call std_out('Failure of kinematick check!')
         call std_out('Will be aborted from <CheckMomentConserv>')
         call std_out('Please, send a report to the authors!')
 
c ------------------------------
          ! option A
          ! call prg_abort
c ------------------------------
          ! option B
c         ! If there is a problem with event,
c         ! ask not to account for it
         OutFlag = .true.
          ! Count as a numerically-unstable event
         UnstableCounter = UnstableCounter + 1
c ------------------------------

      endif

 220  format(A,E12.5)
      return
      end

c**********************************************************************
      subroutine ReportOvershootEvent
      implicit none
      include 'common.ekhara.inc.for'

      CHARACTER msg*78         ! for logging

      call warnlog('-----------------------------------')
      call warnlog('-details of an oversooting event: -')

      msg = ' '
      write (msg,103)'s1 = ', s1, ' [GeV^2]'
      call warnlog(msg)

      msg = ' '
      write (msg,103)'s2 = ', s2, ' [GeV^2]'
      call warnlog(msg)

      msg = ' '
      write (msg,103)'t1 = ', it1, ' [GeV^2]'
      call warnlog(msg)

      msg = ' '
      write (msg,103)'t2 = ', it2, ' [GeV^2]'
      call warnlog(msg)

      msg = ' '
      write (msg,103)'kk = ', kk, ' [GeV^2]'
      call warnlog(msg)

      call warnlog(' ')

      msg = ' '
      write (msg,104)'q1 = (', q1(0) ,',', 
     &      q1(1) ,',',q1(2) ,',',q1(3) ,' ) [GeV]'
      call warnlog(msg)

      msg = ' '
      write (msg,104)'q2 = (', q2(0) ,',', 
     &      q2(1) ,',',q2(2) ,',',q2(3) ,' ) [GeV]'
      call warnlog(msg)

      msg = ' '
      write (msg,104)'qp = (', qpion(0) ,',', 
     &      qpion(1) ,',',qpion(2) ,',',qpion(3) ,' ) [GeV]'
      call warnlog(msg)

      call warnlog('---------------------------')

 103  format(A,1pd12.5,A)
 104  format(A,1pd12.5,A,1pd9.2,A,1pd9.2,A,1pd9.2,A)
      return
      end

c**********************************************************************
! Erasing the written events 
! and resetting the event file for writing from scratch
      subroutine FLUSHreportevent_1pi
      implicit none
      include 'common.ekhara.inc.for'
      if (WriteEvents.ne.0) then
          close(8)
          if (EvtsFile .eq. 1) then
              open(8,file='output/events.out',status='UNKNOWN')
          elseif (EvtsFile .eq. 2) then
              open(8,file='output/events-2.out',status='UNKNOWN')
          endif
      endif
      return
      end

c**********************************************************************
! Misc options for writing the weights,
! distributions, ... into files for further plotting..
!
! This is to launched from  <subroutine evalUpperBound_1pi>
!                      and  <subroutine mc_loop_1pi> 
!
      subroutine reportcontrib
      implicit none
      include 'common.ekhara.inc.for'

      ! Just do nothing!
      return

      ! Below is if you really need to write the weights, 
      !       matrix element distribution, etc.
      
c      write (661,103) it1, ' ',JacobianFactor * Dab
c      write (662,103) it2, ' ',JacobianFactor * Dab
c      write (771,103) it1, ' ',Msq
c      write (772,103) it2, ' ',Msq
      if (contrib .gt. 0.Q0) then
        write (551,103) it1, ' ',contrib * FluxFactor
        write (552,103) it2, ' ',contrib * FluxFactor
        write (555,103) kk,  ' ',contrib * FluxFactor
      endif

 103  format(E24.14,A,E24.14)
      return
      end

c-----------------------------------------------------------------------
c     Here is a wrap for the matrix element squared (averaged)
c 
      subroutine m_el_1pi(Msq_correct)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 Msq_correct, Msq_tmp
      complex*32 ff_value_s,ff_value_t
      complex*32 piggFF, etaggFF, etaPggFF !function

      ! Here we have an option to calculate just the 
      ! VOLUME of the phase space by means of the same rest of the code
      if (PhSpOnly.eq.1) then
            Msq_correct = 1.Q0
            return 
      else
            if (channel_id .eq. 2) then 
              ff_value_s =  piggFF(s, kk,W2)
              ff_value_t =  piggFF(it1,it2,W2)
            elseif (channel_id .eq. 3) then 
              ff_value_s =  etaggFF(s, kk,W2)
              ff_value_t =  etaggFF(it1,it2,W2)
            elseif (channel_id .eq. 4) then 
              ff_value_s =  etaPggFF(s, kk,W2)
              ff_value_t =  etaPggFF(it1,it2,W2)
            endif

!----------------------------------------------------------------------
! Notice:
!       subroutine m_el_1pi_FORM(resultMsq,   ! return value
!      &          ff_value_s,ff_value_t,    ! |pi-g-g form factor|
!      &                        sw,           ! selector of amplitudes
!      &                        s,kk,it1,it2, ! invariants
!      &                        k,k1,l1,l2,   ! momenta
!      &                        q1,q2,p1,p2,me2
!      &                          )
!----------------------------------------------------------------------
            call m_el_1pi_FORM(Msq_tmp,
     &                        ff_value_s,ff_value_t,
     &                        sw_1pi,
     &                        s,kk,it1,it2,  k,g1,l1,l2,
     &                        q1,q2,p1,p2, me2)

            Msq_correct = Msq_tmp * pggCoupling
!----------------------------------------------------------------------
! Convention:
!            sqrt(pggCoupling)  = =  (e^4 Nc/(12 pi^2 F_ps))
!                               = =  4.Q0 * alpha**2 / F_ps
! 
!  -> see   subroutine       subroutine init_1pi
!  -> see   subroutine       subroutine init_1eta
!  -> see   subroutine       subroutine init_1etaP
! 
!----------------------------------------------------------------------
            return 
      endif
      return
      end
c**********************************************************************




c-----------------------------------------------------------------------
c     8>< ---      Matrix element calculation.              --- ><8
c
c     Important: We decided not to use COMMON blocks here
c-----------------------------------------------------------------------

      subroutine m_el_1pi_FORM(resultMsq,   ! return value
     &          ff2_value_s,ff2_value_t,   ! |pi-g-g form factor|
     &                        sw,          ! selector of amplitudes
     &                        s,kk,t1,t2,  ! invariants
     &                        k,k1,l1,l2,  ! momenta
     &                        q1,q2,p1,p2,me2
     &                          )
      ! Calculates the squared matrix element using the 
      ! expression from FORM.
      implicit none
      real*16 resultMsq
      complex*32 ff2_value_s,ff2_value_t
      integer sw
      real*16 s,kk,t1,t2
      real*16 k(0:3),k1(0:3),l1(0:3),l2(0:3)
      real*16 q1(0:3),q2(0:3),p1(0:3),p2(0:3)
      real*16 me2
      !----------------------------------------------------------------
      real*16 Msq, MsqS, MsqT, MsqINTERF

      real*16 DT                   ! function, Lorentz-vector DT product

      real*16 DT_kk, DT_kk1, DT_kl1, DT_kl2, 
     &         DT_k1k1, DT_k1l1, DT_k1l2, 
     &         DT_q1k, DT_q1k1, DT_q1q2,  DT_q1l1, DT_q1l2,
     &         DT_q2k, DT_q2k1,  DT_q2l1, DT_q2l2,
     &         DT_p1k, DT_p1k1, DT_p1q1, DT_p1q2, DT_p1p2,
     &         DT_p1l1, DT_p1l2,
     &         DT_p2k,  DT_p2k1, DT_p2q1, DT_p2q2,
     &         DT_p2l1, DT_p2l2,
     &         DT_l1l1, DT_l1l2,  DT_l2l2

       DT_kk   = kk        ! DT(k,k)
       DT_kk1  = DT(k,k1)
       DT_kl1  = DT(k,l1)
       DT_kl2  = DT(k,l2)

       DT_k1k1 = DT(k1,k1)
       DT_k1l1 = DT(k1,l1)
       DT_k1l2 = DT(k1,l2)

       DT_q1k  = DT(q1,k)
       DT_q1k1 = DT(q1,k1)
       DT_q1q2 = DT(q1,q2)
       DT_q1l1 = DT(q1,l1)
       DT_q1l2 = DT(q1,l2)

       DT_q2k  = DT(q2,k)
       DT_q2k1 = DT(q2,k1)
       DT_q2l1 = DT(q2,l1)
       DT_q2l2 = DT(q2,l2)

       DT_p1k  = DT(p1,k)
       DT_p1k1 = DT(p1,k1)
       DT_p1q1 = DT(p1,q1)
       DT_p1q2 = DT(p1,q2)
       DT_p1p2 = DT(p1,p2)
       DT_p1l1 = DT(p1,l1)
       DT_p1l2 = DT(p1,l2)

       DT_p2k  = DT(p2,k)
       DT_p2k1 = DT(p2,k1)
       DT_p2q1 = DT(p2,q1)
       DT_p2q2 = DT(p2,q2)
       DT_p2l1 = DT(p2,l1)
       DT_p2l2 = DT(p2,l2)

       DT_l1l1 = t1        ! DT(l1,l1)
       DT_l1l2 = DT(l1,l2)
       DT_l2l2 = t2        ! DT(l2,l2)


      ! Below is for calculation of the matrix element squared
      MsqS = 0.Q0
      MsqT = 0.Q0
      MsqINTERF = 0.Q0

      if (sw.ne.2) then       ! sw = 1 or 3  ->  include s-channel
            if (kk .ne. 0.Q0) then
                  MsqS = - ( 32.Q0*DT_p1p2*
     & DT_q1q2*DT_kk*DT_k1k1 - 32.Q0*DT_p1p2
     & *DT_q1q2*DT_kk1*DT_kk1 - 32.Q0*DT_p1p2*DT_q1k*
     & DT_q2k*DT_k1k1 + 32.Q0*DT_p1p2*DT_q1k*DT_q2k1*
     & DT_kk1 + 32.Q0*DT_p1p2*DT_q1k1*DT_q2k*DT_kk1
     &  - 32.Q0*DT_p1p2*DT_q1k1*DT_q2k1*DT_kk
     &  - 32.Q0*DT_p1q1*DT_p2q2*DT_kk*DT_k1k1
     &  + 32.Q0*DT_p1q1*DT_p2q2*
     & DT_kk1*DT_kk1 + 32.Q0*DT_p1q1*DT_p2k*DT_q2k*
     & DT_k1k1 - 32.Q0*DT_p1q1*DT_p2k*DT_q2k1*DT_kk1 
     & - 32.Q0*DT_p1q1*DT_p2k1*DT_q2k*DT_kk1 
     & + 32.Q0*DT_p1q1*DT_p2k1*DT_q2k1*DT_kk 
     & - 32.Q0*DT_p1q2*DT_p2q1*DT_kk*DT_k1k1 
     & + 32.Q0*DT_p1q2*DT_p2q1*DT_kk1*
     & DT_kk1 + 32.Q0*DT_p1q2*DT_p2k*DT_q1k*DT_k1k1
     &  - 32.Q0*DT_p1q2*DT_p2k*DT_q1k1*DT_kk1
     &  - 32.Q0*DT_p1q2*DT_p2k1*DT_q1k*DT_kk1
     &  + 32.Q0*DT_p1q2*DT_p2k1*DT_q1k1*DT_kk 
     &  + 32.Q0*DT_p1k*DT_p2q1*DT_q2k*DT_k1k1
     &  - 32.Q0*DT_p1k*DT_p2q1*DT_q2k1*DT_kk1 + 32.Q0
     & *DT_p1k*DT_p2q2*DT_q1k*DT_k1k1 - 32.Q0*DT_p1k*
     & DT_p2q2*DT_q1k1*DT_kk1 - 32.Q0*DT_p1k*DT_p2k*
     & DT_q1q2*DT_k1k1 + 64.Q0*DT_p1k*DT_p2k*DT_q1k1*
     & DT_q2k1 + 32.Q0*DT_p1k*DT_p2k*DT_k1k1*me2 
     & + 32.Q0*DT_p1k*DT_p2k1*DT_q1q2*DT_kk1 
     & - 32.Q0*DT_p1k*DT_p2k1*DT_q1k*DT_q2k1
     &  - 32.Q0*DT_p1k*DT_p2k1*DT_q1k1*DT_q2k
     &  - 32.Q0*DT_p1k*DT_p2k1*DT_kk1*me2 - 32.Q0*
     & DT_p1k1*DT_p2q1*DT_q2k*DT_kk1 + 32.Q0*DT_p1k1*
     & DT_p2q1*DT_q2k1*DT_kk - 32.Q0*DT_p1k1*DT_p2q2*
     & DT_q1k*DT_kk1 + 32.Q0*DT_p1k1*DT_p2q2*DT_q1k1*
     & DT_kk + 32.Q0*DT_p1k1*DT_p2k*DT_q1q2*DT_kk1 -
     & 32.Q0*DT_p1k1*DT_p2k*DT_q1k*DT_q2k1
     &  - 32.Q0*DT_p1k1*DT_p2k*DT_q1k1*DT_q2k 
     & - 32.Q0*DT_p1k1*DT_p2k*DT_kk1*me2
     &  - 32.Q0*DT_p1k1*DT_p2k1*DT_q1q2*DT_kk +
     & 64.Q0*DT_p1k1*DT_p2k1*DT_q1k*DT_q2k + 32.Q0*
     & DT_p1k1*DT_p2k1*DT_kk*me2 + 32.Q0*DT_q1k*DT_q2k*
     & DT_k1k1*me2 - 32.Q0*DT_q1k*DT_q2k1*DT_kk1*me2
     &  - 32.Q0*DT_q1k1*DT_q2k*DT_kk1*me2
     &  + 32.Q0*DT_q1k1*DT_q2k1*DT_kk*me2
     &  + 32.Q0*DT_kk*DT_k1k1*me2**2
     &  - 32.Q0*DT_kk1*DT_kk1*me2**2
     &     )
                MsqS = MsqS / (s*kk)**2
                ! now account for the transition form factor
                MsqS = MsqS * abs(ff2_value_s)**2
            else
                MsqS = 0.Q0
                call warnlog('kk = 0.  MsqS => 0')
            endif
      endif
      if (sw.ne.1) then       ! sw = 2 or 3  -> include t-channel
         if ((t1 .ne. 0.Q0) .and. (t2 .ne. 0.Q0)) then
                   MsqT = -( 
     &  - 32.Q0*DT_p1p2*DT_q1q2*DT_l1l1*DT_l2l2 + 32.Q0*
     & DT_p1p2*DT_q1q2*DT_l1l2*DT_l1l2 + 32.Q0*DT_p1p2*
     & DT_q1l1*DT_q2l1*DT_l2l2 - 32.Q0*DT_p1p2*DT_q1l1*
     & DT_q2l2*DT_l1l2 - 32.Q0*DT_p1p2*DT_q1l2*DT_q2l1*
     & DT_l1l2 + 32.Q0*DT_p1p2*DT_q1l2*DT_q2l2*DT_l1l1
     &  + 32.Q0*DT_p1q1*DT_p2q2*DT_l1l1*DT_l2l2 - 32.Q0*
     & DT_p1q1*DT_p2q2*DT_l1l2*DT_l1l2 - 32.Q0*DT_p1q1*
     & DT_p2l1*DT_q2l1*DT_l2l2 + 32.Q0*DT_p1q1*DT_p2l1*
     & DT_q2l2*DT_l1l2 + 32.Q0*DT_p1q1*DT_p2l2*DT_q2l1*
     & DT_l1l2 - 32.Q0*DT_p1q1*DT_p2l2*DT_q2l2*DT_l1l1
     &  - 32.Q0*DT_p1q2*DT_p2q1*DT_l1l1*DT_l2l2 + 32.Q0*
     & DT_p1q2*DT_p2q1*DT_l1l2*DT_l1l2 + 32.Q0*DT_p1q2*
     & DT_p2l1*DT_q1l1*DT_l2l2 - 32.Q0*DT_p1q2*DT_p2l1*
     & DT_q1l2*DT_l1l2 - 32.Q0*DT_p1q2*DT_p2l2*DT_q1l1*
     & DT_l1l2 + 32.Q0*DT_p1q2*DT_p2l2*DT_q1l2*DT_l1l1
     &  + 32.Q0*DT_p1l1*DT_p2q1*DT_q2l1*DT_l2l2 - 32.Q0*
     & DT_p1l1*DT_p2q1*DT_q2l2*DT_l1l2 - 32.Q0*DT_p1l1*
     & DT_p2q2*DT_q1l1*DT_l2l2 + 32.Q0*DT_p1l1*DT_p2q2*
     & DT_q1l2*DT_l1l2 + 32.Q0*DT_p1l1*DT_p2l1*DT_q1q2*
     & DT_l2l2 - 32.Q0*DT_p1l1*DT_p2l1*DT_q1l2*DT_q2l2
     &  - 32.Q0*DT_p1l1*DT_p2l2*DT_q1q2*DT_l1l2 + 64.Q0*
     & DT_p1l1*DT_p2l2*DT_q1l1*DT_q2l2 - 32.Q0*DT_p1l1*
     & DT_p2l2*DT_q1l2*DT_q2l1 - 32.Q0*DT_p1l1*DT_q1l1*
     & DT_l2l2*me2 + 32.Q0*DT_p1l1*DT_q1l2*DT_l1l2*me2
     &  - 32.Q0*DT_p1l2*DT_p2q1*DT_q2l1*DT_l1l2 + 32.Q0*
     & DT_p1l2*DT_p2q1*DT_q2l2*DT_l1l1 + 32.Q0*DT_p1l2*
     & DT_p2q2*DT_q1l1*DT_l1l2 - 32.Q0*DT_p1l2*DT_p2q2*
     & DT_q1l2*DT_l1l1 - 32.Q0*DT_p1l2*DT_p2l1*DT_q1q2*
     & DT_l1l2 - 32.Q0*DT_p1l2*DT_p2l1*DT_q1l1*DT_q2l2
     &  + 64.Q0*DT_p1l2*DT_p2l1*DT_q1l2*DT_q2l1 + 32.Q0*
     & DT_p1l2*DT_p2l2*DT_q1q2*DT_l1l1 - 32.Q0*DT_p1l2*
     & DT_p2l2*DT_q1l1*DT_q2l1 + 32.Q0*DT_p1l2*DT_q1l1*
     & DT_l1l2*me2 - 32.Q0*DT_p1l2*DT_q1l2*DT_l1l1*me2
     &  - 32.Q0*DT_p2l1*DT_q2l1*DT_l2l2*me2 + 32.Q0*
     & DT_p2l1*DT_q2l2*DT_l1l2*me2 + 32.Q0*DT_p2l2*
     & DT_q2l1*DT_l1l2*me2 - 32.Q0*DT_p2l2*DT_q2l2*
     & DT_l1l1*me2 + 32.Q0*DT_l1l1*DT_l2l2*me2**2
     &  - 32.Q0*DT_l1l2*DT_l1l2*me2**2
     &           )
                MsqT =  MsqT / (t1 * t2)**2
                ! now account for the transition form factor
                MsqT =  MsqT * abs(ff2_value_t)**2
          else
                MsqT = 0.Q0
                call warnlog('t1 or t2 = 0.  MsqT => 0')
          endif
      endif

      if (sw.eq.3) then       ! sw == 3 -> include s/t interference
            if ((t1 .ne. 0.Q0) .and. (t2 .ne. 0.Q0) 
     &                           .and. (kk .ne. 0.Q0)) then
       MsqINTERF = 8.Q0*DT_p1p2*DT_q1k
     & *DT_q2l1*DT_k1l2 - 8.Q0*DT_p1p2
     & *DT_q1k*DT_q2l2*DT_k1l1 - 8.Q0*DT_p1p2*DT_q1k1*
     & DT_q2l1*DT_kl2 + 8.Q0*DT_p1p2*DT_q1k1*DT_q2l2*
     & DT_kl1 - 8.Q0*DT_p1p2*DT_q1l1*DT_q2k*DT_k1l2+8.Q0
     & *DT_p1p2*DT_q1l1*DT_q2k1*DT_kl2 + 8.Q0*DT_p1p2*
     & DT_q1l2*DT_q2k*DT_k1l1 - 8.Q0*DT_p1p2*DT_q1l2*
     & DT_q2k1*DT_kl1 - 8.Q0*DT_p1q1*DT_p2k*DT_q2l1*
     & DT_k1l2 + 8.Q0*DT_p1q1*DT_p2k*DT_q2l2*DT_k1l1
     &  + 8.Q0*DT_p1q1*DT_p2k1*DT_q2l1*DT_kl2
     &  - 8.Q0*DT_p1q1*DT_p2k1*DT_q2l2*DT_kl1
     &  + 8.Q0*DT_p1q1*DT_p2l1*DT_q2k*DT_k1l2
     &  - 8.Q0*DT_p1q1*DT_p2l1*DT_q2k1*DT_kl2 
     &  - 8.Q0*DT_p1q1*DT_p2l2*DT_q2k*DT_k1l1 
     &  + 8.Q0*DT_p1q1*DT_p2l2*DT_q2k1*DT_kl1 
     & - 8.Q0*DT_p1q2*DT_p2k*DT_q1l1*DT_k1l2
     &  + 8.Q0*DT_p1q2*DT_p2k*DT_q1l2*DT_k1l1
     &   + 8.Q0*DT_p1q2*DT_p2k1*DT_q1l1*DT_kl2 
     & - 8.Q0*DT_p1q2*DT_p2k1*DT_q1l2*DT_kl1 - 8.Q0*
     & DT_p1q2*DT_p2l1*DT_q1k*DT_k1l2 + 8.Q0*DT_p1q2*
     & DT_p2l1*DT_q1k1*DT_kl2 + 8.Q0*DT_p1q2*DT_p2l2*
     & DT_q1k*DT_k1l1 - 8.Q0*DT_p1q2*DT_p2l2*DT_q1k1*
     & DT_kl1 - 16.Q0*DT_p1q2*DT_kl1*DT_k1l2*me2 + 16.Q0*
     & DT_p1q2*DT_kl2*DT_k1l1*me2 + 8.Q0*DT_p1k*DT_p2q1
     & *DT_q2l1*DT_k1l2 - 8.Q0*DT_p1k*DT_p2q1*DT_q2l2*
     & DT_k1l1+ 8.Q0*DT_p1k*DT_p2q2*DT_q1l1*DT_k1l2-8.Q0
     & *DT_p1k*DT_p2q2*DT_q1l2*DT_k1l1 - 8.Q0*DT_p1k*
     & DT_p2l1*DT_q1q2*DT_k1l2 - 8.Q0*DT_p1k*DT_p2l1*
     & DT_k1l2*me2 + 8.Q0*DT_p1k*DT_p2l2*DT_q1q2*
     & DT_k1l1 + 8.Q0*DT_p1k*DT_p2l2*DT_k1l1*me2
     &  - 8.Q0*DT_p1k*DT_q1l1*DT_k1l2*me2
     &  + 8.Q0*DT_p1k*DT_q1l2*DT_k1l1*me2
     &  + 8.Q0*DT_p1k*DT_q2l1*DT_k1l2*me2 - 8.Q0*
     & DT_p1k*DT_q2l2*DT_k1l1*me2 - 8.Q0*DT_p1k1
     & *DT_p2q1*DT_q2l1*DT_kl2 + 8.Q0*DT_p1k1*DT_p2q1
     & *DT_q2l2*DT_kl1 -8.Q0*DT_p1k1*DT_p2q2*DT_q1l1
     & *DT_kl2+8.Q0*DT_p1k1*DT_p2q2*DT_q1l2*DT_kl1
     &  + 8.Q0*DT_p1k1*DT_p2l1*DT_q1q2*DT_kl2 
     &  + 8.Q0*DT_p1k1*DT_p2l1*DT_kl2*me2
     &  - 8.Q0*DT_p1k1*DT_p2l2*DT_q1q2*DT_kl1 
     & - 8.Q0*DT_p1k1*DT_p2l2*DT_kl1*me2 + 8.Q0*DT_p1k1
     & *DT_q1l1*DT_kl2*me2
     &  - 8.Q0*DT_p1k1*DT_q1l2*DT_kl1*me2

      MsqINTERF = MsqINTERF-8.Q0*DT_p1k1*DT_q2l1*DT_kl2*me2 +
     & 8.Q0*DT_p1k1*DT_q2l2*DT_kl1*me2 + 8.Q0*DT_p1l1*
     & DT_p2q1*DT_q2k*DT_k1l2 - 8.Q0*DT_p1l1*DT_p2q1*
     & DT_q2k1*DT_kl2 - 8.Q0*DT_p1l1*DT_p2q2*DT_q1k*
     & DT_k1l2 + 8.Q0*DT_p1l1*DT_p2q2*DT_q1k1*DT_kl2
     &  + 8.Q0*DT_p1l1*DT_p2k*DT_q1q2*DT_k1l2 
     & + 8.Q0*DT_p1l1*DT_p2k*DT_k1l2*me2 
     & - 8.Q0*DT_p1l1*DT_p2k1*DT_q1q2*DT_kl2 
     & - 8.Q0*DT_p1l1*DT_p2k1*DT_kl2*me2 + 8.Q0
     & *DT_p1l1*DT_q1k*DT_k1l2*me2 - 8.Q0*DT_p1l1*
     & DT_q1k1*DT_kl2*me2 + 8.Q0*DT_p1l1*DT_q2k*DT_k1l2*me2
     &  - 8.Q0*DT_p1l1*DT_q2k1*DT_kl2*me2 - 8.Q0*DT_p1l2*
     & DT_p2q1*DT_q2k*DT_k1l1 + 8.Q0*DT_p1l2*DT_p2q1*
     & DT_q2k1*DT_kl1 + 8.Q0*DT_p1l2*DT_p2q2*DT_q1k*
     & DT_k1l1 - 8.Q0*DT_p1l2*DT_p2q2*DT_q1k1*DT_kl1 - 8.Q0*
     & DT_p1l2*DT_p2k*DT_q1q2*DT_k1l1 - 8.Q0*DT_p1l2*
     & DT_p2k*DT_k1l1*me2 + 8.Q0*DT_p1l2*DT_p2k1*
     & DT_q1q2*DT_kl1 + 8.Q0*DT_p1l2*DT_p2k1*DT_kl1*me2
     &  - 8.Q0*DT_p1l2*DT_q1k*DT_k1l1*me2 + 8.Q0*DT_p1l2*
     & DT_q1k1*DT_kl1*me2 - 8.Q0*DT_p1l2*DT_q2k*DT_k1l1*me2
     &  + 8.Q0*DT_p1l2*DT_q2k1*DT_kl1*me2 + 16.Q0*DT_p2q1
     & *DT_kl1*DT_k1l2*me2 - 16.Q0*DT_p2q1*DT_kl2*
     & DT_k1l1*me2 - 8.Q0*DT_p2k*DT_q1l1*DT_k1l2*me2 + 8.Q0*
     & DT_p2k*DT_q1l2*DT_k1l1*me2 + 8.Q0*DT_p2k*
     & DT_q2l1*DT_k1l2*me2 - 8.Q0*DT_p2k*DT_q2l2*DT_k1l1*me2
     &  + 8.Q0*DT_p2k1*DT_q1l1*DT_kl2*me2 - 8.Q0*DT_p2k1*
     & DT_q1l2*DT_kl1*me2 - 8.Q0*DT_p2k1*DT_q2l1*DT_kl2
     & *me2 + 8.Q0*DT_p2k1*DT_q2l2*DT_kl1*me2 - 8.Q0*
     & DT_p2l1*DT_q1k*DT_k1l2*me2 + 8.Q0*DT_p2l1*DT_q1k1*
     & DT_kl2*me2 - 8.Q0*DT_p2l1*DT_q2k*DT_k1l2*me2 + 
     & 8.Q0*DT_p2l1*DT_q2k1*DT_kl2*me2 + 8.Q0*DT_p2l2*
     & DT_q1k*DT_k1l1*me2 - 8.Q0*DT_p2l2*DT_q1k1*DT_kl1*
     & me2 + 8.Q0*DT_p2l2*DT_q2k*DT_k1l1*me2 - 8.Q0*
     & DT_p2l2*DT_q2k1*DT_kl1*me2 + 8.Q0*DT_q1k*DT_q2l1*
     & DT_k1l2*me2 - 8.Q0*DT_q1k*DT_q2l2*DT_k1l1*me2 - 8.Q0
     & *DT_q1k1*DT_q2l1*DT_kl2*me2 + 8.Q0*DT_q1k1*
     & DT_q2l2*DT_kl1*me2 - 8.Q0*DT_q1l1*DT_q2k*DT_k1l2*me2
     &  + 8.Q0*DT_q1l1*DT_q2k1*DT_kl2*me2 + 8.Q0*DT_q1l2*
     & DT_q2k*DT_k1l1*me2 - 8.Q0*DT_q1l2*DT_q2k1*
     & DT_kl1*me2
              MsqINTERF = 2.Q0 * MsqINTERF / (t1 * t2)
              MsqINTERF = MsqINTERF / (s*kk)
              ! now account for the transition form factor
              MsqINTERF = MsqINTERF 
     &            * qreal(conjg(ff2_value_s) * ff2_value_t) 
            else
                MsqT = 0.Q0
                call warnlog('t1 or t2 or kk = 0.  MsqInterf => 0')
            endif
      endif
      Msq = MsqS + MsqT + MsqINTERF
      Msq = Msq * 0.25Q0
      resultMsq = Msq
      return
      end

c**********************************************************************
      real*16 function DT(a,b)
      ! Dot product of Lorentz 4-vectors: (a.b)
      implicit none
      real*16 a(0:3), b(0:3)

      DT = a(0)*b(0)-a(1)*b(1)-a(2)*b(2)-a(3)*b(3)
      return
      end
c**********************************************************************




c-----------------------------------------------------------------------
c     8>< ---             Ph.Sp. generators               --- ><8
c-----------------------------------------------------------------------

c**********************************************************************
!  wrap for final state phase space generation
      subroutine phasespace_1pi(rrc) 
      implicit none
      include 'common.ekhara.inc.for'
      real*8 rrc(0:6)     ! generated randoms
                          ! use rrc(1) to rrc(6) as randoms.
      real*8 branch_rnd
      real*8 branch_param

      real*16 Ds,Dt
      ! Here I select which routine is used to generate the phase space.
      select case(WorkingGeneratorId)
                              ! 1 = s-channel specific, 
                              ! 2 = t-channel specific,
                              ! 3 = (s+t)-specific 
            case(1)
                  call phsp_1pi_S(rrc)
            case(2)
                  call phsp_1pi_G(rrc)
            case(3)
                  call ranlxdf(branch_rnd, 1)

                  ! define how much of each branch we want
                  ! 0.d0 <= branch_param <= 1.d0
                  ! full S ............... full T
                  !
                  !branch_param = 0.5d0
                  branch_param = 0.9d0
                  ! branches with partial probability
                  if (branch_rnd .ge. branch_param) then
                      call phsp_1pi_S(rrc)
                                         ! branch_param < r < 1
                      Ds = kk
                      Dt = it1*it2
                      ! correction of volume due to branching,
                      ! Note:    the below formula depends 
                      !          on the choice of mappings in S- and T- generators.
                      !          The expression is given for ("log" & "t1 t2").
                      if (Dab .gt. 0.Q0) then
                          Dab = Dab * Dt
     &                        /(branch_param*Ds 
     &                           + (1.d0-branch_param)*Dt )
                      endif
                  else
                      call phsp_1pi_G(rrc)
                                         ! 0 < r < branch_param
                      Ds = kk
                      Dt = it1*it2
                      ! correction of volume due to branching,
                      ! Note:    the below formula depends 
                      !          on the choice of mappings in S- and T- generators.
                      !          The expression is given for ("log" & "t1 t2").
                      if (Dab .gt. 0.Q0) then
                          Dab = Dab * Ds
     &                        /(branch_param*Ds 
     &                           + (1.d0-branch_param)*Dt )
                      endif
                  endif
      end select

      ! Now test the numerical momentum conservation and 
      ! whether the final particles are on-shell
      call CheckMomentConserv

      ! Notice: the kinematic cuts are to be called from MC loop, 
      !         not here!
      return
      end

c**********************************************************************
! s-generator of phase space
      subroutine phsp_1pi_S(rrc) 
      implicit none
      include 'common.ekhara.inc.for'

      real*8 rrc(0:6)     ! generated randoms
                          ! use rrc(1) to rrc(6) here only
      real*16 cthpion, sthpion, cphipion, sphipion
      real*16 phipion

      real*16 cthAst, sthAst, cphiAst, sphiAst
      real*16 phiAst
      real*16 qAst(0:3), qAstabs

      real*16 phi1, cphi1, sphi1, cth1, sth1     ! angles of the final positron
      real*16 phi2, cphi2, sphi2, cth2, sth2     ! angles of the final elektron

      real*16 gBoost, Ek, kabs2, scalprod, momBoost1, momBoost2 
              !! variables for Lorentz boost

      real*16 finalZ, compZ

      real*16 lambdaf ! kinematical lambda-function

      compZ = 10.Q0**(-15)

! Important: this functions actively use the COMMON variables
!            -- momenta, "volume factor" and Jacobian

      phipion = 2.Q0 * pi * rrc(1)                   ! 1st random
      cphipion = qcos(phipion)
      sphipion = qsin(phipion)

      cthpion = -1.Q0 + 2.Q0*rrc(2)                  ! 2nd random
c      sthpion = sqrt(1.Q0 - cthpion**2)       ! cancelation at phi small and near pi
      sthpion = 2.Q0 * qsqrt(rrc(2)*(1.Q0 - rrc(2)) )       !  cancelation at phi near pi/2

      phiAst = 2.Q0 * pi * rrc(3)                    ! 3rd random
      cphiAst = qcos(phiAst)
      sphiAst = qsin(phiAst)

      cthAst = -1.Q0 + 2.Q0*rrc(4)                   ! 4th random
c      sthAst = sqrt(1.Q0 - cthAst**2)       ! cancelation at phi small and near pi
      sthAst = 2.Q0 * qsqrt(rrc(4)*(1.Q0 - rrc(4)) )       !  cancelation at phi near pi/2

! change of variables
      select case(change_id_s) 
        case (4) !(most efficient)
           kk = 4.Q0 * me2 * 
     &     ((ss-W)**2/(4.Q0*me2))**(rrc(5))         ! 5th random
        case (3)
           kk = 4.Q0 * me2 * 
     &     ((ss-W)**2/(4.Q0*me2))**(rrc(5)**(1.Q0/3.Q0))
        case (2)
           kk = 4.Q0 * me2 + rrc(5)*
     &     ((ss-W)**2 - (4.Q0*me2))
        case default ! default is also (1)
           kk = 4.Q0 * me2 /
     &      (1.Q0 - rrc(5)*
     &      (1.Q0 - (4.Q0*me2)/(ss-W)**2) )
      end select

      qpionabs = 0.5Q0/ss * qsqrt( lambdaf(ss**2, kk, W2) )
      qpion(0) = qsqrt(W2 + qpionabs**2)
      qpion(1) = qpionabs * sthpion * sphipion
      qpion(2) = qpionabs * sthpion * cphipion
      qpion(3) = qpionabs * cthpion

      k(0) = qsqrt(qpionabs**2 + kk)
      k(1) = - qpion(1)
      k(2) = - qpion(2)
      k(3) = - qpion(3)

      ! final electron (q_2^\ast)
      qAstabs = 0.5Q0/qsqrt(kk) * qsqrt( lambdaf(kk,me2,me2) )
      qAst(0) = qsqrt(me2 + qAstabs**2)
      qAst(1) = qAstabs * sthAst * sphiAst
      qAst(2) = qAstabs * sthAst * cphiAst
      qAst(3) = qAstabs * cthAst

      ! now we boost to the initial e+e- cm frame
      Ek = k(0)
      kabs2 = k(1)**2 + k(2)**2 + k(3)**2
      gBoost = Ek/qsqrt(kk)
      scalprod = k(1)*qAst(1) + k(2)*qAst(2) + k(3)*qAst(3)
      momBoost1 = qAst(0)/qsqrt(kk) - (gBoost-1.Q0)*scalprod/kabs2
      momBoost2 = qAst(0)/qsqrt(kk) + (gBoost-1.Q0)*scalprod/kabs2

      q1(0) = gBoost*(qAst(0) - scalprod/Ek)
      q1(1) = -qAst(1) + k(1)*momBoost1
      q1(2) = -qAst(2) + k(2)*momBoost1
      q1(3) = -qAst(3) + k(3)*momBoost1

      q2(0) = gBoost*(qAst(0) + scalprod/Ek)
      q2(1) = qAst(1) + k(1)*momBoost2
      q2(2) = qAst(2) + k(2)*momBoost2
      q2(3) = qAst(3) + k(3)*momBoost2

      ! Now calculate the angles of the final leptons.
      ! positron
      q1abs = qsqrt(q1(1)**2 + q1(2)**2 + q1(3)**2)
      cth1 = q1(3)/q1abs
      sth1 = qsqrt(1.Q0 - cth1**2)
      cphi1 = q1(2)/(q1abs * sth1)
      sphi1 = q1(1)/(q1abs * sth1)
      if (sphi1 .ge. 0.Q0) then
            phi1 = qacos(cphi1)
      else
            phi1 = 2.Q0*pi - qacos(cphi1)
      endif

      ! electron
      q2abs = qsqrt(q2(1)**2 + q2(2)**2 + q2(3)**2)
      cth2 = q2(3)/q2abs 
      sth2 = qsqrt(1.Q0 - cth2**2)
      cphi2 = q2(2)/(q2abs * sth2)
      sphi2 = q2(1)/(q2abs * sth2)
      if (sphi2 .ge. 0.Q0) then
            phi2 = qacos(cphi2)
      else
            phi2 = 2.Q0*pi - qacos(cphi2)
      endif

        !  l1 and l2 label the virtual-photon momenta which appear in 
        !   the formula for the "t-channel" amplitude
        !            l1 = p1 - q1
        !            l2 = p2 - q2
      l1(0) = p1(0) - q1(0)
      l1(1) = p1(1) - q1(1)
      l1(2) = p1(2) - q1(2)
      l1(3) = p1(3) - q1(3)

      l2(0) = p2(0) - q2(0)
      l2(1) = p2(1) - q2(1)
      l2(2) = p2(2) - q2(2)
      l2(3) = p2(3) - q2(3)

      it1 = l1(0)**2 - l1(1)**2 - l1(2)**2 - l1(3)**2
      it2 = l2(0)**2 - l2(1)**2 - l2(2)**2 - l2(3)**2

      ! Jacobian Factor: s-channel
      select case(change_id_s) ! change of variables
        case (4) !(most efficient)
             Dab = qabs(kk)
             JacobianFactor = 4.Q0 * (2.Q0*pi)**2 *
     &       qlog((ss - W)**2/(4.Q0 * me2))
        case (3)
             Dab = qabs(kk)
             JacobianFactor = ( 4.Q0 * (2.Q0*pi)**2 *
     &       qlog((ss - W)**2/(4.Q0 * me2))/3.Q0 ) *
     &       rrc(5)**(-2.Q0/3.Q0)
        case (2)
             Dab = 1.Q0
             JacobianFactor =  4.Q0 * (2.Q0*pi)**2 *
     &       ((ss - W)**2 - (4.Q0 * me2)) 
        case default ! default is also (1)
             Dab = kk**2
             JacobianFactor = 4.Q0 * (2.Q0*pi)**2 *
     &       (1.Q0/(4.Q0 * me2) - 1.Q0/(ss - W)**2 )
      end select

      ! Phase Space diff. Volume
      PhaseVol=qsqrt( (ss**2+kk-W2)**2/(4.Q0*ss**2)-kk )
     &            * qsqrt( kk/4.Q0-me2 ) / (ss*qsqrt(kk))
     &            * 1.Q0/(pi**5 * 2.Q0**9)
      
      g1(0) = ss
      g1(1) = 0.Q0
      g1(2) = 0.Q0
      g1(3) = 0.Q0

      ! -----------------------------------------------------
      !  s1 and s2 are the invariants, which naturally appear 
      !  the "t-channel" mapping:
      !            s1 = (q1 + qpion)^2
      !            s2 = (q2 + qpion)^2
      !  we calculate it here just in order to be able 
      !  to histogram it, 
      !  and, e.g. compare the event distributions vs (s1) and (s2)
      s1 = (q1(0) + qpion(0))**2 - (q1(1) + qpion(1))**2
     &    -(q1(2) + qpion(2))**2 - (q1(3) + qpion(3))**2 

      s2 = (q2(0) + qpion(0))**2 - (q2(1) + qpion(1))**2
     &    -(q2(2) + qpion(2))**2 - (q2(3) + qpion(3))**2 

      return
      end

c**********************************************************************
! t-generator of phase space (wrap)
      subroutine phsp_1pi_G(rrc) 
      implicit none
      include 'common.ekhara.inc.for'

      real*8 rrc(0:6)     ! generated randoms
                          ! use rrc(1) to rrc(6) here only!

      select case(change_id_g) ! change of variables
            case (3) ! t-specific mapping:         ab. 1/t2 ,   1/t1
                 call generate_t_usual(rrc)
            case (2) ! t-specific mapping:         flat
                 call generate_t_plain(rrc)
            case default ! = usual
                 call generate_t_usual(rrc)
      end select

      g1(0) = ss
      g1(1) = 0.Q0
      g1(2) = 0.Q0
      g1(3) = 0.Q0

      return
      end

c**********************************************************************
! t-generator of phase space : plain
      subroutine generate_t_plain(rrc)
      implicit none
      include 'common.ekhara.inc.for'

      real*8 rrc(0:6)     ! generated randoms
                          ! use rrc(1) to rrc(6) here only
      real*16 E1, E2
      real*16 Emaximum ! maximum of the E1 and E2 -- final e+ and e- energies
      real*16 Ecritic  ! value of E, after which the cos2beta is bounded.
      real*16 cosbeta, cbM, cbP

      ! variables for solving the equation for E2
      real*16 A, B, C, D, BR, E2M, E2P

      real*16 lambdaf ! function
      real*16 xcrit   ! function

      real*16 phi1, cphi1, sphi1, cth1, sth1     ! angles of the final positron
      real*16 phi2, cphi2, sphi2, cth2, sth2     ! angles of the final elektron

      CHARACTER msg*78         ! for logging

      Emaximum  = (s-W2-2.Q0*me*W)/(2.Q0*ss)
      Ecritic   = (s-2.Q0*me*ss-W2+2.Q0*me2)
     &               /(2.Q0*(ss-me)) 

      E1    = CUT_E1min + rrc(1)*(Emaximum - CUT_E1min)  ! 1st random

      q1abs = qsqrt(E1**2 - me2)

      phi1 = 2.Q0 * pi * rrc(2)                     ! 2nd random
      cphi1 = qcos(phi1)
      sphi1 = qsin(phi1)
      cth1 = -1.Q0 + 2.Q0 * rrc(3)                  ! 3rd random
      sth1 = 2.Q0 * qsqrt(rrc(3) * (1.Q0 - rrc(3)))

      q1(0) = E1
      q1(1) = q1abs * sth1 * sphi1
      q1(2) = q1abs * sth1 * cphi1
      q1(3) = q1abs * cth1 

      phi2 = 2.Q0 * pi* rrc(4)                      ! 4th random
      cphi2 = qcos(phi2)
      sphi2 = qsin(phi2)
      cth2 = -1.Q0 + 2.Q0 * rrc(5)                  ! 5th random
      sth2 = 2.Q0 * qsqrt(rrc(5) * (1.Q0 - rrc(5)))

      !-----------------------------------------------------------------
      !---------- Take control on the allowed angles  ------------------
      cosbeta =   sth1 * sphi1 * sth2 * sphi2 +
     &            sth1 * cphi1 * sth2 * cphi2 + cth1 * cth2 

      if (E1 .gt. Ecritic) then     
            if (cosbeta .gt. 0.Q0) then
                  OutFlag = .true.
                  return
            endif
            if (cosbeta**2 .lt. xcrit(E1)) then
                  OutFlag = .true.
                  return
            endif
      endif

      if (cosbeta .lt. -1.Q0) then
            msg = ' '
            write (msg,1031)"cos(beta)= ", cosbeta,", now set to (-1)"
            call warnlog(msg)
            cosbeta = -1.Q0
      elseif (cosbeta .gt. 1.Q0) then
            msg = ' '
            write (msg,1031)"cos(beta)= ", cosbeta,", now set to (+1)"
            call warnlog(msg)
            cosbeta = 1.Q0
      endif

      !-----------------------------------------------------------------
      !---------- evaluation of the E2 ---------------------------------
      A  = (ss - E1)**2 - (E1**2 - me2) * cosbeta**2
      BR = s - 2.Q0*ss*E1 - W2 + 2.Q0* me2
      B  = -(ss - E1)*BR
      C  = (BR**2)/4.Q0 + me2*(E1**2 - me2)* cosbeta**2
      D  = B**2 - 4.Q0*A*C
      if (D .lt. 0.Q0) then
           call warnlog("D < 0, now set to D = 0.")
           D  = 0.Q0
      endif
      E2M = (- B - qsqrt(D) )/(2.Q0 * A)
      E2P = (- B + qsqrt(D) )/(2.Q0 * A)
      ! Now we have to pick up only one solution for E2 of two (E2P and E2M)
      if (cosbeta .lt. 0.Q0) then
            E2 = E2P
      else
            E2 = E2M
      endif
      if (E2 .lt. me) then
            call warnlog('Error! E2 < me !')
            E2 = me
      endif
      if (E2 .gt. Emaximum) then
            call warnlog('Error! E2 > Emax !')
            call warnlog('setting E2 to Emax.')
            E2 = Emaximum
      endif
      ! ---------------------------------------------------------------

      q2abs = qsqrt(E2**2 - me2)
      q2(0) = E2
      q2(1) = q2abs * sth2 * sphi2
      q2(2) = q2abs * sth2 * cphi2
      q2(3) = q2abs * cth2 

      qpion(0) = ss - E1 - E2
      qpion(1) = - q1(1) - q2(1)
      qpion(2) = - q1(2) - q2(2)
      qpion(3) = - q1(3) - q2(3)
      qpionabs = qsqrt(qpion(1)**2 + qpion(2)**2 + qpion(3)**2)

      PhaseVol = 2.Q0 * (E2**2 - me2) 
     &                  / qabs(E2 * cosbeta + (ss - E1)*q2abs/q1abs)
     &                  * 1.Q0/(pi**5 * 2.Q0**9)
     
      Dab = 1.Q0
      JacobianFactor = (4.Q0*pi)**2  *(Emaximum - me)

      ! -----------------------------------------------------
      ! k = q1 + q2 = (k0, - \vec(qpion))
      k(0) = q1(0) + q2(0)
      k(1) = - qpion(1)
      k(2) = - qpion(2)
      k(3) = - qpion(3)

      kk   = k(0)**2 - k(1)**2 - k(2)**2 - k(3)**2

      ! -----------------------------------------------------
      !  l1 and l2 label the virtual-photon momenta which appear in 
      !   the formula for the "t-channel" amplitude
      !            l1 = p1 - q1
      !            l2 = p2 - q2
      l1(0) = p1(0) - q1(0)
      l1(1) = p1(1) - q1(1)
      l1(2) = p1(2) - q1(2)
      l1(3) = p1(3) - q1(3)

      l2(0) = p2(0) - q2(0)
      l2(1) = p2(1) - q2(1)
      l2(2) = p2(2) - q2(2)
      l2(3) = p2(3) - q2(3)

      ! -----------------------------------------------------

      it1 = l1(0)**2 - l1(1)**2 - l1(2)**2 - l1(3)**2
      it2 = l2(0)**2 - l2(1)**2 - l2(2)**2 - l2(3)**2
 1031 format(A,1pd12.5,A)
      return
      end


c**********************************************************************
! t-generator of phase space (init)

      subroutine t_init
      implicit none
      include 'common.ekhara.inc.for'

      call t_init_correct
      ! call t_init_problem
      return
      end

c-----------------------------------------------------------------------

      subroutine t_init_correct
      implicit none
      include 'common.ekhara.inc.for'

      real*16 Wp, Wm

      real*16 Wm2, beta2, tcrit, s1u, s1l, theta, aux,
     &        s1hat, t2hat, s1z, t2low

!      real*16 lambdaf ! kinematical lambda-function   <-- for some events is not appropriate

      real*16 lambdaf,x,y,z
      lambdaf(x,y,z) = (x - y - z)**2  - 4.Q0*y*z

      Wm    = W + 2.Q0*me  ! the + sign is accordingly to code
      Wm2   = Wm*Wm

      beta2 = 1.Q0 - 4.Q0*me2/s
      beta  = qsqrt( beta2 )

      tcrit = 2.Q0*me2 - me*ss

!     --------------- kinematic limits due to cuts [ t2 ]---------------
      ! calculate t2max accordingly to CUT_th2min and E2 cuts
      s1u   = (ss - me)**2
      s1l   = (W  + me)**2
      s1u   = qmin1( s1u, me2 + s*(1.Q0 - 2.Q0*CUT_E2min/ss) )
      s1l   = qmax1( s1l, me2 + s*(1.Q0 - 2.Q0*CUT_E2max/ss) )

      If(s1l.ge.s1u) then 
          call errlog('No phase space: s1l .ge. s1u')
          stop
      Endif

      theta = (180.Q0 - CUT_th2max)*Pi/180.Q0

      aux  = 4.Q0*me2/s + beta2*qsin(theta)**2

      s1hat = me2 + s*beta2*qsin(theta)**2/
     &            (aux*(1.Q0 + 2.Q0*me/qsqrt(s*aux)))
      t2hat = 2.Q0*me2-me*ss*qsqrt(4.Q0*me2/S+beta2*qsin(theta)**2)
      If( s1hat.ge.s1u ) then
         s1z    = s1u
         If(CUT_E2min.le.me) then
            aux = 0.Q0
            t2max = tcrit
         Else
            aux  = lambdaf(s,s1z,me2)
            t2max =(3.Q0*me2-s+s1z-qcos(theta)*beta*qsqrt(aux))/2.Q0
         Endif
         t2max = (me2*(s1z-me2)**2/s+beta2*aux*qsin(theta)**2/4.Q0)
     &            / t2max
      Elseif( s1hat.le.s1l ) then
         s1z    = s1l
         aux  = lambdaf(s,s1z,me2)
         t2max = (3.Q0*me2-s+s1z-qcos(theta)*beta*qsqrt(aux))/2.Q0
         t2max =(me2*(s1z-me2)**2/s+beta2*aux*qsin(theta)**2/4.Q0)
     &            / t2max
      Else
         t2max = t2hat
      Endif

      ! calculate t2min accordingly to CUT_th2max and E2 cuts
      theta = (180.Q0 - CUT_th2min)*Pi/180.Q0

      s1z    = s1l
      aux  = lambdaf(s,s1z,me2)
      t2min = (3.Q0*me2-s+s1z+qcos(theta)*beta*qsqrt(aux))/2.Q0
      s1z    = s1u
      If(CUT_E2min.le.me) then
            aux = 0.Q0
            t2low = tcrit
      Else
            aux  = lambdaf(s,s1z,me2)
            t2low =(3.Q0*me2-s+s1z+qcos(theta)*beta*qsqrt(aux))/2.Q0
      Endif

      t2min = qmin1(t2min,t2low)
!     ------------------------------------------------------------------


!     --------------- kinematic limits due to USER cuts DIRECTLY on t2 -
      t2min = qmax1(t2min,CUT_t2min)
      t2max = qmin1(t2max,CUT_t2max)
!     ------------------------------------------------------------------

      If(t2min.ge.t2max) then
          call errlog('No phase space: t2min .ge. t2max')
          stop
      Endif

      return
      end

c-----------------------------------------------------------------------
      subroutine t_init_problem
      implicit none
      include 'common.ekhara.inc.for'

      real*16 DELt2
      real*16 Wp, Wm

      beta = qsqrt(1.Q0 - 4.Q0 * me2 / s)

      Wp = W + 2.Q0 * me ! paper
      Wm = W - 2.Q0 * me ! paper

      DELt2 = beta * qsqrt((s-W2)*(s-Wp**2))
      t2min = - 0.5Q0 * (s - W2 - 2.Q0*me*W - 4.Q0*me2 * DELt2)
      t2max = me2* W2 * Wp**2 /(s * t2min )

      return
      end


c**********************************************************************
! Important: this functions actively use the COMMON variables
!            -- momenta, "volume factor" and Jacobian
      subroutine generate_t_usual(rrc)
      implicit none
      include 'common.ekhara.inc.for'

      real*8 rrc(0:6)     ! generated randoms
                          ! use rrc(1) to rrc(6) here only!
      real*16 
     &        Qkin, DEL1, a1, b1, c1, DELt1

      real*16 
     &        DEL, a, b, c, D3, D4, dln, Xcap1, Gkin3, Gkin4,
     &        Gkin1, D1, D6, D5, DEL4

      real*16  y1, y2

      real*16 
     &        cphi1UR, sphi1UR,  ! to be used before randomPHI rotation 
     &        cphi2UR, sphi2UR   ! to be used before randomPHI rotation 

      real*16 randomPHI

      real*16 
     &        sthX, cthX    ! not affected by randomPHI rotation 

      real*16 cth1, sth1    ! angles of the final positron
      real*16 cth2, sth2    ! angles of the final elektron

      real*16 NZero         ! "Numerical zero" - quantity small "enough"

      real*16 beta2, tcrit, s2u, s2l, theta, aux, ! <-\__ for cuts on t1 
     &        s2hat, t1hat, s2z, t1low, t1upp     ! <-/

      real*16 mirror_z_axis_state
      
      CHARACTER msg*78      ! for logging

      real*16 lambdaf,x,y,z
      lambdaf(x,y,z) = (x - y - z)**2  - 4.Q0*y*z

      NZero = 1.Q-8         ! "Numerical zero" - quantity small "enough"

!      call warnlog('=========== new event =========== ')
      ! -----------------------------------------------------
      ! 1st random         = t2 =
      ! Notice: kinematic limits due to cuts on t2 
      !         are implemented in subroutine t_init_correct

      it2 = t2min * qexp(qlog(t2max/t2min)*rrc(1))
      
      y2 = qsqrt(1.Q0 - 4.Q0 * me2 / it2)

      Qkin = 4.Q0 * (s + it2 - 4.Q0*me2) / (1.Q0 + beta* y2) - it2 - W2
      DEL1 =  (Qkin + it2 - W2 + 4.Q0 * me * W)
     &      * (Qkin + it2 - W2 - 4.Q0 * me * W)
     &      * (Qkin**2 - 2.Q0 * Qkin * it2 + 2.Q0 * Qkin * W2 
     &           + it2**2 + W2**2 - 16.Q0 * me2 * it2 - 2.Q0 * W2 * it2)

      a1 = 2.Q0 * (Qkin + it2 +2.Q0 * me2 + W2)
      b1 = Qkin**2 - W**4 + 2.Q0 * W2 * it2 - it2**2 
     &       - 8.Q0 * me2 * it2 - 8.Q0 * me2 * W2
      c1 = 4.Q0 * me2 * (W2 - it2)**2

!     --------------- kinematic limits due to cuts [ t1 ]---------------
      beta2 = 1.Q0 - 4.Q0*me2/s
      tcrit = 2.Q0*me2 - me*ss
      
      ! calculate t1max accordingly to CUT_th1min and E1 cuts
      s2u   = (ss - me)**2
      s2l   = (W  + me)**2
      s2u   = qmin1( s2u, me2 + s*(1.Q0 - 2.Q0*CUT_E1min/ss) )
      s2l   = qmax1( s2l, me2 + s*(1.Q0 - 2.Q0*CUT_E1max/ss) )

      If(s2l.ge.s2u) then 
          call errlog('No phase space: s2l .ge. s2u')
          call prg_abort
      Endif

      theta = CUT_th1min*Pi/180.Q0

      aux  = 4.Q0*me2/s + beta2*qsin(theta)**2

      s2hat = me2 + s*beta2*qsin(theta)**2/
     &            (aux*(1.Q0 + 2.Q0*me/qsqrt(s*aux)))
      t1hat = 2.Q0*me2-me*ss*qsqrt(4.Q0*me2/s+beta2*qsin(theta)**2)

      If( s2hat.ge.s2u ) then
         s2z    = s2u
         If(CUT_E1min.le.me) then
            aux = 0.Q0
            t1upp = tcrit
         Else
            aux  = lambdaf(s,s2z,me2)
            t1upp =(3.Q0*me2-s+s2z-qcos(theta)*beta*qsqrt(aux))/2.Q0
         Endif
         t1upp = (me2*(s2z-me2)**2/s+beta2*aux*qsin(theta)**2/4.Q0)
     &            / t1upp
      Elseif( s2hat.le.s2l ) then
         s2z    = s2l
         aux  = lambdaf(s,s2z,me2)
         t1upp = (3.Q0*me2-s+s2z-qcos(theta)*beta*qsqrt(aux))/2.Q0
         t1upp =(me2*(s2z-me2)**2/s+beta2*aux*qsin(theta)**2/4.Q0)
     &            / t1upp
      Else
         t1upp = t1hat
      Endif

      ! calculate t1min accordingly to CUT_th1max and E1 cuts
      theta = CUT_th1max*Pi/180.Q0

      s2z    = s2l
      aux  = lambdaf(s,s2z,me2)
      t1min = (3.Q0*me2-s+s2z+qcos(theta)*beta*qsqrt(aux))/2.Q0
      s2z    = s2u
      If(CUT_E1min.le.me) then
            aux = 0.Q0
            t1low = tcrit
      Else
            aux  = lambdaf(s,s2z,me2)
            t1low =(3.Q0*me2-s+s2z+qcos(theta)*beta*qsqrt(aux))/2.Q0
      Endif
      t1low = qmin1(t1min,t1low)

      DELt1 = qsqrt(DEL1)/a1
      t1min = -0.5Q0 * (b1/a1 + DELt1)
      t1max = c1/(a1 * t1min)

      t1min = qmax1(t1min,t1low)
      t1max = qmin1(t1max,t1upp)
!     ------------------------------------------------------------------

!     --------------- kinematic limits due to USER cuts DIRECTLY on t1 -
      t1min = qmax1(t1min,CUT_t1min)
      t1max = qmin1(t1max,CUT_t1max)
!     ------------------------------------------------------------------

!      write(*,*) 't2max = ', t2max
!      write(*,*) 't2min = ', t2min
!      stop

      if ( (t1max.le.t1min) .or. (t2max.le.t2min) ) then
          JacobianFactor = 0.Q0
          Dab = 0.Q0
          OutFlag = .true.
c          msg = ' '
c          write(msg,*) 'OutFlag : ',
c     &                 '(t1max.le.t1min) .or. (t2max.le.t2min)'
c          call warnlog(msg)
c          call reportevent_1pi_outflag
          return
      endif

      ! -----------------------------------------------------
      ! 2nd random         = t1 =
      it1 = t1min * qexp(qlog(t1max/t1min)*rrc(2))

      y1 = qsqrt(1.Q0 - 4.Q0 * me2 / it1)

      nuKIN = 0.5Q0 * (W2 - it1 - it2)

      KW = qsqrt(nuKIN**2 - it1*it2)
      dln=qlog(s*(1.Q0+beta)**2/((nuKIN + KW)*(1.Q0 + y1)*(1.Q0+y2)))

      ! -----------------------------------------------------
      ! 3rd random         = s1 =
      Xcap1 = (nuKIN + KW)*(1.Q0 + y1)*qexp(dln*rrc(3))

      s1 = 0.5Q0 * Xcap1 + me2 + it2 + 2.Q0*me2 * it2 / Xcap1

c-----------------------------------------------------------------------
      a = lambdaf(s1, it2, me2)

      b = -2.Q0 * s * me2 * it1 - 2.Q0 * me2 * s1**2+8.Q0 * it2 * me2**2
     &    -2.Q0 * me2 * it2**2 - 2.Q0 * s * s1 * W2+2.Q0 * me2 * s * W2
     &   + 2.Q0 * it1 * s * s1 + 2.Q0 * s * it2 * s1+4.Q0 * me2*s1 * W2
     &   + 4.Q0 * me2**2 * s1
     &   + 2.Q0 * it1*it2*s - 2.Q0 * it2 * me2 * it1-2.Q0 * it2**2 * s 
     &   - 2.Q0 * me2*it2 *s + 2.Q0 *it1 * it2 * s1-4.Q0 * me2**2 *W2
     &   - 2.Q0 * it1 * s1**2 + 2.Q0 * s * it2 * W2 - 2.Q0 * me2**3 
     &   + 2.Q0 * me2**2 * it1

      c = - 2.Q0 * s * me2**2 * W2 - 2.Q0 * it1**2 * me2 * s1 
     &    - 2.Q0 * it1 * it2 * s**2 + 2.Q0 * s * it1 * it2 * s1
     &    - 2.Q0 * s * it1**2 * s1 + it1**2 * s**2 + it1**2 * s1**2 
     &    + it2**2 * s**2 + me2**2 * s1**2 + me2**2 * it1**2
     & - 6.Q0 * me2**3 * it1 - 2.Q0 * me2**3 * s1 - 4.Q0 *me2**2*s1*W2
     &    + 2.Q0 * me2**2 * it2 * s + 2.Q0 * me2**2 * it2 * s1 
     &    + 8.Q0 * me2**2 * it1 * s1 - 2.Q0 * s**2 * it2 * W2 
     &    - 2.Q0 * it1 * s**2 * W2 - 2.Q0 * me2 * it1 * s1**2
     & + me2**4 - 2.Q0 * me2 * s * it2 * s1 + 4.Q0 * me2**3 * W2 
     &    + me2**2 * it2**2 + 4.Q0 * me2 * it1 * it2 * s 
     &    - 2.Q0 * me2 * it1 * it2 * s1 - 6.Q0 * me2**3 * it2+s**2*W2**2
     &    + 6.Q0 * me2 * s * it2 * W2
     & - 4.Q0 * s * me2 * W2**2 - 2.Q0 * s * me2 * it1**2 
     &    + 2.Q0 * s * it1 * me2**2 - 2.Q0 * s * me2 * it2**2 
     &    + 2.Q0 * it1 * it2 * me2**2 + 2.Q0 * s * me2 * s1 * W2
     &    - 4.Q0 * s * it1 * it2 * W2 - 2.Q0 * s * it1 * me2 * s1
     & + 6.Q0 * s * it1 * me2 * W2 + 2.Q0 * s * it1 * s1 * W2

      Gkin3 = me2 * s1**2 - 2.Q0 * me2**2 * s1 - s* it2 * s1 
     &        - 3.Q0 * me2 * it2 * s + me2**3 + it2 * s**2 + it2**2 * s
      Gkin4 = -2.Q0 * it1 * me2 * s1 - it2 * me2 * it1 + me2**2 * it1 
     &         - me2 * W2 * it1 + me2 * it2**2 + it2 * W2 * it1 
     &         - it1 * s1 * W2
     &         -2.Q0 * me2 * it2 * W2 + me2 * W2**2 + it1 * s1**2 
     &         + it1**2 * s1 - it1 * it2 * s1

      DEL = 16.Q0 * Gkin3 * Gkin4
c-----------------------------------------------------------------------


      if (DEL .le. 0.Q0) then
           JacobianFactor = 0.Q0
           Dab = 0.Q0
           OutFlag = .true.
           msg = ' '
           write(msg,*) 'OutFlag : ',
     &                 '(DEL .le. 0.Q0)'
           call warnlog(msg)
           call reportevent_1pi_outflag
           return
      endif

      ! -----------------------------------------------------
      ! 4th random         = s2 =
      s2 = 0.5Q0 / a * (- b + qsqrt(DEL)*qsin( Pi*(rrc(4)-0.5Q0) ))

      ! -----------------------------------------------------
      ! Phase Space diff. Volume
      PhaseVol = 4.Q0 * Pi**2/ (s*beta) 
     &                 * (- qlog(t2max/t2min) )
     &                 * 1.Q0/(pi**5 * 2.Q0**9)

      ! Jacobian Factor: t-channel
      Dab      = it1 * it2
      JacobianFactor = - qlog(t1max/t1min) * dln

      ! -----------------------------------------------------
      ! Now calculate the particle momenta in LAB frame

      ! Energies
      q1(0) = 0.5Q0 * (s + me2 - s2)/ss
      q2(0) = 0.5Q0 * (s + me2 - s1)/ss
      qpion(0) = 0.5Q0 * (s1 + s2 - 2.Q0 * me2)/ss

      ! Abs values of 3-momenta
      q1abs = qsqrt(q1(0)**2 - me2)
      q2abs = qsqrt(q2(0)**2 - me2)

      qpionabs = qsqrt(qpion(0)**2 - W2)

c-----------------------------------------------------------------------
      D3 = -0.25Q0 * Gkin3

      Gkin1 = me2 * s2**2 - 2.Q0 * me2**2 * s2 - s* it1 * s2 
     &        - 3.Q0 * me2 * it1 * s + me2**3 + it1 * s**2 + it1**2 * s
      D1 = -0.25Q0 * Gkin1
      D6 = s/8.Q0 * ( - (s-4.Q0*me2)*(W2-it1-it2)
     &                + (s1-it2-me2)*(s2-it1-me2) + it1*it2 )
     &    -me2/4.Q0 * (s1-me2) * (s2-me2)
      D5 = D1 + D3 + 2.Q0 * D6

c-----------------------------------------------------------------------

      ! Polar angles (thetas)
      sthX = 2.Q0 * qsqrt(D5) / (s * beta * qpionabs)
      sth1 = 2.Q0 * qsqrt(D1) / (s * beta * q1abs)
      sth2 = 2.Q0 * qsqrt(D3) / (s * beta * q2abs)

      cthX = - (s2 - s1 + 2.Q0*(it2-it1))/(2.Q0*beta*ss*qpionabs)

      ! the following cosines are numerically unstable,
      ! use the below expressions only to fix the sign!
      cth1 = (s - s2 + 2.Q0*it1 - 3.Q0*me2)/(2.Q0*beta*ss*q1abs)
      cth2 = (s - s1 + 2.Q0*it2 - 3.Q0*me2)/(2.Q0*beta*ss*q2abs)
      ! namely:
      if (cth1.gt.0.Q0) then
         cth1 = qsqrt(1.Q0 - sth1**2)
      else
         cth1 = - qsqrt(1.Q0 - sth1**2)
      endif
      if (cth2.gt.0.Q0) then
         cth2 = qsqrt(1.Q0 - sth2**2)
      else
         cth2 = - qsqrt(1.Q0 - sth2**2)
      endif

c-----------------------------------------------------------------------
      ! Azimutal angles (phi)
      DEL4 = - DEL * qsin( pi * rrc(4) )**2 / (64.Q0 * a)

      cphi1UR = (D1 + D6)/qsqrt(D1 * D5)
      cphi2UR = (D3 + D6)/qsqrt(D3 * D5)

      sphi1UR = 2.Q0* qsqrt(-DEL4)
     &          /qabs( s * beta *qpionabs * sthX * q1abs * sth1 ) ! can not be < 0, due to qabs
      ! (qabs added by S.I.)
      sphi2UR = 2.Q0* qsqrt(-DEL4)
     &          /qabs( s * beta *qpionabs * sthX * q2abs * sth2 ) ! can not be < 0, due to qabs
      ! (qabs added by S.I.)
c-----------------------------------------------------------------------

      call chkABS1('sth1      ',sth1)
      call chkABS1('sth2      ',sth2)
      call chkABS1('sthX      ',sthX)
      call chkABS1('cth1      ',cth1)
      call chkABS1('cth2      ',cth2)
      call chkABS1('cthX      ',cthX)
      call chkABS1('cphi1UR   ',cphi1UR)
      call chkABS1('cphi2UR   ',cphi2UR)
      call chkABS1('sphi1UR   ',sphi1UR)
      call chkABS1('sphi2UR   ',sphi2UR)

c-----------------------------------------------------------------------
      ! Now we need extra numerical control

      if (D1.le.0.Q0)  then 
          sth1 = 0.Q0
          JacobianFactor = 0.Q0
          Dab = 0.Q0
          OutFlag = .true.
          msg = ' '
          write(msg,*) 'OutFlag : ',
     &                 ' D1 .le. 0.Q0, D1 = ', D1
          call warnlog(msg)
          call reportevent_1pi_outflag
          return
      elseif (sth1.gt.1.Q0)  then
          JacobianFactor = 0.Q0
          Dab = 0.Q0
          OutFlag = .true.
          msg = ' '
          write(msg,*) 'OutFlag : ',
     &                 ' (sth1.gt.1.Q0) '
          call warnlog(msg)
          call reportevent_1pi_outflag
          return
      endif

      if (D3.le.0.Q0)  then 
          sth2 = 0.Q0
          JacobianFactor = 0.Q0
          Dab = 0.Q0
          OutFlag = .true.
          msg = ' '
          write(msg,*) 'OutFlag : ',
     &                 ' D3 .le. 0.Q0, D3 = ', D3
          call warnlog(msg)
          call reportevent_1pi_outflag
          return
      elseif (sth2.gt.1.Q0)  then
          JacobianFactor = 0.Q0
          Dab = 0.Q0
          OutFlag = .true.
          msg = ' '
          write(msg,*) 'OutFlag : ',
     &                 ' (sth2.gt.1.Q0) '
          call warnlog(msg)
          call reportevent_1pi_outflag
          return
      endif

      if (D5.le.0.Q0)  then 
          sthX = 0.Q0
          JacobianFactor = 0.Q0
          Dab = 0.Q0
          OutFlag = .true.
          msg = ' '
          write(msg,*) 'OutFlag : ',
     &                 ' D5 .le. 0.Q0, D5 = ', D5
          call warnlog(msg)
          call reportevent_1pi_outflag
          return
      elseif (sthX.gt.1.Q0)  then
          sthX = 1.Q0
          JacobianFactor = 0.Q0
          Dab = 0.Q0
          OutFlag = .true.
          msg = ' '
          write(msg,*) 'OutFlag : ',
     &                 ' (sthX.gt.1.Q0) '
          call warnlog(msg)
          call reportevent_1pi_outflag
          return
      endif
      if ((sphi1UR.lt.-1.Q0).or. (sphi1UR.gt.1.Q0) ) then 
          sphi1UR = 1.Q0
          JacobianFactor = 0.Q0
          Dab = 0.Q0
          OutFlag = .true.
          msg = ' '
          write(msg,*) 'OutFlag : ',
     &                 ' (sphi1UR.lt.-1.Q0) .or. (sphi1UR.gt.1.Q0) '
          call warnlog(msg)
          call reportevent_1pi_outflag
          return
      endif
      if( (sphi2UR.lt.-1.Q0).or. ( sphi2UR.gt.1.Q0 )) then 
          sphi2UR = 1.Q0
          JacobianFactor = 0.Q0
          Dab = 0.Q0
          OutFlag = .true.
          msg = ' '
          write(msg,*) 'OutFlag : ',
     &                 ' (sphi2UR.lt.-1.Q0) .or. (sphi2UR.gt.1.Q0) '
          call warnlog(msg)
          call reportevent_1pi_outflag
          return
      endif
      if ( (cphi2UR.lt.-1.Q0) .or. (cphi2UR.gt.1.Q0) ) then
           JacobianFactor = 0.Q0
           Dab = 0.Q0
           OutFlag = .true.
           msg = ' '
           write(msg,*) 'OutFlag : ',
     &                 ' (cphi2UR.lt.-1.Q0) .or. (cphi2UR.gt.1.Q0) '
           call warnlog(msg)
           call reportevent_1pi_outflag
           return
       endif
      if ( (cphi1UR.lt.-1.Q0) .or. (cphi1UR.gt.1.Q0) ) then
           JacobianFactor = 0.Q0
           Dab = 0.Q0
           OutFlag = .true.
           msg = ' '
           write(msg,*) 'OutFlag : ',
     &                 ' (cphi1UR.lt.-1.Q0) .or. (cphi1UR.gt.1.Q0) '
           call warnlog(msg)
           call reportevent_1pi_outflag
       return
       endif

c-----------------------------------------------------------------------
      ! Build up un-rotated momenta

      ! We do the symmetrization of the phase space generator,
      ! if selected by user:

      if ((DoSymmetrize .eq. 0) .or. (rrc(6) .ge. 0.5D0)) then
            q1(1) = - q1abs * sth1 * cphi1UR
            q1(2) = - q1abs * sth1 * sphi1UR
            q1(3) =   q1abs * cth1 ! <- mind the sign!

            q2(1) = - q2abs * sth2 * cphi2UR
            q2(2) =   q2abs * sth2 * sphi2UR
            q2(3) = - q2abs * cth2 ! <- mind the sign!

            qpion(1) =   qpionabs * sthX 
            qpion(2) =   0.Q0 
            qpion(3) = - qpionabs * cthX ! <- mind the sign!

            t_taged = it1
      else
           
            q2(1) = - q1abs * sth1 * cphi1UR
            q2(2) = - q1abs * sth1 * sphi1UR
            q2(3) = - q1abs * cth1 ! <- mind the sign!

            q1(1) = - q2abs * sth2 * cphi2UR
            q1(2) =   q2abs * sth2 * sphi2UR
            q1(3) =   q2abs * cth2 ! <- mind the sign!

            qpion(1) =   qpionabs * sthX 
            qpion(2) =   0.Q0 
            qpion(3) =   qpionabs * cthX ! <- mind the sign!

            aux   = q2(0)
            q2(0) = q1(0)
            q1(0) = aux
            
            aux = q2abs
            q2abs = q1abs
            q1abs = aux
            
            aux = it1
            it1 = it2
            it2 = aux
            
            aux = s1
            s1 = s2
            s2 = aux
            
            t_taged = it2
      endif
      
      ! -----------------------------------------------------
      ! 5th random         = randomPHI =
      randomPHI = rrc(5) * 2.Q0 * pi

      !    Do the final random azimutal rotation 
      call rotate3d(q1, randomPHI)
      call rotate3d(q2, randomPHI)
      call rotate3d(qpion, randomPHI)

      ! -----------------------------------------------------
      ! k = q1 + q2 = (k0, - \vec(qpion))
      k(0) = q1(0) + q2(0)
      k(1) = - qpion(1)
      k(2) = - qpion(2)
      k(3) = - qpion(3)

      kk   = k(0)**2 - k(1)**2 - k(2)**2 - k(3)**2

      ! -----------------------------------------------------
      !  l1 and l2 label the virtual-photon momenta which appear in 
      !   the formula for the "t-channel" amplitude
      !            l1 = p1 - q1
      !            l2 = p2 - q2
      l1(0) = p1(0) - q1(0)
      l1(1) = p1(1) - q1(1)
      l1(2) = p1(2) - q1(2)
      l1(3) = p1(3) - q1(3)

      l2(0) = p2(0) - q2(0)
      l2(1) = p2(1) - q2(1)
      l2(2) = p2(2) - q2(2)
      l2(3) = p2(3) - q2(3)
      return
      end

c**********************************************************************
!     Auxiliary routine to perform a 3-dimentional azimutal (phi) 
!     rotation wrt z-axis of 4-vectors.

      subroutine rotate3d(mom4d, angl)
      implicit none
      real*16 mom4d(0:3), momTEMP(1:2)
      real*16 angl

      momTEMP(1) = mom4d(1)
      momTEMP(2) = mom4d(2)

      mom4d(1) =   momTEMP(1)* qcos(angl) + momTEMP(2)* qsin(angl)
      mom4d(2) = - momTEMP(1)* qsin(angl) + momTEMP(2)* qcos(angl)

      return
      end

c**********************************************************************
!     Auxiliary routine to check whether the abs of argument is 
!     within [0,1] range. 
!     designed to contrlos sines, cosines...

      subroutine chkABS1(text, value)
      implicit none
      CHARACTER msg*78         ! for logging
      real*16 value
      Character*10 text
      if ((qabs(value).lt.0.Q0) .or. (qabs(value).gt.1.Q0)) then
            msg = ' '
            write (msg,*)'[ ! ]', text, value
            call warnlog(msg)
      endif
      return
      end

c**********************************************************************
! xcrit = cos^2 (beta): D(x,y)=0
      real*16 function xcrit(y)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 y ! Energy
           xcrit = -(4.Q0*s*y**2 - 4.Q0*me2*y**2 
     &         - 4.Q0*s*ss*y+4.Q0*W**2*ss*y
     &         + s**2-2.Q0*W2 * s
     &         + W2**2 - 4.Q0*me2*W2+4.Q0*me2**2)
     &          /(me2*(y-me)*(y+me))/4.Q0
      return
      end

c**********************************************************************
      subroutine init_in_momenta
      implicit none
      include 'common.ekhara.inc.for'
      p1(0) = ss/2.Q0
      p1(1) = 0.Q0
      p1(2) = 0.Q0
c ---   z axis along p1 !!!
      p1(3) = qsqrt((ss+2.Q0*me)*(ss-2.Q0*me))/2.Q0

      p2(0) = ss/2.Q0
      p2(1) = 0.Q0
      p2(2) = 0.Q0
      p2(3) = -p1(3)

      return
      end 

c**********************************************************************
! May cause problems, if arguments are negative !!
      real*16 function lambdaf(awr, bwr, cwr) 
      implicit none
      real*16 awr, bwr, cwr
      lambdaf = (awr - (qsqrt(bwr) + qsqrt(cwr))**2) 
     &          * (awr - (qsqrt(bwr) - qsqrt(cwr))**2)
      return
      end

c**********************************************************************
! "Safe" formula for lambda-function, though 
! may be numerically unstable
      real*16 function lambdaG(a, b, c)
      implicit none
      real*16 a, b, c
      lambdaG = (a - b - c)**2  - 4.Q0*b*c
      return
      end

c**********************************************************************
      real*16 function dlips2(a,b,c)
      implicit none
      include 'common.ekhara.inc.for'

      real*16 a,b,c,x,y

      x=1-(qsqrt(b)+qsqrt(c))**2/a
      y=1-(qsqrt(b)-qsqrt(c))**2/a

      dlips2=1/(32.Q0*pi**2)*qsqrt(x*y)
      return
      end

c-------------------------------------------------------------
c  List of generated kinematic variables
c
c         qpion(0..3)  final pion
c         q1(0..3)     final positron
c         q2(0..3)     final electron
c
c         k(0:3)       final electron-positron pair
c                      = "s-channel" virtual photon
c
c         kk           "s-channel" virtual photon virtuality
c
c         l1(0..3)     l1 = p1 - q1, "t-channel" virtual photon
c         l2(0..3)     l2 = p2 - q2, "t-channel" virtual photon
c
c         it1, it2       "t-channel" virtual photon virtuality
c
c-------------------------------------------------------------

c-----------------------------------------------------------------------
c     8>< ---          End of Ph.Sp. generators               --- ><8
c-----------------------------------------------------------------------
