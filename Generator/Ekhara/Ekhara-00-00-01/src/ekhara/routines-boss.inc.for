c--------------------------------------------------------------------------c
c floating point parameters handeled by BOSS jobOption file are transfered c
c and converted to quadruple precision. Further parameters handeled by     c
c input.dat, card_1pi.dat, and card_2pi.dat are hardcoded here             c
c--------------------------------------------------------------------------c
      subroutine boss_init_read(xpar)
      implicit none
      include 'common.ekhara.inc.for'
      double precision xpar(0:19)

      call EKHARA_SET_ONE_EVENT_MODE
      call EKHARA_SET_SILENT

      WriteEvents = 0       ! 1 to write events, other - not to write to the file
      NeedHisto = 0         ! 1 to write histograms, 0 - to ignore histogramming-option for "2pi" mode
      if (sw_silent .eq. 1) then
            NeedHisto = 0
            WriteEvents = 0
      endif
      DoHisto = NeedHisto   ! <- option for "1pi" mode

      SeedMode = 1          ! Use constant seed =1, 
                            ! use variable (consequently) seed =2


      ngUpperBound = xpar(1)*1.Q0     ! number of iterations to find upper bound
      DoSymmetrize = 0                ! symmetrize the PhSp generation wrt (e+ <-> e-)
      PhSpOnly = 0                    ! (0)=normal, (1)=calculate the phase space volume only
      ignoreUB = 0.Q0                 ! handle UB underestim <0> = halt, <positive> = use new UB if overshoot, <negative> = ignore 
      UBEnlargeFactor = 1.2Q0         ! Enlarge factor for upper bound

      CUT_t1max = 0.Q0                ! [GeV^2, .le. 0.0]
      CUT_t1min = -200.Q0             ! [GeV^2, .le. 0.0]
      CUT_t2max = 0.Q0                ! [GeV^2, .le. 0.0]
      CUT_t2min = -200.Q0             ! [GeV^2, .le. 0.0]
       
      Fpi = 0.0924Q0                  ! [GeV] pion decay constant (in the chiral limit)


      !---- BEGIN READING THE DATACARD

      if (channel_id .eq. 1) then  !  parameters and settings for 2pi mode
      !----BEGIN INITIALIZE DEFAULT VALUES OF USER-DEFINED PARAMS
         ss            = 3.773Q0
         pi1lab_min    = 50.Q0
         pi1lab_max    = 130.Q0
         pipicut1_min  = 15.Q0
         pipicut1_max  = 165.Q0
      !---- END INITIALIZE DEFAULT VALUES OF USER-DEFINED PARAMS
         ss            = xpar(0)*1.Q0 ! CMS-Energy
         pi1lab_min    = xpar(14)*1.Q0   ! pion angle cut
         pi1lab_max    = xpar(15)*1.Q0 
         pipicut1_min  = xpar(16)*1.Q0   ! missing momomentum direction cut                        
         pipicut1_max  = xpar(17)*1.Q0

      elseif ((channel_id .eq. 2) .or.  ! pi0
     &    (channel_id .eq. 3) .or.  ! eta
     &    (channel_id .eq. 4)) then ! eta'

      !---- BEGIN INITIALIZE DEFAULT VALUES OF USER-DEFINED PARAMS
          ss            = 3.773Q0
          CUT_E1min     = 0.Q0
          CUT_E1max     = 200.Q5
          CUT_th1min    = 0.Q0
          CUT_th1max    = 180.Q0
          CUT_E2min     = 0.Q0
          CUT_E2max     = 200.Q5
          CUT_th2min    = 0.Q0
          CUT_th2max    = 180.Q0
          CUT_Epionmin  = 0.Q0
          CUT_Epionmax  = 200.Q5
          CUT_thpionmin = 0.Q0
          CUT_thpionmax = 180.Q0
      !---- END INITIALIZE DEFAULT VALUES OF USER-DEFINED PARAMS
          ss            = xpar(0)*1.Q0
          CUT_E1min     = xpar(4)*1.Q0
          CUT_E1max     = xpar(5)*1.Q0
          CUT_th1min    = xpar(2)*1.Q0
          CUT_th1max    = xpar(3)*1.Q0
          CUT_E2min     = xpar(8)*1.Q0
          CUT_E2max     = xpar(9)*1.Q0
          CUT_th2min    = xpar(6)*1.Q0
          CUT_th2max    = xpar(7)*1.Q0
          CUT_Epionmin  = xpar(12)*1.Q0
          CUT_Epionmax  = xpar(13)*1.Q0
          CUT_thpionmin = xpar(10)*1.Q0
          CUT_thpionmax = xpar(11)*1.Q0
       endif
       s = ss*ss

       tagangle = xpar(18)*1.Q0
       tagqsqr  = xpar(19)*1.Q0

      ! which routine is used for phase space.
      WorkingGeneratorId = sw_1pi

      change_id_s = 4 ! option for the change of variables in s-channel
                      ! mapping <2> flat, <3> log^3,   <4> log (most efficient!)
      change_id_g = 3 ! option for the change of variables in t-channel
                      ! mapping <2> flat, <3> t1 t2
 


      ! load General parameters (all modes)
      alpha = 137.03599976Q0 ! 1/alpha (QED)
      alpha = 1.Q0/alpha     !

      me = 0.51099906Q-3     ! [GeV] Electron mass
      mpi = 0.13957018Q0     ! [GeV] Charged pion mass ! PDG 08
      mpi0 = 0.1349766Q0     ! [GeV] Neutral pion mass ! PDG 08
      meta = 0.54785Q0       ! [GeV] Eta meson mass    ! PDG 08, 10
      metaP = 0.95778Q0      ! [GeV] Eta' meson mass   ! PDG 10

c-----------------------------------------------------------------------
c     RxT parameters for the form factor  ( e+e- ->  e+e- pi,eta,eta' )

      Fchir = Fpi             ![GeV]
      Fk    = 1.22Q0 * Fchir  ![GeV]
      F8 = 1.26Q0 * Fchir     ![GeV]
      F0 = 1.17Q0 * Fchir     ![GeV]

      Mrho_c = 0.7755Q0          ![GeV]
      Momega_c = 0.78265Q0       ![GeV]
      Mphi_c = 1.019456Q0        ![GeV]

      Mrho_pr   = 1.465Q0 ![GeV]
      Momega_pr = 1.42Q0  ![GeV]
      Mphi_pr   = 1.680Q0 ![GeV]
 
c      Mko  = 0.49765Q0      ![GeV]
      Mk   = 0.49368Q0      ![GeV]

      GammaTotalrho_c = 0.1494Q0
      GammaTotalomega_c = 0.00849Q0
      GammaTotalphi_c = 0.00426Q0

      GammaTotalrho_pr = 0.400Q0
      GammaTotalomega_pr = 0.215Q0
      GammaTotalphi_pr = 0.150Q0

      pi = 4.Q0*qatan(1.Q0)
      theta0 = -9.2Q0/180.0Q0*pi
      theta8 = -21.2Q0/180.0Q0*pi

      Cq_c = Fchir/(sqrt(3.Q0)*cos(theta8 - theta0)) *
     &     (cos(theta0)/F8 - sqrt(2.Q0)*sin(theta8)/F0) 
      Cs_c = Fchir/(sqrt(3.Q0)*cos(theta8 - theta0)) *
     &     (sqrt(2.Q0)*cos(theta0)/F8 + sin(theta8)/F0) 
      
      Cq_P = Fchir/(sqrt(3.Q0)*cos(theta8 - theta0)) *
     &     (sqrt(2.Q0)*cos(theta0)/F8 + sin(theta8)/F0) 
      Cs_P = Fchir/(sqrt(3.Q0)*cos(theta8 - theta0)) *
     &     (cos(theta0)/F8 - sqrt(2.Q0)*sin(theta8)/F0)

      EPSomph = 0.058Q0           !       0.058 = Our EPJ C49 p.699
      EPSprime = -0.0026Q0        ! phi-omega-pi0, 0.0026 = see drafts

c-----1 octet scheme--------
      Fv = 7.04Q-6*12.Q0*pi*Mrho_c
     &     /(16.Q0*pi**2/137.0359895Q0/137.0359895Q0)
      Fv = sqrt(Fv)

      Fsmv = Fv/Mrho_c
      Gv = Fv/2.Q0 ! HIGH ENERGY CONSTRAINT

      Hv = 3.Q0/32.Q0/sqrt(2.Q0)/pi/pi/Fsmv   !*1.03Q0 ??? ! HIGH ENERGY CONSTRAINT

      SIGv = 3.Q0/32.Q0/pi/pi/Fsmv**2 ! HIGH ENERGY CONSTRAINT
      

c-----2 octets scheme--------
      Fsmv1 = Fsmv       ! from width

      if (channel_id .ne. 1) then
         Hv1 = 3.120890585777768Q-2  ! from fit [Czyz, Ivashyn, Korchin, Shekhovtsova (2012): 3.120890585777768Q-2]
      endif

      SIGv1 = sqrt(2.Q0)*Hv1/Fsmv1 ! HIGH ENERGY CONSTRAINT

      Fsmv2Hv2 = (3.Q0/4.Q0/pi/pi -8.Q0*sqrt(2.Q0)*Hv1*Fsmv1)
     &          /8.Q0/sqrt(2.Q0)          ! HIGH ENERGY CONSTRAINT

      Fsmv2sqSIGv2 = sqrt(2.Q0)*Fsmv2Hv2  ! HIGH ENERGY CONSTRAINT
c-----------------------------------------------------------------------

      !-----constants---------------------------------------------
      me2 = me**2
      mpi2 = mpi**2
      mpi02 = mpi0**2
      meta2 = meta**2
      metaP2 = metaP**2

      gev2nbarn = .389379292Q6
      !-----
      t_taged = 0.Q0

      return
      end







c---------------------------------------------------------------------c
c screen dump of all variables originally read in via input cards:    c
c input.dat, card_1pi.dat, card_2pi.dat                               c
c---------------------------------------------------------------------c
      subroutine diagnose()
      implicit none
      include 'common.ekhara.inc.for'

      write(*,*)'nges',nges
      write(*,*)'channel_id',channel_id
      write(*,*)'WriteEvents',WriteEvents
      write(*,*)'NeedHisto',NeedHisto
      write(*,*)'sw_silent',sw_silent
      write(*,*)'SeedMode',SeedMode

C      call readparams_1pi
      write(*,*)'ngUpperBound',ngUpperBound
      write(*,*)'DoSymmetrize',DoSymmetrize
      write(*,*)'PhSpOnly',PhSpOnly
      write(*,*)'ignoreUB',ignoreUB
      write(*,*)'UBEnlargeFactor',UBEnlargeFactor
      write(*,*)'CUT_t1max',CUT_t1max
      write(*,*)'CUT_t1min',CUT_t1min
      write(*,*)'CUT_t2max',CUT_t2max
      write(*,*)'CUT_t2min',CUT_t2min
      write(*,*)'Fpi',Fpi

C      call read_card_1pi
      write(*,*)'CMS energy',ss
      write(*,*)'sw_1pi',sw_1pi
      write(*,*)'piggFFsw',piggFFsw
      write(*,*)'CUT_E1min',CUT_E1min
      write(*,*)'CUT_E1max',CUT_E1max
      write(*,*)'CUT_th1min',CUT_th1min
      write(*,*)'CUT_th1max',CUT_th1max
      write(*,*)'CUT_E2min',CUT_E2min
      write(*,*)'CUT_E2max',CUT_E2max
      write(*,*)'CUT_th2min',CUT_th2min
      write(*,*)'CUT_th2max',CUT_th2max
      write(*,*)'CUT_Epionmin',CUT_Epionmin
      write(*,*)'CUT_Epionmax',CUT_Epionmax
      write(*,*)'CUT_thpionmin',CUT_thpionmin
      write(*,*)'CUT_thpionmax',CUT_thpionmax
      write(*,*)'s',s
      write(*,*)'tagmode',tagmode
      write(*,*)'tagangel',tagangle

      write(*,*)'WorkingGeneratorId',WorkingGeneratorId
      write(*,*)'change_id_s',change_id_s
      write(*,*)'change_id_g',change_id_g

C      call readparams_2pi
      write(*,*)'sw_2pi',sw_2pi
      write(*,*)'pi1lab_min',pi1lab_min
      write(*,*)'pi1lab_max',pi1lab_max
      write(*,*)'pipicut1_min',pipicut1_min
      write(*,*)'pipicut1_max',pipicut1_max
      write(*,*)'alpha',alpha
      write(*,*)'me',me
      write(*,*)'mpi',mpi
      write(*,*)'mpi0',mpi0
      write(*,*)'meta',meta
      write(*,*)'metaP',metaP

      write(*,*)'Fchir',Fchir
      write(*,*)'metaP',metaP
      write(*,*)'Fk',Fk
      write(*,*)'F8',F8
      write(*,*)'F0',F0
      write(*,*)'Mrho_c',Mrho_c
      write(*,*)'Momega_c',Momega_c
      write(*,*)'Mphi_c',Mphi_c
      write(*,*)'Mrho_pr',Mrho_pr
      write(*,*)'Momega_pr',Momega_pr
      write(*,*)'Mphi_pr',Mphi_pr
      write(*,*)'Mk',Mk
      write(*,*)'GammaTotalrho_c',GammaTotalrho_c
      write(*,*)'GammaTotalomega_c',GammaTotalomega_c
      write(*,*)'GammaTotalphi_c',GammaTotalphi_c
      write(*,*)'GammaTotalrho_pr',GammaTotalrho_pr
      write(*,*)'GammaTotalomega_pr',GammaTotalomega_pr
      write(*,*)'GammaTotalphi_pr',GammaTotalphi_pr
      write(*,*)'pi',pi
      write(*,*)'theta0',theta0
      write(*,*)'theta8',theta8
      write(*,*)'Cq_c',Cq_c
      write(*,*)'Cs_c',Cs_c
      write(*,*)'Cq_P',Cq_P
      write(*,*)'Cs_P',Cs_P
      write(*,*)'EPSomph',EPSomph
      write(*,*)'EPSprime',EPSprime
      write(*,*)'Fv',Fv
      write(*,*)'Fsmv',Fsmv
      write(*,*)'Gv',Gv
      write(*,*)'Hv',Hv
      write(*,*)'SIGv',SIGv
      write(*,*)'Fsmv1',Fsmv1
      write(*,*)'Hv1',Hv1
      write(*,*)'SIGv1',SIGv1
      write(*,*)'Fsmv2Hv2',Fsmv2Hv2
      write(*,*)'Fsmv2sqSIGv2',Fsmv2sqSIGv2
      write(*,*)'gev2nbarn',gev2nbarn

      return
      end






c------------------------------------------------------------------c
c additional kinematic cuts to generate only a tag-specific event  c
c configuration. Tagging angle is applied symmetrically!           c
c------------------------------------------------------------------c
      subroutine taggingCutsBES(accepted)
      implicit none
      include 'common.ekhara.inc.for'
      logical accepted
      logical accepted_extra

      real*16 cth1, cth2, costag, deg
      real*16 motra1, motra2

      deg = 180.Q0/pi
      costag = cos(tagangle/deg)

      cth1 = q1(3)/q1abs         ! positron
      motra1 = -((p1(0)-q1(0))**2
     & - (p1(1)-q1(1))**2
     & - (p1(2)-q1(2))**2
     & - (p1(3)-q1(3))**2)

      cth2 = q2(3)/q2abs         ! electron
      motra2 = -((p2(0)-q2(0))**2
     & - (p2(1)-q2(1))**2
     & - (p2(2)-q2(2))**2
     & - (p2(3)-q2(3))**2)


      accepted_extra = .true.

      if(tagmode .eq. 1 .and.
     &   min(abs(cth1),abs(cth2)) .lt. costag) then
         accepted_extra = .false.
      endif

      if(tagmode .eq. 2 .and.
     &(min(abs(cth1),abs(cth2)) .gt. costag .or.
     & max(abs(cth1),abs(cth2)) .lt. costag)) then
         accepted_extra = .false.
      endif

      if(tagmode .eq. 3 .and.
     &   max(abs(cth1),abs(cth2)) .gt. costag) then
         accepted_extra = .false.
      endif

      if(tagmode .eq. 11 .and.
     &   max(motra1,motra2) .gt. tagqsqr) then
         accepted_extra = .false.
      endif

      if(tagmode .eq. 12 .and.
     &(max(motra1,motra2) .lt. tagqsqr .or.
     & min(motra1,motra2) .gt. tagqsqr)) then
         accepted_extra = .false.
      endif

      if(tagmode .eq. 13 .and.
     &   min(motra1,motra2) .lt. tagqsqr) then
         accepted_extra = .false.
      endif
      
      accepted = (accepted_extra)
      return
      end






c------------------------------------------------------------------c
c convert for momenta to double precision in order to export them  c
c to BOSS                                                          c
c------------------------------------------------------------------c
      subroutine get_momentum(p1p,p2p,q1p,q2p,pi1p,pi2p,qpionp)
      implicit none
      include 'common.ekhara.inc.for'
      double precision p1p(0:3),p2p(0:3),q1p(0:3),q2p(0:3)
      double precision pi1p(0:3),pi2p(0:3),qpionp(0:3)
      integer i

      do i=0,3

         p1p(i)=p1(i)*1.D0
         p2p(i)=p2(i)*1.D0
         q1p(i)=q1(i)*1.D0
         q2p(i)=q2(i)*1.D0
         pi1p(i)=pi1(i)*1.D0
         pi2p(i)=pi2(i)*1.D0
         qpionp(i)=qpion(i)*1.D0
       enddo

      return
      end




c-------------------------------------------------------------------c
c convert info for S/R mc_fin_1pi to double precision in order to   c
c export them to BOSS                                               c
c-------------------------------------------------------------------c
      subroutine get_final_meson_info(mesfinpar)
      implicit none
      include 'common.ekhara.inc.for'
      double precision mesfinpar(0:9)

      mesfinpar(0) = totIterations*1.D0
      mesfinpar(1) = MCTrialsCount*1.D0
      mesfinpar(2) = ContribMax_MCloop*1.D0
      mesfinpar(3) = IntegralContrib*1.D0
      mesfinpar(4) = IntegralContrib2*1.D0
      mesfinpar(5) = IntegralContribSample*1.D0
      mesfinpar(6) = gev2nbarn*1.D0
      mesfinpar(7) = OSCounter*1.D0
      mesfinpar(8) = CS_OS*1.D0
      mesfinpar(9) = CSerr_OS*1.D0

      return
      end




c----------------------------------------------------------------------c
c convert CS info from S/R mc_fin_1pi to double precision in order to  c
c export them to BOSS                                                  c
c----------------------------------------------------------------------c
      subroutine get_final_twopi_info(twopifinpar)
      implicit none
      include 'common.ekhara.inc.for'

      real*16 Hsum,Hsum2,suma(0:9,0:99),suma2(0:9,0:99)
     &       ,ququmi(0:9,0:99),ququma(0:9,0:99)
      common/sums/Hsum,Hsum2,suma,suma2,ququmi,ququma

      double precision twopifinpar(0:1)

      twopifinpar(0) = Hsum*1.D0
      twopifinpar(1) = Hsum2*1.D0

      return
      end

