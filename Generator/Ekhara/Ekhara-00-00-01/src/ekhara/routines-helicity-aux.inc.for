c-----------------------------------------------------------------------
c
c     EKHARA. include file.
c     auxiliary routines for helicity-formalism
c
c-----------------------------------------------------------------------

c**********************************************************************
      subroutine init_ha
      implicit none
      include 'common.ekhara.inc.for'
      complex*32 I(2,2)
      integer i1,i2,i3

      I(1,1)=qcmplx(1.Q0,0.Q0)
      I(1,2)=qcmplx(0.Q0,0.Q0)
      I(2,1)=qcmplx(0.Q0,0.Q0)
      I(2,2)=qcmplx(1.Q0,0.Q0)

      do i1 = 1,2
       do i2 = 1,2
        sigpl(0,i1,i2) = I(i1,i2)
        sigmi(0,i1,i2) = I(i1,i2)
       enddo
      enddo

      do i1 = 1,2
       sigpl(1,i1,i1)=qcmplx(0.Q0,0.Q0)
       sigpl(2,i1,i1)=qcmplx(0.Q0,0.Q0)
       sigpl(3,i1,i1)=qcmplx((-1.Q0)**(i1+1),0.Q0)
      enddo

      sigpl(1,1,2)=qcmplx(1.Q0,0.Q0)
      sigpl(1,2,1)=qcmplx(1.Q0,0.Q0)
      sigpl(2,1,2)=qcmplx(0.Q0,-1.Q0)
      sigpl(2,2,1)=qcmplx(0.Q0,1.Q0)
      sigpl(3,1,2)=qcmplx(0.Q0,0.Q0)
      sigpl(3,2,1)=qcmplx(0.Q0,0.Q0)

      do i3 = 1,3
       do i1 = 1,2
        do i2 = 1,2
         sigmi(i3,i1,i2) = -sigpl(i3,i1,i2)
        enddo
       enddo
      enddo

      tau(0) = 2*p1(0)
      tau(1) = 0.Q0
      tau(2) = 0.Q0
      tau(3) = 0.Q0
      return
      end 

c**********************************************************************
      subroutine spinory_i
      implicit none
      include 'common.ekhara.inc.for'
      real*16   em,sq,pm
      integer ic1,ic2

c---- pozitron p = (E,0,0,p)
c---- elektron p = (E,0,0,-p)

      em = p1(0)
      pm = p1(3) 
      sq = qsqrt(em+pm)
c-------- helicity 1 == +

      vp1(1,1) = 0.Q0
      vp1(2,1) = - sq
      vp2(1,1) = 0.Q0
      vp2(2,1) = me/sq

      up1(1,1) = 0.Q0
      up1(2,1) =  -me/sq
      up2(1,1) = 0.Q0
      up2(2,1) = - sq

c-------- helicity 2 == -

      vp1(1,2) = me/sq
      vp1(2,2) = 0.Q0
      vp2(1,2) = -sq
      vp2(2,2) = 0.Q0

      up1(1,2) = sq
      up1(2,2) =  0.Q0
      up2(1,2) = me/sq
      up2(2,2) = 0.Q0

      return
      end

c**********************************************************************

      subroutine spinory_f
      implicit none
      include 'common.ekhara.inc.for'

      real*16 th1,th2,sphi1,cphi1,sphi2,cphi2,cth1d2,sth1d2,
     *       cth2d2,sth2d2,sq1,sq2,em1,em2,pm1,pm2,sth1,sth2
      complex*32  ex1,ex2
      integer ic1,ic2

      ! q = (Eq,|q|cosphi*sintheta,|q|sinphi*sintheta,|q|costheta)

        em1 = q1(0)
        em2 = q2(0)
        pm1 = qsqrt(q1(1)**2+q1(2)**2+q1(3)**2)
        pm2 = qsqrt(q2(1)**2+q2(2)**2+q2(3)**2)
        sq1 = qsqrt(em1+pm1)
        sq2 = qsqrt(em2+pm2)
        if ( (q1(3).gt.0.Q0).and.((q1(3)/pm1).gt.1.Q0) ) then
         write(6,'(1x,3(1pd22.15))') q1(3),pm1,(q1(3)/pm1)
         th1 = 0.Q0
        elseif( (q1(3).lt.0.Q0).and.((q1(3)/pm1).lt.(-1.Q0)) ) then
         write(6,'(1x,3(1pd22.15))') q1(3),pm1,(q1(3)/pm1)
         th1 = pi
         else
         th1 = qacos(q1(3)/pm1)
        endif
        cth1d2 = qcos(th1/2.Q0)   
        if ( (q2(3).gt.0.Q0).and.((q2(3)/pm2).gt.1.Q0) ) then
         write(6,'(1x,3(1pd22.15))') q2(3),pm2,(q2(3)/pm2)
         th2 = 0.Q0
        elseif( (q2(3).lt.0.Q0).and.((q2(3)/pm2).lt.(-1.Q0)) ) then
         write(6,'(1x,3(1pd22.15))') q2(3),pm2,(q2(3)/pm2)
         th2 = pi
         else
         th2 = qacos(q2(3)/pm2)
        endif
        cth2d2 = qcos(th2/2.Q0) 

         v1(2,1) = -sq1*cth1d2
         v2(2,1) = me/sq1*cth1d2

         v1(1,2) = me/sq1*cth1d2
         v2(1,2) = -sq1*cth1d2

         ub1(1,1) = me/sq2*cth2d2
         ub2(1,1) = sq2*cth2d2

         ub1(2,2) = sq2*cth2d2
         ub2(2,2) =  me/sq2*cth2d2


        if((th1.eq.0.Q0).or.(th1.eq.pi)) then
         v1(1,1)= 0.Q0
         v2(1,1)= 0.Q0
         v1(2,2)= 0.Q0
         v2(2,2)= 0.Q0
        else
        
         sth1  = qsin(th1)
         sth1d2= qsin(th1/2.Q0)
         cphi1 = q1(1)/pm1/sth1
         sphi1 = q1(2)/pm1/sth1
         ex1 = cphi1+qcmplx(0.Q0,1.Q0)*sphi1

c-------- helicity 1 == +

         v1(1,1) = sq1/ex1*sth1d2
         v2(1,1) = -me/sq1/ex1*sth1d2

c-------- helicity 2 == -
       
         v1(2,2) = me/sq1*ex1*sth1d2
         v2(2,2) = -sq1*ex1*sth1d2

       endif

       if((th2.eq.0.Q0).or.(th2.eq.pi)) then
         ub1(2,1)= 0.Q0
         ub2(2,1)= 0.Q0
         ub1(1,2)= 0.Q0
         ub2(1,2)= 0.Q0
        else

         sth2  = qsin(th2)
         sth2d2= qsin(th2/2.Q0)
         cphi2 = q2(1)/pm2/sth2
         sphi2 = q2(2)/pm2/sth2
         ex2 = cphi2+qcmplx(0.Q0,1.Q0)*sphi2

c-------- helicity 1 == +

         ub1(2,1) =  me/sq2/ex2*sth2d2
         ub2(2,1) = sq2/ex2*sth2d2

c-------- helicity 2 == -

         ub1(1,2) = -sq2*ex2*sth2d2
         ub2(1,2) = -me/sq2*ex2*sth2d2

        endif
        end

c**********************************************************************
      subroutine evalgammu !(taken from old ekhara)
      ! helicity-amplitude formula for the FINAL vertex
      ! photon* -> e+ e-
      ! s-channel
      implicit none
      include 'common.ekhara.inc.for'

      integer ic1,ic2

      ! gammu: (lorenz-vector index, 
      !         helicity-outgoing-positron,
      !         helicity-outgoing-electron)
      ! helicity: 1="+", 2="-"

       do ic1=1,2
       do ic2=1,2
        gammu(0,ic1,ic2) = (ub1(1,ic1)*v1(1,ic2)+ub1(2,ic1)*v1(2,ic2)
     &                    +ub2(1,ic1)*v2(1,ic2)+ub2(2,ic1)*v2(2,ic2))
        gammu(1,ic1,ic2) = (-ub1(1,ic1)*v1(2,ic2)-ub1(2,ic1)*v1(1,ic2)
     &                     +ub2(1,ic1)*v2(2,ic2)+ub2(2,ic1)*v2(1,ic2))
        gammu(2,ic1,ic2) = qcmplx(0.Q0,1.Q0)* 
     &                    (ub1(1,ic1)*v1(2,ic2)-ub1(2,ic1)*v1(1,ic2)
     &                    -ub2(1,ic1)*v2(2,ic2)+ub2(2,ic1)*v2(1,ic2))
        gammu(3,ic1,ic2) = (-ub1(1,ic1)*v1(1,ic2)+ub1(2,ic1)*v1(2,ic2)
     &                     +ub2(1,ic1)*v2(1,ic2)-ub2(2,ic1)*v2(2,ic2))
       enddo
       enddo
      end
c**********************************************************************
