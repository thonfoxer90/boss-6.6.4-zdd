c-----------------------------------------------------------------------
c
c     EKHARA. include file.
c     histograming routines.
c
c-----------------------------------------------------------------------
      subroutine hist_init_2pi(file_settings)
      !             file_settings: filename for histogram settings
      !                            see example settings file.
      implicit none
      character file_settings*(*)
      include 'common.ekhara.inc.for'

      if (NeedHisto.eq.0) then
            return 
      endif

      ! Open settings file
      open (70,file=file_settings,status='UNKNOWN')
      ! Skip the header line
      read (70,*)
      read (70,*)

      ! Read how many histograms we need
      read(70,*) lh
      lh=lh-1 ! to start from 0
      read (70,*)
      
      read(70,*) ag(0),bg(0),ng(0)
      read(70,*) ag(1),bg(1),ng(1)
      read(70,*) ag(2),bg(2),ng(2)
      read(70,*) ag(3),bg(3),ng(3)
      read(70,*) ag(4),bg(4),ng(4)
      read(70,*) ag(5),bg(5),ng(5)
      read(70,*) ag(6),bg(6),ng(6)
      read(70,*) ag(7),bg(7),ng(7)
      read(70,*) ag(8),bg(8),ng(8)
      read(70,*) ag(9),bg(9),ng(9)

      close (70)

      ! Reset all histograms to initial values
      call FLUSHhist
      return
      end


c**********************************************************************

      subroutine FLUSHhist
      implicit none
      include 'common.ekhara.inc.for'
      integer i,j
      real*16 Hsum,Hsum2,suma(0:9,0:99),suma2(0:9,0:99)
     &       ,ququmi(0:9,0:99),ququma(0:9,0:99)
      common/sums/Hsum,Hsum2,suma,suma2,ququmi,ququma
       
       Hsum=0.Q0 
       Hsum2=0.Q0
       do j=0,lh
       do i=0,ng(j)-1 
       suma(j,i) = 0.Q0
       suma2(j,i) = 0.Q0
       ququmi(j,i) =  ag(j)+(bg(j)-ag(j))/qfloat(ng(j))*qfloat(i)
       ququma(j,i) =  ag(j)+(bg(j)-ag(j))/qfloat(ng(j))
     &                *(qfloat(i)+1.Q0)
       enddo
       enddo
      return
      end

c**********************************************************************

      subroutine hist(ff,var)
      implicit none 
      include 'common.ekhara.inc.for'
      integer i,j
      real*16 var(0:9),ff,Hsum,Hsum2,suma(0:9,0:99),suma2(0:9,0:99)
     &       ,ququmi(0:9,0:99),ququma(0:9,0:99)
      common/sums/Hsum,Hsum2,suma,suma2,ququmi,ququma
c----
      if (NeedHisto.eq.0) then
         return 
      endif

       do j=0,lh
       do i=0,ng(j)-1
        if((var(j).ge.ququmi(j,i)).and.(var(j).lt.ququma(j,i))) then
           suma(j,i) = suma(j,i)+ff
           suma2(j,i) = suma2(j,i)+ff**2
        endif
       enddo
       enddo
c----
         Hsum = Hsum+ff
         Hsum2 = Hsum2+ff**2
c----
      return
      end

c**********************************************************************

      subroutine hist_fin_2pi
      implicit none
      include 'common.ekhara.inc.for'

      integer i,j
      real*16 integ(0:9,0:99),dinteg(0:9,0:99)
      real*16 Hsum,Hsum2,suma(0:9,0:99),suma2(0:9,0:99)
     &       ,ququmi(0:9,0:99),ququma(0:9,0:99)
      common/sums/Hsum,Hsum2,suma,suma2,ququmi,ququma

      if (NeedHisto.eq.0) then
         call runlog('subroutine hist_fin_2pi: histogramming ignored')
         return 
      endif

      open (88,file='output/histograms_control.out',status='UNKNOWN')
      ! 88 = histograms_control.out  === to be used for test-run scripts
      !                              === it contains ALL histograms

      call runlog('Saving histograms to <histograms_2pi.out>')
      open (8,file='output/histograms_2pi.out',status='UNKNOWN')

      do j = 0,lh
      write(8,*)
      write(88,*)
      do i = 0,ng(j)-1
      integ(j,i)  = suma(j,i)/(nges)
      dinteg(j,i)=sqrt((suma2(j,i)/(nges)-integ(j,i)**2)
     &             /(nges-1.Q0))
      integ(j,i) = integ(j,i)/(ququma(j,i)-ququmi(j,i))
      dinteg(j,i) = dinteg(j,i)/(ququma(j,i)-ququmi(j,i))

      write(8,950) dbleq(ag(j)+(bg(j)-ag(j))/qfloat(ng(j))
     &          *(qfloat(i)+0.5Q0)),' ',
     &            dbleq(integ(j,i)),' ',dbleq(dinteg(j,i))
      write(88,950) dbleq(ag(j)+(bg(j)-ag(j))/qfloat(ng(j))
     &          *(qfloat(i)+0.5Q0)),' ',
     &            dbleq(integ(j,i)),' ',dbleq(dinteg(j,i))
      enddo
      enddo
      close(8)

      close(88)

      return
  950 format(1pe17.10,A,1pe17.10,A,1pe17.10)
      end


      subroutine tellSIGMA_2pi
      implicit none
      include 'common.ekhara.inc.for'

      CHARACTER msg*78         ! for logging

      real*16 int,dint
      real*16 Hsum,Hsum2,suma(0:9,0:99),suma2(0:9,0:99)
     &       ,ququmi(0:9,0:99),ququma(0:9,0:99)
      common/sums/Hsum,Hsum2,suma,suma2,ququmi,ququma

      if (NeedHisto.eq.0) then
         call runlog('subroutine hist_fin_2pi: histogramming ignored')
         return 
      endif

c---- 
      int  = Hsum/(nges)
      dint = sqrt((Hsum2/(nges)-int**2)/(nges-1.Q0))
      call runlog(' ')
      call std_out(' ')
      msg = ' '
      write(msg,100)' sigma (nb) = ',int,' +- ',dint
      call runlog(msg)
      call std_out(msg)
 100  format(A,1pd12.5,A,1pd9.2)
c---- 
      return
      end
c-------------------------------------------------------------
c     8>< ---          End of file                     --- ><8
c-------------------------------------------------------------
