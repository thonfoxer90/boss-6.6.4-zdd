c-----------------------------------------------------------------------
c
c     EKHARA. include file.
c
c     Put User's functions here:
c         -- reading datacards
c         -- reporting of events
c         -- pi-gamma-gamma form factor
c         -- eta-gamma-gamma form factor
c         -- eta'-gamma-gamma form factor
c         -- filling the histograms
c
c-----------------------------------------------------------------------


c**********************************************************************
      subroutine read_card_1pi
      implicit none
      include 'common.ekhara.inc.for'
      !---- BEGIN READING THE DATACARD
      open(793,file='card_1pi.dat',status='old')
      read(793,*)
      read(793,*)
      read(793,*)
      read(793,*)

      ! Important: if you read a datacard in a different way,
      !            please make sure that ALL the below parameters
      !            are initialized with correct values

      !---- BEGIN INITIALIZE DEFAULT VALUES OF USER-DEFINED PARAMS
       ss            = 1.02Q0
       sw_1pi        = 2
       piggFFsw      = 5
       CUT_E1min     = 0.Q0
       CUT_E1max     = 200.Q5
       CUT_th1min    = 0.Q0
       CUT_th1max    = 180.Q0
       CUT_E2min     = 0.Q0
       CUT_E2max     = 200.Q5
       CUT_th2min    = 0.Q0
       CUT_th2max    = 180.Q0
       CUT_Epionmin  = 0.Q0
       CUT_Epionmax  = 200.Q5
       CUT_thpionmin = 0.Q0
       CUT_thpionmax = 180.Q0
      !---- END INITIALIZE DEFAULT VALUES OF USER-DEFINED PARAMS

      read(793,*)
     &            ss,         ! CMS-Energy 
     &            sw_1pi,     piggFFsw,
     &            CUT_E1min,  CUT_E1max,
     &            CUT_th1min, CUT_th1max,
     &            CUT_E2min,  CUT_E2max,
     &            CUT_th2min, CUT_th2max,
     &            CUT_Epionmin,  CUT_Epionmax,
     &            CUT_thpionmin, CUT_thpionmax
      close(793)
      !---- END READING THE DATACARD
      s = ss*ss

      if ((channel_id .eq. 2).and.(sw_1pi .eq. 1)) then       ! pi0, s-channel
        if ((piggFFsw .eq. 7) .or. (piggFFsw .eq. 8)) then    ! resonance model
        call runlog('Models 7 and 8 were not compared to data')
        call runlog('in the only-s-channel configuration.')
        call warnlog('Models 7 and 8 were not compared to data')
        call warnlog('in the only-s-channel configuration.')
        call errlog('Models 7 and 8 were not compared to data')
        call errlog('in the only-s-channel configuration.')
        call std_out('Models 7 and 8 were not compared to data')
        call std_out('in the only-s-channel configuration.')
        call prg_abort
        endif
      endif

      if ((channel_id .eq. 3).and.(sw_1pi .eq. 1)) then       ! eta, s-channel
        if ((piggFFsw .eq. 7) .or. (piggFFsw .eq. 8)) then    ! resonance model
        call runlog('Models 7 and 8 were not compared to data')
        call runlog('in the only-s-channel configuration.')
        call warnlog('Models 7 and 8 were not compared to data')
        call warnlog('in the only-s-channel configuration.')
        call errlog('Models 7 and 8 were not compared to data')
        call errlog('in the only-s-channel configuration.')
        call std_out('Models 7 and 8 were not compared to data')
        call std_out('in the only-s-channel configuration.')
        call prg_abort
        endif
      endif

      if ((channel_id .eq. 4).and.(sw_1pi .eq. 1)) then       ! eta', s-channel
        if ((piggFFsw .eq. 7) .or. (piggFFsw .eq. 8)) then    ! resonance model
        call runlog('Models 7 and 8 were not compared to data')
        call runlog('in the only-s-channel configuration.')
        call warnlog('Models 7 and 8 were not compared to data')
        call warnlog('in the only-s-channel configuration.')
        call errlog('Models 7 and 8 were not compared to data')
        call errlog('in the only-s-channel configuration.')
        call std_out('Models 7 and 8 were not compared to data')
        call std_out('in the only-s-channel configuration.')
        call prg_abort
        endif
      endif

      return
      end

c**********************************************************************
      subroutine ExtraCuts_1pi(accepted)
      implicit none
      include 'common.ekhara.inc.for'
      logical accepted
      logical accepted_extra
      
      accepted_extra = .true.
      
      call taggingCutsBES(accepted_extra)

      accepted = (accepted_extra)
      return
      end


c**********************************************************************
! generic routine for writing the generated events into file
! Please use file descriptor 8 for output of generated events
!  (it is already open for writing).
!
!     [e+e- -> e+e-pi]
!
!----------------------------------------------------------------------
      subroutine reportevent_1pi
      implicit none
      include 'common.ekhara.inc.for'

      if (WriteEvents .eq. 1) then

c            write(8,*) ' '

c            write(8,101) kk

            write(8,102) q1(0) ,'  ', 
     &      q1(1) ,'  ',q1(2) ,'  ',q1(3) 
            write(8,102) q2(0) ,'  ', 
     &      q2(1) ,'  ',q2(2) ,'  ',q2(3) 
            write(8,102) qpion(0) ,'  ', 
     &      qpion(1) ,'  ',qpion(2) ,'  ',qpion(3) 
      endif

 101  format(1pd22.15)
 102  format(1pd22.15,A,1pd22.15,A,1pd22.15,A,1pd22.15)
      return
      end

!----------------------------------------------------------------------
      subroutine reportevent_1pi_outflag
      implicit none
      include 'common.ekhara.inc.for'
      CHARACTER msg*78         ! for logging

      call warnlog(' ')
      call warnlog('OUTFLAG was set to .true. for this kinematics:')

      msg = ' '
      write(msg,103)  'kk = ', kk, ' [GeV^2]'
      call warnlog(msg)

      msg = ' '
      write(msg,103) 'q1 = (', q1(0) ,',', 
     &      q1(1) ,',',q1(2) ,',',q1(3) ,' ) [GeV]'
      call warnlog(msg)

      msg = ' '
      write(msg,103) 'q2 = (', q2(0) ,',', 
     &      q2(1) ,',',q2(2) ,',',q2(3) ,' ) [GeV]'
      call warnlog(msg)

      msg = ' '
      write(msg,103) 'qp = (', qpion(0) ,',', 
     &      qpion(1) ,',',qpion(2) ,',',qpion(3) ,' ) [GeV]'
      call warnlog(msg)

      call warnlog(' ')

      UnstableCounter = UnstableCounter + 1

 103  format(A,1pd12.5,A,1pd9.2,A,1pd9.2,A,1pd9.2,A)
      return
      end



c**********************************************************************
! Pion-gamma*-gamma* form factor
! Important: normalization           [piggFF( 0 , 0 , mpi^2 ) == 1]
! Example: 
!          WZW:      piggFF = 1 
!
! Notice, pggCoupling is accounted for at the level of 
! the matrix element evalution 
!
!  -> see   subroutine m_el_1pi(Msq_correct)   in  routines_1pi.inc.for
!
!            sqrt(pggCoupling)  = =  (e^4 Nc/(12 pi^2 Fpi))
!                               = =  4.Q0 * alpha**2 / Fpi
!----------------------------------------------------------------------


!----------------------------------------------------------------------
!     Wrap for form factor formulae of different models
!
      complex*32  function piggFF(t1_t,t2_t,W2_t)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_t,t2_t,W2_t
      real*16 t1_a,t2_a,W2_a
      real*16 Mv

      complex*32 piggFF_WZW       ! function
      complex*32 piggFF_rho_pole  ! function
      complex*32 piggFF_LMD       ! function
      complex*32 piggFF_LMDpV     ! function
      complex*32 piggFF_LMDpV_new ! function
      complex*32 piggFF_Our       ! function
      complex*32 piggFF_Our2       ! function

      t1_a = t1_t
      t2_a = t2_t
      W2_a = W2_t

c----------------------------------hack---      
c      if (t1_a .lt. t2_a) then
c          t2_a = 0.Q0
c      else
c          t1_a = 0.Q0
c      endif
c----------------------------------hack---      


      !----------------------------------------------------------------
       select case(piggFFsw) ! 1-WZW, 2-rho_pole, 3-LMD, 4-LMD+V, 
      !                      ! 5-LMD+V_new,       7-our 1 octet,     8-our 2 octets
            case (1)
            piggFF = piggFF_WZW(t1_a,t2_a,W2_a)
            case (2)
            piggFF = piggFF_rho_pole(t1_a,t2_a,W2_a)
            case (3)
            piggFF = piggFF_LMD(t1_a,t2_a,W2_a)
            case (4)
            piggFF = piggFF_LMDpV(t1_a,t2_a,W2_a)
            case (5)
            piggFF = piggFF_LMDpV_new(t1_a,t2_a,W2_a)
            case (7)
            piggFF = piggFF_Our(t1_a,t2_a,W2_a)
            case (8)
            piggFF = piggFF_Our2(t1_a,t2_a,W2_a)
       end select
      !----------------------------------------------------------------
      return
      end


c----------------------------------------------------------
!     W Z W
!
      complex*32 function piggFF_WZW(t1_a,t2_a,W2_a)
      implicit none
      real*16 t1_a,t2_a,W2_a
      piggFF_WZW = (1.Q0,0.Q0)
      return
      end

c----------------------------------------------------------
!     V M D
!
      complex*32 function piggFF_rho_pole(t1_a,t2_a,W2_a)
      implicit none
      real*16 t1_a,t2_a,W2_a
      real*16 Mv
      complex*32  GAMv ! function
      
      Mv = 0.7755Q0   ! "generic" vector meson mass     (rho)

      piggFF_rho_pole =(1.Q0,0.Q0)
     &       * Mv**2/(t1_a-Mv**2 + (0.Q0,1.Q0)*Mv*GAMv(t1_a, Mv))
     &       * Mv**2/(t2_a-Mv**2 + (0.Q0,1.Q0)*Mv*GAMv(t2_a, Mv)) 
      return
      end

c----------------------------------------------------------
!     L M D
!
      complex*32 function piggFF_LMD(t1_a,t2_a,W2_a)
      implicit none
      real*16 t1_a,t2_a,W2_a
      real*16 Mv
      complex*32  GAMv ! function
      real*16 pi, fpi, coef, cv

      Mv = 0.7755Q0   ! "generic" vector meson mass     (rho)
      pi = 3.14159265358979324Q0
      Fpi= 0.0924Q0        ![GeV]

      coef = 4.Q0 * pi**2 * fpi

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [piggFF( 0 , 0 , mpi^2 ) == Nc/(12 pi^2 Fpi)]
!      - here in the program we require: [piggFF( 0 , 0 , mpi^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      cv = 3.Q0/(4.Q0 * pi**2) * Mv**4 /(fpi**2)

      piggFF_LMD = coef* fpi / 3.Q0 
     &   *(1.Q0,0.Q0)/(t1_a-Mv**2 + (0.Q0,1.Q0)*Mv*GAMv(t1_a, Mv))
     &               /(t2_a-Mv**2 + (0.Q0,1.Q0)*Mv*GAMv(t2_a, Mv))
     &             * (t1_a + t2_a - cv)
      return
      end

c----------------------------------------------------------
!     L M D + V -- fitted to CLEO
!
      complex*32 function piggFF_LMDpV(t1_a,t2_a,W2_a)
      implicit none
      real*16 t1_a,t2_a,W2_a
      real*16 Mv, Mvp
      complex*32  GAMv,GAMvp ! function
      real*16 pi, fpi, coef, h1, h2, h5, h7

      Mv = 0.7755Q0   ! "generic" vector meson mass     (rho)
      Mvp = 1.465Q0   ! "generic" vector meson mass     (rho)
      pi = 3.14159265358979324Q0
      Fpi= 0.0924Q0        ![GeV]

      coef = 4.Q0 * pi**2 * fpi

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [piggFF( 0 , 0 , mpi^2 ) == Nc/(12 pi^2 Fpi)]
!      - here in the program we require: [piggFF( 0 , 0 , mpi^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      h1 = 0.Q0
      h2 = -10.63Q0
      h5 = 6.93Q0
      h7 = - 3.Q0/(4.Q0 * pi**2) * Mv**4 * Mvp**4 /(fpi**2)

      piggFF_LMDpV = coef* fpi / 3.Q0 
     &    *(1.Q0,0.Q0)/(t1_a-Mv**2 + (0.Q0,1.Q0)*Mv*GAMv(t1_a, Mv))
     &            /(t2_a-Mv**2 + (0.Q0,1.Q0)*Mv*GAMv(t2_a, Mv))
     &            /(t1_a-Mvp**2 + (0.Q0,1.Q0)*Mvp*GAMvp(t1_a, Mvp))
     &            /(t2_a-Mvp**2 + (0.Q0,1.Q0)*Mvp*GAMvp(t2_a, Mvp))
     & * (t1_a*t2_a*(t1_a+t2_a) + h1*(t1_a+t2_a)**2
     &       + h2*t1_a*t2_a + h5*(t1_a+t2_a) + h7 )
      return
      end

c----------------------------------------------------------
!     L M D + V -- fitted to BABAR
!
      complex*32 function piggFF_LMDpV_new(t1_a,t2_a,W2_a)
      implicit none
      real*16 t1_a,t2_a,W2_a
      real*16 Mv, Mvp
      complex*32  GAMv,GAMvp ! function
      real*16 pi, fpi, coef, h1, h2, h5, h7

      Mv = 0.7755Q0   ! "generic" vector meson mass     (rho)
      Mvp = 1.465Q0   ! "generic" vector meson mass     (rho)
      pi = 3.14159265358979324Q0
      Fpi= 0.0924Q0        ![GeV]

      coef = 4.Q0 * pi**2 * fpi

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [piggFF( 0 , 0 , mpi^2 ) == Nc/(12 pi^2 Fpi)]
!      - here in the program we require: [piggFF( 0 , 0 , mpi^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      h1 = -0.17Q0
      h2 = -10.63Q0
      h5 = 6.51Q0
      h7 = - 3.Q0/(4.Q0 * pi**2) * Mv**4 * Mvp**4 /(fpi**2)

      piggFF_LMDpV_new = coef* fpi / 3.Q0 
     &    *(1.Q0,0.Q0)/(t1_a-Mv**2 + (0.Q0,1.Q0)*Mv*GAMv(t1_a, Mv))
     &             /(t2_a-Mv**2 + (0.Q0,1.Q0)*Mv*GAMv(t2_a, Mv))
     &             /(t1_a-Mvp**2 + (0.Q0,1.Q0)*Mvp*GAMvp(t1_a, Mvp))
     &             /(t2_a-Mvp**2 + (0.Q0,1.Q0)*Mvp*GAMvp(t2_a, Mvp))
     & * (t1_a*t2_a*(t1_a+t2_a) + h1*(t1_a+t2_a)**2 
     &       + h2*t1_a*t2_a + h5*(t1_a+t2_a) + h7 )
      return
      end

c----------------------------------------------------------
c     Momentum-dependent width (rough) for a "generic" vector meson,
c     which is used in the above form factors.
c     Inclusion of widths is important, because we do use these formulae
c     in the time-like region, when including the s-channel amplitude.
c
c     Formula for width is taken for the rho meson.
c     It is based on RChT and includes the pi pi and K K-bar decay channels.
c
      complex*32 function GAMv(t_a, Mv)
      implicit none
      real*16 t_a, Mv
      real*16 mpi, mK, pi, fpi, Gv
      mpi = 0.1349766Q0    ![GeV]
      mK  = 0.49368Q0      ![GeV]
      pi = 3.14159265358979324Q0
      Fpi= 0.0924Q0        ![GeV]
      Gv = 0.06534Q0       ![GeV]
      
      if (t_a .le. 4.Q0 * mpi**2) then
         GAMv = (0.Q0, 0.Q0)
         return
      endif
      
      GAMv = (1.Q0, 0.Q0) * Gv**2 * Mv**2 /(48.Q0*pi*fpi**4 * t_a) *
     &       (t_a - 4.Q0 * mpi**2)**(1.5Q0)

      if (t_a .gt. 4.Q0 * mk**2) then
         GAMv = GAMv + (1.Q0, 0.Q0) * 
     &       Gv**2 * Mv**2 /(48.Q0*pi*fpi**4 * t_a) *
     &       0.5Q0 * (t_a - 4.Q0 * mK**2)**(1.5Q0)
      endif

      return
c      GAMv = (0.1494Q0, 0.Q0)  
c                   ! "generic" vector meson width    (rho)
c                   ! PDG 16-02-2010; rho(770) -> 146.2 \pm 0.7
c                   !               omega(782) -> 8.49 \pm 0.08
c                   !                phi(1020) -> 4.26 \pm 0.04
      end
      
c----------------------------------------------------------
c     Momentum-dependent width (rough) for a "generic" "excited" vector meson
c     to be used in the above form factors
c
      complex*32 function GAMvp(t_a, Mvp)
      implicit none
      real*16 t_a, Mvp
      real*16 mpi, mK, pi, fpi, Gv, Gvp
      mpi = 0.1349766Q0    ![GeV]
      mK  = 0.49368Q0      ![GeV]
      pi = 3.14159265358979324Q0
      Fpi= 0.0924Q0        ![GeV]
      Gv = 0.06534Q0       ![GeV]
      Gvp= 0.06534Q0       ![GeV]  <- this is a very rough estimate.
                           !          at the moment I don't know the value

      if (t_a .le. 4.Q0 * mpi**2) then
         GAMvp = (0.Q0, 0.Q0)
         return
      endif
      
      GAMvp = (1.Q0, 0.Q0) * Gvp**2 * Mvp**2 /(48.Q0*pi*fpi**4 * t_a) *
     &       (t_a - 4.Q0 * mpi**2)**(1.5Q0)

      if (t_a .gt. 4.Q0 * mk**2) then
         GAMvp = GAMvp +  (1.Q0, 0.Q0) * 
     &       Gvp**2 * Mvp**2 /(48.Q0*pi*fpi**4 * t_a) *
     &       0.5Q0 * (t_a - 4.Q0 * mK**2)**(1.5Q0)
      endif

      return
c      GAMv = (0.4Q0 , 0.Q0)
c                   ! "generic" "excited" vector meson width    (rho')
c                   ! PDG 16-02-2010; rho'(1450) -> 400 \pm 60
c                   !               omega'(1420) -> 180 - 250
c                   !                 phi'(1680) -> 150 \pm 50
      end
      

c----------------------------------------------------------------------c
! REFERENCES: Form factors for [gamma gamma ->  pi0, eta, eta']        c
!                                                                      c
!             H. Czyz, S. Ivashyn, A. Korchin, O. Shekhovtsova,        c
!             Phys.Rev. D85 (2012) 094010                              c
c----------------------------------------------------------------------c
      complex*32 function piggFF_Our(t1_a,t2_a,W2_a)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_a,t2_a,W2_a
      complex*32 Drho, Domega, Dphi   ! function (q2t)
      real*16 coef

      complex*32 ffdummy

      coef = -4.Q0 * pi**2 * Fchir

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [piggFF( 0 , 0 , mpi^2 ) == -Nc/(12 pi^2 Fpi)]
!      - here in the program we require: [piggFF( 0 , 0 , mpi^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      ffdummy =  coef *(
     & (-1.Q0,0.Q0) / 4.Q0 /pi**2 / Fchir
     & + 4.Q0*sqrt(2.Q0)*Fsmv*Hv/3.Q0/Fchir * t1_a
     & *(Drho(t1_a) + Domega(t1_a))
     & + 4.Q0*sqrt(2.Q0)*Fsmv*Hv/3.Q0/Fchir * t2_a
     & *(Drho(t2_a) + Domega(t2_a))
     & - 4.Q0*Fsmv**2 * SIGv/Fchir/3.Q0 * t1_a*t2_a
     & *(   Drho(t1_a)* Domega(t2_a) +  Drho(t2_a)* Domega(t1_a))
     &  )

      piggFF_Our = ffdummy
      return
      end


c----------------------------------------------------------------------c
! REFERENCES: Form factors for [gamma gamma ->  pi0, eta, eta']        c
!                                                                      c
!             H. Czyz, S. Ivashyn, A. Korchin, O. Shekhovtsova,        c
!             Phys.Rev. D85 (2012) 094010                              c
c----------------------------------------------------------------------c
      complex*32 function piggFF_Our2(t1_a,t2_a,W2_a)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_a,t2_a,W2_a
      complex*32 Drho, Domega, Dphi   ! function (q2t)
      complex*32 Drho2, Domega2, Dphi2   ! function (q2t); 2nd octet
      real*16 coef

      complex*32 ffdummy

      coef = -4.Q0 * pi**2 * Fchir

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [piggFF( 0 , 0 , mpi^2 ) == -Nc/(12 pi^2 Fpi)]
!      - here in the program we require: [piggFF( 0 , 0 , mpi^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      ffdummy =  coef *(
     & (-1.Q0,0.Q0) / 4.Q0 /pi**2 / Fchir
     & + 4.Q0*sqrt(2.Q0)*Fsmv1*Hv1/3.Q0/Fchir * t1_a
     & *(Drho(t1_a) + Domega(t1_a))
     & + 4.Q0*sqrt(2.Q0)*Fsmv1*Hv1/3.Q0/Fchir * t2_a
     & *(Drho(t2_a) + Domega(t2_a))
     & - 4.Q0*Fsmv1**2 * SIGv1/Fchir/3.Q0 * t1_a*t2_a
     & *(   Drho(t1_a)* Domega(t2_a) +  Drho(t2_a)* Domega(t1_a))
     &  )

      ffdummy = ffdummy +coef*(
     & + 4.Q0*sqrt(2.Q0)*Fsmv2Hv2/3.Q0/Fchir * t1_a
     & *(Drho2(t1_a) + Domega2(t1_a))
     & + 4.Q0*sqrt(2.Q0)*Fsmv2Hv2/3.Q0/Fchir * t2_a
     & *(Drho2(t2_a) + Domega2(t2_a))
     & - 4.Q0*Fsmv2sqSIGv2/Fchir/3.Q0 * t1_a*t2_a
     & *(   Drho2(t1_a)* Domega2(t2_a) +  Drho2(t2_a)* Domega2(t1_a))
     &  )

      piggFF_Our2 = ffdummy
      return
      end


c------------------------------------------------------------------------
c
c     Vector meson momentum-dependent widths
c
      complex*32 function GammaRhoTotal(pv2)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 pv2

      if ( (pv2 - 4.Q0*Mpi**2).gt.0.Q0 ) then
            GammaRhoTotal = (1.Q0, 0.Q0) *
     &       Gv**2 * Mrho_c**2/(48.Q0 * pi * Fpi**4 * pv2)
     &                            *(pv2- 4.Q0*Mpi**2)**(3.Q0/2.Q0) 
      else
            GammaRhoTotal = GammaTotalRho_c
      endif
      if ( (pv2 - 4.Q0*Mk**2).gt.0.Q0 ) then
            GammaRhoTotal = GammaRhoTotal + (0.Q5, 0.Q0) *
     &       Gv**2 * Mrho_c**2/(48.Q0 * pi * Fpi**4 * pv2)
     &                            *(pv2 - 4.Q0*Mk**2)**(3.Q0/2.Q0) 
      endif

      GammaRhoTotal = GammaTotalRho_c
      return
      end

      complex*32 function GammaOmegaTotal(pv2)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 pv2
      GammaOmegaTotal = GammaTotalOmega_c
      return
      end

      complex*32 function GammaPhiTotal(pv2)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 pv2
      GammaPhiTotal = GammaTotalPhi_c
      return
      end

      complex*32 function GammaRho_pr_Total(pv2)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 pv2
      GammaRho_pr_Total = GammaTotalRho_pr
      return
      end

      complex*32 function GammaOmega_pr_Total(pv2)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 pv2
      GammaOmega_pr_Total = GammaTotalOmega_pr
      return
      end

      complex*32 function GammaPhi_pr_Total(pv2)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 pv2
      GammaPhi_pr_Total = GammaTotalPhi_pr
      return
      end

c----------------------------------------------------------
c
c     Vector meson propagators

      complex*32 function Drho(q2t)
      implicit none
      include 'common.ekhara.inc.for'
      complex*32 GammaRhoTotal
      real*16 q2t
      if (q2t .gt. 0.Q0) then
        Drho = (q2t - Mrho_c**2 
     &      + (0.Q0, 1.Q0) * sqrt(q2t)
     &       * GammaRhoTotal(q2t))**-1.Q0
      else
        Drho = (q2t - Mrho_c**2)**-1.Q0
      endif
      return
      end
c
c
      complex*32 function Domega(q2t)
      implicit none
      include 'common.ekhara.inc.for'
      complex*32 GammaOmegaTotal
      real*16 q2t
      if (q2t .gt. 0.0) then
        Domega = (q2t - Momega_c**2 
     &      + (0.Q0, 1.Q0) * sqrt(q2t)
     &       * GammaOmegaTotal(q2t))**-1.Q0
      else
        Domega = (q2t - Momega_c**2)**-1.Q0
      endif
      return
      end
c
c
      complex*32 function DPhi(q2t)
      implicit none
      include 'common.ekhara.inc.for'
      complex*32 GammaPhiTotal
      real*16 q2t
      if (q2t .gt. 0.Q0) then
        DPhi = (1.Q0, 0.Q0)*(q2t - MPhi_c **2 
     &       + (0.Q0, 1.Q0) * sqrt(q2t)
     &       * GammaPhiTotal(q2t) )**-1.Q0
      else
        DPhi =(q2t - MPhi_c**2)**-1.Q0
      endif
      return
      end
c
c
      complex*32 function Drho2(q2t)
      implicit none
      include 'common.ekhara.inc.for'
      complex*32 GammaRho_pr_Total
      real*16 q2t

      if (q2t .gt. 0.Q0) then
        Drho2 = (q2t - Mrho_pr**2 
     &      + (0.Q0, 1.Q0) * sqrt(q2t)
     &       * GammaRho_pr_Total(q2t))**(-1.Q0)
      else
        Drho2 = (q2t - Mrho_pr**2)**(-1.Q0)
      endif
      return
      end
c
c
      complex*32 function Domega2(q2t)
      implicit none
      include 'common.ekhara.inc.for'
      complex*32 GammaOmega_pr_Total
      real*16 q2t

      if (q2t .gt. 0.Q0) then
        Domega2 = (q2t - Momega_pr**2 
     &      + (0.Q0, 1.Q0) * sqrt(q2t)
     &       * GammaOmega_pr_Total(q2t))**(-1.Q0)
      else
        Domega2 = (q2t - Momega_pr**2)**(-1.Q0)
      endif
      return
      end
c
c
      complex*32 function DPhi2(q2t)
      include 'common.ekhara.inc.for'
      complex*32 GammaPhi_pr_Total
      real*16 q2t

      if (q2t .gt. 0.Q0) then
        DPhi2 = (1.Q0, 0.Q0)*(q2t - MPhi_pr **2 
     &       + (0.Q0, 1.Q0) * sqrt(q2t)
     &       * GammaPhi_pr_Total(q2t) )**(-1.Q0)
      else
        DPhi2 =(q2t - MPhi_pr**2)**(-1.Q0)
      endif
      return
      end



c**********************************************************************
! Eta-gamma*-gamma* form factor
! Important: normalization           [etaggFF( 0 , 0 , meta^2 ) == 1]
! Example: 
!          WZW:      etaggFF = 1 
!
! Notice, pggCoupling is accounted for at the level of 
! the matrix element evalution 
!
!  -> see   subroutine m_el_1pi(Msq_correct)   in  routines_1pi.inc.for
!
!            sqrt(pggCoupling)  = =  (e^4 Nc/(12 pi^2 Feta))
!                               = =  4.Q0 * alpha**2 / Feta
!----------------------------------------------------------------------

!----------------------------------------------------------------------
!     Wrap for the form factor formulae of different models
!
      complex*32  function etaggFF(t1_t,t2_t,W2_t)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_t,t2_t,W2_t
      real*16 t1_a,t2_a,W2_a

      complex*32 etaggFF_WZW       ! function
      complex*32 etaggFF_Silagadze ! function
      complex*32 etaggFF_Our       ! function
      complex*32 etaggFF_Our2       ! function

      t1_a = t1_t
      t2_a = t2_t
      W2_a = W2_t
      
      !----------------------------------------------------------------
       select case(piggFFsw) ! 1-WZW, 2=7 
      !                      ! 3-Silagadze
      !                      ! 7-our 1 octet,            8-our 2 octets
            case (1)
            etaggFF = etaggFF_WZW(t1_a,t2_a,W2_a)
            case (2)
            etaggFF = etaggFF_Our(t1_a,t2_a,W2_a)
            case (3)
            etaggFF = etaggFF_Silagadze(t1_a,t2_a,W2_a)
            case (7)
            etaggFF = etaggFF_Our(t1_a,t2_a,W2_a)
            case (8)
            etaggFF = etaggFF_Our2(t1_a,t2_a,W2_a)
       end select
      !----------------------------------------------------------------
      return
      end


c----------------------------------------------------------
!     W Z W
!
      complex*32 function etaggFF_WZW(t1_a,t2_a,W2_a)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_a,t2_a,W2_a
      etaggFF_WZW = (1.Q0,0.Q0)
      return
      end

c----------------------------------------------------------------------c
! REFERENCES: Form factors for [gamma gamma ->  pi0, eta, eta']        c
!                                                                      c
!             H. Czyz, S. Ivashyn, A. Korchin, O. Shekhovtsova,        c
!             Phys.Rev. D85 (2012) 094010                              c
c----------------------------------------------------------------------c
      complex*32 function etaggFF_Our(t1_a,t2_a,W2_a)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_a,t2_a,W2_a
      complex*32 Drho, Domega, Dphi   ! function (q2t)
      real*16 coef
      
      complex*32 ffdummy
      
      coef = - 4.Q0 * pi**2 * Fchir
     &         /(  Cq_c*5.Q0/3.Q0 - Cs_c*sqrt(2.Q0)/3.Q0 ) 

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [etaggFF( 0 , 0 , meta^2 ) == -Nc/(12 pi^2 Fpi)(Cq_c*5/3-Cs_c*sqrt(2)/3) ]
!      - here in the program we require: [etaggFF( 0 , 0 , meta^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      ffdummy =  coef *(
     & (-1.Q0,0.Q0) / 4.Q0 /pi**2 / Fchir
     & *(  Cq_c*5.Q0/3.Q0
     &   - Cs_c*sqrt(2.Q0)/3.Q0 ) 
     & +(1.Q0,0.Q0)
     & * 4.d0*sqrt(2.Q0)*Fsmv*Hv/3.Q0/Fchir * t1_a
     & *(3.Q0*Cq_c* Drho(t1_a) + Cq_c* Domega(t1_a)/3.Q0 
     &     - 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_c* Dphi(t1_a))
     & +(1.Q0,0.Q0)
     & * 4.Q0*sqrt(2.Q0)*Fsmv*Hv/3.Q0/Fchir * t2_a
     & *(3.Q0*Cq_c* Drho(t2_a) + Cq_c* Domega(t2_a)/3.Q0 
     &     - 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_c* Dphi(t2_a))
     & -(1.Q0,0.Q0)
     & * 8.Q0*Fsmv**2 * SIGv/Fchir * t1_a*t2_a
     & *(  Cq_c* Drho(t1_a)* Drho(t2_a)/2.Q0 
     &   + Cq_c* Domega(t1_a)* Domega(t2_a)/18.Q0 
     &   - sqrt(2.Q0)/9.Q0 * Cs_c* Dphi(t1_a)* Dphi(t2_a)) !sign corrected
     &  )

      etaggFF_Our = ffdummy
      return
      end



c----------------------------------------------------------------------c
! REFERENCES: Form factors for [gamma gamma ->  pi0, eta, eta']        c
!                                                                      c
!             H. Czyz, S. Ivashyn, A. Korchin, O. Shekhovtsova,        c
!             Phys.Rev. D85 (2012) 094010                              c
c----------------------------------------------------------------------c
      complex*32 function etaggFF_Our2(t1_a,t2_a,W2_a)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_a,t2_a,W2_a
      complex*32 Drho, Domega, Dphi   ! function (q2t)
      complex*32 Drho2, Domega2, Dphi2   ! function (q2t); 2nd octet
      real*16 coef
      
      complex*32 ffdummy
      
      coef = - 4.Q0 * pi**2 * Fchir
     &         /(  Cq_c*5.Q0/3.Q0 - Cs_c*sqrt(2.Q0)/3.Q0 ) 

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [etaggFF( 0 , 0 , meta^2 ) == -Nc/(12 pi^2 Fpi)(Cq_c*5/3-Cs_c*sqrt(2)/3) ]
!      - here in the program we require: [etaggFF( 0 , 0 , meta^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      ffdummy =  coef *(
     & (-1.Q0,0.Q0) / 4.Q0 /pi**2 / Fchir
     & *(  Cq_c*5.Q0/3.Q0
     &   - Cs_c*sqrt(2.Q0)/3.Q0 ) 
     & +(1.Q0,0.Q0)
     & * 4.Q0*sqrt(2.Q0)*Fsmv1*Hv1/3.Q0/Fchir * t1_a
     & *(3.Q0*Cq_c* Drho(t1_a) + Cq_c* Domega(t1_a)/3.Q0 
     &     - 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_c* Dphi(t1_a))
     & +(1.Q0,0.Q0)
     & * 4.Q0*sqrt(2.Q0)*Fsmv1*Hv1/3.Q0/Fchir * t2_a
     & *(3.Q0*Cq_c* Drho(t2_a) + Cq_c* Domega(t2_a)/3.Q0 
     &     - 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_c* Dphi(t2_a))
     & -(1.Q0,0.Q0)
     & * 8.Q0*Fsmv1**2 * SIGv1/Fchir * t1_a*t2_a
     & *(  Cq_c* Drho(t1_a)* Drho(t2_a)/2.Q0 
     &   + Cq_c* Domega(t1_a)* Domega(t2_a)/18.Q0 
     &   - sqrt(2.Q0)/9.Q0 * Cs_c* Dphi(t1_a)* Dphi(t2_a)) !sign corrected
     &  )

      ffdummy = ffdummy +coef*(
     & (1.Q0,0.Q0)
     & * 4.Q0*sqrt(2.Q0)*Fsmv2Hv2/3.Q0/Fchir * t1_a
     & *(3.Q0*Cq_c* Drho2(t1_a) + Cq_c* Domega2(t1_a)/3.Q0 
     &     - 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_c* Dphi2(t1_a))
     & +(1.Q0,0.Q0)
     & * 4.Q0*sqrt(2.Q0)*Fsmv2Hv2/3.Q0/Fchir * t2_a
     & *(3.Q0*Cq_c* Drho2(t2_a) + Cq_c* Domega2(t2_a)/3.Q0 
     &     - 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_c* Dphi2(t2_a))
     & -(1.Q0,0.Q0)
     & * 8.Q0*Fsmv2sqSIGv2/Fchir * t1_a*t2_a
     & *(  Cq_c* Drho2(t1_a)* Drho2(t2_a)/2.Q0 
     &   + Cq_c* Domega2(t1_a)* Domega2(t2_a)/18.Q0 
     &   - sqrt(2.Q0)/9.Q0 * Cs_c* Dphi2(t1_a)* Dphi2(t2_a))  !sign corrected
     &  )

      etaggFF_Our2 = ffdummy
      return
      end


c----------------------------------------------------------
!      Z.K. Silagadze ( Phys Rev D74, 054003 (2006) )
!      with modification by S.Ivashyn: 'generic' width in the propagator
!
      complex*32 function etaggFF_Silagadze(t1_a,t2_a,W2_a)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_a,t2_a,W2_a
      complex*32  GAMv ! function
      real*16 coef

      real*16 g_eta_rho_rho, g_eta_omega_omega, g_eta_phi_phi,
     &       g_gamma_rho,   g_gamma_omega,     g_gamma_phi 
c      real*16 Mrho_c, Momega_c, Mphi_c
            
c      Mrho_c = 0.7755D0          ![GeV]
c      Momega_c = 0.78265D0       ![GeV]
c      Mphi_c = 1.019456D0        ![GeV]

      g_eta_rho_rho     = 0.723Q0
      g_eta_omega_omega = 0.735Q0
      g_eta_phi_phi     = 0.858Q0
      g_gamma_rho   = 0.2014Q0
      g_gamma_omega = 0.0586Q0
      g_gamma_phi   = 0.0747Q0

      coef = 1.q0/ ( g_eta_rho_rho * g_gamma_rho**2 
     &        + g_eta_omega_omega * g_gamma_omega**2
     &        - g_eta_phi_phi * g_gamma_phi**2 )  *  Fpi

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [etaggFF( 0 , 0 , meta^2 ) == ... ]
!      - here in the program we require: [etaggFF( 0 , 0 , meta^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      etaggFF_Silagadze = coef*(
     & (1.Q0,0.Q0) /Fpi*                                                  ! <- 1/Fpi !
     & (  g_eta_rho_rho * g_gamma_rho**2 * Mrho_c**4 
     &   / (t1_a-Mrho_c**2+(0.Q0,1.Q0)*Mrho_c*GAMv(t1_a, Mrho_c))
     &   / (t2_a-Mrho_c**2+(0.Q0,1.Q0)*Mrho_c*GAMv(t2_a, Mrho_c))
     &   +g_eta_omega_omega * g_gamma_omega**2 * Momega_c**4 
     &   /(t1_a-Momega_c**2+(0.Q0,1.Q0)*Momega_c*GAMv(t1_a,Momega_c))
     &   /(t2_a-Momega_c**2+(0.Q0,1.Q0)*Momega_c*GAMv(t2_a,Momega_c))
     &   +g_eta_phi_phi * g_gamma_phi**2 * Mphi_c**4
     &   /(t1_a - Mphi_c**2 + (0.Q0,1.Q0)*Mphi_c*GAMv(t1_a, Mphi_c))
     &   /(t2_a - Mphi_c**2 + (0.Q0,1.Q0)*Mphi_c*GAMv(t2_a, Mphi_c))
     &  )
     &  )
      return
      end

c**********************************************************************
! Eta'-gamma*-gamma* form factor
! Important: normalization           [etaPggFF( 0 , 0 , meta' ^2 ) == 1]
! Example: 
!          WZW:      etaPggFF = 1 
!
! Notice, pggCoupling is accounted for at the level of 
! the matrix element evalution 
!
!  -> see   subroutine m_el_1pi(Msq_correct)   in  routines_1pi.inc.for
!
!            sqrt(pggCoupling)  = =  (e^4 Nc/(12 pi^2 FetaP))
!                               = =  4.Q0 * alpha**2 / FetaP
!----------------------------------------------------------------------

!----------------------------------------------------------------------
!     Wrap for the form factor formulae of different models
!
      complex*32  function etaPggFF(t1_t,t2_t,W2_t)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_t,t2_t,W2_t
      real*16 t1_a,t2_a,W2_a

      complex*32 etaPggFF_WZW       ! function
      complex*32 etaPggFF_Our       ! function
      complex*32 etaPggFF_Our2       ! function
      complex*32 etaPggFF_Silagadze ! function

      t1_a = t1_t
      t2_a = t2_t
      W2_a = W2_t
      
      !----------------------------------------------------------------
       select case(piggFFsw) ! 1-WZW, 2=7 
      !                      ! 3-Silagadze
      !                      ! 7-our 1 octet,            8-our 2 octets
            case (1)
            etaPggFF = etaPggFF_WZW(t1_a,t2_a,W2_a)
            case (2)
            etaPggFF = etaPggFF_Our(t1_a,t2_a,W2_a)
            case (3)
            etaPggFF = etaPggFF_Silagadze(t1_a,t2_a,W2_a)
            case (7)
            etaPggFF = etaPggFF_Our(t1_a,t2_a,W2_a)
            case (8)
            etaPggFF = etaPggFF_Our2(t1_a,t2_a,W2_a)
       end select
      !----------------------------------------------------------------
      return
      end


c----------------------------------------------------------
!     W Z W
!
      complex*32 function etaPggFF_WZW(t1_a,t2_a,W2_a)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_a,t2_a,W2_a
      etaPggFF_WZW = (1.Q0,0.Q0)
      return
      end

c----------------------------------------------------------------------c
! REFERENCES: Form factors for [gamma gamma ->  pi0, eta, eta']        c
!                                                                      c
!             H. Czyz, S. Ivashyn, A. Korchin, O. Shekhovtsova,        c
!             Phys.Rev. D85 (2012) 094010                              c
c----------------------------------------------------------------------c
      complex*32 function etaPggFF_Our(t1_a,t2_a,W2_a)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_a,t2_a,W2_a
      complex*32 Drho, Domega, Dphi   ! function (q2t)
      real*16 coef
      
      complex*32 ffdummy

      coef = - 4.Q0 * pi**2 * Fchir
     &         /( Cq_P*5.Q0/3.Q0 + Cs_P*sqrt(2.Q0)/3.Q0 ) 

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [etaPggFF( 0 , 0 , metaP^2 ) == -Nc/(12 pi^2 Fpi)(Cq_P*5/3 + Cs_P*sqrt(2)/3) ]
!      - here in the program we require: [etaPggFF( 0 , 0 , metaP^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      ffdummy =  coef *(
     & (-1.Q0,0.Q0) / 4.Q0 /pi**2 / Fchir
     & *(  Cq_P*5.Q0/3.Q0
     &   + Cs_P*sqrt(2.Q0)/3.Q0 ) 
     & +(1.Q0,0.Q0)
     & * 4.d0*sqrt(2.Q0)*Fsmv*Hv/3.Q0/Fchir * t1_a
     & *(3.Q0*Cq_P* Drho(t1_a) + Cq_P* Domega(t1_a)/3.Q0 
     &     + 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_P* Dphi(t1_a))
     & +(1.Q0,0.Q0)
     & * 4.Q0*sqrt(2.Q0)*Fsmv*Hv/3.Q0/Fchir * t2_a
     & *(3.Q0*Cq_P* Drho(t2_a) + Cq_P* Domega(t2_a)/3.Q0 
     &     + 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_P* Dphi(t2_a))
     & -(1.Q0,0.Q0)
     & * 8.Q0*Fsmv**2 * SIGv/Fchir * t1_a*t2_a
     & *(  Cq_P* Drho(t1_a)* Drho(t2_a)/2.Q0 
     &   + Cq_P* Domega(t1_a)* Domega(t2_a)/18.Q0 
     &   + sqrt(2.Q0)/9.Q0 * Cs_P* Dphi(t1_a)* Dphi(t2_a))  !sign corrected
     &  )

      etaPggFF_Our = ffdummy
      return
      end


c----------------------------------------------------------------------c
! REFERENCES: Form factors for [gamma gamma ->  pi0, eta, eta']        c
!                                                                      c
!             H. Czyz, S. Ivashyn, A. Korchin, O. Shekhovtsova,        c
!             Phys.Rev. D85 (2012) 094010                              c
c----------------------------------------------------------------------c
      complex*32 function etaPggFF_Our2(t1_a,t2_a,W2_a)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_a,t2_a,W2_a
      complex*32 Drho, Domega, Dphi   ! function (q2t)
      complex*32 Drho2, Domega2, Dphi2   ! function (q2t); 2nd octet
      real*16 coef
      
      complex*32 ffdummy

      coef = - 4.Q0 * pi**2 * Fchir
     &         /( Cq_P*5.Q0/3.Q0 + Cs_P*sqrt(2.Q0)/3.Q0 ) 

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [etaPggFF( 0 , 0 , metaP^2 ) == -Nc/(12 pi^2 Fpi)(Cq_P*5/3 + Cs_P*sqrt(2)/3) ]
!      - here in the program we require: [etaPggFF( 0 , 0 , metaP^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      ffdummy =  coef *(
     & (-1.Q0,0.Q0) / 4.Q0 /pi**2 / Fchir
     & *(  Cq_P*5.Q0/3.Q0
     &   + Cs_P*sqrt(2.Q0)/3.Q0 ) 
     & +(1.Q0,0.Q0)
     & * 4.Q0*sqrt(2.Q0)*Fsmv1*Hv1/3.Q0/Fchir * t1_a
     & *(3.Q0*Cq_P* Drho(t1_a) + Cq_P* Domega(t1_a)/3.Q0 
     &     + 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_P* Dphi(t1_a))
     & +(1.Q0,0.Q0)
     & * 4.Q0*sqrt(2.Q0)*Fsmv1*Hv1/3.Q0/Fchir * t2_a
     & *(3.Q0*Cq_P* Drho(t2_a) + Cq_P* Domega(t2_a)/3.Q0 
     &     + 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_P* Dphi(t2_a))
     & -(1.Q0,0.Q0)
     & * 8.Q0*Fsmv1**2 * SIGv1/Fchir * t1_a*t2_a
     & *(  Cq_P* Drho(t1_a)* Drho(t2_a)/2.Q0 
     &   + Cq_P* Domega(t1_a)* Domega(t2_a)/18.Q0 
     &   + sqrt(2.Q0)/9.Q0 * Cs_P* Dphi(t1_a)* Dphi(t2_a))  !sign corrected
     &  )

       ffdummy = ffdummy +coef*(
     & +(1.Q0,0.Q0)
     & * 4.Q0*sqrt(2.Q0)*Fsmv2Hv2/3.Q0/Fchir * t1_a
     & *(3.Q0*Cq_P* Drho2(t1_a) + Cq_P* Domega2(t1_a)/3.Q0 
     &     + 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_P* Dphi2(t1_a))
     & +(1.Q0,0.Q0)
     & * 4.Q0*sqrt(2.Q0)*Fsmv2Hv2/3.Q0/Fchir * t2_a
     & *(3.Q0*Cq_P* Drho2(t2_a) + Cq_P* Domega2(t2_a)/3.Q0 
     &     + 2.Q0*sqrt(2.Q0)/3.Q0 * Cs_P* Dphi2(t2_a))
     & -(1.Q0,0.Q0)
     & * 8.Q0*Fsmv2sqSIGv2/Fchir * t1_a*t2_a
     & *(  Cq_P* Drho2(t1_a)* Drho2(t2_a)/2.Q0 
     &   + Cq_P* Domega2(t1_a)* Domega2(t2_a)/18.Q0 
     &   + sqrt(2.Q0)/9.Q0 * Cs_P* Dphi2(t1_a)* Dphi2(t2_a))  !sign corrected
     &  )

      etaPggFF_Our2 = ffdummy
      return
      end

c----------------------------------------------------------
!      Z.K. Silagadze ( Phys Rev D74, 054003 (2006) )
!      with modification by S.Ivashyn: 'generic' width in the propagator
!
      complex*32 function etaPggFF_Silagadze(t1_a,t2_a,W2_a)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 t1_a,t2_a,W2_a
      complex*32  GAMv ! function
      real*16 coef

      real*16 g_etaP_rho_rho, g_etaP_omega_omega, g_etaP_phi_phi,
     &       g_gamma_rho,   g_gamma_omega,     g_gamma_phi 
c      real*16 Mrho_c, Momega_c, Mphi_c
            
c      Mrho_c = 0.7755D0          ![GeV]
c      Momega_c = 0.78265D0       ![GeV]
c      Mphi_c = 1.019456D0        ![GeV]

      g_etaP_rho_rho     = 0.624Q0
      g_etaP_omega_omega = 0.724Q0
      g_etaP_phi_phi     = 0.886Q0
      g_gamma_rho   = 0.2014Q0
      g_gamma_omega = 0.0586Q0
      g_gamma_phi   = 0.0747Q0

      coef = 1.q0/ ( g_etaP_rho_rho * g_gamma_rho**2 
     &        + g_etaP_omega_omega * g_gamma_omega**2
     &        + g_etaP_phi_phi * g_gamma_phi**2 )  *  Fpi

!~~~~~~~~~~~~~~~~~~~~
!     Notice:
!     coef stands for the normalization change:
!      - in the paper it was: [etaPggFF( 0 , 0 , metaP^2 ) == ... ]
!      - here in the program we require: [etaPggFF( 0 , 0 , metaP^2 ) == 1]
!~~~~~~~~~~~~~~~~~~~~

      etaPggFF_Silagadze = coef*(
     & (1.Q0,0.Q0) /Fpi*                                                  ! <- 1/Fpi !
     & (  g_etaP_rho_rho * g_gamma_rho**2 * Mrho_c**4 
     &   / (t1_a-Mrho_c**2+(0.Q0,1.Q0)*Mrho_c*GAMv(t1_a, Mrho_c))
     &   / (t2_a-Mrho_c**2+(0.Q0,1.Q0)*Mrho_c*GAMv(t2_a, Mrho_c))
     &   +g_etaP_omega_omega * g_gamma_omega**2 * Momega_c**4 
     &   /(t1_a-Momega_c**2+(0.Q0,1.Q0)*Momega_c*GAMv(t1_a,Momega_c))
     &   /(t2_a-Momega_c**2+(0.Q0,1.Q0)*Momega_c*GAMv(t2_a,Momega_c))
     &   +g_etaP_phi_phi * g_gamma_phi**2 * Mphi_c**4
     &   /(t1_a - Mphi_c**2 + (0.Q0,1.Q0)*Mphi_c*GAMv(t1_a, Mphi_c))
     &   /(t2_a - Mphi_c**2 + (0.Q0,1.Q0)*Mphi_c*GAMv(t2_a, Mphi_c))
     &  )
     &  )
      return
      end


c**********************************************************************
      subroutine correc_binning_edge(histogramN,CUT_min,CUT_max)
      implicit none
      include 'common.ekhara.inc.for'
      integer histogramN
      real*16 CUT_min,CUT_max
      
      integer binN

      do binN=1, nBinsH(histogramN)
         if ( (loBin(histogramN,binN) .lt. CUT_min) .and.
     &        (upBin(histogramN,binN) .gt. CUT_min) ) then
             loBin(histogramN,binN) = CUT_min
             if (binN .eq. 1) then
                 loLimH(histogramN) = CUT_min
             else
                 upBin(histogramN,(binN-1)) = CUT_min
             endif
         endif

         if ( (loBin(histogramN,binN) .lt. CUT_max) .and.
     &        (upBin(histogramN,binN) .gt. CUT_max) ) then
             upBin(histogramN,binN) = CUT_max
             if (binN .eq. nBinsH(histogramN)) then
                 upLimH(histogramN) = CUT_max
             else
                 loBin(histogramN,(binN+1)) = CUT_max
             endif
         endif
      enddo

      return
      end
      
c**********************************************************************
      subroutine hist_prepare_user_1pi
      implicit none
      include 'common.ekhara.inc.for'
      integer binN, histogramN

      ! Fix the edge-effects
      call correc_binning_edge(12,CUT_E1min,CUT_E1max)
      call correc_binning_edge(8,CUT_th1min,CUT_th1max)
      call correc_binning_edge(13,CUT_E2min,CUT_E2max)
      call correc_binning_edge(6,CUT_th2min,CUT_th2max)
      call correc_binning_edge(14,CUT_Epionmin,CUT_Epionmax)
      call correc_binning_edge(10,CUT_thpionmin,CUT_thpionmax)
      call correc_binning_edge(2,CUT_t1min,CUT_t1max)
      call correc_binning_edge(3,CUT_t2min,CUT_t2max)

      return !=================================================<<<<<<<<

      ! Below is absent (or commented) in the officially distributed version      

      ! ..... ..... .....
      !=================================================================
      return
      end


c**********************************************************************
! This routine is invoked for EVERY ACCEPTED UNWEIGHTED event.
! Use it to fill the histograms in your favourite way.
!
!     [e+e- -> e+e-pi]
!
!----------------------------------------------------------------------
      subroutine histo_event_1pi
      implicit none
      include 'common.ekhara.inc.for'
      real*16 var(1:20) ! for histogramming

      real*16 cthpion, sthpion, cphipion, sphipion ! pion
      real*16 cphi1, sphi1, cth1, sth1             ! final positron
      real*16 cphi2, sphi2, cth2, sth2             ! final elektron
      real*16 deg

      !---hack: boost to DAPHNE LAB frame ---
      !   call DAPHNE_cms2lab_1pi ! it's important to call it before PSgg_decay_1pi
      !---hack: end                                               ---

      deg = 180.Q0/pi
      
      ! invariants
      var(1) = kk
      var(2) = it1
      var(3) = it2
      var(4) = s1
      var(5) = s2

      ! electron
      cth2 = q2(3)/q2abs 
      sth2 = qsqrt(1.Q0 - cth2**2)
      cphi2 = q2(2)/(q2abs * sth2)
      sphi2 = q2(1)/(q2abs * sth2)

      var(6) = acos(cth2) * deg

      !---------------------------
      ! remark: 
      !         0 <= ACOS <= pi
      !     -pi/2 <= ASIN <= pi/2
      !---------------------------
      if (sphi2 .le. 0.Q0) then
         var(7) = (2.Q0*pi - acos(cphi2))  * deg
      else 
         var(7) = acos(cphi2)  * deg
      endif
      if (var(7) .gt. 360.Q0) then
            var(7) = 360.Q0
      elseif (var(7) .lt. 0.Q0) then
            var(7) = 0.Q0
      endif

      ! positron
      cth1 = q1(3)/q1abs
      sth1 = qsqrt(1.Q0 - cth1**2)
      cphi1 = q1(2)/(q1abs * sth1)
      sphi1 = q1(1)/(q1abs * sth1)

      var(8) = acos(cth1)  * deg

      if (sphi1 .le. 0.Q0) then
         var(9) = (2.Q0*pi - acos(cphi1)) * deg
      else 
         var(9) = acos(cphi1) * deg
      endif
      if (var(9) .gt. 360.Q0) then
            var(9) = 360.Q0
      elseif (var(9) .lt. 0.Q0) then
            var(9) = 0.Q0
      endif

      ! pion
      cthpion = qpion(3)/qpionabs
      sthpion = qsqrt(1.Q0 - cthpion**2)
      cphipion = qpion(2)/(qpionabs * sthpion)
      sphipion = qpion(1)/(qpionabs * sthpion)

      var(10) = acos(cthpion) * deg

      if (sphipion .le. 0.Q0) then
         var(11) = (2.Q0*pi - acos(cphipion) )  * deg
      else 
         var(11) = acos(cphipion) * deg
      endif
      if (var(11) .gt. 360.Q0) then
            var(11) = 360.Q0
      elseif (var(11) .lt. 0.Q0) then
            var(11) = 0.Q0
      endif

      var(12) = q1(0)
      var(13) = q2(0)
      var(14) = qpion(0)
      
      !---special tagging histo ---
      var(15) = t_taged

      !---hack: histo for the angle between photons from PS decay ---
      !   call PSgg_decay_1pi
      !   var(6) = THrel
      !---hack: end                                               ---
      
      call hist_add_1pi(1.Q0,var)      ! WEIGHT = 1 !

      return
      end

c**********************************************************************
c**********************************************************************
c**********************************************************************
c**********************************************************************

c**********************************************************************
      subroutine read_card_2pi
      implicit none
      include 'common.ekhara.inc.for'
      !---- BEGIN READING THE DATACARD
      open(793,file='card_2pi.dat',status='old')
      read(793,*)
      read(793,*)
      read(793,*)
      read(793,*)

      ! Important: if you read a datacard in a different way,
      !            please make sure that ALL the below parameters
      !            are initialized with correct values

      !---- BEGIN INITIALIZE DEFAULT VALUES OF USER-DEFINED PARAMS
       ss            = 1.02Q0
       sw_2pi        = 3
       pi1lab_min    = 50.Q0
       pi1lab_max    = 130.Q0
       pipicut1_min  = 15.Q0
       pipicut1_max  = 165.Q0
      !---- END INITIALIZE DEFAULT VALUES OF USER-DEFINED PARAMS

      read(793,*) 
     &            ss,         ! CMS-Energy 
     &            sw_2pi,     ! 1 - s, 2 - s+t, 3 - s+t+2gamma
     &            pi1lab_min,   pi1lab_max,   ! pions angle cut
     &            pipicut1_min, pipicut1_max  ! missing mom. direction cut
       
      close(793)
      !---- END READING THE DATACARD
      s = ss*ss
      return
      end

c**********************************************************************
! Extra kinematic cuts, which are provided by user.
! Set "accepted" to .false. in order to reject an event.
!
!     [e+e- -> e+e-pi+pi-]
!
!----------------------------------------------------------------------
      subroutine ExtraCuts_2pi(accepted)
      implicit none
      include 'common.ekhara.inc.for'
      logical accepted

      real*16 q_tr(1:4),qq,pmodtr(1:2),
     &        Mphi,asquared,Efot_pi,trkmass

      accepted = .true.
      
      !-----------------
      ! KLOE track-mass cut, which was provided
      ! in the distributed EKHARA version 1.
      !
      qq = qu(0)**2 - qu(1)**2 - qu(2)**2 - qu(3)**2

      pmodtr(1)=Sqrt(pi2(1)**2 + pi2(2)**2 + pi2(3)**2)
      pmodtr(2)=Sqrt(pi1(1)**2 + pi1(2)**2 + pi1(3)**2)
      Mphi = 1.02Q0

      Efot_pi = Sqrt(qu(1)**2+qu(2)**2+qu(3)**2)
                                                                      
      asquared = (Efot_pi - Mphi)**2 - pmodtr(1)**2
     &           - pmodtr(2)**2
      asquared = 0.5Q0*asquared
      trkmass = ( asquared**2 - pmodtr(1)**2*pmodtr(2)**2 ) /
     &     ( 2.Q0*asquared + pmodtr(1)**2 + pmodtr(2)**2 )
      if ( trkmass.ge.0.Q0) then
         trkmass = Sqrt(trkmass)
      else
         trkmass = -Sqrt(-trkmass)
      endif
                                                                      
      accepted = (
     &  (trkmass.gt.0.12Q0).and.
     &  ( (qq/0.85Q0)**2+((trkmass-0.25Q0)/0.105Q0)**2.gt.1.Q0 )
     &  .and.(trkmass.lt.0.22Q0)
     &    ) 
      ! end of KLOE track-mass cut.
      !-----------------

      return
      end

c**********************************************************************
! This routine is invoked for EVERY ACCEPTED WEIGHTED event.
! Use it to fill the histograms in your favourite way.
!
!     [e+e- -> e+e-pi+pi-]
!
!----------------------------------------------------------------------
      subroutine histo_event_2pi(current_weight)
      implicit none
      include 'common.ekhara.inc.for'
      real*16 var(0:9) ! for histogramming

      real*16 current_weight

      var(0) = ququ           ! choice of histogramming variables
      call hist(current_weight,var)       ! WEIGHTED !

      return
      end

c**********************************************************************
