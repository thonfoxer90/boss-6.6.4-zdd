c-----------------------------------------------------------------------
c
c     EKHARA. include file.
c     misc math routines, manipulation with 4-vectors and matrices
c
c-----------------------------------------------------------------------


c**************************************************************
      subroutine boostek(cmsvector,boostvector)
      !
      ! boostvector is the 4-momentum of the system, FROM which we transform, given in LAB frame
      ! cmsvector is some 4-vector given in the system, FROM which we transform
      ! result is the cmsvector boosted from the rest-frame of boostvector TO the LAB frame
      !
      ! this routine is not inversable, use with care!!!
      !
      implicit none
      real*16 cmsvector(0:3),boostvector(0:3),
     &  labvector(0:3),m(0:3,0:3),
     &  E,p,beta,gamma,costheta,sintheta,cosphi,sinphi
      integer i,j

      E = boostvector(0)
      p = Sqrt(boostvector(1)**2+boostvector(2)**2+
     &    boostvector(3)**2)
      beta  =-p/E
      gamma = 1.Q0/Sqrt(1.Q0-beta*beta)
      costheta = boostvector(3)/p
      sintheta = Sqrt(1.Q0-costheta*costheta)
      cosphi   = boostvector(1)/(p*sintheta)
      sinphi   = boostvector(2)/(p*sintheta)

        m(0,0) = gamma
        m(0,1) = 0.Q0
        m(0,2) = 0.Q0
        m(0,3) = -beta*gamma
        m(1,0) = -beta*gamma*sintheta*cosphi
        m(1,1) = costheta*cosphi
        m(1,2) = -sinphi
        m(1,3) = gamma*sintheta*cosphi
        m(2,0) = -beta*gamma*sintheta*sinphi
        m(2,1) = costheta*sinphi
        m(2,2) = cosphi
        m(2,3) = gamma*sintheta*sinphi
        m(3,0) = -beta*gamma*costheta
        m(3,1) = -sintheta
        m(3,2) = 0.Q0
        m(3,3) = gamma*costheta

      do i=0,3
         labvector(i) = 0.Q0
         do j=0,3
            labvector(i) = labvector(i)+m(i,j)*cmsvector(j)
         enddo
      enddo
      do i=0,3
            cmsvector(i)= labvector(i)
      enddo

      return
      end


c *******************************************************************
c this subroutine boost fourvector qq from Q' rest frame to Q rest frame
c           Q' = Q - q1
c
      subroutine boost1(q0,q0p,e1,vq1,c1,s1,cf1,sf1,qq)
      implicit none
      real*16 q0,q0p,e1,vq1,c1,s1,cf1,sf1,qq1(0:3),qq(0:3)
     1                ,transf(0:3,0:3),fac1,fac2
      integer i,j
c
      fac1 = (q0-e1)/q0p
      fac2 = -vq1/q0p
c
      transf(0,0) =  fac1
      transf(1,0) =  fac2 * s1 * cf1      
      transf(2,0) =  fac2 * s1 * sf1     
      transf(3,0) =  fac2 * c1      
      transf(0,1) =  0.Q0      
      transf(1,1) =  c1 * cf1     
      transf(2,1) =  c1 * sf1      
      transf(3,1) = -s1      
      transf(0,2) =  0.Q0      
      transf(1,2) = -sf1      
      transf(2,2) =  cf1      
      transf(3,2) =  0.Q0     
      transf(0,3) =  fac2     
      transf(1,3) =  fac1 * s1 * cf1     
      transf(2,3) =  fac1 * s1 * sf1  
      transf(3,3) =  fac1 * c1   
c
      do i=0,3
        qq1(i) = 0.Q0
c
        do j = 0,3
         qq1(i) = qq1(i) + transf(i,j)*qq(j)
        enddo
      enddo
c
      do i=0,3
        qq(i) = qq1(i)
      enddo
c
      return
      end


c *******************************************************************
c this subroutine boost fourvector qq from Q' rest frame to p1+p2 rest frame
c           Q' = Q - q1
c
      subroutine boost2(q0,q0p,e1,vq1,c1,s1,cf1,sf1,qq)
      implicit none
      real*16 q0,q0p,e1,vq1,c1,s1,cf1,sf1,qq1(0:3),qq(0:3)
     1                ,transf(0:3,0:3),fac1,fac2
      integer i,j
c
      fac1 = (q0-e1)/q0p
      fac2 = -vq1/q0p
c
      transf(0,0) =  fac1
      transf(0,1) =  -fac2 * s1 * cf1      
      transf(0,2) =  -fac2 * s1 * sf1     
      transf(0,3) =  -fac2 * c1      
      transf(1,0) =  0.Q0      
      transf(1,1) =  c1 * cf1     
      transf(1,2) =  c1 * sf1      
      transf(1,3) = -s1      
      transf(2,0) =  0.Q0      
      transf(2,1) = -sf1      
      transf(2,2) =  cf1      
      transf(2,3) =  0.Q0     
      transf(3,0) =  -fac2     
      transf(3,1) =  fac1 * s1 * cf1     
      transf(3,2) =  fac1 * s1 * sf1  
      transf(3,3) =  fac1 * c1   
c
      do i=0,3
        qq1(i) = 0.Q0
c
        do j = 0,3
         qq1(i) = qq1(i) + transf(i,j)*qq(j)
        enddo
      enddo
c
      do i=0,3
        qq(i) = qq1(i)
      enddo
c
      return
      end
c ------------------------------------------------------------------


c****************************************************************************
c     multiplication of the 4x2x2 and 2x2 matrices with the (4)index fixed
c     mu - is this index; the result is 4x2x2 matrix
c
      subroutine matr1(mat1,mat2,mat3)
      implicit none    
c
      complex*32  mat1(0:3,2,2),mat2(2,2),mat3(0:3,2,2)
      integer i,j,k,mu
c
      do mu=0,3     
      do i=1,2
         do j=1,2
            mat3(mu,i,j)=qcmplx(0.Q0,0.Q0)
         enddo
      enddo
      enddo
c    
      do mu=0,3
      do i=1,2
         do j=1,2
            do k=1,2
               mat3(mu,i,j)=mat3(mu,i,j)+mat1(mu,i,k)*mat2(k,j)
            enddo
         enddo
      enddo
      enddo
c
      end
      
c---------------------------------------------------------------------
c     multiplication of the 2x2 and 4x2x2 matrices with the (4)index fixed
c     mu - is this index; the result is 4x2x2 matrix
c
      subroutine matr2(mat1,mat2,mat3)
      implicit none
c
      complex*32  mat1(2,2),mat2(0:3,2,2),mat3(0:3,2,2)
      integer i,j,k,mu
c
      do mu=0,3
      do i=1,2
         do j=1,2
            mat3(mu,i,j)=qcmplx(0.Q0,0.Q0)
         enddo
      enddo
      enddo
c   
      do mu=0,3
      do i=1,2
         do j=1,2
            do k=1,2
               mat3(mu,i,j)=mat3(mu,i,j)+mat1(i,k)*mat2(mu,k,j)
            enddo
         enddo
      enddo
      enddo
c
      end

c******************************************************************
c     multiplication of the 2x2 matrices
c
      subroutine matr(mat1,mat2,mat3)
      implicit none
      integer i,j,k
      complex*32  mat1(2,2),mat2(2,2),mat3(2,2)
c
      do i=1,2
         do j=1,2
            mat3(i,j)=(0.Q0,0.Q0)
         enddo
      enddo
c   
      do i=1,2
         do j=1,2
            do k=1,2
               mat3(i,j)=mat3(i,j)+mat1(i,k)*mat2(k,j)
            enddo
         enddo
      enddo
c
      end

c****************************************************************************
c     2x2 matrix subtraction
c
      subroutine minmat(mat1,mat2,mat3)
      implicit none
      integer i,j
      complex*32 mat1(2,2),mat2(2,2),mat3(2,2)
c
      do i=1,2
         do j=1,2
            mat3(i,j)=mat1(i,j)-mat2(i,j)
         enddo
      enddo
c
      end
      
c****************************************************************
c     2x2 matrix adding two matrices
c
      subroutine dodmat(mat1,mat2,mat3)
      implicit none
      integer i,j
      complex*32 mat1(2,2),mat2(2,2),mat3(2,2)
c
      do i=1,2
         do j=1,2
            mat3(i,j)=mat1(i,j)+mat2(i,j)
         enddo
      enddo
c
      end
      
c****************************************************************
      subroutine plus(vect,mat)
      implicit none
      real*16 vect(0:3)
      complex*32 mat(2,2)
         mat(1,1) = vect(0)-vect(3)
         mat(1,2) = qcmplx(-vect(1),vect(2))
         mat(2,1) = qcmplx(-vect(1),-vect(2))
         mat(2,2) = vect(0)+vect(3)
      return
      end
      
c****************************************************************
      subroutine minus(vect,mat)
      implicit none
      real*16 vect(0:3)
      complex*32 mat(2,2)
         mat(1,1) = vect(0)+vect(3)
         mat(1,2) = qcmplx(vect(1),-vect(2))
         mat(2,1) = qcmplx(vect(1),vect(2))
         mat(2,2) = vect(0)-vect(3)
      return
      end   
      
c****************************************************************
      subroutine cplus(vect,mat)
      implicit none
      complex*32 vect(0:3),mat(2,2)
         mat(1,1) = vect(0)-vect(3)
         mat(1,2) = -vect(1)+qcmplx(0.Q0,1.Q0)*vect(2)
         mat(2,1) = -vect(1)-qcmplx(0.Q0,1.Q0)*vect(2)
         mat(2,2) = vect(0)+vect(3)
      return
      end
      
c****************************************************************
      subroutine cminus(vect,mat)
      implicit none
      complex*32 vect(0:3),mat(2,2)
         mat(1,1) = vect(0)+vect(3)
         mat(1,2) = vect(1)-qcmplx(0.Q0,1.Q0)*vect(2)
         mat(2,1) = vect(1)+qcmplx(0.Q0,1.Q0)*vect(2)
         mat(2,2) = vect(0)-vect(3)
      return
      end   
      
c**************************************************************

      real*16 function dot(a,b)
      implicit none

      real*16 a(0:3), b(0:3)
      dot = a(0)*b(0)-a(1)*b(1)-a(2)*b(2)-a(3)*b(3)
      return
      end
      
c**************************************************************

      complex*32 function dotc(a,b)
      implicit none

      real*16 a(0:3)
      complex*32 b(0:3)
      dotc = a(0)*b(0)-a(1)*b(1)-a(2)*b(2)-a(3)*b(3)
      return
      end
      
c**************************************************************

      complex*32 function dotcc(a,b)
      implicit none  
 
      complex*32 a(0:3),b(0:3)
      dotcc = a(0)*b(0)-a(1)*b(1)-a(2)*b(2)-a(3)*b(3)
      return
      end
      
c**************************************************************

      subroutine Ig(I1,gammus,m,n,r,z,I1g)
      implicit none
      
      complex*32 I1(0:3,2,2),gammus(0:3,2,2),I1g(2,2,2,2)
      integer m,n,r,z,ksi
         
      I1g(m,n,r,z) = I1(0,m,n)*gammus(0,r,z)
        
      do ksi=1,3
      I1g(m,n,r,z)=I1g(m,n,r,z) - I1(ksi,m,n)*
     *gammus(ksi,r,z)
      enddo
        
      end
c-------------------------------------------------------------

      subroutine Igg(vect,gammus,r,z,pi1g)      
      implicit none
      real*16 vect(0:3)
      complex*32 gammus(0:3,2,2),pi1g(2,2)
      integer r,z,ksi
         
      pi1g(r,z) = vect(0)*gammus(0,r,z)
        
      do ksi=1,3
      pi1g(r,z) = pi1g(r,z) - vect(ksi)*gammus(ksi,r,z)
      enddo

      end
c----------------------------------------------------------------------
  
      subroutine plus1(vect,mat)
      implicit none
      complex*32 vect(0:3)
      complex*32 mat(2,2)
         mat(1,1) = vect(0)-vect(3)
         mat(1,2) = -vect(1)+qcmplx(0.Q0,1.Q0)*vect(2)
         mat(2,1) = -vect(1)-qcmplx(0.Q0,1.Q0)*vect(2)
         mat(2,2) = vect(0)+vect(3)
      return
      end
c----------------------------------------------------------------------
      
      subroutine minus1(vect,mat)
      implicit none

      complex*32 vect(0:3)
      complex*32 mat(2,2)
         mat(1,1) = vect(0)+vect(3)
         mat(1,2) = vect(1)-qcmplx(0.Q0,1.Q0)*vect(2)
         mat(2,1) = vect(1)+qcmplx(0.Q0,1.Q0)*vect(2)
         mat(2,2) = vect(0)-vect(3)
      return
      end   

c--------------------------------------------------------------------
c     multiplication of a 2x2 matrix by a constant
c
      subroutine conmat(alfa,mat,amat)
      implicit none

      integer i,j
c
      complex*32 alfa
      complex*32 mat(2,2),amat(2,2)
c
      do i=1,2
         do j=1,2
            amat(i,j)=alfa*mat(i,j)
         enddo
      enddo
c
      end

c---------------------------------------------------------------------
c     multiplication of a 2x2 matrix by a 4-vector; the result is 4x2x2 matrix
c
c
      subroutine vecmat(vec,mat,vmat)
      implicit none
c
      real*16 vec(0:3)
      complex*32 mat(2,2),vmat(0:3,2,2)
      integer mu,i,j

      do mu=0,3
      do i=1,2
         do j=1,2
            vmat(mu,i,j)=vec(mu)*mat(i,j)
         enddo
      enddo
      enddo
c
      end

c---------------------------------------------------------------------
c     multiplication of a 4x2x2 matrix by a factor
c
c
      subroutine fac4mat(fac,mat,fmat)
      implicit none
c
      real*16 fac
      complex*32 mat(0:3,2,2),fmat(0:3,2,2)
      integer mu,i,j

      do mu=0,3
      do i=1,2
         do j=1,2
            fmat(mu,i,j)=fac*mat(mu,i,j)
         enddo
      enddo
      enddo
c
      end

c---------------------------------------------------------------------
c     multiplication of a 4x2x2 matrix by a complex factor
c
c
      subroutine fac4matc(fac,mat,fmat)
      implicit none

      complex*32 fac
      complex*32 mat(0:3,2,2),fmat(0:3,2,2)
      integer mu,i,j

      do mu=0,3
      do i=1,2
         do j=1,2
            fmat(mu,i,j)=fac*mat(mu,i,j)
         enddo
      enddo
      enddo
c
      end

c---------------------------------------------------------------------
c      adding two  4-vectors
c
      subroutine vecplvec(vec1,vec2,vec3)
      implicit none

      integer mu
      real*16 vec1(0:3),vec2(0:3),vec3(0:3)
      
      do mu=0,3
            vec3(mu)= vec1(mu) + vec2(mu)
      enddo
      end

c---------------------------------------------------------------------
c      substracting two  4-vectors
c
      subroutine vecmivec(vec1,vec2,vec3)
      implicit none

      integer mu
      real*16 vec1(0:3),vec2(0:3),vec3(0:3)
      
      do mu=0,3
            vec3(mu)= vec1(mu) - vec2(mu)
      enddo
      end

c---------------------------------------------------------------------
c     multiplication of  a 4-vector by constant
c
      subroutine convec(alpha,vec,vec1)
      implicit none

      integer mu
      real*16 vec(0:3),vec1(0:3)
      real*16 alpha

      do mu=0,3
            vec1(mu)=alpha*vec(mu)
      enddo
      end

c--------------------------------------------------------------------
c     4x2x2 matrix subtraction
c
      subroutine min4mat(mat1,mat2,mat3)
      implicit none

      integer mu,i,j
      complex*32 mat1(0:3,2,2),mat2(0:3,2,2),mat3(0:3,2,2)

      do mu=0,3
      do i=1,2
         do j=1,2
            mat3(mu,i,j)=mat1(mu,i,j)-mat2(mu,i,j)
         enddo
      enddo
      enddo
      end

c--------------------------------------------------------------------
c     4x2x2 matrix addition
c
      subroutine dod4nmat(mat1,mat2,mat3)
      implicit none

      integer mu,i,j
      complex*32 mat1(0:3,2,2),mat2(0:3,2,2),mat3(0:3,2,2)

      do mu=0,3
      do i=1,2
         do j=1,2
            mat3(mu,i,j)=mat1(mu,i,j)+mat2(mu,i,j)
         enddo
      enddo
      enddo
      end



c-------------------------------------------------------------
c     8>< ---          End of file                     --- ><8
c-------------------------------------------------------------
