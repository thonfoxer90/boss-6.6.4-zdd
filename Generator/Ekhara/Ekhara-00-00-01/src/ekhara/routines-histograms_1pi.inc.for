c----------------------------------------------------------------------
c     (c) 2009, S.Ivashyn
c     Histograming routines.
c
c     Note: the file to save the histograms is opened for writing
c           and closed after that in the routine <hist_fin>.
c
c     Note: IO 70 is used for histogram settings
c           IO 71 is used for writing the histograms
c
c----------------------------------------------------------------------

      subroutine hist_init_1pi(file_settings)
      !             file_settings: filename for histogram settings
      !                            see example settings file.
      !             num_histos: 1 to 20  if need histogramming,
      !                         0        for ignoring all histograming requests
      !
      ! Initializes the histogramming procedures from the scratch,
      !  i.e., reads the configuration file and then call flushing the histogram-data
      ! It is necessary to call this routine BEFORE any hisogramming requests!
      !
      implicit none
      include 'common.ekhara.inc.for'
      character file_settings*(*)
      integer      num_histos
      integer i

      ! If we are going to ignore the histogramming,
      ! remember this setting and quit.
      if (DoHisto.eq.0) then
            ! Reset the counters of all contributions
            sum1 =0.Q0
            sum2 =0.Q0
            nHistosH = 0
            return 
      endif

      ! Open settings file
      open (70,file=file_settings,status='UNKNOWN')
      ! Skip the header line
      read (70,*)
      read (70,*)

      ! Read how many histograms we need
      read (70,*) num_histos
      nHistosH = num_histos

      ! Exit, if we do not need any histogram
      if (num_histos.eq.0) then
            DoHisto = 0
            ! Reset the counters of all contributions
            sum1 =0.Q0
            sum2 =0.Q0
            close (70)
            return 
      endif

      ! read the settings for <nHistosH> histograms
      do i = 1,nHistosH
         ! Skip the separator line
         read (70,*)
         ! read title (Character*51)
         read (70,*) histofilename(i)
         ! read limits (real*16) and number of bins (integer)
         read (70,*) loLimH(i),upLimH(i),nBinsH(i)

         !normH(i) = 1.Q0
         !normH(i) = 0.389379292Q6  ! gev2nbarn=.389379292Q6       nano
         normH(i) = 0.389379292Q12 ! gev2fbarn=.389379292Q12      femto
      enddo
      close (70)

      ! Reset all histograms to initial values
      call hist_FLUSH_1pi

      return
      end

c**********************************************************************

      subroutine hist_FLUSH_1pi
      !
      ! Prepares all histograms to receive contributions:
      !  flushes the data of each bin and adjusting the bins.
      ! It is called automaticaly by routine <hist_init>,
      !  thus usually you do not need to call it manualy.
      ! However, you may want to call it if you want just
      !  to flush the bin data, not reading the configuration file.
      !
      implicit none
      include 'common.ekhara.inc.for'
      integer binN, histogramN
      
      integer binN_1, binN_2 ! for 3D-bargraph (t1, t2)

      ! Reset the counters of all contributions
      sum1 =0.Q0
      sum2 =0.Q0

      ! Reset each bin value and adjust the lower and upper bounds of each bin
      do histogramN=1, nHistosH
            do binN=1, nBinsH(histogramN)
                  bins(histogramN,binN) = 0.Q0

                  bins2(histogramN,binN) = 0.Q0

                  loBin(histogramN,binN) =
     &              loLimH(histogramN)
     &                +(upLimH(histogramN) - loLimH(histogramN))
     &                  /float(nBinsH(histogramN))
     &                  *float(binN-1)

                  upBin(histogramN,binN) =
     &              loLimH(histogramN)
     &                +(upLimH(histogramN) - loLimH(histogramN))
     &                  /float(nBinsH(histogramN))
     &                  *float(binN)

            enddo
      enddo

      ! Prepare histograms for 3D-bargraph (t1, t2)
      do binN_1=1, nBinsH(2)
          do binN_2=1, nBinsH(3)
             bins_3d(binN_1,binN_2) = 0.Q0
             bins2_3d(binN_1,binN_2) = 0.Q0
      
             loBin_1_3d(binN_1,binN_2) = loBin(2,binN_1)
             loBin_2_3d(binN_1,binN_2) = loBin(3,binN_2)

             upBin_1_3d(binN_1,binN_2) = upBin(2,binN_1)
             upBin_2_3d(binN_1,binN_2) = upBin(3,binN_2)
          enddo
      enddo

      ! Now do the user-specific preparation of histograms..
      ! (e.g., customize binning, fix edge-effects...)
      call hist_prepare_user_1pi

      return
      end

c**********************************************************************

      subroutine hist_add_1pi(value,h_var)
      !            value: contribution,
      !                   e.g., 1.Q0   --- count the number of events
      !                         dsigma --- calculate differential cross section
      !            h_var: "vector" of histogramming variables,
      !                   e.g., (qq, cos_theta, E_1, ...)
      !
      ! Adds a contribution "value" to the histograms.
      !
      implicit none 
      include 'common.ekhara.inc.for'
      real*16 value
      real*16 h_var(1:20)
      integer binN, histogramN

      integer binN_1, binN_2 ! for 3D-bargraph (t1, t2)

      ! Update the counters of all contributions
      sum1 = sum1 + value
      sum2 = sum2 + value**2

      ! If we ignore the histogramming, just quit.
      if (DoHisto.eq.0) then
         return 
      endif

      ! Add contribution "value" to each histogram into appropriate bins
      do histogramN=1, nHistosH
       do binN=1, (nBinsH(histogramN)-1)
        if (  (h_var(histogramN).ge.loBin(histogramN,binN))
     &   .and.(h_var(histogramN).lt.upBin(histogramN,binN))) then

           bins(histogramN,binN) = bins(histogramN,binN) + value
           bins2(histogramN,binN)= bins2(histogramN,binN)+ value**2
           
           ! when we calculate (dsig/dQ^2) we need to take it twice
           ! (see discussion in the documentation,
           !  regarding the single-tag observables)
           if (histogramN .eq. 15) then
               bins(histogramN,binN) = bins(histogramN,binN) - value
               bins2(histogramN,binN)= bins2(histogramN,binN)- value**2          

               bins(histogramN,binN) = bins(histogramN,binN) +2.Q0
               bins2(histogramN,binN)= bins2(histogramN,binN)+4.Q0
           endif
           
        endif
       enddo
      enddo
      do histogramN=1, nHistosH
        binN=nBinsH(histogramN)
        if (  (h_var(histogramN).ge.loBin(histogramN,binN))
     &   .and.(h_var(histogramN).le.upBin(histogramN,binN))) then

           bins(histogramN,binN) = bins(histogramN,binN) + value
           bins2(histogramN,binN)= bins2(histogramN,binN)+ value**2
           
           ! when we calculate (dsig/dQ^2) we need to take it twice
           ! (see discussion in the documentation,
           !  regarding the single-tag observables)
           if (histogramN .eq. 15) then
               bins(histogramN,binN) = bins(histogramN,binN) - value
               bins2(histogramN,binN)= bins2(histogramN,binN)- value**2          

               bins(histogramN,binN) = bins(histogramN,binN) +2.Q0
               bins2(histogramN,binN)= bins2(histogramN,binN)+4.Q0
           endif
           
        endif
      enddo

      ! Add contribution to histogram for 3D-bargraph (t1, t2)
      do binN_1=1, nBinsH(2)
        do binN_2=1, nBinsH(3)
           if (  (h_var(2) .ge. loBin_1_3d(binN_1,binN_2))
     &      .and. (h_var(2) .lt. upBin_1_3d(binN_1,binN_2))
     &      .and. (h_var(3) .ge. loBin_2_3d(binN_1,binN_2))
     &      .and. (h_var(3) .lt. upBin_2_3d(binN_1,binN_2))
     &          )  then   
             bins_3d(binN_1,binN_2) = bins_3d(binN_1,binN_2) + value
             bins2_3d(binN_1,binN_2)= bins2_3d(binN_1,binN_2)+ value**2
           endif
        enddo
      enddo

      return
      end

c**********************************************************************

      subroutine hist_fin_1pi(tot_n_evts,norm_cs)
      implicit none
      include 'common.ekhara.inc.for'
      !          tot_n_evts : total number of generated events
      !                       (includes also non-histogrammed events)
      !
      ! Finalizes the histograms and saves the results into files.
      !
      real*16  tot_n_evts,norm_cs
      integer binN, histogramN

      integer binN_1, binN_2 ! for 3D-bargraph (t1, t2)
      real*16 var_1, var_2   ! for 3D-bargraph (t1, t2)

      real*16  v_bin, err_bin, var

      ! If we ignore the histogramming, just quit.
      if (DoHisto.eq.0) then
         return
      endif

      ! 88 = histograms_control.out  === to be used for test-run scripts
      !                              === it contains ALL histograms
      open (88,file='output/histograms_control.out',status='UNKNOWN')
      
      ! For each histogram: perform normalization and save to file
      do histogramN=1, nHistosH
        open (71,file=histofilename(histogramN),status='UNKNOWN')
        write(71,*)
        write(71,*)'### EKHARA histogram',histofilename(histogramN)
        write(71,*)'### '
        write(71,*)'### binlow   binhigh   bincenter=x   dsig/dx   err'
        write(71,*)'### '
        write(71,*)

        write(88,*)

        do binN=1, nBinsH(histogramN)

            ! Normalize by the total number of generated events
            v_bin   = bins(histogramN,binN)/(tot_n_evts)
            err_bin = sqrt( (bins2(histogramN,binN)/(tot_n_evts)
     &                       - v_bin**2)
     &                     /(tot_n_evts-1.Q0) )

            ! Normalize by the histogram limits
            v_bin   = v_bin
     &            /(upBin(histogramN,binN) - loBin(histogramN,binN))
            err_bin = err_bin
     &            /(upBin(histogramN,binN) - loBin(histogramN,binN))

            ! Normalize to differential cross section
            v_bin   = v_bin   * norm_cs
            err_bin = err_bin * norm_cs

            ! Normalize by extra factor (e.g. convert to [nb] or [fb])
            v_bin   = v_bin   * normH(histogramN)
            err_bin = err_bin * normH(histogramN)

            ! Calculate the histogramming variable value for current bin
            !  (central value at bin)
            var = loBin(histogramN,binN) +
     &         (upBin(histogramN,binN) - loBin(histogramN,binN))*0.5Q0
            
            ! Write to file
            write (71,95) loBin(histogramN,binN), ' ',
     &                    upBin(histogramN,binN), ' ', 
     &                    var, ' ', v_bin, ' ', err_bin

            write (88,95) loBin(histogramN,binN), ' ',
     &                    upBin(histogramN,binN), ' ', 
     &                    var, ' ', v_bin, ' ', err_bin
        enddo
        write(71,*)'### '
        close(71)
      enddo

      close(88)


      ! Finalize the histogram for 3D-bargraph (t1, t2)
      ! (double-differential cross section)
        open (71,file="output/histo3D.dat",status='UNKNOWN')
        write(71,*)
        write(71,*)'### EKHARA histogram',histofilename(histogramN)
        write(71,*)'### ( prepared for 3D-bar plotting wiht GNUPLOT )'
        write(71,*)'### Notice: we do not give the errors here!'
        write(71,*)'### '
        write(71,*)'### t1     t2     dsig/(dt1 dt2)       dt1      dt2'   
        write(71,*)'### [GeV^2][GeV^2] [fb GeV^-4]      [GeV^2] [GeV^2]'   
        write(71,*)'### '
        write(71,*)
      do binN_1=1, nBinsH(2)
        do binN_2=1, nBinsH(3)

            ! Normalize by the total number of generated events
            v_bin   = bins_3d(binN_1,binN_2)/(tot_n_evts)

            ! Normalize by the histogram limits
            v_bin   = v_bin
     &         /(upBin_1_3d(binN_1,binN_2) - loBin_1_3d(binN_1,binN_2))
     &         /(upBin_2_3d(binN_1,binN_2) - loBin_2_3d(binN_1,binN_2))

            ! Normalize to differential cross section
            v_bin   = v_bin   * norm_cs

            ! Normalize by extra factor (e.g. convert to [nb] or [fb])
            v_bin   = v_bin   * 0.389379292Q12 ! gev2fbarn=.389379292Q12

            ! Calculate the histogramming variable value for current bin
            !  (central value at bin)
            var_1 = loBin_1_3d(binN_1,binN_2) +
     &         (upBin_1_3d(binN_1,binN_2) - loBin_1_3d(binN_1,binN_2))
     &          *0.5Q0
            var_2 = loBin_2_3d(binN_1,binN_2) +
     &         (upBin_2_3d(binN_1,binN_2) - loBin_2_3d(binN_1,binN_2))
     &          *0.5Q0

            ! Write to file
            write (71,95) loBin_1_3d(binN_1,binN_2), ' ',
     &                    loBin_2_3d(binN_1,binN_2), ' ', 
     &                    v_bin, ' ', 
     &      abs((upBin_1_3d(binN_1,binN_2)-loBin_1_3d(binN_1,binN_2))),
     &         ' ', 
     &      abs((upBin_2_3d(binN_1,binN_2)-loBin_2_3d(binN_1,binN_2)))
     
            write (71,95) loBin_1_3d(binN_1,binN_2), ' ',
     &                    upBin_2_3d(binN_1,binN_2), ' ', 
     &                    v_bin, ' ', 
     &      abs((upBin_1_3d(binN_1,binN_2)-loBin_1_3d(binN_1,binN_2))),
     &         ' ', 
     &      abs((upBin_2_3d(binN_1,binN_2)-loBin_2_3d(binN_1,binN_2)))
        enddo
        write (71,*) ' '
        do binN_2=1, nBinsH(3)

            ! Normalize by the total number of generated events
            v_bin   = bins_3d(binN_1,binN_2)/(tot_n_evts)

            ! Normalize by the histogram limits
            v_bin   = v_bin
     &         /(upBin_1_3d(binN_1,binN_2) - loBin_1_3d(binN_1,binN_2))
     &         /(upBin_2_3d(binN_1,binN_2) - loBin_2_3d(binN_1,binN_2))

            ! Normalize to differential cross section
            v_bin   = v_bin   * norm_cs

            ! Normalize by extra factor (e.g. convert to [nb] or [fb])
            v_bin   = v_bin   * 0.389379292Q12 ! gev2fbarn=.389379292Q12

            ! Calculate the histogramming variable value for current bin
            !  (central value at bin)
            var_1 = loBin_1_3d(binN_1,binN_2) +
     &         (upBin_1_3d(binN_1,binN_2) - loBin_1_3d(binN_1,binN_2))
     &          *0.5Q0
            var_2 = loBin_2_3d(binN_1,binN_2) +
     &         (upBin_2_3d(binN_1,binN_2) - loBin_2_3d(binN_1,binN_2))
     &          *0.5Q0

            ! Write to file
            write (71,95) upBin_1_3d(binN_1,binN_2), ' ',
     &                    loBin_2_3d(binN_1,binN_2), ' ', 
     &                    v_bin, ' ', 
     &      abs((upBin_1_3d(binN_1,binN_2)-loBin_1_3d(binN_1,binN_2))),
     &         ' ', 
     &      abs((upBin_2_3d(binN_1,binN_2)-loBin_2_3d(binN_1,binN_2)))
            write (71,95) upBin_1_3d(binN_1,binN_2), ' ',
     &                    upBin_2_3d(binN_1,binN_2), ' ', 
     &                    v_bin, ' ', 
     &      abs((upBin_1_3d(binN_1,binN_2)-loBin_1_3d(binN_1,binN_2))),
     &         ' ', 
     &      abs((upBin_2_3d(binN_1,binN_2)-loBin_2_3d(binN_1,binN_2)))
        enddo
        write (71,*) ' '
      enddo
        write(71,*)'### '
        close(71)
      
!  95  format(1pe12.5,A,1pe12.5,A,1pe12.5,A,1pe12.5,A,1pe12.5)
  95  format(1pe17.10,A,1pe17.10,A,1pe17.10,A,1pe17.10,A,1pe17.10)

      return
      end

c    8>< ------------------  End of file  ------------------------- ><8
