c-----------------------------------------------------------------------
c     EKHARA. include file.     e+e- -> e+e-pi pi
c
c     Here:      - Main MC loop
c                - basic phase space cuts
c                - some auxiliary routines
c                - Matrix element
c                - Ph.Sp. generator
c-----------------------------------------------------------------------

c**********************************************************************
! Main MC loop for   e+e- -> e+e-pi pi
!
! Important: with this we can produce only WEIGHTED events
!
      subroutine mc_loop_2pi
      implicit none
      include 'common.ekhara.inc.for'
      complex*32 PionFormFactor
      logical accepted

      real*8 rr(11),rrc(0:10)      ! generated random numbers
      real*16 fac,ff       ! variables related to matrix element 
      integer ic               ! loop variable
      real*16 ww1              ! for very long loop we take it real

      real*16 Msq_c

      CHARACTER msg*78         ! for logging

      FF_pion = 0
      PFFS = PionFormFactor(s)

      msg = ' '
      write(msg,*)'MC iterations to be done =',nges
      call runlog(msg)

      ww1=nges 

c ======================================================================
c ----------------------------------------------------------------------
c ----                        MC loop                               ----
      do  while (ww1 .gt. 0.Q0)
            ww1=ww1 - 1.Q0
            call animate(ww1,nges)
            call ranlxdf(rrc,11)  ! creating random variables (RANLUX)

            do ic=0,10
                  rr(ic+1) = rrc(ic)
            enddo

      !     p = (E,0,0,-p) - initial electron four-momentum
            constant = 1.1Q0
            if(qext(rr(11)).le.(1.Q0/(2.Q0+constant))) then
              call phsp1(fac,rr)
            elseif(qext(rr(11)).le.(2.Q0/(2.Q0+constant))) then
              p1(3) = -sqrt((ss+2.Q0*me)*(ss-2.Q0*me))/2.Q0 !z axis along p2!!!
              p2(3) = -p1(3)
              call phsp2(fac,rr)
            else
              call phsp3(fac,rr)
            endif

            ! apply kinematic cuts
            call eventselection_2pi(accepted)  
            
            if (accepted) then
                PFF = PionFormFactor(ququ)
                call scprod
                call spinory_f
                call evalgammu
                call matrixelmt(Msq_c)
                ff =   fac*Msq_c
                if (ff.lt.0.Q0) then
                   msg = ' '
                   write(msg,*) ' ff = ',ff, ' fac = ',fac
                   call warnlog(msg)
                endif
            else
                ff = 0.Q0
            endif
                
            call histo_event_2pi(ff) ! "_2pi" works with weighted histograms

      enddo    
c ----                      end of MC loop                          ----
c ----------------------------------------------------------------------
c ======================================================================
      ! report the results
      call tellSIGMA_2pi

      return
      end
c**********************************************************************




c-----------------------------------------------------------------------
c     8>< ---          Basic phase space cuts.              --- ><8
c-----------------------------------------------------------------------

      subroutine eventselection_2pi(accepted)
      !                   here the cuts due to input file are done
      implicit none
      include 'common.ekhara.inc.for'

      logical accepted
      logical accepted_extra

      real*16 pi1lab,pi2lab,pipicut1


      ! ----------------- do basic cuts
      !     p1,p2 - four momenta of initial positon and electron
      !     q1,q2 - four momenta of final positon and electron
      !     pi1,pi2 - four momenta of pions (pi^+ and pi^-)

      pi1lab = acos(pi1(3)/
     &         Sqrt(pi1(1)**2 + pi1(2)**2 + pi1(3)**2))
     &         *180.Q0/pi
      pi2lab = acos(pi2(3)/
     &         Sqrt(pi2(1)**2 + pi2(2)**2 + pi2(3)**2))
     &         *180.Q0/pi
                                                        
      pipicut1 = acos( qu(3)/Sqrt(qu(1)**2 +
     1           qu(2)**2 + qu(3)**2) )*180.Q0/pi
 
      accepted = (
     & (((pi1lab.ge.pi1lab_min).and.(pi1lab.le.pi1lab_max)).and.
     &  ((pi2lab.ge.pi1lab_min).and.(pi2lab.le.pi1lab_max)))
     &  .and.
     & ((pipicut1.lt.pipicut1_min).or.(pipicut1.gt.pipicut1_max))
     &    ) 
 
      ! ----------------- do more cuts
      if (accepted) then
       call ExtraCuts_2pi(accepted_extra)  ! => see routines-user.inc.for
       accepted = (accepted .and. accepted_extra)
      endif
     
      return
      end
c**********************************************************************



c-----------------------------------------------------------------------
c     8>< ---          Some auxiliary routines              --- ><8
c-----------------------------------------------------------------------

c**********************************************************************
      subroutine init_2pi
      implicit none
      include 'common.ekhara.inc.for'

      ! This mode is implemented only in helicity-framework!
      ! Prepare the initial helicity-framework objects 
      !         (spinors, sigma-matr.)
      call init_ha

      ! creating spinors for initial e+ e-
      call spinory_i

c-----constants necessary in phsp1,2,3-------------------------
      ququmax= (ss- 2.Q0*me)**2
      dququdr2 = ququmax - 4.Q0*mpi2 
      logg = 2.Q0*log(2.Q0*me/ss)
      aaamin = (logg)**3
      stala = 1000.Q0
      bbbmin = -stala*Log((ss-2.Q0*me)*(ss+2.Q0*me)/ss**2)
      wwmin = -(ss-2.Q0*me-2.Q0*mpi)**2/2.Q0 ! wwmax = 0
      zzmax = -1.Q0/3.Q0/(ss*(ss-2.Q0*me))**3
      czynnik = 1.Q0/4.Q0/ss/Sqrt((ss/2.Q0 - me)*(ss/2.Q0 + me))
     &      /4.Q0* gev2nbarn*(4.Q0*pi*alpha)**4
      czyn_pi = 1.Q0/(8.Q0*pi)**3/me2

c----------PionFormFactor parameters ----------------------

      if (FF_pion.eq.0) then  !------------- KS PionFormFactor ---------
            c_0_pion = 1.171Q0
            c_1_pion =-0.1194Q0
            c_2_pion = 0.011519Q0
            c_3_pion =-0.0437612Q0
            c_n_pion =-0.0193578Q0
            c_om_pion= 0.00184Q0
            m_rho0_pion = 0.773945036Q0
            g_rho0_pion = 0.144923Q0
            m_rho1_pion = 1.35671Q0
            g_rho1_pion = 0.43685Q0
            m_rho2_pion = 1.7Q0
            g_rho2_pion = 0.24Q0
            m_rho3_pion = 0.7755Q0*sqrt(7.Q0)
            g_rho3_pion = 0.2Q0*m_rho3_pion
            m_om0_pion  = 0.783Q0
            g_om0_pion  = 0.0084Q0      
      elseif (FF_pion.eq.1) then  !------------- GS PionFormFactor ---------
            c_0_pion = 1.0979Q0
            c_1_pion =-0.069164Q0
            c_2_pion = 0.021579Q0
            c_3_pion =-0.0309572Q0
            c_n_pion =-0.0193578Q0
            c_om_pion= 0.00195Q0
            m_rho0_pion = 0.77628Q0
            g_rho0_pion = 0.15053Q0
            m_rho1_pion = 1.3806Q0
            g_rho1_pion = 0.33981Q0
            m_rho2_pion = 1.7Q0
            g_rho2_pion = 0.24Q0
            m_rho3_pion = 0.7755Q0*sqrt(7.Q0) !!!
            g_rho3_pion = 0.2Q0*m_rho3_pion   !!!
            m_om0_pion  = 0.783Q0
            g_om0_pion  = 0.0084Q0      
      endif
c----------PionFormFactor parameters  - for 2 gamma exchange diagrams--
      mrho = 0.7685Q0      ! GeV ---------- Rho mass
      mrhol = 1.37Q0       ! GeV ---------- Rho' mass
      momega = 0.78194Q0   ! GeV ---------- Omega mass
      al = 1.85Q-3         ! --------- Pion form factor parameters a
      be = -0.145Q0        ! --------- and b: Kuehn, Santamaria, ZPC48(1990)445
c----------------------------------------------------------
      return
      end 

c--------------------------------------------------------------
      subroutine scprod
      implicit none
      include 'common.ekhara.inc.for'
      
      integer kl
      complex*32 gammab(0:3)

       p1qu = p1(0)*qu(0)-p1(3)*qu(3)

      do kl=0,3
      gamma(kl)=qcmplx(0.Q0,1.Q0)*PFF*
     &         (2.Q0*pi1(kl)-qu(kl))      
      gammab(kl)=conjg(gamma(kl))
      enddo

      taugamma = ss*gamma(0)
      qupgamma = (qu(0)+q1(0))*gamma(0)-(qu(1)+q1(1))*gamma(1)
     &          -(qu(2)+q1(2))*gamma(2)-(qu(3)+q1(3))*gamma(3)
      p1gamma = p1(0)*gamma(0)-p1(3)*gamma(3)
      taugammab = ss*gammab(0)
      qupgammab = (qu(0)+q1(0))*gammab(0)-(qu(1)+q1(1))*gammab(1)
     &           -(qu(2)+q1(2))*gammab(2)-(qu(3)+q1(3))*gammab(3)
      p1gammab = p1(0)*gammab(0)-p1(3)*gammab(3)

      gamma2 = gamma(0)*gammab(0)-gamma(1)*gammab(1)
     &        -gamma(2)*gammab(2)-gamma(3)*gammab(3)
      return
      end

c **********************************************************************

      complex*32 function PionFormFactor(a)
      implicit none
      include 'common.ekhara.inc.for'
      
      real*16 a
      complex*32 BW,BW_rho,BW_GS,BW_om,c_sum,appr_sum

      c_sum = appr_sum(a)

      if (FF_Pion.eq.0) then
      PionFormFactor = 
     1   c_0_pion*BW_rho(m_rho0_pion,g_rho0_pion,a,1)/
     2     (1.Q0+c_om_pion)*
     1     (1.Q0+c_om_pion*BW_rho(m_om0_pion,g_om0_pion,a,1)) 
     4 + c_1_pion*BW_rho(m_rho1_pion,g_rho1_pion,a,1) 
     3 + c_2_pion*BW_rho(m_rho2_pion,g_rho2_pion,a,1) 
     4 + c_3_pion*BW_rho(m_rho3_pion,g_rho3_pion,a,1) 
     5 + c_sum

      elseif (FF_Pion.eq.1) then
      PionFormFactor = 
     1   c_0_pion*BW_GS(m_rho0_pion,g_rho0_pion,a,1)/
     2     (1.Q0+c_om_pion)*
     1     (1.Q0+c_om_pion*BW_rho(m_om0_pion,g_om0_pion,a,1)) 
     2 + c_1_pion*BW_GS(m_rho1_pion,g_rho1_pion,a,1) 
     3 + c_2_pion*BW_GS(m_rho2_pion,g_rho2_pion,a,1) 
     4 + c_3_pion*BW_rho(m_rho3_pion,g_rho3_pion,a,1) 
     5 + c_sum
      endif

      return
      end

c **********************************************************************

      complex*32 function BW_rho(m,breite,x,kx)
      implicit none
      include 'common.ekhara.inc.for'
      
      integer kx
      real*16 m,breite,x,g
      complex*32 i

         g=breite*m*m/x*(x-4.Q0*mpi*mpi)**(1.5Q0)/
     &     (m*m-4.Q0*mpi*mpi)**(1.5Q0)

      i=(0.Q0,1.Q0)
      if (kx.eq.1) then
         BW_rho=m*m/(m*m-x-i*sqrt(x)*g)
      else
         BW_rho=m*m/(m*m-x+i*sqrt(x)*g)
      endif
      return
      end

c **********************************************************************

      complex*32 function BW_GS(m,breite,x,kx)
      implicit none
      include 'common.ekhara.inc.for'

      integer kx
      real*16 m,breite,x,g,HH_GS,dd,p_rho
      complex*32 i

         g=breite*m*m/x*(x-4.Q0*mpi*mpi)**(1.5Q0)/
     &     (m*m-4.Q0*mpi*mpi)**(1.5Q0)

      i=(0.Q0,1.Q0)

      p_rho = sqrt(m*m-4.Q0*mpi*mpi) / 2.Q0

      dd = 3.Q0/2.Q0/pi * mpi**2 / p_rho**2 
     1   * log( ( m + 2.Q0*p_rho )**2 / 4.Q0/mpi**2 )
     2   + m/2.Q0/pi/p_rho 
     3   - mpi**2 * m /pi/p_rho**3

      if (kx.eq.1) then
         BW_GS=(m*m+dd*breite*m)/(m*m-x+HH_GS(m,breite,x)-i*sqrt(x)*g)
      else
         BW_GS=(m*m+dd*breite*m)/(m*m-x+HH_GS(m,breite,x)+i*sqrt(x)*g)
      endif
  
      return
      end

c **********************************************************************

      real*16 function HH_GS(m,breite,x)
      implicit none
      include 'common.ekhara.inc.for'
      
      real*16 breite,x,HH_p,poch,vv_rho,m

      vv_rho = sqrt( 1.Q0 - 4.Q0*mpi**2/m**2 ) 

      poch = m**2 * breite / pi / (m**2-4.Q0*mpi**2)**(1.5Q0)
     1     * ( vv_rho**2 + vv_rho/2.Q0*(3.Q0-vv_rho**2) 
     2       * log( (1.Q0+vv_rho)**2/4.Q0/mpi**2 * m**2) )
      
      HH_GS = HH_p(m,breite,x) - HH_p(m,breite,m**2) 
     1   - ( x - m**2 ) * poch

      return
      end

c **********************************************************************

      real*16 function HH_p(m,breite,par)
      implicit none
      include 'common.ekhara.inc.for'
      
      real*16 breite,par,vv,m

      vv = sqrt( 1.Q0 - 4.Q0*mpi**2/par )

      HH_p = m**2 * breite * par / pi / (m**2-4.Q0*mpi**2)**(1.5Q0)
     1     * vv**3 * log( (1.Q0+vv)**2 / 4.Q0 / mpi**2 * par )

      return
      end

c **********************************************************************

      complex*32 function appr_sum(qq)
      implicit none
      include 'common.ekhara.inc.for'
      
      real*16 qq,appr_r,appr_i,q_yy
      real*16 aa,bb,cc,tt,dd,ee,ff,gg,hh,jj,xx,ll,mm,nn,oo,pp,rr,yy,uu
     
      CHARACTER msg*78         ! for logging

      q_yy = sqrt(qq)
c ---------------------------------------
c imaginary part
c (imaginary part - to be used for 2*mpi < sqrt(s) < 1.9 GeV
      if (q_yy.lt.1.9Q0) then             
       tt = q_yy-2.Q0*mpi
       aa = -0.000718099867Q0
       bb = 1.49883073Q0
       cc = -0.599502161Q0
       dd = 3.01291361Q0
       ee = 0.000112440192Q0
       ff = 16.8164504Q0
       gg = -4.81600874Q0
       hh = 5.54036557Q0
       jj = -3.2948141Q0
       xx = 0.916269688Q0
c
       appr_i = aa*tt**bb
     1 *(1.Q0+cc*tt+dd*tt**2+gg*tt**3+hh*tt**4
     1  +jj*tt**5 +xx*tt**6
     2 +ee*tt**ff)
      elseif (q_yy.lt.2.6Q0) then
c (imaginary part - to be used for 1.9 GeV < sqrt(s) < 2.6 GeV
c
       aa = -0.039071345Q0
       bb = 0.0129735898Q0
       cc = 2.30479409Q0
       dd = -0.513327099Q0
       ee = -1.45771258Q0
       ff = 25.2344274Q0
       gg = -0.55843411Q0
       hh = -43.3152678Q0
       jj = -0.328423868Q0
       xx = 113.986304Q0
       tt = q_yy-cc
      appr_i = (aa +tt*bb+tt**2*dd+tt**3*gg+tt**4*jj)
     1       /(1.Q0+tt*ee+tt**2*ff+tt**3*hh+tt**4*xx)
      elseif (q_yy.lt.11.Q0) then
c (imaginary part - to be used for 2.6 GeV < sqrt(s) < 10.6 GeV
c
        aa = -0.433813582Q0
        bb = -0.00309846176Q0
        cc = 2.42660454Q0
        dd = -189.541887Q0
        ee = 7.73108923Q0
        appr_i = aa*(1.Q0/q_yy-bb)**cc*(1.Q0 + dd/q_yy**ee)
       else
         msg = ' '
         write(msg,*) 'ERROR in <function appr_sum(qq)>',
     &           ' this function is to be used  below sqrt(s) = 11 GeV'
         call warnlog(msg)
         call errlog(msg)
         call prg_abort
       endif
c ---------------------------------------
c  real part
c ---------------------------------------
c below a fitted function 
c (real part - to be used for sqrt(s) < 2.35 GeV
c
      if (q_yy.lt.2.35Q0) then             
       aa = -0.0235588194Q0
       bb = 0.0509353703Q0
       cc = -2.53568629Q0
       dd =-0.0410886763Q0
       ee =2.37249385Q0
       ff =0.012325431Q0
       gg =-0.800187742Q0
       hh =-0.000404693395Q0
       jj =-0.05464375Q0
       xx =0.000491537627Q0
       ll =0.012346788Q0
       mm =0.000197618Q0
       nn =0.0171119177Q0
       oo =2.64910139Q-05
       pp =0.0270500914Q0
       rr = -0.000478819839Q0
c
      appr_r = (aa+ bb*(q_yy-1.2Q0)+ dd*(q_yy-1.2Q0)**2
     1 + ff*(q_yy-1.2Q0)**3+ hh*(q_yy-1.2Q0)**4
     1 + xx*(q_yy-1.2Q0)**5+ mm*(q_yy-1.2Q0)**6
     1 + oo*(q_yy-1.2Q0)**7+ rr*(q_yy-1.2Q0)**8 )
     1  /(1.Q0+ cc*(q_yy-1.2Q0)+ ee*(q_yy-1.2Q0)**2
     1 + gg*(q_yy-1.2Q0)**3+ jj*(q_yy-1.2Q0)**4
     1 + ll*(q_yy-1.2Q0)**5+ nn*(q_yy-1.2Q0)**6 
     1 + pp*(q_yy-1.2Q0)**7 )
      elseif (q_yy.lt.3.09Q0) then
c
      aa = 0.113336875Q0
      bb = -0.24255157Q0
      cc = -3.45189001Q0
      dd = -24.9323934Q0
      ee = -80.9049411Q0
      ff = 14.8839941Q0
      gg = 111.231181Q0
      hh = 402.100665Q0
      jj = -95.4007448Q0
      xx = 687.517122Q0
      ll = 66.258075Q0
      mm = 409.908489Q0
      nn = 234.397174Q0
      oo = -154.63515Q0
      pp = 130.735593Q0
      rr = -175.517676Q0
      yy = -47.3948306Q0
      tt = 87.7912501Q0
      uu = 0.999466217Q0
c
      if ((1.Q0/q_yy**2-aa).ge.0.Q0) then
      appr_r = (1.Q0/q_yy**2-aa)**uu
      else
      appr_r = -(-1.Q0/q_yy**2+aa)**uu
      endif
      appr_r = appr_r
     1 *(bb+cc*(q_yy-2.9Q0)+dd*(q_yy-2.9Q0)**2
     2 +ee*(q_yy-2.9Q0)**3+jj*(q_yy-2.9Q0)**4+ll*(q_yy-2.9Q0)**5
     2 +nn*(q_yy-2.9Q0)**6+pp*(q_yy-2.9Q0)**7+yy*(q_yy-2.9Q0)**8 )
     3 /(1.Q0+ff*(q_yy-2.9Q0)+gg*(q_yy-2.9Q0)**2
     4 +hh*(q_yy-2.9Q0)**3+xx*(q_yy-2.9Q0)**4+mm*(q_yy-2.9Q0)**5
     2 +oo*(q_yy-2.9Q0)**6+rr*(q_yy-2.9Q0)**7+tt*(q_yy-2.9Q0)**8 )
      elseif (q_yy.lt.11Q0) then
c
c below a fitted function 
c (real part - to be used for 3.09 GeV < sqrt(s) < 10.6 GeV
      aa = 0.315023275Q0
      bb = 49.4458006Q0
      cc = 219.202125Q0
      dd = 85.4051109Q0
      ee = 3254.82596Q0
      ff = -4817.48097Q0
      gg = -7126.62555Q0
c
      appr_r = 1.Q0/q_yy**2*
     1 (aa+bb/q_yy**2+dd/q_yy**4+ff/q_yy**6)
     2 /(1.Q0+cc/q_yy**2+ee/q_yy**4+gg/q_yy**6)
      else
       continue
      endif     
c ---------------------------------------
      appr_sum = qcmplx(appr_r,appr_i)
c
      return
      end

c **********************************************************************

      real*16 function PionFormFactor2(a)
      implicit none
      
      real*16 a
      complex*32 PionFormFactor
       
      PionFormFactor2= PionFormFactor(a)*conjg(PionFormFactor(a))
      return
      end

c**************************************************************

      real*16 function PionFormFactor_t(a)
      implicit none
c --- pion form factor  (Kuhn-Santamaria) for 2 gamma exchange diagrams-----
      include 'common.ekhara.inc.for'
            
      real*16 a
      real*16 BW_t
       
      PionFormFactor_t = (BW_t(mrho,a)
     &     *(1.Q0+al*BW_t(momega,a))/
     &     (1.Q0+al)+be*BW_t(mrhol,a))/(1.Q0+be)
      return
      end

c**************************************************************

      real*16 function PionFormFactor2_t(a)   
      implicit none
      
      real*16 a
      real*16 PionFormFactor_t
       
      PionFormFactor2_t= PionFormFactor_t(a)**2
      return
      end

c**************************************************************

      real*16 function BW_t(m,x)
      implicit none
      real*16 m,x

      BW_t=m*m/(m*m-x)

      return
      end
c**********************************************************************



c-----------------------------------------------------------------------
c     8>< ---      Matrix element calculation.              --- ><8
c-----------------------------------------------------------------------

c**************************************************************
      subroutine matrixelmt(Msq_c)
      implicit none
      include 'common.ekhara.inc.for'
      
      complex*32 I1g(2,2,2,2),I2g(2,2,2,2),I345g(2,2,2,2)
     &          ,amp910(2,2,2,2),amp1112(2,2,2,2),amp1314(2,2,2,2)
     &          ,amp7(2,2,2,2),amp8(2,2,2,2),amp6(2,2,2,2)
      real*16 is,Msq_c
      integer j1,j2,y1,y2
      
      CHARACTER msg*78         ! for logging

      call  amp1(I1g)  
      call  amp2(I2g)  
      call  amp345(I345g)  
      call  helamp4(amp910,amp1112)
      call  helamp1314(amp1314)
      call  helamp678(amp6,amp7,amp8)


      Msq_c = 0.Q0
      is = s

      do j1 = 1,2
      do j2 = 1,2
      do y1 = 1,2
      do y2 = 1,2
      if (sw_2pi.eq.1) then
      Msq_c = Msq_c + cqabs(
     & +I1g(j1,j2,y2,y1)+I2g(j1,j2,y2,y1)+I345g(j1,j2,y2,y1)
     &                   +amp1314(j1,j2,y2,y1)  
     &                  )**2 
      elseif (sw_2pi.eq.2) then
      Msq_c = Msq_c + cqabs(
     & +I1g(j1,j2,y2,y1)+I2g(j1,j2,y2,y1)+I345g(j1,j2,y2,y1)
     &                   - amp910(j1,j2,y1,y2) 
     &                   - amp1112(j1,j2,y1,y2)
     &                   +amp1314(j1,j2,y2,y1) 
     &                  )**2
      elseif (sw_2pi.eq.3) then
      Msq_c = Msq_c + cqabs(
     & +I1g(j1,j2,y2,y1)+I2g(j1,j2,y2,y1)+I345g(j1,j2,y2,y1)
     &                   - amp910(j1,j2,y1,y2) 
     &                   - amp1112(j1,j2,y1,y2)
     &                   +amp1314(j1,j2,y2,y1)
     &    -amp7(j1,j2,y1,y2)
     &    -amp8(j1,j2,y1,y2)
     &    -amp6(j1,j2,y1,y2)   
     &                  )**2
      else
         msg = ' '
         write(msg,*) 'ERROR in <subroutine matrixelmt(Msq)>',
     &           'Wrong "sw_2pi" switch !' 
         call warnlog(Msq_c)
         call errlog(Msq_c)
         call prg_abort
      endif

      enddo
      enddo
      enddo
      enddo

      return
      end

c===========================I1================================

      subroutine amp1(I1g)
      implicit none
      include 'common.ekhara.inc.for'

      real*16 two_p1(0:3),k1p1,dot,ccc1
      complex*32 gammapl(2,2),gammami(2,2),I1g(2,2,2,2),
     *I1(0:3,2,2),two_p1gammami(0:3,2,2),two_p1gammapl(0:3,2,2),
     *k1plgammami(2,2),k1migammapl(2,2),ila(0:3,2,2),ilb(0:3,2,2),
     *ma1(0:3,2,2),mb1(0:3,2,2),ma(0:3,2,2),mb(0:3,2,2),
     *k1pl(2,2),k1mi(2,2)
      integer ic1,ic2,rho,m1,n1,w1,z1,kl

      k1p1=dot(k1,p1)
      ccc1=-1.Q0/k1k1/ququ/(k1k1-2.Q0*k1p1)

      call plus(k1,k1pl)
      call minus(k1,k1mi)
      call plus1(gamma,gammapl)
      call minus1(gamma,gammami)

      call convec(2.Q0,p1,two_p1)

      call vecmat(two_p1,gammami,two_p1gammami)      
      call vecmat(two_p1,gammapl,two_p1gammapl)

      call matr(k1pl,gammami,k1plgammami)
      call matr(k1mi,gammapl,k1migammapl)
    
      call matr1(sigmi,k1plgammami,ila)     
      call matr1(sigpl,k1migammapl,ilb)

      call min4mat(ila,two_p1gammami,ma1)
      call min4mat(ilb,two_p1gammapl,mb1)

      call fac4mat(ccc1,ma1,ma)
      call fac4mat(ccc1,mb1,mb)

      call  Ipart(s,ma,mb,I1)
    
      do m1=1,2
      do n1=1,2
      do w1=1,2
      do z1=1,2
      call Ig(I1,gammu,m1,n1,w1,z1,I1g)
      enddo
      enddo
      enddo
      enddo
      
      end

c===================I2================================

      subroutine amp2(I2g)
      implicit none
      include 'common.ekhara.inc.for'

      real*16 two_p2(0:3),k1p2,dot,ccc2
      complex*32 gammapl(2,2),gammami(2,2),I2g(2,2,2,2),
     *I2(0:3,2,2),two_p2gammami(0:3,2,2),two_p2gammapl(0:3,2,2),
     *gammamik1pl(2,2),gammaplk1mi(2,2),ilc(0:3,2,2),ild(0:3,2,2),
     *mc1(0:3,2,2),md1(0:3,2,2),mc(0:3,2,2),md(0:3,2,2),
     *k1pl(2,2),k1mi(2,2)
      integer ic1,ic2,rho,m1,n1,w1,z1,kl
  
      k1p2=dot(k1,p2)

      ccc2=1.Q0/k1k1/ququ/(k1k1-2.Q0*k1p2)

      call plus(k1,k1pl)
      call minus(k1,k1mi)
      call plus1(gamma,gammapl)
      call minus1(gamma,gammami)
      
      call convec(2.Q0,p2,two_p2)

      call vecmat(two_p2,gammami,two_p2gammami)      
      call vecmat(two_p2,gammapl,two_p2gammapl)

      call matr(gammami,k1pl,gammamik1pl)
      call matr(gammapl,k1mi,gammaplk1mi)
      
      call matr2(gammamik1pl,sigmi,ilc)      
      call matr2(gammaplk1mi,sigpl,ild)

      call min4mat(ilc,two_p2gammami,mc1)
      call min4mat(ild,two_p2gammapl,md1)

      call fac4mat(ccc2,mc1,mc)
      call fac4mat(ccc2,md1,md)

      call  Ipart(s,mc,md,I2)
     
      do m1=1,2
      do n1=1,2
      do w1=1,2
      do z1=1,2
      call Ig(I2,gammu,m1,n1,w1,z1,I2g)
      enddo
      enddo
      enddo
      enddo
      
      end

c===================I345====================================

      subroutine amp345(I345g)
      implicit none
      include 'common.ekhara.inc.for'
      
      real*16 two_p2(0:3),
     *k1pi1,k1pi2,conpi1(0:3),conpi2(0:3),aaa1,aaa2,dot
      complex*32 gammapl(2,2),gammami(2,2),I345g(2,2,2,2),
     *I345(0:3,2,2),ccc345,
     *pi1g(2,2),pi2g(2,2),ddda(0:3,2,2),dddb(0:3,2,2),
     *ddd(0:3,2,2),ddd1(0:3,2,2),mg(0:3,2,2),mh(0:3,2,2)
      integer ic1,ic2,mu,m1,n1,w1,z1

      k1pi1=dot(k1,pi1)
      k1pi2=dot(k1,pi2)

      ccc345=qcmplx(0.Q0,2.Q0)*PFFS/k1k1/S 
          
      aaa1=-2.Q0/(k1k1+2.Q0*k1pi1)
      aaa2=-2.Q0/(k1k1+2.Q0*k1pi2)
 
      call convec(aaa1,pi2,conpi2)
      call convec(aaa2,pi1,conpi1)
   
      do w1=1,2
      do z1=1,2
      call Igg(pi1,gammu,w1,z1,pi1g)
      enddo
      enddo
     
      do w1=1,2
      do z1=1,2
      call Igg(pi2,gammu,w1,z1,pi2g)
      enddo
      enddo

      call vecmat(conpi2,pi1g,ddda)
      call vecmat(conpi1,pi2g,dddb)

      do mu=0,3
      do w1=1,2
      do z1=1,2
      ddd1(mu,z1,w1)=ddda(mu,z1,w1)+dddb(mu,z1,w1)-gammu(mu,z1,w1)
      mg(mu,z1,w1)=sigmi(mu,z1,w1)
      mh(mu,z1,w1)=sigpl(mu,z1,w1)
      enddo
      enddo
      enddo

      call fac4matc(ccc345,ddd1,ddd)

      call  Ipart(s,mg,mh,I345)

      do m1=1,2
      do n1=1,2
      do w1=1,2
      do z1=1,2
      call Ig(I345,ddd,m1,n1,w1,z1,I345g)
      enddo
      enddo
      enddo
      enddo

      end


c**************************************************************

      subroutine helamp4(amp910,amp1112)
      implicit none
      include 'common.ekhara.inc.for'
      
      integer j1,j2,y1,y2,nu
      complex*32 skalar9,skalar10,skalar11,skalar12
      complex*32 qupl(2,2),qumi(2,2),gammapl(2,2),gammami(2,2)
     &          ,quplgammami(2,2),qumigammapl(2,2)
     &          ,skalar9matr(2,2),skalar10matr(2,2)
     &          ,skalar11matr(2,2),skalar12matr(2,2)
     &          ,s9plmi(2,2),s9mipl(2,2),s10plmi(2,2),s10mipl(2,2)
     &          ,s11plmi(2,2),s11mipl(2,2),s12plmi(2,2),s12mipl(2,2)
     &          ,matrix9_1(2,2),matrix9_2(2,2)
     &          ,matrix10_1(2,2),matrix10_2(2,2)
     &          ,matrix11_1(2,2),matrix11_2(2,2)
     &          ,matrix12_1(2,2),matrix12_2(2,2)
      complex*32 I9(0:3,2,2),I10(0:3,2,2),II910(0:3,2,2)
     &          ,I11(0:3,2,2),I12(0:3,2,2),II1112(0:3,2,2)
     &          ,II910g5(0:3,2,2),II1112g5(0:3,2,2)
      complex*32 amp9(2,2,2,2),amp10(2,2,2,2)
     &          ,amp11(2,2,2,2),amp12(2,2,2,2)
     &          ,amp910(2,2,2,2),Mepsil910(2,2,2,2)
     &          ,amp1112(2,2,2,2),Mepsil1112(2,2,2,2)
     &          ,II910II1112(2,2,2,2),II910II1112QG(2,2,2,2)
      real*16 th1,th2,sphi1,cphi1,sphi2,cphi2,cth1d2,sth1d2,
     *       cth2d2,sth2d2,sq1,sq2,em1,em2,pm1,pm2,sth1,sth2,
     *       em,sq,pm
      real*16 czyn1_1,czyn1_2,czyn1_3,czyn1_4
      real*16 czyn2_1,czyn2_2,czyn2_3,czyn2_4
      complex*32 ex1,ex2

      CHARACTER msg*78         ! for logging

c ------------
      skalar9 = qcmplx(0.Q0,2.Q0) * PFF *
     & (2.Q0*p1pi1-p1(0)*qu(0)+p1(3)*qu(3))

      skalar10 = qcmplx(0.Q0,2.Q0) * PFF *
     & (2.Q0*quppi1-qupqup/2.Q0+(me-sququ)*(me+sququ)/2.Q0)

      skalar11 = qcmplx(0.Q0,2.Q0) * PFF *
     & (2.Q0*qupppi1-quppqupp/2.Q0+(me-sququ)*(me+sququ)/2.Q0)

      skalar12 = qcmplx(0.Q0,2.Q0) * PFF *
     & (2.Q0*p2pi1-p2(0)*qu(0)+p2(3)*qu(3))

c============================================================
        em = p1(0)
        pm = p1(3) 
        sq = sqrt(em+pm)
        em1 = q1(0)
        em2 = q2(0)
        pm1 = sqrt(q1(1)**2+q1(2)**2+q1(3)**2)
        pm2 = sqrt(q2(1)**2+q2(2)**2+q2(3)**2)
        sq1 = sqrt(em1+pm1)
        sq2 = sqrt(em2+pm2)

        if ( (q1(3).gt.0.Q0).and.((q1(3)/pm1).gt.1.Q0) ) 
     &  then
         msg = ' '
         write(msg,'(1x,3(1pd22.15))') q1(3),pm1,(q1(3)/pm1)
         call warnlog(msg)
         th1 = 0.Q0
        elseif ( (q1(3).lt.0.Q0).and.((q1(3)/pm1).lt.(-1.Q0)) ) then
         msg = ' '
         write(msg,'(1x,3(1pd22.15))') q1(3),pm1,(q1(3)/pm1)
         call warnlog(msg)
         th1 = pi
        else
         th1 = acos(q1(3)/pm1)
        endif
        cth1d2 = cos(th1/2.Q0)   
        if ( (q2(3).gt.0.Q0).and.((q2(3)/pm2).gt.1.Q0) ) then
         msg = ' '
         write(msg,'(1x,3(1pd22.15))') q2(3),pm2,(q2(3)/pm2)
         call warnlog(msg)
         th2 = 0.Q0
        elseif ( (q2(3).lt.0.Q0).and.((q2(3)/pm2).lt.(-1.Q0)) ) then
         msg = ' '
         write(msg,'(1x,3(1pd22.15))') q2(3),pm2,(q2(3)/pm2)
         call warnlog(msg)
         th2 = pi
        else
         th2 = acos(q2(3)/pm2)
        endif
        cth2d2 = cos(th2/2.Q0) 

         czyn1_1 = sq1/sq*(me2/(em1+pm1)+em+pm)
         czyn1_2 = sq1/sq*(me2/(em1+pm1)-em-pm)
         czyn1_3 = me*(em1+pm1+em+pm)/sq/sq1
         czyn1_4 = me*(em1+pm1-em-pm)/sq/sq1
         czyn2_1 = sq2/sq*(me2/(em2+pm2)+em+pm)
         czyn2_2 = sq2/sq*(me2/(em2+pm2)-em-pm)
         czyn2_3 = me*(em2+pm2+em+pm)/sq/sq2
         czyn2_4 = me*(em2+pm2-em-pm)/sq/sq2
c ---------------------------------------------
        if ((th1.eq.0.Q0).or.(th1.eq.pi)) then
      II1112(1,1,1)=  qcmplx(0.Q0,0.Q0)
      II1112(2,1,1)=  qcmplx(0.Q0,0.Q0)
      II1112(0,2,1)=  qcmplx(0.Q0,0.Q0)
      II1112(3,2,1)=  qcmplx(0.Q0,0.Q0) 
      II1112(0,1,2)=  qcmplx(0.Q0,0.Q0)
      II1112(3,1,2)=  qcmplx(0.Q0,0.Q0)
      II1112(1,2,2)=  qcmplx(0.Q0,0.Q0)
      II1112(2,2,2)=  qcmplx(0.Q0,0.Q0)
      II910g5(1,1,1)=  qcmplx(0.Q0,0.Q0)
      II910g5(2,1,1)=  qcmplx(0.Q0,0.Q0)
      II910g5(0,2,1)=  qcmplx(0.Q0,0.Q0)
      II910g5(3,2,1)=  qcmplx(0.Q0,0.Q0)
      II910g5(0,1,2)=  qcmplx(0.Q0,0.Q0)
      II910g5(3,1,2)=  qcmplx(0.Q0,0.Q0)
      II910g5(1,2,2)=  qcmplx(0.Q0,0.Q0)
      II910g5(2,2,2)=  qcmplx(0.Q0,0.Q0)
        else
         sth1  = sin(th1)
         sth1d2= sin(th1/2.Q0)
         cphi1 = q1(1)/pm1/sth1
         sphi1 = q1(2)/pm1/sth1
         ex1 = cphi1+qcmplx(0.Q0,1.Q0)*sphi1

      II1112(1,1,1) = -sth1d2/ex1 * czyn1_2 
      II1112(2,1,1) = qcmplx(0.Q0,-1.Q0)*sth1d2/ex1 * czyn1_2
      II1112(0,2,1) = sth1d2/ex1 * czyn1_3
      II1112(3,2,1) = -sth1d2/ex1 * czyn1_4
      II1112(0,1,2) = -sth1d2*ex1 * czyn1_3
      II1112(3,1,2) = sth1d2*ex1 * czyn1_4
      II1112(1,2,2) = -sth1d2*ex1 * czyn1_2
      II1112(2,2,2) = qcmplx(0.Q0,1.Q0)*sth1d2*ex1 * czyn1_2
      II910g5(1,1,1) = sth1d2/ex1 * czyn1_1  
      II910g5(2,1,1)= qcmplx(0.Q0,1.Q0)* sth1d2/ex1 * czyn1_1 
      II910g5(0,2,1) = sth1d2/ex1 * czyn1_4 
      II910g5(3,2,1) = -sth1d2/ex1 * czyn1_3
      II910g5(0,1,2) = sth1d2*ex1 * czyn1_4 
      II910g5(3,1,2) = -sth1d2*ex1 * czyn1_3
      II910g5(1,2,2) = -sth1d2*ex1 * czyn1_1
      II910g5(2,2,2) = qcmplx(0.Q0,1.Q0)*sth1d2*ex1 * czyn1_1
        endif

       if ((th2.eq.0.Q0).or.(th2.eq.pi)) then
      II910(0,1,1) =  qcmplx(0.Q0,0.Q0)
      II910(3,1,1) =  qcmplx(0.Q0,0.Q0) 
      II910(1,2,1) =  qcmplx(0.Q0,0.Q0)
      II910(2,2,1) =  qcmplx(0.Q0,0.Q0)
      II910(1,1,2) =  qcmplx(0.Q0,0.Q0)
      II910(2,1,2) =  qcmplx(0.Q0,0.Q0)
      II910(0,2,2) =  qcmplx(0.Q0,0.Q0)
      II910(3,2,2) =  qcmplx(0.Q0,0.Q0) 
      II1112g5(0,1,1) =  qcmplx(0.Q0,0.Q0)
      II1112g5(3,1,1) =  qcmplx(0.Q0,0.Q0)
      II1112g5(1,2,1) =  qcmplx(0.Q0,0.Q0)
      II1112g5(2,2,1) =  qcmplx(0.Q0,0.Q0)
      II1112g5(1,1,2) =  qcmplx(0.Q0,0.Q0)
      II1112g5(2,1,2) =  qcmplx(0.Q0,0.Q0)
      II1112g5(0,2,2) =  qcmplx(0.Q0,0.Q0)
      II1112g5(3,2,2) =  qcmplx(0.Q0,0.Q0)

        else
         sth2  = sin(th2)
         sth2d2= sin(th2/2.Q0)
         cphi2 = q2(1)/pm2/sth2
         sphi2 = q2(2)/pm2/sth2
         ex2 = cphi2+qcmplx(0.Q0,1.Q0)*sphi2

      II910(0,1,1) = -sth2d2/ex2 * czyn2_1
      II910(3,1,1) = -sth2d2/ex2 * czyn2_2
      II910(1,2,1) = sth2d2/ex2 * czyn2_4
      II910(2,2,1) = qcmplx(0.Q0,1.Q0)*sth2d2/ex2 * czyn2_4
      II910(1,1,2) = -sth2d2*ex2 * czyn2_4
      II910(2,1,2) = qcmplx(0.Q0,1.Q0)*sth2d2*ex2 * czyn2_4
      II910(0,2,2) = -sth2d2*ex2 * czyn2_1
      II910(3,2,2) = - sth2d2*ex2 * czyn2_2
      II1112g5(0,1,1) = -sth2d2/ex2 * czyn2_2
      II1112g5(3,1,1) = -sth2d2/ex2 * czyn2_1
      II1112g5(1,2,1) = -sth2d2/ex2 * czyn2_3
      II1112g5(2,2,1) = qcmplx(0.Q0,-1.Q0)*sth2d2/ex2 * czyn2_3
      II1112g5(1,1,2) = -sth2d2*ex2 * czyn2_3
      II1112g5(2,1,2) = qcmplx(0.Q0,1.Q0)*sth2d2*ex2 * czyn2_3
      II1112g5(0,2,2) = sth2d2*ex2 * czyn2_2
      II1112g5(3,2,2) = sth2d2*ex2 * czyn2_1
        endif
c ---
      II910(1,1,1) = cth2d2 * czyn2_2
      II910(2,1,1) = qcmplx(0.Q0,-1.Q0)*cth2d2 * czyn2_2
c ---
      II910(0,2,1) = cth2d2 * czyn2_3
      II910(3,2,1) = cth2d2 * czyn2_4
c ---
      II910(0,1,2) = -cth2d2 * czyn2_3
      II910(3,1,2) = - cth2d2 * czyn2_4
c ---
      II910(1,2,2) = cth2d2 * czyn2_2
      II910(2,2,2) = qcmplx(0.Q0,1.Q0)*cth2d2 * czyn2_2
c ---
      II1112(0,1,1) = cth1d2 * czyn1_1
      II1112(3,1,1) = -cth1d2 * czyn1_2
c ---
      II1112(1,2,1) = cth1d2 * czyn1_4
      II1112(2,2,1) = qcmplx(0.Q0,-1.Q0)*cth1d2 * czyn1_4
c ---
      II1112(1,1,2) = -cth1d2 * czyn1_4
      II1112(2,1,2) = qcmplx(0.Q0,-1.Q0)*cth1d2 * czyn1_4
c ---
      II1112(0,2,2) = cth1d2 * czyn1_1
      II1112(3,2,2) = -cth1d2 * czyn1_2
c ---
      II910g5(0,1,1) = - cth1d2 * czyn1_2
      II910g5(3,1,1) = cth1d2 * czyn1_1
c ---
      II910g5(1,2,1) = cth1d2 * czyn1_3  
      II910g5(2,2,1) = qcmplx(0.Q0,-1.Q0)*cth1d2 * czyn1_3
c ---
      II910g5(1,1,2) = cth1d2 * czyn1_3
      II910g5(2,1,2) = qcmplx(0.Q0,1.Q0)*cth1d2 * czyn1_3
c ---
      II910g5(0,2,2) = cth1d2 * czyn1_2
      II910g5(3,2,2) = -cth1d2 * czyn1_1
c --- 
      II1112g5(1,1,1) = cth2d2 * czyn2_1
      II1112g5(2,1,1) = qcmplx(0.Q0,-1.Q0)*cth2d2 * czyn2_1
c ---
      II1112g5(0,2,1) = -cth2d2 * czyn2_4
      II1112g5(3,2,1) = -cth2d2 * czyn2_3
c ---
      II1112g5(0,1,2) = -cth2d2 * czyn2_4
      II1112g5(3,1,2) = -cth2d2 * czyn2_3
c ---
      II1112g5(1,2,2) = -cth2d2 * czyn2_1
      II1112g5(2,2,2) = qcmplx(0.Q0,-1.Q0)*cth2d2 * czyn2_1

      do j1 = 1,2
      do j2 = 1,2
      do y1 = 1,2
      do y2 = 1,2

      Mepsil910(j1,j2,y1,y2) =
     &  - II910(0,j2,y2)*II910g5(1,j1,y1)*pi1(2)*qu(3) + II910(0,j2,y2)
     & *II910g5(1,j1,y1)*pi1(3)*qu(2) + II910(0,j2,y2)*II910g5(2,j1,y1)
     & *pi1(1)*qu(3) - II910(0,j2,y2)*II910g5(2,j1,y1)*pi1(3)*qu(1) -
     & II910(0,j2,y2)*II910g5(3,j1,y1)*pi1(1)*qu(2) + II910(0,j2,y2)*
     & II910g5(3,j1,y1)*pi1(2)*qu(1) + II910(1,j2,y2)*II910g5(0,j1,y1)*
     & pi1(2)*qu(3) - II910(1,j2,y2)*II910g5(0,j1,y1)*pi1(3)*qu(2) -
     & II910(1,j2,y2)*II910g5(2,j1,y1)*pi1(0)*qu(3) + II910(1,j2,y2)*
     & II910g5(2,j1,y1)*pi1(3)*qu(0) + II910(1,j2,y2)*II910g5(3,j1,y1)*
     & pi1(0)*qu(2) - II910(1,j2,y2)*II910g5(3,j1,y1)*pi1(2)*qu(0) -
     & II910(2,j2,y2)*II910g5(0,j1,y1)*pi1(1)*qu(3) + II910(2,j2,y2)*
     & II910g5(0,j1,y1)*pi1(3)*qu(1) + II910(2,j2,y2)*II910g5(1,j1,y1)*
     & pi1(0)*qu(3) - II910(2,j2,y2)*II910g5(1,j1,y1)*pi1(3)*qu(0) -
     & II910(2,j2,y2)*II910g5(3,j1,y1)*pi1(0)*qu(1) + II910(2,j2,y2)*
     & II910g5(3,j1,y1)*pi1(1)*qu(0) + II910(3,j2,y2)*II910g5(0,j1,y1)*
     & pi1(1)*qu(2)
      Mepsil910(j1,j2,y1,y2) = Mepsil910(j1,j2,y1,y2)
     & - II910(3,j2,y2)*II910g5(0,j1,y1)*pi1(2)*qu(1) -
     & II910(3,j2,y2)*II910g5(1,j1,y1)*pi1(0)*qu(2) + II910(3,j2,y2)*
     & II910g5(1,j1,y1)*pi1(2)*qu(0) + II910(3,j2,y2)*II910g5(2,j1,y1)*
     & pi1(0)*qu(1) - II910(3,j2,y2)*II910g5(2,j1,y1)*pi1(1)*qu(0)

      Mepsil1112(j1,j2,y1,y2) =
     &  - II1112(0,j1,y1)*II1112g5(1,j2,y2)*pi1(2)*qu(3) + II1112(0,j1,
     & y1)*II1112g5(1,j2,y2)*pi1(3)*qu(2) + II1112(0,j1,y1)*II1112g5(2,
     & j2,y2)*pi1(1)*qu(3) - II1112(0,j1,y1)*II1112g5(2,j2,y2)*pi1(3)*
     & qu(1) - II1112(0,j1,y1)*II1112g5(3,j2,y2)*pi1(1)*qu(2) + II1112(
     & 0,j1,y1)*II1112g5(3,j2,y2)*pi1(2)*qu(1) + II1112(1,j1,y1)*
     & II1112g5(0,j2,y2)*pi1(2)*qu(3) - II1112(1,j1,y1)*II1112g5(0,j2,
     & y2)*pi1(3)*qu(2) - II1112(1,j1,y1)*II1112g5(2,j2,y2)*pi1(0)*
     & qu(3) + II1112(1,j1,y1)*II1112g5(2,j2,y2)*pi1(3)*qu(0) + II1112(
     & 1,j1,y1)*II1112g5(3,j2,y2)*pi1(0)*qu(2) - II1112(1,j1,y1)*
     & II1112g5(3,j2,y2)*pi1(2)*qu(0) - II1112(2,j1,y1)*II1112g5(0,j2,
     & y2)*pi1(1)*qu(3) + II1112(2,j1,y1)*II1112g5(0,j2,y2)*pi1(3)*
     & qu(1) + II1112(2,j1,y1)*II1112g5(1,j2,y2)*pi1(0)*qu(3) - II1112(
     & 2,j1,y1)*II1112g5(1,j2,y2)*pi1(3)*qu(0) - II1112(2,j1,y1)*
     & II1112g5(3,j2,y2)*pi1(0)*qu(1) + II1112(2,j1,y1)*II1112g5(3,j2,
     & y2)*pi1(1)*qu(0)
      Mepsil1112(j1,j2,y1,y2) = Mepsil1112(j1,j2,y1,y2) 
     & + II1112(3,j1,y1)*II1112g5(0,j2,y2)*pi1(1)*qu(2)
     &  - II1112(3,j1,y1)*II1112g5(0,j2,y2)*pi1(2)*qu(1) - II1112(3,j1,
     & y1)*II1112g5(1,j2,y2)*pi1(0)*qu(2) + II1112(3,j1,y1)*II1112g5(1,
     & j2,y2)*pi1(2)*qu(0) + II1112(3,j1,y1)*II1112g5(2,j2,y2)*pi1(0)*
     & qu(1) - II1112(3,j1,y1)*II1112g5(2,j2,y2)*pi1(1)*qu(0)


      II910II1112(j1,j2,y1,y2) =
     & ( II910(0,j2,y2)*II1112(0,j1,y1)-II910(1,j2,y2)*II1112(1,j1,y1)
     &  -II910(2,j2,y2)*II1112(2,j1,y1)-II910(3,j2,y2)*II1112(3,j1,y1))

      II910II1112QG(j1,j2,y1,y2) =
     &   (  (qu(0)*II910(0,j2,y2)-qu(1)*II910(1,j2,y2)
     &      -qu(2)*II910(2,j2,y2)-qu(3)*II910(3,j2,y2))
     &    *(gamma(0)*II1112(0,j1,y1)-gamma(1)*II1112(1,j1,y1)
     &      -gamma(2)*II1112(2,j1,y1)-gamma(3)*II1112(3,j1,y1))
     &   - (gamma(0)*II910(0,j2,y2)-gamma(1)*II910(1,j2,y2)
     &      -gamma(2)*II910(2,j2,y2)-gamma(3)*II910(3,j2,y2))
     &    *(qu(0)*II1112(0,j1,y1)-qu(1)*II1112(1,j1,y1)
     &      -qu(2)*II1112(2,j1,y1)-qu(3)*II1112(3,j1,y1))  )

      amp910(j1,j2,y1,y2) = 1.Q0/t/ququ*(
     &  (1.Q0/mian9b-1.Q0/mian10b) * II910II1112QG(j1,j2,y1,y2)
     & - (skalar9/mian9b+skalar10/mian10b) * II910II1112(j1,j2,y1,y2) 
     & + 2.Q0*PFF*(1.Q0/mian9b+1.Q0/mian10b)
     &  * Mepsil910(j1,j2,y1,y2)
     & )


      amp1112(j1,j2,y1,y2) = -1.Q0/t1/ququ*(
     &  -(1.Q0/mian11b-1.Q0/mian12b) * II910II1112QG(j1,j2,y1,y2)
     & + (skalar11/mian11b+skalar12/mian12b) * II910II1112(j1,j2,y1,y2)
     & + 2.Q0*PFF*(1.Q0/mian11b+1.Q0/mian12b)
     &  * Mepsil1112(j1,j2,y1,y2)
     & )

      enddo
      enddo
      enddo
      enddo

      return
      end

c**************************************************************

      subroutine helamp1314(amp1314)
      implicit none
      include 'common.ekhara.inc.for'

      complex*32 mg(0:3,2,2),mh(0:3,2,2)
      complex*32 gammapl(2,2),gammami(2,2),qupl(2,2),qumi(2,2)
     &,gammaplqumi(2,2),gammamiqupl(2,2),mq1gamma(2,2),pq2gamma(2,2)
     &,gplqumimq1g(2,2),gmiquplpq2g(2,2),gmiquplmq1g(2,2)
     &,gplqumipq2g(2,2)
      complex*32 ma13aa(0:3,2,2),ma13bb(0:3,2,2),ma14aa(0:3,2,2)
     &,ma14bb(0:3,2,2),ma13a(0:3,2,2),ma13b(0:3,2,2),ma14a(0:3,2,2)
     &,ma14b(0:3,2,2),ma1314a(0:3,2,2),ma1314b(0:3,2,2)
       complex*32 I1314(0:3,2,2),II1314(0:3,2,2),amp1314(2,2,2,2)
      integer mu,m1,n1,w1,z1

      do mu=0,3
      do m1=1,2
      do n1=1,2
      mg(mu,m1,n1)=sigmi(mu,m1,n1)
      mh(mu,m1,n1)=sigpl(mu,m1,n1)
      enddo
      enddo
      enddo

      call  Ipart(s,mg,mh,I1314)
      
      call plus1(gamma,gammapl)
      call minus1(gamma,gammami)
      call plus(qu,qupl)
      call minus(qu,qumi)
      call matr(gammapl,qumi,gammaplqumi)
      call matr(gammami,qupl,gammamiqupl)

      mq1gamma(1,1)=-2.Q0*( q1(0)*gamma(0)-q1(1)*gamma(1)
     &                     -q1(2)*gamma(2)-q1(3)*gamma(3) )
      mq1gamma(2,2) = mq1gamma(1,1)
      mq1gamma(1,2) = 0.Q0
      mq1gamma(2,1) = 0.Q0 
      pq2gamma(1,1)= 2.Q0*( q2(0)*gamma(0)-q2(1)*gamma(1)
     &                     -q2(2)*gamma(2)-q2(3)*gamma(3) )
      pq2gamma(2,2) = pq2gamma(1,1)
      pq2gamma(1,2) = 0.Q0
      pq2gamma(2,1) = 0.Q0

      call dodmat(gammaplqumi,mq1gamma,gplqumimq1g)
      call dodmat(gammamiqupl,mq1gamma,gmiquplmq1g)
      call dodmat(gammamiqupl,pq2gamma,gmiquplpq2g)
      call dodmat(gammaplqumi,pq2gamma,gplqumipq2g)
      call matr1(mg,gplqumimq1g,ma13aa)
      call matr2(gmiquplpq2g,mg,ma14aa)
      call matr1(mh,gmiquplmq1g,ma13bb)
      call matr2(gplqumipq2g,mh,ma14bb)
      call fac4mat(1.Q0/s/ququ/mian10b,ma13aa,ma13a)
      call fac4mat(1.Q0/s/ququ/mian10b,ma13bb,ma13b)
      call fac4mat(1.Q0/s/ququ/mian11b,ma14aa,ma14a)
      call fac4mat(1.Q0/s/ququ/mian11b,ma14bb,ma14b)
      call dod4nmat(ma13a,ma14a,ma1314a)
      call dod4nmat(ma13b,ma14b,ma1314b)
      
      do mu=0,3      
      do w1=1,2
      do z1=1,2
      II1314(mu,w1,z1) =  ub1(1,w1)*ma1314a(mu,1,1)*v1(1,z1)
     &                  + ub1(1,w1)*ma1314a(mu,1,2)*v1(2,z1)
     &                  + ub1(2,w1)*ma1314a(mu,2,1)*v1(1,z1)
     &                  + ub1(2,w1)*ma1314a(mu,2,2)*v1(2,z1)
     &                  + ub2(1,w1)*ma1314b(mu,1,1)*v2(1,z1)
     &                  + ub2(1,w1)*ma1314b(mu,1,2)*v2(2,z1)
     &                  + ub2(2,w1)*ma1314b(mu,2,1)*v2(1,z1)
     &                  + ub2(2,w1)*ma1314b(mu,2,2)*v2(2,z1)
      enddo
      enddo
      enddo

      do w1=1,2
      do z1=1,2
      do m1=1,2
      do n1=1,2
      call Ig(I1314,II1314,m1,n1,w1,z1,amp1314)
      enddo
      enddo
      enddo
      enddo


      return
      end

c**************************************************************

      subroutine helamp678(amp6,amp7,amp8)
      implicit none
      include 'common.ekhara.inc.for'
      
      real*16 cpi1(0:3),mm2_pi1(0:3),pp2_pi2(0:3)
      integer mu,j1,j2,y1,y2,m1,n1
      complex*32  pi1pl(2,2),pi1mi(2,2),pi2pl(2,2),pi2mi(2,2)
     &           ,I7(2,2),II7(2,2),I8(2,2),II8(2,2)
     &           ,I6(0:3,2,2),II6(0:3,2,2)
     &           ,amp7(2,2,2,2),amp8(2,2,2,2),amp6(2,2,2,2)
      real*16 PionFormFactor_t,sPFF_t

      do mu=0,3
      cpi1(mu)=pi1(mu)
      enddo

      call   plus(cpi1,pi1pl)
      call  minus(cpi1,pi1mi)
      call   plus(pi2,pi2pl)
      call  minus(pi2,pi2mi)

      do mu=0,3
      do j1 = 1,2
      do y1 = 1,2
      I6(mu,j1,y1) = vp1(1,j1)*sigmi(mu,1,1)*v1(1,y1)
     &             + vp1(1,j1)*sigmi(mu,1,2)*v1(2,y1)
     &             + vp1(2,j1)*sigmi(mu,2,1)*v1(1,y1)
     &             + vp1(2,j1)*sigmi(mu,2,2)*v1(2,y1)
     &             + vp2(1,j1)*sigpl(mu,1,1)*v2(1,y1)
     &             + vp2(1,j1)*sigpl(mu,1,2)*v2(2,y1)
     &             + vp2(2,j1)*sigpl(mu,2,1)*v2(1,y1)
     &             + vp2(2,j1)*sigpl(mu,2,2)*v2(2,y1)
      enddo
      enddo
      enddo

      do mu=0,3
      do y2 = 1,2
      do j2 = 1,2
      II6(mu,j2,y2)= ub1(1,y2)*sigmi(mu,1,1)*up1(1,j2)
     &             + ub1(1,y2)*sigmi(mu,1,2)*up1(2,j2)
     &             + ub1(2,y2)*sigmi(mu,2,1)*up1(1,j2)
     &             + ub1(2,y2)*sigmi(mu,2,2)*up1(2,j2)
     &             + ub2(1,y2)*sigpl(mu,1,1)*up2(1,j2)
     &             + ub2(1,y2)*sigpl(mu,1,2)*up2(2,j2)
     &             + ub2(2,y2)*sigpl(mu,2,1)*up2(1,j2)
     &             + ub2(2,y2)*sigpl(mu,2,2)*up2(2,j2)
      enddo
      enddo
      enddo

      do j1 = 1,2
      do y1 = 1,2
      I7(j1,y1) = vp1(1,j1)*pi1mi(1,1)*v1(1,y1)
     &          + vp1(1,j1)*pi1mi(1,2)*v1(2,y1)
     &          + vp1(2,j1)*pi1mi(2,1)*v1(1,y1)
     &          + vp1(2,j1)*pi1mi(2,2)*v1(2,y1)
     &          + vp2(1,j1)*pi1pl(1,1)*v2(1,y1)
     &          + vp2(1,j1)*pi1pl(1,2)*v2(2,y1)
     &          + vp2(2,j1)*pi1pl(2,1)*v2(1,y1)
     &          + vp2(2,j1)*pi1pl(2,2)*v2(2,y1)
      enddo
      enddo

      do y2 = 1,2
      do j2 = 1,2
      II7(j2,y2)= ub1(1,y2)*pi2mi(1,1)*up1(1,j2)
     &          + ub1(1,y2)*pi2mi(1,2)*up1(2,j2)
     &          + ub1(2,y2)*pi2mi(2,1)*up1(1,j2)
     &          + ub1(2,y2)*pi2mi(2,2)*up1(2,j2)
     &          + ub2(1,y2)*pi2pl(1,1)*up2(1,j2)
     &          + ub2(1,y2)*pi2pl(1,2)*up2(2,j2)
     &          + ub2(2,y2)*pi2pl(2,1)*up2(1,j2)
     &          + ub2(2,y2)*pi2pl(2,2)*up2(2,j2)
      enddo
      enddo

      do j1 = 1,2
      do y1 = 1,2
      I8(j1,y1) = vp1(1,j1)*pi2mi(1,1)*v1(1,y1)
     &          + vp1(1,j1)*pi2mi(1,2)*v1(2,y1)
     &          + vp1(2,j1)*pi2mi(2,1)*v1(1,y1)
     &          + vp1(2,j1)*pi2mi(2,2)*v1(2,y1)
     &          + vp2(1,j1)*pi2pl(1,1)*v2(1,y1)
     &          + vp2(1,j1)*pi2pl(1,2)*v2(2,y1)
     &          + vp2(2,j1)*pi2pl(2,1)*v2(1,y1)
     &          + vp2(2,j1)*pi2pl(2,2)*v2(2,y1)
      enddo
      enddo

      do y2 = 1,2
      do j2 = 1,2
      II8(j2,y2)= ub1(1,y2)*pi1mi(1,1)*up1(1,j2)
     &          + ub1(1,y2)*pi1mi(1,2)*up1(2,j2)
     &          + ub1(2,y2)*pi1mi(2,1)*up1(1,j2)
     &          + ub1(2,y2)*pi1mi(2,2)*up1(2,j2)
     &          + ub2(1,y2)*pi1pl(1,1)*up2(1,j2)
     &          + ub2(1,y2)*pi1pl(1,2)*up2(2,j2)
     &          + ub2(2,y2)*pi1pl(2,1)*up2(1,j2)
     &          + ub2(2,y2)*pi1pl(2,2)*up2(2,j2)
      enddo
      enddo

      sPFF_t = PionFormFactor_t(t)*PionFormFactor_t(t1)

      do j1 = 1,2
      do y1 = 1,2
      do y2 = 1,2
      do j2 = 1,2
      amp6(j1,j2,y1,y2) = qcmplx(0.Q0,-2.Q0)/t/t1 * sPFF_t
     &                  *( I6(0,j1,y1)*II6(0,j2,y2)
     &                    -I6(1,j1,y1)*II6(1,j2,y2)
     &                    -I6(2,j1,y1)*II6(2,j2,y2)
     &                    -I6(3,j1,y1)*II6(3,j2,y2) )
      amp7(j1,j2,y1,y2) = qcmplx(0.Q0,-4.Q0)/t/t1/mian7b * sPFF_t
     &                  *I7(j1,y1)*II7(j2,y2)
      amp8(j1,j2,y1,y2) = qcmplx(0.Q0,-4.Q0)/t/t1/mian8b * sPFF_t
     &                  *I8(j1,y1)*II8(j2,y2)
      enddo
      enddo
      enddo
      enddo

      return
      end

c**************************************************************

      subroutine Ipart(is,ma,mb,I1)
      implicit none
      include 'common.ekhara.inc.for'
      
      complex*32 ma(0:3,2,2),mb(0:3,2,2),I1(0:3,2,2)
      real*16 is,E,p 
      integer nu

      E=sqrt(is)/2.Q0
      p=sqrt(is/4.Q0-me2) 
       
      do nu=0,3
      I1(nu,1,1) = me*(ma(nu,2,2)-mb(nu,2,2))
      I1(nu,2,2) = me*(ma(nu,1,1)-mb(nu,1,1))
      I1(nu,1,2) =-(E+p)*ma(nu,2,1)+(me**2/(E+p))*mb(nu,2,1)
      I1(nu,2,1) = -(me**2/(E+p))*ma(nu,1,2)+(E+p)*mb(nu,1,2)
      enddo

      end
c**********************************************************************




c-----------------------------------------------------------------------
c     8>< ---             Ph.Sp. generators               --- ><8
c-----------------------------------------------------------------------


c**************************************************************

      subroutine phsp1(fac,rr)
      implicit none
      include 'common.ekhara.inc.for'
      
      real*16 qupp(0:3),p1p(0:3)
      
      real*8 rr(11)
      real*16 fac,dely,betlam,yy,q0e1,dqp1,delx,xx,dq,q0,q0p
      real*16 gam,mbetgam,e1,dp1,mian9bt
      real*16 lambda,a,b,c,lambda1,a1,b1,d1
      real*16 vpi1,costh1,sinth1,cosphi1,sinphi1,phi1
      real*16 costh1q,sinth1q,cosphi1q,sinphi1q,phi1q
      real*16 qupmin,qupmax,delqup,cp1p,sp1p,q0pq,pom1,pom2
      real*16 quppi1t,q2t(0:3)
      real*16 fac1,fac2,fac3
      real*16 zz,zzmin,squpqup,ww
      integer i
      real*16 jack1k1ququ,jaccostheta,dk1,ak1,bk1,sum,aaa,bbb
     &        ,costheta,k1t(0:3),dlips2,dlips2pp,dlips2qu,dlips2k1

      CHARACTER msg*78         ! for logging
c ---------------
      lambda(a,b,c) = a**2+b**2+c**2-2.Q0*a*b -2.Q0*a*c-2.Q0*b*c
      lambda1(a1,b1,d1) = (a1+b1-d1)*(a1-b1+d1)*(a1+b1+d1)*(a1-b1-d1)
c ---------------
c sqrt(Q^2) generation
c ---------------
      ww = wwmin*(1.Q0-qext(rr(8)))
      sququ = ss - 2.Q0*me - sqrt(-2.Q0*ww)
      ququ = sququ * sququ
c ---------------
c sqrt(Q'^2) generation
c ---------------
      zzmin = -1.Q0/3.Q0/(ququ+2.Q0*sququ*me)**3
      delqup = zzmax-zzmin
      zz = zzmin+delqup*qext(rr(7))
      squpqup = sqrt(1.Q0/(-3.Q0*zz)**(1.Q0/3.Q0) + me2)
      qupqup = squpqup * squpqup 
      mian10b = (squpqup-me)*(squpqup+me)
c ---------------
c pi1 angles in Q rest frame            
c ---------------     
      phi1    = 2.Q0*pi*qext(rr(1))
      costh1  = -1.Q0 + 2.Q0*qext(rr(2))
      sinth1  = 2.Q0*sqrt(qext(rr(2))*(1.Q0-qext(rr(2))))
      cosphi1 = cos(phi1)
      sinphi1 = sin(phi1)
c
      pi1(0)  = sqrt(ququ)/2.Q0
      vpi1 = pi1(0)*sqrt((sququ-2.Q0*mpi)*(sququ+2.Q0*mpi))/sququ
      pi1(1) =  vpi1 * sinth1 * cosphi1      
      pi1(2) =  vpi1 * sinth1 * sinphi1      
      pi1(3) =  vpi1 * costh1
c ---------------     
c t 
c ---------------     
      betlam = sqrt((ss-2.Q0*me)*(ss+2.Q0*me))/ss
     &        *sqrt(lambda1(ss,squpqup,me))
      dely = s/me2/mian10b**2*betlam
      yy = 2.Q0/((ss-squpqup-me)*(ss-me+squpqup)
     &    +2.Q0*me*(ss-2.Q0*me)+betlam)+dely*qext(rr(3))
      t = -1.Q0/yy
c
c ---------------     
c Q angles in Q' rest frame z axis along p1
c ---------------   
      phi1q    = 2.Q0*pi*qext(rr(4))
      cosphi1q = cos(phi1q)
      sinphi1q = sin(phi1q)
c
      q0p  = sqrt(qupqup)
      q0   = (qupqup+ququ-me2)/2.Q0/q0p
      e1   = (qupqup-t+me2)/2.Q0/q0p
      q0e1 = q0*e1
      dq   = sqrt(lambda1(squpqup,sququ,me)/qupqup)/2.Q0
      dp1  = sqrt((t-(squpqup+me)**2)*(t-(squpqup-me)**2)/qupqup)/2.Q0
      dqp1 = dq*dp1
      delx = 2.Q0*qupqup**2/qupqup/(me2*(qupqup-me2)**2
     & +ququ*t*(-3.Q0*me2+ququ-qupqup)+ququ*t**2 )
      xx = 1.Q0/2.Q0/dqp1/(2.Q0*dqp1 + ((qupqup-me2)*(qupqup+me2-ququ)
     &   -t*(qupqup-me2+ququ))/2.Q0/qupqup ) + delx*qext(rr(5))
      mian9b = -1.Q0/xx/2.Q0/dqp1
      costh1q = (mian9b-ququ+2.Q0*q0e1)/2.Q0/dqp1
      if (costh1q.lt.-1.Q0) then
         msg = ' '
         write(msg,*)'I_costh1q = ',costh1q
         call warnlog(msg)
         costh1q = -1.Q0
      endif
      if (costh1q.gt.1.Q0) then
         msg = ' '
         write(msg,*)'I_costh1q = ',costh1q
         call warnlog(msg)
         costh1q = 1.Q0
      endif
      sinth1q = sqrt((1.Q0-costh1q)*(1.Q0+costh1q))
      qu(0) = sqrt(ququ+dq**2)
      qu(1) = dq*sinth1q*cosphi1q
      qu(2) = dq*sinth1q*sinphi1q
      qu(3) = dq*costh1q
c ---------------     
c boosting pi1 from Q rest frame into Q' rest frame
c ---------------   
c
      call boost1(q0p,sqrt(ququ),sqrt(me2+dq**2)
     1 ,dq,-costh1q,sinth1q,-cosphi1q,-sinphi1q,pi1)
      quppi1 = pi1(0)*q0p
c ---------------     
c q2 angles in p1+p2 rest frame
c ---------------   
c
      phi1    = 2.Q0*pi*qext(rr(6))
      cosphi1 = cos(phi1)
      sinphi1 = sin(phi1)
c
      q0p  = ss
      q0   = ((ss-squpqup)*(ss+squpqup)+me2)/2.Q0/q0p
      q0e1 = q0*q0p/2.Q0
      dq   = sqrt(lambda1(ss,squpqup,me)/s)/2.Q0
      dqp1 = dq*sqrt((ss/2.Q0-me)*(ss/2.Q0+me))
      costh1 = betlam*(2.Q0-s*(s-qupqup-3.Q0*me2)*qext(rr(3))
     &    *(s-qupqup-3.Q0*me2+betlam)/me2/mian10b**2)
     &   /(2.Q0+dely*qext(rr(3))*(s-qupqup-3.Q0*me2+betlam))
     &  /dq/4.Q0/p1(3) 
      if (costh1.lt.-1.Q0) then
         msg = ' '
         write(msg,*)'I_costh1qu2 = ',costh1
         call warnlog(msg)
         costh1 = -1.Q0
      endif
      if (costh1.gt.1.Q0) then
         msg = ' '
         write(msg,*)'I_costh1qu2 = ',costh1
         call warnlog(msg)
         costh1 = 1.Q0
      endif
      sinth1 = sqrt((1.Q0-costh1)*(1.Q0+costh1))
      qup(0) = sqrt(qupqup+dq**2)
      qup(1) = -dq*sinth1*cosphi1
      qup(2) = -dq*sinth1*sinphi1
      qup(3) = -dq*costh1
      q2t(0) = q0
      do i=1,3
       q2t(i) = -qup(i)
      enddo
c
c ---------------     
c boosting p1  from p1+p2 rest frame into Q' rest frame
c ---------------   
      do i=0,3
       p1p(i)=p1(i)
      enddo
      call boost2(q0p,sqrt(qupqup),sqrt(me2+dq**2)
     1 ,dq,costh1,sinth1,cosphi1,sinphi1,p1p)
c
c ---------------   
c calculating the rotation to have proper z-axis
c ---------------   
      cp1p =  p1p(3)/sqrt(p1p(1)**2+p1p(3)**2)
      sp1p = -p1p(1)/sqrt(p1p(1)**2+p1p(3)**2)
c
c ---------------     
c rotating pi1 and qu to have proper z-axis:see p. 17
c ---------------     
       pom1 = cp1p*pi1(1)-sp1p*pi1(3)
       pom2 = sp1p*pi1(1)+cp1p*pi1(3)
       pi1(1) = pom1
       pi1(3) = pom2
       pom1 = cp1p*qu(1)-sp1p*qu(3)
       pom2 = sp1p*qu(1)+cp1p*qu(3)
       qu(1) = pom1
       qu(3) = pom2
c ---------------     
c boosting pi1 and qu from Q' rest frame into p1+p2 rest frame
c ---------------   
c
      call boost1(q0p,sqrt(qupqup),sqrt(me2+dq**2)
     1 ,dq,costh1,sinth1,cosphi1,sinphi1,pi1)
      call boost1(q0p,sqrt(qupqup),sqrt(me2+dq**2)
     1 ,dq,costh1,sinth1,cosphi1,sinphi1,qu)
c
      quppi1t = qup(0)*pi1(0)-qup(1)*pi1(1)-qup(2)*pi1(2)-qup(3)*pi1(3)

      if (abs((quppi1-quppi1t)/quppi1).gt.1Q-12) then 
         msg = ' '
         write(msg,*)'quppi1 =',quppi1,'quppi1t = ',quppi1t
         call warnlog(msg)
      endif
c ---------------     
      p1pi1 = p1(0)*pi1(0)-p1(3)*pi1(3)
      qutau = qu(0)*tau(0)
      taupi1 = pi1(0)*tau(0)
c----------------
      mian9bt = ququ-2*(p1(0)*qu(0)-p1(3)*qu(3))
      if(abs((mian9b-mian9bt)/mian9b).gt.1Q-9) then
         msg = ' '
         write(msg,*)'mian9b =',mian9b,'mian9bt = ',mian9bt
         call warnlog(msg)
      endif

c ---------------     
c - other scalar products and invariants
c ---------------     
      p2pi1 = p2(0)*pi1(0)-p2(3)*pi1(3)
      mian12b  = ququ -2.Q0*(qu(0)*p2(0)-qu(3)*p2(3))
c 
      do i = 0,3
       q1(i)   =  qup(i) - qu(i)
       q2(i)   =  q2t(i)
       qupp(i) =  qu(i)  + q2(i)
       pi2(i)  =  qu(i) - pi1(i)
      enddo
      t1 = 2.Q0*me2-2.Q0*(me2**2+p1(3)**2*(q1(1)**2+q1(2)**2)
     &   +me2*(p1(3)**2+q1(1)**2+q1(2)**2+q1(3)**2))
     &   /(p1(0)*q1(0)+p1(3)*q1(3))

      mian11b  = qupp(0)**2 -me2
      qupppi1  = qupp(0)*pi1(0)
      do i = 1,3
       mian11b = mian11b - qupp(i)**2
       qupppi1 = qupppi1 - qupp(i)*pi1(i)
      enddo

      mian7b = t1-2.Q0*p1pi1+2.Q0*quppi1-ququ
      mian8b = t -2.Q0*p2pi1+2.Q0*qupppi1-ququ
c --------------- 
      fac1 = czyn_pi/qupqup
     1   *sqrt((sququ-2.Q0*mpi)*(sququ+2.Q0*mpi)/ququ)*delx/2.Q0 
     2   *sqrt(lambda1(ss,squpqup,me)*lambda1(squpqup,sququ,me))
     3   /(2.Q0*pi)**2 *delqup
     &   *t**2*mian10b**2*mian9b**2
     &   *2.Q0*sququ*(-wwmin)/(ss-2.Q0*me-sququ) 

      quppqupp =  qupp(0)**2-qupp(1)**2-qupp(2)**2-qupp(3)**2
      q0p  = sqrt(quppqupp)
      q0   = (quppqupp+ququ-me2)/2.Q0/q0p
      e1   = (quppqupp-t1+me2)/2.Q0/q0p
      q0e1 = q0*e1
      dq   = sqrt((quppqupp-(sququ-me)**2)*(quppqupp-(sququ+me)**2)
     &     /quppqupp)/2.Q0
      dp1  = sqrt(lambda(quppqupp,t1,me2)/quppqupp)/2.Q0
      dqp1 = dq*dp1
      delx = 2.Q0*quppqupp**2/quppqupp/(me2*(quppqupp-me2)**2
     & +ququ*t1*(-3.Q0*me2+ququ-quppqupp)+ququ*t1**2 )

      fac2 = czyn_pi/quppqupp
     1   *sqrt((sququ-2.Q0*mpi)*(sququ+2.Q0*mpi)/ququ)*delx/2.Q0
     2   *sqrt((quppqupp-(ss-me)**2)*(quppqupp-(ss+me)**2)
     &   *(quppqupp-(sququ-me)**2)*(quppqupp-(sququ+me)**2))
     3   /(2.Q0*pi)**2 *delqup 
     &   *t1**2*mian11b**2*mian12b**2
     &   *2.Q0*sququ*(-wwmin)/(ss-2.Q0*me-sququ) 


      do i=0,3
      k1(i) = q1(i)+q2(i)
      enddo
      k1k1 = k1(0)**2-k1(1)**2-k1(2)**2-k1(3)**2
      costheta = k1(3)/sqrt(k1(1)**2+k1(2)**2+k1(3)**2)
       aaa = log(((ss-sqrt(ququ))**2)/(4.Q0*me**2))
     * *((Log(((ss-sqrt(ququ))**2)/s))**2 
     * + Log(((ss-sqrt(ququ))**2)/s)*logg
     * + (logg)**2)
       bbb = -stala*log(sqrt(ququ)*(2.Q0*ss-sqrt(ququ))
     *     /(s-4.Q0*me**2))
       sum = aaa + bbb
      jack1k1ququ= dququdr2*sum/(3.Q0*(Log(k1k1/s))**2/k1k1 
     *            + stala/(s-k1k1) )  

      k1t(0)=(S + k1k1-ququ)/2/ss
      dk1=sqrt(k1t(0)**2 - k1k1)
  
      bk1=-2.Q0*dk1*abs(p1(3))
      ak1=(k1k1-2.Q0*k1t(0)*p1(0))/bk1

      jaccostheta=abs( log((( (-k1k1 + 2.Q0*k1t(0)*p1(0) 
     * + 2.Q0*dk1*abs(p1(3)))**2) /
     *   (k1k1**2 + 4.Q0*k1k1*( abs(p1(3))**2 - k1t(0)*p1(0) )
     *  +  4.Q0*me2*(k1t(0)**2)) )**2) *
     *((ak1+costheta)*(ak1-costheta)/2.Q0/ak1))

      dlips2pp = 2.Q0*pi*dlips2(s,ququ,k1k1)
      dlips2qu = 4.Q0*pi*dlips2(ququ,mpi2,mpi2)
      dlips2k1 = 4.Q0*pi*dlips2(k1k1,me2,me2)    
      fac3 = 1/(2.Q0*pi)**2*jack1k1ququ*jaccostheta*
     *dlips2pp*dlips2qu*dlips2k1


      fac = (2.Q0+constant)/(1.Q0/fac1+1.Q0/fac2+constant/fac3)
      fac = fac * czynnik

c ---------------     
      return
      end

c**************************************************************

      subroutine phsp2(fac,rr)     
      implicit none
      include 'common.ekhara.inc.for'
      
      real*16 qupp(0:3),p2p(0:3)
            
      real*8 rr(11)      
      real*16 fac,dely,betlam,yy,q0e1,dqp1,delx,xx,dq,q0,q0p
      real*16 gam,mbetgam,e1,dp1,mian12bt
      real*16 lambda,a,b,c,lambda1,a1,b1,d1
      real*16 vpi1,costh1,sinth1,cosphi1,sinphi1,phi1
      real*16 costh1q,sinth1q,cosphi1q,sinphi1q,phi1q
      real*16 qupmin,qupmax,delqup,cp1p,sp1p,q0pq,pom1,pom2
      real*16 qupppi1t,q1t(0:3)
      real*16 fac1,fac2,fac3
      real*16 zz,zzmin,squppqupp,ww
      integer i
      real*16 jack1k1ququ,jaccostheta,dk1,ak1,bk1,sum,aaa,bbb
     &        ,costheta,k1t(0:3),dlips2,dlips2pp,dlips2qu,dlips2k1

      CHARACTER msg*78         ! for logging
c ---------------
      lambda(a,b,c) = a**2+b**2+c**2-2.Q0*a*b -2.Q0*a*c-2.Q0*b*c
      lambda1(a1,b1,d1) = (a1+b1-d1)*(a1-b1+d1)*(a1+b1+d1)*(a1-b1-d1)
c ---------------
c ---------------
c sqrt(Q^2) generation
c ---------------
      ww = wwmin*(1.Q0-qext(rr(8)))
      sququ = ss - 2.Q0*me - sqrt(-2.Q0*ww)
      ququ = sququ * sququ
c ---------------
c sqrt(Q''^2) generation
c ---------------
      zzmin = -1.Q0/3.Q0/(ququ+2.Q0*sququ*me)**3
      delqup = zzmax-zzmin
      zz = zzmin+delqup*qext(rr(7))
      squppqupp = sqrt(1.Q0/(-3.Q0*zz)**(1.Q0/3.Q0) + me2)
      quppqupp = squppqupp * squppqupp 
      mian11b = (squppqupp-me)*(squppqupp+me)
c ---------------
c pi1 angles in Q rest frame            
c ---------------     
      phi1    = 2.Q0*pi*qext(rr(1))
      costh1  = -1.Q0 + 2.Q0*qext(rr(2))
      sinth1  = 2.Q0*sqrt(qext(rr(2))*(1.Q0-qext(rr(2))))
      cosphi1 = cos(phi1)
      sinphi1 = sin(phi1)
c
      pi1(0)  = sqrt(ququ)/2.Q0
      vpi1 = pi1(0)*sqrt((sququ-2.Q0*mpi)*(sququ+2.Q0*mpi))/sququ
      pi1(1) =  vpi1 * sinth1 * cosphi1      
      pi1(2) =  vpi1 * sinth1 * sinphi1      
      pi1(3) =  vpi1 * costh1
c ---------------     
c t1 
c ---------------     
      betlam = sqrt((ss-2.Q0*me)*(ss+2.Q0*me))/ss
     &        *sqrt(lambda1(ss,squppqupp,me))
      dely = s/me2/mian11b**2*betlam
      yy = 2.Q0/((ss-squppqupp-me)*(ss-me+squppqupp)
     &    +2.Q0*me*(ss-2.Q0*me)+betlam)+dely*qext(rr(3))
      t1 = -1.Q0/yy
c
c ---------------     
c Q angles in Q'' rest frame z axis along p2
c ---------------   
      phi1q    = 2.Q0*pi*qext(rr(4))
      cosphi1q = cos(phi1q)
      sinphi1q = sin(phi1q)
c
      q0p  = sqrt(quppqupp)
      q0   = (quppqupp+ququ-me2)/2.Q0/q0p
      e1   = (quppqupp-t1+me2)/2.Q0/q0p
      q0e1 = q0*e1
      dq   = sqrt(lambda1(squppqupp,sququ,me)/quppqupp)/2.Q0
      dp1  = sqrt((t1-(squppqupp+me)**2)*(t1-(squppqupp-me)**2)
     &      /quppqupp)/2.Q0
      dqp1 = dq*dp1
      delx = 2.Q0*quppqupp**2/quppqupp/(me2*(quppqupp-me2)**2
     & +ququ*t1*(-3.Q0*me2+ququ-quppqupp)+ququ*t1**2 )
      xx =1.Q0/2.Q0/dqp1/(2.Q0*dqp1+((quppqupp-me2)*(quppqupp+me2-ququ)
     &   -t1*(quppqupp-me2+ququ))/2.Q0/quppqupp ) + delx*qext(rr(5))
      mian12b = -1.Q0/xx/2.Q0/dqp1
      costh1q = (mian12b-ququ+2.Q0*q0e1)/2.Q0/dqp1
      if (costh1q.lt.-1.Q0) then
         msg = ' '
         write(msg,*)'II_costh1q = ',costh1q
         call warnlog(msg)
         costh1q = -1.Q0
      endif
      if (costh1q.gt.1.Q0) then
         msg = ' '
         write(msg,*)'II_costh1q = ',costh1q
         call warnlog(msg)
         costh1q = 1.Q0
      endif
      sinth1q = sqrt((1.Q0-costh1q)*(1.Q0+costh1q))
      qu(0) = sqrt(ququ+dq**2)
      qu(1) = dq*sinth1q*cosphi1q
      qu(2) = dq*sinth1q*sinphi1q
      qu(3) = dq*costh1q
c ---------------     
c boosting pi1 from Q rest frame into Q' rest frame
c ---------------   
c
      call boost1(q0p,sqrt(ququ),sqrt(me2+dq**2)
     1 ,dq,-costh1q,sinth1q,-cosphi1q,-sinphi1q,pi1)
      qupppi1 = pi1(0)*q0p
c ---------------     
c q1 angles in p1+p2 rest frame
c ---------------   
c
      phi1    = 2.Q0*pi*qext(rr(6))
      cosphi1 = cos(phi1)
      sinphi1 = sin(phi1)
c
      q0p  = ss
      q0   = ((ss-squppqupp)*(ss+squppqupp)+me2)/2.Q0/q0p
      q0e1 = q0*q0p/2.Q0
      dq   = sqrt(lambda1(ss,squppqupp,me)/s)/2.Q0
      dqp1 = dq*sqrt((ss/2.Q0-me)*(ss/2.Q0+me))
      costh1 = betlam*(2.Q0-s*(s-quppqupp-3.Q0*me2)*qext(rr(3))
     &    *(s-quppqupp-3.Q0*me2+betlam)/me2/mian11b**2)
     &   /(2.Q0+dely*qext(rr(3))*(s-quppqupp-3.Q0*me2+betlam))
     & /dq/4.Q0/p2(3) 
      if (costh1.lt.-1.Q0) then
         msg = ' '
         write(msg,*)'II_costh1qu2 = ',costh1
         call warnlog(msg)
         costh1 = -1.Q0
      endif
      if (costh1.gt.1.Q0) then
         msg = ' '
         write(msg,*)'II_costh1qu2 = ',costh1
         call warnlog(msg)
         costh1 = 1.Q0
      endif
      sinth1 = sqrt((1.Q0-costh1)*(1.Q0+costh1))
      qupp(0) = sqrt(quppqupp+dq**2)
      qupp(1) = -dq*sinth1*cosphi1
      qupp(2) = -dq*sinth1*sinphi1
      qupp(3) = -dq*costh1
      q1t(0) = q0
      do i=1,3
       q1t(i) = -qupp(i)
      enddo
c
c ---------------     
c boosting p1  from p1+p2 rest frame into Q' rest frame
c ---------------   
      do i=0,3
       p2p(i)=p2(i)
      enddo
      call boost2(q0p,sqrt(quppqupp),sqrt(me2+dq**2)
     1 ,dq,costh1,sinth1,cosphi1,sinphi1,p2p)
c
c ---------------   
c calculating the rotation to have proper z-axis
c ---------------   
      cp1p =  p2p(3)/sqrt(p2p(1)**2+p2p(3)**2)
      sp1p = -p2p(1)/sqrt(p2p(1)**2+p2p(3)**2)
c
c ---------------     
c rotating pi1 and qu to have proper z-axis:see p. 17
c ---------------     
       pom1 = cp1p*pi1(1)-sp1p*pi1(3)
       pom2 = sp1p*pi1(1)+cp1p*pi1(3)
       pi1(1) = pom1
       pi1(3) = pom2
       pom1 = cp1p*qu(1)-sp1p*qu(3)
       pom2 = sp1p*qu(1)+cp1p*qu(3)
       qu(1) = pom1
       qu(3) = pom2
c ---------------     
c boosting pi1 and qu from Q'' rest frame into p1+p2 rest frame
c ---------------   
c
      call boost1(q0p,sqrt(quppqupp),sqrt(me2+dq**2)
     1 ,dq,costh1,sinth1,cosphi1,sinphi1,pi1)
      call boost1(q0p,sqrt(quppqupp),sqrt(me2+dq**2)
     1 ,dq,costh1,sinth1,cosphi1,sinphi1,qu)
c
c--------------------------------------------------------------------
c--- transformation of vectors z axis to be along p1  -------------
      p1(3)=p2(3)
      p2(3)=-p2(3)      

      pom1=qupp(1)
      pom2=qupp(2)
      qupp(1)=pom2
      qupp(2)=pom1
      qupp(3)=-qupp(3)

      pom1=qu(1)
      pom2=qu(2)
      qu(1)=pom2
      qu(2)=pom1
      qu(3)=-qu(3)

      pom1=pi1(1)
      pom2=pi1(2)
      pi1(1)=pom2
      pi1(2)=pom1
      pi1(3)=-pi1(3)

      pom1=q1t(1)
      pom2=q1t(2)
      q1t(1)=pom2
      q1t(2)=pom1
      q1t(3)=-q1t(3)
c--------------------------------------------------------------------
c--------------------------------------------------------------------
      qupppi1t 
     &   = qupp(0)*pi1(0)-qupp(1)*pi1(1)-qupp(2)*pi1(2)-qupp(3)*pi1(3)
      if (abs((qupppi1-qupppi1t)/qupppi1).gt.1Q-12) then
         msg = ' '
         write(msg,*)'qupppi1 =',qupppi1,'qupppi1t = ',qupppi1t
         call warnlog(msg)
      endif
c ---------------     
      p2pi1 = p2(0)*pi1(0)-p2(3)*pi1(3)
      qutau = qu(0)*tau(0)
      taupi1 = pi1(0)*tau(0)
c----------------
      mian12bt = ququ-2*(p2(0)*qu(0)-p2(3)*qu(3))
      if (abs((mian12b-mian12bt)/mian12b).gt.1Q-9) then
         msg = ' '
         write(msg,*)'mian12b =',mian12b,'mian12bt = ',mian12bt
         call warnlog(msg)
      endif

c ---------------     
c - other scalar products and invariants
c ---------------     
      p1pi1 = p1(0)*pi1(0)-p1(3)*pi1(3)
      mian9b  = ququ -2.Q0*(qu(0)*p1(0)-qu(3)*p1(3))
c 
      do i = 0,3
       q2(i)   =  qupp(i) - qu(i)
       q1(i)   =  q1t(i)
       qup(i) =  qu(i)  + q1(i)
       pi2(i)  =  qu(i) - pi1(i) 
      enddo

      t = 2.Q0*me2-2.Q0*(me2**2+p1(3)**2*(q2(1)**2+q2(2)**2)
     &   +me2*(p1(3)**2+q2(1)**2+q2(2)**2+q2(3)**2))
     &   /(p2(0)*q2(0)-p1(3)*q2(3))

      mian10b  = qup(0)**2 -me2
      quppi1  = qup(0)*pi1(0)
      do i = 1,3
       mian10b = mian10b - qup(i)**2
       quppi1 = quppi1 - qup(i)*pi1(i)
      enddo

      mian7b = t1-2.Q0*p1pi1+2.Q0*quppi1-ququ
      mian8b = t -2.Q0*p2pi1+2.Q0*qupppi1-ququ
c --------------- 

      fac2 = czyn_pi/quppqupp
     1   *sqrt((sququ-2.Q0*mpi)*(sququ+2.Q0*mpi)/ququ)*delx/2.Q0     
     2   *sqrt(lambda1(ss,squppqupp,me)*lambda1(squppqupp,sququ,me))
     3   /(2.Q0*pi)**2 *delqup 
     &   *t1**2*mian11b**2*mian12b**2 
     &   *2.Q0*sququ*(-wwmin)/(ss-2.Q0*me-sququ) 

      qupqup =  qup(0)**2-qup(1)**2-qup(2)**2-qup(3)**2
      q0p  = sqrt(qupqup)
      q0   = (qupqup+ququ-me2)/2.Q0/q0p
      e1   = (qupqup-t+me2)/2.Q0/q0p
      q0e1 = q0*e1
      dq   = sqrt((qupqup-(sququ-me)**2)*(qupqup-(sququ+me)**2)
     &     /qupqup)/2.Q0
      dp1  = sqrt(lambda(qupqup,t,me2)/qupqup)/2.Q0
      dqp1 = dq*dp1
      delx = 2.Q0*qupqup**2/qupqup/(me2*(qupqup-me2)**2
     & +ququ*t*(-3.Q0*me2+ququ-qupqup)+ququ*t**2 )

      fac1 = czyn_pi/qupqup
     1   *sqrt((sququ-2.Q0*mpi)*(sququ+2.Q0*mpi)/ququ)*delx/2.Q0   
     2   *sqrt((qupqup-(ss-me)**2)*(qupqup-(ss+me)**2)
     &   *(qupqup-(sququ-me)**2)*(qupqup-(sququ+me)**2))
     3   /(2.Q0*pi)**2 *delqup 
     &   *t**2*mian10b**2*mian9b**2 
     &   *2.Q0*sququ*(-wwmin)/(ss-2.Q0*me-sququ)


      do i=0,3
      k1(i) = q1(i)+q2(i)
      enddo
      k1k1 = k1(0)**2-k1(1)**2-k1(2)**2-k1(3)**2
      costheta = k1(3)/sqrt(k1(1)**2+k1(2)**2+k1(3)**2)
       aaa = log(((ss-sqrt(ququ))**2)/(4.Q0*me**2))
     * *((Log(((ss-sqrt(ququ))**2)/s))**2 
     * + Log(((ss-sqrt(ququ))**2)/s)*logg
     * + (logg)**2)
       bbb = -stala*log(sqrt(ququ)*(2.Q0*ss-sqrt(ququ))
     *     /(s-4.Q0*me**2))
       sum = aaa + bbb
      jack1k1ququ= dququdr2*sum/(3.Q0*(Log(k1k1/s))**2/k1k1 
     *            + stala/(s-k1k1) )  

      k1t(0)=(S + k1k1-ququ)/2/ss
      dk1=sqrt(k1t(0)**2 - k1k1)
  
      bk1=-2.Q0*dk1*abs(p1(3))
      ak1=(k1k1-2.Q0*k1t(0)*p1(0))/bk1

      jaccostheta=abs( log((( (-k1k1 + 2.Q0*k1t(0)*p1(0) 
     * + 2.Q0*dk1*abs(p1(3)))**2) /
     *   (k1k1**2 + 4.Q0*k1k1*( abs(p1(3))**2 - k1t(0)*p1(0) )
     *  +  4.Q0*me2*(k1t(0)**2)) )**2) *
     *((ak1+costheta)*(ak1-costheta)/2.Q0/ak1))

      dlips2pp = 2.Q0*pi*dlips2(s,ququ,k1k1)
      dlips2qu = 4.Q0*pi*dlips2(ququ,mpi2,mpi2)
      dlips2k1 = 4.Q0*pi*dlips2(k1k1,me2,me2)    
      fac3 = 1/(2.Q0*pi)**2*jack1k1ququ*jaccostheta*
     *dlips2pp*dlips2qu*dlips2k1


      fac =(2.Q0+constant) /(1.Q0/fac1+1.Q0/fac2+constant/fac3)
      fac = fac *czynnik 

c ---------------     
      return
      end

c**************************************************************

      subroutine phsp3(fac,rr)
      implicit none
      include 'common.ekhara.inc.for'
      
      real*8 rr(11)
      real*16 dk1
      real*16 pi11(0:3),q11(0:3),q22(0:3)
      real*16 costheta,sintheta,phi,
     &costhetae,sinthetae,phie,costhetapi,sinthetapi,phipi,dpi11,dq11
      real*16 kk1(0:3),qqu(0:3),jack1k1ququ,xxx,dxdr1,vvv,ak1,bk1
     &       ,jaccostheta,aaamax,aaa,bbbmax,bbb,sum
      real*16 dlips2pp,dlips2qu,dlips2k1,dlips2   
      integer i11
      real*16 fac1,fac2,fac3,fac,squpqup,squppqupp
     &       ,zzmin,delqup,delx,lambda1,a1,b1,d1

      lambda1(a1,b1,d1) = (a1+b1-d1)*(a1-b1+d1)*(a1+b1+d1)*(a1-b1-d1)

       ququ = 4.Q0*mpi2 + dququdr2*qext(rr(2))
       sququ = sqrt(ququ)     

       aaamax = (Log(((ss-sququ)**2)/s))**3
       aaa = log(((ss-sququ)**2)/(4.Q0*me**2))
     * *((Log(((ss-sququ)**2)/s))**2 
     * + Log(((ss-sququ)**2)/s)*logg
     * + (logg)**2)

       bbbmax = -stala*Log((s-(ss-sququ)**2)/s)
       bbb = -stala*log(sququ*(2.Q0*ss-sququ)
     *     /(s-4.Q0*me**2))

       sum = aaa + bbb

       if (qext(rr(10)).lt.aaa/sum) then
          xxx = aaamin + aaa*qext(rr(1))
          k1k1 = s*exp(-(-xxx)**(1.Q0/3.Q0))
       else
          xxx  =  bbbmin + bbb*qext(rr(1))
          k1k1 = s - s*exp(-xxx/stala)
       endif

      jack1k1ququ= dququdr2*sum/(3.Q0*(Log(k1k1/s))**2/k1k1 
     *            + stala/(s-k1k1) )  
      
      phi=2.Q0*pi*qext(rr(4))
      costhetae=-1.Q0+2.Q0*qext(rr(5))
      phie=2.Q0*pi*qext(rr(6))
      costhetapi=-1.Q0+2.Q0*qext(rr(7))
      phipi=2.Q0*pi*qext(rr(8))      
           
      sinthetapi = Sqrt(1.Q0-costhetapi*costhetapi)
 
      pi11(0)=sququ/2
      dpi11=sqrt(pi11(0)**2-mpi2)
      pi11(1)=dpi11*sinthetapi*cos(phipi)
      pi11(2)=dpi11*sinthetapi*sin(phipi)
      pi11(3)=dpi11*costhetapi

      sinthetae = Sqrt(1.Q0-costhetae*costhetae)
      
      q11(0)=sqrt(k1k1)/2
      dq11=sqrt(q11(0)**2-me2)
      q11(1)=dq11*sinthetae*cos(phie)
      q11(2)=dq11*sinthetae*sin(phie)
      q11(3)=dq11*costhetae

      q22(0)=q11(0)
      q22(1)=-q11(1)
      q22(2)=-q11(2)
      q22(3)=-q11(3)

      k1(0)=(S + k1k1-ququ)/2/ss
      dk1=sqrt(k1(0)**2 - k1k1)
  
      bk1=-2.Q0*dk1*abs(p1(3))
      ak1=(k1k1-2.Q0*k1(0)*p1(0))/bk1

      vvv = log( (k1k1**2 + 4.Q0*k1k1*( abs(p1(3))**2 - k1(0)*p1(0) )
     *  +  4.Q0*me2*(k1(0)**2) )/
     *((-k1k1+2.Q0*k1(0)*p1(0)+2.Q0*dk1*abs(p1(3)))**2) )+qext(rr(3))*
     * log((( (-k1k1 + 2.Q0*k1(0)*p1(0) + 2.Q0*dk1*abs(p1(3)))**2) /
     *   (k1k1**2 + 4.Q0*k1k1*( abs(p1(3))**2 - k1(0)*p1(0) )
     *  +  4.Q0*me2*(k1(0)**2)) )**2)

      costheta = ak1*tanh(vvv/2.Q0)

      jaccostheta=abs( log((( (-k1k1 + 2.Q0*k1(0)*p1(0) 
     * + 2.Q0*dk1*abs(p1(3)))**2) /
     *   (k1k1**2 + 4.Q0*k1k1*( abs(p1(3))**2 - k1(0)*p1(0) )
     *  +  4.Q0*me2*(k1(0)**2)) )**2) *
     *((ak1+costheta)*(ak1-costheta)/2.Q0/ak1))
     
      sintheta = Sqrt(1.Q0-costheta*costheta)
   
      k1(1)=dk1*sintheta*cos(phi)
      k1(2)=dk1*sintheta*sin(phi)
      k1(3)=dk1*costheta

      qu(0)=ss - k1(0)
      qu(1)=-k1(1)
      qu(2)=-k1(2)
      qu(3)=-k1(3)

      do i11=0,3
      qqu(i11) = qu(i11)
      kk1(i11) = k1(i11)            
      enddo  
     
      call boostek(pi11,qqu)
      call boostek(q11,kk1)
      call boostek(q22,kk1)

      do i11=0,3
      pi1(i11) = pi11(i11)
      q1(i11) = q11(i11)
      q2(i11) = q22(i11)
      pi2(i11)  =  qu(i11) - pi1(i11)
      enddo
           
      dlips2pp = 2.Q0*pi*dlips2(s,ququ,k1k1)
      dlips2qu = 4.Q0*pi*dlips2(ququ,mpi2,mpi2)
      dlips2k1 = 4.Q0*pi*dlips2(k1k1,me2,me2)    
      fac3 = 1/(2.Q0*pi)**2*jack1k1ququ*jaccostheta*
     *dlips2pp*dlips2qu*dlips2k1

c --------------- 
      p1pi1 = p1(0)*pi1(0)-p1(3)*pi1(3)
      p2pi1 = p1(0)*pi1(0)+p1(3)*pi1(3)
      qupqup = (ss-q2(0))**2-q2(1)**2-q2(2)**2-q2(3)**2
      quppqupp = (ss-q1(0))**2-q1(1)**2-q1(2)**2-q1(3)**2
      quppi1 = (ss-q2(0))*pi1(0)+q2(1)*pi1(1)+q2(2)*pi1(2)
     &       +q2(3)*pi1(3)
      qupppi1 = (ss-q1(0))*pi1(0)+q1(1)*pi1(1)+q1(2)*pi1(2)
     &       +q1(3)*pi1(3)
      qutau = qu(0)*tau(0)
c --------------- 
      squpqup = sqrt(qupqup)
      squppqupp = sqrt(quppqupp)
      mian9b = ququ-2.Q0*(qu(0)*p1(0)-qu(3)*p1(3))
      mian12b = ququ-2.Q0*(qu(0)*p1(0)+qu(3)*p1(3))
      mian10b = qupqup-me2
      mian11b = quppqupp-me2
      t = (q2(0)-p2(0))**2-q2(1)**2-q2(2)**2-(q2(3)-p2(3))**2
      t1 =(q1(0)-p1(0))**2-q1(1)**2-q1(2)**2-(q1(3)-p1(3))**2
      mian7b = t1-2.Q0*p1pi1+2.Q0*quppi1-ququ
      mian8b = t -2.Q0*p2pi1+2.Q0*qupppi1-ququ
      zzmin = -1.Q0/3.Q0/(ququ+2.Q0*sququ*me)**3
      delqup = zzmax-zzmin

      delx = 2.Q0*qupqup**2/qupqup/(me2*(qupqup-me2)**2
     & +ququ*t*(-3.Q0*me2+ququ-qupqup)+ququ*t**2 )

      fac1 = czyn_pi/qupqup
     1   *sqrt((sququ-2.Q0*mpi)*(sququ+2.Q0*mpi)/ququ)*delx/2.Q0 
     2   *sqrt(lambda1(ss,squpqup,me)*lambda1(squpqup,sququ,me))
     3   /(2.Q0*pi)**2 *delqup
     &   *t**2*mian10b**2*mian9b**2
     &   *2.Q0*sququ*(-wwmin)/(ss-2.Q0*me-sququ)

      delx = 2.Q0*quppqupp**2/quppqupp/(me2*(quppqupp-me2)**2
     & +ququ*t1*(-3.Q0*me2+ququ-quppqupp)+ququ*t1**2 )

      fac2 = czyn_pi/quppqupp
     1   *sqrt((sququ-2.Q0*mpi)*(sququ+2.Q0*mpi)/ququ)*delx/2.Q0
     2   *sqrt((quppqupp-(ss-me)**2)*(quppqupp-(ss+me)**2)
     &   *(quppqupp-(sququ-me)**2)*(quppqupp-(sququ+me)**2))
     3   /(2.Q0*pi)**2 *delqup 
     &   *t1**2*mian11b**2*mian12b**2
     &   *2.Q0*sququ*(-wwmin)/(ss-2.Q0*me-sququ)
c --------------- 

      fac = (2.Q0+constant)/(1.Q0/fac1+1.Q0/fac2+constant/fac3)

      fac = fac * czynnik

      return
      end

c-------------------------------------------------------------
c  List of generated kinematic variables
c
c-------------------------------------------------------------

c-------------------------------------------------------------
c     8>< ---          End of file                     --- ><8
c-------------------------------------------------------------
