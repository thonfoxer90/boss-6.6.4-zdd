#-- start of make_header -----------------

#====================================
#  Library Ekhara
#
#   Generated Wed Feb  1 16:52:49 2017  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_Ekhara_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_Ekhara_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_Ekhara

Ekhara_tag = $(tag)

#cmt_local_tagfile_Ekhara = $(Ekhara_tag)_Ekhara.make
cmt_local_tagfile_Ekhara = $(bin)$(Ekhara_tag)_Ekhara.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Ekhara_tag = $(tag)

#cmt_local_tagfile_Ekhara = $(Ekhara_tag).make
cmt_local_tagfile_Ekhara = $(bin)$(Ekhara_tag).make

endif

include $(cmt_local_tagfile_Ekhara)
#-include $(cmt_local_tagfile_Ekhara)

ifdef cmt_Ekhara_has_target_tag

cmt_final_setup_Ekhara = $(bin)setup_Ekhara.make
#cmt_final_setup_Ekhara = $(bin)Ekhara_Ekharasetup.make
cmt_local_Ekhara_makefile = $(bin)Ekhara.make

else

cmt_final_setup_Ekhara = $(bin)setup.make
#cmt_final_setup_Ekhara = $(bin)Ekharasetup.make
cmt_local_Ekhara_makefile = $(bin)Ekhara.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Ekharasetup.make

#Ekhara :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'Ekhara'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = Ekhara/
#Ekhara::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

Ekharalibname   = $(bin)$(library_prefix)Ekhara$(library_suffix)
Ekharalib       = $(Ekharalibname).a
Ekharastamp     = $(bin)Ekhara.stamp
Ekharashstamp   = $(bin)Ekhara.shstamp

Ekhara :: dirs  EkharaLIB
	$(echo) "Ekhara ok"

#-- end of libary_header ----------------

EkharaLIB :: $(Ekharalib) $(Ekharashstamp)
	@/bin/echo "------> Ekhara : library ok"

$(Ekharalib) :: $(bin)Ekhara.o $(bin)Load.o $(bin)EkharaRandom.o $(bin)Entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(Ekharalib) $?
	$(lib_silent) $(ranlib) $(Ekharalib)
	$(lib_silent) cat /dev/null >$(Ekharastamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(Ekharalibname).$(shlibsuffix) :: $(Ekharalib) $(Ekharastamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" Ekhara $(Ekhara_shlibflags)

$(Ekharashstamp) :: $(Ekharalibname).$(shlibsuffix)
	@if test -f $(Ekharalibname).$(shlibsuffix) ; then cat /dev/null >$(Ekharashstamp) ; fi

Ekharaclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)Ekhara.o $(bin)Load.o $(bin)EkharaRandom.o $(bin)Entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
Ekharainstallname = $(library_prefix)Ekhara$(library_suffix).$(shlibsuffix)

Ekhara :: Ekharainstall

install :: Ekharainstall

Ekharainstall :: $(install_dir)/$(Ekharainstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(Ekharainstallname) :: $(bin)$(Ekharainstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(Ekharainstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(Ekharainstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(Ekharainstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(Ekharainstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(Ekharainstallname) $(install_dir)/$(Ekharainstallname); \
	      echo `pwd`/$(Ekharainstallname) >$(install_dir)/$(Ekharainstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(Ekharainstallname), no installation directory specified"; \
	  fi; \
	fi

Ekharaclean :: Ekharauninstall

uninstall :: Ekharauninstall

Ekharauninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(Ekharainstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(Ekharainstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(Ekharainstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(Ekharainstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),Ekharaclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)Ekhara_dependencies.make :: dirs

ifndef QUICK
$(bin)Ekhara_dependencies.make : $(src)Ekhara.cxx $(src)Load.cxx $(src)EkharaRandom.cxx $(src)Entries.cxx $(use_requirements) $(cmt_final_setup_Ekhara)
	$(echo) "(Ekhara.make) Rebuilding $@"; \
	  $(build_dependencies) Ekhara -all_sources -out=$@ $(src)Ekhara.cxx $(src)Load.cxx $(src)EkharaRandom.cxx $(src)Entries.cxx
endif

#$(Ekhara_dependencies)

-include $(bin)Ekhara_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Ekharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Ekhara.d

$(bin)$(binobj)Ekhara.d : $(use_requirements) $(cmt_final_setup_Ekhara)

$(bin)$(binobj)Ekhara.d : $(src)Ekhara.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Ekhara.dep $(use_pp_cppflags) $(Ekhara_pp_cppflags) $(lib_Ekhara_pp_cppflags) $(Ekhara_pp_cppflags) $(use_cppflags) $(Ekhara_cppflags) $(lib_Ekhara_cppflags) $(Ekhara_cppflags) $(Ekhara_cxx_cppflags)  $(src)Ekhara.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Ekhara.o $(src)Ekhara.cxx $(@D)/Ekhara.dep
endif
endif

$(bin)$(binobj)Ekhara.o : $(src)Ekhara.cxx
else
$(bin)Ekhara_dependencies.make : $(Ekhara_cxx_dependencies)

$(bin)$(binobj)Ekhara.o : $(Ekhara_cxx_dependencies)
endif
	$(cpp_echo) $(src)Ekhara.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Ekhara_pp_cppflags) $(lib_Ekhara_pp_cppflags) $(Ekhara_pp_cppflags) $(use_cppflags) $(Ekhara_cppflags) $(lib_Ekhara_cppflags) $(Ekhara_cppflags) $(Ekhara_cxx_cppflags)  $(src)Ekhara.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Ekharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Load.d

$(bin)$(binobj)Load.d : $(use_requirements) $(cmt_final_setup_Ekhara)

$(bin)$(binobj)Load.d : $(src)Load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Load.dep $(use_pp_cppflags) $(Ekhara_pp_cppflags) $(lib_Ekhara_pp_cppflags) $(Load_pp_cppflags) $(use_cppflags) $(Ekhara_cppflags) $(lib_Ekhara_cppflags) $(Load_cppflags) $(Load_cxx_cppflags)  $(src)Load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Load.o $(src)Load.cxx $(@D)/Load.dep
endif
endif

$(bin)$(binobj)Load.o : $(src)Load.cxx
else
$(bin)Ekhara_dependencies.make : $(Load_cxx_dependencies)

$(bin)$(binobj)Load.o : $(Load_cxx_dependencies)
endif
	$(cpp_echo) $(src)Load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Ekhara_pp_cppflags) $(lib_Ekhara_pp_cppflags) $(Load_pp_cppflags) $(use_cppflags) $(Ekhara_cppflags) $(lib_Ekhara_cppflags) $(Load_cppflags) $(Load_cxx_cppflags)  $(src)Load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Ekharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EkharaRandom.d

$(bin)$(binobj)EkharaRandom.d : $(use_requirements) $(cmt_final_setup_Ekhara)

$(bin)$(binobj)EkharaRandom.d : $(src)EkharaRandom.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/EkharaRandom.dep $(use_pp_cppflags) $(Ekhara_pp_cppflags) $(lib_Ekhara_pp_cppflags) $(EkharaRandom_pp_cppflags) $(use_cppflags) $(Ekhara_cppflags) $(lib_Ekhara_cppflags) $(EkharaRandom_cppflags) $(EkharaRandom_cxx_cppflags)  $(src)EkharaRandom.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/EkharaRandom.o $(src)EkharaRandom.cxx $(@D)/EkharaRandom.dep
endif
endif

$(bin)$(binobj)EkharaRandom.o : $(src)EkharaRandom.cxx
else
$(bin)Ekhara_dependencies.make : $(EkharaRandom_cxx_dependencies)

$(bin)$(binobj)EkharaRandom.o : $(EkharaRandom_cxx_dependencies)
endif
	$(cpp_echo) $(src)EkharaRandom.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Ekhara_pp_cppflags) $(lib_Ekhara_pp_cppflags) $(EkharaRandom_pp_cppflags) $(use_cppflags) $(Ekhara_cppflags) $(lib_Ekhara_cppflags) $(EkharaRandom_cppflags) $(EkharaRandom_cxx_cppflags)  $(src)EkharaRandom.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Ekharaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Entries.d

$(bin)$(binobj)Entries.d : $(use_requirements) $(cmt_final_setup_Ekhara)

$(bin)$(binobj)Entries.d : $(src)Entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Entries.dep $(use_pp_cppflags) $(Ekhara_pp_cppflags) $(lib_Ekhara_pp_cppflags) $(Entries_pp_cppflags) $(use_cppflags) $(Ekhara_cppflags) $(lib_Ekhara_cppflags) $(Entries_cppflags) $(Entries_cxx_cppflags)  $(src)Entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Entries.o $(src)Entries.cxx $(@D)/Entries.dep
endif
endif

$(bin)$(binobj)Entries.o : $(src)Entries.cxx
else
$(bin)Ekhara_dependencies.make : $(Entries_cxx_dependencies)

$(bin)$(binobj)Entries.o : $(Entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)Entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Ekhara_pp_cppflags) $(lib_Ekhara_pp_cppflags) $(Entries_pp_cppflags) $(use_cppflags) $(Ekhara_cppflags) $(lib_Ekhara_cppflags) $(Entries_cppflags) $(Entries_cxx_cppflags)  $(src)Entries.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: Ekharaclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(Ekhara.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(Ekhara.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(Ekhara.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(Ekhara.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_Ekhara)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Ekhara.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Ekhara.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Ekhara.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

Ekharaclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library Ekhara
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)Ekhara$(library_suffix).a $(library_prefix)Ekhara$(library_suffix).s? Ekhara.stamp Ekhara.shstamp
#-- end of cleanup_library ---------------
