#-- start of make_header -----------------

#====================================
#  Library ekhara
#
#   Generated Wed Feb  1 16:52:45 2017  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ekhara_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ekhara_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ekhara

Ekhara_tag = $(tag)

#cmt_local_tagfile_ekhara = $(Ekhara_tag)_ekhara.make
cmt_local_tagfile_ekhara = $(bin)$(Ekhara_tag)_ekhara.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Ekhara_tag = $(tag)

#cmt_local_tagfile_ekhara = $(Ekhara_tag).make
cmt_local_tagfile_ekhara = $(bin)$(Ekhara_tag).make

endif

include $(cmt_local_tagfile_ekhara)
#-include $(cmt_local_tagfile_ekhara)

ifdef cmt_ekhara_has_target_tag

cmt_final_setup_ekhara = $(bin)setup_ekhara.make
#cmt_final_setup_ekhara = $(bin)Ekhara_ekharasetup.make
cmt_local_ekhara_makefile = $(bin)ekhara.make

else

cmt_final_setup_ekhara = $(bin)setup.make
#cmt_final_setup_ekhara = $(bin)Ekharasetup.make
cmt_local_ekhara_makefile = $(bin)ekhara.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Ekharasetup.make

#ekhara :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ekhara'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ekhara/
#ekhara::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

ekharalibname   = $(bin)$(library_prefix)ekhara$(library_suffix)
ekharalib       = $(ekharalibname).a
ekharastamp     = $(bin)ekhara.stamp
ekharashstamp   = $(bin)ekhara.shstamp

ekhara :: dirs  ekharaLIB
	$(echo) "ekhara ok"

#-- end of libary_header ----------------
#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),ekharaclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)ekhara_dependencies.make :: dirs

ifndef QUICK
$(bin)ekhara_dependencies.make :  $(use_requirements) $(cmt_final_setup_ekhara)
	$(echo) "(ekhara.make) Rebuilding $@"; \
	  $(build_dependencies) ekhara -all_sources -out=$@ 
endif

#$(ekhara_dependencies)

-include $(bin)ekhara_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cleanup_header --------------

clean :: ekharaclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ekhara.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ekhara.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(ekhara.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ekhara.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_ekhara)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ekhara.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ekhara.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ekhara.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

ekharaclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library ekhara
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)ekhara$(library_suffix).a $(library_prefix)ekhara$(library_suffix).s? ekhara.stamp ekhara.shstamp
#-- end of cleanup_library ---------------
