#-- start of make_header -----------------

#====================================
#  Library EkharaLib
#
#   Generated Wed Feb  1 16:52:46 2017  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_EkharaLib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_EkharaLib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_EkharaLib

Ekhara_tag = $(tag)

#cmt_local_tagfile_EkharaLib = $(Ekhara_tag)_EkharaLib.make
cmt_local_tagfile_EkharaLib = $(bin)$(Ekhara_tag)_EkharaLib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Ekhara_tag = $(tag)

#cmt_local_tagfile_EkharaLib = $(Ekhara_tag).make
cmt_local_tagfile_EkharaLib = $(bin)$(Ekhara_tag).make

endif

include $(cmt_local_tagfile_EkharaLib)
#-include $(cmt_local_tagfile_EkharaLib)

ifdef cmt_EkharaLib_has_target_tag

cmt_final_setup_EkharaLib = $(bin)setup_EkharaLib.make
#cmt_final_setup_EkharaLib = $(bin)Ekhara_EkharaLibsetup.make
cmt_local_EkharaLib_makefile = $(bin)EkharaLib.make

else

cmt_final_setup_EkharaLib = $(bin)setup.make
#cmt_final_setup_EkharaLib = $(bin)Ekharasetup.make
cmt_local_EkharaLib_makefile = $(bin)EkharaLib.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Ekharasetup.make

#EkharaLib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'EkharaLib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = EkharaLib/
#EkharaLib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

EkharaLiblibname   = $(bin)$(library_prefix)EkharaLib$(library_suffix)
EkharaLiblib       = $(EkharaLiblibname).a
EkharaLibstamp     = $(bin)EkharaLib.stamp
EkharaLibshstamp   = $(bin)EkharaLib.shstamp

EkharaLib :: dirs  EkharaLibLIB
	$(echo) "EkharaLib ok"

#-- end of libary_header ----------------

EkharaLibLIB :: $(EkharaLiblib) $(EkharaLibshstamp)
	@/bin/echo "------> EkharaLib : library ok"

$(EkharaLiblib) :: $(bin)Ekhara.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(EkharaLiblib) $?
	$(lib_silent) $(ranlib) $(EkharaLiblib)
	$(lib_silent) cat /dev/null >$(EkharaLibstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(EkharaLiblibname).$(shlibsuffix) :: $(EkharaLiblib) $(EkharaLibstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" EkharaLib $(EkharaLib_shlibflags)

$(EkharaLibshstamp) :: $(EkharaLiblibname).$(shlibsuffix)
	@if test -f $(EkharaLiblibname).$(shlibsuffix) ; then cat /dev/null >$(EkharaLibshstamp) ; fi

EkharaLibclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)Ekhara.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
EkharaLibinstallname = $(library_prefix)EkharaLib$(library_suffix).$(shlibsuffix)

EkharaLib :: EkharaLibinstall

install :: EkharaLibinstall

EkharaLibinstall :: $(install_dir)/$(EkharaLibinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(EkharaLibinstallname) :: $(bin)$(EkharaLibinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(EkharaLibinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(EkharaLibinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(EkharaLibinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(EkharaLibinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(EkharaLibinstallname) $(install_dir)/$(EkharaLibinstallname); \
	      echo `pwd`/$(EkharaLibinstallname) >$(install_dir)/$(EkharaLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(EkharaLibinstallname), no installation directory specified"; \
	  fi; \
	fi

EkharaLibclean :: EkharaLibuninstall

uninstall :: EkharaLibuninstall

EkharaLibuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(EkharaLibinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(EkharaLibinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(EkharaLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(EkharaLibinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),EkharaLibclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)EkharaLib_dependencies.make :: dirs

ifndef QUICK
$(bin)EkharaLib_dependencies.make : $(src)Ekhara.cxx $(use_requirements) $(cmt_final_setup_EkharaLib)
	$(echo) "(EkharaLib.make) Rebuilding $@"; \
	  $(build_dependencies) EkharaLib -all_sources -out=$@ $(src)Ekhara.cxx
endif

#$(EkharaLib_dependencies)

-include $(bin)EkharaLib_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),EkharaLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Ekhara.d

$(bin)$(binobj)Ekhara.d : $(use_requirements) $(cmt_final_setup_EkharaLib)

$(bin)$(binobj)Ekhara.d : $(src)Ekhara.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Ekhara.dep $(use_pp_cppflags) $(EkharaLib_pp_cppflags) $(lib_EkharaLib_pp_cppflags) $(Ekhara_pp_cppflags) $(use_cppflags) $(EkharaLib_cppflags) $(lib_EkharaLib_cppflags) $(Ekhara_cppflags) $(Ekhara_cxx_cppflags)  $(src)Ekhara.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Ekhara.o $(src)Ekhara.cxx $(@D)/Ekhara.dep
endif
endif

$(bin)$(binobj)Ekhara.o : $(src)Ekhara.cxx
else
$(bin)EkharaLib_dependencies.make : $(Ekhara_cxx_dependencies)

$(bin)$(binobj)Ekhara.o : $(Ekhara_cxx_dependencies)
endif
	$(cpp_echo) $(src)Ekhara.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(EkharaLib_pp_cppflags) $(lib_EkharaLib_pp_cppflags) $(Ekhara_pp_cppflags) $(use_cppflags) $(EkharaLib_cppflags) $(lib_EkharaLib_cppflags) $(Ekhara_cppflags) $(Ekhara_cxx_cppflags)  $(src)Ekhara.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: EkharaLibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(EkharaLib.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(EkharaLib.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(EkharaLib.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(EkharaLib.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_EkharaLib)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(EkharaLib.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(EkharaLib.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(EkharaLib.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

EkharaLibclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library EkharaLib
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)EkharaLib$(library_suffix).a $(library_prefix)EkharaLib$(library_suffix).s? EkharaLib.stamp EkharaLib.shstamp
#-- end of cleanup_library ---------------
