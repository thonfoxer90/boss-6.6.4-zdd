// *****************************************************************************
//
// ManualGenerator.h
//
//  A package to read four-vectors from an input file and send them through
//    the BES detector simulation. 
//
//  Notes:
//
//    *  To use, replace EvtGen in the job options file with:
//
//           #include "$MANUALGENERATORROOT/share/ManualGenerator.txt"
//           ManualGenerator.InputFile = "inputfile.txt";
//
//       where "inputfile.txt" is a text file containing four-vectors in
//       the following format for each event:
//
//           [number of final state particles in this event]
//           [PDG ID of particle 1]  [P_x] [P_y] [P_z] [E]
//           [PDG ID of particle 2]  [P_x] [P_y] [P_z] [E]
//           and so on.
//
//    *  This package deals only with final state particles.  The production
//       of the initial state particle (for example, the J/psi) must be handled
//       by a prior package (for example, KKMC).
//
//    *  Events should be generated with zero net 3-momentum.  Particles are
//       then internally boosted to account for the crossing angle, beam energy
//       spread, etc., as reflected by the four-momentum of the initial state
//       particle.
//
//    *  Events should be generated at the origin.  Particles are then offset
//       according to the production vertex of the initial state particle.
//
//    *  pi0 and Ks decays are handled by GEANT.  
//         (Note that B(pi0 --> gamma gamma) is not 100%.)
//
//
// September 2011 R. Mitchell
//
// *****************************************************************************


#ifndef MANUALGENERATOR_H
#define MANUALGENERATOR_H

#include "CLHEP/Vector/LorentzVector.h"

#include "HepMC/GenEvent.h"
#include "HepMC/GenVertex.h"
#include "HepMC/GenParticle.h"

#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/ISvcLocator.h"

#include <vector>
#include <fstream>

using namespace std;

class ManualGenerator:public Algorithm {

  public:

    ManualGenerator(const string& name, ISvcLocator* pSvcLocator);

    StatusCode initialize();
    StatusCode execute();
    StatusCode finalize();

private:

    int m_eventCounter;
    string m_inputFileName;
    ifstream m_inputFile;

};

#endif
