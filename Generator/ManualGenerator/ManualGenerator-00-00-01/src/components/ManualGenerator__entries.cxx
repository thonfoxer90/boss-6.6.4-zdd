
#include "ManualGenerator/ManualGenerator.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_ALGORITHM_FACTORY( ManualGenerator )

DECLARE_FACTORY_ENTRIES( ManualGenerator ) {
    DECLARE_ALGORITHM( ManualGenerator );
}
