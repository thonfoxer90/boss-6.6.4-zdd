#-- start of make_header -----------------

#====================================
#  Library ManualGenerator
#
#   Generated Thu May  4 17:30:07 2017  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ManualGenerator_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ManualGenerator_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ManualGenerator

ManualGenerator_tag = $(tag)

#cmt_local_tagfile_ManualGenerator = $(ManualGenerator_tag)_ManualGenerator.make
cmt_local_tagfile_ManualGenerator = $(bin)$(ManualGenerator_tag)_ManualGenerator.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ManualGenerator_tag = $(tag)

#cmt_local_tagfile_ManualGenerator = $(ManualGenerator_tag).make
cmt_local_tagfile_ManualGenerator = $(bin)$(ManualGenerator_tag).make

endif

include $(cmt_local_tagfile_ManualGenerator)
#-include $(cmt_local_tagfile_ManualGenerator)

ifdef cmt_ManualGenerator_has_target_tag

cmt_final_setup_ManualGenerator = $(bin)setup_ManualGenerator.make
#cmt_final_setup_ManualGenerator = $(bin)ManualGenerator_ManualGeneratorsetup.make
cmt_local_ManualGenerator_makefile = $(bin)ManualGenerator.make

else

cmt_final_setup_ManualGenerator = $(bin)setup.make
#cmt_final_setup_ManualGenerator = $(bin)ManualGeneratorsetup.make
cmt_local_ManualGenerator_makefile = $(bin)ManualGenerator.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ManualGeneratorsetup.make

#ManualGenerator :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ManualGenerator'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ManualGenerator/
#ManualGenerator::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

ManualGeneratorlibname   = $(bin)$(library_prefix)ManualGenerator$(library_suffix)
ManualGeneratorlib       = $(ManualGeneratorlibname).a
ManualGeneratorstamp     = $(bin)ManualGenerator.stamp
ManualGeneratorshstamp   = $(bin)ManualGenerator.shstamp

ManualGenerator :: dirs  ManualGeneratorLIB
	$(echo) "ManualGenerator ok"

#-- end of libary_header ----------------

ManualGeneratorLIB :: $(ManualGeneratorlib) $(ManualGeneratorshstamp)
	@/bin/echo "------> ManualGenerator : library ok"

$(ManualGeneratorlib) :: $(bin)ManualGenerator.o $(bin)ManualGenerator__entries.o $(bin)ManualGenerator__load.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(ManualGeneratorlib) $?
	$(lib_silent) $(ranlib) $(ManualGeneratorlib)
	$(lib_silent) cat /dev/null >$(ManualGeneratorstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(ManualGeneratorlibname).$(shlibsuffix) :: $(ManualGeneratorlib) $(ManualGeneratorstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" ManualGenerator $(ManualGenerator_shlibflags)

$(ManualGeneratorshstamp) :: $(ManualGeneratorlibname).$(shlibsuffix)
	@if test -f $(ManualGeneratorlibname).$(shlibsuffix) ; then cat /dev/null >$(ManualGeneratorshstamp) ; fi

ManualGeneratorclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)ManualGenerator.o $(bin)ManualGenerator__entries.o $(bin)ManualGenerator__load.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
ManualGeneratorinstallname = $(library_prefix)ManualGenerator$(library_suffix).$(shlibsuffix)

ManualGenerator :: ManualGeneratorinstall

install :: ManualGeneratorinstall

ManualGeneratorinstall :: $(install_dir)/$(ManualGeneratorinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(ManualGeneratorinstallname) :: $(bin)$(ManualGeneratorinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(ManualGeneratorinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(ManualGeneratorinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ManualGeneratorinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ManualGeneratorinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(ManualGeneratorinstallname) $(install_dir)/$(ManualGeneratorinstallname); \
	      echo `pwd`/$(ManualGeneratorinstallname) >$(install_dir)/$(ManualGeneratorinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(ManualGeneratorinstallname), no installation directory specified"; \
	  fi; \
	fi

ManualGeneratorclean :: ManualGeneratoruninstall

uninstall :: ManualGeneratoruninstall

ManualGeneratoruninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(ManualGeneratorinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ManualGeneratorinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ManualGeneratorinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(ManualGeneratorinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),ManualGeneratorclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)ManualGenerator_dependencies.make :: dirs

ifndef QUICK
$(bin)ManualGenerator_dependencies.make : $(src)ManualGenerator.cxx $(src)components/ManualGenerator__entries.cxx $(src)components/ManualGenerator__load.cxx $(use_requirements) $(cmt_final_setup_ManualGenerator)
	$(echo) "(ManualGenerator.make) Rebuilding $@"; \
	  $(build_dependencies) ManualGenerator -all_sources -out=$@ $(src)ManualGenerator.cxx $(src)components/ManualGenerator__entries.cxx $(src)components/ManualGenerator__load.cxx
endif

#$(ManualGenerator_dependencies)

-include $(bin)ManualGenerator_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),ManualGeneratorclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ManualGenerator.d

$(bin)$(binobj)ManualGenerator.d : $(use_requirements) $(cmt_final_setup_ManualGenerator)

$(bin)$(binobj)ManualGenerator.d : $(src)ManualGenerator.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ManualGenerator.dep $(use_pp_cppflags) $(ManualGenerator_pp_cppflags) $(lib_ManualGenerator_pp_cppflags) $(ManualGenerator_pp_cppflags) $(use_cppflags) $(ManualGenerator_cppflags) $(lib_ManualGenerator_cppflags) $(ManualGenerator_cppflags) $(ManualGenerator_cxx_cppflags)  $(src)ManualGenerator.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ManualGenerator.o $(src)ManualGenerator.cxx $(@D)/ManualGenerator.dep
endif
endif

$(bin)$(binobj)ManualGenerator.o : $(src)ManualGenerator.cxx
else
$(bin)ManualGenerator_dependencies.make : $(ManualGenerator_cxx_dependencies)

$(bin)$(binobj)ManualGenerator.o : $(ManualGenerator_cxx_dependencies)
endif
	$(cpp_echo) $(src)ManualGenerator.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ManualGenerator_pp_cppflags) $(lib_ManualGenerator_pp_cppflags) $(ManualGenerator_pp_cppflags) $(use_cppflags) $(ManualGenerator_cppflags) $(lib_ManualGenerator_cppflags) $(ManualGenerator_cppflags) $(ManualGenerator_cxx_cppflags)  $(src)ManualGenerator.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),ManualGeneratorclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ManualGenerator__entries.d

$(bin)$(binobj)ManualGenerator__entries.d : $(use_requirements) $(cmt_final_setup_ManualGenerator)

$(bin)$(binobj)ManualGenerator__entries.d : $(src)components/ManualGenerator__entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ManualGenerator__entries.dep $(use_pp_cppflags) $(ManualGenerator_pp_cppflags) $(lib_ManualGenerator_pp_cppflags) $(ManualGenerator__entries_pp_cppflags) $(use_cppflags) $(ManualGenerator_cppflags) $(lib_ManualGenerator_cppflags) $(ManualGenerator__entries_cppflags) $(ManualGenerator__entries_cxx_cppflags) -I../src/components $(src)components/ManualGenerator__entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ManualGenerator__entries.o $(src)components/ManualGenerator__entries.cxx $(@D)/ManualGenerator__entries.dep
endif
endif

$(bin)$(binobj)ManualGenerator__entries.o : $(src)components/ManualGenerator__entries.cxx
else
$(bin)ManualGenerator_dependencies.make : $(ManualGenerator__entries_cxx_dependencies)

$(bin)$(binobj)ManualGenerator__entries.o : $(ManualGenerator__entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/ManualGenerator__entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ManualGenerator_pp_cppflags) $(lib_ManualGenerator_pp_cppflags) $(ManualGenerator__entries_pp_cppflags) $(use_cppflags) $(ManualGenerator_cppflags) $(lib_ManualGenerator_cppflags) $(ManualGenerator__entries_cppflags) $(ManualGenerator__entries_cxx_cppflags) -I../src/components $(src)components/ManualGenerator__entries.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),ManualGeneratorclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ManualGenerator__load.d

$(bin)$(binobj)ManualGenerator__load.d : $(use_requirements) $(cmt_final_setup_ManualGenerator)

$(bin)$(binobj)ManualGenerator__load.d : $(src)components/ManualGenerator__load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ManualGenerator__load.dep $(use_pp_cppflags) $(ManualGenerator_pp_cppflags) $(lib_ManualGenerator_pp_cppflags) $(ManualGenerator__load_pp_cppflags) $(use_cppflags) $(ManualGenerator_cppflags) $(lib_ManualGenerator_cppflags) $(ManualGenerator__load_cppflags) $(ManualGenerator__load_cxx_cppflags) -I../src/components $(src)components/ManualGenerator__load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ManualGenerator__load.o $(src)components/ManualGenerator__load.cxx $(@D)/ManualGenerator__load.dep
endif
endif

$(bin)$(binobj)ManualGenerator__load.o : $(src)components/ManualGenerator__load.cxx
else
$(bin)ManualGenerator_dependencies.make : $(ManualGenerator__load_cxx_dependencies)

$(bin)$(binobj)ManualGenerator__load.o : $(ManualGenerator__load_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/ManualGenerator__load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ManualGenerator_pp_cppflags) $(lib_ManualGenerator_pp_cppflags) $(ManualGenerator__load_pp_cppflags) $(use_cppflags) $(ManualGenerator_cppflags) $(lib_ManualGenerator_cppflags) $(ManualGenerator__load_cppflags) $(ManualGenerator__load_cxx_cppflags) -I../src/components $(src)components/ManualGenerator__load.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ManualGeneratorclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ManualGenerator.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ManualGenerator.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(ManualGenerator.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ManualGenerator.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_ManualGenerator)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ManualGenerator.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ManualGenerator.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ManualGenerator.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

ManualGeneratorclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library ManualGenerator
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)ManualGenerator$(library_suffix).a $(library_prefix)ManualGenerator$(library_suffix).s? ManualGenerator.stamp ManualGenerator.shstamp
#-- end of cleanup_library ---------------
