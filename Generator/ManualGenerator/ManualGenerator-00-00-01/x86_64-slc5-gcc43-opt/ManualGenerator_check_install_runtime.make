#-- start of make_header -----------------

#====================================
#  Document ManualGenerator_check_install_runtime
#
#   Generated Thu May  4 17:30:13 2017  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ManualGenerator_check_install_runtime_has_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ManualGenerator_check_install_runtime_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ManualGenerator_check_install_runtime

ManualGenerator_tag = $(tag)

#cmt_local_tagfile_ManualGenerator_check_install_runtime = $(ManualGenerator_tag)_ManualGenerator_check_install_runtime.make
cmt_local_tagfile_ManualGenerator_check_install_runtime = $(bin)$(ManualGenerator_tag)_ManualGenerator_check_install_runtime.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ManualGenerator_tag = $(tag)

#cmt_local_tagfile_ManualGenerator_check_install_runtime = $(ManualGenerator_tag).make
cmt_local_tagfile_ManualGenerator_check_install_runtime = $(bin)$(ManualGenerator_tag).make

endif

include $(cmt_local_tagfile_ManualGenerator_check_install_runtime)
#-include $(cmt_local_tagfile_ManualGenerator_check_install_runtime)

ifdef cmt_ManualGenerator_check_install_runtime_has_target_tag

cmt_final_setup_ManualGenerator_check_install_runtime = $(bin)setup_ManualGenerator_check_install_runtime.make
#cmt_final_setup_ManualGenerator_check_install_runtime = $(bin)ManualGenerator_ManualGenerator_check_install_runtimesetup.make
cmt_local_ManualGenerator_check_install_runtime_makefile = $(bin)ManualGenerator_check_install_runtime.make

else

cmt_final_setup_ManualGenerator_check_install_runtime = $(bin)setup.make
#cmt_final_setup_ManualGenerator_check_install_runtime = $(bin)ManualGeneratorsetup.make
cmt_local_ManualGenerator_check_install_runtime_makefile = $(bin)ManualGenerator_check_install_runtime.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ManualGeneratorsetup.make

#ManualGenerator_check_install_runtime :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ManualGenerator_check_install_runtime'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ManualGenerator_check_install_runtime/
#ManualGenerator_check_install_runtime::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of cmt_action_runner_header ---------------

ifdef ONCE
ManualGenerator_check_install_runtime_once = 1
endif

ifdef ManualGenerator_check_install_runtime_once

ManualGenerator_check_install_runtimeactionstamp = $(bin)ManualGenerator_check_install_runtime.actionstamp
#ManualGenerator_check_install_runtimeactionstamp = ManualGenerator_check_install_runtime.actionstamp

ManualGenerator_check_install_runtime :: $(ManualGenerator_check_install_runtimeactionstamp)
	$(echo) "ManualGenerator_check_install_runtime ok"
#	@echo ManualGenerator_check_install_runtime ok

$(ManualGenerator_check_install_runtimeactionstamp) :: $(ManualGenerator_check_install_runtime_dependencies)
	$(silent) /cluster/bes3/dist/6.6.4.p01/BesPolicy/BesPolicy-01-05-03/cmt/bes_check_installations.sh -files= -s=../share *.txt   -installdir=/home/bgarillo/boss-6.6.4-ZDD/InstallArea/share
	$(silent) cat /dev/null > $(ManualGenerator_check_install_runtimeactionstamp)
#	@echo ok > $(ManualGenerator_check_install_runtimeactionstamp)

ManualGenerator_check_install_runtimeclean ::
	$(cleanup_silent) /bin/rm -f $(ManualGenerator_check_install_runtimeactionstamp)

else

ManualGenerator_check_install_runtime :: $(ManualGenerator_check_install_runtime_dependencies)
	$(silent) /cluster/bes3/dist/6.6.4.p01/BesPolicy/BesPolicy-01-05-03/cmt/bes_check_installations.sh -files= -s=../share *.txt   -installdir=/home/bgarillo/boss-6.6.4-ZDD/InstallArea/share

endif

install ::
uninstall ::

#-- end of cmt_action_runner_header -----------------
#-- start of cleanup_header --------------

clean :: ManualGenerator_check_install_runtimeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ManualGenerator_check_install_runtime.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ManualGenerator_check_install_runtime.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(ManualGenerator_check_install_runtime.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ManualGenerator_check_install_runtime.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_ManualGenerator_check_install_runtime)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ManualGenerator_check_install_runtime.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ManualGenerator_check_install_runtime.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ManualGenerator_check_install_runtime.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

ManualGenerator_check_install_runtimeclean ::
#-- end of cleanup_header ---------------
