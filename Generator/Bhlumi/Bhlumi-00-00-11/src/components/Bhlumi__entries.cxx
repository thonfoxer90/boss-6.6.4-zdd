//====================================================================
//  BHLUMI_entries.cxx
//--------------------------------------------------------------------
//
//  Package    : Generator/Bhlumi
//
//  Description: Declaration of the components (factory entries)
//               specified by this component library.
//
//====================================================================

#include "Bhlumi/Bhlumi.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_ALGORITHM_FACTORY( Bhlumi )

DECLARE_FACTORY_ENTRIES( Bhlumi ) {
    DECLARE_ALGORITHM( Bhlumi );
}
