#-- start of make_header -----------------

#====================================
#  Library BhlumiLib
#
#   Generated Thu Sep 15 15:57:17 2016  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_BhlumiLib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_BhlumiLib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_BhlumiLib

Bhlumi_tag = $(tag)

#cmt_local_tagfile_BhlumiLib = $(Bhlumi_tag)_BhlumiLib.make
cmt_local_tagfile_BhlumiLib = $(bin)$(Bhlumi_tag)_BhlumiLib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Bhlumi_tag = $(tag)

#cmt_local_tagfile_BhlumiLib = $(Bhlumi_tag).make
cmt_local_tagfile_BhlumiLib = $(bin)$(Bhlumi_tag).make

endif

include $(cmt_local_tagfile_BhlumiLib)
#-include $(cmt_local_tagfile_BhlumiLib)

ifdef cmt_BhlumiLib_has_target_tag

cmt_final_setup_BhlumiLib = $(bin)setup_BhlumiLib.make
#cmt_final_setup_BhlumiLib = $(bin)Bhlumi_BhlumiLibsetup.make
cmt_local_BhlumiLib_makefile = $(bin)BhlumiLib.make

else

cmt_final_setup_BhlumiLib = $(bin)setup.make
#cmt_final_setup_BhlumiLib = $(bin)Bhlumisetup.make
cmt_local_BhlumiLib_makefile = $(bin)BhlumiLib.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Bhlumisetup.make

#BhlumiLib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'BhlumiLib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = BhlumiLib/
#BhlumiLib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

BhlumiLiblibname   = $(bin)$(library_prefix)BhlumiLib$(library_suffix)
BhlumiLiblib       = $(BhlumiLiblibname).a
BhlumiLibstamp     = $(bin)BhlumiLib.stamp
BhlumiLibshstamp   = $(bin)BhlumiLib.shstamp

BhlumiLib :: dirs  BhlumiLibLIB
	$(echo) "BhlumiLib ok"

#-- end of libary_header ----------------

BhlumiLibLIB :: $(BhlumiLiblib) $(BhlumiLibshstamp)
	@/bin/echo "------> BhlumiLib : library ok"

$(BhlumiLiblib) :: $(bin)Bhlumi.o $(bin)BhlumiRandom.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(BhlumiLiblib) $?
	$(lib_silent) $(ranlib) $(BhlumiLiblib)
	$(lib_silent) cat /dev/null >$(BhlumiLibstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(BhlumiLiblibname).$(shlibsuffix) :: $(BhlumiLiblib) $(BhlumiLibstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" BhlumiLib $(BhlumiLib_shlibflags)

$(BhlumiLibshstamp) :: $(BhlumiLiblibname).$(shlibsuffix)
	@if test -f $(BhlumiLiblibname).$(shlibsuffix) ; then cat /dev/null >$(BhlumiLibshstamp) ; fi

BhlumiLibclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)Bhlumi.o $(bin)BhlumiRandom.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
BhlumiLibinstallname = $(library_prefix)BhlumiLib$(library_suffix).$(shlibsuffix)

BhlumiLib :: BhlumiLibinstall

install :: BhlumiLibinstall

BhlumiLibinstall :: $(install_dir)/$(BhlumiLibinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(BhlumiLibinstallname) :: $(bin)$(BhlumiLibinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(BhlumiLibinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(BhlumiLibinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BhlumiLibinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BhlumiLibinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(BhlumiLibinstallname) $(install_dir)/$(BhlumiLibinstallname); \
	      echo `pwd`/$(BhlumiLibinstallname) >$(install_dir)/$(BhlumiLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(BhlumiLibinstallname), no installation directory specified"; \
	  fi; \
	fi

BhlumiLibclean :: BhlumiLibuninstall

uninstall :: BhlumiLibuninstall

BhlumiLibuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(BhlumiLibinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BhlumiLibinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BhlumiLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(BhlumiLibinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),BhlumiLibclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)BhlumiLib_dependencies.make :: dirs

ifndef QUICK
$(bin)BhlumiLib_dependencies.make : $(src)Bhlumi.cxx $(src)BhlumiRandom.cxx $(use_requirements) $(cmt_final_setup_BhlumiLib)
	$(echo) "(BhlumiLib.make) Rebuilding $@"; \
	  $(build_dependencies) BhlumiLib -all_sources -out=$@ $(src)Bhlumi.cxx $(src)BhlumiRandom.cxx
endif

#$(BhlumiLib_dependencies)

-include $(bin)BhlumiLib_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BhlumiLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Bhlumi.d

$(bin)$(binobj)Bhlumi.d : $(use_requirements) $(cmt_final_setup_BhlumiLib)

$(bin)$(binobj)Bhlumi.d : $(src)Bhlumi.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Bhlumi.dep $(use_pp_cppflags) $(BhlumiLib_pp_cppflags) $(lib_BhlumiLib_pp_cppflags) $(Bhlumi_pp_cppflags) $(use_cppflags) $(BhlumiLib_cppflags) $(lib_BhlumiLib_cppflags) $(Bhlumi_cppflags) $(Bhlumi_cxx_cppflags)  $(src)Bhlumi.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Bhlumi.o $(src)Bhlumi.cxx $(@D)/Bhlumi.dep
endif
endif

$(bin)$(binobj)Bhlumi.o : $(src)Bhlumi.cxx
else
$(bin)BhlumiLib_dependencies.make : $(Bhlumi_cxx_dependencies)

$(bin)$(binobj)Bhlumi.o : $(Bhlumi_cxx_dependencies)
endif
	$(cpp_echo) $(src)Bhlumi.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BhlumiLib_pp_cppflags) $(lib_BhlumiLib_pp_cppflags) $(Bhlumi_pp_cppflags) $(use_cppflags) $(BhlumiLib_cppflags) $(lib_BhlumiLib_cppflags) $(Bhlumi_cppflags) $(Bhlumi_cxx_cppflags)  $(src)Bhlumi.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BhlumiLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BhlumiRandom.d

$(bin)$(binobj)BhlumiRandom.d : $(use_requirements) $(cmt_final_setup_BhlumiLib)

$(bin)$(binobj)BhlumiRandom.d : $(src)BhlumiRandom.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BhlumiRandom.dep $(use_pp_cppflags) $(BhlumiLib_pp_cppflags) $(lib_BhlumiLib_pp_cppflags) $(BhlumiRandom_pp_cppflags) $(use_cppflags) $(BhlumiLib_cppflags) $(lib_BhlumiLib_cppflags) $(BhlumiRandom_cppflags) $(BhlumiRandom_cxx_cppflags)  $(src)BhlumiRandom.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BhlumiRandom.o $(src)BhlumiRandom.cxx $(@D)/BhlumiRandom.dep
endif
endif

$(bin)$(binobj)BhlumiRandom.o : $(src)BhlumiRandom.cxx
else
$(bin)BhlumiLib_dependencies.make : $(BhlumiRandom_cxx_dependencies)

$(bin)$(binobj)BhlumiRandom.o : $(BhlumiRandom_cxx_dependencies)
endif
	$(cpp_echo) $(src)BhlumiRandom.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BhlumiLib_pp_cppflags) $(lib_BhlumiLib_pp_cppflags) $(BhlumiRandom_pp_cppflags) $(use_cppflags) $(BhlumiLib_cppflags) $(lib_BhlumiLib_cppflags) $(BhlumiRandom_cppflags) $(BhlumiRandom_cxx_cppflags)  $(src)BhlumiRandom.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: BhlumiLibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(BhlumiLib.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(BhlumiLib.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(BhlumiLib.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(BhlumiLib.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_BhlumiLib)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BhlumiLib.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BhlumiLib.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BhlumiLib.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

BhlumiLibclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library BhlumiLib
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)BhlumiLib$(library_suffix).a $(library_prefix)BhlumiLib$(library_suffix).s? BhlumiLib.stamp BhlumiLib.shstamp
#-- end of cleanup_library ---------------
