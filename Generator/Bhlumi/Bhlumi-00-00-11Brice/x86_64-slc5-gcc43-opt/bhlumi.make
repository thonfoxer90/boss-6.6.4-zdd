#-- start of make_header -----------------

#====================================
#  Library bhlumi
#
#   Generated Thu Sep 15 15:57:17 2016  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_bhlumi_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_bhlumi_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_bhlumi

Bhlumi_tag = $(tag)

#cmt_local_tagfile_bhlumi = $(Bhlumi_tag)_bhlumi.make
cmt_local_tagfile_bhlumi = $(bin)$(Bhlumi_tag)_bhlumi.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Bhlumi_tag = $(tag)

#cmt_local_tagfile_bhlumi = $(Bhlumi_tag).make
cmt_local_tagfile_bhlumi = $(bin)$(Bhlumi_tag).make

endif

include $(cmt_local_tagfile_bhlumi)
#-include $(cmt_local_tagfile_bhlumi)

ifdef cmt_bhlumi_has_target_tag

cmt_final_setup_bhlumi = $(bin)setup_bhlumi.make
#cmt_final_setup_bhlumi = $(bin)Bhlumi_bhlumisetup.make
cmt_local_bhlumi_makefile = $(bin)bhlumi.make

else

cmt_final_setup_bhlumi = $(bin)setup.make
#cmt_final_setup_bhlumi = $(bin)Bhlumisetup.make
cmt_local_bhlumi_makefile = $(bin)bhlumi.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Bhlumisetup.make

#bhlumi :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'bhlumi'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = bhlumi/
#bhlumi::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

bhlumilibname   = $(bin)$(library_prefix)bhlumi$(library_suffix)
bhlumilib       = $(bhlumilibname).a
bhlumistamp     = $(bin)bhlumi.stamp
bhlumishstamp   = $(bin)bhlumi.shstamp

bhlumi :: dirs  bhlumiLIB
	$(echo) "bhlumi ok"

#-- end of libary_header ----------------

bhlumiLIB :: $(bhlumilib) $(bhlumishstamp)
	@/bin/echo "------> bhlumi : library ok"

$(bhlumilib) :: $(bin)bhllog.o $(bin)oldbis.o $(bin)bhlumi.o $(bin)m2agzi.o $(bin)repi.o $(bin)lumlog.o $(bin)model.o $(bin)modl2a.o $(bin)modl2b.o $(bin)bhlum4.o $(bin)glibk.o $(bin)combine.o $(bin)yfslib.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(bhlumilib) $?
	$(lib_silent) $(ranlib) $(bhlumilib)
	$(lib_silent) cat /dev/null >$(bhlumistamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(bhlumilibname).$(shlibsuffix) :: $(bhlumilib) $(bhlumistamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" bhlumi $(bhlumi_shlibflags)

$(bhlumishstamp) :: $(bhlumilibname).$(shlibsuffix)
	@if test -f $(bhlumilibname).$(shlibsuffix) ; then cat /dev/null >$(bhlumishstamp) ; fi

bhlumiclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)bhllog.o $(bin)oldbis.o $(bin)bhlumi.o $(bin)m2agzi.o $(bin)repi.o $(bin)lumlog.o $(bin)model.o $(bin)modl2a.o $(bin)modl2b.o $(bin)bhlum4.o $(bin)glibk.o $(bin)combine.o $(bin)yfslib.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
bhlumiinstallname = $(library_prefix)bhlumi$(library_suffix).$(shlibsuffix)

bhlumi :: bhlumiinstall

install :: bhlumiinstall

bhlumiinstall :: $(install_dir)/$(bhlumiinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(bhlumiinstallname) :: $(bin)$(bhlumiinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(bhlumiinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(bhlumiinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(bhlumiinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(bhlumiinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(bhlumiinstallname) $(install_dir)/$(bhlumiinstallname); \
	      echo `pwd`/$(bhlumiinstallname) >$(install_dir)/$(bhlumiinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(bhlumiinstallname), no installation directory specified"; \
	  fi; \
	fi

bhlumiclean :: bhlumiuninstall

uninstall :: bhlumiuninstall

bhlumiuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(bhlumiinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(bhlumiinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(bhlumiinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(bhlumiinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),bhlumiclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)bhlumi_dependencies.make :: dirs

ifndef QUICK
$(bin)bhlumi_dependencies.make : $(src)400/bhllog.f $(src)400/oldbis.f $(src)400/bhlumi.f $(src)400/m2agzi.f $(src)400/repi.f $(src)400/lumlog.f $(src)400/model.f $(src)400/modl2a.f $(src)400/modl2b.f $(src)400/bhlum4.f $(src)glibk/glibk.f $(src)glibk/combine.f $(src)glibk/yfslib.f $(use_requirements) $(cmt_final_setup_bhlumi)
	$(echo) "(bhlumi.make) Rebuilding $@"; \
	  $(build_dependencies) bhlumi -all_sources -out=$@ $(src)400/bhllog.f $(src)400/oldbis.f $(src)400/bhlumi.f $(src)400/m2agzi.f $(src)400/repi.f $(src)400/lumlog.f $(src)400/model.f $(src)400/modl2a.f $(src)400/modl2b.f $(src)400/bhlum4.f $(src)glibk/glibk.f $(src)glibk/combine.f $(src)glibk/yfslib.f
endif

#$(bhlumi_dependencies)

-include $(bin)bhlumi_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(bhllog_f_dependencies)
 
$(bin)$(binobj)bhllog.o : $(bhllog_f_dependencies)
	$(fortran_echo) $(src)400/bhllog.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(bhllog_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(bhllog_fflags) $(bhllog_f_fflags) $(ppcmd)../src/400 $(src)400/bhllog.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)bhllog.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(bhllog_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(bhllog_fflags) $(bhllog_f_fflags) $(ppcmd)../src/400 $(src)400/bhllog.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(oldbis_f_dependencies)
 
$(bin)$(binobj)oldbis.o : $(oldbis_f_dependencies)
	$(fortran_echo) $(src)400/oldbis.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(oldbis_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(oldbis_fflags) $(oldbis_f_fflags) $(ppcmd)../src/400 $(src)400/oldbis.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)oldbis.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(oldbis_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(oldbis_fflags) $(oldbis_f_fflags) $(ppcmd)../src/400 $(src)400/oldbis.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(bhlumi_f_dependencies)
 
$(bin)$(binobj)bhlumi.o : $(bhlumi_f_dependencies)
	$(fortran_echo) $(src)400/bhlumi.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(bhlumi_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(bhlumi_fflags) $(bhlumi_f_fflags) $(ppcmd)../src/400 $(src)400/bhlumi.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)bhlumi.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(bhlumi_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(bhlumi_fflags) $(bhlumi_f_fflags) $(ppcmd)../src/400 $(src)400/bhlumi.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(m2agzi_f_dependencies)
 
$(bin)$(binobj)m2agzi.o : $(m2agzi_f_dependencies)
	$(fortran_echo) $(src)400/m2agzi.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(m2agzi_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(m2agzi_fflags) $(m2agzi_f_fflags) $(ppcmd)../src/400 $(src)400/m2agzi.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)m2agzi.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(m2agzi_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(m2agzi_fflags) $(m2agzi_f_fflags) $(ppcmd)../src/400 $(src)400/m2agzi.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(repi_f_dependencies)
 
$(bin)$(binobj)repi.o : $(repi_f_dependencies)
	$(fortran_echo) $(src)400/repi.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(repi_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(repi_fflags) $(repi_f_fflags) $(ppcmd)../src/400 $(src)400/repi.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)repi.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(repi_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(repi_fflags) $(repi_f_fflags) $(ppcmd)../src/400 $(src)400/repi.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(lumlog_f_dependencies)
 
$(bin)$(binobj)lumlog.o : $(lumlog_f_dependencies)
	$(fortran_echo) $(src)400/lumlog.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(lumlog_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(lumlog_fflags) $(lumlog_f_fflags) $(ppcmd)../src/400 $(src)400/lumlog.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)lumlog.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(lumlog_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(lumlog_fflags) $(lumlog_f_fflags) $(ppcmd)../src/400 $(src)400/lumlog.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(model_f_dependencies)
 
$(bin)$(binobj)model.o : $(model_f_dependencies)
	$(fortran_echo) $(src)400/model.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(model_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(model_fflags) $(model_f_fflags) $(ppcmd)../src/400 $(src)400/model.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)model.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(model_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(model_fflags) $(model_f_fflags) $(ppcmd)../src/400 $(src)400/model.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(modl2a_f_dependencies)
 
$(bin)$(binobj)modl2a.o : $(modl2a_f_dependencies)
	$(fortran_echo) $(src)400/modl2a.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(modl2a_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(modl2a_fflags) $(modl2a_f_fflags) $(ppcmd)../src/400 $(src)400/modl2a.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)modl2a.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(modl2a_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(modl2a_fflags) $(modl2a_f_fflags) $(ppcmd)../src/400 $(src)400/modl2a.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(modl2b_f_dependencies)
 
$(bin)$(binobj)modl2b.o : $(modl2b_f_dependencies)
	$(fortran_echo) $(src)400/modl2b.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(modl2b_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(modl2b_fflags) $(modl2b_f_fflags) $(ppcmd)../src/400 $(src)400/modl2b.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)modl2b.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(modl2b_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(modl2b_fflags) $(modl2b_f_fflags) $(ppcmd)../src/400 $(src)400/modl2b.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(bhlum4_f_dependencies)
 
$(bin)$(binobj)bhlum4.o : $(bhlum4_f_dependencies)
	$(fortran_echo) $(src)400/bhlum4.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(bhlum4_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(bhlum4_fflags) $(bhlum4_f_fflags) $(ppcmd)../src/400 $(src)400/bhlum4.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)bhlum4.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(bhlum4_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(bhlum4_fflags) $(bhlum4_f_fflags) $(ppcmd)../src/400 $(src)400/bhlum4.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(glibk_f_dependencies)
 
$(bin)$(binobj)glibk.o : $(glibk_f_dependencies)
	$(fortran_echo) $(src)glibk/glibk.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(glibk_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(glibk_fflags) $(glibk_f_fflags) $(ppcmd)../src/glibk $(src)glibk/glibk.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)glibk.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(glibk_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(glibk_fflags) $(glibk_f_fflags) $(ppcmd)../src/glibk $(src)glibk/glibk.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(combine_f_dependencies)
 
$(bin)$(binobj)combine.o : $(combine_f_dependencies)
	$(fortran_echo) $(src)glibk/combine.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(combine_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(combine_fflags) $(combine_f_fflags) $(ppcmd)../src/glibk $(src)glibk/combine.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)combine.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(combine_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(combine_fflags) $(combine_f_fflags) $(ppcmd)../src/glibk $(src)glibk/combine.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)bhlumi_dependencies.make : $(yfslib_f_dependencies)
 
$(bin)$(binobj)yfslib.o : $(yfslib_f_dependencies)
	$(fortran_echo) $(src)glibk/yfslib.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(yfslib_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(yfslib_fflags) $(yfslib_f_fflags) $(ppcmd)../src/glibk $(src)glibk/yfslib.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)yfslib.o $(use_pp_fflags) $(bhlumi_pp_fflags) $(lib_bhlumi_pp_fflags) $(yfslib_pp_fflags) $(use_fflags) $(bhlumi_fflags) $(lib_bhlumi_fflags) $(yfslib_fflags) $(yfslib_f_fflags) $(ppcmd)../src/glibk $(src)glibk/yfslib.f

#-- end of fortran_library ------
#-- start of cleanup_header --------------

clean :: bhlumiclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(bhlumi.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(bhlumi.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(bhlumi.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(bhlumi.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_bhlumi)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(bhlumi.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(bhlumi.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(bhlumi.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

bhlumiclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library bhlumi
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)bhlumi$(library_suffix).a $(library_prefix)bhlumi$(library_suffix).s? bhlumi.stamp bhlumi.shstamp
#-- end of cleanup_library ---------------
