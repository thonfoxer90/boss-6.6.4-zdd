//*****************************************************************************
//
// Generator/Bhlumi/Bhlumi.h
//
// Algorithm runs small angle  Bhabha event generator BHLUMI
// and stores output to transient store
//
// Jan 2006 Original BES3 code by Alexey Zhemchugov
//
//*****************************************************************************

#ifndef GENERATORMODULESEVTDECAY_H
#define GENERATORMODULESEVTDECAY_H

#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"

#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/ISvcLocator.h"

#include <vector>

#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <stdio.h>
#include <iostream>
#include <TStyle.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>

class IBesRndmGenSvc;

class Bhlumi:public Algorithm 
{	
public:
 Bhlumi(const std::string& name, ISvcLocator* pSvcLocator);
		
 StatusCode initialize();
 StatusCode execute();
 StatusCode finalize();

private:
 double xpar[100];
 int npar[100];        	

 IBesRndmGenSvc* p_BesRndmGenSvc; 

 // angle unit control
 int m_angleMode;
// jobOption params
 double m_cmEnergy, m_minThetaAngle, m_maxThetaAngle, m_infraredCut;
 // Initial Seed
 std::vector<int> m_initSeed;
 
 //Brice Garillon : NTuples variables
	TFile* m_foutput;
	TTree* m_ntuple;
	int m_MCntracks;
	int m_MCpartProp[50];
	double m_MCiniE[50];
	double m_MCiniMomX[50];
	double m_MCiniMomY[50];
	double m_MCiniMomZ[50];
	double m_MCiniMomRho[50];
	double m_MCiniMomTheta[50];
	double m_MCiniMomPhi[50];
};

#endif
