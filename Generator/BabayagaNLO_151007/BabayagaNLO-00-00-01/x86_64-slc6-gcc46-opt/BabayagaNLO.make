#-- start of make_header -----------------

#====================================
#  Library BabayagaNLO
#
#   Generated Fri Sep 25 15:18:40 2015  by honig
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_BabayagaNLO_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_BabayagaNLO_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_BabayagaNLO

BabayagaNLO_tag = $(tag)

#cmt_local_tagfile_BabayagaNLO = $(BabayagaNLO_tag)_BabayagaNLO.make
cmt_local_tagfile_BabayagaNLO = $(bin)$(BabayagaNLO_tag)_BabayagaNLO.make

else

tags      = $(tag),$(CMTEXTRATAGS)

BabayagaNLO_tag = $(tag)

#cmt_local_tagfile_BabayagaNLO = $(BabayagaNLO_tag).make
cmt_local_tagfile_BabayagaNLO = $(bin)$(BabayagaNLO_tag).make

endif

include $(cmt_local_tagfile_BabayagaNLO)
#-include $(cmt_local_tagfile_BabayagaNLO)

ifdef cmt_BabayagaNLO_has_target_tag

cmt_final_setup_BabayagaNLO = $(bin)setup_BabayagaNLO.make
cmt_dependencies_in_BabayagaNLO = $(bin)dependencies_BabayagaNLO.in
#cmt_final_setup_BabayagaNLO = $(bin)BabayagaNLO_BabayagaNLOsetup.make
cmt_local_BabayagaNLO_makefile = $(bin)BabayagaNLO.make

else

cmt_final_setup_BabayagaNLO = $(bin)setup.make
cmt_dependencies_in_BabayagaNLO = $(bin)dependencies.in
#cmt_final_setup_BabayagaNLO = $(bin)BabayagaNLOsetup.make
cmt_local_BabayagaNLO_makefile = $(bin)BabayagaNLO.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)BabayagaNLOsetup.make

#BabayagaNLO :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'BabayagaNLO'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = BabayagaNLO/
#BabayagaNLO::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

BabayagaNLOlibname   = $(bin)$(library_prefix)BabayagaNLO$(library_suffix)
BabayagaNLOlib       = $(BabayagaNLOlibname).a
BabayagaNLOstamp     = $(bin)BabayagaNLO.stamp
BabayagaNLOshstamp   = $(bin)BabayagaNLO.shstamp

BabayagaNLO :: dirs  BabayagaNLOLIB
	$(echo) "BabayagaNLO ok"

#-- end of libary_header ----------------

BabayagaNLOLIB :: $(BabayagaNLOlib) $(BabayagaNLOshstamp)
	@/bin/echo "------> BabayagaNLO : library ok"

$(BabayagaNLOlib) :: $(bin)sampling.o $(bin)phasespace.o $(bin)Rteubner.o $(bin)Acp.o $(bin)userinterface.o $(bin)distributions.o $(bin)intpl.o $(bin)mapmomenta.o $(bin)Asu3.o $(bin)mtx_eeenudbb.o $(bin)sv.o $(bin)routines.o $(bin)hadr5n.o $(bin)interface.o $(bin)matrix_model.o $(bin)userroutines.o $(bin)Aint.o $(bin)BabayagaNLO.o $(bin)BabayagaNLORandom.o $(bin)Entries.o $(bin)Load.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(BabayagaNLOlib) $?
	$(lib_silent) $(ranlib) $(BabayagaNLOlib)
	$(lib_silent) cat /dev/null >$(BabayagaNLOstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(BabayagaNLOlibname).$(shlibsuffix) :: $(BabayagaNLOlib) $(BabayagaNLOstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" BabayagaNLO $(BabayagaNLO_shlibflags)

$(BabayagaNLOshstamp) :: $(BabayagaNLOlibname).$(shlibsuffix)
	@if test -f $(BabayagaNLOlibname).$(shlibsuffix) ; then cat /dev/null >$(BabayagaNLOshstamp) ; fi

BabayagaNLOclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)sampling.o $(bin)phasespace.o $(bin)Rteubner.o $(bin)Acp.o $(bin)userinterface.o $(bin)distributions.o $(bin)intpl.o $(bin)mapmomenta.o $(bin)Asu3.o $(bin)mtx_eeenudbb.o $(bin)sv.o $(bin)routines.o $(bin)hadr5n.o $(bin)interface.o $(bin)matrix_model.o $(bin)userroutines.o $(bin)Aint.o $(bin)BabayagaNLO.o $(bin)BabayagaNLORandom.o $(bin)Entries.o $(bin)Load.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
BabayagaNLOinstallname = $(library_prefix)BabayagaNLO$(library_suffix).$(shlibsuffix)

BabayagaNLO :: BabayagaNLOinstall

install :: BabayagaNLOinstall

BabayagaNLOinstall :: $(install_dir)/$(BabayagaNLOinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(BabayagaNLOinstallname) :: $(bin)$(BabayagaNLOinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(BabayagaNLOinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(BabayagaNLOinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(BabayagaNLOinstallname) $(install_dir)/$(BabayagaNLOinstallname); \
	      echo `pwd`/$(BabayagaNLOinstallname) >$(install_dir)/$(BabayagaNLOinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(BabayagaNLOinstallname), no installation directory specified"; \
	  fi; \
	fi

BabayagaNLOclean :: BabayagaNLOuninstall

uninstall :: BabayagaNLOuninstall

BabayagaNLOuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(BabayagaNLOinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(BabayagaNLOinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependencies ------------------
ifneq ($(MAKECMDGOALS),BabayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)

$(bin)BabayagaNLO_dependencies.make : $(use_requirements) $(cmt_final_setup_BabayagaNLO)
	$(echo) "(BabayagaNLO.make) Rebuilding $@"; \
	  $(build_dependencies) -out=$@ -start_all $(src)Babayaga_NLO/sampling.f $(src)Babayaga_NLO/phasespace.f $(src)Babayaga_NLO/Rteubner.f $(src)Babayaga_NLO/Acp.f $(src)Babayaga_NLO/userinterface.f $(src)Babayaga_NLO/distributions.f $(src)Babayaga_NLO/intpl.f $(src)Babayaga_NLO/mapmomenta.f $(src)Babayaga_NLO/Asu3.f $(src)Babayaga_NLO/mtx_eeenudbb.f $(src)Babayaga_NLO/sv.f $(src)Babayaga_NLO/routines.f $(src)Babayaga_NLO/hadr5n.f $(src)Babayaga_NLO/interface.f $(src)Babayaga_NLO/matrix_model.f $(src)Babayaga_NLO/userroutines.f $(src)Babayaga_NLO/Aint.f -end_all $(includes) $(app_BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) -name=BabayagaNLO $? -f=$(cmt_dependencies_in_BabayagaNLO) -without_cmt

-include $(bin)BabayagaNLO_dependencies.make

endif
endif

BabayagaNLOclean ::
	$(cleanup_silent) \rm -rf $(bin)BabayagaNLO_deps $(bin)BabayagaNLO_dependencies.make
#-- end of dependencies -------------------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(sampling_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/sampling.f
 
$(bin)$(binobj)sampling.o : $(sampling_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/sampling.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(sampling_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(sampling_fflags) $(sampling_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/sampling.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sampling.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(sampling_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(sampling_fflags) $(sampling_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/sampling.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(phasespace_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/phasespace.f
 
$(bin)$(binobj)phasespace.o : $(phasespace_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/phasespace.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(phasespace_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(phasespace_fflags) $(phasespace_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/phasespace.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)phasespace.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(phasespace_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(phasespace_fflags) $(phasespace_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/phasespace.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(Rteubner_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/Rteubner.f
 
$(bin)$(binobj)Rteubner.o : $(Rteubner_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/Rteubner.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(Rteubner_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(Rteubner_fflags) $(Rteubner_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Rteubner.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)Rteubner.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(Rteubner_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(Rteubner_fflags) $(Rteubner_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Rteubner.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(Acp_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/Acp.f
 
$(bin)$(binobj)Acp.o : $(Acp_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/Acp.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(Acp_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(Acp_fflags) $(Acp_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Acp.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)Acp.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(Acp_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(Acp_fflags) $(Acp_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Acp.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(userinterface_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/userinterface.f
 
$(bin)$(binobj)userinterface.o : $(userinterface_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/userinterface.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(userinterface_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(userinterface_fflags) $(userinterface_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/userinterface.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)userinterface.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(userinterface_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(userinterface_fflags) $(userinterface_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/userinterface.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(distributions_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/distributions.f
 
$(bin)$(binobj)distributions.o : $(distributions_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/distributions.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(distributions_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(distributions_fflags) $(distributions_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/distributions.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)distributions.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(distributions_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(distributions_fflags) $(distributions_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/distributions.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(intpl_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/intpl.f
 
$(bin)$(binobj)intpl.o : $(intpl_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/intpl.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(intpl_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(intpl_fflags) $(intpl_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/intpl.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)intpl.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(intpl_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(intpl_fflags) $(intpl_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/intpl.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(mapmomenta_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/mapmomenta.f
 
$(bin)$(binobj)mapmomenta.o : $(mapmomenta_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/mapmomenta.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(mapmomenta_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(mapmomenta_fflags) $(mapmomenta_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/mapmomenta.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)mapmomenta.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(mapmomenta_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(mapmomenta_fflags) $(mapmomenta_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/mapmomenta.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(Asu3_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/Asu3.f
 
$(bin)$(binobj)Asu3.o : $(Asu3_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/Asu3.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(Asu3_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(Asu3_fflags) $(Asu3_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Asu3.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)Asu3.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(Asu3_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(Asu3_fflags) $(Asu3_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Asu3.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(mtx_eeenudbb_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/mtx_eeenudbb.f
 
$(bin)$(binobj)mtx_eeenudbb.o : $(mtx_eeenudbb_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/mtx_eeenudbb.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(mtx_eeenudbb_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(mtx_eeenudbb_fflags) $(mtx_eeenudbb_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/mtx_eeenudbb.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)mtx_eeenudbb.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(mtx_eeenudbb_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(mtx_eeenudbb_fflags) $(mtx_eeenudbb_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/mtx_eeenudbb.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(sv_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/sv.f
 
$(bin)$(binobj)sv.o : $(sv_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/sv.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(sv_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(sv_fflags) $(sv_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/sv.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)sv.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(sv_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(sv_fflags) $(sv_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/sv.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(routines_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/routines.f
 
$(bin)$(binobj)routines.o : $(routines_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/routines.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(routines_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(routines_fflags) $(routines_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/routines.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)routines.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(routines_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(routines_fflags) $(routines_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/routines.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(hadr5n_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/hadr5n.f
 
$(bin)$(binobj)hadr5n.o : $(hadr5n_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/hadr5n.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(hadr5n_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(hadr5n_fflags) $(hadr5n_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/hadr5n.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)hadr5n.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(hadr5n_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(hadr5n_fflags) $(hadr5n_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/hadr5n.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(interface_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/interface.f
 
$(bin)$(binobj)interface.o : $(interface_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/interface.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(interface_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(interface_fflags) $(interface_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/interface.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)interface.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(interface_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(interface_fflags) $(interface_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/interface.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(matrix_model_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/matrix_model.f
 
$(bin)$(binobj)matrix_model.o : $(matrix_model_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/matrix_model.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(matrix_model_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(matrix_model_fflags) $(matrix_model_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/matrix_model.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)matrix_model.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(matrix_model_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(matrix_model_fflags) $(matrix_model_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/matrix_model.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(userroutines_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/userroutines.f
 
$(bin)$(binobj)userroutines.o : $(userroutines_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/userroutines.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(userroutines_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(userroutines_fflags) $(userroutines_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/userroutines.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)userroutines.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(userroutines_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(userroutines_fflags) $(userroutines_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/userroutines.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)BabayagaNLO_dependencies.make : $(Aint_f_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Babayaga_NLO/Aint.f
 
$(bin)$(binobj)Aint.o : $(Aint_f_dependencies)
	$(fortran_echo) $(src)Babayaga_NLO/Aint.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(Aint_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(Aint_fflags) $(Aint_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Aint.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)Aint.o $(use_pp_fflags) $(BabayagaNLO_pp_fflags) $(lib_BabayagaNLO_pp_fflags) $(Aint_pp_fflags) $(use_fflags) $(BabayagaNLO_fflags) $(lib_BabayagaNLO_fflags) $(Aint_fflags) $(Aint_f_fflags) $(ppcmd)../src/Babayaga_NLO $(src)Babayaga_NLO/Aint.f

#-- end of fortran_library ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),BabayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BabayagaNLO.d

$(bin)$(binobj)BabayagaNLO.d :

$(bin)$(binobj)BabayagaNLO.o : $(cmt_final_setup_BabayagaNLO)

$(bin)$(binobj)BabayagaNLO.o : $(src)BabayagaNLO.cxx
	$(cpp_echo) $(src)BabayagaNLO.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(BabayagaNLO_cppflags) $(BabayagaNLO_cxx_cppflags)  $(src)BabayagaNLO.cxx
endif
endif

else
$(bin)BabayagaNLO_dependencies.make : $(BabayagaNLO_cxx_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)BabayagaNLO.cxx

$(bin)$(binobj)BabayagaNLO.o : $(BabayagaNLO_cxx_dependencies)
	$(cpp_echo) $(src)BabayagaNLO.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(BabayagaNLO_cppflags) $(BabayagaNLO_cxx_cppflags)  $(src)BabayagaNLO.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),BabayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BabayagaNLORandom.d

$(bin)$(binobj)BabayagaNLORandom.d :

$(bin)$(binobj)BabayagaNLORandom.o : $(cmt_final_setup_BabayagaNLO)

$(bin)$(binobj)BabayagaNLORandom.o : $(src)BabayagaNLORandom.cxx
	$(cpp_echo) $(src)BabayagaNLORandom.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(BabayagaNLORandom_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(BabayagaNLORandom_cppflags) $(BabayagaNLORandom_cxx_cppflags)  $(src)BabayagaNLORandom.cxx
endif
endif

else
$(bin)BabayagaNLO_dependencies.make : $(BabayagaNLORandom_cxx_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)BabayagaNLORandom.cxx

$(bin)$(binobj)BabayagaNLORandom.o : $(BabayagaNLORandom_cxx_dependencies)
	$(cpp_echo) $(src)BabayagaNLORandom.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(BabayagaNLORandom_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(BabayagaNLORandom_cppflags) $(BabayagaNLORandom_cxx_cppflags)  $(src)BabayagaNLORandom.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),BabayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Entries.d

$(bin)$(binobj)Entries.d :

$(bin)$(binobj)Entries.o : $(cmt_final_setup_BabayagaNLO)

$(bin)$(binobj)Entries.o : $(src)Entries.cxx
	$(cpp_echo) $(src)Entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(Entries_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(Entries_cppflags) $(Entries_cxx_cppflags)  $(src)Entries.cxx
endif
endif

else
$(bin)BabayagaNLO_dependencies.make : $(Entries_cxx_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Entries.cxx

$(bin)$(binobj)Entries.o : $(Entries_cxx_dependencies)
	$(cpp_echo) $(src)Entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(Entries_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(Entries_cppflags) $(Entries_cxx_cppflags)  $(src)Entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),BabayagaNLOclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Load.d

$(bin)$(binobj)Load.d :

$(bin)$(binobj)Load.o : $(cmt_final_setup_BabayagaNLO)

$(bin)$(binobj)Load.o : $(src)Load.cxx
	$(cpp_echo) $(src)Load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(Load_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(Load_cppflags) $(Load_cxx_cppflags)  $(src)Load.cxx
endif
endif

else
$(bin)BabayagaNLO_dependencies.make : $(Load_cxx_dependencies)

$(bin)BabayagaNLO_dependencies.make : $(src)Load.cxx

$(bin)$(binobj)Load.o : $(Load_cxx_dependencies)
	$(cpp_echo) $(src)Load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(lib_BabayagaNLO_pp_cppflags) $(Load_pp_cppflags) $(use_cppflags) $(BabayagaNLO_cppflags) $(lib_BabayagaNLO_cppflags) $(Load_cppflags) $(Load_cxx_cppflags)  $(src)Load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: BabayagaNLOclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(BabayagaNLO.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

BabayagaNLOclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library BabayagaNLO
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)BabayagaNLO$(library_suffix).a $(library_prefix)BabayagaNLO$(library_suffix).s? BabayagaNLO.stamp BabayagaNLO.shstamp
#-- end of cleanup_library ---------------
