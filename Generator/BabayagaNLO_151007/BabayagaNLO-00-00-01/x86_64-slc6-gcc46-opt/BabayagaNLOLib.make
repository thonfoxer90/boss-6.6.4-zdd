#-- start of make_header -----------------

#====================================
#  Library BabayagaNLOLib
#
#   Generated Fri Sep 25 15:18:35 2015  by honig
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_BabayagaNLOLib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_BabayagaNLOLib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_BabayagaNLOLib

BabayagaNLO_tag = $(tag)

#cmt_local_tagfile_BabayagaNLOLib = $(BabayagaNLO_tag)_BabayagaNLOLib.make
cmt_local_tagfile_BabayagaNLOLib = $(bin)$(BabayagaNLO_tag)_BabayagaNLOLib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

BabayagaNLO_tag = $(tag)

#cmt_local_tagfile_BabayagaNLOLib = $(BabayagaNLO_tag).make
cmt_local_tagfile_BabayagaNLOLib = $(bin)$(BabayagaNLO_tag).make

endif

include $(cmt_local_tagfile_BabayagaNLOLib)
#-include $(cmt_local_tagfile_BabayagaNLOLib)

ifdef cmt_BabayagaNLOLib_has_target_tag

cmt_final_setup_BabayagaNLOLib = $(bin)setup_BabayagaNLOLib.make
cmt_dependencies_in_BabayagaNLOLib = $(bin)dependencies_BabayagaNLOLib.in
#cmt_final_setup_BabayagaNLOLib = $(bin)BabayagaNLO_BabayagaNLOLibsetup.make
cmt_local_BabayagaNLOLib_makefile = $(bin)BabayagaNLOLib.make

else

cmt_final_setup_BabayagaNLOLib = $(bin)setup.make
cmt_dependencies_in_BabayagaNLOLib = $(bin)dependencies.in
#cmt_final_setup_BabayagaNLOLib = $(bin)BabayagaNLOsetup.make
cmt_local_BabayagaNLOLib_makefile = $(bin)BabayagaNLOLib.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)BabayagaNLOsetup.make

#BabayagaNLOLib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'BabayagaNLOLib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = BabayagaNLOLib/
#BabayagaNLOLib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

BabayagaNLOLiblibname   = $(bin)$(library_prefix)BabayagaNLOLib$(library_suffix)
BabayagaNLOLiblib       = $(BabayagaNLOLiblibname).a
BabayagaNLOLibstamp     = $(bin)BabayagaNLOLib.stamp
BabayagaNLOLibshstamp   = $(bin)BabayagaNLOLib.shstamp

BabayagaNLOLib :: dirs  BabayagaNLOLibLIB
	$(echo) "BabayagaNLOLib ok"

#-- end of libary_header ----------------

BabayagaNLOLibLIB :: $(BabayagaNLOLiblib) $(BabayagaNLOLibshstamp)
	@/bin/echo "------> BabayagaNLOLib : library ok"

$(BabayagaNLOLiblib) :: $(bin)BabayagaNLO.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(BabayagaNLOLiblib) $?
	$(lib_silent) $(ranlib) $(BabayagaNLOLiblib)
	$(lib_silent) cat /dev/null >$(BabayagaNLOLibstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(BabayagaNLOLiblibname).$(shlibsuffix) :: $(BabayagaNLOLiblib) $(BabayagaNLOLibstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" BabayagaNLOLib $(BabayagaNLOLib_shlibflags)

$(BabayagaNLOLibshstamp) :: $(BabayagaNLOLiblibname).$(shlibsuffix)
	@if test -f $(BabayagaNLOLiblibname).$(shlibsuffix) ; then cat /dev/null >$(BabayagaNLOLibshstamp) ; fi

BabayagaNLOLibclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)BabayagaNLO.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
BabayagaNLOLibinstallname = $(library_prefix)BabayagaNLOLib$(library_suffix).$(shlibsuffix)

BabayagaNLOLib :: BabayagaNLOLibinstall

install :: BabayagaNLOLibinstall

BabayagaNLOLibinstall :: $(install_dir)/$(BabayagaNLOLibinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(BabayagaNLOLibinstallname) :: $(bin)$(BabayagaNLOLibinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(BabayagaNLOLibinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(BabayagaNLOLibinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOLibinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOLibinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(BabayagaNLOLibinstallname) $(install_dir)/$(BabayagaNLOLibinstallname); \
	      echo `pwd`/$(BabayagaNLOLibinstallname) >$(install_dir)/$(BabayagaNLOLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(BabayagaNLOLibinstallname), no installation directory specified"; \
	  fi; \
	fi

BabayagaNLOLibclean :: BabayagaNLOLibuninstall

uninstall :: BabayagaNLOLibuninstall

BabayagaNLOLibuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(BabayagaNLOLibinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOLibinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaNLOLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(BabayagaNLOLibinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),BabayagaNLOLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BabayagaNLO.d

$(bin)$(binobj)BabayagaNLO.d :

$(bin)$(binobj)BabayagaNLO.o : $(cmt_final_setup_BabayagaNLOLib)

$(bin)$(binobj)BabayagaNLO.o : $(src)BabayagaNLO.cxx
	$(cpp_echo) $(src)BabayagaNLO.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(BabayagaNLOLib_pp_cppflags) $(lib_BabayagaNLOLib_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(use_cppflags) $(BabayagaNLOLib_cppflags) $(lib_BabayagaNLOLib_cppflags) $(BabayagaNLO_cppflags) $(BabayagaNLO_cxx_cppflags)  $(src)BabayagaNLO.cxx
endif
endif

else
$(bin)BabayagaNLOLib_dependencies.make : $(BabayagaNLO_cxx_dependencies)

$(bin)BabayagaNLOLib_dependencies.make : $(src)BabayagaNLO.cxx

$(bin)$(binobj)BabayagaNLO.o : $(BabayagaNLO_cxx_dependencies)
	$(cpp_echo) $(src)BabayagaNLO.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BabayagaNLOLib_pp_cppflags) $(lib_BabayagaNLOLib_pp_cppflags) $(BabayagaNLO_pp_cppflags) $(use_cppflags) $(BabayagaNLOLib_cppflags) $(lib_BabayagaNLOLib_cppflags) $(BabayagaNLO_cppflags) $(BabayagaNLO_cxx_cppflags)  $(src)BabayagaNLO.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: BabayagaNLOLibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(BabayagaNLOLib.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

BabayagaNLOLibclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library BabayagaNLOLib
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)BabayagaNLOLib$(library_suffix).a $(library_prefix)BabayagaNLOLib$(library_suffix).s? BabayagaNLOLib.stamp BabayagaNLOLib.shstamp
#-- end of cleanup_library ---------------
