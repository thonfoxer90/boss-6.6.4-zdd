----------> uses
# use BesPolicy BesPolicy-* 
#   use BesCxxPolicy BesCxxPolicy-* 
#     use GaudiPolicy v*  (no_version_directory)
#       use LCG_Settings *  (no_version_directory)
#       use Python * LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.7.3)
#         use LCG_Configuration v*  (no_version_directory)
#         use LCG_Settings v*  (no_version_directory)
#       use tcmalloc * LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=1.7p3)
#         use LCG_Configuration v*  (no_version_directory)
#         use LCG_Settings v*  (no_version_directory)
#         use libunwind v* LCG_Interfaces (no_version_directory) (native_version=5c2cade)
#           use LCG_Configuration v*  (no_version_directory)
#           use LCG_Settings v*  (no_version_directory)
#   use BesFortranPolicy BesFortranPolicy-* 
#     use LCG_Settings v*  (no_version_directory)
# use BesServices BesServices-* Control
#   use BesPolicy BesPolicy-* 
#   use BesKernel BesKernel-* Control
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-01-* External
#       use GaudiKernel *  (no_version_directory)
#         use GaudiPolicy *  (no_version_directory)
#         use Reflex * LCG_Interfaces (no_version_directory)
#           use LCG_Configuration v*  (no_version_directory)
#             use LCG_Platforms *  (no_version_directory)
#           use LCG_Settings v*  (no_version_directory)
#           use ROOT v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=5.34.09)
#             use LCG_Configuration v*  (no_version_directory)
#             use LCG_Settings v*  (no_version_directory)
#             use GCCXML v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=0.9.0_20120309p2)
#               use LCG_Configuration v*  (no_version_directory)
#               use LCG_Settings v*  (no_version_directory)
#             use Python v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.7.3)
#             use xrootd v* LCG_Interfaces (no_version_directory) (native_version=3.2.7)
#               use LCG_Configuration v*  (no_version_directory)
#               use LCG_Settings v*  (no_version_directory)
#         use Boost * LCG_Interfaces (no_version_directory) (native_version=1.50.0_python2.7)
#           use LCG_Configuration v*  (no_version_directory)
#           use LCG_Settings v*  (no_version_directory)
#           use Python v* LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.7.3)
#       use GaudiSvc *  (no_version_directory)
#         use GaudiKernel *  (no_version_directory)
#         use CLHEP * LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=2.0.4.5)
#         use Boost * LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=1.50.0_python2.7)
#         use ROOT * LCG_Interfaces (no_auto_imports) (no_version_directory) (native_version=5.34.09)
#   use BesBoost BesBoost-* External
#     use Boost v* LCG_Interfaces (no_version_directory) (native_version=1.50.0_python2.7)
#   use CLHEP * LCG_Interfaces (no_version_directory) (native_version=2.0.4.5)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
#   use GaudiInterface GaudiInterface-* External
#   use GaudiSvc v*  (no_version_directory)
# use CERNLIB CERNLIB-* External
#   use cernlib v* LCG_Interfaces (no_version_directory) (native_version=2006a)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
#     use blas v* LCG_Interfaces (no_version_directory) (native_version=20110419)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#     use lapack v* LCG_Interfaces (no_version_directory) (native_version=3.4.0)
#       use LCG_Configuration v*  (no_version_directory)
#       use LCG_Settings v*  (no_version_directory)
#       use blas v* LCG_Interfaces (no_version_directory) (native_version=20110419)
#   use CASTOR v* LCG_Interfaces (no_version_directory) (native_version=2.1.13-6)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
#   use Xt * External (native_version=X11R6)
# use GeneratorObject GeneratorObject-* Generator
#   use BesPolicy BesPolicy-* 
#   use HepMC * LCG_Interfaces (no_version_directory) (native_version=2.06.08)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
#   use EventModel EventModel-* Event
#     use BesPolicy BesPolicy-* 
#     use GaudiInterface GaudiInterface-* External
# use HepMC * LCG_Interfaces (no_version_directory) (native_version=2.06.08)
#
# Selection :
use CMT v1r25 (/cluster/bes3)
use Xt Xt-00-00-02 External (/cluster/bes3/dist/6.6.5)
use LCG_Platforms v1  (/cluster/bes3/LCGCMT/LCGCMT_65a)
use LCG_Configuration v1  (/cluster/bes3/LCGCMT/LCGCMT_65a)
use LCG_Settings v1  (/cluster/bes3/LCGCMT/LCGCMT_65a)
use HepMC v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a)
use CASTOR v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a)
use blas v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a)
use lapack v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a)
use cernlib v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a)
use CERNLIB CERNLIB-01-02-03 External (/cluster/bes3/dist/6.6.5)
use CLHEP v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a)
use xrootd v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a) (no_auto_imports)
use GCCXML v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a) (no_auto_imports)
use BesFortranPolicy BesFortranPolicy-00-01-03  (/cluster/bes3/dist/6.6.5)
use libunwind v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a) (no_auto_imports)
use tcmalloc v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a) (no_auto_imports)
use Python v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a) (no_auto_imports)
use Boost v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a)
use BesBoost BesBoost-00-00-01 External (/cluster/bes3/dist/6.6.5)
use ROOT v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a) (no_auto_imports)
use Reflex v1 LCG_Interfaces (/cluster/bes3/LCGCMT/LCGCMT_65a)
use GaudiPolicy v12r7  (/cluster/bes3/gaudi/GAUDI_v23r9)
use GaudiKernel v28r8  (/cluster/bes3/gaudi/GAUDI_v23r9)
use GaudiSvc v19r4  (/cluster/bes3/gaudi/GAUDI_v23r9)
use GaudiInterface GaudiInterface-01-03-07 External (/cluster/bes3/dist/6.6.5)
use BesCxxPolicy BesCxxPolicy-00-01-01  (/cluster/bes3/dist/6.6.5)
use BesPolicy BesPolicy-01-05-05  (/cluster/bes3/dist/6.6.5)
use EventModel EventModel-01-05-33 Event (/cluster/bes3/dist/6.6.5)
use GeneratorObject GeneratorObject-00-01-05 Generator (/cluster/bes3/dist/6.6.5)
use BesKernel BesKernel-00-00-03 Control (/cluster/bes3/dist/6.6.5)
use BesServices BesServices-00-00-11 Control (/cluster/bes3/dist/6.6.5)
----------> tags
CMTv1 (from CMTVERSION)
CMTr25 (from CMTVERSION)
CMTp0 (from CMTVERSION)
Linux (from uname) package [CMT LCG_Platforms BesPolicy] implies [Unix host-linux]
x86_64-slc6-gcc46-opt (from CMTCONFIG) package [LCG_Platforms] implies [target-x86_64 target-slc6 target-gcc46 target-opt]
honig_no_config (from PROJECT) excludes [honig_config]
honig_root (from PROJECT) excludes [honig_no_root]
honig_cleanup (from PROJECT) excludes [honig_no_cleanup]
honig_scripts (from PROJECT) excludes [honig_no_scripts]
honig_prototypes (from PROJECT) excludes [honig_no_prototypes]
honig_with_installarea (from PROJECT) excludes [honig_without_installarea]
honig_with_version_directory (from PROJECT) excludes [honig_without_version_directory]
honig (from PROJECT)
BOSS_no_config (from PROJECT) excludes [BOSS_config]
BOSS_root (from PROJECT) excludes [BOSS_no_root]
BOSS_cleanup (from PROJECT) excludes [BOSS_no_cleanup]
BOSS_scripts (from PROJECT) excludes [BOSS_no_scripts]
BOSS_no_prototypes (from PROJECT) excludes [BOSS_prototypes]
BOSS_with_installarea (from PROJECT) excludes [BOSS_without_installarea]
BOSS_with_version_directory (from PROJECT) excludes [BOSS_without_version_directory]
GAUDI_no_config (from PROJECT) excludes [GAUDI_config]
GAUDI_root (from PROJECT) excludes [GAUDI_no_root]
GAUDI_cleanup (from PROJECT) excludes [GAUDI_no_cleanup]
GAUDI_scripts (from PROJECT) excludes [GAUDI_no_scripts]
GAUDI_prototypes (from PROJECT) excludes [GAUDI_no_prototypes]
GAUDI_with_installarea (from PROJECT) excludes [GAUDI_without_installarea]
GAUDI_without_version_directory (from PROJECT) excludes [GAUDI_with_version_directory]
LCGCMT_no_config (from PROJECT) excludes [LCGCMT_config]
LCGCMT_no_root (from PROJECT) excludes [LCGCMT_root]
LCGCMT_cleanup (from PROJECT) excludes [LCGCMT_no_cleanup]
LCGCMT_scripts (from PROJECT) excludes [LCGCMT_no_scripts]
LCGCMT_prototypes (from PROJECT) excludes [LCGCMT_no_prototypes]
LCGCMT_without_installarea (from PROJECT) excludes [LCGCMT_with_installarea]
LCGCMT_with_version_directory (from PROJECT) excludes [LCGCMT_without_version_directory]
x86_64 (from package CMT) package [LCG_Platforms] implies [host-x86_64]
sl60 (from package CMT)
gcc463 (from package CMT)
Unix (from package CMT) package [LCG_Platforms] implies [host-unix] excludes [WIN32 Win32]
c_native_dependencies (from package CMT) activated GaudiPolicy
cpp_native_dependencies (from package CMT) activated GaudiPolicy
target-unix (from package LCG_Settings) activated LCG_Platforms
target-x86_64 (from package LCG_Settings) activated LCG_Platforms
target-gcc46 (from package LCG_Settings) package [LCG_Platforms] implies [target-gcc4 target-lcg-compiler lcg-compiler] activated LCG_Platforms
host-x86_64 (from package LCG_Settings) activated LCG_Platforms
target-slc (from package LCG_Settings) package [LCG_Platforms] implies [target-linux] activated LCG_Platforms
target-gcc (from package LCG_Settings) activated LCG_Platforms
target-gcc4 (from package LCG_Settings) package [LCG_Platforms] implies [target-gcc] activated LCG_Platforms
target-lcg-compiler (from package LCG_Settings) activated LCG_Platforms
host-linux (from package LCG_Platforms) package [LCG_Platforms] implies [host-unix]
host-unix (from package LCG_Platforms)
target-opt (from package LCG_Platforms)
target-slc6 (from package LCG_Platforms) package [LCG_Platforms] implies [target-slc]
target-linux (from package LCG_Platforms) package [LCG_Platforms] implies [target-unix]
lcg-compiler (from package LCG_Platforms)
ROOT_GE_5_15 (from package LCG_Configuration)
ROOT_GE_5_19 (from package LCG_Configuration)
HasAthenaRunTime (from package BesPolicy)
----------> CMTPATH
# Add path /home/honig/boss from initialization
# Add path /cluster/bes3/dist/6.6.5 from initialization
# Add path /cluster/bes3/gaudi/GAUDI_v23r9 from initialization
# Add path /cluster/bes3/LCGCMT/LCGCMT_65a from initialization
