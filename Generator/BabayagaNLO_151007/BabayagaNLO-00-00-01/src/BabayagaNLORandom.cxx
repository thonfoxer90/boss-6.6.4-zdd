//--------------------------------------------------------------------------
//
// Environment:
//      This software is part of the EvtGen package developed jointly
//      for the BaBar and CLEO collaborations.  If you use all or part
//      of it, please give an appropriate acknowledgement.
//
// Copyright Information: See EvtGen/COPYRIGHT
//      Copyright (C) 1998      Caltech, UCSB
//
// Module: EvtRandom.cc
//
// Description: routines to get random numbers from 
//              random number generator.
//
// Modification history:
//
//    DJL/RYD   September 25, 1996           Module created
//    Wang Boqun September 06, 2010          Add fortran interface
//
//------------------------------------------------------------------------
//

#include "BabayagaNLO/BabayagaNLORandom.h"
#include "BabayagaNLO/cfortran.h"

#include "CLHEP/Random/RanluxEngine.h"
#include <iostream>
using namespace std;
using namespace CLHEP;

HepRandomEngine* BabayagaNLORandom::_randomEngine=0;

void BabayagaNLORandom::setRandomEngine(CLHEP::HepRandomEngine* randomEngine){
  _randomEngine=randomEngine;
}


double BabayagaNLORandom::random(){

  if (_randomEngine==0){
    cerr <<"No random engine available in "
			   <<"BabayagaNLORandom::random()."<<endl;
  }

    return _randomEngine->flat();
  }



double BabayagaNLORandom::Flat( double min, double max){

  if ( min > max ) {
    cerr<< "min>max in BabayagaNLORandom::Flat(" << min << "," << max << ")" <<endl;
  }

  return BabayagaNLORandom::random()*( max - min )+min;

} 

double BabayagaNLORandom::Flat(double max){

  return max*BabayagaNLORandom::random();

} 

double BabayagaNLORandom::Flat(){

  return BabayagaNLORandom::random();

 } 

void BabayagaNLORandom::FlatArray(double* vect, const int size){
  if(_randomEngine == 0) 
	  cout<<"Can not get randomEngine pointer in BabayagaNLORandom::FlatArray"<<endl;
  else {
	  _randomEngine->flatArray(size,vect);
  }
}

FCALLSCSUB2(BabayagaNLORandom::FlatArray,FLATARRAY,flatarray,DOUBLEV,INT)
