----------> uses
#
# Selection :
use CMT v1r25 (/cluster/bes3)
----------> tags
CMTv1 (from CMTVERSION)
CMTr25 (from CMTVERSION)
CMTp0 (from CMTVERSION)
Linux (from uname) package [CMT] implies [Unix]
x86_64-slc6-gcc46-opt (from CMTCONFIG)
honig_no_config (from PROJECT) excludes [honig_config]
honig_root (from PROJECT) excludes [honig_no_root]
honig_cleanup (from PROJECT) excludes [honig_no_cleanup]
honig_scripts (from PROJECT) excludes [honig_no_scripts]
honig_prototypes (from PROJECT) excludes [honig_no_prototypes]
honig_with_installarea (from PROJECT) excludes [honig_without_installarea]
honig_with_version_directory (from PROJECT) excludes [honig_without_version_directory]
honig (from PROJECT)
BOSS_no_config (from PROJECT) excludes [BOSS_config]
BOSS_root (from PROJECT) excludes [BOSS_no_root]
BOSS_cleanup (from PROJECT) excludes [BOSS_no_cleanup]
BOSS_scripts (from PROJECT) excludes [BOSS_no_scripts]
BOSS_prototypes (from PROJECT) excludes [BOSS_no_prototypes]
BOSS_with_installarea (from PROJECT) excludes [BOSS_without_installarea]
BOSS_with_version_directory (from PROJECT) excludes [BOSS_without_version_directory]
GAUDI_no_config (from PROJECT) excludes [GAUDI_config]
GAUDI_root (from PROJECT) excludes [GAUDI_no_root]
GAUDI_cleanup (from PROJECT) excludes [GAUDI_no_cleanup]
GAUDI_scripts (from PROJECT) excludes [GAUDI_no_scripts]
GAUDI_prototypes (from PROJECT) excludes [GAUDI_no_prototypes]
GAUDI_with_installarea (from PROJECT) excludes [GAUDI_without_installarea]
GAUDI_without_version_directory (from PROJECT) excludes [GAUDI_with_version_directory]
LCGCMT_no_config (from PROJECT) excludes [LCGCMT_config]
LCGCMT_no_root (from PROJECT) excludes [LCGCMT_root]
LCGCMT_cleanup (from PROJECT) excludes [LCGCMT_no_cleanup]
LCGCMT_scripts (from PROJECT) excludes [LCGCMT_no_scripts]
LCGCMT_prototypes (from PROJECT) excludes [LCGCMT_no_prototypes]
LCGCMT_without_installarea (from PROJECT) excludes [LCGCMT_with_installarea]
LCGCMT_with_version_directory (from PROJECT) excludes [LCGCMT_without_version_directory]
x86_64 (from package CMT)
sl60 (from package CMT)
gcc463 (from package CMT)
Unix (from package CMT) excludes [WIN32 Win32]
----------> CMTPATH
# Add path /home/honig/boss from initialization
# Add path /cluster/bes3/dist/6.6.5 from initialization
# Add path /cluster/bes3/gaudi/GAUDI_v23r9 from initialization
# Add path /cluster/bes3/LCGCMT/LCGCMT_65a from initialization
