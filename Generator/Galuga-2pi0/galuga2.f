*
      Subroutine ggLcrs(rs,W)
      Implicit None
      Integer nfcall,itmx,ndim,nprn,k,i,ifail
      Integer ivegas,iwaght,iapprx,imodel,npts,nzero,ntrial
      Double Precision cross,error,acc,ggLuF,Whad,roots,Wmin,Wmax,
     &     t2user,t2umin,t2umax,xl,xu,chi2a,Fmax,Fmin,Weight,nbarn, 
     &     pts,avF,avFsq,Sigma,effic,x(10),y,SumFi,SumFsq
      Double Precision s,rs,W,m,Pi,alem
      Double Precision
     &     th1min,th1max,E1min,E1max,th2min,th2max,E2min,E2max
      Double Precision Rmass,Rwidth,Pmass,Rtotw,mPion,arJ(6),sigR
      real time1,time2,rvec(10)
      Integer it1,iD1,iD3,iD5,itX,iph,ip1,ip2,ia1,ia2,ia3,ia4,ie1,ie2,is
     &     ,ipt
      Integer iJP,iq,i1,i23
      Character*9 Rname
*
      External ggLuF
*
      Common /ggLerr/ 
     &   it1,iD1,iD3,iD5,itX,iph,ip1,ip2,ia1,ia2,ia3,ia4,ie1,ie2,ipt,is
      Common /ggLprm/ s,roots,Whad,m,Pi,alem
      Common /ggLvg1/ xl(10),xu(10),acc,ndim,nfcall,itmx,nprn
      Common /ggLapp/Wmin,Wmax,t2user,t2umin,t2umax,iapprx,ivegas,iwaght
      Common /ggLmod/ imodel
      Common /ggLcut/th1min,th1max,E1min,E1max,th2min,th2max,E2min,E2max
      Common /ggLuno/ cross,error,Fmax,Fmin,Weight,npts,nzero,ntrial
      Common /ggLres/ Rmass,Rwidth,Pmass,Rtotw,iJP,iq,i1
*
      Data mPion /0.1349763d0/  ! Just below the pion mass
      Data arJ /0d0,0d0,1d0,2d0,2d0,0d0/, nbarn /0.389385d+06/
*
      Call Timex(time1)
*
      If(iapprx.eq.1) then      ! d2sigma/dtau dt2
         ndim = 3
      Elseif(iapprx.eq.-1) then ! sigma
         ndim = 5
      Else
         ndim = 4
      Endif
*
      If(imodel.ge.100) then    ! Resonance production
         i23 = mod(imodel,100)
         i1  = imodel/100 
         iq  = mod(i23,10)
         iJP = i23/10
         Call InitMassWidth(iJP,iq,Rmass,Rwidth,Rtotw,Pmass,Rname)
*  For resonances at fixed W take W = resonance mass
         If(iapprx.ne.-1) W = Rmass
      Endif
*
      ifail = 0                 ! check phase space
      If(iapprx.eq.-1) then     ! Wmin,Wmax provided by user
         If(Wmin.lt.mPion) Wmin = mPion
         If(Wmax.gt.rs  ) Wmax = rs
         If(Wmin.ge.Wmax) ifail = 1
      Else                      ! W provided by user
         Wmin = mPion         
         Wmax = rs         
         If(W.lt.Wmin .or. W.gt.Wmax) ifail = 1
      Endif
*
      s      = rs*rs
      roots  = rs
      Whad   = W
      Fmax   = 0d0
      Fmin   = 1d9
      nzero  = 0
*
      If(t2umin.lt.- s  ) t2umin = - s
      If(t2umax.gt.0d0  ) t2umax = 0d0
      If(t2umin.gt.t2umax) ifail = 1
      If(E1min.lt.   0d0) E1min  =    0d0
      If(E2min.lt.   0d0) E2min  =    0d0
      If(E1max.gt.rs/2d0) E1max  = rs/2d0
      If(E2max.gt.rs/2d0) E2max  = rs/2d0
      If(E1min.ge.E1max)  ifail  = 1
      If(E2min.ge.E2max)  ifail  = 1
      If(th1min.lt.  0d0) th1min =    0d0
      If(th2min.lt.  0d0) th2min =    0d0
      If(th1max.gt.180d0) th1max =  180d0
      If(th2max.gt.180d0) th2max =  180d0
      If(th1min.ge.th1max) ifail = 1
      If(th2min.ge.th2max) ifail = 1
*
      Write(6,1000) imodel,iapprx,rs,W,Wmin,Wmax,t2user,t2umin,t2umax,
     &     m,th1min,th1max,th2min,th2max,E1min,E1max,E2min,E2max,
     &     1d0/alem,ndim
*
      If(imodel.ge.100) then
         sigR = (2d0*arJ(iJP)+1d0) * nbarn * 8d0*Pi**2*Rwidth/Rmass 
         Write(6,1010) Rname,Rmass*1d3,Rwidth*1d6,Pmass*1d3,
     &        Rtotw*1d3,sigR,i1
      Endif
*
      If(ifail.eq.1) then
            write(6,*) 'Cuts leave no phase space'
            write(6,*) 'check cuts!'
            write(6,*) 'Programme stops'
            Stop
      Endif
*
      If(ivegas.eq.1) then
         Write(6,1001) acc,nfcall,itmx
*
         CALL VEGAS (ggLuF,cross,error,chi2a)
         Sigma = dsqrt(dble(nfcall)) * error
         effic = Fmax/dmax1(1d-20,cross)
      Else
         Write(6,1002) npts
*
         SumFi  = 0d0
         SumFsq = 0d0
         Do 100 i = 1 , npts
            Call RanLux(rvec,ndim)
            Do 50 k = 1 , ndim
               x(k) = rvec(k)
 50         Continue
c AZ            
            x(1)=0.00391740718538654
            x(2)=0.195730316904395
            x(3)=0.211537175868401
            x(4)=0.515781057847979
            x(5)=0.984970222850389

            y = ggLuF(x)
            SumFi  = SumFi  + y
            SumFsq = SumFsq + y*y
 100     Continue
*     
         pts   = dble(npts)
         avF   = SumFi/pts
         cross = avF
         avFsq = SumFsq/pts
         Sigma = dsqrt(avFsq - avF*avF)
         error = Sigma/dsqrt(pts)
         effic = Fmax/dmax1(1d-20,avF)
      Endif
*
      Call Timex(time2)
*
      If(iapprx.eq.-1) then
         Write(6,256) cross
      Elseif(iapprx.eq.1) then
         Write(6,257) cross
      Else
         If((imodel.ge.1) .and. (imodel.le.4)) then
            Write(6,258) cross
         Else
            If(imodel.ge.100) then
               Write(6,255) cross
            Else
               Write(6,259) cross
            Endif
         Endif
      Endif
      Write(6,260) error,Sigma,time2-time1,Fmax,Fmin,
     &     effic,nzero
*
* Print efficiency
*
      Write(6,270)
     &   it1,iD1,iD3,iD5,itX,iph,ip1,ip2,ia1,ia2,ia3,ia4,ie1,ie2,ipt,is
*
 1000 Format(//,3x,'Gamma-Gamma Luminosity program GALUGA',/,
     &     5x,'Version 1.1, August 12, 1997',/,
     &     5x,'Author: G.A. Schuler, CERN-TH',//,
     &     3x,'Parameters:',/,
     &     8x,'imodel:             = ',I12,/,
     &     8x,'iapprx:             = ',I12,/,
     &     8x,'cm energy sqrt(s)   = ',E12.5,/,
     &     8x,'hadronic energy W   = ',E12.5,/,
     &     8x,'minimum, maximum W  = ',2E12.5,/,
     &     8x,'user-fixed t2       = ',E12.5,/,
     &     8x,'minimum, maximum t2 = ',2E12.5,/,
     &     8x,'m                   = ',E12.5,/,
     &     8x,'th1min/max (degree) = ',2E12.5,/,
     &     8x,'th2min/max (degree) = ',2E12.5,/,
     &     8x,'E1min/max           = ',2E12.5,/,
     &     8x,'E2min/max           = ',2E12.5,/,
     &     8x,'1/alphaem           = ',E12.5,/,
     &     8x,'dimension           = ',I12,//)
 1010 Format(/,3X,'Parameters for Resonance production',/,
     &3X,'Resonance name                 = ',A,/,
     &3X,'Resonance Mass   [MeV]         = ',F15.6,/,
     &3X,'Two-photon width [keV]         = ',F15.6,/,
     &3X,'Resonance pole mass [MeV]      = ',F15.6,/,
     &3X,'Total width [MeV]              = ',F15.6,/,
     &3X,'Real-photon cross section [nb] = ',F15.6,/,
     &3X,'QPM (1) or simple VMD (2): i1  = ',I15,/)
 1001 Format(/,3x,'VEGAS integration requested; parameters are:',/,
     &     8x,'accuracy            = ',E12.5,/,
     &     8x,'points/it           = ',I12,/,
     &     8x,'iterations          = ',I12,/)
 1002 Format(/,3x,'Simple integration requested; parameter:',/,
     &     8x,'number of points    = ',I12,//)
 256  Format(//,'Result of integration',//,
     &3x,'Total e+e- cross section [nb] = ',E15.10,/)
 257  Format(//,'Result of integration',//,
     &3x,'Differential e+e- cross section ',
     &'dsigma/dtau dt2 [nb/GeV^2] = ',E12.5,/)
 258  Format(//,'Result of integration',//,
     &3x,'Two-photon luminosity L(tau) = ',E12.5,/)
 255  Format(//,'Result of integration',//,
     &3x,'e+e- cross section in the narrow-width approximation',/,
     &7X,'sigma(tau=M^2/s) [nb] = ',E12.5,/)
 259  Format(//,'Result of integration',//,
     &3x,'Differential e+e- cross section ',
     &'dsigma/dtau [nb] = ',E12.5,/)
 260  Format(
     &3x,'Estimated error         = ',E12.5,/,
     &3x,'One-shot error          = ',E12.5,/,
     &3x,'time (in s)             = ',E12.5,/,
     &3x,'Maximum f value         = ',E12.5,/,
     &3x,'Minimum f value         = ',E12.5,/,
     &3x,'Max. f /average f       = ',E12.5,/,
     &3x,'number of f zeros       = ',I8,//)
 270  Format(6x,'it1',6x,'iD1',6x,'iD3',6x,'iD5',6x,'itX',6x,'iph',
     &6x,'ip1',6x,'ip2',/,8I9,/,
     &6x,'ia1',6x,'ia2',6x,'ia3',6x,'ia4',6x,
     &'ie1',6x,'ie2',6x,'ipt',7x,'is',/,8I9,//)
*
      Return
      End
*
*    
      double precision function lambda(x,y,z)
      double precision x,y,z
      lambda = (x - y - z)**2  - 4d0*y*z
      return
      end
*       
      Double Precision Function ggLuF(xar,wgt)
      Implicit None
      Double Precision xar(10),F,yar,Wmin,Wmax,t2user,t2umin,t2umax,wgt,
     &Fmax,Fmin,Weight,cross,error
      Integer ivegas,iwaght,iapprx,npts,nzero,ntrial
      Integer 
     & it1,iD1,iD3,iD5,itX,iph,ip1,ip2,ia1,ia2,ia3,ia4,ie1,ie2,ipt,is
      Double Precision s,rs,W,m,Pi,alem
      Double Precision 
     &     th1min,th1max,E1min,E1max,th2min,th2max,E2min,E2max
      Double Precision mntum
      Integer i,imax
      Double Precision lambda,x,y,z,m2,W2,Wm,Wm2,beta2,beta,tcrit,
     &     theta,help,s1u,s1l,s1hat,t2hat,s2u,s2l,s2hat,t1hat,
     &     t2,t2max,t2min,t2upp,t2low,dt2,y2,Q,a1,b1,c1,Del1,
     &     t1,t1max,t1min,t1upp,t1low,dt1,y1,twoKW,nu,nupKW,
     &     s1,s1max,s1min,ds1,Xs1,ffa,ffb,ffc,ffdel,
     &     s2,s2max,s2min,ds2,Delta4
      Double Precision dPS,ggLint,
     &     ct1min,ct1max,ct2min,ct2max
      Double Precision E1,E2,EX,P1,P2,PX,cth1,cth2,cthX,
     &     s2mm,s2pp,D1,sth1,dsth1,th1,
     &     s1mm,s1pp,D3,sth2,dsth2,th2,
     &     D6,D5,sthX,dsthX,thX
      Double Precision cosphi,sinphi,phi,pht,
     &     sphi1,phi1,cos1,
     &     sphi2,phi2,cos2
      Double Precision theta1,theta2,x1,x2,u1,u2
      Double Precision phtn,cospht,ww1,ww2,ww3,ww4,ww5,m4,W4,D2,D4
      Double Precision W2s,W2c,Bb1,Bb2,B2app,cpt,spt,gamma1,gamma2,
     &     testphi,sinpht,cpht,spht,m6,B1app
      Double Precision tau,dtau,taumin,dW2
      Double Precision Rmass,Rwidth,Pmass,Rtotw
      Double Precision W2min,W2max
      Integer imodel,iJP,iq,i1
      Integer Ncall
*
      Common /ggLprm/ s,rs,W,m,Pi,alem
      Common /ggLcut/th1min,th1max,E1min,E1max,th2min,th2max,E2min,E2max
      Common /ggLapp/Wmin,Wmax,t2user,t2umin,t2umax,iapprx,ivegas,iwaght
      Common /ggLerr/ 
     &   it1,iD1,iD3,iD5,itX,iph,ip1,ip2,ia1,ia2,ia3,ia4,ie1,ie2,ipt,is
      Common /ggLvec/ mntum(9,5)
      Common /ggLvar/ yar(10),
     &     t2,t1,s1,s2,E1,E2,EX,P1,P2,PX,th1,th2,thX,phi1,phi2,phi,pht
      Common /ggLuno/ cross,error,Fmax,Fmin,Weight,npts,nzero,ntrial
      Common /phitst/ phtn
      Common /ggLres/ Rmass,Rwidth,Pmass,Rtotw,iJP,iq,i1
      Common /ggLmod/ imodel
*
      double precision mPion /0.1349763d0/ !pion mass
      double precision mPich /0.1395702d0/  !pion+- mass
      double precision mKaon /0.493677d0/   ! kaon+- mass
      Integer imodpi
      Common /ggLmodpi/ imodpi
*
      Data imax/0/
      Save imax
*
      Data Ncall/0/
      Save Ncall
*     
c +AZ      
cc      print *,'XAR',xar
c -AZ
c      lambda(x,y,z) = (x - y - z)**2  - 4d0*y*z
*
      is = is + 1
*
      If(iapprx.eq.-1) then     ! Integrate over W
         If(imodel.ge.100) then ! Breit-Wigner mapping for resonances
            W2min  = Wmin*Wmin
            W2max  = Wmax*Wmax
            taumin = W2min/s
            dtau   =   datan( (W2min-Rmass**2)/Rtotw/Rmass )
            dW2    = ( datan( (W2max-Rmass**2)/Rtotw/Rmass )
     &                   - dtau ) / Pi
            W2     = Rmass**2 + Rmass*Rtotw*dtan(Pi*dW2*xar(5)+dtau )
            W      = dsqrt(W2)
            tau    = W2/s
            dtau   = dW2  ! Note factor 1/s included in cross sections
            yar(5) = xar(5)
         Else                   ! logarithmic mapping
*
            taumin = Wmin*Wmin/s
            dtau   = 2.d0*dlog(Wmax/Wmin)
            tau    = taumin*dexp(dtau*xar(5))
            dtau   = dtau * tau
            W2     = tau * s
            W      = dsqrt(W2)
            yar(5) = xar(5)
         Endif
      Endif
*
      ct1min = dcos(th1min*Pi/180d0)
      ct1max = dcos(th1max*Pi/180d0)
      ct2min = dcos(th2min*Pi/180d0)
      ct2max = dcos(th2max*Pi/180d0)
      m2    = m*m
      W2    = W*W
      Wm    = W + 2d0*m
      Wm2   = Wm*Wm
      beta2 = 1d0 - 4d0*m2/s
      beta  = dsqrt( beta2 )
      tcrit = 2d0*m2 - m*rs
*
      do 20 i=1,4
         If(iapprx.eq.1) then
            If(i.eq.4) then
               yar(1) = 1d0
            Else
               yar(i+1) = xar(i)
            Endif
         Else
            yar(i) = xar(i)
         Endif
 20   Continue
*
*-------------------------------------------------------------
*
      If(iapprx.ge.2) then
         beta  = 1d0
         beta2 = 1d0
         If ( (s-2d0*rs*E2max)*(s-2d0*rs*E1min) .gt. s*W2 ) then
            s1min = s - 2d0*rs*E2max
         Else
            s1min = s*W2/(s - 2d0*rs*E1min)
         Endif
         If ( (s-2d0*rs*E2min)*(s-2d0*rs*E1max) .lt. s*W2 ) then
            s1max = s - 2d0*rs*E2min
         Else
            s1max = s*W2/(s - 2d0*rs*E1max)
         Endif
         ds1   = dlog(s1max/s1min)
         s1    = s1min*dexp(ds1*yar(1))
*     
         s2    = W2*s/s1
         ds2   = 1d0
*     
         x1    = s2/s
         x2    = s1/s
*     
         help  = - m2*x2**2/(1d0-x2)
         t2min = help - (1d0-x2)*s*(dsin(th2max*Pi/360d0))**2          
         t2max = help - (1d0-x2)*s*(dsin(th2min*Pi/360d0))**2          
         If(t2min.lt.-(s-s1)) t2min = -(s-s1)
         If(t2max.lt.t2min) then
            t2max = t2min
            F = 0d0
            Goto 9876
         Endif
         dt2   = dlog(t2max/t2min)
         t2    = t2min*dexp(dt2*yar(2))
*     
         help  = - m2*x1**2/(1d0-x1)
         t1min = help - (1d0-x1)*s*(dsin(th1max*Pi/360d0))**2          
         t1max = help - (1d0-x1)*s*(dsin(th1min*Pi/360d0))**2          
         If(t1min.lt.-(s-s2)) t1min = -(s-s2)
         If(s1.ge.s2) then 
            t1low = t2 - s1 + s2
            t1upp = t2
         Else
            t1low = t2
            t1upp = t2 - s1 + s2
         Endif
         If(iapprx.eq.2) then
            t1min = dmax1(t1min,t1low)
            t1max = dmin1(t1max,t1upp)
         Endif
         If(t1max.lt.t1min) then
            t1max = t1min
            F = 0d0
            Goto 9876
         Endif
         dt1   = dlog(t1max/t1min)
         t1    = t1min*dexp(dt1*yar(3))
*     
         E1     = (s - s2)/(2d0*rs)
         E2     = (s - s1)/(2d0*rs)
         EX     = (s1 + s2)/(2d0*rs)
         p1     = E1
         p2     = E2
         pX     = dsqrt(EX**2 - W2)
         cth1   = 1d0 + 2d0*t1/(s - s2)
         cth2   = 1d0 + 2d0*t2/(s - s1)
         cthX   = - (s2-s1+2d0*(t2-t1))/(2d0*rs*pX)
         D1 = - 0.25d0*t1*s*(s - s2 + t1)
         D3 = - 0.25d0*t2*s*(s - s1 + t2)
         D6 = s*(s1*s2 - s*W2 + t1*(s-s1) + t2*(s-s2)+2d0*t1*t2)/8d0
         D5 = dabs(s*(t2-t1)*(s1 -s2 + t1 - t2)/4d0)
*
         th1    = 2d0*dAsin(dsqrt(-t1/(s-s2)))
         th2    = 2d0*dAsin(dsqrt(-t2/(s-s2)))
         If(iapprx.eq.2) then
            If(s1.ge.s2) then
               thx =      2d0*dasin(dsqrt((t2-t1)/(s1-s2)))
            Else
               thx = Pi - 2d0*dasin(dsqrt((t1-t2)/(s2-s1)))
            Endif
         Else
            thx = dacos(dmax1(-1d0,dmin1(1d0,cthX)))
         Endif
*
         th1 = th1*180d0/Pi
         th2 = th2*180d0/Pi
         thx = thx*180d0/Pi
*
         pht    = yar(4)*Pi
         phi    = pht
*
         F     = ds1*dt2*dt1 * alem**2/(8d0*Pi**2*s) * W2/2d0
*
      Else
*     
*---------------------------------------------------------------
*     
*     calculate t2max
*     
         s1u   = (rs - m)**2
         s1l   = (W  + m)**2
         s1u   = dmin1( s1u, m2 + s*(1d0 - 2d0*E2min/rs) )
         s1l   = dmax1( s1l, m2 + s*(1d0 - 2d0*E2max/rs) )
         If(s1l.ge.s1u) then 
            it1 = it1 + 1
            F = 0d0
            Goto 9876
         Endif
*
         theta = th2min*Pi/180d0
         help  = 4d0*m2/s + beta2*dsin(theta)**2
         s1hat = m2 + s*beta2*dsin(theta)**2/
     #            (help*(1d0 + 2d0*m/dsqrt(s*help)))
         t2hat = 2.D0*m2-m*rs*dsqrt(4.D0*m2/S+beta2*dsin(theta)**2)
         If( s1hat.ge.s1u ) then
            s1    = s1u
            If(E2min.le.m) then
               help = 0d0
               t2max = tcrit
            Else
               help  = lambda(s,s1,m2)
               t2max = (3d0*m2-s+s1-dcos(theta)*beta*dsqrt(help))/2d0
            Endif
            t2max = (m2*(s1-m2)**2/s+beta2*help*dsin(theta)**2/4d0)
     #            / t2max
         Elseif( s1hat.le.s1l ) then
            s1    = s1l
            help  = lambda(s,s1,m2)
            t2max = (3d0*m2-s+s1-dcos(theta)*beta*dsqrt(help))/2d0
            t2max = (m2*(s1-m2)**2/s+beta2*help*dsin(theta)**2/4d0)
     #            / t2max
         Else
            t2max = t2hat
         Endif
*
* calculate t2min
*     
         theta = th2max*Pi/180d0
         s1    = s1l
         help  = lambda(s,s1,m2)
         t2min = (3d0*m2-s+s1+dcos(theta)*beta*dsqrt(help))/2d0
         s1    = s1u
         If(E2min.le.m) then
            help = 0d0
            t2low = tcrit
         Else
            help  = lambda(s,s1,m2)
            t2low = (3d0*m2-s+s1+dcos(theta)*beta*dsqrt(help))/2d0
         Endif
         t2min = dmin1(t2min,t2low)
*
         t2min = dmax1(t2min,t2umin)
         t2max = dmin1(t2max,t2umax)
*
*         If(t2min.ge.t2max) then
         If((t2min.ge.t2max) .and. (iapprx.ne.-1)) then
            write(6,*) 'Cuts leave no phase space'
            write(6,*) 'check cuts!'
            write(6,*) 'Programme stops'
            Stop
         Endif
*
         If(iapprx.eq.1) then
            dt2 = - 1d0
            t2  = t2user
            If(t2.ge.t2max .or. t2.le.t2min) then
               Write(6,111) 
               stop
            Endif
         Else
*logarithmic mapping
            dt2   = dlog(t2max/t2min)
            t2    = t2min*dexp(yar(1)*dt2)
         Endif
 111     Format(/,1x,'Warning from ggLuF:',
     &        1x,'user-t2 is outside user cuts ',
     &        1x,'programme stops')
*     
         y2    = dsqrt(1d0 - 4d0*m2/t2)
*
         Q     = 4d0*(s+t2-4d0*m2)/(1d0+beta*y2) - t2 - W2
         a1    = 2d0*(Q+t2+2d0*m2+W2)
         b1    = Q**2-W2**2+2d0*W2*t2-t2**2-8d0*m2*t2-8d0*m2*W2
         c1    = 4d0*m2*(W2-t2)**2
         Del1  = (Q+t2-W2+4d0*m*W)*(Q+t2-W2-4d0*m*W)*
     &        (Q**2-2d0*Q*t2+2d0*Q*W2+t2**2+W2**2-16d0*m2*t2-2d0*W2*t2)
*
*---------------------------------------------------------------
*
* calculate t1max due to th1min and E1 cuts
*
         s2u   = (rs - m)**2
         s2l   = (W  + m)**2
         s2u   = dmin1( s2u, m2 + s*(1d0 - 2d0*E1min/rs) )
         s2l   = dmax1( s2l, m2 + s*(1d0 - 2d0*E1max/rs) )
         theta = th1min*Pi/180d0
         help  = 4d0*m2/s + beta2*dsin(theta)**2
         s2hat = m2 + s*beta2*dsin(theta)**2/
     #            (help*(1d0 + 2d0*m/dsqrt(s*help)))
         t1hat = 2.D0*m2-m*rs*dsqrt(4.D0*m2/S+beta2*dsin(theta)**2)
         If( s2hat.ge.s2u ) then
            s2    = s2u
            If(E1min.le.m) then
               help = 0d0
               t1max = tcrit
            Else
               help  = lambda(s,s2,m2)
               t1max = (3d0*m2-s+s2-dcos(theta)*beta*dsqrt(help))/2d0
            Endif
            t1max = (m2*(s2-m2)**2/s+beta2*help*dsin(theta)**2/4d0)
     #            / t1max
         Elseif( s2hat.le.s2l ) then
            s2    = s2l
            help  = lambda(s,s2,m2)
            t1max = (3d0*m2-s+s2-dcos(theta)*beta*dsqrt(help))/2d0
            t1max = (m2*(s2-m2)**2/s+beta2*help*dsin(theta)**2/4d0)
     #            / t1max
         Else
            t1max = t1hat
         Endif
         t1upp = t1max
*
* calculate t1min due to th1max and E1 cuts
*
         theta = th1max*Pi/180d0
         s2    = s2l
         help  = lambda(s,s2,m2)
         t1min = (3d0*m2-s+s2+dcos(theta)*beta*dsqrt(help))/2d0
         s2    = s2u
         If(E1min.le.m) then
            help = 0d0
            t1low = tcrit
         Else
            help  = lambda(s,s2,m2)
            t1low = (3d0*m2-s+s2+dcos(theta)*beta*dsqrt(help))/2d0
         Endif
         t1min = dmin1(t1min,t1low)
         t1low = t1min
*
* kinematical limits
*
         dt1   = dsqrt(Del1)/a1
         t1min = - (b1/a1 + dt1)/2d0
         t1max = c1/(a1*t1min)
*angle cut
         t1min = dmax1(t1min,t1low)
         t1max = dmin1(t1max,t1upp)
         If(t1min.ge.t1max) then
            it1 = it1 + 1
            F = 0d0
            Goto 9876
         Endif
*
*logarithmic mapping
         dt1   = dlog(t1max/t1min)
         t1    = t1min*dexp(yar(2)*dt1)
*
         y1    = dsqrt(1d0 - 4d0*m2/t1)
*     
         twoKW = dsqrt(lambda(W2,t1,t2))
         nu    = (W2 - t1 - t2)/2d0
         nupKW = nu + twoKW/2d0
*
         s1min = m2 + (W2-t1+t2+y1*twoKW)/2d0
         s1max = m2 + 2d0*(s+t2-4d0*m2)/(1d0+beta*y2)
         ds1   = dlog(s1max/s1min)
         s1    = s1min*dexp(ds1*yar(3))
         ds1   = ds1 * s1 / dsqrt(lambda(s1,t2,m2))
* better
         ds1   = dlog( s*(1d0+beta)**2/(nupKW*(1d0+y1)*(1d0+y2)) )
         Xs1   = nupKW*(1d0+y1)*dexp(ds1*yar(3))
         s1    = Xs1/2d0 + m2 + t2 + 2d0*m2*t2/Xs1
*
         ffa = -2.D0*m**2*s1-2.D0*m**2*t2+m**4+s1**2-2.D0*t2*s1+t2**2
         ffb = -2.D0*m**6+4.D0*m**4*s1-4.D0*m**4*w**2+2.D0*S*t2*w**2-2.
     #D0*m**2*s1**2+2.D0*m**2*S*w**2+2.D0*t1*S*s1-2.D0*m**2*t2*S+8.D0*t2
     #*m**4-2.D0*m**2*t2**2-2.D0*t2*m**2*t1+4.D0*m**2*s1*w**2-2.D0*t2**2
     #*S+2.D0*t1*m**4+2.D0*t1*t2*S+2.D0*S*t2*s1+2.D0*t1*t2*s1-2.D0*S*s1*
     #w**2-2.D0*t1*s1**2-2.D0*S*m**2*t1
         ffc = -2.D0*t1*t2*S**2-2.D0*S*m**2*t2**2-2.D0*S*m**4*w**2-4.D0
     #*m**4*s1*w**2-2.D0*t1*S**2*w**2-2.D0*t1**2*m**2*s1-2.D0*S*m**2*t1*
     #*2-2.D0*m**2*t1*s1**2+2.D0*m**4*t2*S+2.D0*t1*t2*m**4+2.D0*m**4*t2*
     #s1+2.D0*S*t1*m**4-2.D0*S**2*t2*w**2-2.D0*S*t1**2*s1+8.D0*m**4*t1*s
     #1-4.D0*S*m**2*w**4+m**4*s1**2+m**4*t1**2-6.D0*m**6*t1-2.D0*m**6*s1
     #+4.D0*m**6*w**2+m**4*t2**2-6.D0*m**6*t2+S**2*w**4+t1**2*S**2+t1**2
     #*s1**2+t2**2*S**2+m**8-2.D0*m**2*S*t2*s1+6.D0*m**2*S*t2*w**2+4.D0*
     #m**2*t1*t2*S-2.D0*m**2*t1*t2*s1+2.D0*S*m**2*s1*w**2+2.D0*S*t1*t2*s
     #1-4.D0*S*t1*t2*w**2-2.D0*S*t1*m**2*s1+6.D0*S*t1*m**2*w**2+2.D0*S*t
     #1*s1*w**2
         ffdel = 16.D0*(m**2*t2**2+m**2*w**4-t1*s1*w**2-2.D0*m**2*t2*w*
     #*2+t1*t2*w**2-t1*m**2*w**2-2.D0*t1*m**2*s1-t1*t2*s1+t1**2*s1+t1*s1
     #**2-t2*m**2*t1+t1*m**4)*(m**2*s1**2-2.D0*m**4*s1-S*t2*s1-3.D0*m**2
     #*t2*S+t2*S**2+m**6+t2**2*S)
*
         If(ffdel.lt.0d0) then
            F = 0d0
            Goto 9876
         Endif
*
         s2max = (-ffb + dsqrt(ffdel))/(2d0*ffa)
         s2min = ffc/(ffa*s2max)
*     
         s2 = (-ffb + dsqrt(ffdel)*dsin((yar(4)-0.5d0)*Pi))
     &        /(2d0*ffa)
         ds2 = 1d0
*
*  Note: Delta4 = 16 * \Delta_4, where \Delta_4 is the Gram determinant
*
         Delta4 = -ffdel*dsin(yar(4)*Pi)**2/(4d0*ffa)
*
*logarithmic mapping
         dPS = dt1 * dt2 * ds1 * ds2
         F   = dPS * alem**2/(8d0*Pi**2*beta2*s) * twoKW/2d0
*
         E1 = (s + m2 - s2)/(2d0*rs)
         E2 = (s + m2 - s1)/(2d0*rs)
         P1 = dsqrt(lambda(s,s2,m2))
         P2 = dsqrt(lambda(s,s1,m2))
*
         cth1 = (s- s2+ 2d0*t1 - 3d0*m2)/(beta*P1)
         cth2 = (s- s1+ 2d0*t2 - 3d0*m2)/(beta*P2)
*
         P1 = P1/(2d0*rs)
         P2 = P2/(2d0*rs)
*
         EX   = (s1 + s2 - 2d0*m2)/(2d0*rs)
         pX   = dsqrt(EX**2 - W2)
         cthX = - (s2-s1+2d0*(t2-t1))/(beta*2d0*rs*pX)
*
         s2mm = (s*t1+2d0*m2**2-beta*s*dsqrt(lambda(t1,m2,m2)))/(2d0*m2)
         s2pp = (m2**2+s*t1*(s+t1-3d0*m2)/m2)/s2mm
         D1   = - 0.25d0*m2*(s2pp-s2)*(s2mm-s2)
*
         s1mm = (s*t2+2d0*m2**2-beta*s*dsqrt(lambda(t2,m2,m2)))/(2d0*m2)
         s1pp = (m2**2+s*t2*(s+t2-3d0*m2)/m2)/s1mm
         D3   = - 0.25d0*m2*(s1pp-s1)*(s1mm-s1)
*
         D6  = (s*(-(s-4d0*m2)*(W2-t1-t2)+(s1-t2-m2)*(s2-t1-m2)+t1*t2)
     &        - 2d0*m2*(s1-m2)*(s2-m2))/8d0
         D5   = D1 + D3 + 2d0*D6
*
*     azimuthal angles
*     
         If(D1.le.0d0 .or. D3.le.0d0) then
            help = 1d-12
            sinphi = 0d0
            cosphi = 1d0
         Else
            help   = dsqrt(D1*D3)
            cosphi = D6/help
            sinphi = s*beta*dsqrt(-Delta4)/(8d0*help)
            if(sinphi.lt.0d0 .or. sinphi.gt.1d0) then
               iph = iph + 1
               F = 0d0
               sinphi = 1d0
               goto 9876
            Endif
         Endif
         phi = dasin(sinphi)
         If(cosphi.lt.0d0) phi = Pi - phi
*
c         sphi1 = dabs(0.5d0*dsqrt(-Delta4)/(s*beta*PX*sthX*P1*sth1))
c         sphi2 = dabs(0.5d0*dsqrt(-Delta4)/(s*beta*PX*sthX*P2*sth2))
         If(D1.le.0d0 .or. D5.le.0d0) then
            sphi1 = 0d0
            cos1  = 1d0
         Else
            sphi1 = beta*s*dsqrt(-Delta4/(D1*D5))/8d0
            cos1 = (D1+D6)/dsqrt(D1*D5)
        Endif
         If(D3.le.0d0 .or. D5.le.0d0) then
            sphi2 = 0d0
            cos2  = 1d0
         Else
            sphi2 = beta*s*dsqrt(-Delta4/(D3*D5))/8d0
            cos2  = (D3+D6)/dsqrt(D3*D5) 
         Endif
         If(sphi1.lt.0d0 .or. sphi1.gt.1d0) then
            ip1 = ip1 + 1
            sphi1 = 1d0
            F = 0d0
            Goto 9876
         Endif
         If(sphi2.lt.0d0 .or. sphi2.gt.1d0) then
            ip2 = ip2 + 1
            sphi2 = 1d0
            F = 0d0
            Goto 9876
         Endif
         phi1  = dasin(sphi1)
         phi2  = dasin(sphi2)
*
         If(cos1.lt.0d0) phi1 = Pi - phi1
         If(cos2.lt.0d0) phi2 = Pi - phi2
*
         u1  = s2 - m2 - t1
         u2  = s1 - m2 - t2
         pht = ( 4d0*((u2-nu)/twoKW)**2 - y1**2 )
     &       * ( 4d0*((u1-nu)/twoKW)**2 - y2**2 )
     &       *  t1*t2
         If(pht.lt.0d0) then
C            Write(6,*) 'warning root pht '
            ipt = ipt + 1
            pht = 0d0
         Endif
         pht  = dsqrt(pht)
         cpht = (-2d0*s+u1+u2-nu+4d0*m2 
     &            + 4d0*nu*(u2-nu)*(u1-nu)/twoKW**2)/pht
         If(cpht.gt.1d0) then
c           Write(6,*) 'warning cos pht > 1 '
            ipt  = ipt + 1
            cpht = 1d0
         Endif
         If(cpht.lt.-1d0) then
c           Write(6,*) 'warning cos pht < -1 '
            ipt  = ipt + 1
            cpht = - 1d0
         Endif
         pht  = dacos(cpht)
         spht = dsin(pht)
         If(spht.gt.10d0) then
         m4     = m2*m2
         m6     = m4*m2
         W2s    = W2*W2
         W2c    = W2*W2s
         gamma1 = m2/t1
         gamma2 = m2/t2
      ww1 = 2.E0*W2*(s1*s2-S*W2)
      ww2 = 2.E0*t1*(s1*t1-S*t1-s1*s2-W2*s1+2.E0*S*W2)
      ww3 = 2.E0*t2*(-t2*S+t2*s2+2.E0*S*W2-s2*W2-s1*s2)
      ww4 = 2.E0*t1*t2*(2.E0*S-s1+2.E0*W2-s2)
      ww5 = 2.E0*m2*(t2*s2-W2*s1-2.E0*t1*t2-m2*t2+t2**2
     &     +W2*m2-m2*t1+2.E0*W2**2+t1**2-3.E0*W2*t1+s1*t1
     &     -3.E0*W2*t2+t2*s1-s2*W2+t1*s2)
      D2 = 0.5E0*t2*m2*s2+0.25E0*t2*m2*t1-0.25E0*t2*m4+0.25E0*m2
     #*t2*W2-0.25E0*m2*t1**2-0.25E0*t2*W2*t1+0.25E0*t2*s2*W2+0.5
     #E0*m2*W2*t1-0.25E0*m2*W2**2-0.25E0*t2*s2**2-0.25E0*t2**2*s2+0
     #.25E0*t2*t1*s2
      D4 = 0.5E0*t1*m2*s1+0.25E0*t2*m2*t1-0.25E0*m4*t1+0.25E0*m2
     #*W2*t1-0.25E0*m2*t2**2-0.25E0*t2*W2*t1+0.25E0*t1*s1*W2+0.5
     #E0*m2*t2*W2-0.25E0*m2*W2**2-0.25E0*t1*s1**2-0.25E0*t1**2*s1+0
     #.25E0*t1*t2*s1
      If(D2.lt.0d0) then
         Write(6,*) 'D2 < 0 ',D2
         cospht = 1d0
      Elseif(D4.lt.0d0) then
         Write(6,*) 'D4 < 0 ',D4
         cospht = 1d0
      Else
         cospht = (ww1+ww2+ww3+ww4+ww5)/(16d0*dsqrt(D2*D4))
      Endif
      If(dabs(cospht).gt.1d0) then
         Write(6,*) 'warning on cospht ',cospht
         cospht=1d0
      Endif
      phtn   = dacos(cospht)
      sinpht = dsin(phtn)
      If(dabs(1d0-phtn/pht).gt.1d-4) write(6,*) 'pht: ',1d0-phtn/pht
*************
         Bb1 = 
     #(W2s-2.D0*t1*W2-2.D0*t2*W2+t1**2-2.D0*t1*t2+t2**2)*(m2
     #*s2**2-2.D0*m4*s2-S*t1*s2-3.D0*S*m2*t1+m6+t1*S**2+t1**2*S)*(
     #m2*s1**2-2.D0*m4*s1-S*t2*s1-3.D0*m2*t2*S+m6+t2*S**2+t2**2*
     #S)/(t2*m4+m2*W2s+m2*t1**2+t2*s2**2+t2**2*s2-t2*m2*t1+t2*
     #W2*t1-2.D0*m2*W2*t1-m2*t2*W2-2.D0*t2*m2*s2-t2*s2*W2-t2
     #*t1*s2)/(-2.D0*t1*m2*s1-t2*m2*t1+m4*t1-m2*W2*t1+m2*t2
     #**2+t2*W2*t1-t1*s1*W2-2.D0*m2*t2*W2+m2*W2s+t1*s1**2+t1*
     #*2*s1-t1*t2*s1)/S**2
         B1app = 
     & W2s*(s2**2*gamma1+S**2-S*s2)*(s1**2*gamma2+S**2-S*s1)/(W2s*
     #gamma2+s2**2-s2*W2)/(W2s*gamma1+s1**2-W2*s1)/S**2
         Bb2 = 
     & 2.D0*dsqrt((s2**2*gamma1+S**2-S*s2)*(s1**2*gamma2+S**2-S*s1)/
     #gamma1/gamma2)*(W2*S**3-S**2*s2*W2-S**2*W2*s1+S**2*s2*s1
     #-S*gamma1*gamma2*W2c+S*W2s*s2*gamma1+S*W2s*s1*gamma2
     #-gamma1*gamma2*
     #s1*s2*W2s)/S**3/(W2s*gamma2+s2**2-s2*W2)/(W2s*gamma1+s1**2
     #-W2*s1)
         Bb2 = Bb2*m2
         B2app = B2app*m2*dsqrt(gamma1*gamma2)
         cpt = cosphi*dsqrt(1d0+Bb2*sinphi**2/cosphi)
         Bb2 = 1d0-Bb1
         cpt = cosphi*dsqrt(1d0+Bb2*sinphi**2/cosphi**2)
         phtn = dacos(cpt)
         testphi = (spht/sinphi)**2/Bb1
      If(dabs(1d0-phtn/pht).gt.1d-4) then
         write(6,*) 'phtn/pht: ',cpt/cosphi
         write(6,*) 'sinphi: ',testphi
      Endif
*      
      Endif
      B2app = (2.D0*S-s1-s2) * dsqrt(t1*t2)
     &     /( (W2-t1-t2) * dsqrt((S-s1)*(S-s2)) )
      cpt = cosphi + B2app*sinphi**2
      If(cpt.gt. 1d0) cpt= 1d0
      If(cpt.lt.-1d0) cpt=-1d0
      phtn = dacos(cpt)
*-----------------------------------------------------------
*     
      If(D1.le.0d0) then
         sth1  = 0d0
         dsth1 = 0d0
         th1   = 0d0
      Else
         sth1 = 2d0*dsqrt(D1)/(s*beta*P1)
         If(sth1.gt.1d0) then
            iD1 = iD1 + 1
            F   = 0D0
            Goto 9876
         Endif
         dsth1 = sth1
         th1 = dasin(sth1)
         If(cth1.lt.0d0) then
            th1 = Pi - th1
            sth1 = - sth1
         Endif
      Endif
      th1  = th1*180d0/Pi
*     
      If(D3.le.0d0) then
         sth2   = 0d0
         dsth2  = 0d0
         th2    = 0d0
      Else
         sth2 = 2d0*dsqrt(D3)/(s*beta*P2)
         If(sth2.gt.1d0) then
            iD3 = iD3 + 1
            F   = 0D0
            Goto 9876
         Endif
         dsth2 = sth2
         th2 = dasin(sth2)
         If(cth2.lt.0d0) then
            th2 = Pi - th2
            sth2 = - sth2
         Endif
      Endif
      th2  = th2*180d0/Pi
*     
      If(D5.lt.0d0) then
         iD5 = iD5 + 1
         F = 0d0
         goto 9876
      Endif
      sthX = dsqrt(D5)/(s*0.5d0*beta*pX)
      If(sthX.gt.1d0) then
         itX = itX + 1
         sthX = 1d0
         F = 0d0
         goto 9876
      Endif
      dsthX = sthX
      thX = dasin(sthX)
      If(cthX.lt.0d0) then 
         thX = Pi - thX
         sthx = - sthx
      Endif
      thX  = thX*180d0/Pi
      Endif
*
*angle cut
      If (th1.gt.th1max) then
         ia1 = ia1 + 1
         If(ia1.le.imax) write(6,*) 'cut: th1 max = ',th1
         F = 0d0
         Goto 9876
      Endif
      If (th1.lt.th1min) then
         ia2 = ia2 + 1
         If(ia2.le.imax) write(6,*) 'cut: th1 min = ',th1
         F = 0d0
         Goto 9876
      Endif
      If (th2.gt.th2max) then
         ia3 = ia3 + 1
         If(ia3.le.imax) write(6,*) 'cut: th2 max = ',th2
         F = 0d0
         Goto 9876
      Endif
      If (th2.lt.th2min) then
         ia4 = ia4 + 1
         If(ia4.le.imax) write(6,*) 'cut: th2 min = ',th2
         F = 0d0
         Goto 9876
      Endif
*     energy cut
      If (E1.gt.E1max .or. E1.lt.E1min ) then
         ie1 = ie1 + 1
         If(ie1.le.imax) write(6,*) 'cut: E1 = ',E1
         F = 0d0
         Goto 9876
      Endif
      If (E2.gt.E2max .or. E2.lt.E2min ) then
         ie2 = ie2 + 1
         If(ie2.le.imax) write(6,*) 'cut: E2 = ',E2
         F = 0d0
         Goto 9876
      Endif
*----------------------------------------------------------------
*     check kinematics for pipimodf
      if (imodpi.ne.0.and.imodpi.lt.20.and.
     &     w2.le.4*mpion*mpion) then
         F = 0d0
         Goto 9876
      endif
      if (imodpi.ge.20.and.imodpi.lt.40.and.w2.le.4*mpich**2) then
         F = 0d0
c         print *,"CUT PIPIMODF ",imodpi,1-4*mpich**2/w2
         Goto 9876
      endif
      if (imodpi.ge.40.and.w2.le.4*mkaon**2) then
         F = 0d0
         Goto 9876
      endif

*----------------------------------------------------------------
*    
c      print *,'RESULT',F,s1,s2,t1,t2
      F = F * ggLint(W2,m2,-t1,-t2,s1,s2,pht,s)
*
      If(is.le.imax) then
         Call ggLmom()
         Call ggLprt()
      Endif
*
*
 9876 Continue
*
      If(iapprx.eq.-1) F = F * dtau
*
      ggLuF = F
      Weight = wgt*F
*
      help  = F
      If(help.gt.Fmax) Fmax = help
      If(help.lt.Fmin .and. help.ne.0d0) Fmin = help
      If(help.eq.0d0) nzero = nzero + 1
*
      Return
      End
*
*
      Subroutine ggTwoBodyDecay
*
* calculate 2-body decay of hadronic state 
*
      Implicit None
      Double Precision s,rs,W,m,Pi,alem
      Double Precision mntum
      Double Precision 
     &     X,t2,t1,s1,s2,E1,E2,EX,P1,P2,PX,th1,th2,thX,phi1,phi2,phi,pht
      Integer i,j
      Double Precision beta,help
      Real rvec(10)
      Double Precision vx,vy,qx,qy,ranphi,rancos,ransin

      Common /ggLprm/ s,rs,W,m,Pi,alem
      Common /ggLvec/ mntum(9,5)
      Common /ggLvar/ 
     & X(10),t2,t1,s1,s2,E1,E2,EX,P1,P2,PX,th1,th2,thX,phi1,phi2,phi,pht
*
**********calculation of pi+ pi-, decay of resonances.
      double precision mPion /0.1349763d0/  !pion mass
      double precision mPich /0.1395702d0/  !pion+- mass
      double precision mKaon /0.493677d0/   ! kaon+- mass

c      Double Precision betapi,thetapi,P1star, E1star, P2star, E2star
c     &,theta1star,theta2star,thrad
c      Double Precision Ppipl,Ppim,thetapipl,thetapim,check1,check2
c      Common /pipid/   Ppipl,Ppim,thetapipl,thetapim,check1,check2

      Integer imodpi
      Common /ggLmodpi/ imodpi

c      Double precision PiXZ, massp, ppi1(3), ppi2(3), 
      Double precision check1,check2,chk1,chk2
      Double precision massp,Epart,Ppart,thrad,thetap,
     &Pstar(2,3),Pplab(2,3),Eplab(2),betax,gammax,tmp

      if(imodpi.eq.11) massp = mPion
      if(imodpi.eq.31) massp = mPich
      if(imodpi.eq.41) massp = mKaon

* Random theta from Px axis
      Call RanLux(rvec,1)
      tmp=rvec(1)
      thetap = dacos(2.*tmp-1)
      Call RanLux(rvec,1)
      ranphi = rvec(1)*2.*pi

      Epart = W/2
      Ppart = dsqrt(W**2/4-massp**2)

      Pstar(1,1) = Ppart*dsin(thetap)
      Pstar(1,2) = 0
      Pstar(1,3) = Ppart*dcos(thetap)

      Pstar(2,1) = -Ppart*dsin(thetap)
      Pstar(2,2) = 0
      Pstar(2,3) = -Ppart*dcos(thetap)

      betax = Px/Ex
      gammax = Ex/W

      Pplab(1,1) = Pstar(1,1)*dcos(ranphi)
      Pplab(1,2) = Pstar(1,1)*dsin(ranphi)
      Pplab(1,3) = gammax*Pstar(1,3)+gammax*betax*Epart
      Eplab(1) = gammax*Epart + gammax*betax*Pstar(1,3)

      Pplab(2,1) = Pstar(2,1)*dcos(ranphi)
      Pplab(2,2) = Pstar(2,1)*dsin(ranphi)
      Pplab(2,3) = gammax*Pstar(2,3)+gammax*betax*Epart
      Eplab(2) = gammax*Epart + gammax*betax*Pstar(2,3)

      thrad=thX*Pi/180d0

* outgoing pion+ (or first pi0)
      mntum(8,1) = Pplab(1,1)*dcos(thrad) + Pplab(1,3)*dsin(thrad)
      mntum(8,2) = Pplab(1,2)
      mntum(8,3) = - Pplab(1,1)*dsin(thrad) + Pplab(1,3)*dcos(thrad)
      mntum(8,4) = Eplab(1)
      mntum(8,5) = massp

* outgoing pion- (or second pi0)
      mntum(9,1) = Pplab(2,1)*dcos(thrad) + Pplab(2,3)*dsin(thrad)
      mntum(9,2) = Pplab(2,2)
      mntum(9,3) = - Pplab(2,1)*dsin(thrad) + Pplab(2,3)*dcos(thrad)
      mntum(9,4) = Eplab(2)
      mntum(9,5) = massp

c      do 28 i=8,9
c      Write(6,127) "pions: ",(mntum(i,j),j=1,5)
c 28   continue
***************end***********************************
c127   Format(1x,A,6F12.5)
      Return
      End

      Subroutine ggLmom
*
* calculate 4-momenta
*
      Implicit None
      Double Precision s,rs,W,m,Pi,alem
      Double Precision mntum
      Double Precision 
     &     X,t2,t1,s1,s2,E1,E2,EX,P1,P2,PX,th1,th2,thX,phi1,phi2,phi,pht
      Integer i
      Double Precision beta,help
      Real rvec(10)
      Double Precision vx,vy,qx,qy,ranphi,rancos,ransin
      Integer Ncall
      Data Ncall /0/
      Save Ncall
*
      Common /ggLprm/ s,rs,W,m,Pi,alem
      Common /ggLvec/ mntum(9,5)
      Common /ggLvar/ 
     & X(10),t2,t1,s1,s2,E1,E2,EX,P1,P2,PX,th1,th2,thX,phi1,phi2,phi,pht
*
      Integer imodpi
      Common /ggLmodpi/ imodpi

      beta = dsqrt(1d0 - 4d0*m**2/s)
*
* incoming e+
      mntum(1,1) = 0d0
      mntum(1,2) = 0d0
      mntum(1,3) = - beta*rs/2d0
      mntum(1,4) = rs/2d0
      mntum(1,5) = m
* incoming e-
      mntum(2,1) = 0d0
      mntum(2,2) = 0d0
      mntum(2,3) = beta*rs/2d0
      mntum(2,4) = rs/2d0
      mntum(2,5) = m
* outgoing e+
      help       = Pi/180d0
      mntum(5,1) = - P1*dsin(th1*help)*dcos(phi1)
      mntum(5,2) = - P1*dsin(th1*help)*dsin(phi1)
      mntum(5,3) = - P1*dcos(th1*help)
      mntum(5,4) = E1
      mntum(5,5) = m
* outgoing e-
      mntum(6,1) = - P2*dsin(th2*help)*dcos(phi2)
      mntum(6,2) = P2*dsin(th2*help)*dsin(phi2)
      mntum(6,3) = P2*dcos(th2*help)
      mntum(6,4) = E2
      mntum(6,5) = m
* hadronic system X
      mntum(7,1) = PX*dsin(thX*help)
      mntum(7,2) = 0d0
      mntum(7,3) = PX*dcos(thX*help)
      mntum(7,4) = EX
      mntum(7,5) = W
*
      do 27 i=1,4
         mntum(3,i) = mntum(1,i) - mntum(5,i)
         mntum(4,i) = mntum(2,i) - mntum(6,i)
 27   Continue
      mntum(3,5) = - dsqrt(-t1)
      mntum(4,5) = - dsqrt(-t2)
*
      if(imodpi.eq.11.or.imodpi.eq.31.or.imodpi.eq.41) then
         call ggTwoBodyDecay()
      endif

* Random phi around z axis
      Call RanLux(rvec,1)
      ranphi = rvec(1)*2*pi
      If(Ncall.le.10) then
         Write(6,*) 'random phi = ',ranphi
         Ncall = Ncall+1
      Endif
      rancos = dcos(ranphi)
      ransin = dsin(ranphi)
      do 28 i=3,9
         vx = mntum(i,1)
         vy = mntum(i,2)
         qx =   vx*rancos + vy*ransin
         qy = - vx*ransin + vy*rancos
         mntum(i,1) = qx
         mntum(i,2) = qy
 28   Continue
*
*
      Return
      End
*
*
      Subroutine ggLprt
*
* Print 4-momenta and check momentum sum
*      
      Implicit None
      Double Precision s,rs,W,m,Pi,alem
      Double Precision mntum
      Double Precision Pinx,Piny,Pinz,Einp,Qinx,Qiny,Qinz,Einq,
     &     msin,msout,eps
      Character*5 name(7)
      Integer i,j
      Double Precision help
*
      Common /ggLprm/ s,rs,W,m,Pi,alem
      Common /ggLvec/ mntum(9,5)
*
      Data name /'e+ in','e- in','gam +','gam -','e+out','e-out',
     &'  X  '/
      Data eps /1d-4/
*
      Pinx = 0d0
      Piny = 0d0
      Pinz = 0d0
      Einp = 0d0
      Qinx = 0d0
      Qiny = 0d0
      Qinz = 0d0
      EinQ = 0d0
      Write(6,126)
      do 28 i=1,7
         help=mntum(i,4)**2-mntum(i,3)**2-mntum(i,2)**2-mntum(i,1)**2
         help=dSign(dsqrt(dabs(help)),help)
         Write(6,127) name(i),(mntum(i,j),j=1,5),help
         If(i.lt.3 .or. i.gt.4) then
            If(i.eq.1. .or. i.eq.2) then
               Pinx = Pinx + mntum(i,1)
               Piny = Piny + mntum(i,2)
               Pinz = Pinz + mntum(i,3)
               Einp = Einp + mntum(i,4)
            Else
               Qinx = Qinx + mntum(i,1)
               Qiny = Qiny + mntum(i,2)
               Qinz = Qinz + mntum(i,3)
               Einq = Einq + mntum(i,4)
            Endif
         Endif
 28   Continue
      msin  = dsqrt(Einp**2-Pinx**2-Piny**2-Pinz**2)
      msout = dsqrt(Einq**2-Qinx**2-Qiny**2-Qinz**2)
      Write(6,*)
      Write(6,128) Pinx,Piny,Pinz,Einp,msin
      Write(6,128) Qinx,Qiny,Qinz,EinQ,msout
*
 126  Format(//,1x,'name',11x,'Px',10x,'Py',10x,'Pz',10x,'E',10x,
     &'m',10x,'P',/)
 127  Format(1x,A,6F12.5)
 128  Format(6x,5F12.5)
*
      If(dabs(Pinx).gt.eps) write(6,*) 'Pinx accuracy ',Pinx 
      If(dabs(Piny).gt.eps) write(6,*) 'Piny accuracy ',Piny 
      If(dabs(Pinz).gt.eps) write(6,*) 'Pinz accuracy ',Pinz 
      If(dabs(Einp-rs).gt.eps) write(6,*) 'Einp accuracy ',Einp-rs 
      If(dabs(msin-rs).gt.eps) write(6,*) 'msin accuracy ',msin-rs
      If(dabs(Qinx).gt.eps) write(6,*) 'Qinx accuracy ',Qinx 
      If(dabs(Qiny).gt.eps) write(6,*) 'Qiny accuracy ',Qiny 
      If(dabs(Qinz).gt.eps) write(6,*) 'Qinz accuracy ',Qinz 
      If(dabs(EinQ-rs).gt.eps) write(6,*) 'EinQ accuracy ',EinQ-rs 
      If(dabs(msout-rs).gt.eps) write(6,*) 'msout accuracy ',msout-rs
*
      Return
      End
*
*
*       
      Double Precision Function ggLint(W2,m2,Q1s,Q2s,s1,s2,phi,s)
      Implicit None 
      Integer ivegas,iwaght,iapprx
      Double Precision W2,m2,Q1s,Q2s,s1,s2,phi,u1,u2,nu,X,
     &     Wmin,Wmax,t2user,t2umin,t2umax,
     &     rho1pp,rho2pp,rho1zz,rho2zz,ggLhTT,ggLhTS,ggLhSS,s,x1,x2,
     &     rho1pm,rho2pm,rho1p0,rho2p0,ggLrTS,ggLrTT
*
      Common /ggLapp/Wmin,Wmax,t2user,t2umin,t2umax,iapprx,ivegas,iwaght
*
      u1 = s2 - m2 + Q1s
      u2 = s1 - m2 + Q2s
      nu = 0.5d0*(W2 + Q1s + Q2s)
      X  = nu*nu - Q1s*Q2s
      If(iapprx.ge.2) then 
         x2 = s1/s
         x1 = W2/(x2*s)
         rho1pp = (2d0/x1**2)*(1d0+(1d0-x1)**2-2d0*m2*x1**2/Q1s)
         rho2pp = (2d0/x2**2)*(1d0+(1d0-x2)**2-2d0*m2*x2**2/Q2s)
         rho1zz = 4d0*(1d0-x1)/x1**2
         rho2zz = 4d0*(1d0-x2)/x2**2
         rho1pm = 0d0
         rho2pm = 0d0
         rho1p0 = 0d0
         rho2p0 = 0d0
      Else
         rho1pp = (u2-nu)**2/X + 1d0 - 4d0*m2/Q1s
         rho2pp = (u1-nu)**2/X + 1d0 - 4d0*m2/Q2s
         rho1zz = (u2-nu)**2/X - 1d0
         rho2zz = (u1-nu)**2/X - 1d0
         rho1pm = dmax1(0d0,( (u2-nu)**2/X - 1d0 - 4d0*m2/Q1s )/2d0)
         rho2pm = dmax1(0d0,( (u1-nu)**2/X - 1d0 - 4d0*m2/Q2s )/2d0)
         rho1p0 = (u2-nu)*dsqrt(rho1pm/X)
         rho2p0 = (u1-nu)*dsqrt(rho2pm/X)
      Endif
      ggLint = ggLhTT(W2,Q1s,Q2s)*rho1pp*rho2pp
     &       + ggLhTS(W2,Q1s,Q2s)*rho1pp*rho2zz
     &       + ggLhTS(W2,Q2s,Q1s)*rho1zz*rho2pp
     &       + ggLhSS(W2,Q1s,Q2s)*rho1zz*rho2zz
     &   - 8d0*ggLrTS(W2,Q1s,Q2s)*rho1p0*rho2p0*dcos(phi)
     &   + 2d0*ggLrTT(W2,Q1s,Q2s)*rho1pm*rho2pm*dcos(2d0*phi)
*
      Return
      End
*
*-------------------------------------------------------------------
*
      Double Precision Function ggLhTT(W2,Q1s,Q2s)
      Implicit None
      Double Precision W2,Q1s,Q2s,ggLhT,ggLgg
      Double Precision mucrss,SBFKL,resTT
      Integer imodel
      Common /ggLmod/ imodel
*
      If(imodel.eq.9) then
         ggLhTT = mucrss(-Q1s,-Q2s,1)
      Elseif(imodel.ge.100) then
         ggLhTT = resTT(W2,Q1s,Q2s,imodel)   
      Elseif(imodel.eq.30) then
         ggLhTT = SBFKL(dsqrt(Q1s),dsqrt(Q2s),1)
      Else
         ggLhTT = ggLhT(Q1s) * ggLhT(Q2s) * ggLgg(W2)
      Endif
      Return
      End
*
*-------------------------------------------------------------------
*
      Double Precision Function ggLhTS(W2,Q1s,Q2s)
      Implicit None
      Double Precision W2,Q1s,Q2s,ggLhT,ggLhS,ggLgg
      Double Precision mucrss,SBFKL,resTS
      Integer imodel
      Common /ggLmod/ imodel
*
      If(imodel.eq.9) then
         ggLhTS = mucrss(-Q1s,-Q2s,2)
      Elseif(imodel.ge.100) then
         ggLhTS = resTS(W2,Q1s,Q2s,imodel)   
      Elseif(imodel.eq.30) then
         ggLhTS = SBFKL(dsqrt(Q1s),dsqrt(Q2s),2)
      Else
         ggLhTS = ggLhT(Q1s) * ggLhS(Q2s) * ggLgg(W2)
      Endif
      Return
      End
*
*-------------------------------------------------------------------
*
      Double Precision Function ggLhSS(W2,Q1s,Q2s)
      Implicit None
      Double Precision W2,Q1s,Q2s,ggLhS,ggLgg
      Double Precision mucrss,SBFKL,resSS
      Integer imodel
      Common /ggLmod/ imodel
*
      If(imodel.eq.9) then
         ggLhSS = mucrss(-Q1s,-Q2s,4)
      Elseif(imodel.ge.100) then
         ggLhSS = resSS(W2,Q1s,Q2s,imodel)   
      Elseif(imodel.eq.30) then
         ggLhSS = SBFKL(dsqrt(Q1s),dsqrt(Q2s),4)
      Else
         ggLhSS = ggLhS(Q1s) * ggLhS(Q2s) * ggLgg(W2)
      Endif
      Return
      End
*
*-------------------------------------------------------------------
*
      Double Precision Function ggLrTS(W2,Q1s,Q2s)
      Implicit None
      Double Precision W2,Q1s,Q2s
      Double Precision mucrss,tauTS
      Integer imodel
      Common /ggLmod/ imodel
*
      If(imodel.eq.9) then
         ggLrTS = mucrss(-Q1s,-Q2s,6)
      Elseif(imodel.ge.100) then
         ggLrTS = tauTS(W2,Q1s,Q2s,imodel)   
      Else
         ggLrTS = 0d0
      Endif
      Return
      End
*
*-------------------------------------------------------------------
*
      Double Precision Function ggLrTT(W2,Q1s,Q2s)
      Implicit None
      Double Precision W2,Q1s,Q2s
      Double Precision mucrss,tauTT
      Integer imodel
      Common /ggLmod/ imodel
*
      If(imodel.eq.9) then
         ggLrTT = mucrss(-Q1s,-Q2s,5)
      Elseif(imodel.ge.100) then
         ggLrTT = tauTT(W2,Q1s,Q2s,imodel)   
      Else
         ggLrTT = 0d0
      Endif
      Return
      End
*

*-------------------------------------------------------------------
*
      Double Precision Function pipimodF(W2)
      Implicit None
      Double Precision W2
      Integer imodpi
      Common /ggLmodpi/ imodpi
      Double Precision mpi2s,fpi4,fpis,zplus,zminus
      
      Double Precision bepi, delt,mch2s
      double precision mPion /0.1349763d0/   !pion0 mass
      double precision mPich /0.1395702d0/   !pion+- mass
      double precision mKaon /0.493677d0/   ! kaon+- mass

      Double Precision s,rs,W,m,Pi,alem
      Common /ggLprm/ s,rs,W,m,Pi,alem
      Real rvec(10)
      Double precision thgg
      Common /thetagg/ thgg

*     Random theta around Px axis
      Call RanLux(rvec,1)
      thgg = rvec(1)*Pi
      
      mpi2s=mPion*mPion/W2 
      mch2s=mPich*mPich/W2   

*********pi0 pi0 total, chpt 
      if (imodpi.eq.10.and.w2.gt.4*mpion*mpion) then
         fpi4=0.134*0.134*0.134*0.134
         zplus=(1+dsqrt(1-4d0*mpi2s))/2d0
         zminus=(1-dsqrt(1-4d0*mpi2s))/2d0
         
         fpis=2d0*(log(zplus/zminus)*log(zplus/zminus)-pi*pi)
     &        +mpi2s*(log(zplus/zminus)*log(zplus/zminus)+pi*pi)
     &        *(log(zplus/zminus)*log(zplus/zminus)+pi*pi)
         
         pipimodF=1d0/pi/pi/pi/256d0/fpi4/137d0/137d0
     &        *(w2-mpion*mpion)*(w2-mpion*mpion)/w2
     &        *dsqrt(1-4d0*mpi2s)*(1+mpi2s*fpis)
*********pi0 pi0 diff, chpt
      else if (imodpi.eq.11.and.w2.gt.4*mpion*mpion) then
         fpi4=0.134*0.134*0.134*0.134
         zplus=(1+dsqrt(1-4d0*mpi2s))/2d0
         zminus=(1-dsqrt(1-4d0*mpi2s))/2d0
         
         fpis=2d0*(log(zplus/zminus)*log(zplus/zminus)-pi*pi)
     &        +mpi2s*(log(zplus/zminus)*log(zplus/zminus)+pi*pi)
     &        *(log(zplus/zminus)*log(zplus/zminus)+pi*pi)
         
         pipimodF=dsin(thgg)/pi/pi/512d0/fpi4/137d0/137d0
     &        *(w2-mpion*mpion)*(w2-mpion*mpion)/w2
     &        *dsqrt(1-4d0*mpi2s)*(1+mpi2s*fpis)
***********pi+pi- total pointlike pions
      else if(imodpi.eq.20.and.w2.gt.4*mpich*mpich) then
         pipimodF=2d0*pi/W2/137d0/137d0
     &        *(
     &        (1+4d0*mch2s)*dsqrt(1-4d0*mch2s)-8d0*mch2s*(1-2d0*mch2s)
     &        *log(w/2d0/mPich+dsqrt(w2/4d0/mpich/mpich-1))
     &        ) 
***********pi+pi- total  chpt
      else if (imodpi.eq.30.and.w2.gt.4*mpich*mpich) then    
         bepi=dsqrt(1-4d0*mch2s)
         delt=(1d0-bepi*bepi)/bepi*log((1d0+bepi)/(1d0-bepi))
         pipimodF=pi/137d0/137d0/2/w2*bepi
     &        *(
     &        4d0-4*(2d0-delt)
     &        +4d0*(2d0-2d0*delt+(1-bepi*bepi)*(1+delt/2d0))
     &        )
***********pi+pi- differential CHPT
      else if (imodpi.eq.31.and.w2.gt.4*mpich*mpich) then
         bepi=dsqrt(1-4d0*mch2s)
         pipimodF=pi**2/137d0/137d0/2/w2*bepi*dsin(thgg)
     &        *(
     &        2d0-4d0*bepi**2*dsin(thgg)**2
     &        /(1-bepi**2*dcos(thgg)**2)
     &        +4d0*bepi**4*dsin(thgg)**4/(1-bepi**2*dcos(thgg)**2)**2
     &        )        
***********K+K- total  chpt
      else if (imodpi.eq.40.and.w2.gt.4*mkaon*mkaon) then       
         bepi=dsqrt(1-4d0*mch2s)
         delt=(1d0-bepi*bepi)/bepi*log((1d0+bepi)/(1d0-bepi))
         pipimodF=pi/137d0/137d0/2/w2*bepi
     &        *(
     &        4d0-4*(2d0-delt)
     &        +4d0*(2d0-2d0*delt+(1-bepi*bepi)*(1+delt/2d0))
     &        )
         pipimodF=pipimodF*2;
***********K+K- differential CHPT
      else if (imodpi.eq.41.and.w2.gt.4*mkaon*mkaon) then
         bepi=dsqrt(1-4d0*mch2s)

         pipimodF=pi**2/137d0/137d0/2/w2*bepi*dsin(thgg)
     &        *(
     &        2d0-4d0*bepi**2*dsin(thgg)**2
     &        /(1-bepi**2*dcos(thgg)**2)
     &        +4d0*bepi**4*dsin(thgg)**4/(1-bepi**2*dcos(thgg)**2)**2
     &        )
         pipimodF=pipimodF*2;
*************
      else if (imodpi.ne.0.and.imodpi.lt.20.and.
     &        w2.le.4*mpion*mpion) then
         pipimodF = 0d0
      else if (imodpi.ge.20.and.imodpi.lt.40.and.w2.le.4*mpich**2) then
         pipimodF = 0d0
      else if (imodpi.ge.40.and.w2.le.4*mkaon**2) then
         pipimodF = 0d0
      else
         pipimodF=1d0
      endif
      Return
      End
*-------------------------------------------------------------------
*
      Double Precision Function ggLgg(W2)
      Implicit None
      Double Precision W2,eps,eta,Xparm,Yparm,MZsq
      Integer imodel,ncall
      Common /ggLmod/ imodel
      Data ncall /0/, eps /0.0808d0/, eta /0.4525d0/
      Data Xparm /211d3/, Yparm /215d3/, MZsq /8317d0/
      Double Precision pipimodf
      Integer imodpi
      Common /ggLmodpi/ imodpi
*
      If((imodel.ge.1).and.(imodel.le.4)) then ! Luminsoity
         ggLgg = 1d0
      Elseif((imodel.ge.31).and.(imodel.le.34)) then
         If(ncall.eq.0) then
            ncall = 1
            ggLgg = Xparm*MZsq**eps + Yparm/MZsq**eta
            Write(6,1000) Xparm,Yparm,eps,eta,ggLgg
         Endif
         ggLgg = Xparm*W2**eps + Yparm/W2**eta
      Endif
      If(imodpi.ne.0) then 
         ggLgg=pipimodF(W2)*0.389379d6
      endif
*

 1000 Format(//,1x,'Parameters for two-photon cross section [pb]:',/,
     &8x,'of the form X*s**eps + Y/s**eta',/,
     &3x,'                  X    = ',E12.5,/,
     &3x,'                  Y    = ',E12.5,/,
     &3x,'                  eps  = ',E12.5,/,
     &3x,'                  eta  = ',E12.5,/,
     &3x,'sigma[gammagamma](MZ)  = ',E12.5,//)
*
      Return
      End
*
*-------------------------------------------------------------------
*
      Double Precision Function ggLhT(Qs)
      Implicit None
      Double Precision Qs
      Integer imodel
      Double Precision r,xi,m1s,m2s,rrho,romeg,rphi,rc,mrhos,
     &     momegs,mphis,mzeros
*
      Common /ggLmod/ imodel
      Common /ggLhad/ r,xi,m1s,m2s,rrho,romeg,rphi,rc,mrhos,
     &     momegs,mphis,mzeros
*
      If    ((imodel.eq.1).or.(imodel.eq.31)) then
         ggLhT = 0.75d0/(1d0+Qs/m1s)**2 + 0.25d0/(1d0+Qs/m2s)
      Elseif((imodel.eq.2).or.(imodel.eq.32)) then
         ggLhT = rrho/(1d0+Qs/mrhos)**2 + romeg/(1d0+Qs/momegs)**2
     &         + rphi/(1d0+Qs/mphis)**2 + rc/(1d0+Qs/mzeros)
      Elseif((imodel.eq.3).or.(imodel.eq.33)) then
         ggLhT =  1d0/(1d0+Qs/mrhos)**2
      Elseif((imodel.eq.4).or.(imodel.eq.34)) then
         ggLhT = 1d0/(1d0+Qs/mrhos)**2
      Else 
         Write(6,*) 'Index out of bounds in ggLhT'
         Write(6,*) 'Program stops'
         Stop
      Endif
*
      Return
      End
*
*-------------------------------------------------------------------
*
      Double Precision Function ggLhS(Qs)
      Implicit None
      Double Precision Qs
      Integer imodel
      Double Precision r,xi,m1s,m2s,rrho,romeg,rphi,rc,mrhos,
     &     momegs,mphis,mzeros
*
      Common /ggLmod/ imodel
      Common /ggLhad/ r,xi,m1s,m2s,rrho,romeg,rphi,rc,mrhos,
     &     momegs,mphis,mzeros
*
      If    ((imodel.eq.1).or.(imodel.eq.31)) then
         ggLhS = 0.1875d0*Qs/m1s/(1d0+Qs/m1s)**2 
     &         + 0.0625d0*(m2s/Qs*dlog(1d0+Qs/m2s) 
     &         - 1d0/(1d0+Qs/m2s))
      Elseif((imodel.eq.2).or.(imodel.eq.32)) then
         ggLhS = rrho *Qs/(4d0*mrhos *(1d0+Qs/mrhos )**2)
     &         + romeg*Qs/(4d0*momegs*(1d0+Qs/momegs)**2)
     &         + rphi *Qs/(4d0*mphis *(1d0+Qs/mphis )**2)
      Elseif((imodel.eq.3).or.(imodel.eq.33)) then
         ggLhS =       Qs/(4d0*mrhos *(1d0+Qs/mrhos )**2)
      Elseif((imodel.eq.4).or.(imodel.eq.34)) then
         ggLhS = 0d0
      Else
         Write(6,*) 'Index out of bounds in ggLhS'
         Write(6,*) 'Program stops'
         Stop
      Endif
*
      Return
      End
*
*
*       
      Block Data ggLblk
      Implicit None
      Integer 
     & it1,iD1,iD3,iD5,itX,iph,ip1,ip2,ia1,ia2,ia3,ia4,ie1,ie2,ipt,is
      Integer  nfcall,itmx,ndim,nprn
cc,nbook,ihbook
      Integer ivegas,iwaght,iapprx,imodel,iJP,iq,i1
      Integer npts,nzero,ntrial
      Double Precision r,xi,m1s,m2s,rrho,romeg,rphi,rc,mrhos,
     &     momegs,mphis,mzeros
      Double Precision acc, s,rs,W,m,Pi,alem,Wmin,Wmax,t2user,t2umin,
     &     t2umax,xl,xu,Fmax,Fmin,Weight,cross,error
      Double Precision delta,Qmin,Qmax,Lambda,Nf,cQ,cmu
      Double Precision Rmass,Rwidth,Pmass,Rtotw
*
      Common /ggLhad/ r,xi,m1s,m2s,rrho,romeg,rphi,rc,mrhos,
     &     momegs,mphis,mzeros
      Common /ggLerr/ 
     &   it1,iD1,iD3,iD5,itX,iph,ip1,ip2,ia1,ia2,ia3,ia4,ie1,ie2,ipt,is
      Common /ggLprm/ s,rs,W,m,Pi,alem
      Common /ggLvg1/ xl(10),xu(10),acc,ndim,nfcall,itmx,nprn
      Common /ggLapp/Wmin,Wmax,t2user,t2umin,t2umax,iapprx,ivegas,iwaght
      Common /ggLmod/ imodel
      Common /ggLuno/ cross,error,Fmax,Fmin,Weight,npts,nzero,ntrial
      Common /ggBFKL/ delta,Qmin,Qmax,Lambda,Nf,cQ,cmu
      Common /ggLres/ Rmass,Rwidth,Pmass,Rtotw,iJP,iq,i1
*
      Data r /0.75d0/, xi /0.25d0/, m1s /0.54d0/, m2s /1.8d0/,
     &     rrho /0.65d0/, romeg /0.08d0/, rphi /0.05d0/, rc /0.22d0/
     &     mrhos /0.5920d0/, momegs /0.611d0/, mphis /1.039d0/,
     &     mzeros /1.96d0/
*
      Data it1/0/,iD1/0/,iD3/0/,iD5/0/,itX/0/,iph/0/,ip1/0/,ip2/0/,
     &     ia1/0/,ia2/0/,ia3/0/,ia4/0/,ie1/0/,ie2/0/,ipt/0/,is/0/
*
      Data s/1d4/, rs/1d2/, W/1d1/, m/511d-6/, 
     &     Pi/314159265d-8/, alem/729927007d-11/
*
      Data xl /10*0d0/, xu /10*1d0/, acc /1d-4/, ndim/4/, 
     &     nfcall/100000/, itmx/4/, nprn/2/
*
      Data Wmin /0d0/, Wmax /1d12/, t2user /-5d0/, 
     &     t2umin /-1d12/, t2umax /0d0/, 
     &     iapprx/0/, imodel/1/, ivegas/1/ ,iwaght/1/
*
      Data cross /0d0/, error /0d0/, Fmax /0d0/, Fmin /0d0/, 
     &     Weight /0d0/, npts /1000000/, nzero /0/, ntrial /0/
*
      Data delta /1d2/, Qmin /2.0d0/, Qmax /2.0d2/, 
     &     Lambda /0.3d0/, Nf /3d0/, cQ /100d0/,  
     &     cmu /0.1888756028/   ! c_mu = exp(-5/3)
*
      Data Rmass /2.9798d0/, Rwidth /1.0D-6/, Pmass/0.77d0/, 
     &     Rtotw/13.2d-3/,
     &     iJP /1/, iq /4/, i1 /1/
*
      End
*
*
      Subroutine ggLuin
      Implicit None
      Double Precision ggLuG,ggLuF,x(10),y,pts,
     &     Fmax,Fmin,Weight,cross,error,SumFi,SumFsq,avF,avFsq,Sigma,
     &     effic,xl,xu,acc
      Integer i,k,npts,nzero,ntrial,ndim,nfcall,itmx,nprn
      Real rvec(10),time1,time2
      Data x/10*0d0/
      Save x
      Common /ggLvg1/ xl(10),xu(10),acc,ndim,nfcall,itmx,nprn
      Common /ggLuno/ cross,error,Fmax,Fmin,Weight,npts,nzero,ntrial
*
      Call Timex(time1)
      nzero  = 0
      SumFi  = 0d0
      SumFsq = 0d0
      Fmax   = 0d0
      Fmin   = 1d9
      Do 100 i = 1 , npts
         Call RanLux(rvec,ndim)
         Do 50 k = 1 , ndim
            x(k) = rvec(k)
 50      Continue
c         y = ggLuF(x)
         y = ggLuG(x)
c         If(y.gt.Fmax) Fmax = y
c         If(y.lt.Fmin.and.y.ne.0d0) Fmin = y
         SumFi  = SumFi  + y
         SumFsq = SumFsq + y*y
 100  Continue
*
      pts   = dble(npts)
      avF   = SumFi/pts
      avFsq = SumFsq/pts
      Sigma = dsqrt(avFsq - avF*avF)
      error = Sigma/dsqrt(pts)
      effic = Fmax/avF
      Call Timex(time2)
*
      Write(6,1000) npts,avF,Sigma,error,Fmax,Fmin,effic,nzero,
     &     time2-time1
*
 1000 Format(//,
     &3x,'Subroutine ggLuin uses',I12,' points to initialize',/,
     &3x,'Result is:',/,
     &8x,'Average function value   = ',G14.6,/,
     &8x,'One-shot error           = ',G14.6,/,
     &8x,'Standard deviation       = ',G14.6,/,
     &8x,'Maximum function value   = ',G14.6,/,
     &8x,'Minimum function value   = ',G14.6,/,
     &8x,'Maximum f / average f    = ',G14.6,/,
     &8x,'Number of function zeros = ',I14,/,
     &8x,'Time (in s) for ggLuin   = ',E12.5,/)
*
      Return
      End
*
******************************************************************
*
      Double Precision Function ggLuG(z)
      Implicit None
      Double Precision z(10)
*
      Integer NDO,IT
      Double Precision xl,xu,acc,SI,SI2,SWGT,SCHI,XI
      COMMON/ggLvg1/XL(10),XU(10),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON/ggLvg2/XI(50,10),SI,SI2,SWGT,SCHI,NDO,IT
      Integer j,iaj,iaj1,ndim,ncall,itmx,nprn,ncount
      Double Precision xjac,wgt,dxg,xo,rc,ggLuF,x(10),
     &     xn,xnd,one,dx(10)
*
      Data ncount/0/
      Parameter(one=1D0)
*
      If(ncount.eq.0) then
         XJAC = 1d0
         DO 3 J=1,NDIM
            DX(J)=XU(J)-XL(J)
            XJAC=XJAC*DX(J)
 3       Continue
         DXG=dble(ndo)
         XND=dble(ndo)
      Endif
      WGT=XJAC
*
      DO 15 J=1,NDIM
C        XN=(KG(J)-QRAN(J))*DXG+ONE
         XN=          z(j) *DXG+ONE
         IAJ=XN
         IF(IAJ.GT.1) GO TO 13
         XO=XI(IAJ,J)
         RC=(XN-IAJ)*XO
         GO TO 14
 13      XO=XI(IAJ,J)-XI(IAJ-1,J)
         RC=XI(IAJ-1,J)+(XN-IAJ)*XO
 14      X(J)=XL(J)+RC*DX(J)
         WGT=WGT*XO*XND
 15   Continue
C     
c     F=WGT
c     F=F*FXN(X,WGT)
      ggLuG=ggLuF(X,WGT)*WGT
*
      Return
      End
*
*
*
      Subroutine ggLgen(Flag)
      Implicit None
      Logical Flag
      Integer ndim,naccep,itmx,nprn,ndo,it,
     &     npts,nzero,ntrial, i,ncall, iapprx,ivegas,iwaght
      Double Precision xl,xu,acc,xi,si,si2,swgt,schi,
     &     x(10),f,fran,Fmax,Fmin,Weight,cross,error,ggLuG,Fmold
     &     ,Wmin,Wmax,t2user,t2umin,t2umax,ggLuF
      real rvec(10)
      Common /ggLvg1/ XL(10),XU(10),ACC,NDIM,NCALL,ITMX,NPRN
      Common /ggLvg2/ XI(50,10),SI,SI2,SWGT,SCHI,NDO,IT
      Common /ggLuno/ cross,error,Fmax,Fmin,Weight,npts,nzero,ntrial
      Common /ggLapp/Wmin,Wmax,t2user,t2umin,t2umax,iapprx,ivegas,iwaght
      Data Naccep/0/
      Save Naccep
      Save Fmold
*
      If(Naccep.eq.0) then
         nzero = 0
         ntrial = 0
      Endif
      Fmold = Fmax
 10   Continue
      ntrial = ntrial + 1
      Call RanLux(rvec,ndim+1)
      Do 20 i=1,ndim
         x(i) = rvec(i)
 20   Continue
*
      If(ivegas.eq.1) then
         f = ggLuG(x)
      Else
         f = ggLuF(x,1d0)
      Endif
*
      If(iwaght.eq.1) then
         weight = 1d0
         fran = rvec(ndim+1) * Fmold
         If(fran.gt.f) goto 10
      Endif
      Naccep = Naccep + 1
*
      If(f.gt.Fmold) then
         Write(6,1000) f/Fmold - 1d0,ntrial
c         Fmax = f
         Flag = .false.
      Else
         Flag = .true.
      Endif
 1000 Format(/,3x,'Warning from ggLgen:',/,
     & 3x,'new maximum found: f/fmax - 1 = ',E14.6,/,
     & 3x,'number of trials = ',I8,/)
*
      Return
      End
*
*--------------------------------------------------------------------
*
      Double Precision Function mucrss(t1,t2,i)
      Implicit None
      Integer i
      Double Precision t1,t2,m
      Double Precision q12,x,Tt,Dt,Ll,barn
      Double Precision sigTT,sigTS,sigST,sigSS,tauTT,tauTS
      Double Precision s,rs,W,me,Pi,alpha
      Common /ggLprm/ s,rs,W,me,Pi,alpha
*
      Data m /0.1057d0/         !muon mass
      Data barn /3.89385d+05/
*
      q12 = 0.5D0*w**2-0.5D0*t1-0.5D0*t2
       x  = (q12**2-t1*t2)/w**2
      Tt  = 4.D0*m**2*x+t1*t2
      Dt  = 2.D0*dsqrt(x*(w**2-4.D0*m**2))
      Ll  = dlog((q12+0.5D0*Dt)**2/(4.D0*m**2*x+t1*t2))
*
      If(i.eq.1) then
         mucrss = 
c         sigTT =
     #pi*alpha**2/w**2/x*(q12*Ll*(2.D0+2.D0*m**2/x-4.D0*m**4/q12**2
     #+(t1+t2)/x+0.5D0*t1*t2*w**2/x/q12**2+0.75D0*t1**2*t2**2/x**2/q12**
     #2)-Dt*(1.D0+m**2/x+(t1+t2)/x+t1*t2/Tt+0.75D0*t1*t2/x**2))
      Elseif(i.eq.2) then
         mucrss = 
c         sigTS =
     #-pi*alpha**2*t2/w**2/x**2*(Dt*(1.D0+t1/Tt*(6.D0*m**2+t1+0.15E
     #1*t1*t2/x))-Ll/q12*(4.D0*m**2*x+t1*(w**2+2.D0*m**2)+t1*(t1+t2+0.15
     #E1*t1*t2/x)))
      Elseif(i.eq.3) then
         mucrss = 
c         sigST =
     #-pi*alpha**2*t1/w**2/x**2*(Dt*(1.D0+t2/Tt*(6.D0*m**2+t2+0.15E
     #1*t1*t2/x))-Ll/q12*(4.D0*m**2*x+t2*(w**2+2.D0*m**2)+t2*(t1+t2+0.15
     #E1*t1*t2/x)))
      Elseif(i.eq.4) then
         mucrss = 
c         sigSS =
     #pi*alpha**2*t1*t2/w**2/x**3*(Ll/q12*(2.D0*w**2*x+3.D0*t1*t2)-
     #Dt*(2.D0+t1*t2/Tt))
      Elseif(i.eq.5) then
         mucrss = 
c         tauTT =
     #-0.25D0*pi*alpha**2/w**2/x*(2.D0*Dt/x*(2.D0*m**2+(t1-t2)**2/w
     #**2+0.15E1*t1*t2/x)+Ll/q12*(16.D0*m**4-16.D0*m**2*(t1+t2)-4.D0*t1*
     #t2*(2.D0+2.D0*m**2/x+(t1+t2)/x+0.75D0*t1*t2/x**2)))
      Elseif(i.eq.6) then
         mucrss = 
c      tauTS =
     #-pi*alpha**2*dsqrt(t1*t2)/w**2/x**2*(Ll*(2.D0*m**2+t1+t2+0.15
     #E1*t1*t2/x)+Dt*(2.D0-0.15E1*q12/x))
      Else
         mucrss = 0d0
      Endif
*
      mucrss = mucrss*barn
*
      Return
      End
*
*--------------------------------------------------------------------
*
      Double Precision Function SBFKL(Q1,Q2,i)
      Implicit None
      Integer i,ncall
      Integer iz1,iz2,iz3,iz4,iz5
      Double Precision Q1,Q2,delta,Qmin,Qmax,cmu,c1,c2,c3,cQ
     &     ,alsmin,alsmax
      Double Precision nbarn,Nc,Nf,Qf4,Lambda,alphas,xi,t0,MZ
      Double Precision s,rs,W,me,Pi,alpha
      Common /ggLprm/ s,rs,W,me,Pi,alpha
      Common /izahl /iz1,iz2,iz3,iz4,iz5
      Common /ggBFKL/ delta,Qmin,Qmax,Lambda,Nf,cQ,cmu
*
      Data ncall /0/, nbarn /0.389385d+06/, Nc /3d0/, MZ/91.2d0/
      Data c1 /0.5942196D-1/    ! 4/(56*Zeta(3))
      Data c2 /0.2772589D1/     ! 4*ln(2)
      Data c3 /0.1644023E0/     ! Pi^(9/2)/( 256*sqrt(14*Zeta(3)) )
*
      If(ncall.eq.0) then
         ncall = 1
         alsmin = 1d3
         alsmax = 0d0
         If(Nf.lt.3.5d0) then
            Qf4 = 4d0/9d0
         Elseif(Nf.gt.4.5d0) then
            Qf4 = 121d0/81d0
         Else
            Qf4 = 100d0/81d0
         Endif
         alphas = 12d0*Pi/( (33d0 - 2d0*Nf)*dlog(MZ*MZ/Lambda**2) )
         Write(6,1000) delta,Qmin,Qmax,cmu,cQ,Lambda,Nf,alphas,Qf4
      Endif
 1000 Format(//,1x,'Parameters for BFKL cross section [in pb]:',/,
     &3x,'delta*Q1*Q2 < W2    : delta = ',E12.5,/,
     &3x,'minimum Q                   = ',E12.5,/,
     &3x,'maximum Q                   = ',E12.5,/,
     &3x,'c_mu                        = ',E12.5,/,
     &3x,'c_Q                         = ',E12.5,/,
     &3x,'Lambda(QCD) in alphas       = ',E12.5,/,
     &3x,'No. of flavours in alphas   = ',E12.5,/,
     &3x,'alphas(MZ)                  = ',E12.5,/,
     &3x,'( sum(eq**2,q=1..Nf) )**2   = ',E12.5,//)
*
      If( delta*Q1*Q2.ge.W**2 ) then
         iz1 = iz1 + 1
         SBFKL = 0d0
      Elseif( Q1.lt.Qmin )  then
         iz2 = iz2 + 1
         SBFKL = 0d0
      Elseif( Q2.lt.Qmin )  then
         iz3 = iz3 + 1
         SBFKL = 0d0
      Elseif( Q1.gt.Qmax )  then
         iz4 = iz4 + 1
         SBFKL = 0d0
      Elseif( Q2.gt.Qmax )  then
         iz5 = iz5 + 1
         SBFKL = 0d0
      Else
         alphas = 12d0*Pi/( (33d0 - 2d0*Nf)*dlog(cmu*Q1*Q2/Lambda**2) )
         xi = Nc*alphas*dlog(W**2/(cQ*Q1*Q2))/Pi
         t0 = Qf4 *c3 *alpha**2 *alphas**2 / ( Q1*Q2*dsqrt(xi) )
     #        * dexp( c2*xi )
     #        * dexp( - c1*(dlog(Q1/Q2))**2/xi )
*
         If(alsmin.gt.alphas) then
            alsmin = alphas
            Write(6,*) 'alsmin = ', alsmin
         Endif
         If(alsmax.lt.alphas) then
            alsmax = alphas
            Write(6,*) 'alsmax = ', alsmax
         Endif
         If(ncall.le.10) then
            write(6,*) 'alphas = ',alphas
            ncall = ncall + 1
         Endif
*
         If(i.eq.1) then
            SBFKL = 81d0*t0
         Elseif(i.eq.2) then
            SBFKL = 18d0*t0
         Elseif(i.eq.3) then
            SBFKL = 18d0*t0
         Elseif(i.eq.4) then
            SBFKL = 4d0*t0
         Else
            SBFKL = 0d0
         Endif
         SBFKL = SBFKL * nbarn
      Endif
*
      Return
      End
*
*--------------------------------------------------------------------
*
      Double Precision Function resTT(W2,Q1s,Q2s,i)
      Implicit None
      Double Precision W2,Q1s,Q2s
      Integer i,iJP,iq,i1
*
      Double Precision Rmass,Rwidth,Pmass,Rtotw
      Common /ggLres/ Rmass,Rwidth,Pmass,Rtotw,iJP,iq,i1
*
      Double Precision kappa,nu,X,R2,nu2,nu4,nbarn,
     &    resTT0,resTT1,resTT2,Q1h,Q2h,pMsq,arJ(6)
      Integer ncall
*
      Data ncall /0/, nbarn /0.389385d+06/
      Data arJ /0d0,0d0,1d0,2d0,2d0,0d0/
      Data pMsq /0.5920d0/      ! m_rho^2 
*
      Double Precision s,roots,Whad,m,Pi,alem
      Common /ggLprm/ s,roots,Whad,m,Pi,alem
*
      If((i1.eq.2).and.(iJP.ge.1.and.iJP.le.6)) then
         resTT = (1d0/(1d0+Q1s/pMsq))**2 ! rho-mass
     &         * (1d0/(1d0+Q2s/pMsq))**2
      Elseif(i1.eq.1) then
         R2    = Pmass**2
         nu    = (R2 + Q1s + Q2s)/2d0
         nu2   = nu*nu
         nu4   = nu2*nu2
         X     = nu*nu - Q1s*Q2s
         kappa = R2/2d0/dsqrt(X)
         If    (iJP.eq.1 .or. iJP.eq.6) then
            resTT = kappa*X/nu2
         Elseif(iJP.eq.2) then
            resTT = kappa*( (X + nu*R2)/(3d0*nu2) )**2
         Elseif(iJP.eq.3) then
            resTT = kappa*(Q2s-Q1s)**2/(4d0*nu2)
         Elseif(iJP.eq.4) then
            resTT = kappa *(R2/(2d0*nu))**2 *( 1d0 + 
     &           ( 2d0*Q1s*Q2s - nu*(Q1s+Q2s) )**2/(6d0*R2*R2*nu2) )
         Elseif(iJP.eq.5) then
            resTT = kappa * (X/nu2)**3
         Endif
      Else
         Write(6,*) 'Index out of bounds in Resonance cross sections'
         Write(6,*) 'Program stops'
         Stop
      Endif
*
* A factor 1/s is needed since 
* \delta(W^2 - M^2) = (1/s) \delta(\tau - tau_0)
      resTT = (2d0*arJ(iJP)+1d0) * 
     &     resTT * nbarn * 8d0*Pi**2*Rwidth/Rmass/s
      Ncall = Ncall+1
      If(Ncall.le.10) Write(6,*) 'i, resTT = ',i, resTT
      Return
      End
*
*--------------------------------------------------------------------
*
      Double Precision Function resTS(W2,Q1s,Q2s,i)
      Implicit None
      Double Precision W2,Q1s,Q2s
      Integer i,iJP,iq,i1
*
      Double Precision Rmass,Rwidth,Pmass,Rtotw
      Common /ggLres/ Rmass,Rwidth,Pmass,Rtotw,iJP,iq,i1
*
      Double Precision kappa,nu,X,R2,nu2,nu4,nbarn,arJ(6)
      Integer ncall
*
      Data ncall /0/, nbarn /0.389385d+06/
      Data arJ /0d0,0d0,1d0,2d0,2d0,0d0/
*
      Double Precision s,roots,Whad,m,Pi,alem
      Common /ggLprm/ s,roots,Whad,m,Pi,alem
*
      If((i1.eq.2).and.(iJP.ge.1.and.iJP.le.6)) then
         resTS = 0d0
      Elseif(i1.eq.1) then
         R2    = Pmass**2
         nu    = (R2 + Q1s + Q2s)/2d0
         nu2   = nu*nu
         nu4   = nu2*nu2
         X     = nu*nu - Q1s*Q2s
         kappa = R2/2d0/dsqrt(X)
         If    (iJP.eq.1 .or. iJP.eq.6) then
            resTS = 0d0
         Elseif(iJP.eq.2) then
            resTS = 0d0
         Elseif(iJP.eq.3) then
******************************** sign
            resTS = + kappa*R2*Q2s*(nu + Q1s)**2/(2d0*nu4)
         Elseif(iJP.eq.4) then
            resTS = + kappa* R2*Q2s*(nu-Q1s)**2/(4d0*nu4)
         Elseif(iJP.eq.5) then
            resTS = 0d0
         Endif
      Else
         Write(6,*) 'Index out of bounds in Resonance cross sections'
         Write(6,*) 'Program stops'
         Stop
      Endif
*
      resTS = (2d0*arJ(iJP)+1d0) * 
     &     resTS * nbarn * 8d0*Pi**2*Rwidth/Rmass/s
      Return
      End
*
*--------------------------------------------------------------------
*
      Double Precision Function resSS(W2,Q1s,Q2s,i)
      Implicit None
      Double Precision W2,Q1s,Q2s
      Integer i,iJP,iq,i1
*
      Double Precision Rmass,Rwidth,Pmass,Rtotw
      Common /ggLres/ Rmass,Rwidth,Pmass,Rtotw,iJP,iq,i1
*
      Double Precision kappa,nu,X,R2,nu2,nu4,nbarn,arJ(6)
      Integer ncall
*
      Data ncall /0/, nbarn /0.389385d+06/
      Data arJ /0d0,0d0,1d0,2d0,2d0,0d0/
*
      Double Precision s,roots,Whad,m,Pi,alem
      Common /ggLprm/ s,roots,Whad,m,Pi,alem
*
      If((i1.eq.2).and.(iJP.ge.1.and.iJP.le.6)) then
         resSS = 0d0
      Elseif(i1.eq.1) then
         R2    = Pmass**2
         nu    = (R2 + Q1s + Q2s)/2d0
         nu2   = nu*nu
         nu4   = nu2*nu2
         X     = nu*nu - Q1s*Q2s
         kappa = R2/2d0/dsqrt(X)
         If    (iJP.eq.1 .or. iJP.eq.6) then
            resSS = 0d0
         Elseif(iJP.eq.2) then
            resSS = 2d0*kappa*(R2/(3d0*nu2))**2*Q1s*Q2s
         Elseif(iJP.eq.3) then
            resSS = 0d0
         Elseif(iJP.eq.4) then
            resSS = kappa*R2*R2*Q1s*Q2s/(3d0*nu4)
         Elseif(iJP.eq.5) then
            resSS = 0d0
         Endif
      Else
         Write(6,*) 'Index out of bounds in Resonance cross sections'
         Write(6,*) 'Program stops'
         Stop
      Endif
*
      resSS = (2d0*arJ(iJP)+1d0)*
     &     resSS * nbarn * 8d0*Pi**2*Rwidth/Rmass/s
      Return
      End
*
*--------------------------------------------------------------------
*
      Double Precision Function tauTT(W2,Q1s,Q2s,i)
      Implicit None
      Double Precision W2,Q1s,Q2s
      Integer i,iJP,iq,i1
*
      Double Precision Rmass,Rwidth,Pmass,Rtotw
      Common /ggLres/ Rmass,Rwidth,Pmass,Rtotw,iJP,iq,i1
*
      Double Precision kappa,nu,X,R2,nu2,nu4,nbarn,
     &    tauTT0,tauTT1,tauTT2,Q1h,Q2h,pMsq,arJ(6)
      Integer ncall
*
      Data ncall /0/, nbarn /0.389385d+06/
      Data arJ /0d0,0d0,1d0,2d0,2d0,0d0/
      Data pMsq /0.5920d0/      ! m_rho^2 
*
      Double Precision s,roots,Whad,m,Pi,alem
      Common /ggLprm/ s,roots,Whad,m,Pi,alem
*
      If((i1.eq.2).and.(iJP.ge.1.and.iJP.le.6)) then
         tauTT = 0d0
      Elseif(i1.eq.1) then
         R2    = Pmass**2
         nu    = (R2 + Q1s + Q2s)/2d0
         nu2   = nu*nu
         nu4   = nu2*nu2
         X     = nu*nu - Q1s*Q2s
         kappa = R2/2d0/dsqrt(X)
         If    (iJP.eq.1 .or. iJP.eq.6) then
            tauTT = kappa*X/nu2
            tauTT = - 2d0* tauTT
         Elseif(iJP.eq.2) then
            tauTT = kappa*( (X + nu*R2)/(3d0*nu2) )**2
            tauTT = + 2d0* tauTT
         Elseif(iJP.eq.3) then
            tauTT = kappa*(Q2s-Q1s)**2/(4d0*nu2)
            tauTT = - 2d0* tauTT
         Elseif(iJP.eq.4) then
c            tauTT = kappa *(R2/(2d0*nu))**2 *( 1d0 + 
            tauTT = kappa *(R2/(2d0*nu))**2 *( 
     &           ( 2d0*Q1s*Q2s - nu*(Q1s+Q2s) )**2/(6d0*R2*R2*nu2) )
            tauTT = + 2d0* tauTT
         Elseif(iJP.eq.5) then
            tauTT = kappa * (X/nu2)**3
            tauTT = - 2d0* tauTT
         Endif
      Else
         Write(6,*) 'Index out of bounds in Resonance cross sections'
         Write(6,*) 'Program stops'
         Stop
      Endif
*
* A factor 1/s is needed since 
* \delta(W^2 - M^2) = (1/s) \delta(\tau - tau_0)
      tauTT = (2d0*arJ(iJP)+1d0) * 
     &     tauTT * nbarn * 8d0*Pi**2*Rwidth/Rmass/s
      Ncall = Ncall+1
      If(Ncall.le.10) Write(6,*) 'i, tauTT = ',i, tauTT
*
      Return
      End
*
*--------------------------------------------------------------------
*
      Double Precision Function tauTS(W2,Q1s,Q2s,i)
      Implicit None
      Double Precision W2,Q1s,Q2s
      Integer i,iJP,iq,i1
*
      Double Precision Rmass,Rwidth,Pmass,Rtotw
      Common /ggLres/ Rmass,Rwidth,Pmass,Rtotw,iJP,iq,i1
*
      Double Precision kappa,nu,X,R2,nu2,nu4,nbarn,arJ(6)
      Integer ncall
*
      Data ncall /0/, nbarn /0.389385d+06/
      Data arJ /0d0,0d0,1d0,2d0,2d0,0d0/
*
      Double Precision s,roots,Whad,m,Pi,alem
      Common /ggLprm/ s,roots,Whad,m,Pi,alem
*
      If((i1.eq.2).and.(iJP.ge.1.and.iJP.le.6)) then
         tauTS = 0d0
      Elseif(i1.eq.1) then
         R2    = Pmass**2
         nu    = (R2 + Q1s + Q2s)/2d0
         nu2   = nu*nu
         nu4   = nu2*nu2
         X     = nu*nu - Q1s*Q2s
         kappa = R2/2d0/dsqrt(X)
         If    (iJP.eq.1 .or. iJP.eq.6) then
            tauTS = 0d0
         Elseif(iJP.eq.2) then
            tauTS = kappa*dsqrt(Q1s*Q2s)*R2*(X + nu*R2)/(9d0*nu4)
         Elseif(iJP.eq.3) then
            tauTS = + kappa*R2*dsqrt(Q1s*Q2s)*
     &           (nu + Q1s)*(nu + Q2s)/(4d0*nu4)
         Elseif(iJP.eq.4) then
            tauTS = kappa*dsqrt(Q1s*Q2s)*R2
     &           *(3d0*nu2 - nu*(Q1s + Q2s) - Q1s*Q2s)
     &           /(24d0*nu4)
         Elseif(iJP.eq.5) then
            tauTS = 0d0
         Endif
      Else
         Write(6,*) 'Index out of bounds in Resonance cross sections'
         Write(6,*) 'Program stops'
         Stop
      Endif
*
      tauTS = (2d0*arJ(iJP)+1d0) * 
     &     tauTS * nbarn * 8d0*Pi**2*Rwidth/Rmass/s
*
      Return
      End


C------------------------------------------------------------------

      SUBROUTINE InitMassWidth(i,j,M,G,GT,PM,pn)
        IMPLICIT NONE

C M  = Mass, G  = Two-photon width, Gt = Total width
C Pm = Pole mass for form factors

        INTEGER i,j
        DOUBLE PRECISION M,G,GT,Pm
        DOUBLE PRECISION Mass(6,5),GamW(6,5),Gtot(6,5),Wf2(6,5)
        DOUBLE PRECISION MixAng(6)
        DOUBLE PRECISION MixingAngle,eq2
        DOUBLE PRECISION Alpha,pi
        DOUBLE PRECISION mu,alphas
        Character*9 Name(6,5),pn
        
        Data mu /.9645980457d0/

        Alpha=1.0d0/137.0d0
        pi=3.14159d0
C Initialize name array:                                                 
        Data Name(1,1)  ,Name(1,2)  ,Name(1,3)  ,Name(1,4)  ,Name(1,5)/
     +     'Pion     ','eta      ','eta_prime','eta_c    ','eta_b    '/
        Data Name(2,1)  ,Name(2,2)  ,Name(2,3)  ,Name(2,4)  ,Name(2,5)/
     +     'a0(980)  ','f0(980)  ','f0(1370) ','chi_c0   ','chi_b0   '/
        Data Name(3,1)  ,Name(3,2)  ,Name(3,3)  ,Name(3,4)  ,Name(3,5)/
     +     'a1(1260) ','f1(1285) ','f1(1510) ','chi_c1   ','chi_b1   '/
        Data Name(4,1)  ,Name(4,2)  ,Name(4,3)  ,Name(4,4)  ,Name(4,5)/
     +     'a2(1320) ','f2(1270) ','f2(1525) ','chi_c2   ','chi_b2   '/
        Data Name(5,1)  ,Name(5,2)  ,Name(5,3)  ,Name(5,4)  ,Name(5,5)/
     +     'Pion_2   ','eta_D    ','eta_pD   ','eta_cD   ','eta_bD   '/
        Data Name(6,1)  ,Name(6,2)  ,Name(6,3)  ,Name(6,4)  ,Name(6,5)/
     +     'Pion(2S) ','eta(2S)  ','eta_p(2S)','eta_c(2S)','eta_b(2S)'/
C Initialize the mass array: (in MeV)
        DATA Mass(1,1) ,Mass(1,2) ,Mass(1,3) ,Mass(1,4) ,Mass(1,5)/
     +       134.9764d0,547.45d0  ,957.77d0  ,2979.8d0  ,9400d0   /
        DATA Mass(2,1) ,Mass(2,2) ,Mass(2,3) ,Mass(2,4) ,Mass(2,5)/
     +       983.5d0   ,980d0     ,1370d0    ,3415.1d0  ,9859.8d0 /
        DATA Mass(3,1) ,Mass(3,2) ,Mass(3,3) ,Mass(3,4) ,Mass(3,5)/
     +       1230d0    ,1282d0    ,1512d0    ,3510.53d0 ,9891.9d0 /
        DATA Mass(4,1) ,Mass(4,2) ,Mass(4,3) ,Mass(4,4) ,Mass(4,5)/
     +       1318.1d0  ,1275d0    ,1525d0    ,3556.17d0 ,9913.2d0 /    
        DATA Mass(5,1) ,Mass(5,2) ,Mass(5,3) ,Mass(5,4) ,Mass(5,5)/
     +       1670d0    ,1680d0    ,1890d0    ,3840d0    ,10150d0  /
        DATA Mass(6,1) ,Mass(6,2) ,Mass(6,3) ,Mass(6,4) ,Mass(6,5)/
     +       1300d0    ,1295d0    ,1400d0    ,3594d0    ,9980d0  /

C Initialize the TwoGamW array: (in keV)
        DATA GamW(1,1) ,GamW(1,2) ,GamW(1,3) ,GamW(1,4) ,GamW(1,5)/
     +       7.74d-3   ,0.46      ,4.26d0    ,7.5d0     ,0d0      /
        DATA GamW(2,1) ,GamW(2,2) ,GamW(2,3) ,GamW(2,4) ,GamW(2,5)/
     +       0.24d0    ,0.56d0    ,5.4d0     ,4d0       ,0d0      /
        DATA GamW(3,1) ,GamW(3,2) ,GamW(3,3) ,GamW(3,4) ,GamW(3,5)/
     +       0d0       ,0d0       ,0d0       ,0d0       ,0d0      /
        DATA GamW(4,1) ,GamW(4,2) ,GamW(4,3) ,GamW(4,4) ,GamW(4,5)/
     +       1.04d0    ,2.44d0    ,0.097d0   ,0.37d0    ,0d0      /
        DATA GamW(5,1) ,GamW(5,2) ,GamW(5,3) ,GamW(5,4) ,GamW(5,5)/
     +       1.35d0    ,0d0       ,0d0       ,0d0       ,0d0      /
        DATA GamW(6,1) ,GamW(6,2) ,GamW(6,3) ,GamW(6,4) ,GamW(6,5)/
     +       0d0       ,0d0       ,0d0       ,0d0       ,0d0      /

C Initialize the Gamma_total array: (in MeV)
        DATA Gtot(1,1) ,Gtot(1,2) ,Gtot(1,3) ,Gtot(1,4) ,Gtot(1,5)/
     +       7.83d-6   ,1.18d-3   ,0.201d0   ,13.2d0    ,0d0      /
        DATA Gtot(2,1) ,Gtot(2,2) ,Gtot(2,3) ,Gtot(2,4) ,Gtot(2,5)/
     +       60.0d0    ,50.0d0    ,400.d0    ,14.d0     ,0d0      /
        DATA Gtot(3,1) ,Gtot(3,2) ,Gtot(3,3) ,Gtot(3,4) ,Gtot(3,5)/
     +       400.d0    ,24.8d0    ,35.d0     ,0.88d0    ,0d0      /
        DATA Gtot(4,1) ,Gtot(4,2) ,Gtot(4,3) ,Gtot(4,4) ,Gtot(4,5)/
     +       107.d0    ,185.d0    ,76.d0     ,2.0d0     ,0d0      /
        DATA Gtot(5,1) ,Gtot(5,2) ,Gtot(5,3) ,Gtot(5,4) ,Gtot(5,5)/
     +       258.d0    ,20.0d0    ,200.d0    ,0d0       ,0d0      /
        DATA Gtot(6,1) ,Gtot(6,2) ,Gtot(6,3) ,Gtot(6,4) ,Gtot(6,5)/
     +       258.d0    ,20.0d0    ,200.d0    ,0d0       ,0d0      /

C Initialize the Wavefunction squared array:
C (For the S wave (i=1) the wavefunction squared in the origin
C is given. For the P waves (i=2,3,4) the square of the first
C derivative in the origin is given. For the D wave the square
C of the second derivative in the origin is given.)
        DATA Wf2(1,1) ,Wf2(1,2) ,Wf2(1,3) ,Wf2(1,4) ,Wf2(1,5)/
     +       0.074d0  ,0.074d0  ,0.074d0  ,0.81d0   ,6.5d0   /
        DATA Wf2(2,1) ,Wf2(2,2) ,Wf2(2,3) ,Wf2(2,4) ,Wf2(2,5)/
     +       3.5d-3   ,3.5d-3   ,3.5d-3   ,0.075d0  ,1.4d0   /
        DATA Wf2(3,1) ,Wf2(3,2) ,Wf2(3,3) ,Wf2(3,4) ,Wf2(3,5)/
     +       3.5d-3   ,3.5d-3   ,3.5d-3   ,0.075d0  ,1.4d0   /
        DATA Wf2(4,1) ,Wf2(4,2) ,Wf2(4,3) ,Wf2(4,4) ,Wf2(4,5)/
     +       3.5d-3   ,3.5d-3   ,3.5d-3   ,0.075d0  ,1.4d0   /
        DATA Wf2(5,1) ,Wf2(5,2) ,Wf2(5,3) ,Wf2(5,4) ,Wf2(5,5)/
     +       0.35d-3  ,0.35d-3  ,0.35d-3  ,0.015    ,0.64    /
        DATA Wf2(6,1) ,Wf2(6,2) ,Wf2(6,3) ,Wf2(6,4) ,Wf2(6,5)/
     +       0.074d0  ,0.074d0  ,0.074d0  ,0.53d0   ,3.2d0   /

C Initialize the MixingAngle array (in degrees):
        DATA MixAng(1),MixAng(2),MixAng(3),MixAng(4),MixAng(5)/
     +       -11.5d0  ,32d0     ,32d0     ,32d0     ,0d0      /
        Data MixAng(6) /0d0/


C Assign name
        pn = Name(i,j)
C Determine the mass of the resonance:
        M=Mass(i,j)
C Convert the mass into GeV:
        M=M/1000d0
C Determine the mixing angle for the resonance:
        MixingAngle=MixAng(i)
C Convert the mixing angle into radians:
        MixingAngle=MixingAngle*2.0d0*pi/360.0d0

C Calculate eq2 from the mixingangle:
        IF(j.EQ.1) eq2=1/3.0d0/DSQRT(2.0d0)
        IF(j.EQ.2) eq2=(DCOS(MixingAngle)-2.0d0*DSQRT(2.0d0)*
     +                 DSIN(MixingAngle))/3.0d0/DSQRT(6.0d0)
        IF(j.EQ.3) eq2=(DCOS(MixingAngle)+1/2.0d0/DSQRT(2.0d0)*
     +                 DSIN(MixingAngle))*2.0d0/3.0d0/DSQRT(3.0d0)
        IF(j.EQ.4) eq2=4.0d0/9.0d0
        IF(j.EQ.5) eq2=1.0d0/9.0d0
C Correct for f_2 and f_2', etc.:
        IF(i.eq.3.or.i.eq.4) THEN
           IF(j.EQ.2) eq2=(DCOS(MixingAngle)+1/2.0d0/DSQRT(2.0d0)*
     +                 DSIN(MixingAngle))*2.0d0/3.0d0/DSQRT(3.0d0)
           IF(j.EQ.3) eq2=(DCOS(MixingAngle)-2.0d0*DSQRT(2.0d0)*
     +                 DSIN(MixingAngle))/3.0d0/DSQRT(6.0d0)
        ENDIF

C Calculate the GammaGammaWidth from the Mass, eq2 and Wf2:
        IF(i.EQ.1 .or. i.eq.6) 
     &       G=3.0d0*eq2**2*4.0d0*alpha**2*Wf2(i,j)/M**2
        IF(i.EQ.2) G=3.0d0*eq2**2*144.0d0*alpha**2*Wf2(i,j)/M**4
        IF(i.EQ.3) G=2.0d0/9.0d0*(3.0d0*eq2**2*144.0d0*alpha**2*
     +               Wf2(i,j)/M**4)
        IF(i.EQ.4) G=4.0d0/15.0d0*(3.0d0*eq2**2*144.0d0*alpha**2*
     +               Wf2(i,j)/M**4)
        IF(i.EQ.5) G=3.0d0*eq2**2*64.0d0*alpha**2*Wf2(i,j)/M**6

C Include O(alphas) QCD corrections for 0-, 0+, 2+
        If(j.ge.4) then
           If(j.eq.4) then
              alphas = 0.298
           Else
              alphas = 0.190
           Endif
           If(i.eq.1 .or. i.eq.6) 
     &          G = G * (1d0+alphas/Pi*(    Pi**2-20d0)/3d0)
           If(i.eq.2) G = G * (1d0+alphas/Pi*(3d0*Pi**2-28d0)/9d0)
           If(i.eq.4) G = G * (1d0+alphas/Pi*(-16d0)/3d0)
        Endif


C Calculate the total width for charmonia and bottomonia
        If(j.le.3) then
           Gt = Gtot(i,j)/1d3   ! in GeV
        Else
           If(j.eq.4) then
              alphas = 0.298
           Else
              alphas = 0.190
           Endif
           IF(i.EQ.1 .or. i.eq.6) 
     &          gt=(2d0/3d0) *  4.0d0*alphas**2*Wf2(i,j)/M**2
           If(i.eq.2) gt=(2d0/3d0) *144.0d0*alphas**2*Wf2(i,j)/M**4
           If(i.eq.3) gt=(128d0/3d0/Pi)    *alphas**3*Wf2(i,j)/M**4
     &                   *dlog(M/0.5d0)           
           If(i.eq.4) gt=(8d0/45d0)*144.0d0*alphas**2*Wf2(i,j)/M**4
           If(i.eq.5) gt=(2d0/3d0) * 64.0d0*alphas**2*Wf2(i,j)/M**6
        Endif

C Calculate the pole mass
        If(i.eq.1 .and. j.le.3) then
           Pm = 0.7685d0
        Else
           Pm = M
        Endif


C Scale light mesons
        If(j.le.3) then
           If(i.eq.1 .or. i.eq.6) G=G*(M/mu)**5
           If(i.eq.2) G=G*(M/mu)**3
           If(i.eq.3) G=G*(M/mu)**3
           If(i.eq.4) G=G*(M/mu)**3
           If(i.eq.5) G=G*(M/mu)**9
        Endif

C Return to the previous procedure:
        RETURN
        END                     ! SUBROUTINE InitMassWidth 
