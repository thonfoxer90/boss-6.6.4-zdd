C23456789012345678901234567890123456789012345678901234567890123456789012
C        1         2         3         4         5         6         7
*
* Demonstration program for the use of GALUGA
* Author G.A. Schuler, CERN-TH
* October 3, 1997
*
* Stored in ~/public/galuga2/demo2.f
* Run on cernsp with: 
* f77 -g demo1.f galuga.o vegas.o ranlux.o -qextname `cernlib packlib`
*
* GALUGA calculates, using exact kinematics, the process
*   e(+) +  e(-) -> e(+) +  e(-) + hadrons.
* Various cross sections for gamma gamma -> hadrons available
*       
      Program Main
      Implicit None
      Logical Flag
      Character*51 filen,filel
      Double Precision mntum
      Double Precision rs,W,cross,error,Fmax,Fmin,Weight,
     &     th1min,th1max,E1min,E1max,th2min,th2max,E2min,E2max,
     &     xl,xu,acc,
     &     Wmin,Wmax,t2user,t2umin,t2umax
      Double Precision delta,Qmin,Qmax,Lambda,Nf,cQ,cmu
      Integer iapprx,imodel,i,Nev,npts,nzero,ntrial,
     &        ndim,nfcall,itmx,nprn,ivegas,iwaght
      Integer iz1,iz2,iz3,iz4,iz5
      Integer ibatch,n15
      Real Time1,Time2
      Integer imodpi
      Integer iseed 
      Common /gginitial/ iseed
      Common /ggLmodpi/ imodpi
      Common /ggLcut/th1min,th1max,E1min,E1max,th2min,th2max,E2min,E2max
      Common /ggLapp/Wmin,Wmax,t2user,t2umin,t2umax,iapprx,ivegas,iwaght
      Common /ggLmod/ imodel
      Common /ggLuno/ cross,error,Fmax,Fmin,Weight,npts,nzero,ntrial
      Common /ggLvg1/ xl(10),xu(10),acc,ndim,nfcall,itmx,nprn
      Common /izahl /iz1,iz2,iz3,iz4,iz5
      Common /ggBFKL/ delta,Qmin,Qmax,Lambda,Nf,cQ,cmu
      Common /ggLvec/ mntum(9,5)
      Integer ista
      Integer iw
      Double Precision fw
      Double Precision Ppipl,Ppim,thetapipl,thetapim,check1,check2
      Common /pipid/   Ppipl,Ppim,thetapipl,thetapim,check1, check2
 

c      open (18,file="galuga.out",type="new")

*
* File with input data and plotting file
*
      imodpi = 2 ! pions
      ibatch = 1                ! for batch jobs
      If(ibatch.eq.0) then
         n15 = 15
         OPEN(n15,
     &        FILE=
     &        '/home/jemtchou/Desktop/galuga/pipi.in',
     &        STATUS='unknown',FORM='formatted')
      Else
         n15 = 5
      Endif
      read(n15,240) filel
      Write( 6,251) filel
*      call hrOPEN(18,'qq',filel,'n',4096,ista)
*
*      OPEN(18,FILE=filel,STATUS='unknown',FORM='unformatted',
*     &     Recl=4096,Access='direct')
                 
* Read in parameters, cm energy, hadronic energy, and user cuts
*
      print *,'0'
      Read(n15,*) iseed
      Read(n15,*) ivegas,iwaght,imodpi
      print *,'1'
      Read(n15,*) Nev,nfcall,npts
      print *,'2'
c      Read(n15,*) iapprx,imodel,t2umin,t2umax
			Read(n15,*) iapprx,imodel,t2umin,t2umax,t2user
c      Read(n15,*) rs,Wmin,Wmax
			Read(n15,*) rs,Wmin,Wmax,W
      Read(n15,*) th1min,th1max,E1min,E1max
      Read(n15,*) th2min,th2max,E2min,E2max
      print *,'7'
      If(imodel.eq.30) Read(n15,*) delta,Qmin,Qmax,Lambda,Nf
*
* Initialize the random number generator RanLux
      Call rLuxGo(3,iseed,0,0)
*
* Initialize GALUGA; get luminosity within cuts
c 		print *,"W ",W 
c			W = 2d0 
      Call ggLcrs(rs,W)
*
*
      Write(6,2002) iz1,iz2,iz3,iz4,iz5
 2002 Format(/,3x/'iz values',5I7,/)
*
* Initialize plotting
      Call User(0, filel,0)
*
* For safety:
      Fmax = Fmax*2.3d0
* Timing:
*      Call rLuxGo(3,324159265,0,0)
			Call rLuxGo(3,iseed,0,0)
      Call Timex(time1)
*
* Event loop
      Do 10 i=1,Nev
         Call ggLgen(Flag)
         If(.not.Flag) Write(6,*) 'Caution: new maximum'
*
* Calculate 4-momenta
         Call ggLmom
*
* Display first 3 events
         If(i.le.5) call ggLprt

c	 write(18,*),i;
c	 write(18,252) -11, mntum(5,1),  mntum(5,2),  mntum(5,3), mntum(5,4)
c	 write(18,252) 11, mntum(6,1),  mntum(6,2),  mntum(6,3), mntum(6,4)
c	 write(18,252)  211, mntum(8,1),  mntum(8,2),  mntum(8,3), mntum(8,4)
c	 write(18,252)  -211, mntum(9,1),  mntum(9,2),  mntum(9,3), mntum(9,4)

*
* Fill histrograms
         Call User(1,filel,i)
 10   Continue
*
      Call Timex(Time2)
      Write(6,300) Nev,Time2-Time1,(Time2-Time1)/real(Nev),
     &     iwaght,ntrial,nzero,Fmax
*
* Finalize plotting
      Call User(-Nev,filel,0)

 240  Format(A51)

 251  Format(/,'plot file for figures ',A51)

 252  Format(I8,' ',E15.6,' ',E15.6,' ',E15.6,' ',E15.6)

 300  Format(/,3x,'time to generate ',I8,' events is       ',E12.5,/,
     &3x,'resulting in an average time per event of ',E12.5,/,
     &3x,'unweighted events requested if 1:         ',I8,/,
     &3x,'the number of trials was:                 ',I8,/,
     &3x,'the number of zero f was:                 ',I8,/,
     &3x,'the (new) maximum f value was:            ',E12.5)
*

c      close(18)

      Stop
      End
*
*
**************************************************************
*                                                                 
      Subroutine User(n,filel,ievent)
* 
* Plotting with HBOOK
* n=0 : Define    histograms
* n>0 : Fill      histograms         
* n<0 : Normalize histograms
*
      Implicit None
      Integer ievent
      Character*51 filel
      Integer n,icycle,iapprx,ja,il,npts,nzero,ntrial,ivegas,iwaght,i
			Integer imodpi
      Integer inc
      Integer ista
      Double Precision s,rs,W,m,Pi,alem
      Double Precision mntum
      Double Precision xA,xB,phtn
      Double Precision Wmin,Wmax,t2user,t2umin,t2umax
      Double Precision cross,error,Fmax,Fmin,Weight
      Double Precision 
     &     X,t2,t1,s1,s2,E1,E2,EX,P1,P2,PX,th1,th2,thX,phi1,phi2,phi,pht
		 	Common /ggLmodpi/ imodpi
      Common /ggLvar/ 
     & X(10),t2,t1,s1,s2,E1,E2,EX,P1,P2,PX,th1,th2,thX,phi1,phi2,phi,pht
      Common /ggLprm/ s,rs,W,m,Pi,alem
      Common /ggLvec/ mntum(9,5)
      Common /ggLapp/Wmin,Wmax,t2user,t2umin,t2umax,iapprx,ivegas,iwaght
      Common /ggLuno/ cross,error,Fmax,Fmin,Weight,npts,nzero,ntrial

      Double Precision Ppipl,Ppim,thetapipl,thetapim,check1,check2
      Common /pipid/   Ppipl,Ppim,thetapipl,thetapim,check1, check2
      Double precision thgg
      Common /thetagg/ thgg

      REAL HMEMOR,binw1(50),binw2(12),fac,rwgt,rn,wt
      real conten(100),erar(100)
      COMMON /PAWC/ HMEMOR(1000000)
*
      Common /phitst/ phtn
      Data inc/0/
      Save inc
      integer NHEP,PDG

*
      ja = (1+iabs(iapprx))*1000
      If(n.eq.0) then
         open(116,STATUS='unknown',FORM='formatted',FILE='galuga.out')
         CALL HLIMIT(1000000)
         
         call hropen(18,'qq',filel,'n',4096,ista)
         
         call hbnt(1000,'GALUGA','')

         call hbname(1000,'ggEVENT',ievent,"ievent:I*8")
         call hbname(1000,'ggEVENT',mntum(5,1),"eppx:R*8")
         call hbname(1000,'ggEVENT',mntum(5,2),"eppy:R*8")
         call hbname(1000,'ggEVENT',mntum(5,3),"eppz:R*8")
         call hbname(1000,'ggEVENT',mntum(5,4),"epe:R*8")
         call hbname(1000,'ggEVENT',mntum(6,1),"empx:R*8")
         call hbname(1000,'ggEVENT',mntum(6,2),"empy:R*8")
         call hbname(1000,'ggEVENT',mntum(6,3),"empz:R*8")
         call hbname(1000,'ggEVENT',mntum(6,4),"eme:R*8")
         call hbname(1000,'ggEVENT',mntum(8,1),"pppx:R*8")
         call hbname(1000,'ggEVENT',mntum(8,2),"pppy:R*8")
         call hbname(1000,'ggEVENT',mntum(8,3),"pppz:R*8")
         call hbname(1000,'ggEVENT',mntum(8,4),"ppe:R*8")
         call hbname(1000,'ggEVENT',mntum(9,1),"pmpx:R*8")
         call hbname(1000,'ggEVENT',mntum(9,2),"pmpy:R*8")
         call hbname(1000,'ggEVENT',mntum(9,3),"pmpz:R*8")
         call hbname(1000,'ggEVENT',mntum(9,4),"pme:R*8")

c	 write(18,*),i;
c	 write(18,252) -11, mntum(5,1),  mntum(5,2),  mntum(5,3), mntum(5,4)
c	 write(18,252) 11, mntum(6,1),  mntum(6,2),  mntum(6,3), mntum(6,4)
c	 write(18,252)  211, mntum(8,1),  mntum(8,2),  mntum(8,3), mntum(8,4)
c	 write(18,252)  -211, mntum(9,1),  mntum(9,2),  mntum(9,3), mntum(9,4)
c         call hbname(1000,'ggEVENT',E1,"E1:R*8")
c         call hbname(1000,'ggEVENT',E2,"E2:R*8");     
c         call hbname(1000,'ggEVENT',EX,"EX:R*8")                                         
c         call hbname(1000,'ggEVENT',P1,"P1:R*8")
c         call hbname(1000,'ggEVENT',P2,"P2:R*8")
c         call hbname(1000,'ggEVENT',PX,"PX:R*8")
c         call hbname(1000,'ggEVENT',th1,"th1:R*8")
c         call hbname(1000,'ggEVENT',th2,"th2:R*8")
c         call hbname(1000,'ggEVENT',thx,"thx:R*8")
c         call hbname(1000,'ggEVENT',phi1,"phi1:R*8")
c         call hbname(1000,'ggEVENT',phi2,"phi2:R*8")
c         call hbname(1000,'ggEVENT',phi,"phi:R*8")
c         call hbname(1000,'ggEVENT',pht,"pht:R*8")
c         call hbname(1000,'ggEVENT',ppipl,"pip:R*8")
c         call hbname(1000,'ggEVENT',ppim,"pim:R*8")
c         call hbname(1000,'ggEVENT',thetapipl,"thp:R*8")
c         call hbname(1000,'ggEVENT',thetapim,"thm:R*8")
c         call hbname(1000,'ggEVENT',check1,"c1:R*8")
c         call hbname(1000,'ggEVENT',thgg,"thgg:R*8")
c
c         call hbname(1000,'ggEVENT',mntum(7,1),"xx:R*8")
c         call hbname(1000,'ggEVENT',mntum(7,2),"xy:R*8")
c         call hbname(1000,'ggEVENT',mntum(7,3),"xz:R*8")
c         call hbname(1000,'ggEVENT',mntum(8,1),"ppix:R*8")
c         call hbname(1000,'ggEVENT',mntum(8,2),"ppiy:R*8")
c         call hbname(1000,'ggEVENT',mntum(8,3),"ppiz:R*8")
c         call hbname(1000,'ggEVENT',mntum(9,1),"ppmx:R*8")
c         call hbname(1000,'ggEVENT',mntum(9,2),"ppmy:R*8")
c         call hbname(1000,'ggEVENT',mntum(9,3),"ppmz:R*8")
c
         call hbook1(401,'pip',200,0.,2.,0)
         binw1(41)=2.d0/200.
         call hbook1(402,'pim',200,0.,2.,0)
         binw1(42)=2.d0/200.
         call hbook1(403,'thpm',72,0.,180.,0)
         binw1(43)=180./72.
         call hbook1(404,'thpp',72,0.,180.,0)
         binw1(44)=180./72.

         call hbook1(1,'x(1)',72,0.,1.,0)
           binw1(1)=1./72.
         call hbook1(2,'x(2)',72,0.,1.,0)
           binw1(2)=1./72.
         call hbook1(3,'x(3)',72,0.,1.,0)
           binw1(3)=1./72.
         call hbook1(4,'x(4)',72,0.,1.,0)
           binw1(4)=1./72.
         call hbook1(5,'E1',72,0.,4.,0)
           binw1(5)=4./72.
         call hbook1(6,'E2',72,0.,4.,0)
           binw1(6)=4./72.
         call hbook1(7,'th1',72,0.,180.,0)
           binw1(7)=180./72.
         call hbook1(8,'th2',72,0.,180.,0)
           binw1(8)=180./72.
         call hbook1(9,'x(5)',72,0.,1.,0)
           binw1(9)=1./72.
         call hbook1(10,'W',72,0.,7.2,0)
           binw1(10)=7.2/72.
         call hbook1(11,'ln(-s/t1)',72,0.,57.6,0)
           binw1(11)=57.6/72.
         call hbook1(12,'ln(-s/t2)',72,0.,57.6,0)
           binw1(12)=57.6/72.
         call hbook1(13,'-t1',100,0.,0.1,0)
           binw1(13)=0.1/100.
         call hbook1(14,'-t2',100,0.,0.1,0)
           binw1(14)=0.1/100.
         call hbook1(15,'root(s1)',72,0.,144.,0)
           binw1(15)=144./72.
         call hbook1(16,'root(s2)',72,0.,144.,0)
           binw1(16)=144./72.
         call hbook1(17,'-t1',100,0.,5.,0)
           binw1(17)=5./100.
         call hbook1(18,'-t2',100,0.,5.,0)
           binw1(18)=5./100.
         call hbook1(19,'-t1',10,0.,5.,0)
           binw1(19)=5./10.
         call hbook1(20,'-t2',10,0.,5.,0)
           binw1(20)=5./10.
         call hbook1(21,'x(4)',72,0.,2.,0)
           binw1(21)=2./72.
         call hbook1(22,'x(4)',72,0.,0.5,0)
           binw1(22)=0.5/72.
         call hbook1(23,'ln(-t1)',100,-14.,4.,0)
           binw1(23)=18./100.
         call hbook1(24,'ln(-t2)',100,-14.,4.,0)
           binw1(24)=18./100.
*
         call hbook2(101,'root(-t1) vs. root(-t2)',72,0.,1.44,
     &                                   72,0.,1.44,0)
           binw2(1)=(1.44/72.)*(1.44/72.)
         call hbook2(102,'ln(-s/t1) vs. ln(-s/t2)',
     &                                   72,0.,57.6,72,0.,57.6,0)
           binw2(2)=(57.6/72.)*(57.6/72.)
         call hbook2(103,'root s1 vs. root s2',10,0.,4.,10,0.,4.,0)
           binw2(3)=(4./10.)*(4./10.)
         call hbook2(104,
     &        'root s1 vs. ln(-s/t2)',10,0.,4.,72,0.,57.6,0)
           binw2(4)=(4./10.)*(57.6/72.)
         call hbook2(105,
     &        'root s2 vs. ln(-s/t1)',10,0.,4.,72,0.,57.6,0)
           binw2(5)=(4./10.)*(57.6/72.)
         call hbook2(106,'ln(th1) vs. ln(th2)',72,-18.,6.,72,-18.,6.,0)
           binw2(6)=(24./72.)*(24./72.)
         call hbook2(107,'th1 vs. th2',72,0.,180.,72,0.,180.,0)
           binw2(7)=(180./72.)*(180./72.)
         call hbook2(108,'phi1 vs. phi2',72,0.,3.1416,72,0.,3.1416,0)
           binw2(8)=(3.1416/72.)*(3.1416/72.)
         call hbook2(109,'phi vs. pht',72,0.,3.1416,72,0.,3.1416,0)
           binw2(9)=(3.1416/72.)*(3.1416/72.)
         call hbook2(110,'phtn vs. pht',72,0.,3.1416,72,0.,3.1416,0)
           binw2(10)=(3.1416/72.)*(3.1416/72.)
         call hbook2(111,'ln(-t1) vs. ln(-t2)',
     &                                  100,-14.,4.,100,-14.,4.,0)
         binw2(11)=(18./100.)*(18./100.)
*
      Elseif(n.lt.0) then
* Normalize distributions by number of events
* If distributions should add to total cross section
* then need to multiply by bin width
* Here we do not want cross sections per bin but per unit GeV^n etc
c         call hidopt(0,'SHOW')
         rn = 1./real(-n)
         do i=1,24
C            wt = rn*binw1(i)
            wt = rn
            call hopera(i,'+',i,200+i,wt,0.)
         End do
         
         do i=1,4
            wt = rn
            call hopera(400+i,'+',400+i,500+i,wt,0.)
         End do 

*
         do i=101,111
C            wt = rn*binw1(i)
            wt = rn
            call hopera(i,'+',i,200+i,wt,0.)
         End do
*
c         call hunpke(217,conten,'HIST',1)
c         do i=1,72
c            erar(i) = 0.1
c         end do
c         call hpake(217,erar)
*
*         call hidopt(0,'STAT')
*         call hidopt(0,'INTE')
*         call hidopt(0,'SHOW')
*         call hrFile(18,'hexam1','n')
         call hrOut(0,icycle,' ')
         call hrEnd('qq')
         close(116)
*         call histdo
      Else
         call hfnt(1000)
				 If(imodpi.eq.0) then
         	NHEP=3
         	WRITE(116,*) NHEP
c e+
         	PDG=11
         	WRITE(116,10)PDG,mntum(5,1),mntum(5,2),mntum(5,3),mntum(5,4)  
c e-
         	PDG=-11
         	WRITE(116,10)PDG,mntum(6,1),mntum(6,2),mntum(6,3),mntum(6,4)   
c Hadronic system
         	PDG=0
         	WRITE(116,10)PDG,mntum(7,1),mntum(7,2),mntum(7,3),mntum(7,4)
				 Else
				 	NHEP=4
         	WRITE(116,*) NHEP
c e+
         	PDG=11
         	WRITE(116,10)PDG,mntum(5,1),mntum(5,2),mntum(5,3),mntum(5,4)  
c e-
         	PDG=-11
         	WRITE(116,10)PDG,mntum(6,1),mntum(6,2),mntum(6,3),mntum(6,4)   
c pi+
         	PDG=111
         	WRITE(116,10)PDG,mntum(8,1),mntum(8,2),mntum(8,3),mntum(8,4)   
c pi-
         	PDG=111
         	WRITE(116,10)PDG,mntum(9,1),mntum(9,2),mntum(9,3),mntum(9,4)   
				 Endif
C10      FORMAT(I4,' ',D15.8,' ',D15.8,' ',D15.8,' ',D15.8)
10       FORMAT(I8,' ',E15.6,' ',E15.6,' ',E15.6,' ',E15.6)				 
         xB = -t2/(W*W - t2 - t1)
         xA = -t2/(W*W - t2)
         If(iwaght.eq.1) then
            wt = cross*1.E6  ! fb-1
         Else
            wt = Weight
         Endif
         If(dabs(weight-1d0).gt.1d-5) then
            inc = inc + 1
            If(inc.lt.10) Write(6,*) 'weight = ',weight
         Endif

         rwgt = wt/binw1(41)
         call hf1(401,sngl(ppipl),rwgt)
         rwgt = wt/binw1(42)
         call hf1(402,sngl(ppim),rwgt)
         rwgt = wt/binw1(43)
         call hf1(403,sngl(thetapipl*180./3.14159),rwgt)
         rwgt = wt/binw1(44)
         call hf1(404,sngl(thetapim*180./3.14159),rwgt)
         
         rwgt = wt/binw1(1)
         call hf1(1,sngl(x(1)),rwgt)
         rwgt = wt/binw1(2)
         call hf1(2,sngl(X(2)),rwgt)
         rwgt = wt/binw1(3)
         call hf1(3,sngl(X(3)),rwgt)
         rwgt = wt/binw1(4)
         call hf1(4,sngl(X(4)),rwgt)
         rwgt = wt/binw1(5)
         call hf1(5,sngl(E1),rwgt)
         rwgt = wt/binw1(6)
         call hf1(6,sngl(E2),rwgt)
         rwgt = wt/binw1(7)
         call hf1(7,sngl(th1),rwgt)
         rwgt = wt/binw1(8)
         call hf1(8,sngl(th2),rwgt)
         rwgt = wt/binw1(9)
         call hf1(9,sngl(X(5)),rwgt)
         rwgt = wt/binw1(10)
         call hf1(10,sngl(W),rwgt)
         rwgt = wt/binw1(11)
         call hf1(11,sngl(dlog(-s/t1)),rwgt)
         rwgt = wt/binw1(12)
         call hf1(12,sngl(dlog(-s/t2)),rwgt)
         rwgt = wt/binw1(13)
         call hf1(13,sngl(-t1),rwgt)
         rwgt = wt/binw1(14)
         call hf1(14,sngl(-t2),rwgt)
         rwgt = wt/binw1(15)
         call hf1(15,sngl(dmax1(-1D10,dmin1(1D10,dsqrt(s1)))),rwgt)
         rwgt = wt/binw1(16)
         call hf1(16,sngl(dmax1(-1D10,dmin1(1D10,dsqrt(s2)))),rwgt)
         rwgt = wt/binw1(17)
         call hf1(17,sngl(-t1),rwgt)
         rwgt = wt/binw1(18)
         call hf1(18,sngl(-t2),rwgt)
         rwgt = wt/binw1(19)
         call hf1(19,sngl(-t1),rwgt)
         rwgt = wt/binw1(20)
         call hf1(20,sngl(-t2),rwgt)
         rwgt = wt/binw1(21)
         call hf1(21,sngl(X(4)),rwgt)
         rwgt = wt/binw1(22)
         call hf1(22,sngl(X(4)),rwgt)
         rwgt = wt/binw1(23)/(-t1)/alog(10.)
         call hf1(23,sngl(dlog10(-t1)),rwgt)
         rwgt = wt/binw1(24)/(-t2)/alog(10.)
         call hf1(24,sngl(dlog10(-t2)),rwgt)
*
         rwgt = wt/binw2(1)
         call hf2(101,
     &        sngl(dmax1(-1D10,dmin1(1D10,dsqrt(-t1)))),
     &        sngl(dmax1(-1D10,dmin1(1D10,dsqrt(-t2)))),rwgt)
         rwgt = wt/binw2(2)
         call hf2(102,
     &        sngl(dmax1(-1D10,dmin1(1D10,dlog(-s/t1)))),
     &        sngl(dmax1(-1D10,dmin1(1D10,dlog(-s/t2)))),rwgt)
         rwgt = wt/binw2(3)
         call hf2(103,
     &        sngl(dmax1(-1D10,dmin1(1D10,dsqrt(s1)))),
     &        sngl(dmax1(-1D10,dmin1(1D10,dsqrt(s2)))),rwgt)
         rwgt = wt/binw2(4)
         call hf2(104,
     &        sngl(dmax1(-1D10,dmin1(1D10,dsqrt(s1)))),
     &        sngl(dmax1(-1D10,dmin1(1D10,dlog(-s/t2)))),rwgt)
         rwgt = wt/binw2(5)
         call hf2(105,
     &        sngl(dmax1(-1D10,dmin1(1D10,dsqrt(s2)))),
     &        sngl(dmax1(-1D10,dmin1(1D10,dlog(-s/t1)))),rwgt)
         rwgt = wt/binw2(6)
         call hf2(106,
     &        sngl(dlog(dmax1(1d-15,th1))),
     &        sngl(dlog(dmax1(1d-15,th2))),rwgt)
         rwgt = wt/binw2(7)
         call hf2(107,sngl(th1),sngl(th2),rwgt)
         rwgt = wt/binw2(8)
         call hf2(108,sngl(phi1),sngl(phi2),rwgt)
         rwgt = wt/binw2(9)
         call hf2(109,sngl(phi),sngl(pht),rwgt)
         rwgt = wt/binw2(10)
         call hf2(110,sngl(phtn),sngl(pht),rwgt)
         rwgt = wt/binw2(11)
         call hf2(111,
     &        sngl(dlog10(-t1)),
     &        sngl(dlog10(-t2)),rwgt)
      Endif
*
      Return
      End
