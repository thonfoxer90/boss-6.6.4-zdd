#-- start of make_header -----------------

#====================================
#  Library Babayaga
#
#   Generated Tue Aug 30 16:02:21 2016  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_Babayaga_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_Babayaga_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_Babayaga

Babayaga_tag = $(tag)

#cmt_local_tagfile_Babayaga = $(Babayaga_tag)_Babayaga.make
cmt_local_tagfile_Babayaga = $(bin)$(Babayaga_tag)_Babayaga.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Babayaga_tag = $(tag)

#cmt_local_tagfile_Babayaga = $(Babayaga_tag).make
cmt_local_tagfile_Babayaga = $(bin)$(Babayaga_tag).make

endif

include $(cmt_local_tagfile_Babayaga)
#-include $(cmt_local_tagfile_Babayaga)

ifdef cmt_Babayaga_has_target_tag

cmt_final_setup_Babayaga = $(bin)setup_Babayaga.make
#cmt_final_setup_Babayaga = $(bin)Babayaga_Babayagasetup.make
cmt_local_Babayaga_makefile = $(bin)Babayaga.make

else

cmt_final_setup_Babayaga = $(bin)setup.make
#cmt_final_setup_Babayaga = $(bin)Babayagasetup.make
cmt_local_Babayaga_makefile = $(bin)Babayaga.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Babayagasetup.make

#Babayaga :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'Babayaga'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = Babayaga/
#Babayaga::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

Babayagalibname   = $(bin)$(library_prefix)Babayaga$(library_suffix)
Babayagalib       = $(Babayagalibname).a
Babayagastamp     = $(bin)Babayaga.stamp
Babayagashstamp   = $(bin)Babayaga.shstamp

Babayaga :: dirs  BabayagaLIB
	$(echo) "Babayaga ok"

#-- end of libary_header ----------------

BabayagaLIB :: $(Babayagalib) $(Babayagashstamp)
	@/bin/echo "------> Babayaga : library ok"

$(Babayagalib) :: $(bin)babayaga_3.5.o $(bin)babayaga_submain.o $(bin)BabayagaRandom.o $(bin)Babayaga.o $(bin)Babayaga_load.o $(bin)Babayaga_entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(Babayagalib) $?
	$(lib_silent) $(ranlib) $(Babayagalib)
	$(lib_silent) cat /dev/null >$(Babayagastamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(Babayagalibname).$(shlibsuffix) :: $(Babayagalib) $(Babayagastamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" Babayaga $(Babayaga_shlibflags)

$(Babayagashstamp) :: $(Babayagalibname).$(shlibsuffix)
	@if test -f $(Babayagalibname).$(shlibsuffix) ; then cat /dev/null >$(Babayagashstamp) ; fi

Babayagaclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)babayaga_3.5.o $(bin)babayaga_submain.o $(bin)BabayagaRandom.o $(bin)Babayaga.o $(bin)Babayaga_load.o $(bin)Babayaga_entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
Babayagainstallname = $(library_prefix)Babayaga$(library_suffix).$(shlibsuffix)

Babayaga :: Babayagainstall

install :: Babayagainstall

Babayagainstall :: $(install_dir)/$(Babayagainstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(Babayagainstallname) :: $(bin)$(Babayagainstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(Babayagainstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(Babayagainstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(Babayagainstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(Babayagainstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(Babayagainstallname) $(install_dir)/$(Babayagainstallname); \
	      echo `pwd`/$(Babayagainstallname) >$(install_dir)/$(Babayagainstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(Babayagainstallname), no installation directory specified"; \
	  fi; \
	fi

Babayagaclean :: Babayagauninstall

uninstall :: Babayagauninstall

Babayagauninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(Babayagainstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(Babayagainstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(Babayagainstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(Babayagainstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),Babayagaclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)Babayaga_dependencies.make :: dirs

ifndef QUICK
$(bin)Babayaga_dependencies.make : $(src)Babayaga_3.5/babayaga_3.5.f $(src)Babayaga_3.5/babayaga_submain.f $(src)BabayagaRandom.cxx $(src)Babayaga.cxx $(src)components/Babayaga_load.cxx $(src)components/Babayaga_entries.cxx $(use_requirements) $(cmt_final_setup_Babayaga)
	$(echo) "(Babayaga.make) Rebuilding $@"; \
	  $(build_dependencies) Babayaga -all_sources -out=$@ $(src)Babayaga_3.5/babayaga_3.5.f $(src)Babayaga_3.5/babayaga_submain.f $(src)BabayagaRandom.cxx $(src)Babayaga.cxx $(src)components/Babayaga_load.cxx $(src)components/Babayaga_entries.cxx
endif

#$(Babayaga_dependencies)

-include $(bin)Babayaga_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of fortran_library ------

$(bin)Babayaga_dependencies.make : $(babayaga_3.5_f_dependencies)
 
$(bin)$(binobj)babayaga_3.5.o : $(babayaga_3.5_f_dependencies)
	$(fortran_echo) $(src)Babayaga_3.5/babayaga_3.5.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(Babayaga_pp_fflags) $(lib_Babayaga_pp_fflags) $(babayaga_3.5_pp_fflags) $(use_fflags) $(Babayaga_fflags) $(lib_Babayaga_fflags) $(babayaga_3.5_fflags) $(babayaga_3.5_f_fflags) $(ppcmd)../src/Babayaga_3.5 $(src)Babayaga_3.5/babayaga_3.5.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)babayaga_3.5.o $(use_pp_fflags) $(Babayaga_pp_fflags) $(lib_Babayaga_pp_fflags) $(babayaga_3.5_pp_fflags) $(use_fflags) $(Babayaga_fflags) $(lib_Babayaga_fflags) $(babayaga_3.5_fflags) $(babayaga_3.5_f_fflags) $(ppcmd)../src/Babayaga_3.5 $(src)Babayaga_3.5/babayaga_3.5.f

#-- end of fortran_library ------
#-- start of fortran_library ------

$(bin)Babayaga_dependencies.make : $(babayaga_submain_f_dependencies)
 
$(bin)$(binobj)babayaga_submain.o : $(babayaga_submain_f_dependencies)
	$(fortran_echo) $(src)Babayaga_3.5/babayaga_submain.f
	$(fortran_silent) $(fcomp) -o $@ $(use_pp_fflags) $(Babayaga_pp_fflags) $(lib_Babayaga_pp_fflags) $(babayaga_submain_pp_fflags) $(use_fflags) $(Babayaga_fflags) $(lib_Babayaga_fflags) $(babayaga_submain_fflags) $(babayaga_submain_f_fflags) $(ppcmd)../src/Babayaga_3.5 $(src)Babayaga_3.5/babayaga_submain.f
#	$(fortran_silent)cd $(bin); $(fcomp) -o $(binobj)babayaga_submain.o $(use_pp_fflags) $(Babayaga_pp_fflags) $(lib_Babayaga_pp_fflags) $(babayaga_submain_pp_fflags) $(use_fflags) $(Babayaga_fflags) $(lib_Babayaga_fflags) $(babayaga_submain_fflags) $(babayaga_submain_f_fflags) $(ppcmd)../src/Babayaga_3.5 $(src)Babayaga_3.5/babayaga_submain.f

#-- end of fortran_library ------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Babayagaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BabayagaRandom.d

$(bin)$(binobj)BabayagaRandom.d : $(use_requirements) $(cmt_final_setup_Babayaga)

$(bin)$(binobj)BabayagaRandom.d : $(src)BabayagaRandom.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BabayagaRandom.dep $(use_pp_cppflags) $(Babayaga_pp_cppflags) $(lib_Babayaga_pp_cppflags) $(BabayagaRandom_pp_cppflags) $(use_cppflags) $(Babayaga_cppflags) $(lib_Babayaga_cppflags) $(BabayagaRandom_cppflags) $(BabayagaRandom_cxx_cppflags)  $(src)BabayagaRandom.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BabayagaRandom.o $(src)BabayagaRandom.cxx $(@D)/BabayagaRandom.dep
endif
endif

$(bin)$(binobj)BabayagaRandom.o : $(src)BabayagaRandom.cxx
else
$(bin)Babayaga_dependencies.make : $(BabayagaRandom_cxx_dependencies)

$(bin)$(binobj)BabayagaRandom.o : $(BabayagaRandom_cxx_dependencies)
endif
	$(cpp_echo) $(src)BabayagaRandom.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Babayaga_pp_cppflags) $(lib_Babayaga_pp_cppflags) $(BabayagaRandom_pp_cppflags) $(use_cppflags) $(Babayaga_cppflags) $(lib_Babayaga_cppflags) $(BabayagaRandom_cppflags) $(BabayagaRandom_cxx_cppflags)  $(src)BabayagaRandom.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Babayagaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Babayaga.d

$(bin)$(binobj)Babayaga.d : $(use_requirements) $(cmt_final_setup_Babayaga)

$(bin)$(binobj)Babayaga.d : $(src)Babayaga.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Babayaga.dep $(use_pp_cppflags) $(Babayaga_pp_cppflags) $(lib_Babayaga_pp_cppflags) $(Babayaga_pp_cppflags) $(use_cppflags) $(Babayaga_cppflags) $(lib_Babayaga_cppflags) $(Babayaga_cppflags) $(Babayaga_cxx_cppflags)  $(src)Babayaga.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Babayaga.o $(src)Babayaga.cxx $(@D)/Babayaga.dep
endif
endif

$(bin)$(binobj)Babayaga.o : $(src)Babayaga.cxx
else
$(bin)Babayaga_dependencies.make : $(Babayaga_cxx_dependencies)

$(bin)$(binobj)Babayaga.o : $(Babayaga_cxx_dependencies)
endif
	$(cpp_echo) $(src)Babayaga.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Babayaga_pp_cppflags) $(lib_Babayaga_pp_cppflags) $(Babayaga_pp_cppflags) $(use_cppflags) $(Babayaga_cppflags) $(lib_Babayaga_cppflags) $(Babayaga_cppflags) $(Babayaga_cxx_cppflags)  $(src)Babayaga.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Babayagaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Babayaga_load.d

$(bin)$(binobj)Babayaga_load.d : $(use_requirements) $(cmt_final_setup_Babayaga)

$(bin)$(binobj)Babayaga_load.d : $(src)components/Babayaga_load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Babayaga_load.dep $(use_pp_cppflags) $(Babayaga_pp_cppflags) $(lib_Babayaga_pp_cppflags) $(Babayaga_load_pp_cppflags) $(use_cppflags) $(Babayaga_cppflags) $(lib_Babayaga_cppflags) $(Babayaga_load_cppflags) $(Babayaga_load_cxx_cppflags) -I../src/components $(src)components/Babayaga_load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Babayaga_load.o $(src)components/Babayaga_load.cxx $(@D)/Babayaga_load.dep
endif
endif

$(bin)$(binobj)Babayaga_load.o : $(src)components/Babayaga_load.cxx
else
$(bin)Babayaga_dependencies.make : $(Babayaga_load_cxx_dependencies)

$(bin)$(binobj)Babayaga_load.o : $(Babayaga_load_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/Babayaga_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Babayaga_pp_cppflags) $(lib_Babayaga_pp_cppflags) $(Babayaga_load_pp_cppflags) $(use_cppflags) $(Babayaga_cppflags) $(lib_Babayaga_cppflags) $(Babayaga_load_cppflags) $(Babayaga_load_cxx_cppflags) -I../src/components $(src)components/Babayaga_load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Babayagaclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Babayaga_entries.d

$(bin)$(binobj)Babayaga_entries.d : $(use_requirements) $(cmt_final_setup_Babayaga)

$(bin)$(binobj)Babayaga_entries.d : $(src)components/Babayaga_entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Babayaga_entries.dep $(use_pp_cppflags) $(Babayaga_pp_cppflags) $(lib_Babayaga_pp_cppflags) $(Babayaga_entries_pp_cppflags) $(use_cppflags) $(Babayaga_cppflags) $(lib_Babayaga_cppflags) $(Babayaga_entries_cppflags) $(Babayaga_entries_cxx_cppflags) -I../src/components $(src)components/Babayaga_entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Babayaga_entries.o $(src)components/Babayaga_entries.cxx $(@D)/Babayaga_entries.dep
endif
endif

$(bin)$(binobj)Babayaga_entries.o : $(src)components/Babayaga_entries.cxx
else
$(bin)Babayaga_dependencies.make : $(Babayaga_entries_cxx_dependencies)

$(bin)$(binobj)Babayaga_entries.o : $(Babayaga_entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/Babayaga_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Babayaga_pp_cppflags) $(lib_Babayaga_pp_cppflags) $(Babayaga_entries_pp_cppflags) $(use_cppflags) $(Babayaga_cppflags) $(lib_Babayaga_cppflags) $(Babayaga_entries_cppflags) $(Babayaga_entries_cxx_cppflags) -I../src/components $(src)components/Babayaga_entries.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: Babayagaclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(Babayaga.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(Babayaga.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(Babayaga.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(Babayaga.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_Babayaga)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Babayaga.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Babayaga.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Babayaga.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

Babayagaclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library Babayaga
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)Babayaga$(library_suffix).a $(library_prefix)Babayaga$(library_suffix).s? Babayaga.stamp Babayaga.shstamp
#-- end of cleanup_library ---------------
