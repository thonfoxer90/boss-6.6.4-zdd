//====================================================================
//  BABAYAGA_entries.cxx
//--------------------------------------------------------------------
//
//  Package    : Generator/Babayaga
//
//  Description: Declaration of the components (factory entries)
//               specified by this component library.
//
//====================================================================

#include "Babayaga/Babayaga.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_ALGORITHM_FACTORY( Babayaga )

DECLARE_FACTORY_ENTRIES( Babayaga ) {
    DECLARE_ALGORITHM( Babayaga );
}

