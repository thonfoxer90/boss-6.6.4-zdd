if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cluster/bes3/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh
set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt cleanup -csh -pack=Babayaga -version=Babayaga-00-00-24 -path=/cluster/bes3/dist/6.6.4.p01/Generator $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

