#-- start of make_header -----------------

#====================================
#  Library BabayagaLib
#
#   Generated Tue Aug 30 17:38:50 2016  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_BabayagaLib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_BabayagaLib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_BabayagaLib

Babayaga_tag = $(tag)

#cmt_local_tagfile_BabayagaLib = $(Babayaga_tag)_BabayagaLib.make
cmt_local_tagfile_BabayagaLib = $(bin)$(Babayaga_tag)_BabayagaLib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Babayaga_tag = $(tag)

#cmt_local_tagfile_BabayagaLib = $(Babayaga_tag).make
cmt_local_tagfile_BabayagaLib = $(bin)$(Babayaga_tag).make

endif

include $(cmt_local_tagfile_BabayagaLib)
#-include $(cmt_local_tagfile_BabayagaLib)

ifdef cmt_BabayagaLib_has_target_tag

cmt_final_setup_BabayagaLib = $(bin)setup_BabayagaLib.make
#cmt_final_setup_BabayagaLib = $(bin)Babayaga_BabayagaLibsetup.make
cmt_local_BabayagaLib_makefile = $(bin)BabayagaLib.make

else

cmt_final_setup_BabayagaLib = $(bin)setup.make
#cmt_final_setup_BabayagaLib = $(bin)Babayagasetup.make
cmt_local_BabayagaLib_makefile = $(bin)BabayagaLib.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Babayagasetup.make

#BabayagaLib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'BabayagaLib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = BabayagaLib/
#BabayagaLib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

BabayagaLiblibname   = $(bin)$(library_prefix)BabayagaLib$(library_suffix)
BabayagaLiblib       = $(BabayagaLiblibname).a
BabayagaLibstamp     = $(bin)BabayagaLib.stamp
BabayagaLibshstamp   = $(bin)BabayagaLib.shstamp

BabayagaLib :: dirs  BabayagaLibLIB
	$(echo) "BabayagaLib ok"

#-- end of libary_header ----------------

BabayagaLibLIB :: $(BabayagaLiblib) $(BabayagaLibshstamp)
	@/bin/echo "------> BabayagaLib : library ok"

$(BabayagaLiblib) :: $(bin)Babayaga.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(BabayagaLiblib) $?
	$(lib_silent) $(ranlib) $(BabayagaLiblib)
	$(lib_silent) cat /dev/null >$(BabayagaLibstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(BabayagaLiblibname).$(shlibsuffix) :: $(BabayagaLiblib) $(BabayagaLibstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" BabayagaLib $(BabayagaLib_shlibflags)

$(BabayagaLibshstamp) :: $(BabayagaLiblibname).$(shlibsuffix)
	@if test -f $(BabayagaLiblibname).$(shlibsuffix) ; then cat /dev/null >$(BabayagaLibshstamp) ; fi

BabayagaLibclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)Babayaga.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
BabayagaLibinstallname = $(library_prefix)BabayagaLib$(library_suffix).$(shlibsuffix)

BabayagaLib :: BabayagaLibinstall

install :: BabayagaLibinstall

BabayagaLibinstall :: $(install_dir)/$(BabayagaLibinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(BabayagaLibinstallname) :: $(bin)$(BabayagaLibinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(BabayagaLibinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(BabayagaLibinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaLibinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaLibinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(BabayagaLibinstallname) $(install_dir)/$(BabayagaLibinstallname); \
	      echo `pwd`/$(BabayagaLibinstallname) >$(install_dir)/$(BabayagaLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(BabayagaLibinstallname), no installation directory specified"; \
	  fi; \
	fi

BabayagaLibclean :: BabayagaLibuninstall

uninstall :: BabayagaLibuninstall

BabayagaLibuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(BabayagaLibinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaLibinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BabayagaLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(BabayagaLibinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),BabayagaLibclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)BabayagaLib_dependencies.make :: dirs

ifndef QUICK
$(bin)BabayagaLib_dependencies.make : $(src)Babayaga.cxx $(use_requirements) $(cmt_final_setup_BabayagaLib)
	$(echo) "(BabayagaLib.make) Rebuilding $@"; \
	  $(build_dependencies) BabayagaLib -all_sources -out=$@ $(src)Babayaga.cxx
endif

#$(BabayagaLib_dependencies)

-include $(bin)BabayagaLib_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BabayagaLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Babayaga.d

$(bin)$(binobj)Babayaga.d : $(use_requirements) $(cmt_final_setup_BabayagaLib)

$(bin)$(binobj)Babayaga.d : $(src)Babayaga.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Babayaga.dep $(use_pp_cppflags) $(BabayagaLib_pp_cppflags) $(lib_BabayagaLib_pp_cppflags) $(Babayaga_pp_cppflags) $(use_cppflags) $(BabayagaLib_cppflags) $(lib_BabayagaLib_cppflags) $(Babayaga_cppflags) $(Babayaga_cxx_cppflags)  $(src)Babayaga.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Babayaga.o $(src)Babayaga.cxx $(@D)/Babayaga.dep
endif
endif

$(bin)$(binobj)Babayaga.o : $(src)Babayaga.cxx
else
$(bin)BabayagaLib_dependencies.make : $(Babayaga_cxx_dependencies)

$(bin)$(binobj)Babayaga.o : $(Babayaga_cxx_dependencies)
endif
	$(cpp_echo) $(src)Babayaga.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BabayagaLib_pp_cppflags) $(lib_BabayagaLib_pp_cppflags) $(Babayaga_pp_cppflags) $(use_cppflags) $(BabayagaLib_cppflags) $(lib_BabayagaLib_cppflags) $(Babayaga_cppflags) $(Babayaga_cxx_cppflags)  $(src)Babayaga.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: BabayagaLibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(BabayagaLib.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(BabayagaLib.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(BabayagaLib.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(BabayagaLib.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_BabayagaLib)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BabayagaLib.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BabayagaLib.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BabayagaLib.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

BabayagaLibclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library BabayagaLib
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)BabayagaLib$(library_suffix).a $(library_prefix)BabayagaLib$(library_suffix).s? BabayagaLib.stamp BabayagaLib.shstamp
#-- end of cleanup_library ---------------
