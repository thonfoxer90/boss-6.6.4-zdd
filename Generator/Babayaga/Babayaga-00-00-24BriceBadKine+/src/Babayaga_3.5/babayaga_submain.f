      SUBROUTINE BABAYAGA(NEVENTS) 
***********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 QPHOT1(0:3),QPHOT2(0:3),PFINEL(0:3),PFINPOS(0:3)
      CHARACTER*1 INTUPLE,PHCUT,LLG,CUTG
      REAL*4 EVENT(12),RVEC2(2),PH(1)                      
      REAL*8 PM(0:3),PMIRR(0:3),PP(0:3),PPIRR(0:3) 
      REAL*8 QMIRR(0:3),QPIRR(0:3),VCM(0:3)
      REAL*8 mo,mr,mrp,P1(0:3)
      REAL*8 y(10),x(40),qph(40,0:3) 

      INTEGER TEVENTS
********** Ping RG: final momentum of the events
*     CPM(ith-event,j): beam e- momentum for i-th event
*     CPP(ith-event,j): beam e+ momentum for i-th event
*     CFINEL(ith-event,j): final negtive particle momentum for i-th event
*     CPFINPOS(ith-event,j): final positive particle momentum for i-th event
*     CQPH(ith-event,nth-photons,jth-momentum): ISR photons for i-th event
*     NCQPH(ith-event): the ISR photon number for i-th event 
C
      PARAMETER(TEVENTS=160000)
      REAL*8 CPM(0:TEVENTS,0:3),CPP(0:TEVENTS,0:3)
      REAL*8 CPFINEL(0:TEVENTS,0:3),CPFINPOS(0:TEVENTS,0:3)
      REAL*8 CQPH(0:TEVENTS,10,0:3)
      INTEGER NCQPH(TEVENTS)  !total number of ISR photons   
      COMMON/MOMSET/CPM,CPP,CPFINEL,CPFINPOS,CQPH
      COMMON/ISRPHOTONS/NCQPH
      SAVE /MOMSET/
      SAVE /ISRPHOTONS/
****************************************************************
      INTEGER Oalpha,on
*
      PARAMETER (LUX=4,K1=0,K2=0)
      PARAMETER (NWPAWC=10000000)
*      
      COMMON/PAWC/PAW(NWPAWC)
      COMMON/CONST/ALPHA,CONVFAC,PI,AME,AMMU,AMPI
      COMMON/CHANNEL/ICH
      COMMON/PIINVMASS/PIQ2
      COMMON/FOTONI/NPHO  
      common/phot_cuts/icutphotons
      common/allphotons/qph
      common/switchFSR/on,ill
      common/switcharun/iarun
      common/expcuts/thmin,thmax,emin,zmax,egmin,thgmin,thgmax
      common/PFFparams/mo,go,mr,gr,mrp,grp,apff,bpff
      common/resonances/ires
*
      CHARACTER*4  CHTAGS(12)
      CHARACTER*20 OUTFILE,STORFILE
      CHARACTER*15 INFILE
************** Ping RG:set user's parameters ****
      common/beamEnergy/ebeam
      COMMON/DECLAREINT/INT
      COMMON/DECLARESTR/INTUPLE,PHCUT,CUTG
************************************************************************
      write(*,*),'************************INTUPLE = ',INTUPLE 
*
* Important parameters are set....
*
      ALPHA   = 1.D0/137.0359895D0   
      AME     = 0.51099906D-3      ! electron mass
      AMMU    = 105.658389D-3      ! muon mass
      AMPI    = 139.56995D-3       ! pion mass

      CONVFAC = 0.389379660D6      ! conversion factor from natural
                                   ! units to nanobarn
      Oalpha  = 0
*
****** READING INPUTS *********
*
*      on     = 1     ! by default, FSR is switched on
*      arun   = 1     ! by default alpha is running
*      ires   = 1     ! by default hadronic resonances are included

      STORFILE='BABAYAGA.ntuple'
      OUTFILE ='CrossSection.txt'

      MAXNEV = NEVENTS
      IF(MAXNEV.GT.TEVENTS) THEN
      PRINT*,'The events number larger than the set max_event',
     &  TEVENTS, ' I stop running'
      stop
      endif

      
      if (ich.eq.3.or.ich.eq.4) then 
         on = 0
      else if(ich.eq.1) then
         on = 1
      endif

      if (ich.eq.1.and.emin.lt.ame)  emin = ame
      if (ich.eq.2.and.emin.lt.ammu) emin = ammu
      if (ich.eq.4.and.emin.lt.ampi) emin = ampi


      ILL = 0

      if (ich.eq.2.and.on.eq.0) ILL = 1
      if (ich.ge.3) ILL = 1


      iwriout = maxnev/100

      if (ich.ne.3) phcut = 'N'

cc      CUTG = 'N'
      icutphotons = 0
 
      if (CUTG.eq.'Y') icutphotons=1
      
      if (icutphotons.ne.1) then
      egmin  = 0.d0
      thgmin = 0.d0
      thgmax = 180.d0
       else
       endif

      if (ich.ne.4)  then 
       mo =0
       go =0
       mr =0
       gr =0
       mrp=0
       grp=0
       apff=0
       bpff=0
      else ! if ich = 4, pion form factor parameters are read

         mo=0.78194d0
         go=8.43d-3
         mr=8.43d-3
         gr=0.7685d0
         mrp=1.37d0
         grp=0.51d0
         apff=1.85d-3
         bpff=1.85d-3
         
      endif

********************************************************************
*
* Printing input to the screen
*
      print*,' '
c      print*,' Input read from file: ',INFILE
      print*,' '
      write(*,'(1x,A,I10)')' Seed for RANLUX: ',int
      print*,' '
        
      if (ich.eq.1) print*,' Simulated process: e+ e-  -->  e+ e- '
      if (ich.eq.2) print*,' Simulated process: e+ e-  -->  mu+ mu- '
      if (ich.eq.3) print*,' Simulated process: e+ e-  -->  gamma gamma'
      if (ich.eq.4) print*,' Simulated process: e+ e-  -->  pi+ pi-'

      write(*,'(1x,A,I10)')' Number of events to be simulated: ',maxnev

      write(*,'(1x,A,f7.4,A)')' Beam energy: ',ebeam,' GeV'

      write(*,'(1x,A,f8.3,A,f8.3,A)')
     >     ' Angular cuts: ',thmin,' deg. - ',thmax,' deg.'

      write(*,'(1x,A,f11.6,A)')' Minimum energy: ',emin,' GeV'

      write(*,'(1x,A,f8.4,A)')' Maximum acollinearity: ',zmax,' deg.'
      print*,' '      

      if (icutphotons.eq.0) print*,' No cuts are imposed on photons'
      if (icutphotons.eq.1) write(*,'(1x,A,f7.4,A,f8.4,A,f8.4,A)')
     >     ' Photon cuts:',egmin,' GeV,',thgmin,' deg. -',thgmax,' deg.'

      if (on.eq.1) print*,' FSR is switched on'
      if (on.eq.0) print*,' FSR is switched off'

      if (iarun.eq.1.and.ich.ne.3) print*,' Alpha is running'
      if (iarun.eq.0.and.ich.ne.3) print*,' Alpha is not running'

      if (ires.eq.1.and.ich.le.2) print*,
     >     ' Hadronic resonances are included'
      if (ires.eq.0.and.ich.le.2) print*,
     >     ' Hadronic resonances are not included'

c      print*,'************************INTUPLE = ',INTUPLE 
      if (intuple.eq.'Y') print*,' Events stored in file: ',STORFILE
      if (intuple.eq.'N') print*,' Events not stored'

      print*,' '
      print*,' Output file, written every ',iwriout,' events: ',outfile

      if (ich.eq.3) then
        print*,' Phcut (for double counting in e+ e- ----> gamma gamma):
     >        ',phcut
      endif
      if (ich.eq.4) then
         write(*,*)' Pion Form Factor parameters:'
         write(*,'(1x,A,f8.5,A,f8.5,A)')
     >        '  Omega mass and width ',mo,' Gev - ',go,' GeV'
         write(*,'(1x,A,f8.5,A,f8.5,A)')
     >        '  Rho mass and width   ',mr,' Gev - ',gr,' GeV'
         write(*,'(1x,A,f8.5,A,f8.5,A)')
     >        '  Rho'' mass and width  ',mrp,' Gev - ',grp,' GeV'
         write(*,'(1x,A,f8.5,A,f8.5,A)')
     >        '  A and B parameters   ',apff,' - ',bpff
      endif

      print*,''
      print*,' BEGINNING GENERATION.... '
      print*,''
      call flush()
*
**************************************************************************
*
      IF(INTUPLE.EQ.'Y') THEN

         call init_chtags(chtags)

         CALL HLIMIT(NWPAWC)
         CALL HROPEN(1,'PS_NTUPLE',STORFILE,'N',1024,ISTAT)
         CALL HBOOKN(10,'NTUPLE',12,'PS_NTUPLE',50000,CHTAGS)
      ENDIF
*
      PI    = 3.1415926  !4.D0*ATAN(1.D0)
      ZMAX  = ZMAX*PI/180.D0
      THMIN = THMIN*PI/180.D0
      THMAX = THMAX*PI/180.D0

      thgmin = thgmin*pi/180.d0
      thgmax = thgmax*pi/180.d0

      CMIN = COS(THMAX)
      CMAX = COS(THMIN)
      LEN2 = 2
*
      Q2MIN   = EXP(1.D0)*AME**2 
      Q2MINMU = EXP(1.D0)*AMMU**2
      PIQ2    = 4.d0*ampi**2

      RS = 2.D0*EBEAM
      S  = RS**2 
*
      EPS = 1.D-5
      if (Oalpha.gt.0) eps = 1.d-5
*
      EPSP=AME/EBEAM  
*
***********************************************************
* HIT OR MISS MAXIMUM IS CALCULATED (SDMAX).  
*
      X1MIN=EMIN/EBEAM
      A=(1.D0-X1MIN)/(1.D0+X1MIN)
*
      CMAXCM=(CMAX+A)/(1.D0+A*CMAX)
*
      SMAX=4.D0*EMIN**2
*
      FATT = (PNB(S,Q2MIN,EPS)/PNOB(S,Q2MIN,EPS,EPSP))**2
*
      IF (ICH.EQ.1) THEN
         ALNE = LOG(EPS)
         AIEPS= -2.D0*ALNE-1.5D0+2.D0*EPS-0.5D0*EPS**2 
         AI   = LOG((1.D0-EPS)/EPSP*((1.D0-EPSP)/EPS)**2)-1.D0
     >          +EPS+EPSP
       IF (AIEPS.LT.AI) THEN
          Q2F=S*(1.D0-CMIN)/(1.D0+CMIN)
        ELSE
           Q2F=S*(1.D0-CMAX)/(1.D0+CMAX) 
       ENDIF
       FATT=(PNB(Q2F,Q2MIN,EPS)/PNOB(Q2F,Q2MIN,EPS,EPSP))**2
*
       SDMAX=SDIF(S,0.D0)/4.5D0*(4.D0+(1.D0+CMAX)**2)
     >      -SDIF(S,0.D0)/4.5D0*(1.D0-CMIN)*(5.D0*CMIN+CMIN**2
     >      +CMIN**3+1.D0)
*
       SDMAX = FATT*SDMAX
     >      *(CMAX-CMIN)/(1.D0-CMIN)/(1.D0-CMAX) 
      ENDIF
*
      IF (ICH.EQ.2) THEN
      SDMAX=FATT*SDIF(S,0.d0)*(CMAX-CMIN)
     >      *(1.D0+CMAX**2+4.D0*AMMU**2/SMAX)
      ENDIF
*
      IF (ICH.EQ.3) THEN
         ANORM = 0.5D0*LOG((1.D0-CMIN)*(1.D0+CMAX)/
     >        (1.D0+CMIN)/(1.D0-CMAX)) 
*
         SDMAX = FATT*SDIF(S,CMAX)*ANORM*(1.D0-CMAX**2)
      ENDIF

      if (ich.eq.4) then
         piq2 =  (0.7726d0)**2
         sdmax = fatt*sdif(piq2,0.d0)*(cmax-cmin)/1.5d0
         if (Oalpha.gt.0) sdmax = sdmax/fatt*2.5d0
      endif

**************************************************************
*
      CTRL=0.D0 ! it controls if the integrand function goes up
      		! the maximum
*                                                                  
* RANLUX INITIALIZATION                       
*                                                                  
      CALL RLUXGO(LUX,INT,K1,K2) 
*        
      call flush()
      NCALLS=0    
*
      BIN=SQRT(1.D0-AME**2/EBEAM**2)
      PM(0)=EBEAM
      PM(1)=0.D0
      PM(2)=0.D0
      PM(3)=BIN*EBEAM 
*	 	
      PP(0)=EBEAM
      PP(1)=0.D0
      PP(2)=0.D0
      PP(3)=-BIN*EBEAM 
*
      wsum  = 0.d0   ! weighted integration
      wsum2 = 0.d0
*
* BEGIN OF DO LOOP OVER EVENTS
*

      DO 30, NEV = 1,MAXNEV
*
 113    CALL RANLUXBB(RVEC2,LEN2)
*
c        print*,'RVEC2', RVEC2(1),RVEC2(2),LEN2
C       print*,'evt # = ', nev

       NCALLS=NCALLS+1 
*
       CALL RANLUXBB(PH,1)
       PHIFS=2.D0*PI*PH(1)
*        
      IF (ICH.EQ.1) THEN 
         CCM = ((CMAX-CMIN)*RVEC2(1)+(1.D0-CMAX)*CMIN)/
     >        (1.D0-CMAX+(CMAX-CMIN)*RVEC2(1))   
      ENDIF
      IF (ICH.EQ.2.OR.ICH.EQ.4) THEN
         CCM = (CMAX-CMIN)*RVEC2(1)+CMIN
      ENDIF
      IF (ICH.EQ.3) THEN
         ESP=EXP(2.D0*ANORM*RVEC2(1))
         CCM=((1.D0+CMIN)*ESP-(1.D0-CMIN))/
     >        ((1.D0+CMIN)*ESP+(1.D0-CMIN)) 
      ENDIF   
*
        THETAL=ACOS(CCM)
*
        TCM = -0.5D0*S*(1.D0-CCM)
        UCM = -0.5D0*S*(1.D0+CCM)
        IF (ICH.EQ.1) Q2MAX = S*TCM/UCM
        IF (ICH.GE.2.or.on.eq.0) Q2MAX = S 
        IF (ICH.EQ.2.and.on.eq.1)Q2MAX = S*TCM/UCM
*
**   if (Oalpha.eq.0) then  ! for order alpha PS
*
* ALL-ORDER PHOTONS SHOWERS FROM FERMIONS:
*
* INITIAL STATE ELECTRON.....
*
           CALL PHOTS(Q2MIN,Q2MAX,EPS,EPSP,y,nemi,n10mi)
*
           do k=1,10
              x(k)=y(k)
           enddo
*
* .... INITIAL STATE POSITRON.....
*
           CALL PHOTS(Q2MIN,Q2MAX,EPS,EPSP,y,nepi,n10pi)
*
           do k=1,10
              x(k+10)=y(k)
           enddo

           FSQ2M=Q2MIN
           IF (ICH.EQ.2) FSQ2M=Q2MINMU  
*
* .... FINAL STATE ELECTRON.....
*
           CALL PHOTT(FSQ2M,Q2MAX,EPS,y,nemf,n10mf)        
*     
           do k=1,10
              x(k+20)=y(k)
           enddo	
*
* ... AND FINAL STATE POSITRON.
*	
           CALL PHOTT(FSQ2M,Q2MAX,EPS,y,nepf,n10pf)        
*
           do k=1,10
              x(k+30)=y(k)
           enddo
*
**  else                                      ! for order alpha PS           
**           call PSoal(Q2MAX,Q2MIN,EPS,x)  ! for order alpha PS
**   endif                                    ! for order alpha PS
*
        xel=1.d0
	xpos=1.d0
*
	do k=1,10
         xel=xel*x(k)
	 xpos=xpos*x(k+10)
	enddo
*
	if (ich.eq.1) amaa=ame
	if (ich.eq.2) amaa=ammu
        if (ich.eq.3) amaa=0.d0
        if (ich.eq.4) amaa=ampi
*
        call kinematics(pm,pp,amaa,x,ccm,phifs,qmirr,qpirr,qph,iwr)

        if (iwr.gt.0) goto 113
*
        SHAT=XEL*XPOS*S
*
* ANGULAR CUT ON ISR PHOTONS TO AVOID DOUBLE COUNTING IN 
* GAMMA GAMMA FINAL STATE
*
        ENGMAX = EMIN
        IF (PHCUT.EQ.'Y') THEN
         DO K=1,40  
          COSG = 1.D0
          ENG = qph(k,0)
          IF (ENG.GT.1.D-12) COSG=qph(K,3)/ENG
          IF (COSG.GT.CMIN.AND.COSG.LT.CMAX.AND.ENG.GT.ENGMAX) 
     >      GOTO 113 
         ENDDO
        ENDIF
*
        DO K=0,3
           PFINEL(K)=QMIRR(K)        
        ENDDO
*      
        DO K=0,3
           PFINPOS(K)=QPIRR(K)        
        ENDDO
*
*  EXPERIMENTAL CUTS
*
      do i = 0,3
         p1(i)=pfinpos(i)+pfinel(i)
      enddo

      PIQ2 = dot(p1,p1)

      CALL CUTUSER(PFINEL,PFINPOS,ICUT)
      IF(ICUT.LT.1) GO TO 113
*
* HIT OR MISS
*
        IF (ICH.EQ.1) THEN
          XJAC=((1.D0-CCM)**2)*(CMAX-CMIN)/
     >              ((1.D0-CMAX)*(1.D0-CMIN))
        ENDIF   
        IF (ICH.EQ.2.OR.ICH.EQ.4) THEN
          XJAC=(CMAX-CMIN)
        ENDIF
        IF (ICH.EQ.3) THEN
          XJAC=ANORM*(1.D0-CCM**2)
        ENDIF
*
        COSCM=CCM           
*
        SDCM = SDIF(SHAT,COSCM)*XJAC 

        if (Oalpha.eq.0) then
           SDCM = SDCM*XEL*XPOS
           SDCM = SDCM
     >       *(PNB(Q2MAX,Q2MIN,EPS)/PNOB(Q2MAX,Q2MIN,EPS,EPSP))**2
        endif
*
      IF (SDCM.GT.SDMAX) THEN 
        CTRL=CTRL+1.D0
      ENDIF
* 
      wsum  = wsum  + sdcm      ! for weighted cross section
      wsum2 = wsum2 + sdcm**2

      CSI=RVEC2(2)*SDMAX
      IF(CSI.GT.SDCM) GOTO 113     
*
      IF(INTUPLE.EQ.'Y') THEN
*
         call FILL_EVENT(pfinel,pfinpos,qph,event)
         CALL HFN(10,EVENT)
*
      ENDIF
*    
* CROSS SECTION CALCULATION (nb)
*
      XNCALLS = 1.D0*NCALLS
      XNEV    = 1.D0*NEV
      R       = XNEV/XNCALLS
      SEZ     = R*SDMAX*CONVFAC 
      wsez    = wsum/xncalls*convfac
*
* STATISTICAL ERROR IN THE CROSS SECTION CALCULATION
*
      VAR  = SQRT(R*(1.D0-R)/XNCALLS)
      ERR  = VAR*SDMAX*CONVFAC 
      werr = (wsum2/xncalls-(wsez/convfac)**2)/xncalls
      werr = sqrt(werr)*convfac
*
* INSTRUCTIONS TO PRINT OUTPUTS ON FILE 'OUTFILE'
*
c      IF (NEV.EQ.MAXNEV.OR.MOD(NEV,IWRIOUT).EQ.0) THEN
      IF (NEV.EQ.MAXNEV) THEN
      OPEN(10,FILE=OUTFILE,STATUS='UNKNOWN')       
*
      REWIND(10) 
      WRITE(10,*) '  '
      WRITE(10,'(A,A,A)')
c     >     '------ INPUTS (from file: ',infile,') ------'
      WRITE(10,*)' '

      IF (ICH.EQ.1) WRITE(10,*)' PROCESS: e+ e- ----> e+ e- '
      IF (ICH.EQ.2) WRITE(10,*)' PROCESS: e+ e- ----> mu+ mu- '
      IF (ICH.EQ.3) WRITE(10,*)' PROCESS: e+ e- ----> gamma gamma '
      IF (ICH.EQ.4) WRITE(10,*)' PROCESS: e+ e- ----> pi+ pi- '
      WRITE(10,*)' '

      WRITE(10,'(1x,A,f7.4,A)')' EBEAM  = ',EBEAM,' GeV'
      WRITE(10,'(1x,A,f8.4,A,f8.4,A)')
     >     ' ANGULAR CUTS ',thmin*180/pi,' - ',thmax*180/pi,' deg.'  
      WRITE(10,'(1x,A,f7.4,A)')' EMIN   = ',emin,' GeV'
      WRITE(10,'(1x,A,f8.4,A)')' ACOLL. = ',zmax*180/pi,' deg.'

      if (icutphotons.eq.1) WRITE(10,'(1x,A,f7.4,A,f8.4,A,f8.4,A)')
     >     ' Photon cuts: ',egmin,' GeV,',
     >     thgmin*180/pi,' deg. -',thgmax*180/pi,' deg.'
      WRITE(10,*)' '

      if (on.eq.1) WRITE(10,*)' FSR is switched on'
      if (on.eq.0) WRITE(10,*)' FSR is switched off'

      if (iarun.eq.1.and.ich.ne.3) WRITE(10,*)' Alpha is running'
      if (iarun.eq.0.and.ich.ne.3) WRITE(10,*)' Alpha is not running'

      if (ires.eq.1.and.ich.le.2) WRITE(10,*)
     >     ' Hadronic resonances are included'
      if (ires.eq.0.and.ich.le.2) WRITE(10,*)
     >     ' Hadronic resonances are not included'

      if (icutphotons.eq.0) WRITE(10,*)' No cuts are imposed on photons'

      IF (ICH.EQ.3) WRITE(10,*)' PHCUT = ',PHCUT

      if (ich.eq.4) then
         write(10,*)' Pion Form Factor parameters:'
         write(10,'(1x,A,f8.5,A,f8.5,A)')
     >        '  Omega mass and width ',mo,' Gev - ',go,' GeV'
         write(10,'(1x,A,f8.5,A,f8.5,A)')
     >        '  Rho mass and width   ',mr,' Gev - ',gr,' GeV'
         write(10,'(1x,A,f8.5,A,f8.5,A)')
     >        '  Rho'' mass and width  ',mrp,' Gev - ',grp,' GeV'
         write(10,'(1x,A,f8.5,A,f8.5,A)')
     >        '  A and B parameters   ',apff,' - ',bpff
      endif
      WRITE(10,*)' '

      WRITE(10,'(1x,A,I10)')' RANLUX SEED =',INT  
      WRITE(10,*)' OUTPUT FILE = ',OUTFILE
      WRITE(10,*)' ' 

      IF (INTUPLE.EQ.'Y') WRITE(10,*)' EVENTS STORED IN ',STORFILE
      IF (INTUPLE.EQ.'N') WRITE(10,*)' EVENTS NOT STORED'
      WRITE(10,*)' ' 

      WRITE(10,*)'----------------- OUTPUTS --------------------'
      WRITE(10,*)' '
      WRITE(10,*)' EVENT NUMBER ',NEV,' OF ',MAXNEV
      WRITE(10,*)' '
      if (ctrl.gt.0) then
         WRITE(10,*)CTRL,' (CTRL) WARNING: SDMAX TOO SMALL '
         WRITE(10,*)CTRL/XNCALLS,' (CTRL/NCALLS)'
      endif

      WRITE(10,'(1x,A,f7.5)')' Monte Carlo efficiency: ',R

      WRITE(10,*)'  '
      WRITE(10,*)' UNWEIGHTED CROSS SECTION'
      WRITE(10,'(f14.7,A,f14.7,A)')SEZ,'   +-',ERR,' (nb)'

      WRITE(10,*)'  '
      WRITE(10,*)' WEIGHTED CROSS SECTION'
      WRITE(10,'(f14.7,A,f14.7,A)')WSEZ,'   +-',WERR,' (nb)'
      WRITE(10,*)'  '
*
      CLOSE(10) 
*     
      ENDIF 
************** PING RG, 2008-5-11
      DO I1=0,3  !! STORE MOMENTUM FOR THE INI. AND FIN. PATICLES
      CPM(NEV,I1)=PM(I1)
      CPP(NEV,I1)=PP(I1)
      CPFINEL(NEV,I1) =PFINEL(I1)
      CPFINPOS(NEV,I1)=PFINPOS(I1)
      ENDDO


      IIT=0
      DO II=1,40
      IF(QPH(II,0).GT.0.001) THEN  !! PHOTON ENERGY>1MEV IS SELECTED
      IIT = IIT+1
      CQPH(NEV,IIT,0)=QPH(II,0)
      CQPH(NEV,IIT,1)=QPH(II,1)
      CQPH(NEV,IIT,2)=QPH(II,2)
      CQPH(NEV,IIT,3)=QPH(II,3)
      ENDIF
      ENDDO
      NCQPH(NEV)=IIT
******************************************
***** debuging pingrg
c      print*,'****FORTRAN*************'
c       PRINT*, 'NEV= ',NEV
c      print*,(cpfinpos(nev,i),i=0,3) 
c      print*,(cpfinel(nev,i),i=0,3)
c       do ii=1,iit
c       print*, 'NEV, IIT=',NEV,', ',II
c       if(iit.gt.0)print*, (CQPH(NEV,II,i),i=0,3)
c       enddo
*****************************************


  30  CONTINUE
*
      IF(INTUPLE.EQ.'Y') THEN
         CALL HROUT(0,ICYCLE,' ')
         CALL HREND('PS_NTUPLE')
      ENDIF
*
      RETURN
      END
*
************** END OF MAIN PROGRAM ******************
*****************************************************
