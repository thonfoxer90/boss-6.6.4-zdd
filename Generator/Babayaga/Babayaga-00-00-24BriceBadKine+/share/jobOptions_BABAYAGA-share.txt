ApplicationMgr.DLLs += {"Babayaga"};
ApplicationMgr.TopAlg += {"Babayaga"};

////////options for BABAYAGA
Babayaga.Channel=3;              // 1: e+e-->e+e-;2:e+e_->mu+mu-;3:e+e-->gamma gamma;4:e+e--->pi+pi-
Babayaga.Ebeam=1.548;            // Ecm = 2*Ebeam [GeV]
Babayaga.MinThetaAngle=10;       // minimum angle(deg.)
Babayaga.MaxThetaAngle=170;      //maximum angle(deg.)
Babayaga.MinimumEnergy=0.4;      //minimum energy  (GeV)
Babayaga.MaximumAcollinearity=180; //maximum acollinearity (deg.)
Babayaga.RunningAlpha=1;         //running alpha (0 = off, 1 = on)
Babayaga.HadronicResonance=0;    //hadronic resonances for ICH = 1 or 2
Babayaga.FSR_swich=0;            //FSR switch for ICH = 2 (0 = off, 1 = on)
Babayaga.MinEnerCutG=0.1;        //minimum energy for CUTG = Y (GeV)
Babayaga.MinAngCutG=5;           //minimum angle for CUTG = Y  (deg.)
Babayaga.MaxAngCutG=21;          //maximum angle for CUTG = Y  (deg.)
Babayaga.HBOOK = 1;              // close babayaga.ntuple hbook output
Babayaga.PHCUT = 0;
Babayaga.CUTG  = 0;
/////////////////////////////////////////////
