/*
 *    2004/09/10   Zhengyun You     Peking University
 *                 
 */

// $Id: ZddGeomSvc.h,v 1.4 2006/11/23 08:55:10 liangyt Exp $ // -*-c++-*-
// header file for a class called "ZddGeomSvc"
#ifndef ZDD_GEOM_SVC_H
#define ZDD_GEOM_SVC_H

#include <vector>
#include <iostream>

//#include "BesGeoZdd/DB2BesGeoZdd.h"  
#include "ZddGeomSvc/ZddGeoGeneral.h"
#include "ZddGeomSvc/ZddGeoCrystal.h"
#include "ZddGeomSvc/IZddGeomSvc.h"
#include "G4Geo/ZddG4Geo.h"

#include "GaudiKernel/Service.h"
#include "GaudiKernel/IInterface.h"

class ZddGeomSvc : public Service, virtual public IZddGeomSvc
{
public:
  // Constructors and destructors
  ZddGeomSvc( const std::string& name, ISvcLocator* svcloc );
  ~ZddGeomSvc();
  
  virtual StatusCode queryInterface(const InterfaceID& riid, void** ppvUnknown);
  virtual StatusCode initialize ( );
  virtual StatusCode finalize ( );
  
  // Access by others
  virtual const ZddGeoGeneral * const GetGeoGeneral();
  virtual const ZddGeoCrystal * const GetCrystal(int partID, int detectorID, int crystalNo);
  virtual const ZddG4Geo * const GetZddG4Geo();

  virtual void Dump();

  // private method
private:
  void Fill();
  
  // private data members
private:
  ZddGeoGeneral *m_pZddGeoGeneral;
  ZddG4Geo *m_pZddG4Geo;
  int m_Geometry;  //0: G4  ; 1: Root
};

#endif /* ZDD_GEOM_SVC_H */


