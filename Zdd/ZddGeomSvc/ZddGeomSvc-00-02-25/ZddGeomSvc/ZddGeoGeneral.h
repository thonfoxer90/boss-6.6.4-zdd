//$id$
//
//$log$

/*
 *    2003/08/30   Zhengyun You      Peking University
 * 
 *    2004/09/09   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

#ifndef ZDD_GEO_GENERAL_H
#define ZDD_GEO_GENERAL_H

#include <stdlib.h>
#include <math.h>
#include <vector>
#include <map>
#include <CLHEP/Vector/ThreeVector.h>
#include <CLHEP/Geometry/Point3D.h>
#include <CLHEP/Geometry/Plane3D.h>
#include <CLHEP/Vector/Rotation.h>

#include "Identifier/Identifier.h"
#include "Identifier/ZddID.h"
#include "ZddGeomSvc/ZddGeoCrystal.h"

#ifndef ENABLE_BACKWARDS_COMPATIBILITY
typedef HepGeom::Point3D<double> HepPoint3D;
#endif
#ifndef ENABLE_BACKWARDS_COMPATIBILITY
typedef HepGeom::Vector3D<double> HepVector3D;
#endif

using namespace std;
using namespace CLHEP;

//class ZddGeoGap;
//class ZddGeoStrip;

/**
 * Class ZddGeoGeneral contains all of the objects necessary to describe the
 * muon chamber geometry.
 *
 * @author Zhengyun You \URL{mailto:youzy@hep.pku.cn}
 *
 */

class ZddGeoGeneral
{
 public:

  /// Constructor.
  ZddGeoGeneral();

  /// Assignment constructor.
  ZddGeoGeneral& operator=(const ZddGeoGeneral& orig);

  /// Copy constructor.
  ZddGeoGeneral(const ZddGeoGeneral& orig);

  /// Destructor.
  ~ZddGeoGeneral();

  /*
  /// Initialize the instance of ZddGeoGeneral.
  void Init();

  /// Initialize from database.
  void InitFromDatabase();

  /// Initialize form ASCII.
  void InitFromASCII();

  /// Initialize from xml.
  void InitFromXML();

  /// Destroy the single instance of ZddGeoGeneral.
  void Destroy();
  */
  /// Get a pointer to the single instance of ZddGeoGeneral.
  static ZddGeoGeneral *Instance();

  /// Get a pointer to the crystal identified by (partID,detectorID,crystalNo).
  ZddGeoCrystal *GetCrystal(const int partID, const int detectorID, const int crystalNo) const;

  /// Get a pointer to the crystal identified by Iemacs dentifier.
  ZddGeoCrystal *GetCrystal(const Identifier id) const;

  void Clear();

 private:

  // Have we initialize the geometry.
  static int m_gGeometryInit;                    
  // Pointer to the instance of ZddGeoGeneral.
  static ZddGeoGeneral *m_gpZddGeoGeneral;  

  // map containing the pointers to crystals.
  static map<Identifier, ZddGeoCrystal*> m_gpZddGeoCrystal;   

  };

ostream& operator << (ostream& s, const ZddGeoGeneral& geom);

#endif   /* ZDD_GEO_GENERAL_H */
