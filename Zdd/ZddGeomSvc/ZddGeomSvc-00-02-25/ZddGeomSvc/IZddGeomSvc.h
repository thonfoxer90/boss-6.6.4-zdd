/*
 *    2004/09/10   Zhengyun You     Peking University
 *                 
 */

#ifndef IZDD_GEOM_SVC_H_
#define IZDD_GEOM_SVC_H_
#include "GaudiKernel/IInterface.h"
#include "GaudiKernel/Kernel.h"
  
// Declaration of the interface ID 
static const InterfaceID  IID_IZddGeomSvc = 3501;

class IZddGeomSvc : virtual public IInterface  {  
public: 
   static const InterfaceID& interfaceID() { return IID_IZddGeomSvc; } 
   virtual void Dump() = 0;
};
 
#endif // IZDD_GEOM_SVC_H
