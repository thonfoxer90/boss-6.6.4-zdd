//$id$
//
//$log$

/*
 *    2003/08/30   Zhengyun You     Peking University 
 * 
 *    2004/09/09   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

#ifndef ZDD_GEO_GAP_H
#define ZDD_GEO_GAP_H

#include <iostream>
#include <vector>
#include <CLHEP/Vector/ThreeVector.h>
#include <CLHEP/Geometry/Point3D.h>
#include <CLHEP/Geometry/Plane3D.h>
#include <CLHEP/Vector/Rotation.h>
#include <CLHEP/Matrix/Matrix.h>
#include "TGeoNode.h"
#include "TGeoMatrix.h"
#include "TGeoPhysicalNode.h"

#ifndef ENABLE_BACKWARDS_COMPATIBILITY
typedef HepGeom::Point3D<double> HepPoint3D;
#endif
#ifndef ENABLE_BACKWARDS_COMPATIBILITY
typedef HepGeom::Vector3D<double> HepVector3D;
#endif

using namespace std;
using namespace CLHEP;

/**
 * Class ZddGeoGap describes a single gap in the muon chamber
 *
 * @author Zhengyun You \URL{mailto:youzy@hep.pku.cn}
 */

class ZddGeoCrystal
{
 public:

  /// Default constructor.
  ZddGeoCrystal();

  /// Real constructor
  ZddGeoCrystal( const int partID, const int detectorID, const int crystalNo );
 
  /*
  /// ROOT constructor.
  ZddGeoCrystal(const int partID,
		const int detectorID,
		const int crystalNo);
 */
  /// Assignment constructor.
  ZddGeoCrystal& operator=(const ZddGeoCrystal& orig);

  /// Copy constructor.
  ZddGeoCrystal(const ZddGeoCrystal& orig);

  /// Desctructor.
  ~ZddGeoCrystal();

  /// Get part identifier (1,2).
  int PartID() const { return m_PartID; }

  /// Get detector identifier (1-2).
  int DetectorID() const { return m_DetectorID; }
  
  /// Get Crystal no identifier (101 - 224).
  int CrystalNo() const { return m_CrystalNo; }

 private:

  int   m_PartID;             // Part No of this gap.
  int   m_DetectorID;              // Segment No of this gap.
  int   m_CrystalNo;              // Gap No of this gap.

};
 
ostream& operator << (ostream& s, const ZddGeoCrystal& crystal);

#endif    /* ZDD_GEO_GAP_H */

