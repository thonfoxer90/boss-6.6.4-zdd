//======================================================================
//  File:        hash.h
//  Author:      Timothy A. Budd
//  Description: This file contains the interface and implementation
//               of the hash template classes.
//
//  Copyright (c) 1992 by Timothy A. Budd.  All Rights Reserved.
//	may be reproduced for any non-commercial purpose
//
//  Modified:  March 11, 1998 by Kyle Pope
//             Inherit from STL std::vector<T> class.
//
//======================================================================

#ifndef HASH_H
#define HASH_H

#include <stddef.h>
#include <vector>

//----------------------------------------------------------------------
//	class hashVector
//		vector indexed using hashed key values
//              template types are type of key
//              and type of value stored in vector
//----------------------------------------------------------------------

//template <class T> class vector;

using namespace std;

template <class H, class T> class hashVector : public vector<T>
{
public:
    // constructors
    hashVector(size_t max, size_t (*f)(const H &));
    hashVector(size_t max, size_t (*f)(const H &), T & initialValue);
    hashVector(const hashVector<H, T> & v);

    // subscript operation
    T & operator [] (const H & index);
    const T & operator [] (const H & index) const;

private:
    // store the hash function
    size_t (*hashfun)(const H &);
};

//----------------------------------------------------------------------
//	class hashVector implementation
//----------------------------------------------------------------------

template <class H, class T>
inline hashVector<H, T>::hashVector(size_t max, size_t (*f)(const H &))
    : vector<T>(max), hashfun(f)
{
    // no further initialization
}



template <class H, class T>
inline hashVector<H, T>::hashVector(size_t max, size_t (*f)(const H&), T& initialValue)
    : vector<T>(max, initialValue), hashfun(f)
{
    // no further initialization
}



template <class H, class T>
inline hashVector<H, T>::hashVector(const hashVector<H,T> & v)
    : vector<T>(v), hashfun(v.hashfun)
{
    // no further initialization
}



template <class H, class T>
inline T & hashVector<H, T>::operator[] (const H & index)
{
  // subscript a hash vector
  // hash the index value before indexing vector
  return vector<T>::operator[] ((*hashfun)(index) % size());
}


template <class H, class T>
inline const T & hashVector<H, T>::operator[] (const H & index) const
{
  // subscript a hash vector
  // hash the index value before indexing vector
  return vector<T>::operator[] ((*hashfun)(index) % size());
}

//----------------------------------------------------------------------
//	miscellaneous template functions
//----------------------------------------------------------------------

#endif
