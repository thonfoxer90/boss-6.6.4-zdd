//$id$
//
//$log$

/*
 *    2003/09/06   Zhengyun You      Peking University
 * 
 *    2004/09/09   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

using namespace std;

#include <vector>
#include <iostream>
#include <iomanip>
#include <string>
//#include "TRint.h"
//#include <TGeoManager.h>

#include "ZddGeomSvc/ZddGeoGeneral.h"

int
main()
{
  //new TRint("ROOT GDML converter", 0, 0);
  //if (gGeoManager) delete gGeoManager;
  //new TGeoManager("BesGeo", "Bes geometry");

  cout << endl << "****** Initialize ZddGeoGeneral! ******" << endl << endl;
  ZddGeoGeneral::Instance()->Init();

 	
  std::cout<<"================================ begin event " << std::endl;

  HepPoint3D gPoint(0,0,0);
  Hep3Vector gVect(-1,0.8,-1.4);

  cout << "********************" << endl << endl ;
  cout << " Origin    of the track : " << gPoint << endl;
  cout << " Direction of the track : " << gVect  << endl;

  vector<Identifier> idGap, idGapTemp;
  for(unsigned int part = 0; part < ZddID::getPartNum(); part++) {
    for(unsigned int gap = 0; gap < ZddID::getGapNum(part); gap++) {
      idGapTemp = ZddGeoGeneral::Instance()->FindIntersectGaps(part, gap, gPoint, gVect);
      
      for( vector<Identifier>::iterator iter  = idGapTemp.begin(); 
	   iter != idGapTemp.end(); ++iter) {
	idGap.push_back(*iter);
      }
    }
  }
  
  cout << endl << "********************" << endl << endl;
  cout << " Number of intersect gaps : " << idGap.size() << endl << endl;
  
  for( unsigned int i = 0; i< idGap.size(); i++){
    cout << idGap[i] << endl;
  }
  
  vector<Identifier> idStrip, idStripTemp;
  for(unsigned int part = 0; part < ZddID::getPartNum(); part++) {
    for(unsigned int gap = 0; gap < ZddID::getGapNum(part); gap++) {
      idStripTemp = ZddGeoGeneral::Instance()->FindIntersectStrips(part, gap, gPoint, gVect);
      
      for( vector<Identifier>::iterator iter  = idStripTemp.begin(); 
	   iter != idStripTemp.end(); ++iter) {
	idStrip.push_back(*iter);
      }
    }
  }

  cout << endl << "********************" << endl << endl;
  cout << " Number of intersect strips : " << idStrip.size() << endl << endl;
  
  float stripCenterX, stripCenterY, stripCenterZ;
  HepPoint3D localStripCenter, stripCenter;
  ZddGeoStrip* pStrip;
  ZddGeoGap*   pGap;


  for( unsigned int i = 0; i < idStrip.size(); i++) {
    pStrip = ZddGeoGeneral::Instance()->GetStrip(ZddID::part(idStrip[i]),
						 ZddID::seg(idStrip[i]),
						 ZddID::gap(idStrip[i]),
						 ZddID::strip(idStrip[i]));
    pGap = pStrip->GetGap();

    pStrip->GetCenterPos(stripCenterX, 
			 stripCenterY, 
			 stripCenterZ);
    
    localStripCenter.setX(stripCenterX);
    localStripCenter.setY(stripCenterY);
    localStripCenter.setZ(stripCenterZ);

    stripCenter = pGap->TransformToGlobal(localStripCenter);

    cout << idStrip[i] << "   center : "  << stripCenter << endl;
  }

  vector<HepPoint3D> intersection, intersectionTemp;
  HepPoint3D origin(0,0,0);

  for(unsigned int part = 0; part < ZddID::getPartNum(); part++) {
    for(unsigned int gap = 0; gap < ZddID::getGapNum(part); gap++) {
      intersectionTemp =  ZddGeoGeneral::Instance()->FindIntersections(part, gap, gPoint, gVect);
      
      for( vector<HepPoint3D>::iterator iter  = intersectionTemp.begin(); 
	   iter != intersectionTemp.end(); ++iter) {
	intersection.push_back(*iter);
      }
    }
  }

  cout << endl << "********************" << endl << endl;
  cout << " Number of intersections : " << intersection.size() << endl << endl;


  for( unsigned int i = 0; i < intersection.size(); i++) {
    cout << intersection[i] << endl;
  }

  cout << endl << "********************" << endl << endl;

  cout << " ZddGeoTest1:: Execute  Successfully!!"
       << endl;
}




  


//HepPoint3D intersection[9];
//  intersection[i] = pzdd->GetIntersection(0, i, gPoint, gVect);
