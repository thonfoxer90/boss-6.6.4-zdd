#-- start of make_header -----------------

#====================================
#  Library ZddGeomSvc
#
<<<<<<< HEAD
#   Generated Sat May 19 14:50:45 2018  by bgarillo
=======
#   Generated Sun May 20 13:39:52 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ZddGeomSvc_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ZddGeomSvc_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ZddGeomSvc

ZddGeomSvc_tag = $(tag)

#cmt_local_tagfile_ZddGeomSvc = $(ZddGeomSvc_tag)_ZddGeomSvc.make
cmt_local_tagfile_ZddGeomSvc = $(bin)$(ZddGeomSvc_tag)_ZddGeomSvc.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ZddGeomSvc_tag = $(tag)

#cmt_local_tagfile_ZddGeomSvc = $(ZddGeomSvc_tag).make
cmt_local_tagfile_ZddGeomSvc = $(bin)$(ZddGeomSvc_tag).make

endif

include $(cmt_local_tagfile_ZddGeomSvc)
#-include $(cmt_local_tagfile_ZddGeomSvc)

ifdef cmt_ZddGeomSvc_has_target_tag

cmt_final_setup_ZddGeomSvc = $(bin)setup_ZddGeomSvc.make
#cmt_final_setup_ZddGeomSvc = $(bin)ZddGeomSvc_ZddGeomSvcsetup.make
cmt_local_ZddGeomSvc_makefile = $(bin)ZddGeomSvc.make

else

cmt_final_setup_ZddGeomSvc = $(bin)setup.make
#cmt_final_setup_ZddGeomSvc = $(bin)ZddGeomSvcsetup.make
cmt_local_ZddGeomSvc_makefile = $(bin)ZddGeomSvc.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ZddGeomSvcsetup.make

#ZddGeomSvc :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ZddGeomSvc'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ZddGeomSvc/
#ZddGeomSvc::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

ZddGeomSvclibname   = $(bin)$(library_prefix)ZddGeomSvc$(library_suffix)
ZddGeomSvclib       = $(ZddGeomSvclibname).a
ZddGeomSvcstamp     = $(bin)ZddGeomSvc.stamp
ZddGeomSvcshstamp   = $(bin)ZddGeomSvc.shstamp

ZddGeomSvc :: dirs  ZddGeomSvcLIB
	$(echo) "ZddGeomSvc ok"

#-- end of libary_header ----------------

ZddGeomSvcLIB :: $(ZddGeomSvclib) $(ZddGeomSvcshstamp)
	@/bin/echo "------> ZddGeomSvc : library ok"

$(ZddGeomSvclib) :: $(bin)ZddGeomSvc_entries.o $(bin)ZddGeomSvc_load.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(ZddGeomSvclib) $?
	$(lib_silent) $(ranlib) $(ZddGeomSvclib)
	$(lib_silent) cat /dev/null >$(ZddGeomSvcstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(ZddGeomSvclibname).$(shlibsuffix) :: $(ZddGeomSvclib) $(ZddGeomSvcstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" ZddGeomSvc $(ZddGeomSvc_shlibflags)

$(ZddGeomSvcshstamp) :: $(ZddGeomSvclibname).$(shlibsuffix)
	@if test -f $(ZddGeomSvclibname).$(shlibsuffix) ; then cat /dev/null >$(ZddGeomSvcshstamp) ; fi

ZddGeomSvcclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)ZddGeomSvc_entries.o $(bin)ZddGeomSvc_load.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
ZddGeomSvcinstallname = $(library_prefix)ZddGeomSvc$(library_suffix).$(shlibsuffix)

ZddGeomSvc :: ZddGeomSvcinstall

install :: ZddGeomSvcinstall

ZddGeomSvcinstall :: $(install_dir)/$(ZddGeomSvcinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(ZddGeomSvcinstallname) :: $(bin)$(ZddGeomSvcinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(ZddGeomSvcinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(ZddGeomSvcinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(ZddGeomSvcinstallname) $(install_dir)/$(ZddGeomSvcinstallname); \
	      echo `pwd`/$(ZddGeomSvcinstallname) >$(install_dir)/$(ZddGeomSvcinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(ZddGeomSvcinstallname), no installation directory specified"; \
	  fi; \
	fi

ZddGeomSvcclean :: ZddGeomSvcuninstall

uninstall :: ZddGeomSvcuninstall

ZddGeomSvcuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(ZddGeomSvcinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(ZddGeomSvcinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),ZddGeomSvcclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)ZddGeomSvc_dependencies.make :: dirs

ifndef QUICK
$(bin)ZddGeomSvc_dependencies.make : $(src)components/ZddGeomSvc_entries.cxx $(src)components/ZddGeomSvc_load.cxx $(use_requirements) $(cmt_final_setup_ZddGeomSvc)
	$(echo) "(ZddGeomSvc.make) Rebuilding $@"; \
	  $(build_dependencies) ZddGeomSvc -all_sources -out=$@ $(src)components/ZddGeomSvc_entries.cxx $(src)components/ZddGeomSvc_load.cxx
endif

#$(ZddGeomSvc_dependencies)

-include $(bin)ZddGeomSvc_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),ZddGeomSvcclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ZddGeomSvc_entries.d

$(bin)$(binobj)ZddGeomSvc_entries.d : $(use_requirements) $(cmt_final_setup_ZddGeomSvc)

$(bin)$(binobj)ZddGeomSvc_entries.d : $(src)components/ZddGeomSvc_entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ZddGeomSvc_entries.dep $(use_pp_cppflags) $(ZddGeomSvc_pp_cppflags) $(lib_ZddGeomSvc_pp_cppflags) $(ZddGeomSvc_entries_pp_cppflags) $(use_cppflags) $(ZddGeomSvc_cppflags) $(lib_ZddGeomSvc_cppflags) $(ZddGeomSvc_entries_cppflags) $(ZddGeomSvc_entries_cxx_cppflags) -I../src/components $(src)components/ZddGeomSvc_entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ZddGeomSvc_entries.o $(src)components/ZddGeomSvc_entries.cxx $(@D)/ZddGeomSvc_entries.dep
endif
endif

$(bin)$(binobj)ZddGeomSvc_entries.o : $(src)components/ZddGeomSvc_entries.cxx
else
$(bin)ZddGeomSvc_dependencies.make : $(ZddGeomSvc_entries_cxx_dependencies)

$(bin)$(binobj)ZddGeomSvc_entries.o : $(ZddGeomSvc_entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/ZddGeomSvc_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ZddGeomSvc_pp_cppflags) $(lib_ZddGeomSvc_pp_cppflags) $(ZddGeomSvc_entries_pp_cppflags) $(use_cppflags) $(ZddGeomSvc_cppflags) $(lib_ZddGeomSvc_cppflags) $(ZddGeomSvc_entries_cppflags) $(ZddGeomSvc_entries_cxx_cppflags) -I../src/components $(src)components/ZddGeomSvc_entries.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),ZddGeomSvcclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ZddGeomSvc_load.d

$(bin)$(binobj)ZddGeomSvc_load.d : $(use_requirements) $(cmt_final_setup_ZddGeomSvc)

$(bin)$(binobj)ZddGeomSvc_load.d : $(src)components/ZddGeomSvc_load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ZddGeomSvc_load.dep $(use_pp_cppflags) $(ZddGeomSvc_pp_cppflags) $(lib_ZddGeomSvc_pp_cppflags) $(ZddGeomSvc_load_pp_cppflags) $(use_cppflags) $(ZddGeomSvc_cppflags) $(lib_ZddGeomSvc_cppflags) $(ZddGeomSvc_load_cppflags) $(ZddGeomSvc_load_cxx_cppflags) -I../src/components $(src)components/ZddGeomSvc_load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ZddGeomSvc_load.o $(src)components/ZddGeomSvc_load.cxx $(@D)/ZddGeomSvc_load.dep
endif
endif

$(bin)$(binobj)ZddGeomSvc_load.o : $(src)components/ZddGeomSvc_load.cxx
else
$(bin)ZddGeomSvc_dependencies.make : $(ZddGeomSvc_load_cxx_dependencies)

$(bin)$(binobj)ZddGeomSvc_load.o : $(ZddGeomSvc_load_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/ZddGeomSvc_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ZddGeomSvc_pp_cppflags) $(lib_ZddGeomSvc_pp_cppflags) $(ZddGeomSvc_load_pp_cppflags) $(use_cppflags) $(ZddGeomSvc_cppflags) $(lib_ZddGeomSvc_cppflags) $(ZddGeomSvc_load_cppflags) $(ZddGeomSvc_load_cxx_cppflags) -I../src/components $(src)components/ZddGeomSvc_load.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ZddGeomSvcclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ZddGeomSvc.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ZddGeomSvc.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(ZddGeomSvc.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ZddGeomSvc.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_ZddGeomSvc)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ZddGeomSvc.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ZddGeomSvc.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ZddGeomSvc.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

ZddGeomSvcclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library ZddGeomSvc
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)ZddGeomSvc$(library_suffix).a $(library_prefix)ZddGeomSvc$(library_suffix).s? ZddGeomSvc.stamp ZddGeomSvc.shstamp
#-- end of cleanup_library ---------------
