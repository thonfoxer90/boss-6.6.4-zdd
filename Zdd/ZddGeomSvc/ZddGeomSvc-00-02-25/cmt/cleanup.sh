if test "${CMTROOT}" = ""; then
  CMTROOT=/cluster/him/bes3/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt cleanup -sh -pack=ZddGeomSvc -version=ZddGeomSvc-00-02-25 -path=/home/bgarillo/boss-6.6.4-ZDD/Zdd $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

