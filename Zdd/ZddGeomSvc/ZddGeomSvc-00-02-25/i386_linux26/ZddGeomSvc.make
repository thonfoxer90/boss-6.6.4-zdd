#-- start of make_header -----------------

#====================================
#  Library ZddGeomSvc
#
#   Generated Tue Aug 23 17:24:08 2011  by mwerner
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ZddGeomSvc_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ZddGeomSvc_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ZddGeomSvc

ZddGeomSvc_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddGeomSvc = /tmp/CMT_$(ZddGeomSvc_tag)_ZddGeomSvc.make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddGeomSvc = $(ZddGeomSvc_tag)_ZddGeomSvc.make
endif

else

tags      = $(tag),$(CMTEXTRATAGS)

ZddGeomSvc_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddGeomSvc = /tmp/CMT_$(ZddGeomSvc_tag).make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddGeomSvc = $(ZddGeomSvc_tag).make
endif

endif

-include $(cmt_local_tagfile_ZddGeomSvc)

ifdef cmt_ZddGeomSvc_has_target_tag

ifdef READONLY
cmt_final_setup_ZddGeomSvc = /tmp/CMT_ZddGeomSvc_ZddGeomSvcsetup.make
cmt_local_ZddGeomSvc_makefile = /tmp/CMT_ZddGeomSvc$(cmt_lock_pid).make
else
cmt_final_setup_ZddGeomSvc = $(bin)ZddGeomSvc_ZddGeomSvcsetup.make
cmt_local_ZddGeomSvc_makefile = $(bin)ZddGeomSvc.make
endif

else

ifdef READONLY
cmt_final_setup_ZddGeomSvc = /tmp/CMT_ZddGeomSvcsetup.make
cmt_local_ZddGeomSvc_makefile = /tmp/CMT_ZddGeomSvc$(cmt_lock_pid).make
else
cmt_final_setup_ZddGeomSvc = $(bin)ZddGeomSvcsetup.make
cmt_local_ZddGeomSvc_makefile = $(bin)ZddGeomSvc.make
endif

endif

ifdef READONLY
cmt_final_setup = /tmp/CMT_ZddGeomSvcsetup.make
else
cmt_final_setup = $(bin)ZddGeomSvcsetup.make
endif

ZddGeomSvc ::


ifdef READONLY
ZddGeomSvc ::
	@echo tags=$(tags)
	@echo cmt_local_tagfile=$(cmt_local_tagfile)
endif


dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	@echo 'ZddGeomSvc'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ZddGeomSvc/
ZddGeomSvc::
	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
	@echo "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

#-- end of make_header ------------------

#-- start of libary_header ---------------

ZddGeomSvclibname   = $(bin)$(library_prefix)ZddGeomSvc$(library_suffix)
ZddGeomSvclib       = $(ZddGeomSvclibname).a
ZddGeomSvcstamp     = $(bin)ZddGeomSvc.stamp
ZddGeomSvcshstamp   = $(bin)ZddGeomSvc.shstamp

ZddGeomSvc :: dirs  ZddGeomSvcLIB
	@/bin/echo "------> ZddGeomSvc ok"

#-- end of libary_header ----------------

ZddGeomSvcLIB :: $(ZddGeomSvclib) $(ZddGeomSvcshstamp)
	@/bin/echo "------> ZddGeomSvc : library ok"

$(ZddGeomSvclib) :: $(bin)ZddGeomSvc_load.o $(bin)ZddGeomSvc_entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(ZddGeomSvclib) $?
	$(lib_silent) $(ranlib) $(ZddGeomSvclib)
	$(lib_silent) cat /dev/null >$(ZddGeomSvcstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(ZddGeomSvclibname).$(shlibsuffix) :: $(ZddGeomSvclib) $(ZddGeomSvcstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" ZddGeomSvc $(ZddGeomSvc_shlibflags)

$(ZddGeomSvcshstamp) :: $(ZddGeomSvclibname).$(shlibsuffix)
	@if test -f $(ZddGeomSvclibname).$(shlibsuffix) ; then cat /dev/null >$(ZddGeomSvcshstamp) ; fi

ZddGeomSvcclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)ZddGeomSvc_load.o $(bin)ZddGeomSvc_entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
ZddGeomSvcinstallname = $(library_prefix)ZddGeomSvc$(library_suffix).$(shlibsuffix)

ZddGeomSvc :: ZddGeomSvcinstall

install :: ZddGeomSvcinstall

ZddGeomSvcinstall :: $(install_dir)/$(ZddGeomSvcinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(ZddGeomSvcinstallname) :: $(bin)$(ZddGeomSvcinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(ZddGeomSvcinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(ZddGeomSvcinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(ZddGeomSvcinstallname) $(install_dir)/$(ZddGeomSvcinstallname); \
	      echo `pwd`/$(ZddGeomSvcinstallname) >$(install_dir)/$(ZddGeomSvcinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(ZddGeomSvcinstallname), no installation directory specified"; \
	  fi; \
	fi

ZddGeomSvcclean :: ZddGeomSvcuninstall

uninstall :: ZddGeomSvcuninstall

ZddGeomSvcuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(ZddGeomSvcinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(ZddGeomSvcinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------

$(bin)ZddGeomSvc_dependencies.make :: dirs

ifndef QUICK
$(bin)ZddGeomSvc_dependencies.make :: $(src)components/*.cxx  requirements $(use_requirements) $(cmt_final_setup_ZddGeomSvc)
	@echo "------> (ZddGeomSvc.make) Rebuilding $@"; \
	  args=`echo $? | sed -e "s#requirements.*##"`; \
	  $(build_dependencies) ZddGeomSvc -all_sources $${args}
endif

#$(ZddGeomSvc_dependencies)

-include $(bin)ZddGeomSvc_dependencies.make

#-- end of dependency -------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGeomSvc_load.o : $(ZddGeomSvc_load_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGeomSvc_load.o $(use_pp_cppflags) $(ZddGeomSvc_pp_cppflags) $(lib_ZddGeomSvc_pp_cppflags) $(ZddGeomSvc_load_pp_cppflags) $(use_cppflags) $(ZddGeomSvc_cppflags) $(lib_ZddGeomSvc_cppflags) $(ZddGeomSvc_load_cppflags) $(ZddGeomSvc_load_cxx_cppflags) -I../src/components $(src)components/ZddGeomSvc_load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGeomSvc_entries.o : $(ZddGeomSvc_entries_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGeomSvc_entries.o $(use_pp_cppflags) $(ZddGeomSvc_pp_cppflags) $(lib_ZddGeomSvc_pp_cppflags) $(ZddGeomSvc_entries_pp_cppflags) $(use_cppflags) $(ZddGeomSvc_cppflags) $(lib_ZddGeomSvc_cppflags) $(ZddGeomSvc_entries_cppflags) $(ZddGeomSvc_entries_cxx_cppflags) -I../src/components $(src)components/ZddGeomSvc_entries.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ZddGeomSvcclean
	@cd .

ifndef PEDANTIC
.DEFAULT::
	@echo "WARNING >> You should provide a target for $@"
else
.DEFAULT::
	@echo "PEDANTIC MODE >> You should provide a target for $@"
	@if test `expr index $@ '.'` != 0 ; \
	then  echo "PEDANTIC MODE >> This target seems to be a missing file, please check"; exit -1 ;\
	else echo "PEDANTIC MODE >> Just ignore it ; as it seems to be just a fake target due to some pattern" ; exit 0; fi; 		
endif

ZddGeomSvcclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(binobj)$(library_prefix)ZddGeomSvc$(library_suffix).a $(binobj)$(library_prefix)ZddGeomSvc$(library_suffix).s? $(binobj)ZddGeomSvc.stamp $(binobj)ZddGeomSvc.shstamp
#-- end of cleanup_library ---------------

