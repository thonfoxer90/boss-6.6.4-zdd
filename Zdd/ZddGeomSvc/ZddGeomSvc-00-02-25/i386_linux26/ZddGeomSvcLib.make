#-- start of make_header -----------------

#====================================
#  Library ZddGeomSvcLib
#
#   Generated Tue Aug 23 17:24:08 2011  by mwerner
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ZddGeomSvcLib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ZddGeomSvcLib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ZddGeomSvcLib

ZddGeomSvc_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddGeomSvcLib = /tmp/CMT_$(ZddGeomSvc_tag)_ZddGeomSvcLib.make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddGeomSvcLib = $(ZddGeomSvc_tag)_ZddGeomSvcLib.make
endif

else

tags      = $(tag),$(CMTEXTRATAGS)

ZddGeomSvc_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddGeomSvcLib = /tmp/CMT_$(ZddGeomSvc_tag).make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddGeomSvcLib = $(ZddGeomSvc_tag).make
endif

endif

-include $(cmt_local_tagfile_ZddGeomSvcLib)

ifdef cmt_ZddGeomSvcLib_has_target_tag

ifdef READONLY
cmt_final_setup_ZddGeomSvcLib = /tmp/CMT_ZddGeomSvc_ZddGeomSvcLibsetup.make
cmt_local_ZddGeomSvcLib_makefile = /tmp/CMT_ZddGeomSvcLib$(cmt_lock_pid).make
else
cmt_final_setup_ZddGeomSvcLib = $(bin)ZddGeomSvc_ZddGeomSvcLibsetup.make
cmt_local_ZddGeomSvcLib_makefile = $(bin)ZddGeomSvcLib.make
endif

else

ifdef READONLY
cmt_final_setup_ZddGeomSvcLib = /tmp/CMT_ZddGeomSvcsetup.make
cmt_local_ZddGeomSvcLib_makefile = /tmp/CMT_ZddGeomSvcLib$(cmt_lock_pid).make
else
cmt_final_setup_ZddGeomSvcLib = $(bin)ZddGeomSvcsetup.make
cmt_local_ZddGeomSvcLib_makefile = $(bin)ZddGeomSvcLib.make
endif

endif

ifdef READONLY
cmt_final_setup = /tmp/CMT_ZddGeomSvcsetup.make
else
cmt_final_setup = $(bin)ZddGeomSvcsetup.make
endif

ZddGeomSvcLib ::


ifdef READONLY
ZddGeomSvcLib ::
	@echo tags=$(tags)
	@echo cmt_local_tagfile=$(cmt_local_tagfile)
endif


dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	@echo 'ZddGeomSvcLib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ZddGeomSvcLib/
ZddGeomSvcLib::
	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
	@echo "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

#-- end of make_header ------------------

#-- start of libary_header ---------------

ZddGeomSvcLiblibname   = $(bin)$(library_prefix)ZddGeomSvcLib$(library_suffix)
ZddGeomSvcLiblib       = $(ZddGeomSvcLiblibname).a
ZddGeomSvcLibstamp     = $(bin)ZddGeomSvcLib.stamp
ZddGeomSvcLibshstamp   = $(bin)ZddGeomSvcLib.shstamp

ZddGeomSvcLib :: dirs  ZddGeomSvcLibLIB
	@/bin/echo "------> ZddGeomSvcLib ok"

#-- end of libary_header ----------------

ZddGeomSvcLibLIB :: $(ZddGeomSvcLiblib) $(ZddGeomSvcLibshstamp)
	@/bin/echo "------> ZddGeomSvcLib : library ok"

$(ZddGeomSvcLiblib) :: $(bin)ZddGeomSvc.o $(bin)ZddGeometron.o $(bin)ZddGeoCrystal.o $(bin)ZddGeoGeneral.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(ZddGeomSvcLiblib) $?
	$(lib_silent) $(ranlib) $(ZddGeomSvcLiblib)
	$(lib_silent) cat /dev/null >$(ZddGeomSvcLibstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(ZddGeomSvcLiblibname).$(shlibsuffix) :: $(ZddGeomSvcLiblib) $(ZddGeomSvcLibstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" ZddGeomSvcLib $(ZddGeomSvcLib_shlibflags)

$(ZddGeomSvcLibshstamp) :: $(ZddGeomSvcLiblibname).$(shlibsuffix)
	@if test -f $(ZddGeomSvcLiblibname).$(shlibsuffix) ; then cat /dev/null >$(ZddGeomSvcLibshstamp) ; fi

ZddGeomSvcLibclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)ZddGeomSvc.o $(bin)ZddGeometron.o $(bin)ZddGeoCrystal.o $(bin)ZddGeoGeneral.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
ZddGeomSvcLibinstallname = $(library_prefix)ZddGeomSvcLib$(library_suffix).$(shlibsuffix)

ZddGeomSvcLib :: ZddGeomSvcLibinstall

install :: ZddGeomSvcLibinstall

ZddGeomSvcLibinstall :: $(install_dir)/$(ZddGeomSvcLibinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(ZddGeomSvcLibinstallname) :: $(bin)$(ZddGeomSvcLibinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(ZddGeomSvcLibinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(ZddGeomSvcLibinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcLibinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcLibinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(ZddGeomSvcLibinstallname) $(install_dir)/$(ZddGeomSvcLibinstallname); \
	      echo `pwd`/$(ZddGeomSvcLibinstallname) >$(install_dir)/$(ZddGeomSvcLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(ZddGeomSvcLibinstallname), no installation directory specified"; \
	  fi; \
	fi

ZddGeomSvcLibclean :: ZddGeomSvcLibuninstall

uninstall :: ZddGeomSvcLibuninstall

ZddGeomSvcLibuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(ZddGeomSvcLibinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcLibinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeomSvcLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(ZddGeomSvcLibinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------

$(bin)ZddGeomSvcLib_dependencies.make :: dirs

ifndef QUICK
$(bin)ZddGeomSvcLib_dependencies.make :: $(src)*.cxx  requirements $(use_requirements) $(cmt_final_setup_ZddGeomSvcLib)
	@echo "------> (ZddGeomSvcLib.make) Rebuilding $@"; \
	  args=`echo $? | sed -e "s#requirements.*##"`; \
	  $(build_dependencies) ZddGeomSvcLib -all_sources $${args}
endif

#$(ZddGeomSvcLib_dependencies)

-include $(bin)ZddGeomSvcLib_dependencies.make

#-- end of dependency -------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGeomSvc.o : $(ZddGeomSvc_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGeomSvc.o $(use_pp_cppflags) $(ZddGeomSvcLib_pp_cppflags) $(lib_ZddGeomSvcLib_pp_cppflags) $(ZddGeomSvc_pp_cppflags) $(use_cppflags) $(ZddGeomSvcLib_cppflags) $(lib_ZddGeomSvcLib_cppflags) $(ZddGeomSvc_cppflags) $(ZddGeomSvc_cxx_cppflags)  $(src)ZddGeomSvc.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGeometron.o : $(ZddGeometron_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGeometron.o $(use_pp_cppflags) $(ZddGeomSvcLib_pp_cppflags) $(lib_ZddGeomSvcLib_pp_cppflags) $(ZddGeometron_pp_cppflags) $(use_cppflags) $(ZddGeomSvcLib_cppflags) $(lib_ZddGeomSvcLib_cppflags) $(ZddGeometron_cppflags) $(ZddGeometron_cxx_cppflags)  $(src)ZddGeometron.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGeoCrystal.o : $(ZddGeoCrystal_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGeoCrystal.o $(use_pp_cppflags) $(ZddGeomSvcLib_pp_cppflags) $(lib_ZddGeomSvcLib_pp_cppflags) $(ZddGeoCrystal_pp_cppflags) $(use_cppflags) $(ZddGeomSvcLib_cppflags) $(lib_ZddGeomSvcLib_cppflags) $(ZddGeoCrystal_cppflags) $(ZddGeoCrystal_cxx_cppflags)  $(src)ZddGeoCrystal.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGeoGeneral.o : $(ZddGeoGeneral_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGeoGeneral.o $(use_pp_cppflags) $(ZddGeomSvcLib_pp_cppflags) $(lib_ZddGeomSvcLib_pp_cppflags) $(ZddGeoGeneral_pp_cppflags) $(use_cppflags) $(ZddGeomSvcLib_cppflags) $(lib_ZddGeomSvcLib_cppflags) $(ZddGeoGeneral_cppflags) $(ZddGeoGeneral_cxx_cppflags)  $(src)ZddGeoGeneral.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ZddGeomSvcLibclean
	@cd .

ifndef PEDANTIC
.DEFAULT::
	@echo "WARNING >> You should provide a target for $@"
else
.DEFAULT::
	@echo "PEDANTIC MODE >> You should provide a target for $@"
	@if test `expr index $@ '.'` != 0 ; \
	then  echo "PEDANTIC MODE >> This target seems to be a missing file, please check"; exit -1 ;\
	else echo "PEDANTIC MODE >> Just ignore it ; as it seems to be just a fake target due to some pattern" ; exit 0; fi; 		
endif

ZddGeomSvcLibclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(binobj)$(library_prefix)ZddGeomSvcLib$(library_suffix).a $(binobj)$(library_prefix)ZddGeomSvcLib$(library_suffix).s? $(binobj)ZddGeomSvcLib.stamp $(binobj)ZddGeomSvcLib.shstamp
#-- end of cleanup_library ---------------

