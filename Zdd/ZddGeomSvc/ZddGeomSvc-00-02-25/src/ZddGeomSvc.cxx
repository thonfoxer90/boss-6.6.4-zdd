/*
 *    2004/09/10   Zhengyun You     Peking University
 *                 
 */

#include "ZddGeomSvc/ZddGeomSvc.h"
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/IInterface.h"
#include "GaudiKernel/StatusCode.h"
//#include "GaudiKernel/ISvcFactory.h"
#include "GaudiKernel/SvcFactory.h"
#include "GaudiKernel/MsgStream.h"

//static SvcFactory<ZddGeomSvc> s_factory;
//const ISvcFactory& ZddGeomSvcFactory = s_factory;
  
ZddGeomSvc::ZddGeomSvc( const std::string& name, ISvcLocator* svcloc ) : Service(name, svcloc) 
{
  //Declare the properties
  declareProperty("GeometryMode",m_Geometry=1);

}

StatusCode ZddGeomSvc::queryInterface (const InterfaceID& riid, void** ppvInterface ){
  
  if ( IID_IZddGeomSvc.versionMatch(riid) ) { 
    *ppvInterface = static_cast<IZddGeomSvc*> (this); 
  } else { 
    return Service::queryInterface(riid, ppvInterface) ; 
  }
  return StatusCode::SUCCESS;
}

StatusCode ZddGeomSvc::initialize ( ) {
  MsgStream log(messageService(), name());
  log << MSG::INFO << name() << ": Start of run initialisation" << endreq;
  
  StatusCode sc = Service::initialize();
  if ( sc.isFailure() ) return sc;

  //  get geometry data
  Fill();
    
  return StatusCode::SUCCESS;
}
  
  
StatusCode ZddGeomSvc::finalize ( ) {
  MsgStream log(messageService(), name());
  log << MSG::INFO << name() << ": End of Run" << endreq;
  return StatusCode::SUCCESS;
}


ZddGeomSvc::~ZddGeomSvc(){
}

void ZddGeomSvc::Fill(){ 
  if(m_Geometry==1){
    m_pZddGeoGeneral = ZddGeoGeneral::Instance();
    //    m_pZddGeoGeneral->Init();  
    //m_pZddGeoGeneral->InitFromASCII();
    //    m_pZddGeoGeneral->InitFromXML();  
  } else{ //geant4 geo 
    m_pZddG4Geo = ZddG4Geo::Instance();
  }
}

const ZddGeoGeneral * const 
ZddGeomSvc::GetGeoGeneral()
{
  return m_pZddGeoGeneral;
}

const ZddGeoCrystal * const 
ZddGeomSvc::GetCrystal(int partID, int detectorID, int crystalNo)
{
  return m_pZddGeoGeneral->GetCrystal(partID, detectorID, crystalNo);
}

const ZddG4Geo * const     //for geant4 geo 2006.11.23
ZddGeomSvc::GetZddG4Geo()
{
  return m_pZddG4Geo;
}
