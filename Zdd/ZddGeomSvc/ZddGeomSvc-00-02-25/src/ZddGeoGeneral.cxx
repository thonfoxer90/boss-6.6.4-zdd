//$id$
//
//$log$

/*
 *    2003/08/30   Zhengyun You     Peking University
 * 
 *    2004/09/09   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

using namespace std;

#include <fstream>
#include <iostream>
#include <strstream>
#include <vector>
//#include "TRint.h"
//#include <TROOT.h>
//#include <TApplication.h>
#include <TGeoManager.h>

#include "ZddGeomSvc/ZddGeoGeneral.h"
#include "ROOTGeo/ZddROOTGeo.h"
//#include "TGDMLProcessor.h"
//#include "SAXProcessor.h"
//#include "ProcessingConfigurator.h"

int 
ZddGeoGeneral::m_gGeometryInit = 0;

ZddGeoGeneral* 
ZddGeoGeneral::m_gpZddGeoGeneral = 0L;

map<Identifier, ZddGeoCrystal*>
ZddGeoGeneral::m_gpZddGeoCrystal = map<Identifier, ZddGeoCrystal*>();


ZddGeoGeneral::ZddGeoGeneral()
{ 
  // Default constructor.
}

ZddGeoGeneral::~ZddGeoGeneral()
{ 
  // Destructor.

}

ZddGeoGeneral& 
ZddGeoGeneral::operator=(const ZddGeoGeneral& orig)
{
  // Assignment operator.
  if ( this != &orig ) {             // Watch out for self-assignment!
    m_gpZddGeoGeneral = orig.m_gpZddGeoGeneral;
  }
  return *this;
}

/*
void
ZddGeoGeneral::Init()
{
  for(unsigned int part = 0; part < ZddID::getPartNum(); part++) {
    for(unsigned int seg = 0; seg < ZddID::getSegMax(); seg++) {
      for(unsigned int gap = 0; gap < ZddID::getGapMax(); gap++) {
	m_StripNumInGap[part][seg][gap] = 0;
      }
    }
  }
}

void 
ZddGeoGeneral::InitFromXML()
{
  //new TRint("ROOT GDML converter", 0, 0);
  
  // Initialize Bes Zdd Geometry for XML files.
  bool geomanager = true;
  if (!gGeoManager) {
    gGeoManager = new TGeoManager("BesGeo", "Bes geometry");
    geomanager = false;
  }
  //gGeoManager->SetVisOption(0);  // to show all visible volumes.
  //gGeoManager->SetVisLevel(5);
  
  ZddROOTGeo *m_ZddROOTGeo = new ZddROOTGeo();
  
  TGeoVolume *volZdd = m_ZddROOTGeo->GetVolumeZdd();
  if(volZdd) std::cout << "Construct Zdd from Zdd.gdml" << std::endl;
  else       std::cout << "volume Zdd not found " << std::endl;

  float m_BesR = 5200;
  float m_BesZ = 5680;
  TGeoIdentity *identity = new TGeoIdentity();
  
  TGeoMaterial *mat = new TGeoMaterial("VOID",0,0,0);
  TGeoMedium   *med = new TGeoMedium("MED",1,mat);
  TGeoVolume   *m_Bes = gGeoManager->MakeBox("volBes", med, 0.5*m_BesR, 0.5*m_BesR, 0.5*m_BesZ);
  gGeoManager->SetTopVolume(m_Bes);
  m_Bes->AddNode(volZdd, 0, identity); 
  m_ZddROOTGeo->SetChildNo(m_Bes->GetNdaughters()-1); 
  
  gGeoManager->SetDrawExtraPaths();  // draw physical node or not;
  if(!geomanager)gGeoManager->CloseGeometry();
  gGeoManager->SetNsegments(20);

  m_ZddROOTGeo->SetPhysicalNode();

  for (int part = 0; part < m_ZddROOTGeo->GetPartNum(); part++) {
    for (int seg = 0; seg < m_ZddROOTGeo->GetSegNum(part); seg++) {
      for (int gap = 0; gap < m_ZddROOTGeo->GetGapNum(part); gap++) {
	Identifier gapID = ZddID::channel_id(part,seg,gap,0);

	float ironThickness = 0.0;
	if (part == 1) {
	  if (gap > 0) ironThickness = m_ZddROOTGeo->GetAbsorberThickness(part, seg, gap-1);
	}
	else {
	  ironThickness = m_ZddROOTGeo->GetAbsorberThickness(part, seg, gap);
	}
	//std::cout << "part " << part << " seg " << seg << " gap " << gap << " thick " << ironThickness << std::endl;

	int orient = 0;
	if ( (part == 1 && gap % 2 == 0) || (part != 1 && gap % 2 == 1) ) orient = 1;
 	ZddGeoGap *pGap = new ZddGeoGap(part, seg, gap, orient, 0, m_ZddROOTGeo->GetPhysicalGap(part, seg, gap), ironThickness); 
	m_gpZddGeoGap[gapID] = pGap;

	for (int strip = 0; strip < m_ZddROOTGeo->GetStripNum(part, seg, gap); strip++) {
	  Identifier stripID = ZddID::channel_id(part,seg,gap,strip);
	  
     	  ZddGeoStrip* pStrip = m_gpZddGeoGap[gapID]->AddStrip(strip);
	  pStrip->SetStrip(m_ZddROOTGeo->GetPhysicalStrip(part, seg, gap, strip));
	  m_gpZddGeoStrip[stripID] = pStrip;
	}
      }
    }
  }

  m_gGeometryInit = 1;  
}

void 
ZddGeoGeneral::InitFromASCII()
{
  // Initialize Bes ZDD Geometry for ASCII files.
  string gapSizeFile   = "zdd-gap-size.dat";
  string gapGeomFile   = "zdd-gap-geom.dat";
  string stripSizeFile = "zdd-strip-size.dat";
  string stripGeomFile = "zdd-strip-geom.dat";

  static const int bufSize=512;
  char lineBuf[bufSize];

  //
  // File gapSizeFile contains the gap sizes.
  //

  ifstream ifGapSize(gapSizeFile.c_str());
  if(!ifGapSize) {
    cout << "error opening gap size data file : "
	 << gapSizeFile 
	 << endl;
    return;
  }

  int part, seg, gap, strip, orient, panel;
  float xGapTemp, yGapTemp, zGapTemp;
  float xGapSize[m_kPartNum][m_kSegMax][m_kGapMax];
  float yGapSize[m_kPartNum][m_kSegMax][m_kGapMax];
  float zGapSize[m_kPartNum][m_kSegMax][m_kGapMax];;

  // Read the data line by line until we reach EOF.

  while ( ifGapSize.getline(lineBuf,bufSize,'\n')) {
    if (ifGapSize.gcount() > bufSize) {
      cout << "input buffer too small!  gcount = " << ifGapSize.gcount()
	   << endl;
      return;
      }
    
    istrstream stringBuf(lineBuf,strlen(lineBuf));
      
    if (stringBuf >> part >> seg >> gap >> xGapTemp >> yGapTemp >> zGapTemp) {
      xGapSize[part][seg][gap] = xGapTemp;
      yGapSize[part][seg][gap] = yGapTemp;
      zGapSize[part][seg][gap] = zGapTemp;

      //cout << part << "  " << seg << "  " << gap << "  " 
      //   << " x " << xGapSize[part][seg][gap]
      //   << " y " << yGapSize[part][seg][gap]
      //     << " z " << zGapSize[part][seg][gap]
      //   << endl;
    }
    else {
      // Skip any header or comment lines.
      // cout << "read comment line" << endl;
    }
  }
  
  ifGapSize.close();
  
  //
  // File stripSizeFile contains the strip sizes.
  //

  ifstream ifStripSize(stripSizeFile.c_str());
  if (!ifStripSize) {
    cout << "error opening strip size data file : "
	 << stripSizeFile 
	 << endl;
    return;
  }

  float xStripTemp, yStripTemp, zStripTemp;
  float xStripSize[m_kPartNum][m_kSegMax][m_kGapMax][m_kStripMax];
  float yStripSize[m_kPartNum][m_kSegMax][m_kGapMax][m_kStripMax];
  float zStripSize[m_kPartNum][m_kSegMax][m_kGapMax][m_kStripMax];

  // Read the data line by line until we reach EOF.

  while (ifStripSize.getline(lineBuf,bufSize,'\n')) {

    if (ifStripSize.gcount() > bufSize) {
      cout << "input buffer too small!  gcount = " << ifStripSize.gcount()
	   << endl;
      return;
    }

    istrstream stringBuf(lineBuf,strlen(lineBuf));

    if (stringBuf >> part >> seg >> gap >> strip >> xStripTemp >> yStripTemp >> zStripTemp) {
      xStripSize[part][seg][gap][strip] = xStripTemp;
      yStripSize[part][seg][gap][strip] = yStripTemp;
      zStripSize[part][seg][gap][strip] = zStripTemp;

      m_StripNumInGap[part][seg][gap]++;
      //cout << part << "  " << seg << "  " << gap << "  " 
      //     << strip << "  " 
      //     << " x " << xStripSize[part][seg][gap][strip]
      //     << " y " << yStripSize[part][seg][gap][strip]
      //     << " z " << zStripSize[part][seg][gap][strip]
      //     << endl;
    }
    else {
      // Skip any header or comment lines.
      //       cout << "read comment line" << endl;
    }
  }

  ifStripSize.close();

  //for(int part = 0; part < ZddSoftID::m_kPart; part++) {
  //for(int seg = 0; seg < ZddSoftID::m_kSegInPartMax; seg++) {
  //  for(int gap = 0; gap < ZddSoftID::m_kGapInSegMax; gap++) {
  //cout << "nStrips In part " << part << " seg " << seg << " gap " << gap << " is " 
  //     << m_NStripInGap[part][seg][gap] << endl;
  //  }
  //}
  //}

  //
  // File gapGeomFile contains the gap positions, etc.
  //

  ifstream ifGapGeom(gapGeomFile.c_str());
  if (!ifGapGeom) {
    cout << "error opening gap geometry data file : " 
	 << gapGeomFile
	 << endl;
    return;
  }

  float xGapTarg1, yGapTarg1, zGapTarg1;
  float xGapTarg2, yGapTarg2, zGapTarg2;
  float xGapTarg3, yGapTarg3, zGapTarg3;
  float dzHighEdge;

  float dzFarFrontGas, dzNearFrontGas;
  float dzFarBackGas, dzNearBackGas;

  float dxTarget1ToFiducial, dyTarget1ToFiducial;
  float dxFiducialToCenter,  dyFiducialToCenter;

  Identifier gapID   = ZddID::channel_id(0,0,0,0);
  Identifier stripID = ZddID::channel_id(0,0,0,0);

  // Read the gap geometry data line by line until we reach EOF.

  while (ifGapGeom.getline(lineBuf,bufSize,'\n')) {
    
    if ( ifGapGeom.gcount() > bufSize ) {
      cout << "input buffer too small!  gcount = " << ifGapGeom.gcount()
	   << endl;
      return;
    }

    istrstream stringBuf(lineBuf,strlen(lineBuf));

    if ( stringBuf >> part >> seg >> gap >> orient
	 >> xGapTarg1 >> yGapTarg1 >> zGapTarg1
	 >> xGapTarg2 >> yGapTarg2 >> zGapTarg2
	 >> xGapTarg3 >> yGapTarg3 >> zGapTarg3
	 >> dzHighEdge 
	 >> dzFarFrontGas >> dzNearFrontGas
	 >> dzNearBackGas >> dzFarBackGas
	 >> dxTarget1ToFiducial
	 >> dyTarget1ToFiducial
	 >> dxFiducialToCenter
	 >> dyFiducialToCenter ) {
      
      //cout << " " << part << " " << seg << "  " << gap 
      //   << " " << xGapTarg1 << " " << yGapTarg1 << " " << zGapTarg1
      //   << " " << xGapTarg2 << " " << yGapTarg2 << " " << zGapTarg2
      //   << " " << xGapTarg3 << " " << yGapTarg3 << " " << zGapTarg3
      //   << " " << dzHighEdge 
      //   << " " << dzFarFrontGas << " " << dzNearFrontGas
      //   << " " << dzFarBackGas  << " " << dzNearBackGas
      //   << " " << dxTarget1ToFiducial << " " << dyTarget1ToFiducial
      //   << " " << dxFiducialToCenter  << " " << dyFiducialToCenter
      //   << endl;
      
      gapID = ZddID::channel_id(part,seg,gap,0);
      
      ZddGeoGap *pGap = new ZddGeoGap(part, seg, gap,
				      orient, 0,
				      xGapSize[part][seg][gap],
				      yGapSize[part][seg][gap],
				      zGapSize[part][seg][gap],
				      xGapTarg1, yGapTarg1, zGapTarg1,
				      xGapTarg2, yGapTarg2, zGapTarg2,
				      xGapTarg3, yGapTarg3, zGapTarg3,
				      dzHighEdge,
				      dzFarFrontGas, dzNearFrontGas,
				      dzNearBackGas, dzFarBackGas,  
				      dxTarget1ToFiducial,
				      dyTarget1ToFiducial,
				      dxFiducialToCenter,
				      dyFiducialToCenter);
      m_gpZddGeoGap[gapID] = pGap;
    }
    else {
      // Skip any header or comment lines.
      //      cout << "read comment line" << endl;
    }

  }

  ifGapGeom.close();
 
  //
  // File stripGeomFile contains the strip positions, etc.
  //

  ifstream ifStripGeom(stripGeomFile.c_str());
  if (!ifStripGeom) {
    cout << "error opening strip geometry data file" 
	 << stripGeomFile
	 << endl;
    return;
  }
  
  // Read the strip geometry data line by line until we reach EOF.

  float xStripTarg1, yStripTarg1, xStripTarg2, yStripTarg2;

  while (ifStripGeom.getline(lineBuf,bufSize,'\n')) {

    if (ifStripGeom.gcount() > bufSize) {
      cout << "input buffer too small!  gcount = " << ifStripGeom.gcount()
	   << endl;
      return;
    }

    istrstream stringBuf(lineBuf,strlen(lineBuf));

    if (stringBuf >> part >> seg >> gap >> strip >> panel
	>> xStripTarg1 >> xStripTarg2
	>> yStripTarg1 >> yStripTarg2) {
      
      //       cout << part << " " << seg << " " << gap    << " " 
      //            << strip << " " << panel << " " << orient << " "
      //            << xStripTarg1 << " " << xStripTarg2 << " "
      //            << yStripTarg1 << " " << yStripTarg2
      //            << endl;

      ZddGeoStrip* pStrip = 0;
      stripID = ZddID::channel_id(part,seg,gap,strip);
      gapID   = ZddID::channel_id(part,seg,gap,0);

      if (!m_gpZddGeoStrip[stripID]) {
	if (m_gpZddGeoGap[gapID]) {
	  pStrip = m_gpZddGeoGap[gapID]->AddStrip(strip);
	  pStrip->SetStrip(xStripTarg1, xStripTarg2,
			   yStripTarg1, yStripTarg2,
			   xStripSize[part][seg][gap][strip],
			   yStripSize[part][seg][gap][strip],
			   zStripSize[part][seg][gap][strip]);
	  m_gpZddGeoStrip[stripID] = pStrip;
	}
	else {
	  cout << "missing gap" << gapID << endl;
	  continue;
	}
      }

    }
    else {
      // Skip any header or comment lines.
      //       cout << "read comment line" << endl;
    }

  }

  ifStripGeom.close();

  m_gGeometryInit = 1;  
}
*/

ZddGeoGeneral* 
ZddGeoGeneral::Instance()
{
  // Get a pointer to the single instance of ZddGeoGeneral.
  if(!m_gpZddGeoGeneral) {
    m_gpZddGeoGeneral = new ZddGeoGeneral;
  }

  return m_gpZddGeoGeneral;
}

ZddGeoCrystal*
ZddGeoGeneral::GetCrystal(const int partID,
		      const int detectorID,
		      const int crystalNo) const
{
  // Get a pointer to the gap identified by (part,seg,gap).
  Identifier crystalID = ZddID::cell_id(partID, detectorID, crystalNo);

  return m_gpZddGeoCrystal[crystalID];
}

ZddGeoCrystal*
ZddGeoGeneral::GetCrystal(const Identifier id) const
{
  // Get a pointer to the gap identified by Identifier.
  Identifier crystalNo = ZddID::cell_id(ZddID::partID(id), 
				       ZddID::detectorID(id),
				       ZddID::crystalNo(id)
					);
  
  return m_gpZddGeoCrystal[crystalNo];
}

