//$id$
//
//$log$

/*
 *    2003/08/30   Zhengyun You     Peking University
 * 
 *    2004/09/09   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

#include <iostream>
#include <vector>
#include "TGeoBBox.h"

#include "ZddGeomSvc/ZddGeoCrystal.h"
#include "ZddGeomSvc/ZddGeometron.h"
#include "ZddGeomSvc/ZddConstant.h"
#include "Identifier/ZddID.h"

using namespace std;

ZddGeoCrystal::ZddGeoCrystal()
  : m_PartID(-99),
    m_DetectorID(-99),
    m_CrystalNo(-99)
{
  // Default constructor.
}

ZddGeoCrystal::ZddGeoCrystal(const int partID,
		     const int detectorID,
		     const int crystalNo)
  : m_PartID(partID),
    m_DetectorID(detectorID),
    m_CrystalNo(crystalNo)
{
}
/*
/// ROOT constructor.
ZddGeoCrystal::ZddGeoCrystal(const int part,
		     const int seg,
		     const int gap,
		     const int orient,
		     const int stripNum,
		     const TGeoPhysicalNode *gapPhysicalNode,
		     const float ironThickness)
  : m_Part(part),
    m_Seg(seg),
    m_Gap(gap),
    m_StripNum(stripNum),
    m_Orient(orient),
    m_pZddGeoStrip(ZddID::getStripNum(part, seg, gap)),
    m_IronThickness(ironThickness)

{
}
*/

ZddGeoCrystal& 
ZddGeoCrystal::operator=(const ZddGeoCrystal &orig)
{
  // Assignment operator.
  if (this != &orig) {             // Watch out for self-assignment!
    m_PartID       = orig.m_PartID;
    m_DetectorID   = orig.m_DetectorID;
    m_CrystalNo    = orig.m_CrystalNo;
  }
  return *this;
}

ZddGeoCrystal::ZddGeoCrystal(const ZddGeoCrystal &orig)
  : m_PartID(orig.m_PartID),
    m_DetectorID(orig.m_DetectorID),
    m_CrystalNo(orig.m_CrystalNo)

{ 
  // Copy constructor.
}
				 
ZddGeoCrystal::~ZddGeoCrystal()
{ 
  // Destructor.
 }

/*
ZddGeoStrip* 
ZddGeoGap::GetStrip(const int strip) const
{
  // Point to a strip within this gap.
  if ( strip < 0 || strip >= int(m_pZddGeoStrip.size()) ) {
    std::cout << "Error: ZddGeoGap::GetStrip() strip " << strip << " exceed strip range" << endl;
    return 0;
  }
  if ( !m_pZddGeoStrip[strip] ) {
    std::cout << "Error: ZddGeoGap::GetStrip() strip " << strip << " not found" << endl;
    return 0;
  }
  
  return m_pZddGeoStrip[strip];
}

void
ZddGeoGap::GetSize(float &xSize, float &ySize, float &zSize) const
{
  // Get size of this gap.
  xSize = m_XSize;
  ySize = m_YSize;
  zSize = m_ZSize;

  return;
}

void 
ZddGeoGap::GetRotationMatrix(float &thetaX, float &phiX,
			     float &thetaY, float &phiY,
			     float &thetaZ, float &phiZ) const
{
  // Get the rotation angles (in degrees) of the gap in the global coordinate system.
  Hep3Vector x(m_Rotation.colX());
  Hep3Vector y(m_Rotation.colY());
  Hep3Vector z(m_Rotation.colZ());

  thetaX = kRad * x.theta();
  phiX   = kRad * x.phi();
  thetaY = kRad * y.theta();
  phiY   = kRad * y.phi();
  thetaZ = kRad * z.theta();
  phiZ   = kRad * z.phi();

  // cout << "Gap rotation matrix : " << endl
  //      << "X:  theta = " << thetaX << "  phi = " << phiX << endl
  //      << "Y:  theta = " << thetaY << "  phi = " << phiY << endl
  //      << "Z:  theta = " << thetaZ << "  phi = " << phiZ << endl;

  return;
}


int
ZddGeoGap::GuessStrip(const float x,
		      const float y,
		      const float ) const
{
  // Given a position (gap coordinate), find the approximate strip containing the position, as there are intervals between the strips.
  float x0, y0, z0;  // XYZ position of the strip NO min.
  float xn, yn, zn;  // XYZ position of the strip NO max.
  float dx;          // Approximate width of a strip and a interval.
  int n, iGuess;           

  n = m_StripNum - 1;

  GetStrip(0)->GetCenterPos(x0, y0, z0);
  GetStrip(n)->GetCenterPos(xn, yn, zn);

  //cout << n << endl;
  //cout << "x0 " << x0 << " xn " << xn << " x " << x << endl;
  //cout << "y0 " << y0 << " yn " << yn << " y " << y << endl;
  //cout << "orient " << m_Orient << endl;

  if(m_Orient == 0){
    dx = (yn-y0)/n;
    iGuess = int ((y-y0)/dx+0.5);
    return iGuess;
  } 
  else{
    dx = (xn-x0)/n;
    iGuess = int ((x-x0)/dx+0.5);
    return iGuess;
  }
}

HepPoint3D
ZddGeoGap::ProjectToGap(const HepPoint3D gPoint,
			const Hep3Vector gDirection) const
{
  // Given a line by basepoint gPoint and direction gDirection,
  // find the intersection with the gap (in global coordinate).
  Hep3Vector gapVect  = m_Rotation.colZ();
  HepPoint3D gCross(0,0,0);

  HepPlane3D plane(gapVect, m_Center);
  ZddGeometron geometron;
  //bool getPoint = 
  geometron.GetIntersectionLinePlane(gPoint, gDirection, plane, gCross);

  return gCross;
}

HepPoint3D
ZddGeoGap::ProjectToGapWithSigma(const HepPoint3D gPoint,
    const Hep3Vector gDirection,
    const HepPoint3D gPointSigma,
    const Hep3Vector gDirectionSigma,
    HepPoint3D &gCross,
    HepPoint3D &gCrossSigma) const
{
  Hep3Vector gapVect  = m_Rotation.colZ();

  HepPlane3D plane(gapVect, m_Center);
  ZddGeometron geometron;
  geometron.GetIntersectionLinePlaneWithSigma(gPoint, gDirection, gPointSigma, gDirectionSigma, plane, gCross, gCrossSigma);


  return gCross;
} 


void
ZddGeoGap::ProjectToGapSurface(const HepPoint3D gPoint,
			       const Hep3Vector gDirection,
			       HepPoint3D& gCross1,
			       HepPoint3D& gCross2 ) const
{
  // Given a line by basepoint gPoint and direction gDirection,
  // find the intersection with the gap (in global coordinate).
  Hep3Vector gapVect  = m_Rotation.colZ();

  HepPlane3D planeInner(gapVect, m_SurfaceInner);
  HepPlane3D planeOuter(gapVect, m_SurfaceOuter);
  ZddGeometron geometron;
  //bool getPoint = 

  geometron.GetIntersectionLinePlane(gPoint, gDirection, planeInner, gCross1);
  geometron.GetIntersectionLinePlane(gPoint, gDirection, planeOuter, gCross2);

}

HepPoint3D
ZddGeoGap::ProjectToGapQuadLocal(const int part,      //liangyt 2009.3.12 
			const int orient,
                        const float a,    //y = ax*x + b*x +c;
                        const float b,
                        const float c,
                        const int whichhalf,
                        HepPoint3D& gCross1,
                        HepPoint3D& gCross2) const
{
  Hep3Vector gapVect, center_local;
  if(part == 1 && orient == 1)  {gapVect  = m_Rotation.colZ(); center_local = m_Center;}
  else if(part == 1 && orient == 0)  {//in this orientation, the fitting is done in local coordinate.
     gapVect.setX(0); gapVect.setY(1); gapVect.setZ(0);
     center_local.setX(0); center_local.setY(m_Center.mag()); center_local.setZ(0);
  }
  else  {//in this orientation, the fitting is done in local coordinate. 
     gapVect.setX(1); gapVect.setY(0); gapVect.setZ(0);
     center_local.setX(m_Center.z()); center_local.setY(0); center_local.setZ(0);	
  }

  //cout<<"in zddgeogap: part = "<<part<<" o: "<<orient<<" gapvect: "<<gapVect<<"  center = "<<m_Center<<endl;
  //cout<<"in zddgeogap: center local = "<<center_local<<endl;

  HepPlane3D plane(gapVect, center_local);

  ZddGeometron geometron;
  geometron.GetIntersectionQuadPlaneLocal(part,orient, a, b, c, plane, gCross1, gCross2);


}


HepPoint3D
ZddGeoGap::ProjectToGap(const HepPoint3D gPoint,      //liangyt 2007.4.9 
			const float vy,
			const float y0,
			const float a,    //y = ax*x + b*x +c;
			const float b,
			const float c,
			const int whichhalf,
			HepPoint3D& gCross1,
			HepPoint3D& gCross2) const
{
  // Given a line by basepoint gPoint and direction gDirection,
  // find the intersection with the gap (in global coordinate).
  Hep3Vector gapVect  = m_Rotation.colZ();

  HepPlane3D plane(gapVect, m_Center);
  ZddGeometron geometron;
  //bool getPoint = 
  geometron.GetIntersectionQuadPlane(gPoint, vy, y0, a, b, c, plane, gCross1, gCross2);

  HepPoint3D localCross = TransformToGap(gCross1);

  bool found = false;
  bool found1 = false;
  bool found2 = false;
  HepPoint3D temp;
  int good = 0;

  if ( IsInGap(localCross.x(),
	       localCross.y(),
	       localCross.z()) ) {
    good = 1;
    found1 = true;
  }

  localCross = TransformToGap(gCross2);
  if ( IsInGap(localCross.x(),
	       localCross.y(),
	       localCross.z()) ) {
    good = 2;
    found2 = true;
  }
  if(found1&&found2) {
    float center = b/(-2*a);
    if(whichhalf==2){
      if(gCross1.x()>center) good = 1;
      if(gCross2.x()>center) good = 2;
    }
    if(whichhalf==1){
      if(gCross1.x()<center) good = 1;
      if(gCross2.x()<center) good = 2;
    }
  }
  if(good == 2) {temp = gCross1; gCross1 = gCross2; gCross2 = temp;}  //keep gCross1 be the good one!
  //cout<<"detail: "<<whichhalf<<" "<<gCross1<<" & "<<gCross2<<" "<<a<<" "<<b<<" "<<c<<endl;

  return gCross1;
}


void
ZddGeoGap::ProjectToGapSurface(const HepPoint3D gPoint,      //liangyt 2007.4.9 
			       const float vy,
			       const float y0,
			       const float a,    //y = ax*x + b*x +c;
			       const float b,
			       const float c,
			       const int whichhalf,
			       HepPoint3D& gCross1,
			       HepPoint3D& gCross2) const
{
  // Given a line by basepoint gPoint and direction gDirection,
  // find the intersection with the gap (in global coordinate).

  HepPoint3D cross1, cross2, cross3, cross4;
  
  Hep3Vector gapVect  = m_Rotation.colZ();

  HepPlane3D planeInner(gapVect, m_SurfaceInner);
  HepPlane3D planeOuter(gapVect, m_SurfaceOuter);

  ZddGeometron geometron;
  //bool getPoint = 
  geometron.GetIntersectionQuadPlane(gPoint, vy, y0, a, b, c, planeInner, cross1, cross2);
  geometron.GetIntersectionQuadPlane(gPoint, vy, y0, a, b, c, planeOuter, cross3, cross4);

  gCross1 = CompareIntersection(whichhalf, cross1, cross2, a, b, c);
  gCross2 = CompareIntersection(whichhalf, cross3, cross4, a, b, c);

}

HepPoint3D
ZddGeoGap::CompareIntersection(const int whichhalf, const HepPoint3D gCross1,
			       const HepPoint3D gCross2, const float a, 
			       const float b, const float c )const
{
  bool found = false;
  bool found1 = false;
  bool found2 = false;
  int good = 0;

  HepPoint3D localCross = TransformToGap(gCross1);
  if ( IsInGap(localCross.x(),
	       localCross.y(),
	       0.0) ) {
    good = 1;
    found1 = true;
  }

  localCross = TransformToGap(gCross2);
  if ( IsInGap(localCross.x(),
	       localCross.y(),
	       0.0) ) {
    good = 2;
    found2 = true;
  }
  if(found1&&found2) {
    float center = b/(-2*a);
    if(whichhalf==2){
      if(gCross1.x()>center) good = 1;
      if(gCross2.x()>center) good = 2;
    }
    if(whichhalf==1){
      if(gCross1.x()<center) good = 1;
      if(gCross2.x()<center) good = 2;
    }
  }

  if(good == 1) return gCross1;
  else if(good == 2) return gCross2;
  else {
    HepPoint3D Empty(-9999,-9999,-9999);
    cout<<"in ZddGeoGap:: both intersection position are bad!!!"<<endl; return Empty;
  }

}


Hep3Vector 
ZddGeoGap::RotateToGlobal(const Hep3Vector pVect) const
{
  // Rotate a vector from gap coordinate to Bes global coordinate.
  return m_Rotation * pVect;
}

Hep3Vector
ZddGeoGap:: RotateToGap(const Hep3Vector gVect) const 
{
  // Rotate a vector from Bes global coordinate to gap coordinate.
  return m_RotationT * gVect;
}

HepPoint3D 
ZddGeoGap::TransformToGlobal(const HepPoint3D pPoint) const
{
  // Transform a point from gap coordinate to Bes global coordinate.
  return m_Rotation * pPoint + m_Translation;
}

HepPoint3D
ZddGeoGap::TransformToGap(const HepPoint3D gPoint) const
{
  // Transform a point from Bes global coordinate to gap coordinate.
  return m_RotationT * (gPoint - m_Translation);
}

bool 
ZddGeoGap::IsInGap(const float x,
		   const float y,
		   const float z) const
{
  // Check if the point (given in gap coordinate) is within the gap boundary.
  return ( ( x > -0.5*m_XSize - kGapEdge ) && ( x < 0.5*m_XSize + kGapEdge ) &&
	   ( y > -0.5*m_YSize - kGapEdge ) && ( y < 0.5*m_YSize + kGapEdge ) &&
	   ( z > ( m_dzHighEdge - m_ZSize ) ) && ( z < m_dzHighEdge ) );
}

ZddGeoStrip* 
ZddGeoGap::AddStrip(const int strip)
{ 
  // Add a strip to the gap.
  ZddGeoStrip *pStrip;
  ZddGeoStrip *neighbor;

  if( strip >= int(m_pZddGeoStrip.size()) ) {
    cout << " ZddGeoGap::AddStrip  strip number "
	 << strip << " outside of expected range " 
	 << m_pZddGeoStrip.size() 
	 << endl;
    return 0;
  }

  if( (strip + 1) > m_StripNum ) m_StripNum = strip + 1;

  if(m_pZddGeoStrip[strip]) {
    // The strip object has already been created; don't create a new one.
    return m_pZddGeoStrip[strip];
  }

  pStrip = new ZddGeoStrip(strip,  this);
  
  m_pZddGeoStrip[strip] = pStrip;
  pStrip->SetLeftNeighbor(0L);
  pStrip->SetRightNeighbor(0L);
  
  // Make this strip and the previous one neighbors.
  if ( strip > 0 ) {
    neighbor = m_pZddGeoStrip[strip-1];
    if(neighbor) {
      neighbor->SetRightNeighbor(pStrip);
      pStrip->SetLeftNeighbor(neighbor);
    }
  }
  
  return pStrip;
}
*/

ostream&
operator << (ostream& s, const ZddGeoCrystal&  crystal)
{
  s << " Identifier : " << crystal.PartID() << " " << crystal.DetectorID() << " " << crystal.CrystalNo() << endl;

  return s;
}


