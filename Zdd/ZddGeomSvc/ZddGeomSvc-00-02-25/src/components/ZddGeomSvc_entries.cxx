#include "ZddGeomSvc/ZddGeomSvc.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
 
DECLARE_SERVICE_FACTORY( ZddGeomSvc )
  
DECLARE_FACTORY_ENTRIES( ZddGeomSvc ) { 
 DECLARE_SERVICE( ZddGeomSvc );
}
