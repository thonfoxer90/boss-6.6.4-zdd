//$id$
//
//$log$

/*
 *    2003/08/30   Zhengyun You     Peking University
 * 
 *    2004/09/09   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

using namespace std;

#include <iostream>

#include "ZddGeomSvc/ZddSoftID.h"

//const int 
//ZddSoftID::m_kPart = 3;

//const int 
//ZddSoftID::m_kSegInPartMax = 8;

const int 
ZddSoftID::m_kSegInPart[ZddSoftID::m_kPart] = {4, 8, 4};

//const int 
//ZddSoftID::m_kGapInSegMax = 9;

const int 
ZddSoftID::m_kGapInPart[ZddSoftID::m_kPart] = {8, 9, 8};

//const int 
//ZddSoftID::m_kStripInGapMax = 96;

const int 
ZddSoftID::m_kStripInGap[ZddSoftID::m_kPart][ZddSoftID::m_kGapInSegMax] = { {64, 64, 64, 64, 64, 64, 64, 64},
									    {48, 96, 48, 96, 48, 96, 48, 96, 48},
									    {64, 64, 64, 64, 64, 64, 64, 64} };

const int 
ZddSoftID::m_kSegTotal   = ZddSoftID::m_kSegInPartMax  * ZddSoftID::m_kPart;

const int 
ZddSoftID::m_kGapTotal   = ZddSoftID::m_kGapInSegMax   * ZddSoftID::m_kSegTotal;

const int
ZddSoftID::m_kStripTotal = ZddSoftID::m_kStripInGapMax * ZddSoftID::m_kGapTotal;

// Assume a maximum occupancy of 100% for now.
const int
ZddSoftID::m_kHitInGapMax = ZddSoftID::m_kStripInGapMax;

const int
ZddSoftID::m_kHitTotalMax = ZddSoftID::m_kStripTotal;


ZddSoftID::ZddSoftID(const int part,
		     const int seg,
		     const int gap,
		     const int strip)
  : m_Part(part),
    m_Seg(seg),
    m_Gap(gap),
    m_Strip(strip)
{
  // Constructor.
}

ZddSoftID& 
ZddSoftID::operator=(const ZddSoftID& orig)
{
  // Assignment operator.
  if (this != &orig) {             // Watch out for self-assignment!
    m_Part  = orig.m_Part;
    m_Seg   = orig.m_Seg;
    m_Gap   = orig.m_Gap;
    m_Strip = orig.m_Strip;
  }
  return *this;
}

ZddSoftID::ZddSoftID(const ZddSoftID& orig)
  : m_Part(orig.m_Part),
    m_Seg(orig.m_Seg),
    m_Gap(orig.m_Gap),
    m_Strip(orig.m_Strip)
{
  // Copy constructor.
}

bool
ZddSoftID::operator == (const ZddSoftID& id) const
{
  if( m_Part     == id.GetPart() 
      && m_Seg   == id.GetSeg() 
      && m_Gap   == id.GetGap() 
      && m_Strip == id.GetStrip() ) {
    return true;
  }
  else {
    return false;
  }
}

void
ZddSoftID::SetID(const int part,
		 const int seg,
		 const int gap,
		 const int strip)
{
  // Set the sub-identifier, m_GapID and m_StripID.
  m_Part  = part;
  m_Seg   = seg;
  m_Gap   = gap;
  m_Strip = strip;
}

ostream& operator << (ostream& s, const ZddSoftID& id)
{
  // Print ZddSoftID information to a stream.
  s << " part "   << id.GetPart()
    << " seg "    << id.GetSeg()
    << " gap "    << id.GetGap() 
    << " strip "  << id.GetStrip();

  return s;
}

size_t GapHash(const ZddSoftID& id)
{
  // Return the position of a ZddSoftID object in the GapGeo hash table.
  return  ( ( id.GetPart()
	      *ZddSoftID::m_kSegInPartMax + id.GetSeg() )
	    *ZddSoftID::m_kGapInSegMax + id.GetGap() );
}

size_t StripHash(const ZddSoftID& id)
{
  // Return the position of a ZddSoftID object in the StripGeo hash table.
  return ( ( ( id.GetPart()
	       *ZddSoftID::m_kSegInPartMax + id.GetSeg() )
	     *ZddSoftID::m_kGapInSegMax + id.GetGap() )
	   *ZddSoftID::m_kStripInGapMax + id.GetStrip() ); 
}
