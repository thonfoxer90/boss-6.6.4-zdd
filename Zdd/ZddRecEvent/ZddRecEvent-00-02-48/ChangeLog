
2010-04-02  Xie Yuguang <ygxie@ihep.ac.cn>
   *  Fix a bug in Endcap projection
   *  Make a correct for depth calcuation in Endcap
   
2010-03-25  Xie Yuguang <ygxie@ihep.ac.cn>
   *  Modify links for Geant4 and CLHEP update
   
2009-10-09  Xie Yuguang <ygxie@ihep.ac.cn>
   *  Comment some messages output

2009-10-08  Xie Yuguang <ygxie@ihep.ac.cn>
   *  fix a bug of calculation sequence in ComputeTrackInfo()
   
2009-09-30  Xie Yuguang <ygxie@ihep.ac.cn>
   *  Correct depth calculation according to different cases
   
2009-09-28  Xie Yuguang <ygxie@ihep.ac.cn>
   *  Initialize the depth parameter with -99 instead of 0

2009-09-17  Xie Yuguang <ygxie@ihep.ac.cn>
   *  Initialize the distance parameter with -9990 instead of -999
      (since there is unit change in OutputUnitChange() function)

2009-09-14  Xie Yuguang <ygxie@ihep.ac.cn>
   *  Initialize the distance parameter with -999 instead of 0
   
2009-03-19  Liang Yutie <liangyt@hep.pku.cn>
   *  fix a bug with big vr and change to set pos and mom before refit

2009-03-17  Liang Yutie <liangyt@hep.pku.cn>
   *  fix bugs in the calc of dir/pos of 3d road

2009-03-12  Liang Yutie <liangyt@hep.pku.cn>
   *  calc distance between hits and track with quad fitting

2009-03-05  Liang Yutie <liangyt@hep.pku.cn>
   *  fix a bug in the calc. of expected pos

2009-02-27  Liang Yutie <liangyt@hep.pku.cn>
   *  fix a bug in the initialize of m_distHits

2009-01-07  Liang Yutie <liangyt@hep.pku.cn>
   *  calc expected hit list without info. of current gap

2008-12-25  Liang Yutie <liangyt@hep.pku.cn>
   *  add program to record pad id of expected hits

2008-12-18  Liang Yutie <liangyt@hep.pku.cn>
   *  add varible m_IsSeed to record whether the hit is seed
   *  calculate expected hits to last layer

2008-08-25  Liang Yutie <liangyt@hep.pku.cn>
   *  fix a bug with z direction

2008-08-21  Liang Yutie <liangyt@hep.pku.cn>
   *  change SetZddHitCol for associate 2 rec algorith

2008-07-28  Liang Yutie <liangyt@hep.pku.cn>
   *  adjust mom according to ZDD rec.
   
2008-07-24  Liang Yutie <liangyt@hep.pku.cn>
   *  change weight of strips

2008-07-22  Liang Yutie <liangyt@hep.pku.cn>
   *  litte modification of simple fit

2008-07-12  Liang Yutie <liangyt@hep.pku.cn>
   *  add attach hit function without fit for calibration, only seeds are used
   to do fiting

2008-06-05  Liang Yutie <liangyt@hep.pku.cn>
   *  change a bug in calc expected hit list

2008-05-08  Liang Yutie <liangyt@hep.pku.cn>
   *  update for cosmic ray

2008-04-14  Liang Yutie <liangyt@hep.pku.cn>
   *  add protect for calculation of depth

2008-04-09  Liang Yutie <liangyt@hep.pku.cn>
   *  add protect for calculation of posx,y,z

2008-03-31  Liang Yutie <liangyt@hep.pku.cn>
   *  add protect for calculation of depth and chi2

2008-03-26  Liang Yutie <liangyt@hep.pku.cn>
   *  add protect for the unit change function

2008-03-24  Liang Yutie <liangyt@hep.pku.cn>
   *  change Unit, mm->cm, MeV->GeV

2008-03-11  Liang Yutie <liangyt@hep.pku.cn>
   *  fix a bug when do project

2008-03-06  Liang Yutie <liangyt@hep.pku.cn>
   *  add copy constructor from DstZddTrack
   *  add a list to record distance of attached hits and track.

2008-03-03  Liang Yutie <liangyt@hep.pku.cn>
   *  some modification because ExtTrack changed 

2008-01-24  Liang Yutie <liangyt@hep.pku.cn>
   *  add program to calculate sigma of position
   *  add expected hit list for calibration in strip level

2007-10-13  Liang Yutie <liangyt@hep.pku.cn>
	 *  change ZddTrack to inherit DstZddTrack, and add clID() funtion
	 
2007-06-06  Liang Yutie <liangyt@hep.pku.cn>
	 *  fix a bug in computing depth

2007-04-25  Liang Yutie <liangyt@hep.pku.cn>
	 *  add quadratic fitting method in barrel as magnetic field is notable.
	 *  add new method to compute depth with intersection position of a track (line or parabola) to surfaces of absorber.

2006-10-13  You Zhengyun  <youzy@hep.pku.cn>
         *  Comment out some debug info in ComputeDepth()

2005-03-20  Li Weidong  <liwd@ihep.ac.cn>
         *  Using CLHEP1.9


2005-12-15  You Zhengyun  <youzy@hep.pku.cn>
         *  Check the error of opposite reco direction in some event

2005-10-28  You Zhengyun  <youzy@hep.pku.cn>
         *  ZddRec2D,3DRoad and LineFit moved from ZddRecAlg to here

2005-10-23  You Zhengyun  <youzy@hep.pku.cn>
         *  Some functions added to ZddTrack

2005-05-18  You Zhengyun  <youzy@hep.pku.cn>
         *  First import
         *  Version Number : ZddRecEvent-00-00-01
