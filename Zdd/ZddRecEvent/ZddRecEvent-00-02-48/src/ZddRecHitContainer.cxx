//$id$
//
//$log$

/*
 *    2003/12/13   Zhengyun You     Peking University
 * 
 *    2004/09/12   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

#include "ZddRecEvent/ZddRecHitContainer.h"

// Constructor.
ZddRecHitContainer::ZddRecHitContainer()
{ 
}

// Destructor.
ZddRecHitContainer::~ZddRecHitContainer()
{
  //Clear();
}

// Initializes the ZddRecHitContainer.
void 
ZddRecHitContainer::Init()
{
  int crystalNumbers[12] = {1,2,3,4,11,12,13,14,21,22,23,24};

  int offset;
  for(int partID = 1; partID <=2; partID++)
    {
      for( int detectorID = 1; detectorID <= 2; detectorID++)
	{
	  if(detectorID == 1) offset = 100;
          if(detectorID == 2) offset = 200;
	  for( int i = 0; i < 12; i++)
	    {
	      ZddRecHitID crystalID(partID,detectorID,crystalNumbers[i]+offset) ;
	      m_CrystalHitCount[crystalID] = 0;	
	    }
	}
    }
  //  cout << "ZddRecHitContainer::Init-I1  hello world" << endl;

  Clear();

  //cout << "*********** Hit Container Initialized! *********** " << endl;
}

void 
ZddRecHitContainer::Clear()
{

  //m_pZddRecHitCol->clear();
  m_pHit.clear();
}

int 
ZddRecHitContainer::GetHitCount()
{
  int crystalNumbers[12] = {1,2,3,4,11,12,13,14,21,22,23,24};

  int count = 0;
  int offset;
  for(int partID = 1; partID <=2; partID++)
    {
      for( int detectorID = 1; detectorID <= 2; detectorID++)
        {
	  if(detectorID == 1) offset = 100;
          if(detectorID == 2) offset = 200;
          for( int i = 0; i < 12; i++)
            {
	      ZddRecHitID crystalID(partID,detectorID,crystalNumbers[i]+offset);
	      count += m_CrystalHitCount[crystalID];
            }
        }
    }
  return count;
}
int 
ZddRecHitContainer::GetCrystalHitCount(const ZddRecHitID crystalID)
{
  if (m_CrystalHitCount.count(crystalID)) return m_CrystalHitCount[crystalID];
  return 0;
}

int 
ZddRecHitContainer::GetCrystalHitCount(const int partID,
				   const int detectorID,
				   const int crystalNo)
{
  ZddRecHitID crystalID(partID, detectorID, crystalNo);
  return m_CrystalHitCount[crystalID];
}

ZddRecHit* 
ZddRecHitContainer::GetHit(const ZddRecHitID id)
{
  if (m_pHit.count(id)) return m_pHit[id];
  else return 0;
}

ZddRecHit* 
ZddRecHitContainer::GetHit(const int partID,
			   const int detectorID,
			   const int crystalNo,
			   const int hit)
{
  ZddRecHitID id(partID, detectorID, crystalNo, hit);
  return m_pHit[id];
}

ZddRecHit* 
ZddRecHitContainer::GetHitByIdentifier(const Identifier id)
{
  int partID  = ZddID::partID(id);
  int detectorID = ZddID::detectorID(id);
  int crystalNo  = ZddID::crystalNo(id);
  
  return GetHitByIdentifier(partID, detectorID, crystalNo);
}

ZddRecHit* 
ZddRecHitContainer::GetHitByIdentifier(const int partID,
				       const int detectorID,
				       const int crystalNo)

{
  Identifier id = ZddID::cell_id(partID, detectorID, crystalNo);
  for(int i = 0; i < GetCrystalHitCount(partID, detectorID, crystalNo); i++) {
    ZddRecHit *hit = GetHit( partID, detectorID, crystalNo, i );
    if(id == hit->GetID()) {
      return hit;
    }
  }
  
  return 0;
}

// Place a new hit object in the container.
void
ZddRecHitContainer::AddHit(const Identifier id)
{
  int partID  = ZddID::partID(id);
  int detectorID   = ZddID::detectorID(id);
  int crystalNo   = ZddID::crystalNo(id);

  AddHit(partID, detectorID, crystalNo);
}

// Place a new hit object in the container.
void
ZddRecHitContainer::AddHit(const int partID,
			   const int detectorID,
			   const int crystalNo)
{
  if ( (partID  >= 1) && (partID  <= 2) &&
       (detectorID  >= 1) && (detectorID <=2 ) &&
       (crystalNo   >= 101) && (crystalNo <=224 ) )
    {
      ZddRecHitID crystalID(partID, detectorID, crystalNo);
      //       cout << "ZddRecHitContainer::AddHit-I1  " << idGap << endl;
      
      int hitCount = m_CrystalHitCount[crystalID];
      ZddRecHitID id(partID, detectorID, crystalNo);
      //       cout << "ZddRecHitContainer::AddHit-I2  hit id = " << id << endl;
      
      ZddRecHit *pHit = new ZddRecHit(partID, detectorID, crystalNo);
      m_pZddRecHitCol->push_back(pHit);

      m_pHit[id] = pHit;
      if(!m_pHit[id]) {
	cout << "ZddRecHitContainer::AddHit-F1  failed to create new ZddRecHit" << endl;
      }
      else {
	m_CrystalHitCount[crystalID]++;
      }
    }
}

void
ZddRecHitContainer::SetZddRecHitCol(ZddRecHitCol* p)
{
  m_pZddRecHitCol = p;

  ZddRecHitCol::iterator izddhit;
  for(izddhit = p->begin();
      izddhit != p->end();
      izddhit++){
	int partID = (*izddhit)->PartID();
	int detectorID  = (*izddhit)->DetectorID();
	int crystalNo  = (*izddhit)->CrystalNo();

	
	ZddRecHitID crystalID(partID, detectorID, crystalNo);
	int hitCount = m_CrystalHitCount[crystalID];
	ZddRecHitID id(partID, detectorID, crystalNo, hitCount);

	m_pHit[id] = *izddhit;
	if(!m_pHit[id]) {
		cout << "ZddRecHitContainer::AddHit-F1  failed to create new ZddRecHit" << endl;
	}
	else {
		m_CrystalHitCount[crystalID]++;
	}


  }

}

