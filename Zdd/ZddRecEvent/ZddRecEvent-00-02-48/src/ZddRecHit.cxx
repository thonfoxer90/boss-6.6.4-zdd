//$id$
//
//$log$

/*
 *    2003/12/13   Zhengyun You     Peking University
 * 
 *    2004/09/12   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

#include "ZddRecEvent/ZddRecHit.h"

// IMPLEMENTATION OF ZddRecHit METHODS.  ================================

// Constructors.
ZddRecHit::ZddRecHit(const Identifier &id)
  : m_ZddID(id)
{
  m_pZddGeoCrystal   = ZddGeoGeneral::Instance()->GetCrystal(m_ZddID);
  m_IsSeed = false;
 }

ZddRecHit::ZddRecHit(const int partID,
		     const int detectorID,
		     const int crystalNo )
{
  m_ZddID = ZddID::cell_id(partID, detectorID, crystalNo );
  //cout << "part " << partID << " detectorID " << detectorID << " crystal No " << crystalNo << endl;

  m_pZddGeoCrystal   = ZddGeoGeneral::Instance()->GetCrystal(m_ZddID);
  //  m_IsSeed = false;
  
  //m_pZddGeoCrystal   = ZddGeoGeneral::Instance()->GetCrystal(partID, detectorID, crystalNo);


}

// Copy constructor.
ZddRecHit::ZddRecHit(const ZddRecHit& source)
  : m_ZddID(source.m_ZddID), 
    m_pZddGeoCrystal(source.m_pZddGeoCrystal)
{ 

}

// Assignment operator.
ZddRecHit& 
ZddRecHit::operator=(const ZddRecHit& orig)
{
  // Assignment operator.
  if (this != &orig) {             // Watch out for self-assignment!
    m_ZddID        = orig.m_ZddID;
    m_pZddGeoCrystal   = orig.m_pZddGeoCrystal;
  }
  return *this;
}

// Destructor.
ZddRecHit::~ZddRecHit()
{ 
  // No need to delete ZddGeo pointer objects; the allocation/deallocation
  // is done elsewhere.
  m_pZddGeoCrystal   = 0; // 0L
}


