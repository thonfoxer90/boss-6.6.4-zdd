//$id$
//
//$log$

/*
 *    2003/12/13   Zhengyun You     Peking University
 * 
 *    2004/09/12   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

#include "ZddRecEvent/ZddRecHitID.h"

// IMPLEMENTATION OF ZddRecHitID METHODS.  ================================

// Constructor.
ZddRecHitID::ZddRecHitID(const int partID,
			 const int detectorID,
			 const int crystalNo,
			 const int hit)
  : m_PartID(partID), m_DetectorID(detectorID), m_CrystalNo(crystalNo), m_Hit(hit)
{ }

// Copy constructor.
ZddRecHitID::ZddRecHitID(const ZddRecHitID& source)
  : m_PartID(source.m_PartID),
    m_DetectorID(source.m_DetectorID),
    m_CrystalNo(source.m_CrystalNo),
    m_Hit(source.m_Hit)
{ }

// Destructor.
ZddRecHitID::~ZddRecHitID()
{ }

// Assignment operator.
ZddRecHitID&
ZddRecHitID::operator=(const ZddRecHitID& orig)
{
  // Assignment operator.
  if (this != &orig) {             // Watch out for self-assignment!
    m_PartID = orig.m_PartID;
    m_DetectorID  = orig.m_DetectorID;
    m_CrystalNo  = orig.m_CrystalNo;
    m_Hit = orig.m_Hit;
  }
  return *this;
}

bool
ZddRecHitID::operator == (const ZddRecHitID& other) const
{
  if ( m_PartID == other.m_PartID && 
       m_DetectorID  == other.m_DetectorID  &&
       m_CrystalNo  == other.m_CrystalNo && 
       m_Hit == other.m_Hit )
    return true;
  else 
    return false;
}

bool
ZddRecHitID::operator != (const ZddRecHitID& other) const
{
  if ( m_PartID == other.m_PartID && 
       m_DetectorID  == other.m_DetectorID &&
       m_CrystalNo  == other.m_CrystalNo &&
       m_Hit == other.m_Hit )
    return false;
  else 
    return true;
}

bool
ZddRecHitID::operator < (const ZddRecHitID& other) const
{
  if (m_PartID < other.m_PartID ||
     (m_PartID == other.m_PartID && m_DetectorID <  other.m_DetectorID) ||
     (m_PartID == other.m_PartID && m_DetectorID == other.m_DetectorID && m_CrystalNo < other.m_CrystalNo) ||
     (m_PartID == other.m_PartID && m_DetectorID == other.m_DetectorID && m_CrystalNo == other.m_CrystalNo && m_Hit < other.m_Hit)  )
    {  
      return true;
    }
  else {
    return false;
  }
}

bool
ZddRecHitID::operator > (const ZddRecHitID& other) const
{
  if (m_PartID < other.m_PartID ||
      (m_PartID == other.m_PartID && m_DetectorID <  other.m_DetectorID) ||
      (m_PartID == other.m_PartID && m_DetectorID == other.m_DetectorID && m_CrystalNo<  other.m_CrystalNo) ||
      (m_PartID == other.m_PartID && m_DetectorID == other.m_DetectorID && m_CrystalNo == other.m_CrystalNo && m_Hit < other.m_Hit)  )
    {
      return false;
    }
  else {
    return true;
  }
}

// Set the identifier variables.
void
ZddRecHitID::SetID(const int partID,
		   const int detectorID,
		   const int crystalNo,
		   const int hit)
		  
{
  m_PartID = partID;
  m_DetectorID  = detectorID;
  m_CrystalNo  = crystalNo;
  m_Hit = hit;
}

// Output to a stream.
//ostream& operator << (ostream& s, const ZddRecHitID& hitID)
  //{
  //return s << " Part " << hitID.Part() << " Seg " << hitID.Seg()
    //   << " Gap "  << hitID.Gap()  << " Hit " << hitID.Hit();
  //}
