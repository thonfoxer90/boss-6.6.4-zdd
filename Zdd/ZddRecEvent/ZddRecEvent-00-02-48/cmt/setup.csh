# echo "Setting ZddRecEvent ZddRecEvent-00-02-48 in /home/bgarillo/boss-6.6.4-ZDD/Zdd"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cluster/him/bes3/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=ZddRecEvent -version=ZddRecEvent-00-02-48 -path=/home/bgarillo/boss-6.6.4-ZDD/Zdd  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

