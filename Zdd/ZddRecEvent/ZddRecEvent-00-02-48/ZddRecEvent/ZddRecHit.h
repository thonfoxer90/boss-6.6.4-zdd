//$id$
//
//$log$

/*
 *    2003/12/12   Zhengyun You     Peking University
 * 
 *    2004/09/12   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

#ifndef ZDD_REC_HIT_H
#define ZDD_REC_HIT_H

#include <CLHEP/Vector/ThreeVector.h>
#include <CLHEP/Geometry/Point3D.h>

#include "GaudiKernel/ContainedObject.h"
#include "GaudiKernel/ObjectVector.h"
#include "GaudiKernel/SmartRef.h"
#include "EventModel/EventModel.h"
#include "Identifier/Identifier.h"
#include "/home/bgarillo/boss-6.6.4-ZDD/Zdd/ZddGeomSvc/ZddGeomSvc-00-02-25/ZddGeomSvc/ZddGeomSvc.h"

#ifndef ENABLE_BACKWARDS_COMPATIBILITY
typedef HepGeom::Point3D<double> HepPoint3D;
#endif
#ifndef ENABLE_BACKWARDS_COMPATIBILITY
typedef HepGeom::Vector3D<double> HepVector3D;
#endif

using namespace std;
using namespace CLHEP;

extern const  CLID &CLID_ZddRecHit;

class ZddRecHit : virtual public ContainedObject 
{
 public:

  /// Constructor.
  ZddRecHit() {;}

  /// Constructor.
  ZddRecHit(const Identifier& id);

  /// Constructor.
  ZddRecHit(const int partID  = -99,
	    const int detectorID   = -99,
	    const int crystalNo   = -99
	    );

  /// Copy constructor.
  ZddRecHit(const ZddRecHit& source);
  
  /// Assignment operator.
  ZddRecHit& operator=(const ZddRecHit& orig);
  
  /// Destructor.
  ~ZddRecHit();

  virtual const CLID& clID() const { return ZddRecHit::classID(); }
  static  const CLID& classID() { return CLID_ZddRecHit; }

 public:
  
  /// Get soft identifier of this hit.
  Identifier GetID() const { return m_ZddID; }
  
  /// Get PartID
  int PartID() const { return ZddID::partID(m_ZddID); }

  /// Get detectorID
  int DetectorID() const { return ZddID::detectorID(m_ZddID); }
  
  /// Get crystal.
  int CrystalNo() const { return ZddID::crystalNo(m_ZddID); }
  
  /// Get geometry data for the crystal containing this hit.
  ZddGeoCrystal *GetCrystal() const { return m_pZddGeoCrystal; }
  
  void SetHitSeed(int seed ){m_IsSeed = seed;}

  int HitIsSeed() const {return m_IsSeed;}


 private:
  
  Identifier   m_ZddID;

  ZddGeoCrystal   *m_pZddGeoCrystal;
  int          m_IsSeed;
};

typedef ObjectVector<ZddRecHit> ZddRecHitCol;

#endif  /* ZDD_REC_HIT_H */
