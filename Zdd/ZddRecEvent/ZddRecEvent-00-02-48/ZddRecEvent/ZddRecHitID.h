//$id$
//
//$log$

/*
 *    2003/12/12   Zhengyun You     Peking University
 * 
 *    2004/09/12   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

#ifndef ZDD_REC_HIT_ID_H
#define ZDD_REC_HIT_ID_H

#include <stdlib.h>
#include <math.h>
#include <vector>
#include <map>
#include <iostream>


/**
 * Class BesZDDRecRawHit contains the four numbers necessary to identify a hit uniquely:
 * partID, detectorID, crystalNo
 *
 * @author Marcel Werner You \URL{mailto:marcel.werner@exp2.physik.uni-giessen.de}
 *
 */

class ZddRecHitID
{
 public:
  // PUBLIC CONSTANTS =================================================

  // CONSTRUCTOR/DESTRUCTOR METHODS ===================================

  /// Constructor.
  ZddRecHitID(const int partID = -99,
	      const int detectorID  = -99,
	      const int crystal  = -99,
	      const int hit = -99);

  /// Copy constructor. 
  ZddRecHitID(const ZddRecHitID& source);
  
  /// Assignment operator.
  ZddRecHitID& operator=(const ZddRecHitID& orig);

  /// Comparison operators.
  bool operator ==    (const ZddRecHitID& other) const;
  bool operator !=    (const ZddRecHitID& other) const;
  bool operator <     (const ZddRecHitID& other) const;
  bool operator >     (const ZddRecHitID& other) const;
  
  /// Destructor.
  ~ZddRecHitID();
  
  /// Set the identifier.
  void SetID(const int partID = -99,
	     const int detectorID  = -99,
	     const int crystalNo  = -99,
	     const int hit = -99);

  /// Get partID number. (1-2)
  int PartID() const { return m_PartID; }

  /// Get detectorID  number. (1-2)
  int DetectorID() const { return m_DetectorID; }
  
  /// Get crystal  number. (101-224)
  int CrystalNo() const { return m_CrystalNo; }
  
  // Get hit number within this crystal (starts with 0)
  int Hit() const { return m_Hit; }
 
 private:
  int m_PartID; 
  int m_DetectorID;  
  int m_CrystalNo;  
  int m_Hit;
};

/// Print a ZddRecHitID hit identifier to a stream.
//ostream& operator << (ostream& s, const ZddRecHitID& n);

#endif  /* ZDD_REC_HIT_ID_H */
