//$id$
//
//$log$

/*
 *    2003/12/12   Zhengyun You     Peking University
 * 
 *    2004/09/12   Zhengyun You     Peking University
 *                 transplanted to Gaudi framework
 */

#ifndef ZDD_REC_HIT_CONTAINER_H
#define ZDD_REC_HIT_CONTAINER_H

#include <map>
#include "Identifier/Identifier.h"

#include "ZddRecEvent/ZddRecHitID.h"
#include "ZddRecEvent/ZddRecHit.h"

/**
 * ZddRecHitContainer has a ZddRecHitCol which contains all ZddRecHit objects for a single event.
 * Access is optimized for referencing by BesZDDRecHitID indices.
 *
 * @author Zhengyun You \URL{mailto:youzy@hep.pku.cn}
 * @see ZddRecHit
 */

class ZddRecHitContainer
{
 public:
  typedef std::map<ZddRecHitID, int>        CrystalHitCountMap;
  typedef std::map<ZddRecHitID, ZddRecHit*> pHitMap;       
  
  // Constructor.
  ZddRecHitContainer();
  
  // Copy constructor.
  ZddRecHitContainer(const ZddRecHitContainer& source);
  
  // Assignment operator.
  ZddRecHitContainer& operator=(const ZddRecHitContainer& orig);

  // Destructor.
  ~ZddRecHitContainer();

 public:

  /// Points to the *single* instance of ZddRecHitContainer.
  //static ZddRecHitContainer *Instance();
  
  /// Initializes the ZddRecHitContainer with ZddRecHitCol.
  void Init();
  
  /// Destroys the *single* instance of ZddRecHitContainer.
  void Destroy();

  void SetZddRecHitCol(ZddRecHitCol* p);// { m_pZddRecHitCol = p; }
  
  /// Get ZddRecHitCol pointer.
  ZddRecHitCol* GetZddRecHitCol() { return m_pZddRecHitCol; }

  /// How many hits are there in this event?
  int GetHitCount();
  
  /// How many hits are there in this crystal?
  int GetCrystalHitCount(const ZddRecHitID crystalNo);

  /// How many hits are there in this crystal?
  int GetCrystalHitCount(const int partID,
		     const int detectorID,
		     const int crystalNo);

  /// Get a ZddRecHit object by hit identifier.
  ZddRecHit *GetHit(const ZddRecHitID hitID);

  /// Get a ZddRecHit by part, detectorID, crystalNo
  ZddRecHit *GetHit(const int partID,
		    const int detectorID,
		    const int crystalNo,
		    const int hit);
 
  /// Get a ZddRecHit by Identifier.
  /// If doesnt exist, return null.
  ZddRecHit *GetHitByIdentifier(const Identifier id);

  /// Get a ZddRecHit by part, seg, gap, and strip.
  /// If doesnt exist, return null.
  ZddRecHit *GetHitByIdentifier(const int partID,
				const int detectorID,
				const int crystalNo);				
  
  void AddHit(const Identifier id);

  /// Place a new hit object in the container.
  void AddHit(const int partID,
	      const int detectorID,
	      const int crystalNo);

  /// Remove all hit objects from the container, and destroy them.
  void Clear();

 private:
  ZddRecHitCol *m_pZddRecHitCol;

  ZddRecHitContainer::CrystalHitCountMap m_CrystalHitCount;
  ZddRecHitContainer::pHitMap        m_pHit;
  
};

#endif  /* ZDD_REC_HIT_CONTAINER_H */
