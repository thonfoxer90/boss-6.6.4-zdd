#-- start of make_header -----------------

#====================================
#  Library ZddRecEvent
#
<<<<<<< HEAD
#   Generated Sat May 19 14:50:57 2018  by bgarillo
=======
#   Generated Sun May 20 13:40:29 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ZddRecEvent_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ZddRecEvent_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ZddRecEvent

ZddRecEvent_tag = $(tag)

#cmt_local_tagfile_ZddRecEvent = $(ZddRecEvent_tag)_ZddRecEvent.make
cmt_local_tagfile_ZddRecEvent = $(bin)$(ZddRecEvent_tag)_ZddRecEvent.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ZddRecEvent_tag = $(tag)

#cmt_local_tagfile_ZddRecEvent = $(ZddRecEvent_tag).make
cmt_local_tagfile_ZddRecEvent = $(bin)$(ZddRecEvent_tag).make

endif

include $(cmt_local_tagfile_ZddRecEvent)
#-include $(cmt_local_tagfile_ZddRecEvent)

ifdef cmt_ZddRecEvent_has_target_tag

cmt_final_setup_ZddRecEvent = $(bin)setup_ZddRecEvent.make
#cmt_final_setup_ZddRecEvent = $(bin)ZddRecEvent_ZddRecEventsetup.make
cmt_local_ZddRecEvent_makefile = $(bin)ZddRecEvent.make

else

cmt_final_setup_ZddRecEvent = $(bin)setup.make
#cmt_final_setup_ZddRecEvent = $(bin)ZddRecEventsetup.make
cmt_local_ZddRecEvent_makefile = $(bin)ZddRecEvent.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ZddRecEventsetup.make

#ZddRecEvent :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ZddRecEvent'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ZddRecEvent/
#ZddRecEvent::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

ZddRecEventlibname   = $(bin)$(library_prefix)ZddRecEvent$(library_suffix)
ZddRecEventlib       = $(ZddRecEventlibname).a
ZddRecEventstamp     = $(bin)ZddRecEvent.stamp
ZddRecEventshstamp   = $(bin)ZddRecEvent.shstamp

ZddRecEvent :: dirs  ZddRecEventLIB
	$(echo) "ZddRecEvent ok"

#-- end of libary_header ----------------

ZddRecEventLIB :: $(ZddRecEventlib) $(ZddRecEventshstamp)
	@/bin/echo "------> ZddRecEvent : library ok"

$(ZddRecEventlib) :: $(bin)ZddRecHit.o $(bin)ZddRecHitContainer.o $(bin)ZddRecHitID.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(ZddRecEventlib) $?
	$(lib_silent) $(ranlib) $(ZddRecEventlib)
	$(lib_silent) cat /dev/null >$(ZddRecEventstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(ZddRecEventlibname).$(shlibsuffix) :: $(ZddRecEventlib) $(ZddRecEventstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" ZddRecEvent $(ZddRecEvent_shlibflags)

$(ZddRecEventshstamp) :: $(ZddRecEventlibname).$(shlibsuffix)
	@if test -f $(ZddRecEventlibname).$(shlibsuffix) ; then cat /dev/null >$(ZddRecEventshstamp) ; fi

ZddRecEventclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)ZddRecHit.o $(bin)ZddRecHitContainer.o $(bin)ZddRecHitID.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
ZddRecEventinstallname = $(library_prefix)ZddRecEvent$(library_suffix).$(shlibsuffix)

ZddRecEvent :: ZddRecEventinstall

install :: ZddRecEventinstall

ZddRecEventinstall :: $(install_dir)/$(ZddRecEventinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(ZddRecEventinstallname) :: $(bin)$(ZddRecEventinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(ZddRecEventinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(ZddRecEventinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddRecEventinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddRecEventinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(ZddRecEventinstallname) $(install_dir)/$(ZddRecEventinstallname); \
	      echo `pwd`/$(ZddRecEventinstallname) >$(install_dir)/$(ZddRecEventinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(ZddRecEventinstallname), no installation directory specified"; \
	  fi; \
	fi

ZddRecEventclean :: ZddRecEventuninstall

uninstall :: ZddRecEventuninstall

ZddRecEventuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(ZddRecEventinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddRecEventinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddRecEventinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(ZddRecEventinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),ZddRecEventclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)ZddRecEvent_dependencies.make :: dirs

ifndef QUICK
$(bin)ZddRecEvent_dependencies.make : $(src)ZddRecHit.cxx $(src)ZddRecHitContainer.cxx $(src)ZddRecHitID.cxx $(use_requirements) $(cmt_final_setup_ZddRecEvent)
	$(echo) "(ZddRecEvent.make) Rebuilding $@"; \
	  $(build_dependencies) ZddRecEvent -all_sources -out=$@ $(src)ZddRecHit.cxx $(src)ZddRecHitContainer.cxx $(src)ZddRecHitID.cxx
endif

#$(ZddRecEvent_dependencies)

-include $(bin)ZddRecEvent_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),ZddRecEventclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ZddRecHit.d

$(bin)$(binobj)ZddRecHit.d : $(use_requirements) $(cmt_final_setup_ZddRecEvent)

$(bin)$(binobj)ZddRecHit.d : $(src)ZddRecHit.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ZddRecHit.dep $(use_pp_cppflags) $(ZddRecEvent_pp_cppflags) $(lib_ZddRecEvent_pp_cppflags) $(ZddRecHit_pp_cppflags) $(use_cppflags) $(ZddRecEvent_cppflags) $(lib_ZddRecEvent_cppflags) $(ZddRecHit_cppflags) $(ZddRecHit_cxx_cppflags)  $(src)ZddRecHit.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ZddRecHit.o $(src)ZddRecHit.cxx $(@D)/ZddRecHit.dep
endif
endif

$(bin)$(binobj)ZddRecHit.o : $(src)ZddRecHit.cxx
else
$(bin)ZddRecEvent_dependencies.make : $(ZddRecHit_cxx_dependencies)

$(bin)$(binobj)ZddRecHit.o : $(ZddRecHit_cxx_dependencies)
endif
	$(cpp_echo) $(src)ZddRecHit.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ZddRecEvent_pp_cppflags) $(lib_ZddRecEvent_pp_cppflags) $(ZddRecHit_pp_cppflags) $(use_cppflags) $(ZddRecEvent_cppflags) $(lib_ZddRecEvent_cppflags) $(ZddRecHit_cppflags) $(ZddRecHit_cxx_cppflags)  $(src)ZddRecHit.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),ZddRecEventclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ZddRecHitContainer.d

$(bin)$(binobj)ZddRecHitContainer.d : $(use_requirements) $(cmt_final_setup_ZddRecEvent)

$(bin)$(binobj)ZddRecHitContainer.d : $(src)ZddRecHitContainer.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ZddRecHitContainer.dep $(use_pp_cppflags) $(ZddRecEvent_pp_cppflags) $(lib_ZddRecEvent_pp_cppflags) $(ZddRecHitContainer_pp_cppflags) $(use_cppflags) $(ZddRecEvent_cppflags) $(lib_ZddRecEvent_cppflags) $(ZddRecHitContainer_cppflags) $(ZddRecHitContainer_cxx_cppflags)  $(src)ZddRecHitContainer.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ZddRecHitContainer.o $(src)ZddRecHitContainer.cxx $(@D)/ZddRecHitContainer.dep
endif
endif

$(bin)$(binobj)ZddRecHitContainer.o : $(src)ZddRecHitContainer.cxx
else
$(bin)ZddRecEvent_dependencies.make : $(ZddRecHitContainer_cxx_dependencies)

$(bin)$(binobj)ZddRecHitContainer.o : $(ZddRecHitContainer_cxx_dependencies)
endif
	$(cpp_echo) $(src)ZddRecHitContainer.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ZddRecEvent_pp_cppflags) $(lib_ZddRecEvent_pp_cppflags) $(ZddRecHitContainer_pp_cppflags) $(use_cppflags) $(ZddRecEvent_cppflags) $(lib_ZddRecEvent_cppflags) $(ZddRecHitContainer_cppflags) $(ZddRecHitContainer_cxx_cppflags)  $(src)ZddRecHitContainer.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),ZddRecEventclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ZddRecHitID.d

$(bin)$(binobj)ZddRecHitID.d : $(use_requirements) $(cmt_final_setup_ZddRecEvent)

$(bin)$(binobj)ZddRecHitID.d : $(src)ZddRecHitID.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ZddRecHitID.dep $(use_pp_cppflags) $(ZddRecEvent_pp_cppflags) $(lib_ZddRecEvent_pp_cppflags) $(ZddRecHitID_pp_cppflags) $(use_cppflags) $(ZddRecEvent_cppflags) $(lib_ZddRecEvent_cppflags) $(ZddRecHitID_cppflags) $(ZddRecHitID_cxx_cppflags)  $(src)ZddRecHitID.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ZddRecHitID.o $(src)ZddRecHitID.cxx $(@D)/ZddRecHitID.dep
endif
endif

$(bin)$(binobj)ZddRecHitID.o : $(src)ZddRecHitID.cxx
else
$(bin)ZddRecEvent_dependencies.make : $(ZddRecHitID_cxx_dependencies)

$(bin)$(binobj)ZddRecHitID.o : $(ZddRecHitID_cxx_dependencies)
endif
	$(cpp_echo) $(src)ZddRecHitID.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ZddRecEvent_pp_cppflags) $(lib_ZddRecEvent_pp_cppflags) $(ZddRecHitID_pp_cppflags) $(use_cppflags) $(ZddRecEvent_cppflags) $(lib_ZddRecEvent_cppflags) $(ZddRecHitID_cppflags) $(ZddRecHitID_cxx_cppflags)  $(src)ZddRecHitID.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ZddRecEventclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ZddRecEvent.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ZddRecEvent.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(ZddRecEvent.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ZddRecEvent.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_ZddRecEvent)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ZddRecEvent.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ZddRecEvent.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ZddRecEvent.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

ZddRecEventclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library ZddRecEvent
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)ZddRecEvent$(library_suffix).a $(library_prefix)ZddRecEvent$(library_suffix).s? ZddRecEvent.stamp ZddRecEvent.shstamp
#-- end of cleanup_library ---------------
