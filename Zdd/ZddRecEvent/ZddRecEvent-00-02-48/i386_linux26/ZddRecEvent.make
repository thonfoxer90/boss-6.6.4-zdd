#-- start of make_header -----------------

#====================================
#  Library ZddRecEvent
#
#   Generated Mon Aug 22 21:30:30 2011  by mwerner
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ZddRecEvent_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ZddRecEvent_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ZddRecEvent

ZddRecEvent_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddRecEvent = /tmp/CMT_$(ZddRecEvent_tag)_ZddRecEvent.make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddRecEvent = $(ZddRecEvent_tag)_ZddRecEvent.make
endif

else

tags      = $(tag),$(CMTEXTRATAGS)

ZddRecEvent_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddRecEvent = /tmp/CMT_$(ZddRecEvent_tag).make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddRecEvent = $(ZddRecEvent_tag).make
endif

endif

-include $(cmt_local_tagfile_ZddRecEvent)

ifdef cmt_ZddRecEvent_has_target_tag

ifdef READONLY
cmt_final_setup_ZddRecEvent = /tmp/CMT_ZddRecEvent_ZddRecEventsetup.make
cmt_local_ZddRecEvent_makefile = /tmp/CMT_ZddRecEvent$(cmt_lock_pid).make
else
cmt_final_setup_ZddRecEvent = $(bin)ZddRecEvent_ZddRecEventsetup.make
cmt_local_ZddRecEvent_makefile = $(bin)ZddRecEvent.make
endif

else

ifdef READONLY
cmt_final_setup_ZddRecEvent = /tmp/CMT_ZddRecEventsetup.make
cmt_local_ZddRecEvent_makefile = /tmp/CMT_ZddRecEvent$(cmt_lock_pid).make
else
cmt_final_setup_ZddRecEvent = $(bin)ZddRecEventsetup.make
cmt_local_ZddRecEvent_makefile = $(bin)ZddRecEvent.make
endif

endif

ifdef READONLY
cmt_final_setup = /tmp/CMT_ZddRecEventsetup.make
else
cmt_final_setup = $(bin)ZddRecEventsetup.make
endif

ZddRecEvent ::


ifdef READONLY
ZddRecEvent ::
	@echo tags=$(tags)
	@echo cmt_local_tagfile=$(cmt_local_tagfile)
endif


dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	@echo 'ZddRecEvent'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ZddRecEvent/
ZddRecEvent::
	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
	@echo "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

#-- end of make_header ------------------

#-- start of libary_header ---------------

ZddRecEventlibname   = $(bin)$(library_prefix)ZddRecEvent$(library_suffix)
ZddRecEventlib       = $(ZddRecEventlibname).a
ZddRecEventstamp     = $(bin)ZddRecEvent.stamp
ZddRecEventshstamp   = $(bin)ZddRecEvent.shstamp

ZddRecEvent :: dirs  ZddRecEventLIB
	@/bin/echo "------> ZddRecEvent ok"

#-- end of libary_header ----------------

ZddRecEventLIB :: $(ZddRecEventlib) $(ZddRecEventshstamp)
	@/bin/echo "------> ZddRecEvent : library ok"

$(ZddRecEventlib) :: $(bin)ZddRecHit.o $(bin)ZddRecHitContainer.o $(bin)ZddRecHitID.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(ZddRecEventlib) $?
	$(lib_silent) $(ranlib) $(ZddRecEventlib)
	$(lib_silent) cat /dev/null >$(ZddRecEventstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(ZddRecEventlibname).$(shlibsuffix) :: $(ZddRecEventlib) $(ZddRecEventstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" ZddRecEvent $(ZddRecEvent_shlibflags)

$(ZddRecEventshstamp) :: $(ZddRecEventlibname).$(shlibsuffix)
	@if test -f $(ZddRecEventlibname).$(shlibsuffix) ; then cat /dev/null >$(ZddRecEventshstamp) ; fi

ZddRecEventclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)ZddRecHit.o $(bin)ZddRecHitContainer.o $(bin)ZddRecHitID.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
ZddRecEventinstallname = $(library_prefix)ZddRecEvent$(library_suffix).$(shlibsuffix)

ZddRecEvent :: ZddRecEventinstall

install :: ZddRecEventinstall

ZddRecEventinstall :: $(install_dir)/$(ZddRecEventinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(ZddRecEventinstallname) :: $(bin)$(ZddRecEventinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(ZddRecEventinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(ZddRecEventinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddRecEventinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddRecEventinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(ZddRecEventinstallname) $(install_dir)/$(ZddRecEventinstallname); \
	      echo `pwd`/$(ZddRecEventinstallname) >$(install_dir)/$(ZddRecEventinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(ZddRecEventinstallname), no installation directory specified"; \
	  fi; \
	fi

ZddRecEventclean :: ZddRecEventuninstall

uninstall :: ZddRecEventuninstall

ZddRecEventuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(ZddRecEventinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddRecEventinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddRecEventinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(ZddRecEventinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------

$(bin)ZddRecEvent_dependencies.make :: dirs

ifndef QUICK
$(bin)ZddRecEvent_dependencies.make :: $(src)*.cxx  requirements $(use_requirements) $(cmt_final_setup_ZddRecEvent)
	@echo "------> (ZddRecEvent.make) Rebuilding $@"; \
	  args=`echo $? | sed -e "s#requirements.*##"`; \
	  $(build_dependencies) ZddRecEvent -all_sources $${args}
endif

#$(ZddRecEvent_dependencies)

-include $(bin)ZddRecEvent_dependencies.make

#-- end of dependency -------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddRecHit.o : $(ZddRecHit_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddRecHit.o $(use_pp_cppflags) $(ZddRecEvent_pp_cppflags) $(lib_ZddRecEvent_pp_cppflags) $(ZddRecHit_pp_cppflags) $(use_cppflags) $(ZddRecEvent_cppflags) $(lib_ZddRecEvent_cppflags) $(ZddRecHit_cppflags) $(ZddRecHit_cxx_cppflags)  $(src)ZddRecHit.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddRecHitContainer.o : $(ZddRecHitContainer_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddRecHitContainer.o $(use_pp_cppflags) $(ZddRecEvent_pp_cppflags) $(lib_ZddRecEvent_pp_cppflags) $(ZddRecHitContainer_pp_cppflags) $(use_cppflags) $(ZddRecEvent_cppflags) $(lib_ZddRecEvent_cppflags) $(ZddRecHitContainer_cppflags) $(ZddRecHitContainer_cxx_cppflags)  $(src)ZddRecHitContainer.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddRecHitID.o : $(ZddRecHitID_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddRecHitID.o $(use_pp_cppflags) $(ZddRecEvent_pp_cppflags) $(lib_ZddRecEvent_pp_cppflags) $(ZddRecHitID_pp_cppflags) $(use_cppflags) $(ZddRecEvent_cppflags) $(lib_ZddRecEvent_cppflags) $(ZddRecHitID_cppflags) $(ZddRecHitID_cxx_cppflags)  $(src)ZddRecHitID.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ZddRecEventclean
	@cd .

ifndef PEDANTIC
.DEFAULT::
	@echo "WARNING >> You should provide a target for $@"
else
.DEFAULT::
	@echo "PEDANTIC MODE >> You should provide a target for $@"
	@if test `expr index $@ '.'` != 0 ; \
	then  echo "PEDANTIC MODE >> This target seems to be a missing file, please check"; exit -1 ;\
	else echo "PEDANTIC MODE >> Just ignore it ; as it seems to be just a fake target due to some pattern" ; exit 0; fi; 		
endif

ZddRecEventclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(binobj)$(library_prefix)ZddRecEvent$(library_suffix).a $(binobj)$(library_prefix)ZddRecEvent$(library_suffix).s? $(binobj)ZddRecEvent.stamp $(binobj)ZddRecEvent.shstamp
#-- end of cleanup_library ---------------

