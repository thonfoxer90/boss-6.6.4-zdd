----------> uses
use CMT v1r20
use LCG_Configuration * 
use ExternalComponents ExternalComponents-00-00-32 External
use LCG_Settings v1 
use CASTOR v1 LCG_Interfaces
use XercesC v1 LCG_Interfaces
use HepPDT v1 LCG_Interfaces
use CLHEP v1 LCG_Interfaces
use HepMC v1 LCG_Interfaces
use Python v1 LCG_Interfaces
use Boost v1 LCG_Interfaces
use GCCXML v1 LCG_Interfaces
use ROOT v1 LCG_Interfaces
use BesROOT BesROOT-00-00-06 External
use Reflex v1 LCG_Interfaces
use BesFortranPolicy BesFortranPolicy-00-01-01 
use GaudiPolicy v7r0 
use GaudiKernel v23r0 
use ExternalPolicy ExternalPolicy-00-00-17 External
use BesExternalArea BesExternalArea-00-00-18 External
use Sqlite Sqlite-00-00-01 External
use MYSQL MYSQL-00-00-07 External
use BesCLHEP BesCLHEP-00-00-06 External
use GaudiInterface GaudiInterface-01-03-05 External
use BesCxxPolicy BesCxxPolicy-00-00-08 
use BesPolicy BesPolicy-01-05-03 
use RelTable RelTable-00-00-02 Event
use DatabaseSvc DatabaseSvc-00-00-08 Database
use facilities facilities-00-00-01 Calibration
use CalibData CalibData-00-00-39 Calibration
use xmlBase xmlBase-00-00-02 Calibration
use rdbModel rdbModel-00-00-13 Calibration
use calibUtil calibUtil-00-00-29 Calibration
use EventModel EventModel-01-05-30 Event
use MdcGeomSvc MdcGeomSvc-00-01-36 Mdc
use Identifier Identifier-00-02-10 DetectorDescription
use MdcRecEvent MdcRecEvent-00-05-12-patch Mdc
use DstEvent DstEvent-00-02-43-patch1 Event
use ExtEvent ExtEvent-00-00-13 Event
----------> tags
CMTv1
CMTr20
CMTp0
Linux
i386_linux26
mwerner_no_config
mwerner_root
mwerner_cleanup
mwerner_no_prototypes
mwerner_with_installarea
mwerner_with_version_directory
mwerner
Boss_no_config
Boss_root
Boss_cleanup
Boss_no_prototypes
Boss_with_installarea
Boss_with_version_directory
GAUDI_no_config
GAUDI_root
GAUDI_cleanup
GAUDI_prototypes
GAUDI_with_installarea
GAUDI_with_version_directory
LCGCMT_no_config
LCGCMT_no_root
LCGCMT_cleanup
LCGCMT_prototypes
LCGCMT_without_installarea
LCGCMT_with_version_directory
i686
slc45
gcc346
Unix
gcc34
gcc-3.4
slc4
HasAthenaRunTime
ROOTBasicLibs
----------> CMTPATH
# Add path /afs/ihep.ac.cn/users/m/mwerner/workarea from initialization
# Add path /afs/ihep.ac.cn/bes3/offline/Boss/6.5.4 from initialization
# Add path /afs/ihep.ac.cn/bes3/offline/sw/Gaudi/slc4_ia32_gcc34-clhep2.0.4.5/GAUDI_v19r4 from initialization
# Add path /afs/ihep.ac.cn/bes3/offline/sw/lcg/app/releases/LCGCMT/LCGCMT_52a_clhep2.0.4.5 from initialization
