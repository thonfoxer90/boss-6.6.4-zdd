#-- start of make_header -----------------

#====================================
#  Library ZddRawEvent
#
<<<<<<< HEAD
#   Generated Sat May 19 14:50:54 2018  by bgarillo
=======
#   Generated Sun May 20 13:40:17 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ZddRawEvent_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ZddRawEvent_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ZddRawEvent

ZddRawEvent_tag = $(tag)

#cmt_local_tagfile_ZddRawEvent = $(ZddRawEvent_tag)_ZddRawEvent.make
cmt_local_tagfile_ZddRawEvent = $(bin)$(ZddRawEvent_tag)_ZddRawEvent.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ZddRawEvent_tag = $(tag)

#cmt_local_tagfile_ZddRawEvent = $(ZddRawEvent_tag).make
cmt_local_tagfile_ZddRawEvent = $(bin)$(ZddRawEvent_tag).make

endif

include $(cmt_local_tagfile_ZddRawEvent)
#-include $(cmt_local_tagfile_ZddRawEvent)

ifdef cmt_ZddRawEvent_has_target_tag

cmt_final_setup_ZddRawEvent = $(bin)setup_ZddRawEvent.make
#cmt_final_setup_ZddRawEvent = $(bin)ZddRawEvent_ZddRawEventsetup.make
cmt_local_ZddRawEvent_makefile = $(bin)ZddRawEvent.make

else

cmt_final_setup_ZddRawEvent = $(bin)setup.make
#cmt_final_setup_ZddRawEvent = $(bin)ZddRawEventsetup.make
cmt_local_ZddRawEvent_makefile = $(bin)ZddRawEvent.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ZddRawEventsetup.make

#ZddRawEvent :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ZddRawEvent'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ZddRawEvent/
#ZddRawEvent::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

ZddRawEventlibname   = $(bin)$(library_prefix)ZddRawEvent$(library_suffix)
ZddRawEventlib       = $(ZddRawEventlibname).a
ZddRawEventstamp     = $(bin)ZddRawEvent.stamp
ZddRawEventshstamp   = $(bin)ZddRawEvent.shstamp

ZddRawEvent :: dirs  ZddRawEventLIB
	$(echo) "ZddRawEvent ok"

#-- end of libary_header ----------------

ZddRawEventLIB :: $(ZddRawEventlib) $(ZddRawEventshstamp)
	@/bin/echo "------> ZddRawEvent : library ok"

$(ZddRawEventlib) :: $(bin)ZddDigi.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(ZddRawEventlib) $?
	$(lib_silent) $(ranlib) $(ZddRawEventlib)
	$(lib_silent) cat /dev/null >$(ZddRawEventstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(ZddRawEventlibname).$(shlibsuffix) :: $(ZddRawEventlib) $(ZddRawEventstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" ZddRawEvent $(ZddRawEvent_shlibflags)

$(ZddRawEventshstamp) :: $(ZddRawEventlibname).$(shlibsuffix)
	@if test -f $(ZddRawEventlibname).$(shlibsuffix) ; then cat /dev/null >$(ZddRawEventshstamp) ; fi

ZddRawEventclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)ZddDigi.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
ZddRawEventinstallname = $(library_prefix)ZddRawEvent$(library_suffix).$(shlibsuffix)

ZddRawEvent :: ZddRawEventinstall

install :: ZddRawEventinstall

ZddRawEventinstall :: $(install_dir)/$(ZddRawEventinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(ZddRawEventinstallname) :: $(bin)$(ZddRawEventinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(ZddRawEventinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(ZddRawEventinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddRawEventinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddRawEventinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(ZddRawEventinstallname) $(install_dir)/$(ZddRawEventinstallname); \
	      echo `pwd`/$(ZddRawEventinstallname) >$(install_dir)/$(ZddRawEventinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(ZddRawEventinstallname), no installation directory specified"; \
	  fi; \
	fi

ZddRawEventclean :: ZddRawEventuninstall

uninstall :: ZddRawEventuninstall

ZddRawEventuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(ZddRawEventinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddRawEventinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddRawEventinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(ZddRawEventinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),ZddRawEventclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)ZddRawEvent_dependencies.make :: dirs

ifndef QUICK
$(bin)ZddRawEvent_dependencies.make : $(src)ZddDigi.cxx $(use_requirements) $(cmt_final_setup_ZddRawEvent)
	$(echo) "(ZddRawEvent.make) Rebuilding $@"; \
	  $(build_dependencies) ZddRawEvent -all_sources -out=$@ $(src)ZddDigi.cxx
endif

#$(ZddRawEvent_dependencies)

-include $(bin)ZddRawEvent_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),ZddRawEventclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ZddDigi.d

$(bin)$(binobj)ZddDigi.d : $(use_requirements) $(cmt_final_setup_ZddRawEvent)

$(bin)$(binobj)ZddDigi.d : $(src)ZddDigi.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ZddDigi.dep $(use_pp_cppflags) $(ZddRawEvent_pp_cppflags) $(lib_ZddRawEvent_pp_cppflags) $(ZddDigi_pp_cppflags) $(use_cppflags) $(ZddRawEvent_cppflags) $(lib_ZddRawEvent_cppflags) $(ZddDigi_cppflags) $(ZddDigi_cxx_cppflags)  $(src)ZddDigi.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ZddDigi.o $(src)ZddDigi.cxx $(@D)/ZddDigi.dep
endif
endif

$(bin)$(binobj)ZddDigi.o : $(src)ZddDigi.cxx
else
$(bin)ZddRawEvent_dependencies.make : $(ZddDigi_cxx_dependencies)

$(bin)$(binobj)ZddDigi.o : $(ZddDigi_cxx_dependencies)
endif
	$(cpp_echo) $(src)ZddDigi.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(ZddRawEvent_pp_cppflags) $(lib_ZddRawEvent_pp_cppflags) $(ZddDigi_pp_cppflags) $(use_cppflags) $(ZddRawEvent_cppflags) $(lib_ZddRawEvent_cppflags) $(ZddDigi_cppflags) $(ZddDigi_cxx_cppflags)  $(src)ZddDigi.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ZddRawEventclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ZddRawEvent.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ZddRawEvent.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(ZddRawEvent.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ZddRawEvent.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_ZddRawEvent)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ZddRawEvent.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ZddRawEvent.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ZddRawEvent.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

ZddRawEventclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library ZddRawEvent
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)ZddRawEvent$(library_suffix).a $(library_prefix)ZddRawEvent$(library_suffix).s? ZddRawEvent.stamp ZddRawEvent.shstamp
#-- end of cleanup_library ---------------
