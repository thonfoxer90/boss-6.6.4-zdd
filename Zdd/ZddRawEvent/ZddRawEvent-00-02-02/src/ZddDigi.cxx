#include "ZddRawEvent/ZddDigi.h"

ZddDigi::ZddDigi(const Identifier& id, const unsigned int timeChannel, const unsigned int chargeChannel) :
  RawData(id, timeChannel, chargeChannel),
  m_measure(0) {
}

ZddDigi::ZddDigi(const unsigned int id) :
  RawData(id),
  m_measure(0) {
}


//mixing two digi with EmcWaveform                                                                                                                     
/*
EmcDigi& EmcDigi::operator+=(const EmcDigi &digi)
{
    if(m_id != digi.identify()) // do nothing                                                                                                            
    return *this;

  EmcWaveform wave1,wave2;
  wave1.makeWaveform(m_chargeChannel,m_timeChannel);
  wave2.makeWaveform(digi.getChargeChannel(),digi.getTimeChannel());

  //mixing                                                                                                                                             
  wave1 += wave2;

  double energy;
  int time;
  energy = wave1.max(time);

  m_chargeChannel = (unsigned int)energy;
  m_timeChannel = (unsigned int)time;

  return *this;
}*/
