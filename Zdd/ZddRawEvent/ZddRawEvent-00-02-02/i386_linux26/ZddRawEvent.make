#-- start of make_header -----------------

#====================================
#  Library ZddRawEvent
#
#   Generated Thu May 24 18:29:33 2012  by mwerner
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ZddRawEvent_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ZddRawEvent_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ZddRawEvent

ZddRawEvent_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddRawEvent = /tmp/CMT_$(ZddRawEvent_tag)_ZddRawEvent.make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddRawEvent = $(ZddRawEvent_tag)_ZddRawEvent.make
endif

else

tags      = $(tag),$(CMTEXTRATAGS)

ZddRawEvent_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddRawEvent = /tmp/CMT_$(ZddRawEvent_tag).make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddRawEvent = $(ZddRawEvent_tag).make
endif

endif

-include $(cmt_local_tagfile_ZddRawEvent)

ifdef cmt_ZddRawEvent_has_target_tag

ifdef READONLY
cmt_final_setup_ZddRawEvent = /tmp/CMT_ZddRawEvent_ZddRawEventsetup.make
cmt_local_ZddRawEvent_makefile = /tmp/CMT_ZddRawEvent$(cmt_lock_pid).make
else
cmt_final_setup_ZddRawEvent = $(bin)ZddRawEvent_ZddRawEventsetup.make
cmt_local_ZddRawEvent_makefile = $(bin)ZddRawEvent.make
endif

else

ifdef READONLY
cmt_final_setup_ZddRawEvent = /tmp/CMT_ZddRawEventsetup.make
cmt_local_ZddRawEvent_makefile = /tmp/CMT_ZddRawEvent$(cmt_lock_pid).make
else
cmt_final_setup_ZddRawEvent = $(bin)ZddRawEventsetup.make
cmt_local_ZddRawEvent_makefile = $(bin)ZddRawEvent.make
endif

endif

ifdef READONLY
cmt_final_setup = /tmp/CMT_ZddRawEventsetup.make
else
cmt_final_setup = $(bin)ZddRawEventsetup.make
endif

ZddRawEvent ::


ifdef READONLY
ZddRawEvent ::
	@echo tags=$(tags)
	@echo cmt_local_tagfile=$(cmt_local_tagfile)
endif


dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	@echo 'ZddRawEvent'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ZddRawEvent/
ZddRawEvent::
	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
	@echo "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

#-- end of make_header ------------------

#-- start of libary_header ---------------

ZddRawEventlibname   = $(bin)$(library_prefix)ZddRawEvent$(library_suffix)
ZddRawEventlib       = $(ZddRawEventlibname).a
ZddRawEventstamp     = $(bin)ZddRawEvent.stamp
ZddRawEventshstamp   = $(bin)ZddRawEvent.shstamp

ZddRawEvent :: dirs  ZddRawEventLIB
	@/bin/echo "------> ZddRawEvent ok"

#-- end of libary_header ----------------

ZddRawEventLIB :: $(ZddRawEventlib) $(ZddRawEventshstamp)
	@/bin/echo "------> ZddRawEvent : library ok"

$(ZddRawEventlib) :: $(bin)ZddDigi.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(ZddRawEventlib) $?
	$(lib_silent) $(ranlib) $(ZddRawEventlib)
	$(lib_silent) cat /dev/null >$(ZddRawEventstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(ZddRawEventlibname).$(shlibsuffix) :: $(ZddRawEventlib) $(ZddRawEventstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" ZddRawEvent $(ZddRawEvent_shlibflags)

$(ZddRawEventshstamp) :: $(ZddRawEventlibname).$(shlibsuffix)
	@if test -f $(ZddRawEventlibname).$(shlibsuffix) ; then cat /dev/null >$(ZddRawEventshstamp) ; fi

ZddRawEventclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)ZddDigi.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
ZddRawEventinstallname = $(library_prefix)ZddRawEvent$(library_suffix).$(shlibsuffix)

ZddRawEvent :: ZddRawEventinstall

install :: ZddRawEventinstall

ZddRawEventinstall :: $(install_dir)/$(ZddRawEventinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(ZddRawEventinstallname) :: $(bin)$(ZddRawEventinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(ZddRawEventinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(ZddRawEventinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddRawEventinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddRawEventinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(ZddRawEventinstallname) $(install_dir)/$(ZddRawEventinstallname); \
	      echo `pwd`/$(ZddRawEventinstallname) >$(install_dir)/$(ZddRawEventinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(ZddRawEventinstallname), no installation directory specified"; \
	  fi; \
	fi

ZddRawEventclean :: ZddRawEventuninstall

uninstall :: ZddRawEventuninstall

ZddRawEventuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(ZddRawEventinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddRawEventinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddRawEventinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(ZddRawEventinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------

$(bin)ZddRawEvent_dependencies.make :: dirs

ifndef QUICK
$(bin)ZddRawEvent_dependencies.make :: $(src)*.cxx  requirements $(use_requirements) $(cmt_final_setup_ZddRawEvent)
	@echo "------> (ZddRawEvent.make) Rebuilding $@"; \
	  args=`echo $? | sed -e "s#requirements.*##"`; \
	  $(build_dependencies) ZddRawEvent -all_sources $${args}
endif

#$(ZddRawEvent_dependencies)

-include $(bin)ZddRawEvent_dependencies.make

#-- end of dependency -------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddDigi.o : $(ZddDigi_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddDigi.o $(use_pp_cppflags) $(ZddRawEvent_pp_cppflags) $(lib_ZddRawEvent_pp_cppflags) $(ZddDigi_pp_cppflags) $(use_cppflags) $(ZddRawEvent_cppflags) $(lib_ZddRawEvent_cppflags) $(ZddDigi_cppflags) $(ZddDigi_cxx_cppflags)  $(src)ZddDigi.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ZddRawEventclean
	@cd .

ifndef PEDANTIC
.DEFAULT::
	@echo "WARNING >> You should provide a target for $@"
else
.DEFAULT::
	@echo "PEDANTIC MODE >> You should provide a target for $@"
	@if test `expr index $@ '.'` != 0 ; \
	then  echo "PEDANTIC MODE >> This target seems to be a missing file, please check"; exit -1 ;\
	else echo "PEDANTIC MODE >> Just ignore it ; as it seems to be just a fake target due to some pattern" ; exit 0; fi; 		
endif

ZddRawEventclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(binobj)$(library_prefix)ZddRawEvent$(library_suffix).a $(binobj)$(library_prefix)ZddRawEvent$(library_suffix).s? $(binobj)ZddRawEvent.stamp $(binobj)ZddRawEvent.shstamp
#-- end of cleanup_library ---------------

