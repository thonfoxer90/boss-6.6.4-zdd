#ifndef ZddDigi_H
#define ZddDigi_H 
#include <vector>
#include "GaudiKernel/ObjectVector.h"
#include "RawEvent/RawData.h"
#include "EventModel/EventModel.h"

using namespace EventModel;
extern const CLID& CLID_ZddDigi;

/*
 *
 */

class ZddDigi : public RawData { 
public:
 // Constructor 
 ZddDigi(const Identifier& id, const unsigned int timeChannel, const unsigned int chargeChannel);
 ZddDigi(const unsigned int id);

 // Set Measure Word                                                                                                                                  
 void setMeasure(const unsigned int measure) { m_measure = measure; }

 // Get Measure Word                                                                                                                                  
 unsigned int getMeasure() const { return m_measure; }

 // Retrieve reference to class definition structure
 virtual const CLID& clID() const    { return ZddDigi::classID(); }
 static  const CLID& classID()       { return CLID_ZddDigi; }

private:
 unsigned int m_measure;
};

typedef ObjectVector<ZddDigi> ZddDigiCol;

#endif
