//------------------------------------------------------------------------------|
//      [File  ]:                       ZddGeoMgr.cxx      	                    |
//      [Brief ]:       ZDD geometry created manager class   			              |
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       Mar 28, 2006                                            |
//------------------------------------------------------------------------------|

#include<iostream>
#include<string>
#include<fstream>

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"

#include "ZddGeoCreateAlg/ZddGeoMgr.h"
#include "ZddGeoCreateAlg/ZddIdTransform.h"

using namespace std;

// Constructor
ZddGeoMgr::ZddGeoMgr( const std::string createFlag, bool alignFlag, const std::string alignFile )
{
	m_CreateFlag 	= createFlag;
	m_AlignFlag 	= alignFlag;
	m_AlignFile 	= alignFile;
	
  Gaudi::svcLocator() -> service("MessageSvc", msgSvc);

	InitOffset();	
}

// Destructor
ZddGeoMgr::~ZddGeoMgr()
{
	delete []m_BoxOffset;
	delete []m_StripPlaneOffset;

	delete m_ZddAbsorber;
	delete m_ZddGap;
	delete m_ZddBox;
	delete m_ZddStripPlane;
	delete m_ZddStrip;
	delete m_ZddRpc;
	delete m_ZddGas;
	delete m_ZddBakelite;
	delete m_ZddBoxCover;
}

//========================================= Alignment initialization==================================================
// Offset init
StatusCode ZddGeoMgr::InitOffset()
{
 	MsgStream log(msgSvc, "ZddGeoMgr");

  m_IdTr = new ZddIdTransform();

	if( m_AlignFlag == true )
	{
 		log << MSG::INFO << "ZddGeoMgr::initOffset()" << endreq;

    TFile* froot = new TFile(m_AlignFile.c_str(), "read");
    if( froot->IsZombie() ) 
    {
      log << MSG:: ERROR << "Open alignment data error!" << endreq;
      return StatusCode::FAILURE;
    }

    const char OFFSET_NAME[3][5] = {"dx", "dy", "dz"};
    double box_offset[3];
    double strpln_offset[3];
   
    TTree* tr_Offset;
    
    tr_Offset = (TTree*)froot->Get("Offset");
    tr_Offset->SetBranchAddress("box_dx", &box_offset[0]);
    tr_Offset->SetBranchAddress("box_dy", &box_offset[1]);
    tr_Offset->SetBranchAddress("box_dz", &box_offset[2]);
    tr_Offset->SetBranchAddress("strpln_dx", &strpln_offset[0]);
    tr_Offset->SetBranchAddress("strpln_dy", &strpln_offset[1]);
    tr_Offset->SetBranchAddress("strpln_dz", &strpln_offset[2]);

    int part, segment, layer;
        part = segment = layer = 0;
    
		log << MSG::INFO << "------------------------- offset data--------------------------" << endreq;
    log << MSG::INFO << "Part\tSegment\tLayer\tdx0\tdy0\tdz0\tdx1\tdy1\tdz1" << endreq;
    for( int i=0; i<BOX_MAX; i++)
    {
      m_IdTr->SetBoxPos(i, &part, &segment, &layer);
      tr_Offset->GetEntry(i);
            
      log << MSG::INFO << part << "\t" << segment << "\t" << layer << "\t";
      for( int j=0; j<3; j++ )
      {
        log << MSG::INFO << box_offset[j] << "\t";

        if( !CheckBoxOffset(part, segment, layer, j, box_offset[j]) )
        {
          log << MSG::INFO << endreq << "Box offset P" << part << "S" << segment << "L" << layer
              << "_" << OFFSET_NAME[j] << "\tout range!" << endreq;
          box_offset[j] = B_X_MAX[j];
        }
      }

      for( int j=0; j<3; j++ )
      {
        log << MSG::INFO << strpln_offset[j] << "\t";

        if( !CheckStripPlaneOffset(part, segment, layer, j, strpln_offset[j]) )
        {
          log << MSG::INFO << endreq << "Strip plane offset P" << part << "S" << segment << "L" << layer
              << "_" << OFFSET_NAME[j] << "\tout range!" << endreq;
          strpln_offset[j] = STR_OFFSET_MAX[j];
        }
      }  
    
      log << MSG::INFO << endreq;   
    } // end box    

    froot->Close(); 
    log << MSG::INFO << "---------------------------------------------------------------" << endreq;
	} // end alignflag
	else
	{
		for(int i=0; i<PART_MAX; i++)
		  for(int j=0; j<B_SEG_NUM; j++)
		    for(int k=0; k<B_LAY_NUM; k++)
		      for(int m=0; m<3; m++)
          {
            m_BoxOffset[i][j][k][m] = 0.0;
			      m_StripPlaneOffset[i][j][k][m] = 0.0;
          }							
	} 

	return StatusCode::SUCCESS;
}

bool ZddGeoMgr::CheckBoxOffset( int part, int segment, int layer, int axis, double offset )
{
	int outRangeFlag = 0;

	if( part == BRID )
	{
		switch( axis )
		{
		 case 0:	// x
			int layerFlag;

			if( layer == 0 ) 	 layerFlag = 0; 
			else if( layer%2 == 1 )  layerFlag = 1; 
			else 			 layerFlag = 2; 

			if( B_X_MAX[layerFlag] - fabs(offset) >= 0.0 ) // |offset|<=B_X_MAX
				m_BoxOffset[part][segment][layer][axis] = offset;			
			else
			{	
				outRangeFlag ++;	
				m_BoxOffset[part][segment][layer][axis] = B_X_MAX[layerFlag] * MAX_FRACTION;
			}
			break;
		 case 1: 	// y
                        if( B_Y_MAX - fabs(offset) >= 0.0 ) // |offset|<=B_Y_MAX
                                m_BoxOffset[part][segment][layer][axis] = offset;                          
                        else
                        {
				outRangeFlag ++;
			        m_BoxOffset[part][segment][layer][axis] = B_Y_MAX * MAX_FRACTION;			
			}
			break;
		 case 2: 	// z
                        if( B_Z_MAX - fabs(offset) >= 0.0 ) // |offset|<=B_Y_MAX
                                m_BoxOffset[part][segment][layer][axis] = offset;
                        else
                        {
				outRangeFlag ++;
			        m_BoxOffset[part][segment][layer][axis] = B_Z_MAX * MAX_FRACTION;
			}
                        break;
		 default: ;
		}
	}	
	else
	{
                if( E_OFFSET_MAX[axis] - fabs(offset) >= 0.0 ) // |offset|<=B_Y_MAX
                	m_BoxOffset[part][segment][layer][axis] = offset;
                else
                {
			outRangeFlag ++;
		        m_BoxOffset[part][segment][layer][axis] = E_OFFSET_MAX[axis] * MAX_FRACTION;
		}
	}
	
	if( outRangeFlag > 0 ) 	return false;
	else 			return true;	
}

bool ZddGeoMgr::CheckStripPlaneOffset( int part, int segment, int layer, int axis, double offset )
{
        int outRangeFlag = 0;

        if( STR_OFFSET_MAX[axis] - fabs(offset) >= 0.0 ) // |offset|<=STR_OFFSET_MAX
	        m_StripPlaneOffset[part][segment][layer][axis] = offset;
        else
        {
                outRangeFlag ++;
                m_StripPlaneOffset[part][segment][layer][axis] = STR_OFFSET_MAX[axis] * MAX_FRACTION;
        }

        if( outRangeFlag > 0 )  return false;
        else                    return true;
}


//====================================== Geometry entities creating methods=============================================
//
//------------------------------------------Total data of all entities -------------------------------------------------
StatusCode ZddGeoMgr::CreateEntities()
{
  MsgStream log(msgSvc, "ZddGeoMgr");

	StatusCode sc;

	if( m_CreateFlag.size() < ENTITY_NUM )
	{
		for( unsigned int i=m_CreateFlag.size(); i<ENTITY_NUM; i++ )
			m_CreateFlag += '0';
	}

	int entity = 0;
	for( unsigned int i=0; i< ENTITY_NUM; i++ )
		if( m_CreateFlag[i] == '1' ) entity++;
	log << MSG::INFO << entity << "\tentities should be created." << endreq << endreq;

	if( m_CreateFlag[0] == '1' )
	{
		sc = CreateAbsorber();
		if( sc == StatusCode::SUCCESS )
			log << MSG::INFO << "Create absorber successfully!" << endreq << endreq;
		else
			log << MSG::INFO << "Create absorber failure!" << endreq << endreq;			
	}

        if( m_CreateFlag[1] == '1' )
        {
                sc = CreateGap();
                if( sc == StatusCode::SUCCESS )
                        log << MSG::INFO << "Create gap successfully!" << endreq << endreq;
                else
                        log << MSG::INFO << "Create gap failure!" << endreq << endreq;       
        }

        if( m_CreateFlag[2] == '1' )
        {
                sc = CreateBox();
                if( sc == StatusCode::SUCCESS )
                        log << MSG::INFO << "Create box successfully!" << endreq << endreq;
                else
                        log << MSG::INFO << "Create box failure!" << endreq << endreq;       
        }

        if( m_CreateFlag[3] == '1' )
        {
                sc = CreateStripPlane();
                if( sc == StatusCode::SUCCESS )
                        log << MSG::INFO << "Create strip_plane successfully!" << endreq << endreq;
                else
                        log << MSG::INFO << "Create strip_plane failure!" << endreq << endreq;       
        }

        if( m_CreateFlag[4] == '1' )
        {
                sc = CreateStrip();
                if( sc == StatusCode::SUCCESS )
                        log << MSG::INFO << "Create strip successfully!" << endreq << endreq;
                else
                        log << MSG::INFO << "Create strip failure!" << endreq << endreq;       
        }

        if( m_CreateFlag[5] == '1' )
        {
                sc = CreateRpc();
                if( sc == StatusCode::SUCCESS )
                        log << MSG::INFO << "Create RPC successfully!" << endreq << endreq;
                else
                        log << MSG::INFO << "Create RPC failure!" << endreq << endreq;       
        }

        if( m_CreateFlag[6] == '1' )
        {
                sc = CreateGas();
                if( sc == StatusCode::SUCCESS )
                        log << MSG::INFO << "Create gas mixture successfully!" << endreq << endreq;
                else
                        log << MSG::INFO << "Create gas mixture failure!" << endreq << endreq;       
        }

        if( m_CreateFlag[7] == '1' )
        {
                sc = CreateBakelite();
                if( sc == StatusCode::SUCCESS )
                        log << MSG::INFO << "Create bakelite successfully!" << endreq << endreq;
                else
                        log << MSG::INFO << "Create bakelite failure!" << endreq << endreq;       
        }

        if( m_CreateFlag[8] == '1' )
        {
                sc = CreateBoxCover();
                if( sc == StatusCode::SUCCESS )
                        log << MSG::INFO << "Create box cover successfully!" << endreq << endreq;
                else
                        log << MSG::INFO << "Create box cover failure!" << endreq << endreq;       
        }

	
	return StatusCode::SUCCESS;
}

//------------------------------------ROOT geometry-----------------------------------------------
StatusCode ZddGeoMgr::CreateRootGeo()
{
   MsgStream log(msgSvc, "ZddGeoMgr");
	//StatusCode sc;


	return StatusCode::SUCCESS;
}	


//------------------------------------Strip geometry for online display----------------------------
StatusCode ZddGeoMgr::CreateOnlineStripGeo()
{
	MsgStream log(msgSvc, "ZddGeoMgr");
	//StatusCode sc;

  //-------------------------- ideal geometry----------------------
  ofstream fEast("EastEndStripGeo.dat", ios::out);
  ofstream fBarrel("BarrelStripGeo.dat", ios::out);	
  ofstream fWest("WestEndStripGeo.dat", ios::out);	

  if( fEast.bad() || fBarrel.bad() || fWest.bad() )
  {
  	log << MSG::INFO << "Strip: create ouput file error!" << endl;
		return StatusCode::FAILURE;
  }
  
	for( int i=0; i<PART_MAX; i++ )
  {
  	if( i == BRID )
    {
    	for( int j=0; j<B_SEG_NUM; j++ )
      {
      	for( int k=0; k<B_LAY_NUM; k++ )
        {
			  	// Set maximum strip 
          int maxStrip;
          if( ( k+1 )%2 == 1 )
          	maxStrip = B_ZSTR_NUM;          // odd layer
          else if( j != B_TOP )
          	maxStrip = B_PHISTR_NUM;        // even layer not top segment
          else
          	maxStrip = B_TOPSTR_NUM;        // even layer top segment 

          for( int n=0; n<maxStrip; n++ )
          {
          	ZddStrip *aZddStrip = new ZddStrip( i, j, k, n );
						
						fBarrel	<< i << "\t" << j << "\t" << k << "\t" << n << "\t"
						      	<< aZddStrip->GetType()-2		  << "\t"
                    << aZddStrip->GetL() 		  <<"\t"
						      	<< aZddStrip->GetW() 		  << "\t"
						      	<< aZddStrip->GetH() 		  << "\t"
                    << aZddStrip->GetObjOrgInBes(1)   <<"\t"
                    << aZddStrip->GetObjOrgInBes(2)   <<"\t"
                    << aZddStrip->GetObjOrgInBes(3)   <<"\t"
                    << endl;
						// delete aZddStrip;
          } // for
        } // layer
      } // segment
    } // barrel
    else if( i == EEID )
    {
    	for( int j=0; j<E_SEG_NUM; j++ )
      {
      	for( int k=0; k<E_LAY_NUM; k++ )
        {
        	for( int n=0; n<E_STR_NUM; n++ )
          {
          	ZddStrip *aZddStrip = new ZddStrip( i, j, k, n );

	          fEast << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                  << aZddStrip->GetType()   	  << "\t"
                  << aZddStrip->GetL() 		  <<"\t"
                  << aZddStrip->GetW() 		  <<"\t"
                  << aZddStrip->GetH() 	 	  <<"\t"
                  << aZddStrip->GetObjOrgInBes(1)   <<"\t"
                  << aZddStrip->GetObjOrgInBes(2)   <<"\t"
                  << aZddStrip->GetObjOrgInBes(3)   <<"\t"
                  << endl;
						// delete aZddStrip;
          } // strip
        } // layer
      } // segment
    } // east endcap
		else
		{
   		for( int j=0; j<E_SEG_NUM; j++ )
      {
      	for( int k=0; k<E_LAY_NUM; k++ )
        {
        	for( int n=0; n<E_STR_NUM; n++ )
          {
          	ZddStrip *aZddStrip = new ZddStrip( i, j, k, n );

	          fWest << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                  << aZddStrip->GetType()   	  << "\t"
                  << aZddStrip->GetL() 		  <<"\t"
                  << aZddStrip->GetW() 		  <<"\t"
                  << aZddStrip->GetH() 	 	  <<"\t"
                  << aZddStrip->GetObjOrgInBes(1)   <<"\t"
                  << aZddStrip->GetObjOrgInBes(2)   <<"\t"
                  << aZddStrip->GetObjOrgInBes(3)   <<"\t"
                  << endl;
						// delete aZddStrip;
          } // strip
        } // layer
      } // segment
		} // west endcap

  } // part

	fEast.close();
	fBarrel.close();	
	fWest.close();

	log << MSG::INFO << "Online display strips created." << endreq;

	return StatusCode::SUCCESS;
}	


///////////////////////////////////////////////////////////////////////////////////////////////
//                                   Sub funtions                                            //
///////////////////////////////////////////////////////////////////////////////////////////////                                   
//------------------ZddAbsorber-------------
// No alignment
StatusCode ZddGeoMgr::CreateAbsorber()
{
  MsgStream log(msgSvc, "ZddGeoMgr");

	ofstream fOrigin("ZddAbsorberOrigin.dat", ios::out);
	ofstream fPanel("ZddAbsorberPanel.dat", ios::out);
	ofstream fPos("ZddAbsorberPanelPos.dat", ios::out);
	
	if( fOrigin.bad() || fPanel.bad() || fPos.bad() )
	{
		log << MSG::INFO << "Absorber: create ouput file error!" << endreq;
		return StatusCode::FAILURE;
	}
	fOrigin << "part\tsegment\tlayer\tW\tH\tL\tBes_x\tBes_y\tBes_z\tRot_x\tRot_y\tRot_z" << endl;
	fPanel  << "part\tsegment\tlayer\tpanel\tWu\tWd\tH\tL\tLoc_x\tLoc_y\tLoc_z" << endl;	
	fPos    << "part\tsegment\tlayer\tpanel\tLBes_x\tLBes_y\tLBes_z\tOBes_x\tOBes_y\tOBes_z\tLoc_x\tLoc_y\tLoc_z" << endl;

	int totalObject = 0;

	for( int i=0; i<PART_MAX; i++ )
	{
		if( i == BRID )
		{
			for( int j=0; j<B_SEG_NUM; j++ )
			{
				for( int k=0; k<B_LAY_NUM; k++ )
				{
					for( int n=0; n<B_AS_NUM; n++ )
					{
						ZddAbsorber *aZddAbsorber = new ZddAbsorber( i, j, k, n );
						fOrigin << i << "\t" << j << "\t" << k << "\t" 
                                                        << aZddAbsorber->GetW() <<"\t"
                                                        << aZddAbsorber->GetH() <<"\t"
                                                        << aZddAbsorber->GetL() <<"\t"				
					       		<< aZddAbsorber->GetLocOrgInBes(1)   <<"\t"
					       		<< aZddAbsorber->GetLocOrgInBes(2)   <<"\t"
					       		<< aZddAbsorber->GetLocOrgInBes(3)   <<"\t"
							<< aZddAbsorber->GetObjRotToMot(1)   <<"\t"
                                                        << aZddAbsorber->GetObjRotToMot(2)   <<"\t"
                                                        << aZddAbsorber->GetObjRotToMot(3)   <<"\t"
					       		<< endl;
						fPanel  <<  i << "\t" << j << "\t" << k << "\t" << n << "\t"
					       		<< aZddAbsorber->GetWu() <<"\t"
							<< aZddAbsorber->GetWd() <<"\t"
					       		<< aZddAbsorber->GetH()  <<"\t"
					       		<< aZddAbsorber->GetL()  <<"\t"	
							<< aZddAbsorber->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInLoc(3)   <<"\t"
							<< endl;
						fPos    << i << "\t" << j << "\t" << k << "\t" << n << "\t"
							<< aZddAbsorber->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddAbsorber->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddAbsorber->GetLocOrgInBes(3)   <<"\t"
							<< aZddAbsorber->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInBes(3)   <<"\t"
							<< aZddAbsorber->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInLoc(3)   <<"\t"
							<< endl;

						totalObject++;
					//	delete aZddAbsorber;
					}
				} // layer
			} // segment
		} // barrel
		else
		{
			for( int j=0; j<E_SEG_NUM; j++ )
			{
				for( int k=0; k<E_ASLAY_NUM; k++ )
				{
					for( int n=-1; n<E_PANEL_NUM; n++ )
					{
						ZddAbsorber *aZddAbsorber = new ZddAbsorber( i, j, k, n );
						if( n == -1 )
						{
						 fOrigin<< i << "\t" << j << "\t" << k << "\t"
                                                        << aZddAbsorber->GetW() <<"\t"
                                                        << aZddAbsorber->GetH() <<"\t"
                                                        << aZddAbsorber->GetL() <<"\t"
                                                        << aZddAbsorber->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddAbsorber->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddAbsorber->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddAbsorber->GetObjRotToMot(1)   <<"\t"
                                                        << aZddAbsorber->GetObjRotToMot(2)   <<"\t"
                                                        << aZddAbsorber->GetObjRotToMot(3)   <<"\t"
                                                        << endl;

							totalObject ++;
							// delete aZddAbsorber;
						}
						else
						{
						  fPanel<< i << "\t" << j << "\t" << k << "\t" << n << "\t"
							<< aZddAbsorber->GetWu() <<"\t"
                                                        << aZddAbsorber->GetWd() <<"\t"
                                                        << aZddAbsorber->GetH()  <<"\t"
                                                        << aZddAbsorber->GetL()  <<"\t"
                                                        << aZddAbsorber->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;

						fPos    << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddAbsorber->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddAbsorber->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddAbsorber->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddAbsorber->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
							// delete aZddAbsorber;
						}
					}
				} // layer
			} // segment
		} // endcap
	} // for
	
	fOrigin.close();
	fPanel.close();
	fPos.close();
	
	log << MSG::INFO << totalObject << "\tabsorbers created." << endreq;
	
	return StatusCode::SUCCESS;
} // ZddAbsorber

//------------------ZddGap-------------
// No alignment
StatusCode ZddGeoMgr::CreateGap()
{
        MsgStream log(msgSvc, "ZddGeoMgr");

        ofstream fOrigin("ZddGapOrigin.dat", ios::out);
        ofstream fPanel("ZddGapPanel.dat", ios::out);
        ofstream fPos("ZddGapPanelPos.dat", ios::out);

        if( fOrigin.bad() || fPanel.bad() || fPos.bad() )
        {
                log << MSG::INFO << "Gap: create ouput file error!" << endreq;
                return StatusCode::FAILURE;
        }
        fOrigin << "part\tsegment\tlayer\tW\tH\tL\tBes_x\tBes_y\tBes_z\tRot_x\tRot_y\tRot_z" << endl;
        fPanel  << "part\tsegment\tlayer\tpanel\tWu\tWd\tH\tL\tLoc_x\tLoc_y\tLoc_z" << endl;
        fPos    << "part\tsegment\tlayer\tpanel\tLBes_x\tLBes_y\tLBes_z\tOBes_x\tOBes_y\tOBes_z\tLoc_x\tLoc_y\tLoc_z" << endl;

	int totalObject = 0;

        for( int i=0; i<PART_MAX; i++ )
        {
                if( i == BRID )
                {
                        for( int j=0; j<B_SEG_NUM; j++ )
                        {
				// set panel number
				int idMin, idMax;
				if( j != B_TOP ) { idMin = 0; idMax = B_GP_NUM;	}
				else		 { idMin = -1; idMax = 3;    	}

                                for( int k=0; k<B_LAY_NUM; k++ )
                                {
                                        for( int n=idMin; n<idMax; n++ )
                                        {
                                                ZddGap *aZddGap = new ZddGap( i, j, k, n );

						if( j == B_TOP && n != -1 ) // barrel top segment panels
						{
                                                  fPanel<< i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddGap->GetWu() <<"\t"
                                                        << aZddGap->GetWd() <<"\t"
                                                        << aZddGap->GetH()  <<"\t"
                                                        << aZddGap->GetL()  <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						}

                                                if( j !=B_TOP || n == -1 )
                                                {
                                                 fOrigin<< i << "\t" << j << "\t" << k << "\t"
                                                        << aZddGap->GetW() <<"\t"
                                                        << aZddGap->GetH() <<"\t"
                                                        << aZddGap->GetL() <<"\t"	
                                                        << aZddGap->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddGap->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddGap->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddGap->GetObjRotToMot(1)   <<"\t"
                                                        << aZddGap->GetObjRotToMot(2)   <<"\t"
                                                        << aZddGap->GetObjRotToMot(3)   <<"\t"
                                                        << endl;

						 totalObject++;
						 // delete aZddGap;
                                                }						

                                                fPos    << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddGap->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddGap->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddGap->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddGap->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddGap->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddGap->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
                                        }
                                } // layer
                        } // segment
                } // barrel
                else
		{
                        for( int j=0; j<E_SEG_NUM; j++ )
                        {
                                for( int k=0; k<E_LAY_NUM; k++ )
                                {
                                        for( int n=-1; n<E_PANEL_NUM; n++ )
                                        {
                                                ZddGap *aZddGap = new ZddGap( i, j, k, n );

                                                if( n == -1 )
                                                {
                                                 fOrigin<< i << "\t" << j << "\t" << k << "\t"
                                                        << aZddGap->GetW() <<"\t"
                                                        << aZddGap->GetH() <<"\t"
                                                        << aZddGap->GetL() <<"\t"	
                                                        << aZddGap->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddGap->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddGap->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddGap->GetObjRotToMot(1)   <<"\t"
                                                        << aZddGap->GetObjRotToMot(2)   <<"\t"
                                                        << aZddGap->GetObjRotToMot(3)   <<"\t"
                                                        << endl;

						 totalObject++;
                                                }
                                                else
                                                {
                                                  fPanel<< i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddGap->GetWu() <<"\t"
                                                        << aZddGap->GetWd() <<"\t"
                                                        << aZddGap->GetH()  <<"\t"
                                                        << aZddGap->GetL()  <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;

                                                  fPos  << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddGap->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddGap->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddGap->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddGap->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddGap->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddGap->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddGap->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						  // delete aZddGap;
                                                }
                                        }
                                } // layer
                        } // segment
                } // endcap
        } // for

      	fOrigin.close();
        fPanel.close();
        fPos.close();

        log << MSG::INFO << totalObject << "\tgaps created." << endreq;

        return StatusCode::SUCCESS;
} // ZddGap

//------------------ZddBox-------------
// Alignment
StatusCode ZddGeoMgr::CreateBox()
{
        MsgStream log(msgSvc, "ZddGeoMgr");

	//-------------------------- ideal geometry----------------------
        ofstream fOrigin("ZddBoxOrigin.dat", ios::out);
        ofstream fPanel("ZddBoxPanel.dat", ios::out);
        ofstream fPos("ZddBoxPanelPos.dat", ios::out);

        if( fOrigin.bad() || fPanel.bad() || fPos.bad() )
        {
                log << MSG::INFO << "Box: create ouput file error!" << endl;
		return StatusCode::FAILURE;
        }
        fOrigin << "part\tsegment\tlayer\tW\tH\tL\tLoc_x\tLoc_y\tLoc_z" << endl;
        fPanel  << "part\tsegment\tlayer\tpanel\tWu\tWd\tH\tL\tLoc_x\tLoc_y\tLoc_z" << endl;
        fPos    << "part\tsegment\tlayer\tpanel\tLBes_x\tLBes_y\tLBes_z\tOBes_x\tOBes_y\tOBes_z\tLoc_x\tLoc_y\tLoc_z" << endl;

	int totalObject = 0;
        for( int i=0; i<PART_MAX; i++ )
        {
                if( i == BRID )
                {
                        for( int j=0; j<B_SEG_NUM; j++ )
                        {	
				// set panel number
				int idMin, idMax;
				if( j != B_TOP ) { idMin = 0; idMax = B_GP_NUM;	}
				else		 { idMin = -1; idMax = 3;    	}

                                for( int k=0; k<B_LAY_NUM; k++ )
                                {
                                        for( int n=idMin; n<idMax; n++ )
                                        {
                                                ZddBox *aZddBox = new ZddBox( i, j, k, n );

						if( j == B_TOP && n != -1 ) // barrel top segment panels
						{
                                                  fPanel<< i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddBox->GetWu() <<"\t"
                                                        << aZddBox->GetWd() <<"\t"
                                                        << aZddBox->GetH()  <<"\t"
                                                        << aZddBox->GetL()  <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						}
						
						if( j !=B_TOP || n == -1 ) // box
						{
                                                  fOrigin << i << "\t" << j << "\t" << k << "\t"
                                                        << aZddBox->GetW() <<"\t"
                                                        << aZddBox->GetH() <<"\t"
                                                        << aZddBox->GetL() <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;

						  totalObject++;
						  // delete aZddBox;
						}

                                                fPos    << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddBox->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddBox->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddBox->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddBox->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddBox->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddBox->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						// delete aZddBox;
                                        } // panel
                                } // layer
                        } // segment
                } // barrel
                else
                {
                        for( int j=0; j<E_SEG_NUM; j++ )
                        {
                                for( int k=0; k<E_LAY_NUM; k++ )
                                {
                                        for( int n=-1; n<E_PANEL_NUM; n++ )
                                        {
                                                ZddBox *aZddBox = new ZddBox( i, j, k, n );
                                                if( n == -1 )
                                                {
                                                 fOrigin<< i << "\t" << j << "\t" << k << "\t"
                                                        << aZddBox->GetW() <<"\t"
                                                        << aZddBox->GetH() <<"\t"
                                                        << aZddBox->GetL() <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;

						 totalObject++;
						 // delete aZddBox;
                                                }
                                                else
                                                {
                                                  fPanel<< i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddBox->GetWu() <<"\t"
                                                        << aZddBox->GetWd() <<"\t"
                                                        << aZddBox->GetH()  <<"\t"
                                                        << aZddBox->GetL()  <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						  // delete aZddBox;
						}

                                                  fPos  << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddBox->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddBox->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddBox->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddBox->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddBox->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddBox->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						  // delete aZddBox;
                                        }
                                } // layer
                        } // segment
                } // endcap
        } // for

        fOrigin.close();
        fPanel.close();
        fPos.close();

	//-----------------------------real geometry-------------------------

	if( m_AlignFlag )
	{
		ofstream fOrgAlign("ZddBoxOriginAligned.dat", ios::out);
		fOrgAlign  << "part\tsegment\tlayer\tW\tH\tL\tLoc_x\tLoc_y\tLoc_z" << endl;
		double offset[3];
		for( int i=0; i<PART_MAX; i++ )
        	{
                	if( i == BRID )
                	{
                        	for( int j=0; j<B_SEG_NUM; j++ )
                        	{
                                	for( int k=0; k<B_LAY_NUM; k++ )
                                	{
                                                ZddBox *aZddBox = new ZddBox( i, j, k, ((j==B_TOP)?-1:0) );
						offset[0] =m_BoxOffset[i][j][k][0];
					        offset[1] =m_BoxOffset[i][j][k][1];
						offset[2] =m_BoxOffset[i][j][k][2];
						aZddBox->SetAlignment( offset[0], offset[1], offset[2] );

                                                fOrgAlign << i << "\t" << j << "\t" << k << "\t"
                                                        << aZddBox->GetW() <<"\t"
                                                        << aZddBox->GetH() <<"\t"
                                                        << aZddBox->GetL() <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						// delete aZddBox;
                                	} // layer
                        	} // segment
                	} // barrel
			else
                	{
                        	for( int j=0; j<E_SEG_NUM; j++ )
                        	{
                                	for( int k=0; k<E_LAY_NUM; k++ )
                                	{
                                                ZddBox *aZddBox = new ZddBox( i, j, k, -1 );
						offset[0] =m_BoxOffset[i][j][k][0];
					        offset[1] =m_BoxOffset[i][j][k][1];
						offset[2] =m_BoxOffset[i][j][k][2];
						aZddBox->SetAlignment( offset[0], offset[1], offset[2] );
						
                                                 fOrgAlign<< i << "\t" << j << "\t" << k << "\t"
                                                        << aZddBox->GetW() <<"\t"
                                                        << aZddBox->GetH() <<"\t"
                                                        << aZddBox->GetL() <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddBox->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						// delete aZddBox;
                                	} // layer
                        	} // segment
                	} // endcap
		} // for
		
		fOrgAlign.close();
	} // if


        log << MSG::INFO << totalObject << "\tboxes created." << endreq;

        return StatusCode::SUCCESS;
} // ZddBox


//------------------ZddStripPlane-------------
// Alignment
StatusCode ZddGeoMgr::CreateStripPlane()
{
	MsgStream log(msgSvc, "ZddGeoMgr");

        //-------------------------- ideal geometry----------------------
        ofstream fOrigin("ZddStripPlaneOrigin.dat", ios::out);
        ofstream fPanel("ZddStripPlanePanel.dat", ios::out);
        ofstream fPos("ZddStripPlanePanelPos.dat", ios::out);

        if( fOrigin.bad() || fPanel.bad() || fPos.bad() )
        {
                log << MSG::INFO << "StripPlane: create ouput file error!" << endl;
	 	return StatusCode::FAILURE;
        }
        fOrigin << "part\tsegment\tlayer\tW\tH\tL\tLoc_x\tLoc_y\tLoc_z" << endl;
        fPanel  << "part\tsegment\tlayer\tpanel\tWu\tWd\tH\tL\tLoc_x\tLoc_y\tLoc_z" << endl;
        fPos    << "part\tsegment\tlayer\tpanel\tLBes_x\tLBes_y\tLBes_z\tOBes_x\tOBes_y\tOBes_z\tLoc_x\tLoc_y\tLoc_z" << endl;
	
	int totalObject = 0;

        for( int i=0; i<PART_MAX; i++ )
        {
                if( i == BRID )
                {
                        for( int j=0; j<B_SEG_NUM; j++ )
                        {
                                for( int k=0; k<B_LAY_NUM; k++ )
                                {	
					if( j==B_TOP )
					{
                                           for( int n=-1; n<B_STR_PANEL_NUM; n++ )
					   {	   
						ZddStripPlane *aZddStripPlane = new ZddStripPlane( i, j, k, n );
						if( n == -1 )
						{
                                                  fOrigin << i << "\t" << j << "\t" << k << "\t"
                                                        << aZddStripPlane->GetW() <<"\t"
                                                        << aZddStripPlane->GetH() <<"\t"
                                                        << aZddStripPlane->GetL() <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
                                                  fPos  << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddStripPlane->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddStripPlane->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddStripPlane->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						  totalObject++;
						  // delete aZddStripPlane;
						}
						else
						{	
					    	  fPanel<< i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddStripPlane->GetWu() <<"\t"
                                                        << aZddStripPlane->GetWd() <<"\t"
                                                        << aZddStripPlane->GetH()  <<"\t"
                                                        << aZddStripPlane->GetL()  <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;

                                                  fPos  << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddStripPlane->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddStripPlane->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddStripPlane->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						  // delete aZddStripPlane;	
						}
					   }// for	
					}// B_TOP
					else	
                                        {
                                                ZddStripPlane *aZddStripPlane = new ZddStripPlane( i, j, k, 0 );
                                                fOrigin << i << "\t" << j << "\t" << k << "\t"
                                                        << aZddStripPlane->GetW() <<"\t"
                                                        << aZddStripPlane->GetH() <<"\t"
                                                        << aZddStripPlane->GetL() <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
                                                fPos    << i << "\t" << j << "\t" << k << "\t" << "\t"
                                                        << aZddStripPlane->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddStripPlane->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddStripPlane->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						totalObject++;
						// delete aZddStripPlane;
                                        }
                                } // layer
                        } // segment
                } // barrel
                else
                {
                        for( int j=0; j<E_SEG_NUM; j++ )
                        {
                                for( int k=0; k<E_LAY_NUM; k++ )
                                {
                                        for( int n=-1; n<E_PANEL_NUM; n++ )
                                        {
                                                ZddStripPlane *aZddStripPlane = new ZddStripPlane( i, j, k, n );
                                                if( n == -1 )
                                                {
                                                 fOrigin<< i << "\t" << j << "\t" << k << "\t"
                                                        << aZddStripPlane->GetW() <<"\t"
                                                        << aZddStripPlane->GetH() <<"\t"
                                                        << aZddStripPlane->GetL() <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;

						  fPos  << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddStripPlane->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddStripPlane->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddStripPlane->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						 totalObject++;
						 // delete aZddStripPlane;
                                                }
                                                else
                                                {
                                                  fPanel<< i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddStripPlane->GetWu() <<"\t"
                                                        << aZddStripPlane->GetWd() <<"\t"
                                                        << aZddStripPlane->GetH()  <<"\t"
                                                        << aZddStripPlane->GetL()  <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;

                                                  fPos  << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddStripPlane->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddStripPlane->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddStripPlane->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						  // delete aZddStripPlane;
                                                }
                                        }
                                } // layer
                        } // segment
                } // endcap
	} // for

        fOrigin.close();
        fPanel.close();
        fPos.close();

        //-----------------------------real geometry-------------------------

        if( m_AlignFlag )
        {
                ofstream fOrgAlign("ZddStripPlaneOriginAligned.dat", ios::out);
                fOrgAlign  << "part\tsegment\tlayer\tW\tH\tL\tLoc_x\tLoc_y\tLoc_z" << endl;
		double offset[3]; 
                for( int i=0; i<PART_MAX; i++ )
                {
                        if( i == BRID )
                        {
                                for( int j=0; j<B_SEG_NUM; j++ )
                                {
                                        for( int k=0; k<B_LAY_NUM; k++ )
                                        {
                                                ZddStripPlane *aZddStripPlane = new ZddStripPlane( i, j, k, 0 );
						offset[0] =m_StripPlaneOffset[i][j][k][0];
					        offset[1] =m_StripPlaneOffset[i][j][k][1];
					        offset[2] =m_StripPlaneOffset[i][j][k][2];
						aZddStripPlane->SetAlignment( offset[0], offset[1], offset[2] );
						
                                                fOrgAlign << i << "\t" << j << "\t" << k << "\t"
                                                        << aZddStripPlane->GetW() <<"\t"
                                                        << aZddStripPlane->GetH() <<"\t"
                                                        << aZddStripPlane->GetL() <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						// delete aZddStripPlane;
                                        } // layer
                                } // segment
                        } // barrel
                        else
                        {
                                for( int j=0; j<E_SEG_NUM; j++ )
                                {
                                        for( int k=0; k<E_LAY_NUM; k++ )
                                        {
                                                ZddStripPlane *aZddStripPlane = new ZddStripPlane( i, j, k, -1 );
						offset[0] =m_StripPlaneOffset[i][j][k][0];
					        offset[1] =m_StripPlaneOffset[i][j][k][1];
					        offset[2] =m_StripPlaneOffset[i][j][k][2];
						aZddStripPlane->SetAlignment( offset[0], offset[1], offset[2] );

                                                 fOrgAlign<< i << "\t" << j << "\t" << k << "\t"
                                                        << aZddStripPlane->GetW() <<"\t"
                                                        << aZddStripPlane->GetH() <<"\t"
                                                        << aZddStripPlane->GetL() <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStripPlane->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						// delete aZddStripPlane;
                                        } // layer
                                } // segment
                        } // endcap
                } // for

		fOrgAlign.close();
        } // if
	

        log << MSG::INFO << totalObject << "\tstrip_planes created." << endreq;

        return StatusCode::SUCCESS;

} // ZddStripPlane

//------------------ZddStrip------------------
StatusCode ZddGeoMgr::CreateStrip()
{
	MsgStream log(msgSvc, "ZddGeoMgr");

        //-------------------------- ideal geometry----------------------
        ofstream fOrigin("ZddStrip.dat", ios::out);
        ofstream fPos("ZddStripPos.dat", ios::out);	

        if( fOrigin.bad() || fPos.bad() )
        {
                log << MSG::INFO << "Strip: create ouput file error!" << endl;
		return StatusCode::FAILURE;
        }
        fOrigin << "part\tsegment\tlayer\tstrip\ttype\tW\tH\tL\tLoc_x\tLoc_y\tLoc_z\tBes_x\tBes_y\tBes_z" << endl;
	fPos 	<< "part\tsegment\tlayer\tstrip\ttype\tLBes_x\tLBes_y\tLBes_z\tOBes_x\tOBes_y\tOBes_z\tLoc_x\tLoc_y\tLoc_z" << endl;
	
	int totalObject = 0;
	
        for( int i=0; i<PART_MAX; i++ )
        {
                if( i == BRID )
                {
                        for( int j=0; j<B_SEG_NUM; j++ )
                        {
                                for( int k=0; k<B_LAY_NUM; k++ )
                                {
			                // Set maximum strip 
                			int maxStrip;
                			if( ( k+1 )%2 == 1 )
                        			maxStrip = B_ZSTR_NUM;          // odd layer
                			else if( j != B_TOP )
                        			maxStrip = B_PHISTR_NUM;        // even layer not top segment
                			else
                        			maxStrip = B_TOPSTR_NUM;        // even layer top segment 

                                        for( int n=0; n<maxStrip; n++ )
                                        {
                                                ZddStrip *aZddStrip = new ZddStrip( i, j, k, n );

						fOrigin	<< i << "\t" << j << "\t" << k << "\t" << n << "\t"
						     	<< aZddStrip->GetType()		  << "\t"
						     	<< aZddStrip->GetW() 		  << "\t"
						     	<< aZddStrip->GetH() 		  << "\t"
                                                        << aZddStrip->GetL() 		  <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(3)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(3)   <<"\t"
                                                        << endl;

						fPos    << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddStrip->GetType()           <<"\t"
                                                        << aZddStrip->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddStrip->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddStrip->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;

						totalObject++;
						// delete aZddStrip;
                                        } // for
                                } // layer
                        } // segment
                } // barrel
                else
                {
                        for( int j=0; j<E_SEG_NUM; j++ )
                        {
                                for( int k=0; k<E_LAY_NUM; k++ )
                                {
                                        for( int n=0; n<E_STR_NUM; n++ )
                                        {
                                                ZddStrip *aZddStrip = new ZddStrip( i, j, k, n );

                                                fOrigin << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddStrip->GetType()   	  << "\t"
                                                        << aZddStrip->GetW() 		  <<"\t"
                                                        << aZddStrip->GetH() 	 	  <<"\t"
                                                        << aZddStrip->GetL() 		  <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(3)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(3)   <<"\t"
                                                        << endl;

                                                fPos  	<< i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddStrip->GetType()           << "\t"
                                                        << aZddStrip->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddStrip->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddStrip->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddStrip->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddStrip->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
						totalObject++;
						// delete aZddStrip;
                                        } // for
                                } // layer
                        } // segment
                } // endcap
        } // for

	fOrigin.close();
	fPos.close();	

        log << MSG::INFO << totalObject << "\tstrips created." << endreq;

        return StatusCode::SUCCESS;

} // ZddStrip


//------------------ZddRpc--------------------
StatusCode ZddGeoMgr::CreateRpc()
{
        MsgStream log(msgSvc, "ZddGeoMgr");

        //-------------------------- ideal geometry----------------------

        ofstream fOrigin("ZddRpc.dat", ios::out);
        ofstream fPos("ZddRpcPos.dat", ios::out);

        if( fOrigin.bad() || fPos.bad() )
        {
                log << MSG::INFO << "Rpc: create ouput file error!" << endl;
                return StatusCode::FAILURE;
        }
        fOrigin << "part\tsegment\tlayer\tupDown\trpc\tWu\tWd\tH\tL\tLoc_x\tLoc_y\tLoc_z\tBes_x\tBes_y\tBes_z" << endl;
        fPos    << "part\tsegment\tlayer\tupDown\trpc\tLBes_x\tLBes_y\tLBes_z\tOBes_x\tOBes_y\tOBes_z\tLoc_x\tLoc_y\tLoc_z" << endl;

        int totalObject = 0;

        for( int i=0; i<PART_MAX; i++ )
        {
                if( i == BRID )
                {
                        for( int j=0; j<B_SEG_NUM; j++ )
                        {
                                // Set maximum rpc
                                int maxRpc;
                                if( j ==B_TOP )
                                        maxRpc = B_TOP_RPC_NUM;    // top segment
                                else
                                        maxRpc = B_RPC_NUM;        // not top segment

                                for( int k=0; k<B_LAY_NUM; k++ )
                                {
                                        for( int m=0; m<SL_NUM; m++ )
                                        {
                                                for( int n=0; n<maxRpc; n++ )
                                                {
                                                        ZddRpc *aZddRpc = new ZddRpc( i, j, k, m, n );

                                                fOrigin << i << "\t" << j << "\t" << k << "\t"
                                                        << m << "\t" << n << "\t"
                                                        << aZddRpc->GetWu()             << "\t"
                                                        << aZddRpc->GetWd()             << "\t"
                                                        << aZddRpc->GetH()              << "\t"
                                                        << aZddRpc->GetL()              <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(3)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(3)   <<"\t"
                                                        << endl;

                                                fPos    << i << "\t" << j << "\t" << k << "\t" 
							<< m << "\t" << n << "\t"
                                                        << aZddRpc->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddRpc->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddRpc->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;

                                                totalObject++;
						// delete aZddRpc;
                                                } // for
                                        } // super layer
                                } // layer
                        } // segment
                } // barrel
                else
                {
                        for( int j=0; j<E_SEG_NUM; j++ )
                        {
                                for( int k=0; k<E_LAY_NUM; k++ )
                                {
                                        for( int m=0; m<SL_NUM; m++ )
                                        {
                                                for( int n=0; n<E_RPC_NUM[m]; n++ )
                                                {
                                                        ZddRpc *aZddRpc = new ZddRpc( i, j, k, m, n );

                                                fOrigin << i << "\t" << j << "\t" << k << "\t"
                                                        << m << "\t" << n << "\t"
                                                        << aZddRpc->GetWu()              <<"\t"
                                                        << aZddRpc->GetWd()              <<"\t"
                                                        << aZddRpc->GetH()              <<"\t"
                                                        << aZddRpc->GetL()              <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(3)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(3)   <<"\t"
                                                        << endl;

                                                fPos    << i << "\t" << j << "\t" << k << "\t"
                                                        << m << "\t" << n << "\t"
                                                        << aZddRpc->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddRpc->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddRpc->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddRpc->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddRpc->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
                                                totalObject++;
						// delete aZddRpc;
                                                } // for
                                        } // super layer
                                } // layer
                        } // segment
                } // endcap
        } // for, part

	fOrigin.close();
	fPos.close();

        log << MSG::INFO << totalObject << "\t RPCs created." << endreq;

        return StatusCode::SUCCESS;

} // ZddRpc

//------------------ZddGas--------------------
StatusCode ZddGeoMgr::CreateGas()
{
        MsgStream log(msgSvc, "ZddGeoMgr");

        //-------------------------- ideal geometry----------------------

        ofstream fOrigin("ZddGas.dat", ios::out);
        ofstream fPos("ZddGasPos.dat", ios::out);

        if( fOrigin.bad() || fPos.bad() )
        {
                log << MSG::INFO << "Gas: create ouput file error!" << endl;
                return StatusCode::FAILURE;
        }
        fOrigin << "part\tsegment\tlayer\tupDown\tgas\tWu\tWd\tH\tL\tLoc_x\tLoc_y\tLoc_z\tBes_x\tBes_y\tBes_z" << endl;
        fPos    << "part\tsegment\tlayer\tupDown\tgas\tLBes_x\tLBes_y\tLBes_z\tOBes_x\tOBes_y\tOBes_z\tLoc_x\tLoc_y\tLoc_z" << endl;

        int totalObject = 0;

        for( int i=0; i<PART_MAX; i++ )
        {
                if( i == BRID )
                {
                        for( int j=0; j<B_SEG_NUM; j++ )
                        {
                                // Set maximum gas
                                int maxRpc;
                                if( j ==B_TOP )
                                        maxRpc = B_TOP_RPC_NUM;    // top segment
                                else
                                        maxRpc = B_RPC_NUM;        // not top segment

                                for( int k=0; k<B_LAY_NUM; k++ )
                                {
                                        for( int m=0; m<SL_NUM; m++ )
                                        {
                                                for( int n=0; n<maxRpc; n++ )
                                                {
                                                        ZddGas *aZddGas = new ZddGas( i, j, k, m, n, 0);

                                                fOrigin << i << "\t" << j << "\t" << k << "\t"
                                                        << m << "\t" << n << "\t"
                                                        << aZddGas->GetWu()             <<"\t"
                                                        << aZddGas->GetWd()             <<"\t"
                                                        << aZddGas->GetH()              <<"\t"
                                                        << aZddGas->GetL()              <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(3)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(3)   <<"\t"
                                                        << endl;

                                                fPos    << i << "\t" << j << "\t" << k << "\t" << n << "\t"
                                                        << aZddGas->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddGas->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddGas->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;

                                                totalObject++;
						// delete aZddGas;
                                                } // for
                                        } // super layer
                                } // layer
                        } // segment
                } // barrel
                else
                {
                        for( int j=0; j<E_SEG_NUM; j++ )
                        {
                                for( int k=0; k<E_LAY_NUM; k++ )
                                {
                                        for( int m=0; m<SL_NUM; m++ )
                                        {
                                                for( int n=0; n<E_RPC_NUM[m]; n++ )
                                                {
                                                        ZddGas *aZddGas = new ZddGas( i, j, k, m, n, 0 );

                                                fOrigin << i << "\t" << j << "\t" << k << "\t"
                                                        << m << "\t" << n << "\t"
                                                        << aZddGas->GetWu()              <<"\t"
                                                        << aZddGas->GetWd()              <<"\t"
                                                        << aZddGas->GetH()              <<"\t"
                                                        << aZddGas->GetL()              <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(3)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(3)   <<"\t"
                                                        << endl;

                                                fPos    << i << "\t" << j << "\t" << k << "\t"
                                                        << m << "\t" << n << "\t"
                                                        << aZddGas->GetLocOrgInBes(1)   <<"\t"
                                                        << aZddGas->GetLocOrgInBes(2)   <<"\t"
                                                        << aZddGas->GetLocOrgInBes(3)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(1)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(2)   <<"\t"
                                                        << aZddGas->GetObjOrgInBes(3)   <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(1)   <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(2)   <<"\t"
                                                        << aZddGas->GetObjOrgInLoc(3)   <<"\t"
                                                        << endl;
                                                totalObject++;
						// delete aZddGas;
                                                } // for
                                        } // super layer
                                } // layer
                        } // segment
                } // endcap
        } // for, part

	fOrigin.close();
	fPos.close();

        log << MSG::INFO << totalObject << "\tgases created." << endreq;

        return StatusCode::SUCCESS;

} // ZddGas

//------------------ZddBakelite---------------
StatusCode ZddGeoMgr::CreateBakelite()
{
        MsgStream log(msgSvc, "ZddGeoMgr");

        //-------------------------- ideal geometry----------------------

        ofstream fOrigin("ZddBakelite.dat", ios::out);
        ofstream fPos("ZddBakelitePos.dat", ios::out);

        if( fOrigin.bad() || fPos.bad() )
        {
                log << MSG::INFO << "Bakelite: create ouput file error!" << endl;
                return StatusCode::FAILURE;
        }
        fOrigin << "part\tsegment\tlayer\tupDown\trpcId\tBKLT\t"
                << "Wu\tWd\tH\tL\tLoc_x\tLoc_y\tLoc_z\tBes_x\tBes_y\tBes_z" << endl;
        fPos    << "part\tsegment\tlayer\tupDown\trpcId\tBKLT\t"
                << "LBes_x\tLBes_y\tLBes_z\tOBes_x\tOBes_y\tOBes_z\tLoc_x\tLoc_y\tLoc_z" << endl;

        int totalObject = 0;

        for( int i=0; i<PART_MAX; i++ )
        {
                if( i == BRID )
                {
                        for( int j=0; j<B_SEG_NUM; j++ )
                        {
                                // Set maximum rpc number
                                int maxRpc;
                                if( j ==B_TOP )
                                        maxRpc = B_TOP_RPC_NUM;    // top segment
                                else
                                        maxRpc = B_RPC_NUM;        // not top segment

                                for( int k=0; k<B_LAY_NUM; k++ )
                                {
                                        for( int m=0; m<SL_NUM; m++ )
                                        {
                                                for( int n=0; n<maxRpc; n++ )
                                                {
                                                        for( int t=0; t<BKLT_NUM; t++)
                                                        {
                                                         ZddBakelite *aZddBakelite = new ZddBakelite( i, j, k, m, n, t );

                                                         fOrigin<< i << "\t" << j << "\t" << k << "\t"
                                                                << m << "\t" << n << "\t" << t << "\t"
                                                                << aZddBakelite->GetWu()             <<"\t"
                                                                << aZddBakelite->GetWd()             <<"\t"
                                                                << aZddBakelite->GetH()              <<"\t"
                                                                << aZddBakelite->GetL()              <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(1)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(2)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(3)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(1)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(2)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(3)   <<"\t"
                                                                << endl;

                                                         fPos   << i << "\t" << j << "\t" << k << "\t"
                                                                << m << "\t" << n << "\t" << t << "\t"
                                                                << aZddBakelite->GetLocOrgInBes(1)   <<"\t"
                                                                << aZddBakelite->GetLocOrgInBes(2)   <<"\t"
                                                                << aZddBakelite->GetLocOrgInBes(3)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(1)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(2)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(3)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(1)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(2)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(3)   <<"\t"
                                                                << endl;

                                                         totalObject++;
							 // delete aZddBakelite;	
                                                        } // bakelite
                                                } // rpc
                                        } // super layer
                                } // layer
                        } // segment
                } // barrel
                else
                {
                        for( int j=0; j<E_SEG_NUM; j++ )
                        {
                                for( int k=0; k<E_LAY_NUM; k++ )
                                {
                                        for( int m=0; m<SL_NUM; m++ )
                                        {
                                                for( int n=0; n<E_RPC_NUM[m]; n++ )
                                                {
                                                        for( int t=0; t<BKLT_NUM; t++ )
                                                        {
                                                         ZddBakelite *aZddBakelite = new ZddBakelite( i, j, k, m, n, t );

                                                         fOrigin<< i << "\t" << j << "\t" << k << "\t"
                                                                << m << "\t" << n << "\t" << t << "\t"
                                                                << aZddBakelite->GetWu()             <<"\t"
                                                                << aZddBakelite->GetWd()             <<"\t"
                                                                << aZddBakelite->GetH()              <<"\t"
                                                                << aZddBakelite->GetL()              <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(1)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(2)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(3)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(1)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(2)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(3)   <<"\t"
                                                                << endl;

                                                         fPos   << i << "\t" << j << "\t" << k << "\t"
                                                                << m << "\t" << n << "\t" << t << "\t"
                                                                << aZddBakelite->GetLocOrgInBes(1)   <<"\t"
                                                                << aZddBakelite->GetLocOrgInBes(2)   <<"\t"
                                                                << aZddBakelite->GetLocOrgInBes(3)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(1)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(2)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInBes(3)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(1)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(2)   <<"\t"
                                                                << aZddBakelite->GetObjOrgInLoc(3)   <<"\t"
                                                                << endl;

                                                         totalObject++;
							 // delete aZddBakelite;
                                                        } // bakelite
                                                } // rpc
                                        } // super layer
                                } // layer
                        } // segment
                } // endcap
        } // for, part

	fOrigin.close();
	fPos.close();

        log << MSG::INFO << totalObject << "\tbakelites created." << endreq;

        return StatusCode::SUCCESS;

} // ZddBakelite

//------------------ZddBoxCover---------------
StatusCode ZddGeoMgr::CreateBoxCover()
{
        MsgStream log(msgSvc, "ZddGeoMgr");

        //-------------------------- ideal geometry----------------------
        ofstream fOrigin("ZddBoxCoverOrigin.dat", ios::out);
        ofstream fPanel("ZddBoxCoverPanel.dat", ios::out);
        ofstream fPos("ZddBoxCoverPanelPos.dat", ios::out);

        if( fOrigin.bad() || fPanel.bad() || fPos.bad() )
        {
                log << MSG::INFO << "BoxCover: create ouput file error!" << endl;
                return StatusCode::FAILURE;
        }
        fOrigin << "part\tsegment\tlayer\tU/D\tW\tH\tL\tLoc_x\tLoc_y\tLoc_z" << endl;
        fPanel  << "part\tsegment\tlayer\tU/D\tpanel\tWu\tWd\tH\tL\tLoc_x\tLoc_y\tLoc_z" << endl;
        fPos    << "part\tsegment\tlayer\tU/D\tpanel\tLBes_x\tLBes_y\tLBes_z\tOBes_x\tOBes_y\tOBes_z\tLoc_x\tLoc_y\tLoc_z" << endl;

        int totalObject = 0;

        for( int i=0; i<PART_MAX; i++ )
        {
                if( i == BRID )
                {
                        for( int j=0; j<B_SEG_NUM; j++ )
                        {
                                // set panel number
                                int idMin, idMax;
                                if( j != B_TOP ) { idMin = 0; idMax = B_GP_NUM; }
                                else             { idMin = -1; idMax = 3;       }
				
                                for( int k=0; k<B_LAY_NUM; k++ )
                                {
                                        for( int m=0; m<SL_NUM; m++ )
                                        {	
						for( int n = idMin; n<idMax; n++ )
						{
                                                	ZddBoxCover *aZddBoxCover = new ZddBoxCover( i, j, k, m, n );
						 	if( j == B_TOP || n != -1 ) // barrel top segment panels
                                                 	{
                                                  	  fPanel << i << "\t" << j << "\t" << k << "\t"
                                                        	<< m << "\t" << n << "\t"
	                                                        << aZddBoxCover->GetW() <<"\t"
                                                                << aZddBoxCover->GetW() <<"\t"
        	                                                << aZddBoxCover->GetH() <<"\t"
                	                                        << aZddBoxCover->GetL() <<"\t"
                        	                                << aZddBoxCover->GetObjOrgInLoc(1)   <<"\t"
                                	                        << aZddBoxCover->GetObjOrgInLoc(2)   <<"\t"
                                        	                << aZddBoxCover->GetObjOrgInLoc(3)   <<"\t"
                                                	        << endl;
							  	// delete aZddBoxCover;
							 }

							 if( j != B_TOP || n == -1 ) // box cover
							 {
                	                                  fOrigin << i << "\t" << j << "\t" << k << "\t" 
								<< m << "\t"  
                                	                        << aZddBoxCover->GetW() <<"\t"
                                        	                << aZddBoxCover->GetH() <<"\t"
                                                	        << aZddBoxCover->GetL() <<"\t"
	                                                        << aZddBoxCover->GetObjOrgInLoc(1)   <<"\t"
        	                                                << aZddBoxCover->GetObjOrgInLoc(2)   <<"\t"
                	                                        << aZddBoxCover->GetObjOrgInLoc(3)   <<"\t"
                        	                                << endl;
	
        	                                         	totalObject++;
								// delete aZddBoxCover;
							 }

                                                  	fPos << i << "\t" << j << "\t" << k << "\t" 
								<< m << "\t" << n << "\t"
	                                                        << aZddBoxCover->GetLocOrgInBes(1)   <<"\t"
        	                                                << aZddBoxCover->GetLocOrgInBes(2)   <<"\t"
                	                                        << aZddBoxCover->GetLocOrgInBes(3)   <<"\t"
                        	                                << aZddBoxCover->GetObjOrgInBes(1)   <<"\t"
                                	                        << aZddBoxCover->GetObjOrgInBes(2)   <<"\t"
                                        	                << aZddBoxCover->GetObjOrgInBes(3)   <<"\t"
                                                	        << aZddBoxCover->GetObjOrgInLoc(1)   <<"\t"
                                                        	<< aZddBoxCover->GetObjOrgInLoc(2)   <<"\t"
	                                                        << aZddBoxCover->GetObjOrgInLoc(3)   <<"\t"
        	                                                << endl;
								// delete aZddBoxCover;
						} // panel
                                        }// super layer
                                } // layer
                        } // segment
                } // barrel
                else
                {
                        for( int j=0; j<E_SEG_NUM; j++ )
                        {
                                for( int k=0; k<E_LAY_NUM; k++ )
                                {
                                        for( int m=0; m<SL_NUM; m++ )
                                        {
						for( int n=-1; n<E_PANEL_NUM; n++ )
						{
                                                	ZddBoxCover *aZddBoxCover = new ZddBoxCover( i, j, k, m, n );
                                                	if( n == -1 )
                                                	{
	                                                 fOrigin<< i << "\t" << j << "\t" << k << "\t" 
								<< m << "\t" 
        	                                                << aZddBoxCover->GetW() <<"\t"
                	                                        << aZddBoxCover->GetH() <<"\t"
                        	                                << aZddBoxCover->GetL() <<"\t"
                                	                        << aZddBoxCover->GetObjOrgInLoc(1)   <<"\t"
                                                	        << aZddBoxCover->GetObjOrgInLoc(2)   <<"\t"
                                        	                << aZddBoxCover->GetObjOrgInLoc(3)   <<"\t"
                                                        	<< endl;
	                                                 totalObject++;
							 // delete aZddBoxCover;
        	                                        }
                	                                else
                        	                        {
                                	                  fPanel<< i << "\t" << j << "\t" << k << "\t" 
								<< m << "\t" << n << "\t"
                                        	                << aZddBoxCover->GetWu() <<"\t"
                                                	        << aZddBoxCover->GetWd() <<"\t"
                                                        	<< aZddBoxCover->GetH()  <<"\t"
	                                                        << aZddBoxCover->GetL()  <<"\t"
        	                                                << aZddBoxCover->GetObjOrgInLoc(1)   <<"\t"
                	                                        << aZddBoxCover->GetObjOrgInLoc(2)   <<"\t"
                        	                                << aZddBoxCover->GetObjOrgInLoc(3)   <<"\t"
	                               	                        << endl;
							 // delete aZddBoxCover;
							}

	                                                  fPos  << i << "\t" << j << "\t" << k << "\t" 
								<< m << "\t" << n << "\t"
        	                                                << aZddBoxCover->GetLocOrgInBes(1)   <<"\t"
                	                                        << aZddBoxCover->GetLocOrgInBes(2)   <<"\t"
                        	                                << aZddBoxCover->GetLocOrgInBes(3)   <<"\t"
                                	                        << aZddBoxCover->GetObjOrgInBes(1)   <<"\t"
                                        	                << aZddBoxCover->GetObjOrgInBes(2)   <<"\t"
                                                	        << aZddBoxCover->GetObjOrgInBes(3)   <<"\t"
                                                        	<< aZddBoxCover->GetObjOrgInLoc(1)   <<"\t"
	                                                        << aZddBoxCover->GetObjOrgInLoc(2)   <<"\t"
        	                                                << aZddBoxCover->GetObjOrgInLoc(3)   <<"\t"
                	                                        << endl;
							  // delete aZddBoxCover;
						} // panel
                                        } // super layer
                                } // layer
                        } // segment
                } // endcap
        } // for

        fOrigin.close();
        fPanel.close();
        fPos.close();

        log << MSG::INFO << totalObject << "\tbox_covers created." << endreq;

        return StatusCode::SUCCESS;

} // ZddBoxCover



//=================================== Geometry entities getting methods====================================
ZddAbsorber* ZddGeoMgr::GetAbsorber( int part, int segment, int layer, int id )
{
	if( m_ZddAbsorber != NULL ) delete m_ZddAbsorber;

	return ( m_ZddAbsorber = new ZddAbsorber(part, segment, layer, id) );
}

ZddGap* ZddGeoMgr::GetGap( int part, int segment, int layer, int id )
{
        if( m_ZddGap != NULL ) delete m_ZddGap;

	return ( m_ZddGap = new ZddGap(part, segment, layer, id) );
}

ZddBox* ZddGeoMgr::GetBox( int part, int segment, int layer, int id )
{
        if( m_ZddBox != NULL ) delete m_ZddBox;

	return ( m_ZddBox = new ZddBox(part, segment, layer, id) );
}

ZddStripPlane* ZddGeoMgr::GetStripPlane( int part, int segment, int layer, int id )
{
        if( m_ZddStripPlane != NULL ) delete m_ZddStripPlane;

        return ( m_ZddStripPlane = new ZddStripPlane(part, segment, layer, id) );
}

ZddStrip* ZddGeoMgr::GetStrip( int part, int segment, int layer, int id )
{
        if( m_ZddStrip != NULL ) delete m_ZddStrip;

        return ( m_ZddStrip = new ZddStrip(part, segment, layer, id) );
}

ZddRpc* ZddGeoMgr::GetRpc( int part, int segment, int layer, int upDown, int id )
{
        if( m_ZddRpc != NULL ) delete m_ZddRpc;

	return ( m_ZddRpc = new ZddRpc(part, segment, layer, upDown, id) );
}

ZddGas* ZddGeoMgr::GetGas( int part, int segment, int layer, int upDown, int rpcId, int id )
{
        if( m_ZddGas != NULL ) delete m_ZddGas;

        return ( m_ZddGas = new ZddGas(part, segment, layer, upDown, rpcId, id) );
}

ZddBakelite* ZddGeoMgr::GetBakelite( int part, int segment, int layer, int upDown, int rpcId, int id )
{
        if( m_ZddBakelite != NULL ) delete m_ZddBakelite;

        return ( m_ZddBakelite = new ZddBakelite(part, segment, layer, upDown, rpcId, id) );
}

ZddBoxCover* ZddGeoMgr::GetBoxCover( int part, int segment, int layer, int upDown, int id )
{
        if( m_ZddBoxCover != NULL ) delete m_ZddBoxCover;

	return ( m_ZddBoxCover = new ZddBoxCover(part, segment, layer, upDown, id) );
}

// END
