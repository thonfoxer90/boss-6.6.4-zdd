#include "GaudiKernel/DeclareFactoryEntries.h"

#include "ZddGeoCreateAlg/ZddGeoCreateAlg.h"

DECLARE_ALGORITHM_FACTORY( ZddGeoCreateAlg )

DECLARE_FACTORY_ENTRIES( ZddGeoCreateAlg ) {
    DECLARE_ALGORITHM( ZddGeoCreateAlg );
}

