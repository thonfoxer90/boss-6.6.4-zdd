//------------------------------------------------------------------------------|
//      [File  ]:                       ZddGeoCreateAlg.cxx                     |
//      [Brief ]:       Muon detector geometry creating algrithom               |
//			Both designed and aligned geometry can be generated     |
//			Geometry entries included:				|
//				yoke, gas gap, RPC, strip,			|		
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       Jul 2, 2006                                             |
//      [Log   ]:       See ChangLog                                            |
//------------------------------------------------------------------------------|
#include<iostream>

#include "GaudiKernel/MsgStream.h"

#include "ZddGeoCreateAlg/ZddGeoCreateAlg.h"

using namespace std;

// -----------------------------------Declaration----------------------------------------
ZddGeoCreateAlg::ZddGeoCreateAlg(const std::string& name, ISvcLocator* pSvcLocator) :
  Algorithm(name, pSvcLocator), m_bAlignFlag(true)
{
  // Declare the properties
	declareProperty("CreateFlag", m_sCreateFlag);
  declareProperty("AlignFlag",  m_bAlignFlag);
  declareProperty("AlignFile",  m_sAlignFile = "ZddGeoAlignConst.root");
}

// ----------------------------------Initialize----------------------------------------- 
StatusCode ZddGeoCreateAlg::initialize()
{
  MsgStream log(msgSvc(), name());

  log << MSG::INFO << endreq << "In initialize() " << endreq;
	log << MSG::INFO << "Add alignment:\t"  << ((m_bAlignFlag)?"YES":"NO") << endreq << endreq;

  if( m_bAlignFlag )
	log << MSG::INFO << "Alignment file:\t" << m_sAlignFile << endreq;	

	m_pZddGeoMgr = new ZddGeoMgr( m_sCreateFlag, m_bAlignFlag, m_sAlignFile );
	
  return StatusCode::SUCCESS;
}

// ----------------------------------Execute--------------------------------------------
StatusCode ZddGeoCreateAlg::execute()
{
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << endreq << "In execute()" << endreq;

	return StatusCode::SUCCESS;
}
// ----------------------------------Finalize------------------------------------------
StatusCode ZddGeoCreateAlg::finalize()
{
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << endreq << "In finalize()" << endreq << endreq;
	
	m_pZddGeoMgr->CreateEntities();	
	m_pZddGeoMgr->CreateRootGeo();
	m_pZddGeoMgr->CreateOnlineStripGeo();

  return StatusCode::SUCCESS;
}

//--------------------------------------------END-----------------------------------------

