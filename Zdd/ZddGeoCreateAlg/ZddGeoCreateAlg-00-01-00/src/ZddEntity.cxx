//------------------------------------------------------------------------------|
//      [File  ]:                       ZddEntity.cxx                           |
//      [Brief ]:       ZDD geometry entity class                      		|
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       Mar 28, 2006                                            |
//------------------------------------------------------------------------------|

#include<iostream>
#include<string>
#include<cmath>

#include "ZddGeoCreateAlg/ZddEntity.h"

using namespace std;

// Constructor
ZddEntity::ZddEntity( int part, int segment, int layer )
{
        m_Part          = part;
        m_Segment       = segment;
        m_Layer         = layer;
        m_UpDown        = -1;           // no up/down distinction
	m_RpcId		= 0;
        m_ID            = 0;		// only 1 entity, default

	ZddEntity::Init();
}

ZddEntity::ZddEntity( int part, int segment, int layer, int id )
{
	m_Part 		= part;
	m_Segment 	= segment;
	m_Layer 	= layer;
	m_ID		= id;
	m_UpDown	= -1;		
	m_RpcId		= 0;

	Init();
}

ZddEntity::ZddEntity( int part, int segment, int layer, int upDown, int id )
{
        m_Part          = part;
        m_Segment       = segment;
        m_Layer         = layer;
        m_UpDown        = upDown;          
        m_ID            = id;
	m_RpcId		= id;

	ZddEntity::Init();
}

ZddEntity::ZddEntity( int part, int segment, int layer, int upDown, int rpcId, int id )
{
        m_Part          = part;
        m_Segment       = segment; 
        m_Layer         = layer;
        m_UpDown        = upDown;           
        m_RpcId         = rpcId;
        m_ID            = id;

        ZddEntity::Init();
}

ZddEntity::ZddEntity( const ZddEntity &other )
{
	m_Part          = other.m_Part;
        m_Segment       = other.m_Segment;
       	m_Layer         = other.m_Layer;
	m_UpDown	= other.m_UpDown;
	m_RpcId		= other.m_RpcId;
	m_ID		= other.m_ID;
	
	m_Theta		= other.m_Theta;
	m_Thin		= other.m_Thin;
        m_W             = other.m_W;
        m_H             = other.m_H;
        m_L             = other.m_L;
        m_Wu            = other.m_Wu;
        m_Wd            = other.m_Wd;
	m_Area		= other.m_Area;

	 for(int i=0; i<3; i++)
        {
		m_LocOrgInBes[i] = other.m_LocOrgInBes[i];
		m_ObjRotToMot[i] = other.m_ObjRotToMot[i];
                m_ObjOrgInBes[i] = other.m_ObjOrgInBes[i];
                m_ObjOrgInLoc[i] = other.m_ObjOrgInLoc[i];
        }
}

ZddEntity& ZddEntity::operator =( const ZddEntity &other )
{
	if( this == &other)
		return *this;
	
	m_Part          = other.m_Part;
        m_Segment       = other.m_Segment;
        m_Layer         = other.m_Layer;
        m_UpDown        = other.m_UpDown;
	m_RpcId		= other.m_RpcId;
        m_ID            = other.m_ID;

        m_Theta         = other.m_Theta;	
        m_Thin          = other.m_Thin;	
	m_W		= other.m_W;
	m_H		= other.m_H;
	m_L		= other.m_L;
	m_Wu		= other.m_Wu;
	m_Wd		= other.m_Wd;
	m_Area          = other.m_Area;

	  for(int i=0; i<3; i++)
        {
		m_LocOrgInBes[i] = other.m_LocOrgInBes[i];
                m_ObjRotToMot[i] = other.m_ObjRotToMot[i];
                m_ObjOrgInBes[i] = other.m_ObjOrgInBes[i];
                m_ObjOrgInLoc[i] = other.m_ObjOrgInLoc[i];
        }

	return *this;
}

// Destructor
ZddEntity::~ZddEntity()
{
	delete [] m_LocOrgInBes;
	delete [] m_ObjRotToMot;
	delete [] m_ObjOrgInBes;
	delete [] m_ObjOrgInLoc;
}

// Get methods
int ZddEntity::GetPart() 	{ return m_Part; 	}
int ZddEntity::GetSegment() 	{ return m_Segment; 	}
int ZddEntity::GetLayer() 	{ return m_Layer; 	}
int ZddEntity::GetUpDown() 	{ return m_UpDown; 	}
int ZddEntity::GetRpcId()	{ return m_RpcId;	}
int ZddEntity::GetID() 		{ return m_ID; 		}
double ZddEntity::GetTheta()	{ return m_Theta;	}
double ZddEntity::GetRin()      { return m_Rin;         }
double ZddEntity::GetRout()     { return m_Rout;        }
double ZddEntity::GetRc()       { return m_Rc;          }
double ZddEntity::GetThin() 	{ return m_Thin; 	} 
double ZddEntity::GetW() 	{ return m_W; 		}
double ZddEntity::GetH()	{ return m_H; 		}
double ZddEntity::GetL() 	{ return m_L; 		}
double ZddEntity::GetWu()	{ return m_Wu;		}
double ZddEntity::GetWd()	{ return m_Wd;		}
double ZddEntity::GetArea()	{ return m_Area;	}

double ZddEntity::GetLocOrgInBes( int i )
{
	if( i<1 || i>3 ) i=1;
        return m_LocOrgInBes[i-1];
}

double ZddEntity::GetObjRotToMot( int i )
{
	if( i<1 || i>3 ) i=1;
        return m_ObjRotToMot[i-1];
}

double ZddEntity::GetObjOrgInBes( int i )
{
        if( i<1 || i>3 ) i=1;
        return m_ObjOrgInBes[i-1];
}

double ZddEntity::GetObjOrgInLoc( int i )
{
        if( i<1 || i>3 ) i=1;
        return m_ObjOrgInLoc[i-1];
}

double* ZddEntity::GetLocOrgInBes() { return &m_LocOrgInBes[0]; }
double* ZddEntity::GetObjRotToMot() { return &m_ObjRotToMot[0]; }
double* ZddEntity::GetObjOrgInBes() { return &m_ObjOrgInBes[0]; }
double* ZddEntity::GetObjOrgInLoc() { return &m_ObjOrgInLoc[0]; }


// Set methods
void ZddEntity::Init()
{
	ZddEntity::SetTheta();
	ZddEntity::SetRin();
	ZddEntity::SetRout();
	ZddEntity::SetRc();
	ZddEntity::SetThin();
	ZddEntity::SetW();
	ZddEntity::SetWu();
	ZddEntity::SetWd();
	ZddEntity::SetH();
	ZddEntity::SetL();
	ZddEntity::SetArea();
	ZddEntity::SetLocOrgInBes();
	ZddEntity::SetObjRotToMot();
	ZddEntity::SetObjOrgInBes();
	ZddEntity::SetObjOrgInLoc();
}

void ZddEntity::SetTheta(){ m_Theta     = 0.;   }
void ZddEntity::SetRin()  { m_Rin       = 0.;   }
void ZddEntity::SetRout() { m_Rout      = 0.;   }
void ZddEntity::SetRc()   { m_Rc      	= 0.;   }
void ZddEntity::SetThin() { m_Thin 	= 0.; 	}
void ZddEntity::SetW()    { m_W		= 0.; 	}
void ZddEntity::SetWu()	  { m_Wu 	= 0.; 	}
void ZddEntity::SetWd()	  { m_Wd 	= 0.; 	}
void ZddEntity::SetH()    { m_H 	= 0.; 	}
void ZddEntity::SetL()	  { m_L 	= 0.; 	}
void ZddEntity::SetArea() { m_Area	= 0.;	}

void ZddEntity::SetLocOrgInBes() { for( int i=0; i<3; i++) m_LocOrgInBes[i] = 0.; }
void ZddEntity::SetObjRotToMot() { for( int i=0; i<3; i++) m_ObjRotToMot[i] = 0.; }
void ZddEntity::SetObjOrgInBes() { for( int i=0; i<3; i++) m_ObjOrgInBes[i] = 0.; }
void ZddEntity::SetObjOrgInLoc() { for( int i=0; i<3; i++) m_ObjOrgInLoc[i] = 0.; }

void ZddEntity::SetAlignment( double dx, double dy, double dz ) { ; }

// Coordinate transform function, from global(BES) to local 
// Z coordinate is parellel, phi is the inclination angle of the BES coordinate x direction and the Loc one
void ZddEntity::TransBesToLoc( double LocCoord[], double BesCoord[], double LocOrgInBes[], double Rot_z )
{
       LocCoord[0] = (BesCoord[0] - LocOrgInBes[0])*cos(Rot_z) + (BesCoord[1] - LocOrgInBes[1])*sin(Rot_z);
       LocCoord[1] = -(BesCoord[0] - LocOrgInBes[0])*sin(Rot_z) + (BesCoord[1] - LocOrgInBes[1])*cos(Rot_z);
       LocCoord[2] = BesCoord[2] - LocOrgInBes[2];
}

// Coordinate transform function, from local to global(BES)
// Z coordinate is parellel, phi is the inclination angle of the BES coordinate x direction and the Loc one 
void ZddEntity::TransLocToBes( double BesCoord[], double LocCoord[], double LocOrgInBes[], double Rot_z )
{
	BesCoord[0] = LocCoord[0]*cos(Rot_z) - LocCoord[1]*sin(Rot_z) + LocOrgInBes[0];
	BesCoord[1] = LocCoord[0]*sin(Rot_z) + LocCoord[1]*cos(Rot_z) + LocOrgInBes[1];
	BesCoord[2] = LocCoord[2] + LocOrgInBes[2];
}


// END


