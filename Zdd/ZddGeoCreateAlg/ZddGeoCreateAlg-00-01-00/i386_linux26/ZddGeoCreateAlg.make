#-- start of make_header -----------------

#====================================
#  Library ZddGeoCreateAlg
#
#   Generated Thu Jul  7 18:09:04 2011  by mwerner
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ZddGeoCreateAlg_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ZddGeoCreateAlg_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ZddGeoCreateAlg

ZddGeoCreateAlg_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddGeoCreateAlg = /tmp/CMT_$(ZddGeoCreateAlg_tag)_ZddGeoCreateAlg.make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddGeoCreateAlg = $(ZddGeoCreateAlg_tag)_ZddGeoCreateAlg.make
endif

else

tags      = $(tag),$(CMTEXTRATAGS)

ZddGeoCreateAlg_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddGeoCreateAlg = /tmp/CMT_$(ZddGeoCreateAlg_tag).make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddGeoCreateAlg = $(ZddGeoCreateAlg_tag).make
endif

endif

-include $(cmt_local_tagfile_ZddGeoCreateAlg)

ifdef cmt_ZddGeoCreateAlg_has_target_tag

ifdef READONLY
cmt_final_setup_ZddGeoCreateAlg = /tmp/CMT_ZddGeoCreateAlg_ZddGeoCreateAlgsetup.make
cmt_local_ZddGeoCreateAlg_makefile = /tmp/CMT_ZddGeoCreateAlg$(cmt_lock_pid).make
else
cmt_final_setup_ZddGeoCreateAlg = $(bin)ZddGeoCreateAlg_ZddGeoCreateAlgsetup.make
cmt_local_ZddGeoCreateAlg_makefile = $(bin)ZddGeoCreateAlg.make
endif

else

ifdef READONLY
cmt_final_setup_ZddGeoCreateAlg = /tmp/CMT_ZddGeoCreateAlgsetup.make
cmt_local_ZddGeoCreateAlg_makefile = /tmp/CMT_ZddGeoCreateAlg$(cmt_lock_pid).make
else
cmt_final_setup_ZddGeoCreateAlg = $(bin)ZddGeoCreateAlgsetup.make
cmt_local_ZddGeoCreateAlg_makefile = $(bin)ZddGeoCreateAlg.make
endif

endif

ifdef READONLY
cmt_final_setup = /tmp/CMT_ZddGeoCreateAlgsetup.make
else
cmt_final_setup = $(bin)ZddGeoCreateAlgsetup.make
endif

ZddGeoCreateAlg ::


ifdef READONLY
ZddGeoCreateAlg ::
	@echo tags=$(tags)
	@echo cmt_local_tagfile=$(cmt_local_tagfile)
endif


dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	@echo 'ZddGeoCreateAlg'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ZddGeoCreateAlg/
ZddGeoCreateAlg::
	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
	@echo "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

#-- end of make_header ------------------

#-- start of libary_header ---------------

ZddGeoCreateAlglibname   = $(bin)$(library_prefix)ZddGeoCreateAlg$(library_suffix)
ZddGeoCreateAlglib       = $(ZddGeoCreateAlglibname).a
ZddGeoCreateAlgstamp     = $(bin)ZddGeoCreateAlg.stamp
ZddGeoCreateAlgshstamp   = $(bin)ZddGeoCreateAlg.shstamp

ZddGeoCreateAlg :: dirs  ZddGeoCreateAlgLIB
	@/bin/echo "------> ZddGeoCreateAlg ok"

#-- end of libary_header ----------------

ZddGeoCreateAlgLIB :: $(ZddGeoCreateAlglib) $(ZddGeoCreateAlgshstamp)
	@/bin/echo "------> ZddGeoCreateAlg : library ok"

$(ZddGeoCreateAlglib) :: $(bin)ZddBakelite.o $(bin)ZddBox.o $(bin)ZddBoxCover.o $(bin)ZddEntity.o $(bin)ZddGap.o $(bin)ZddGas.o $(bin)ZddGeoMgr.o $(bin)ZddIdTransform.o $(bin)ZddRpc.o $(bin)ZddStrip.o $(bin)ZddStripPlane.o $(bin)ZddAbsorber.o $(bin)ZddGeoCreateAlg.o $(bin)ZddGeoCreateAlg_load.o $(bin)ZddGeoCreateAlg_entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(ZddGeoCreateAlglib) $?
	$(lib_silent) $(ranlib) $(ZddGeoCreateAlglib)
	$(lib_silent) cat /dev/null >$(ZddGeoCreateAlgstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(ZddGeoCreateAlglibname).$(shlibsuffix) :: $(ZddGeoCreateAlglib) $(ZddGeoCreateAlgstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" ZddGeoCreateAlg $(ZddGeoCreateAlg_shlibflags)

$(ZddGeoCreateAlgshstamp) :: $(ZddGeoCreateAlglibname).$(shlibsuffix)
	@if test -f $(ZddGeoCreateAlglibname).$(shlibsuffix) ; then cat /dev/null >$(ZddGeoCreateAlgshstamp) ; fi

ZddGeoCreateAlgclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)ZddBakelite.o $(bin)ZddBox.o $(bin)ZddBoxCover.o $(bin)ZddEntity.o $(bin)ZddGap.o $(bin)ZddGas.o $(bin)ZddGeoMgr.o $(bin)ZddIdTransform.o $(bin)ZddRpc.o $(bin)ZddStrip.o $(bin)ZddStripPlane.o $(bin)ZddAbsorber.o $(bin)ZddGeoCreateAlg.o $(bin)ZddGeoCreateAlg_load.o $(bin)ZddGeoCreateAlg_entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
ZddGeoCreateAlginstallname = $(library_prefix)ZddGeoCreateAlg$(library_suffix).$(shlibsuffix)

ZddGeoCreateAlg :: ZddGeoCreateAlginstall

install :: ZddGeoCreateAlginstall

ZddGeoCreateAlginstall :: $(install_dir)/$(ZddGeoCreateAlginstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(ZddGeoCreateAlginstallname) :: $(bin)$(ZddGeoCreateAlginstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(ZddGeoCreateAlginstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(ZddGeoCreateAlginstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeoCreateAlginstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeoCreateAlginstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(ZddGeoCreateAlginstallname) $(install_dir)/$(ZddGeoCreateAlginstallname); \
	      echo `pwd`/$(ZddGeoCreateAlginstallname) >$(install_dir)/$(ZddGeoCreateAlginstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(ZddGeoCreateAlginstallname), no installation directory specified"; \
	  fi; \
	fi

ZddGeoCreateAlgclean :: ZddGeoCreateAlguninstall

uninstall :: ZddGeoCreateAlguninstall

ZddGeoCreateAlguninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(ZddGeoCreateAlginstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeoCreateAlginstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddGeoCreateAlginstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(ZddGeoCreateAlginstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------

$(bin)ZddGeoCreateAlg_dependencies.make :: dirs

ifndef QUICK
$(bin)ZddGeoCreateAlg_dependencies.make :: $(src)*.cxx $(src)components/*.cxx  requirements $(use_requirements) $(cmt_final_setup_ZddGeoCreateAlg)
	@echo "------> (ZddGeoCreateAlg.make) Rebuilding $@"; \
	  args=`echo $? | sed -e "s#requirements.*##"`; \
	  $(build_dependencies) ZddGeoCreateAlg -all_sources $${args}
endif

#$(ZddGeoCreateAlg_dependencies)

-include $(bin)ZddGeoCreateAlg_dependencies.make

#-- end of dependency -------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddBakelite.o : $(ZddBakelite_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddBakelite.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddBakelite_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddBakelite_cppflags) $(ZddBakelite_cxx_cppflags)  $(src)ZddBakelite.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddBox.o : $(ZddBox_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddBox.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddBox_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddBox_cppflags) $(ZddBox_cxx_cppflags)  $(src)ZddBox.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddBoxCover.o : $(ZddBoxCover_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddBoxCover.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddBoxCover_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddBoxCover_cppflags) $(ZddBoxCover_cxx_cppflags)  $(src)ZddBoxCover.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddEntity.o : $(ZddEntity_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddEntity.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddEntity_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddEntity_cppflags) $(ZddEntity_cxx_cppflags)  $(src)ZddEntity.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGap.o : $(ZddGap_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGap.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddGap_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddGap_cppflags) $(ZddGap_cxx_cppflags)  $(src)ZddGap.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGas.o : $(ZddGas_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGas.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddGas_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddGas_cppflags) $(ZddGas_cxx_cppflags)  $(src)ZddGas.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGeoMgr.o : $(ZddGeoMgr_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGeoMgr.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddGeoMgr_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddGeoMgr_cppflags) $(ZddGeoMgr_cxx_cppflags)  $(src)ZddGeoMgr.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddIdTransform.o : $(ZddIdTransform_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddIdTransform.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddIdTransform_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddIdTransform_cppflags) $(ZddIdTransform_cxx_cppflags)  $(src)ZddIdTransform.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddRpc.o : $(ZddRpc_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddRpc.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddRpc_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddRpc_cppflags) $(ZddRpc_cxx_cppflags)  $(src)ZddRpc.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddStrip.o : $(ZddStrip_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddStrip.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddStrip_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddStrip_cppflags) $(ZddStrip_cxx_cppflags)  $(src)ZddStrip.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddStripPlane.o : $(ZddStripPlane_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddStripPlane.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddStripPlane_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddStripPlane_cppflags) $(ZddStripPlane_cxx_cppflags)  $(src)ZddStripPlane.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddAbsorber.o : $(ZddAbsorber_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddAbsorber.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddAbsorber_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddAbsorber_cppflags) $(ZddAbsorber_cxx_cppflags)  $(src)ZddAbsorber.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGeoCreateAlg.o : $(ZddGeoCreateAlg_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGeoCreateAlg.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddGeoCreateAlg_cppflags) $(ZddGeoCreateAlg_cxx_cppflags)  $(src)ZddGeoCreateAlg.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGeoCreateAlg_load.o : $(ZddGeoCreateAlg_load_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGeoCreateAlg_load.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddGeoCreateAlg_load_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddGeoCreateAlg_load_cppflags) $(ZddGeoCreateAlg_load_cxx_cppflags) -I../src/components $(src)components/ZddGeoCreateAlg_load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)ZddGeoCreateAlg_entries.o : $(ZddGeoCreateAlg_entries_cxx_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)ZddGeoCreateAlg_entries.o $(use_pp_cppflags) $(ZddGeoCreateAlg_pp_cppflags) $(lib_ZddGeoCreateAlg_pp_cppflags) $(ZddGeoCreateAlg_entries_pp_cppflags) $(use_cppflags) $(ZddGeoCreateAlg_cppflags) $(lib_ZddGeoCreateAlg_cppflags) $(ZddGeoCreateAlg_entries_cppflags) $(ZddGeoCreateAlg_entries_cxx_cppflags) -I../src/components $(src)components/ZddGeoCreateAlg_entries.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ZddGeoCreateAlgclean
	@cd .

ifndef PEDANTIC
.DEFAULT::
	@echo "WARNING >> You should provide a target for $@"
else
.DEFAULT::
	@echo "PEDANTIC MODE >> You should provide a target for $@"
	@if test `expr index $@ '.'` != 0 ; \
	then  echo "PEDANTIC MODE >> This target seems to be a missing file, please check"; exit -1 ;\
	else echo "PEDANTIC MODE >> Just ignore it ; as it seems to be just a fake target due to some pattern" ; exit 0; fi; 		
endif

ZddGeoCreateAlgclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(binobj)$(library_prefix)ZddGeoCreateAlg$(library_suffix).a $(binobj)$(library_prefix)ZddGeoCreateAlg$(library_suffix).s? $(binobj)ZddGeoCreateAlg.stamp $(binobj)ZddGeoCreateAlg.shstamp
#-- end of cleanup_library ---------------

