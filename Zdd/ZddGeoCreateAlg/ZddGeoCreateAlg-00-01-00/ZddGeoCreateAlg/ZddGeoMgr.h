//------------------------------------------------------------------------------|
//      [File  ]:                       ZddGeoMgr.h      	                      |
//      [Brief ]:       Header file of ZDD geometry manager class   		        |
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       Mar 28, 2006                                            |
//------------------------------------------------------------------------------|

#ifndef ZDD_GEO_MGR_ALG_H
#define ZDD_GEO_MGR_ALG_H

#include<iostream>
#include<string>

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/IMessageSvc.h"

#include "ZddGeoCreateAlg/ZddGeoConst.h"
#include "ZddGeoCreateAlg/ZddEntity.h"
#include "ZddGeoCreateAlg/ZddAbsorber.h"
#include "ZddGeoCreateAlg/ZddGap.h"
#include "ZddGeoCreateAlg/ZddBox.h"
#include "ZddGeoCreateAlg/ZddStripPlane.h"
#include "ZddGeoCreateAlg/ZddStrip.h"
#include "ZddGeoCreateAlg/ZddRpc.h"
#include "ZddGeoCreateAlg/ZddBoxCover.h"
#include "ZddGeoCreateAlg/ZddBakelite.h"
#include "ZddGeoCreateAlg/ZddGas.h"
#include "ZddGeoCreateAlg/ZddIdTransform.h"

/*
#include "TH1F.h"
#include "TGeometry.h"
#include "TGeoManager.h"
#include "TGeoVolume.h"
#include "TGeoMaterial.h"
#include "TGeoMedium.h"
#include "TGeoBBox.h"
#include "TCanvas.h"
*/

using namespace std;

const unsigned int ENTITY_NUM = 9;

class ZddGeoMgr 
{
	  public:
    	ZddGeoMgr( const std::string createFlag, bool alignFlag, const std::string alignFile );
      ~ZddGeoMgr();
			
		StatusCode CreateEntities();	
		StatusCode CreateRootGeo();
		StatusCode CreateOnlineStripGeo();
                
	  ZddAbsorber	*GetAbsorber( 	int part, int segment, int layer, int id );
		ZddGap		*GetGap( 	int part, int segment, int layer, int id );
		ZddBox		*GetBox( 	int part, int segment, int layer, int id );
		ZddStripPlane   *GetStripPlane( int part, int segment, int layer, int id );
	  ZddStrip	*GetStrip( 	int part, int segment, int layer, int id );
    ZddRpc		*GetRpc( 	int part, int segment, int layer, int upDown, int id );
		ZddGas		*GetGas( 	int part, int segment, int layer, int upDown, int rpcId, int id );
		ZddBakelite	*GetBakelite( 	int part, int segment, int layer, int upDown, int rpcId, int id );
		ZddBoxCover	*GetBoxCover( 	int part, int segment, int layer, int upDown, int id );

		IMessageSvc	*msgSvc;
	
		protected:

	  StatusCode CreateAbsorber();
    StatusCode CreateGap();
		StatusCode CreateBox();
		StatusCode CreateStripPlane();
	  StatusCode CreateStrip();
    StatusCode CreateRpc();
		StatusCode CreateGas();
		StatusCode CreateBakelite();
		StatusCode CreateBoxCover();

		StatusCode InitOffset();
		bool 	   CheckBoxOffset( int part, int segment, int layer, int axis, double offset);
    bool       CheckStripPlaneOffset( int part, int segment, int layer, int axis, double offset);

    private:

    bool        	m_AlignFlag;
		std::string	m_CreateFlag;
		std::string	m_AlignFile;
		double	 	m_BoxOffset[PART_MAX][B_SEG_NUM][B_LAY_NUM][3];
		double    m_StripPlaneOffset[PART_MAX][B_SEG_NUM][B_LAY_NUM][3];
  
    ZddIdTransform* m_IdTr;			

		ZddAbsorber	*m_ZddAbsorber;
		ZddGap		*m_ZddGap;
		ZddBox		*m_ZddBox;
		ZddStripPlane	*m_ZddStripPlane;
		ZddStrip	*m_ZddStrip;
		ZddRpc		*m_ZddRpc;
		ZddBoxCover	*m_ZddBoxCover;
		ZddBakelite	*m_ZddBakelite;
		ZddGas		*m_ZddGas;

		//TGeoManager *geoMgr;

};

#endif
