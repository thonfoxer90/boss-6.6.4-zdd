//------------------------------------------------------------------------------|
//      [File  ]:                       ZddPadChain.h  	                        |
//      [Brief ]:       Head file of class ZddPadChain for ZDD calibration      |
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       May 25, 2006                                            |
//      [Log   ]:       See ChangLog                                            |
//------------------------------------------------------------------------------|

#ifndef ZDD_PAD_CHAIN_H
#define ZDD_PAD_CHAIN_H

#include<iostream>

class ZddPadChain {

	public:
		ZddPadChain( int padNumber );
		ZddPadChain( int padNumber, double padWidth );
		ZddPadChain( int padNumber, double padWidth, double aveEff );
		ZddPadChain( int part, int segment, int layer, int strip, int padNumber );
		
		ZddPadChain& operator=( const ZddPadChain& );
		
		double	GetEff( int padID );
		double  GetAvaEff();
		int	GetSize();
		int 	GetHit();
		int	GetHit( int padID );
		int	GetTrack();
		int 	GetTrack( int padID );
		bool 	IsEmpty();
		
		
		void	AddHit( int padID );
		void 	AddTrack( int padID );
		void	SetEff();
		void	SetEff( int padID, double eff );
		void 	SetSize( int size );
		void 	SetPadWidth( double width );
		void	SetDeathFlag( int padID );
		void	SetFiredFlag( int padID );
			
	private:
	
   static const int	m_MaxSize  = 500;
   static const int 	m_MaxWidth = 100;
   static const int 	m_DeathFlag = -1;
		int 	m_Size;
		int	m_PadWidth;
		
	vector<	int >	m_Hit;
	vector<	int >	m_Track;
	vector<	double> m_Eff;		
}

#endif
		
