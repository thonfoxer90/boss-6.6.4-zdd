//------------------------------------------------------------------------------|
//      [File  ]:                       ZddBox.h                                |
//      [Brief ]:       Head file of ZDD geometry gap of Box class 		|
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       May 22, 2005                                            |
//------------------------------------------------------------------------------|

#ifndef ZDD_BOX_H
#define ZDD_BOX_H

#include<iostream>

#include "ZddGeoCreateAlg/ZddEntity.h"
#include "ZddGeoCreateAlg/ZddStripPlane.h"
#include "ZddGeoCreateAlg/ZddRpc.h"
#include "ZddGeoCreateAlg/ZddBoxCover.h"

using namespace std;

class ZddBox : public ZddEntity
{
	public:
		ZddBox( int part, int segment, int layer, int id );
		ZddBox( const ZddBox &other );
		ZddBox& operator =( const ZddBox &other );
		~ZddBox();
		
	virtual	void	SetAlignment( double dx, double dy, double dz );

		ZddStripPlane* 	GetStripPlane();
                ZddRpc* 	GetRpc( int upDown, int id );
                ZddBoxCover* 	GetBoxCover( int upDown, int id );

	protected:			
        	
	virtual	void    Init();

        virtual	void    SetTheta();
        virtual	void    SetRin();
        virtual	void    SetRout();
        virtual	void    SetRc();

        virtual void    SetThin();
        virtual void    SetW();
        virtual void    SetH();
        virtual void    SetL();
        virtual void    SetWu();
        virtual void    SetWd();
	virtual void	SetArea();

        virtual void    SetLocOrgInBes();
        virtual void    SetObjRotToMot();
        virtual void    SetObjOrgInBes();
        virtual void    SetObjOrgInLoc();
	
	private:

		ZddStripPlane*  m_ZddStripPlane; 
		ZddRpc*		m_ZddRpc;
		ZddBoxCover*	m_ZddBoxCover;

};

#endif

