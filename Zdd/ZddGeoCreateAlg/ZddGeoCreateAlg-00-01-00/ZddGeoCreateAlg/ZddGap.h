//------------------------------------------------------------------------------|
//      [File  ]:                       ZddGap.h                                |
//      [Brief ]:       Head file of ZDD geometry gap of Gap class 		|
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       May 22, 2005                                            |
//------------------------------------------------------------------------------|

#ifndef ZDD_GAP_H
#define ZDD_GAP_H

#include<iostream>

#include "ZddGeoCreateAlg/ZddEntity.h"
#include "ZddGeoCreateAlg/ZddBox.h"

using namespace std;

class ZddGap : public ZddEntity
{
	public:
		ZddGap( int part, int segment, int layer, int id );
		ZddGap( const ZddGap &other );
		ZddGap& operator =( const ZddGap &other );
		~ZddGap();
		
		ZddBox* GetBox();

	protected:	
		
        virtual void    Init();

        virtual	void    SetTheta();
        virtual	void    SetRin();
        virtual	void    SetRout();
        virtual	void    SetRc();

        virtual void    SetThin();
        virtual void    SetW();
        virtual void    SetH();
        virtual void    SetL();
        virtual void    SetWu();
        virtual void    SetWd();

        virtual void    SetLocOrgInBes();
        virtual void    SetObjRotToMot();
        virtual void    SetObjOrgInBes();
        virtual void    SetObjOrgInLoc();
	
	private:

		ZddBox* m_ZddBox; 
};

#endif

