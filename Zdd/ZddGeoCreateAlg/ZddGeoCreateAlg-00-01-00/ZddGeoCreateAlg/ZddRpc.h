//------------------------------------------------------------------------------|
//      [File  ]:                       ZddRpc.h                         	|
//      [Brief ]:       Head file of ZDD geometry  Rpc class 			|
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       May 22, 2005                                            |
//------------------------------------------------------------------------------|

#ifndef ZDD_RPC_H
#define ZDD_RPC_H

#include<iostream>

#include "ZddGeoCreateAlg/ZddEntity.h"
#include "ZddGeoCreateAlg/ZddRpc.h"
#include "ZddGeoCreateAlg/ZddGas.h"
#include "ZddGeoCreateAlg/ZddBakelite.h"

using namespace std;

class ZddRpc : public ZddEntity
{
	public:
		ZddRpc( int part, int segment, int layer, int upDown, int id );
		ZddRpc( const ZddRpc &other );
		ZddRpc& operator =( const ZddRpc &other );
		virtual ~ZddRpc();
		
	      double  	GetPhi();
	     ZddGas* 	GetGas();
	ZddBakelite* 	GetBakelite( int id );

//	virtual void	SetAlignment( double dx, double dy, double dz );

	protected:			
        virtual void    Init();

	        void	SetPhi();
        virtual	void    SetTheta();
        virtual	void    SetRc();
        virtual	void    SetRin();
        virtual	void    SetRout();

        virtual void    SetThin();
        virtual void    SetW();
        virtual void    SetH();
        virtual void    SetL();
        virtual void    SetWu();
        virtual void    SetWd();

		int	SetEndcapRpcInBes();
		int	SetBarrelRpcInLoc();

        virtual void    SetLocOrgInBes();
        virtual void    SetObjOrgInBes();
        virtual void    SetObjOrgInLoc();
	
	private:
	   
	       double 	m_Phi;		// inclination angle of BES x axis and Loc x axis
	      ZddGas*  	m_ZddGas;
         ZddBakelite*  	m_ZddBakelite; 

};

#endif

