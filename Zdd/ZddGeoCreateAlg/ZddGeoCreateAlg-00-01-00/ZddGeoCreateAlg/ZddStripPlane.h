//------------------------------------------------------------------------------|
//      [File  ]:                       ZddStripPlane.h                         |
//      [Brief ]:       Head file of ZDD geometry of StripPlane class 		|
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       May 22, 2005                                            |
//------------------------------------------------------------------------------|

#ifndef ZDD_STRIP_PLANE_H
#define ZDD_STRIP_PLANE_H

#include<iostream>

#include "ZddGeoCreateAlg/ZddEntity.h"
#include "ZddGeoCreateAlg/ZddStripPlane.h"
#include "ZddGeoCreateAlg/ZddStrip.h"

using namespace std;

class ZddStripPlane : public ZddEntity
{
	public:
		ZddStripPlane( int part, int segment, int layer, int id );
		ZddStripPlane( const ZddStripPlane &other );
		ZddStripPlane& operator =( const ZddStripPlane &other );
		virtual ~ZddStripPlane();
		
	virtual void	SetAlignment( double dx, double dy, double dz );

	   ZddStrip* 	GetStrip( int id );

	protected:			
        virtual void    Init();

	virtual	void    SetTheta();
        virtual	void    SetRc();
        virtual	void    SetRin();
        virtual	void    SetRout();

        virtual void    SetThin();
        virtual void    SetW();
        virtual void    SetH();
        virtual void    SetL();
        virtual void    SetWu();
        virtual void    SetWd();

        virtual void    SetLocOrgInBes();
        virtual void    SetObjOrgInBes();
        virtual void    SetObjOrgInLoc();
	
	private:

		ZddStrip*  m_ZddStrip; 

};

#endif

