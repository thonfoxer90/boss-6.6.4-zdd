//------------------------------------------------------------------------------|
//      [File  ]:                       ZddGeoCreateAlg.h                       |
//      [Brief ]:       Header file of ZDD geometry creating algorithem class   |
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       Mar 28, 2006                                            |
//------------------------------------------------------------------------------|

#ifndef ZDD_GEO_CREATE_ALG_H
#define ZDD_GEO_CREATE_ALG_H

#include <iostream>
#include <string>

#include "GaudiKernel/Algorithm.h"

#include "ZddGeoCreateAlg/ZddGeoMgr.h"

class ZddGeoCreateAlg : public Algorithm
{
        public:
                ZddGeoCreateAlg(const std::string& name, ISvcLocator* pSvcLocator);
                ~ZddGeoCreateAlg(){};

                StatusCode initialize();
                StatusCode execute();
                StatusCode finalize();

        private:

                       bool	m_bAlignFlag;
	    	std::string 	m_sCreateFlag;
		std::string	m_sAlignFile;

		ZddGeoMgr	*m_pZddGeoMgr;
};

#endif
