//------------------------------------------------------------------------------|
//      [File  ]:                       ZddBoxCover.h                           |
//      [Brief ]:       Head file of ZDD geometry BoxCover class 		|
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       May 22, 2005                                            |
//------------------------------------------------------------------------------|

#ifndef ZDD_BOXCOVER_H
#define ZDD_BOXCOVER_H

#include<iostream>

#include "ZddGeoCreateAlg/ZddEntity.h"

using namespace std;

class ZddBoxCover : public ZddEntity
{
	public:
		ZddBoxCover( int part, int segment, int layer, int upDown, int id );
		ZddBoxCover( const ZddBoxCover &other );
		ZddBoxCover& operator =( const ZddBoxCover &other );
		~ZddBoxCover();
		
//	virtual	void	SetAlignment( double dx, double dy, double dz );

	protected:			
        	
	virtual	void    Init();

        virtual	void    SetTheta();
        virtual	void    SetRin();
        virtual	void    SetRout();
        virtual	void    SetRc();

        virtual void    SetThin();
        virtual void    SetW();
        virtual void    SetH();
        virtual void    SetL();
        virtual void    SetWu();
        virtual void    SetWd();

        virtual void    SetLocOrgInBes();
        virtual void    SetObjOrgInBes();
        virtual void    SetObjOrgInLoc();
};

#endif

