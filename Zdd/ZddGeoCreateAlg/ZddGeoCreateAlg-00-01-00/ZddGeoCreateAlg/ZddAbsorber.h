//------------------------------------------------------------------------------|
//      [File  ]:                       ZddAbsorber.h                           |
//      [Brief ]:       Head file of ZDD geometry absorber class 		|
//      [Author]:       Xie Yuguang, <ygxie@mail.ihep.ac.cn>                    |
//      [Date  ]:       May 22, 2005                                            |
//------------------------------------------------------------------------------|

#ifndef ZDD_ABSORBER_H
#define ZDD_ABSORBER_H

#include<iostream>

#include "ZddGeoCreateAlg/ZddEntity.h"

using namespace std;

class ZddAbsorber : public ZddEntity
{
	public:
		ZddAbsorber( int part, int segment, int layer, int id );
		ZddAbsorber( const ZddAbsorber &other );
		ZddAbsorber &operator =( const ZddAbsorber &other );
		virtual ~ZddAbsorber();
	
	protected:
	virtual void	Init();

	virtual	void 	SetTheta();
        virtual	void    SetRin();
        virtual	void    SetRout();
        virtual	void    SetRc();

	virtual void 	SetThin();	
	virtual void	SetW();
        virtual void    SetH();
	virtual	void    SetL();
        virtual void    SetWu();
        virtual void    SetWd();
	virtual void    SetLocOrgInBes();
        virtual void    SetObjRotToMot();
        virtual void    SetObjOrgInBes();
        virtual void    SetObjOrgInLoc();
			
};

#endif

