//DENG Zi-yan 2006-02-28
// Job options file for Geant4 Simulations

ApplicationMgr.DLLs += { "MagneticField","RootHistCnv"};
ApplicationMgr.TopAlg += { "MagFieldReader"};

#include "$MAGNETICFIELDROOT/share/MagneticField.txt"
#include "$REALIZATIONSVCROOT/share/jobOptions_Realization.txt"
#include "$BESSIMROOT/share/Bes_Gen.txt"
//#include "$BESSIMROOT/share/PartPropSvc.txt"

ApplicationMgr.DLLs   += { "BesTimerSvc"};
ApplicationMgr.ExtSvc += { "BesTimerSvc"};

//magnetic field
ApplicationMgr.HistogramPersistency = "ROOT";
NTupleSvc.Output = { "FILE1 DATAFILE='test.root' OPT='NEW' TYP='ROOT'"};

MagneticFieldSvc.IfRealField = true;

//Grid distance of field map, (cm)
MagneticFieldSvc.GridDistance = 5;

//Select run mode, 1: SCQ 0.GeV,   SSM 1.0tesla, AS 0.tesla; 
//                 2: SCQ 1.55GeV, SSM 1.0tesla, AS 1.0tesla;
//                 3: SCQ 1.89GeV, SSM 1.0tesla, AS 1.0tesla;
//                 4: SCQ 2.01GeV, SSM 1.0tesla, AS 1.0tesla;
//                 5: SCQ 1.89GeV, SSM 0.0tesla, AS 0.0tesla;
//                 6: SCQ 0.0GeV,  SSM 0.0tesla, AS 1.0tesla;
//                 7: SCQ 1.55GeV, SSM 0.9tesla, AS 0.9tesla;
//                 8: SCQ 1.89GeV, SSM 0.9tesla, AS 0.9tesla;
MagneticFieldSvc.RunMode = 3;

MagFieldReader.filename = "TEMap5cm.dat";
// OUTPUT PRINTOUT LEVEL
// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 5;

// Event related parameters
// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 10000;
