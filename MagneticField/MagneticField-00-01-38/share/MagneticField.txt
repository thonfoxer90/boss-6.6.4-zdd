// Job options file for Magnetic Field
ApplicationMgr.DLLs   += { "MagneticField"};
ApplicationMgr.ExtSvc += { "MagneticFieldSvc"};

MagneticFieldSvc.IfRealField = true;

//output level
MagneticFieldSvc.OutLevel = 1;

//Grid distance of field map, (cm)
MagneticFieldSvc.GridDistance = 5;

//Select run mode, 1: SCQ 0.GeV,   SSM 1.0tesla, AS 0.tesla;
//                 2: SCQ 1.55GeV, SSM 1.0tesla, AS 1.0tesla;
//                 3: SCQ 1.89GeV, SSM 1.0tesla, AS 1.0tesla;
//                 4: SCQ 2.10GeV, SSM 1.0tesla, AS 1.0tesla;
//                 5: SCQ 1.89GeV, SSM 0.0tesla, AS 0.0tesla;
//                 6: SCQ 0.0GeV,  SSM 0.0tesla, AS 1.0tesla;
//                 7: SCQ 1.55GeV, SSM 0.9tesla, AS 0.9tesla;
//                 8: SCQ 1.89GeV, SSM 0.9tesla, AS 0.9tesla;
MagneticFieldSvc.RunMode = 3;

//The current of SCQ at the energy point 1.55GeV, 1.89GeV, 2.10GeV
MagneticFieldSvc.Cur_SCQ1_55 = 349.4;
MagneticFieldSvc.Cur_SCQ1_89 = 426.2;
MagneticFieldSvc.Cur_SCQ2_10 = 474.2;

MagneticFieldSvc.UseDBFlag = true;
MagneticFieldSvc.TurnOffField = false;
