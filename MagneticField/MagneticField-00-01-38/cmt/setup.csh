# echo "Setting MagneticField MagneticField-00-01-38 in /cluster/bes3/dist/6.6.4.p01"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cluster/bes3/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=MagneticField -version=MagneticField-00-01-38 -path=/cluster/bes3/dist/6.6.4.p01  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

