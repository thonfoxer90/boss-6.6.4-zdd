#include "GaudiKernel/DeclareFactoryEntries.h"
#include "MagneticField/MagneticFieldSvc.h"
#include "MagneticField/IMagneticFieldSvc.h"
#include "MagneticField/MagFieldReader.h"

DECLARE_SERVICE_FACTORY(MagneticFieldSvc)
DECLARE_ALGORITHM_FACTORY(MagFieldReader)

DECLARE_FACTORY_ENTRIES( MagneticField ) { 
  DECLARE_SERVICE( MagneticFieldSvc );
  DECLARE_ALGORITHM( MagFieldReader );
}
