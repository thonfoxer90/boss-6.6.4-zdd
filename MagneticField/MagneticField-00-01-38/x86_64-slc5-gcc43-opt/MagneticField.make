#-- start of make_header -----------------

#====================================
#  Library MagneticField
#
#   Generated Mon May  8 15:40:46 2017  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_MagneticField_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_MagneticField_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_MagneticField

MagneticField_tag = $(tag)

#cmt_local_tagfile_MagneticField = $(MagneticField_tag)_MagneticField.make
cmt_local_tagfile_MagneticField = $(bin)$(MagneticField_tag)_MagneticField.make

else

tags      = $(tag),$(CMTEXTRATAGS)

MagneticField_tag = $(tag)

#cmt_local_tagfile_MagneticField = $(MagneticField_tag).make
cmt_local_tagfile_MagneticField = $(bin)$(MagneticField_tag).make

endif

include $(cmt_local_tagfile_MagneticField)
#-include $(cmt_local_tagfile_MagneticField)

ifdef cmt_MagneticField_has_target_tag

cmt_final_setup_MagneticField = $(bin)setup_MagneticField.make
#cmt_final_setup_MagneticField = $(bin)MagneticField_MagneticFieldsetup.make
cmt_local_MagneticField_makefile = $(bin)MagneticField.make

else

cmt_final_setup_MagneticField = $(bin)setup.make
#cmt_final_setup_MagneticField = $(bin)MagneticFieldsetup.make
cmt_local_MagneticField_makefile = $(bin)MagneticField.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)MagneticFieldsetup.make

#MagneticField :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'MagneticField'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = MagneticField/
#MagneticField::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

MagneticFieldlibname   = $(bin)$(library_prefix)MagneticField$(library_suffix)
MagneticFieldlib       = $(MagneticFieldlibname).a
MagneticFieldstamp     = $(bin)MagneticField.stamp
MagneticFieldshstamp   = $(bin)MagneticField.shstamp

MagneticField :: dirs  MagneticFieldLIB
	$(echo) "MagneticField ok"

#-- end of libary_header ----------------

MagneticFieldLIB :: $(MagneticFieldlib) $(MagneticFieldshstamp)
	@/bin/echo "------> MagneticField : library ok"

$(MagneticFieldlib) :: $(bin)MucMagneticField.o $(bin)ConnectionDB.o $(bin)MagFieldReader.o $(bin)MagneticFieldSvc.o $(bin)MagneticField_load.o $(bin)MagneticField_entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(MagneticFieldlib) $?
	$(lib_silent) $(ranlib) $(MagneticFieldlib)
	$(lib_silent) cat /dev/null >$(MagneticFieldstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(MagneticFieldlibname).$(shlibsuffix) :: $(MagneticFieldlib) $(MagneticFieldstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" MagneticField $(MagneticField_shlibflags)

$(MagneticFieldshstamp) :: $(MagneticFieldlibname).$(shlibsuffix)
	@if test -f $(MagneticFieldlibname).$(shlibsuffix) ; then cat /dev/null >$(MagneticFieldshstamp) ; fi

MagneticFieldclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)MucMagneticField.o $(bin)ConnectionDB.o $(bin)MagFieldReader.o $(bin)MagneticFieldSvc.o $(bin)MagneticField_load.o $(bin)MagneticField_entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
MagneticFieldinstallname = $(library_prefix)MagneticField$(library_suffix).$(shlibsuffix)

MagneticField :: MagneticFieldinstall

install :: MagneticFieldinstall

MagneticFieldinstall :: $(install_dir)/$(MagneticFieldinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(MagneticFieldinstallname) :: $(bin)$(MagneticFieldinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(MagneticFieldinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(MagneticFieldinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(MagneticFieldinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(MagneticFieldinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(MagneticFieldinstallname) $(install_dir)/$(MagneticFieldinstallname); \
	      echo `pwd`/$(MagneticFieldinstallname) >$(install_dir)/$(MagneticFieldinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(MagneticFieldinstallname), no installation directory specified"; \
	  fi; \
	fi

MagneticFieldclean :: MagneticFielduninstall

uninstall :: MagneticFielduninstall

MagneticFielduninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(MagneticFieldinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(MagneticFieldinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(MagneticFieldinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(MagneticFieldinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),MagneticFieldclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)MagneticField_dependencies.make :: dirs

ifndef QUICK
$(bin)MagneticField_dependencies.make : $(src)MucMagneticField.cxx $(src)ConnectionDB.cxx $(src)MagFieldReader.cxx $(src)MagneticFieldSvc.cxx $(src)components/MagneticField_load.cxx $(src)components/MagneticField_entries.cxx $(use_requirements) $(cmt_final_setup_MagneticField)
	$(echo) "(MagneticField.make) Rebuilding $@"; \
	  $(build_dependencies) MagneticField -all_sources -out=$@ $(src)MucMagneticField.cxx $(src)ConnectionDB.cxx $(src)MagFieldReader.cxx $(src)MagneticFieldSvc.cxx $(src)components/MagneticField_load.cxx $(src)components/MagneticField_entries.cxx
endif

#$(MagneticField_dependencies)

-include $(bin)MagneticField_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),MagneticFieldclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MucMagneticField.d

$(bin)$(binobj)MucMagneticField.d : $(use_requirements) $(cmt_final_setup_MagneticField)

$(bin)$(binobj)MucMagneticField.d : $(src)MucMagneticField.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MucMagneticField.dep $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(MucMagneticField_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(MucMagneticField_cppflags) $(MucMagneticField_cxx_cppflags)  $(src)MucMagneticField.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MucMagneticField.o $(src)MucMagneticField.cxx $(@D)/MucMagneticField.dep
endif
endif

$(bin)$(binobj)MucMagneticField.o : $(src)MucMagneticField.cxx
else
$(bin)MagneticField_dependencies.make : $(MucMagneticField_cxx_dependencies)

$(bin)$(binobj)MucMagneticField.o : $(MucMagneticField_cxx_dependencies)
endif
	$(cpp_echo) $(src)MucMagneticField.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(MucMagneticField_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(MucMagneticField_cppflags) $(MucMagneticField_cxx_cppflags)  $(src)MucMagneticField.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),MagneticFieldclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ConnectionDB.d

$(bin)$(binobj)ConnectionDB.d : $(use_requirements) $(cmt_final_setup_MagneticField)

$(bin)$(binobj)ConnectionDB.d : $(src)ConnectionDB.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ConnectionDB.dep $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(ConnectionDB_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(ConnectionDB_cppflags) $(ConnectionDB_cxx_cppflags)  $(src)ConnectionDB.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ConnectionDB.o $(src)ConnectionDB.cxx $(@D)/ConnectionDB.dep
endif
endif

$(bin)$(binobj)ConnectionDB.o : $(src)ConnectionDB.cxx
else
$(bin)MagneticField_dependencies.make : $(ConnectionDB_cxx_dependencies)

$(bin)$(binobj)ConnectionDB.o : $(ConnectionDB_cxx_dependencies)
endif
	$(cpp_echo) $(src)ConnectionDB.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(ConnectionDB_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(ConnectionDB_cppflags) $(ConnectionDB_cxx_cppflags)  $(src)ConnectionDB.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),MagneticFieldclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MagFieldReader.d

$(bin)$(binobj)MagFieldReader.d : $(use_requirements) $(cmt_final_setup_MagneticField)

$(bin)$(binobj)MagFieldReader.d : $(src)MagFieldReader.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MagFieldReader.dep $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(MagFieldReader_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(MagFieldReader_cppflags) $(MagFieldReader_cxx_cppflags)  $(src)MagFieldReader.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MagFieldReader.o $(src)MagFieldReader.cxx $(@D)/MagFieldReader.dep
endif
endif

$(bin)$(binobj)MagFieldReader.o : $(src)MagFieldReader.cxx
else
$(bin)MagneticField_dependencies.make : $(MagFieldReader_cxx_dependencies)

$(bin)$(binobj)MagFieldReader.o : $(MagFieldReader_cxx_dependencies)
endif
	$(cpp_echo) $(src)MagFieldReader.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(MagFieldReader_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(MagFieldReader_cppflags) $(MagFieldReader_cxx_cppflags)  $(src)MagFieldReader.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),MagneticFieldclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MagneticFieldSvc.d

$(bin)$(binobj)MagneticFieldSvc.d : $(use_requirements) $(cmt_final_setup_MagneticField)

$(bin)$(binobj)MagneticFieldSvc.d : $(src)MagneticFieldSvc.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MagneticFieldSvc.dep $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(MagneticFieldSvc_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(MagneticFieldSvc_cppflags) $(MagneticFieldSvc_cxx_cppflags)  $(src)MagneticFieldSvc.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MagneticFieldSvc.o $(src)MagneticFieldSvc.cxx $(@D)/MagneticFieldSvc.dep
endif
endif

$(bin)$(binobj)MagneticFieldSvc.o : $(src)MagneticFieldSvc.cxx
else
$(bin)MagneticField_dependencies.make : $(MagneticFieldSvc_cxx_dependencies)

$(bin)$(binobj)MagneticFieldSvc.o : $(MagneticFieldSvc_cxx_dependencies)
endif
	$(cpp_echo) $(src)MagneticFieldSvc.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(MagneticFieldSvc_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(MagneticFieldSvc_cppflags) $(MagneticFieldSvc_cxx_cppflags)  $(src)MagneticFieldSvc.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),MagneticFieldclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MagneticField_load.d

$(bin)$(binobj)MagneticField_load.d : $(use_requirements) $(cmt_final_setup_MagneticField)

$(bin)$(binobj)MagneticField_load.d : $(src)components/MagneticField_load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MagneticField_load.dep $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(MagneticField_load_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(MagneticField_load_cppflags) $(MagneticField_load_cxx_cppflags) -I../src/components $(src)components/MagneticField_load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MagneticField_load.o $(src)components/MagneticField_load.cxx $(@D)/MagneticField_load.dep
endif
endif

$(bin)$(binobj)MagneticField_load.o : $(src)components/MagneticField_load.cxx
else
$(bin)MagneticField_dependencies.make : $(MagneticField_load_cxx_dependencies)

$(bin)$(binobj)MagneticField_load.o : $(MagneticField_load_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/MagneticField_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(MagneticField_load_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(MagneticField_load_cppflags) $(MagneticField_load_cxx_cppflags) -I../src/components $(src)components/MagneticField_load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),MagneticFieldclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MagneticField_entries.d

$(bin)$(binobj)MagneticField_entries.d : $(use_requirements) $(cmt_final_setup_MagneticField)

$(bin)$(binobj)MagneticField_entries.d : $(src)components/MagneticField_entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MagneticField_entries.dep $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(MagneticField_entries_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(MagneticField_entries_cppflags) $(MagneticField_entries_cxx_cppflags) -I../src/components $(src)components/MagneticField_entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MagneticField_entries.o $(src)components/MagneticField_entries.cxx $(@D)/MagneticField_entries.dep
endif
endif

$(bin)$(binobj)MagneticField_entries.o : $(src)components/MagneticField_entries.cxx
else
$(bin)MagneticField_dependencies.make : $(MagneticField_entries_cxx_dependencies)

$(bin)$(binobj)MagneticField_entries.o : $(MagneticField_entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/MagneticField_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(MagneticField_pp_cppflags) $(lib_MagneticField_pp_cppflags) $(MagneticField_entries_pp_cppflags) $(use_cppflags) $(MagneticField_cppflags) $(lib_MagneticField_cppflags) $(MagneticField_entries_cppflags) $(MagneticField_entries_cxx_cppflags) -I../src/components $(src)components/MagneticField_entries.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: MagneticFieldclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(MagneticField.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(MagneticField.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(MagneticField.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(MagneticField.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_MagneticField)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(MagneticField.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(MagneticField.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(MagneticField.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

MagneticFieldclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library MagneticField
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)MagneticField$(library_suffix).a $(library_prefix)MagneticField$(library_suffix).s? MagneticField.stamp MagneticField.shstamp
#-- end of cleanup_library ---------------
