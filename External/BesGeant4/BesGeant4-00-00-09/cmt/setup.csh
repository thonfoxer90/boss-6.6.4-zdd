# echo "Setting BesGeant4 BesGeant4-00-00-09 in /home/bgarillo/boss-6.6.4-ZDD/External"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cluster/him/bes3/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=BesGeant4 -version=BesGeant4-00-00-09 -path=/home/bgarillo/boss-6.6.4-ZDD/External  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

