----------> uses
# use BesExternalArea BesExternalArea-00-* External
# use BesCLHEP BesCLHEP-00-* External
#   use CLHEP v* LCG_Interfaces (no_version_directory) (native_version=2.0.4.5)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
#       use LCG_SettingsCompat *  (no_version_directory)
#   use HepMC * LCG_Interfaces (no_version_directory) (native_version=2.03.11)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
#   use HepPDT * LCG_Interfaces (no_version_directory) (native_version=2.05.04)
#     use LCG_Configuration v*  (no_version_directory)
#     use LCG_Settings v*  (no_version_directory)
#   use BesExternalArea BesExternalArea-* External
#
# Selection :
use CMT v1r20p20090520 (/cluster/him/bes3)
use LCG_SettingsCompat v1  (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5)
use LCG_Settings v1  (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5)
use LCG_Configuration v1  (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5)
use HepPDT v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use HepMC v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use CLHEP v1 LCG_Interfaces (/cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5/)
use BesExternalArea BesExternalArea-00-00-21 External (/cluster/him/bes3/dist/6.6.4/)
use BesCLHEP BesCLHEP-00-00-09 External (/cluster/him/bes3/dist/6.6.4/)
----------> tags
CMTv1 (from CMTVERSION)
CMTr20 (from CMTVERSION)
CMTp20090520 (from CMTVERSION)
Linux (from uname) package LCG_Settings implies [Unix host-linux]
x86_64-slc5-gcc43-opt (from CMTCONFIG) package LCG_Settings implies [Linux slc5-amd64 gcc43 optimized target-linux target-x86_64 target-slc5 target-gcc43 target-opt]
LOCAL (from CMTSITE)
bgarillo_no_config (from PROJECT) excludes [bgarillo_config]
bgarillo_root (from PROJECT) excludes [bgarillo_no_root]
bgarillo_cleanup (from PROJECT) excludes [bgarillo_no_cleanup]
bgarillo_prototypes (from PROJECT) excludes [bgarillo_no_prototypes]
bgarillo_with_installarea (from PROJECT) excludes [bgarillo_without_installarea]
bgarillo_with_version_directory (from PROJECT) excludes [bgarillo_without_version_directory]
bgarillo (from PROJECT)
BOSS_no_config (from PROJECT) excludes [BOSS_config]
BOSS_root (from PROJECT) excludes [BOSS_no_root]
BOSS_cleanup (from PROJECT) excludes [BOSS_no_cleanup]
BOSS_prototypes (from PROJECT) excludes [BOSS_no_prototypes]
BOSS_with_installarea (from PROJECT) excludes [BOSS_without_installarea]
BOSS_with_version_directory (from PROJECT) excludes [BOSS_without_version_directory]
GAUDI_no_config (from PROJECT) excludes [GAUDI_config]
GAUDI_root (from PROJECT) excludes [GAUDI_no_root]
GAUDI_cleanup (from PROJECT) excludes [GAUDI_no_cleanup]
GAUDI_prototypes (from PROJECT) excludes [GAUDI_no_prototypes]
GAUDI_with_installarea (from PROJECT) excludes [GAUDI_without_installarea]
GAUDI_without_version_directory (from PROJECT) excludes [GAUDI_with_version_directory]
LCGCMT_no_config (from PROJECT) excludes [LCGCMT_config]
LCGCMT_no_root (from PROJECT) excludes [LCGCMT_root]
LCGCMT_cleanup (from PROJECT) excludes [LCGCMT_no_cleanup]
LCGCMT_prototypes (from PROJECT) excludes [LCGCMT_no_prototypes]
LCGCMT_without_installarea (from PROJECT) excludes [LCGCMT_with_installarea]
LCGCMT_with_version_directory (from PROJECT) excludes [LCGCMT_without_version_directory]
x86_64 (from package CMT) package LCG_Settings implies [host-x86_64]
slc73 (from package CMT)
gcc432 (from package CMT) package LCG_Settings implies [gcc43 host-gcc43]
Unix (from package CMT) package LCG_Settings implies [host-unix] package LCG_Settings excludes [WIN32 Win32]
ROOT_GE_5_15 (from package LCG_Configuration)
ROOT_GE_5_19 (from package LCG_Configuration)
gcc43 (from package LCG_SettingsCompat)
amd64 (from package LCG_SettingsCompat)
slc5 (from package LCG_SettingsCompat)
slc5-amd64 (from package LCG_SettingsCompat) package LCG_SettingsCompat implies [slc5 amd64]
optimized (from package LCG_SettingsCompat)
target-unix (from package LCG_Settings)
host-x86_64 (from package LCG_Settings)
host-gcc4 (from package LCG_Settings) package LCG_Settings implies [host-gcc]
host-gcc43 (from package LCG_Settings) package LCG_Settings implies [host-gcc4]
host-gcc (from package LCG_Settings)
host-linux (from package LCG_Settings)
host-unix (from package LCG_Settings)
target-linux (from package LCG_Settings) package LCG_Settings implies [target-unix]
target-slc5 (from package LCG_Settings) package LCG_Settings implies [target-slc]
target-opt (from package LCG_Settings)
target-gcc43 (from package LCG_Settings) package LCG_Settings implies [target-gcc4]
target-x86_64 (from package LCG_Settings)
target-slc (from package LCG_Settings)
target-gcc4 (from package LCG_Settings) package LCG_Settings implies [target-gcc]
target-gcc (from package LCG_Settings)
----------> CMTPATH
# Add path /home/bgarillo/boss-6.6.4-ZDD from initialization
# Add path /cluster/him/bes3/dist/6.6.4 from initialization
# Add path /cluster/him/bes3/Gaudi/GAUDI_v21r6/x86_64-slc5-gcc43-clhep2.0.4.5 from initialization
# Add path /cluster/him/bes3/lcg/app/releases/LCGCMT/LCGCMT_57a_clhep2.0.4.5 from initialization
