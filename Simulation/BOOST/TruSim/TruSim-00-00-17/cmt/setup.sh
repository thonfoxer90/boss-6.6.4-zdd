# echo "Setting TruSim TruSim-00-00-17 in /home/bgarillo/boss-6.6.4-ZDD/Simulation/BOOST"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cluster/him/bes3/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh

tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=TruSim -version=TruSim-00-00-17 -path=/home/bgarillo/boss-6.6.4-ZDD/Simulation/BOOST  -no_cleanup $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

