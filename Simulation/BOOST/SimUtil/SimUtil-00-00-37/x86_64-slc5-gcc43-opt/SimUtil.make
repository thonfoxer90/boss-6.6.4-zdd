#-- start of make_header -----------------

#====================================
#  Library SimUtil
#
<<<<<<< HEAD
#   Generated Sat May 19 14:57:13 2018  by bgarillo
=======
#   Generated Sun May 20 13:51:40 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_SimUtil_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_SimUtil_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_SimUtil

SimUtil_tag = $(tag)

#cmt_local_tagfile_SimUtil = $(SimUtil_tag)_SimUtil.make
cmt_local_tagfile_SimUtil = $(bin)$(SimUtil_tag)_SimUtil.make

else

tags      = $(tag),$(CMTEXTRATAGS)

SimUtil_tag = $(tag)

#cmt_local_tagfile_SimUtil = $(SimUtil_tag).make
cmt_local_tagfile_SimUtil = $(bin)$(SimUtil_tag).make

endif

include $(cmt_local_tagfile_SimUtil)
#-include $(cmt_local_tagfile_SimUtil)

ifdef cmt_SimUtil_has_target_tag

cmt_final_setup_SimUtil = $(bin)setup_SimUtil.make
#cmt_final_setup_SimUtil = $(bin)SimUtil_SimUtilsetup.make
cmt_local_SimUtil_makefile = $(bin)SimUtil.make

else

cmt_final_setup_SimUtil = $(bin)setup.make
#cmt_final_setup_SimUtil = $(bin)SimUtilsetup.make
cmt_local_SimUtil_makefile = $(bin)SimUtil.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)SimUtilsetup.make

#SimUtil :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'SimUtil'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = SimUtil/
#SimUtil::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

SimUtillibname   = $(bin)$(library_prefix)SimUtil$(library_suffix)
SimUtillib       = $(SimUtillibname).a
SimUtilstamp     = $(bin)SimUtil.stamp
SimUtilshstamp   = $(bin)SimUtil.shstamp

SimUtil :: dirs  SimUtilLIB
	$(echo) "SimUtil ok"

#-- end of libary_header ----------------

SimUtilLIB :: $(SimUtillib) $(SimUtilshstamp)
	@/bin/echo "------> SimUtil : library ok"

$(SimUtillib) :: $(bin)ReadBoostRoot.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(SimUtillib) $?
	$(lib_silent) $(ranlib) $(SimUtillib)
	$(lib_silent) cat /dev/null >$(SimUtilstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(SimUtillibname).$(shlibsuffix) :: $(SimUtillib) $(SimUtilstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" SimUtil $(SimUtil_shlibflags)

$(SimUtilshstamp) :: $(SimUtillibname).$(shlibsuffix)
	@if test -f $(SimUtillibname).$(shlibsuffix) ; then cat /dev/null >$(SimUtilshstamp) ; fi

SimUtilclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)ReadBoostRoot.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
SimUtilinstallname = $(library_prefix)SimUtil$(library_suffix).$(shlibsuffix)

SimUtil :: SimUtilinstall

install :: SimUtilinstall

SimUtilinstall :: $(install_dir)/$(SimUtilinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(SimUtilinstallname) :: $(bin)$(SimUtilinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(SimUtilinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(SimUtilinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(SimUtilinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(SimUtilinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(SimUtilinstallname) $(install_dir)/$(SimUtilinstallname); \
	      echo `pwd`/$(SimUtilinstallname) >$(install_dir)/$(SimUtilinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(SimUtilinstallname), no installation directory specified"; \
	  fi; \
	fi

SimUtilclean :: SimUtiluninstall

uninstall :: SimUtiluninstall

SimUtiluninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(SimUtilinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(SimUtilinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(SimUtilinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(SimUtilinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),SimUtilclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)SimUtil_dependencies.make :: dirs

ifndef QUICK
$(bin)SimUtil_dependencies.make : $(src)ReadBoostRoot.cc $(use_requirements) $(cmt_final_setup_SimUtil)
	$(echo) "(SimUtil.make) Rebuilding $@"; \
	  $(build_dependencies) SimUtil -all_sources -out=$@ $(src)ReadBoostRoot.cc
endif

#$(SimUtil_dependencies)

-include $(bin)SimUtil_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),SimUtilclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ReadBoostRoot.d

$(bin)$(binobj)ReadBoostRoot.d : $(use_requirements) $(cmt_final_setup_SimUtil)

$(bin)$(binobj)ReadBoostRoot.d : $(src)ReadBoostRoot.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ReadBoostRoot.dep $(use_pp_cppflags) $(SimUtil_pp_cppflags) $(lib_SimUtil_pp_cppflags) $(ReadBoostRoot_pp_cppflags) $(use_cppflags) $(SimUtil_cppflags) $(lib_SimUtil_cppflags) $(ReadBoostRoot_cppflags) $(ReadBoostRoot_cc_cppflags)  $(src)ReadBoostRoot.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ReadBoostRoot.o $(src)ReadBoostRoot.cc $(@D)/ReadBoostRoot.dep
endif
endif

$(bin)$(binobj)ReadBoostRoot.o : $(src)ReadBoostRoot.cc
else
$(bin)SimUtil_dependencies.make : $(ReadBoostRoot_cc_dependencies)

$(bin)$(binobj)ReadBoostRoot.o : $(ReadBoostRoot_cc_dependencies)
endif
	$(cpp_echo) $(src)ReadBoostRoot.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(SimUtil_pp_cppflags) $(lib_SimUtil_pp_cppflags) $(ReadBoostRoot_pp_cppflags) $(use_cppflags) $(SimUtil_cppflags) $(lib_SimUtil_cppflags) $(ReadBoostRoot_cppflags) $(ReadBoostRoot_cc_cppflags)  $(src)ReadBoostRoot.cc

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: SimUtilclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(SimUtil.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(SimUtil.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(SimUtil.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(SimUtil.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_SimUtil)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(SimUtil.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(SimUtil.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(SimUtil.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

SimUtilclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library SimUtil
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)SimUtil$(library_suffix).a $(library_prefix)SimUtil$(library_suffix).s? SimUtil.stamp SimUtil.shstamp
#-- end of cleanup_library ---------------
