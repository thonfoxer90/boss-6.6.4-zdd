//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description: Hit in ZDD
//Author:  Youzy      Peking University      mail: youzy@hep.pku.cn
//Created:  Nov, 2003
//Modified:
//Comment:
//---------------------------------------------------------------------------//

//
// $Id: BesZddHit.hh,v 1.3 2009/08/25 13:33:54 xieyg Exp $
// GEANT4 tag $Name: ZddSim-00-01-01 $

#ifndef BesZddHit_h
#define BesZddHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4String.hh"
#include "G4ThreeVector.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
//#include "BesDetectorConstruction.hh"
#include "G4Track.hh"

#include <vector>

class BesZddHit : public G4VHit
{
public:
  BesZddHit();
  //  BesZddHit(G4int, G4int, G4int, G4double, G4double, G4double, G4ThreeVector, G4ThreeVector);
  BesZddHit(G4int, G4int, G4int, G4double, G4double, G4double, G4ThreeVector, G4ThreeVector, G4double, G4double);
  //BesZddHit(BesDetectorConstruction*);
  ~BesZddHit();
  BesZddHit(const BesZddHit&);
  const BesZddHit& operator=(const BesZddHit&);
  int operator==(const BesZddHit&) const;
  
  inline void* operator new(size_t);
  inline void  operator delete(void*);
  
  void Draw(); 
  void Print();
  
public:
  // Set method s
  void SetTrackID           (G4int track)  { m_TrackID = track; };
  void SetPreCrystalNo  (G4int crystalNo)  { m_preCrystalNo = crystalNo; }; 
  void SetPostCrystalNo (G4int crystalNo)  { m_postCrystalNo = crystalNo; }; 
  void SetPDGCode             (G4int pdg)  { m_PDGCode = pdg;};
  void SetEDep            (G4double edep)  { m_EDep = edep; };
  void SetMomentum(G4ThreeVector momentum) { m_Momentum = momentum;};
  void SetVolume   (G4VPhysicalVolume* pv) { m_Volume = pv;};
  void SetPreTime            (G4double t)  { m_preTime = t;};
  void SetPostTime            (G4double t) { m_postTime = t;};
  void SetPrePos   (G4ThreeVector pre_xyz) { m_prePos = pre_xyz; };
  void SetPostPos (G4ThreeVector post_xyz) { m_postPos = post_xyz; };
  void SetXImpact   (G4double xImpact)         { m_xImpact = xImpact; };
  void SetYImpact     (G4double yImpact)           {m_yImpact = yImpact; };
	void SetZImpact     (G4double zImpact)           {m_zImpact = zImpact; };
	void SetIsTrackFromIP(bool val) {m_IsTrackFromIP=val;};
  void SetPrePhysVolName(G4String name) {m_prePhysVolName=name;};
	void SetPostPhysVolName(G4String name) {m_postPhysVolName=name;};
	    
  // Get methods
  G4int GetTrackID()          { return m_TrackID; };
  G4int GetPreCrystalNo()     { return m_preCrystalNo; };
  G4int GetPostCrystalNo()    { return m_postCrystalNo; };      
  G4int GetPDGCode()          { return m_PDGCode;};
  G4double GetEDep()          { return m_EDep; };  
  G4ThreeVector GetMomentum() { return m_Momentum;};
  G4double GetPreTime()       { return m_preTime;};
  G4double GetPostTime()      { return m_postTime;};
  G4ThreeVector GetPrePos()   { return m_prePos; };
  G4ThreeVector GetPostPos()  { return m_postPos; };

  G4double GetXImpact()         { return m_xImpact;};
  G4double GetYImpact()           { return m_yImpact;};
	G4double GetZImpact()           { return m_zImpact;};

  G4VPhysicalVolume* GetVolume(){ return m_Volume; };

	bool IsTrackFromIP() {return m_IsTrackFromIP;};
      
private:
  G4int         m_TrackID;

  G4int         m_preCrystalNo;
  G4int         m_postCrystalNo;
  G4int         m_PDGCode;
  G4double      m_EDep; // EDep during step
  G4ThreeVector m_Momentum;
  G4double      m_preTime; // time at PreStepPoint
  G4double      m_postTime; // time at PostStepPoint
  G4ThreeVector m_prePos; // Position of PreStep Point
  G4ThreeVector m_postPos; // Position of PostStep Point

  G4double      m_xImpact; // theta value from generated particle, important for impact position reconstruction
  G4double     m_yImpact;   // theta value from generated particle, important for impact position reconstruction
	G4double     m_zImpact;   // theta value from generated particle, important for impact position reconstruction
  G4VPhysicalVolume* m_Volume;
	bool m_IsTrackFromIP;
	G4String			m_prePhysVolName;
	G4String			m_postPhysVolName;
};

typedef G4THitsCollection<BesZddHit> BesZddHitsCollection;

extern G4Allocator<BesZddHit> BesZddHitAllocator;

inline void* BesZddHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) BesZddHitAllocator.MallocSingle();
  return aHit;
}

inline void BesZddHit::operator delete(void *aHit)
{
  BesZddHitAllocator.FreeSingle((BesZddHit*) aHit);
}

#endif


