//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description:
//Author: Youzy
//Created: July, 2004
//Modified:
//Comment:
//---------------------------------------------------------------------------//
// $Id: BesZddDigi.hh

#ifndef BesZddDigi_h
#define BesZddDigi_h 1

#include "G4VDigi.hh"
#include "G4TDigiCollection.hh"
#include "G4Allocator.hh"

class BesZddDigi : public G4VDigi
{
public:
  BesZddDigi();
  virtual ~BesZddDigi();
  
  BesZddDigi(const BesZddDigi&);
  const BesZddDigi& operator=(const BesZddDigi&);
  
  virtual G4int operator==(const BesZddDigi&) const;
  inline void* operator new(size_t);
  inline void  operator delete(void*);
  
  virtual void Draw();
  virtual void Print();
  
public:

  // Set part ID // 1 = east zdd; // 2 = west zdd 
  void SetPartID (G4int partID) {m_PartID = partID; };

  // Set detector ID // 1 = upper crystals, 2 = lower crystals
  void SetDetectorID (G4int detectorID) {m_DetectorID = detectorID; };

  // Set Crystal No
  void SetCrystalNo        (G4int crystalNo) {m_CrystalNo = crystalNo; };

  // Set Energy deposition // bigger charge means more energy
  void SetChargeChannel          (G4double charge){ m_ChargeChannel = charge; };

  // set time, when signal was triggered
  void SetTimeChannel           (G4double time) { m_TimeChannel = time; /* BesZddDigi::Print();*/ };
  
  // Get method
  G4int GetPartID() {return m_PartID; };

  G4int GetDetectorID() {return m_DetectorID; };

  G4int GetCrystalNo() { return m_CrystalNo; };

  G4double GetChargeChannel() { return m_ChargeChannel; };
 
  G4double GetTimeChannel() { return m_TimeChannel; };

private:

  // PartID: 1 = left Zdd, 2 = right Zdd // viewpoint from BesIII control room 
  G4int         m_PartID;
  // DetectorID: 1 = upper crystals, 2 = lower crystals
  G4int         m_DetectorID;
  // CrystalNo
  G4int         m_CrystalNo;
  // total energy depostion per event
  G4double      m_ChargeChannel;
  // timing information for the single crystal
  G4double      m_TimeChannel;
};

typedef G4TDigiCollection<BesZddDigi> BesZddDigisCollection;

extern G4Allocator<BesZddDigi> BesZddDigiAllocator;

inline void* BesZddDigi::operator new(size_t)
{
  void *aDigi;
  aDigi = (void *) BesZddDigiAllocator.MallocSingle();
  return aDigi;
}

inline void BesZddDigi::operator delete(void *aDigi)
{
  BesZddDigiAllocator.FreeSingle((BesZddDigi*) aDigi);
}

#endif

