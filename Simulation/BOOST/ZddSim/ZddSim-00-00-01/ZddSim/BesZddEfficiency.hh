//
//
//
//


#ifndef BesZddEfficiency_h
#define BesZddEfficiency_h 1

#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/MsgStream.h"

#include "BesZddHit.hh"
#include "BesZddDigit.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"

//#include "ZddCalibConstSvc/IZddCalibConstSvc.h"
//#include "ZddCalibConstSvc/ZddCalibConstSvc.h"

#define part_Max (3)
//#define segOnPart[3] ({4,8,4})
//#define gapOnPart[3] ({8,9,8})
#define seg_Max (8)
#define gap_Max (9)
#define strip_Max (96)
#define pad_Max (100)

using namespace std;

class BesZddEfficiency
{
public:
  BesZddEfficiency();
  ~BesZddEfficiency();
  /*
  void Initialize(G4String filename);

  void CheckCalibSvc();
  void SetHit(BesZddHit* hit);
  void GetPosLengthWidth(G4VPhysicalVolume* pvStrip);

  G4double GetEfficiency();
  static BesZddEfficiency * Instance(void);

public:
  IMessageSvc* 		msgSvc;
*/
private:
  /*  BesZddHit*  m_pHit;
  static BesZddEfficiency * fPointer;
  G4double    IsAddEffi;

  IZddCalibConstSvc*  m_ptrCalibSvc;*/   // pointer of calibration constants service
};

#endif
