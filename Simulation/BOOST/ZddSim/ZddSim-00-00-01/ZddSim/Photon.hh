// virtual numerical photon class
#ifndef Photon_h
#define Photon_h 1

#include "G4ThreeVector.hh"
#include "G4Allocator.hh"
#include <vector>

class Photon {
  
  public:
    Photon();
  Photon(double pos_x, double pos_y, double pos_z, double t_traj, double theta);
    ~Photon();

  public:
     G4double GetTotalTime(int i){return m_t_traj.at(i) + m_t_emit.at(i) + m_t_prop.at(i) + m_t_trans.at(i);};
     void AddPhoton(double pos_x, double pos_y, double pos_z, double t_traj, double theta);
     
     void SetTheta	(double theta);
     void SetPosX	(double x)		{ m_x.push_back(x); };
     void SetPosY	(double y)		{ m_y.push_back(y); };
     void SetPosZ	(double z)		{ m_z.push_back(z); };
     void SetTrajTime	(double t_traj)		{ m_t_traj.push_back(t_traj); };
     void SetEmitTime	(double t_emit)		{ m_t_emit.push_back(t_emit); };    
     void SetPropTime	(double t_prop)		{ m_t_prop.push_back(t_prop); };  
     void SetTransTime	(double t_trans)	{ m_t_trans.push_back(t_trans); };

//     void SetDestroyFlag(int destroyFlag);
//     void SetDestroyFlag(int i, int destroyFlag);

     void SetChargeChannel( int chrChannel) { m_chargeChannel = chrChannel; };
     void SetTimeChannel( int timeChannel)  { m_timeChannel = timeChannel; };     
     void AddOutput(int i, double y)        { m_output[i][0] += y; };   //! add y at x = i;
     G4float GetOutput(int i)              { return m_output[i][0]; };
     
     void SetCrystalNo(int crystalNo)	{ m_crystalNo = crystalNo; };
     
     G4double GetTheta(int i)		{ return m_theta.at(i); };
     G4double GetPosX(int i)		{ return m_x.at(i); };
     G4double GetPosY(int i)		{ return m_y.at(i); };
     G4double GetPosZ(int i)		{ return m_z.at(i); };
     G4double GetTrajTime(int i)	{ return m_t_traj.at(i); };
     G4double GetEmitTime(int i)	{ return m_t_emit.at(i); };
     G4double GetPropTime(int i)	{ return m_t_prop.at(i); };
     G4double GetTransTime(int i)	{ return m_t_trans.at(i); };     
  //   G4int GetDestroyFlag(int i)  	{ return m_destroyFlag.at(i); };
 
     G4double GetThetaSize()		{ return m_theta.size(); };
     G4double GetPosXSize()		{ return m_x.size(); };
     G4double GetPosYSize()		{ return m_y.size(); };
     G4double GetPosZSize()		{ return m_z.size(); };
     G4double GetTrajTimeSize()		{ return m_t_traj.size(); };
     G4double GetEmitTimeSize()		{ return m_t_emit.size(); };
     G4double GetPropTimeSize()		{ return m_t_prop.size(); };
     G4double GetTransTimeSize()	{ return m_t_trans.size(); };     
 //    G4int GetDestroyFlagSize()  	{ return m_destroyFlag.size(); };     
     
     G4int GetCrystalNo()		{ return m_crystalNo;};
     G4int GetChargeChannel()		{ return m_chargeChannel;};
     G4int GetTimeChannel()		{ return m_timeChannel;};   
//     G4int GetDestroyFlgs(int i);
     
     G4bool EraseVectorElement(); //! delete last vector elements
     G4bool EraseVectorElement(int i); //! delete i-th vector elements 
     
  private:
     G4int m_entries;
     G4float m_n_scint; //! optical refraction index of PbWO4
     G4int m_crystalNo;
    
     std::vector<G4double> m_theta;    //! emission angle of photon
     std::vector<G4double> m_x;        //! xPosition of creation
     std::vector<G4double> m_y;        //! yPosition of creation
     std::vector<G4double> m_z;        //! zPosition of creation
     std::vector<G4double> m_t_traj;   //! first time: t_trajectory
     std::vector<G4double> m_t_emit;   //! emission time
     std::vector<G4double> m_t_prop;   //! propagation time
     std::vector<G4double> m_t_trans;  //! transit time in PMT

//     std::vector<G4int> m_destroyFlag; //! =0 not destroyed
				       //! =1 destroyed through theta_cut
				       //! =2 destroyed through attenuation
				       //! =3 destroyed through mismatch cross-section crystal <-> PMT
				       //! =4 destroyed through angular response of PMT sensitivity
				       //! =5 destroyed through quantum efficiency of PMT
  /*  
    G4int m_destroyFlg0; //! how many photons with destroyFlag 0 1 2 3 4 5 s
    G4int m_destroyFlg1;
    G4int m_destroyFlg2;
    G4int m_destroyFlg3;
    G4int m_destroyFlg4;    
    G4int m_destroyFlg5;
    */
  //PMT value
  //  G4float m_output[3200][1];  //! array to save crystal output information [0,80ns] -> 1 entries per 25ps; first box gives x position [0 = 0ns, 3200 = 80ns], second stores y-info
  // SiPM value
  static G4int arraysize; //400
  G4double m_output[400][1];//array to save crystal output information [0,400ns] -> 1 entries per 1ns; first box gives x position [0 = 0ns, 200 = 400ns], second stores y-info

    G4int m_chargeChannel; //! Charge channel of crystal output
    G4int m_timeChannel; //! Time channel of crystal output
};		



#endif
