//Description: Digitization of BesZdd Hits

#ifndef BesZddDigit_h
#define BesZddDigit_h 1

#include "BesZddHit.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TTree.h"
#include "TF1.h"
#include "TMath.h"
//#include "TRandom.h"
#include "TRandom3.h"
#include "Photon.hh"
#include <vector>

class BesZddDigit
{
public:
	static void SetCrystalMaterial(G4String material);
	static void SetSensor(G4String sensor);
  BesZddDigit();
  ~BesZddDigit();
  //BesZddDigit(const BesZddDigit&);
  //const BesZddDigit$ operator=(const BesZddDigit&);
  //int operator==(const BesZddDigit&) const;

  //inline void* operator new(size_t);
  //inline void operator delete(void*);
  void SetHit(BesZddHit* hit);
  void PhotonCalculations( G4int& a, Photon& photon );

  void EndOfEvent();
//  void ResizeVectors(Photon& photon);
  void CalculatePMToutput(Photon& photon); // usual PMT
  void CalculateSiPMoutput(Photon& photon, double adc_smear);// use SiPM
  
  // Get energy deposited in certain crystal
      G4double GetTotEDep()    					{ return m_totEDep; };
      G4int GetTrackID()					{ return m_TrackID; };
      
      G4double GetRandomTimeEmit()				{ return fTimeEmit->GetRandom(); };  //! Get random variable distributed as function fTimeEmit
      
      G4int GetChargeChannel101()			{ if(Photon101!=NULL) return Photon101->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel102()			{ if(Photon102!=NULL) return Photon102->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel103()			{ if(Photon103!=NULL) return Photon103->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel104()			{ if(Photon104!=NULL) return Photon104->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel111()			{ if(Photon111!=NULL) return Photon111->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel112()			{ if(Photon112!=NULL) return Photon112->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel113()			{ if(Photon113!=NULL) return Photon113->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel114()			{ if(Photon114!=NULL) return Photon114->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel121()			{ if(Photon121!=NULL) return Photon121->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel122()			{ if(Photon122!=NULL) return Photon122->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel123()			{ if(Photon123!=NULL) return Photon123->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel124()			{ if(Photon124!=NULL) return Photon124->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel201()			{ if(Photon201!=NULL) return Photon201->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel202()			{ if(Photon202!=NULL) return Photon202->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel203()			{ if(Photon203!=NULL) return Photon203->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel204()			{ if(Photon204!=NULL) return Photon204->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel211()			{ if(Photon211!=NULL) return Photon211->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel212()			{ if(Photon212!=NULL) return Photon212->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel213()			{ if(Photon213!=NULL) return Photon213->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel214()			{ if(Photon214!=NULL) return Photon214->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel221()			{ if(Photon221!=NULL) return Photon221->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel222()			{ if(Photon222!=NULL) return Photon222->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel223()			{ if(Photon223!=NULL) return Photon223->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel224()			{ if(Photon224!=NULL) return Photon224->GetChargeChannel(); else return -99; };

      // Get timing information of certain crystal
      G4int GetTimeChannel101()			{ if(Photon101!=NULL) return Photon101->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel102()			{ if(Photon102!=NULL) return Photon102->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel103()			{ if(Photon103!=NULL) return Photon103->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel104()			{ if(Photon104!=NULL) return Photon104->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel111()			{ if(Photon111!=NULL) return Photon111->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel112()			{ if(Photon112!=NULL) return Photon112->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel113()			{ if(Photon113!=NULL) return Photon113->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel114()			{ if(Photon114!=NULL) return Photon114->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel121()			{ if(Photon121!=NULL) return Photon121->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel122()			{ if(Photon122!=NULL) return Photon122->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel123()			{ if(Photon123!=NULL) return Photon123->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel124()			{ if(Photon124!=NULL) return Photon124->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel201()			{ if(Photon201!=NULL) return Photon201->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel202()			{ if(Photon202!=NULL) return Photon202->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel203()			{ if(Photon203!=NULL) return Photon203->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel204()			{ if(Photon204!=NULL) return Photon204->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel211()			{ if(Photon211!=NULL) return Photon211->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel212()			{ if(Photon212!=NULL) return Photon212->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel213()			{ if(Photon213!=NULL) return Photon213->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel214()			{ if(Photon214!=NULL) return Photon214->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel221()			{ if(Photon221!=NULL) return Photon221->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel222()			{ if(Photon222!=NULL) return Photon222->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel223()			{ if(Photon223!=NULL) return Photon223->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel224()			{ if(Photon224!=NULL) return Photon224->GetTimeChannel(); else return -99; };

      G4int GetChargeChannel_101()			{ if(Photon_101!=NULL) return Photon_101->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_102()			{ if(Photon_102!=NULL) return Photon_102->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_103()			{ if(Photon_103!=NULL) return Photon_103->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_104()			{ if(Photon_104!=NULL) return Photon_104->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_111()			{ if(Photon_111!=NULL) return Photon_111->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_112()			{ if(Photon_112!=NULL) return Photon_112->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_113()			{ if(Photon_113!=NULL) return Photon_113->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_114()			{ if(Photon_114!=NULL) return Photon_114->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_121()			{ if(Photon_121!=NULL) return Photon_121->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_122()			{ if(Photon_122!=NULL) return Photon_122->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_123()			{ if(Photon_123!=NULL) return Photon_123->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_124()			{ if(Photon_124!=NULL) return Photon_124->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_201()			{ if(Photon_201!=NULL) return Photon_201->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_202()			{ if(Photon_202!=NULL) return Photon_202->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_203()			{ if(Photon_203!=NULL) return Photon_203->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_204()			{ if(Photon_204!=NULL) return Photon_204->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_211()			{ if(Photon_211!=NULL) return Photon_211->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_212()			{ if(Photon_212!=NULL) return Photon_212->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_213()			{ if(Photon_213!=NULL) return Photon_213->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_214()			{ if(Photon_214!=NULL) return Photon_214->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_221()			{ if(Photon_221!=NULL) return Photon_221->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_222()			{ if(Photon_222!=NULL) return Photon_222->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_223()			{ if(Photon_223!=NULL) return Photon_223->GetChargeChannel(); else return -99; };
      G4int GetChargeChannel_224()			{ if(Photon_224!=NULL) return Photon_224->GetChargeChannel(); else return -99; };

      // Get timing information of certain crystal
      G4int GetTimeChannel_101()			{ if(Photon_101!=NULL) return Photon_101->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_102()			{ if(Photon_102!=NULL) return Photon_102->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_103()			{ if(Photon_103!=NULL) return Photon_103->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_104()			{ if(Photon_104!=NULL) return Photon_104->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_111()			{ if(Photon_111!=NULL) return Photon_111->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_112()			{ if(Photon_112!=NULL) return Photon_112->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_113()			{ if(Photon_113!=NULL) return Photon_113->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_114()			{ if(Photon_114!=NULL) return Photon_114->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_121()			{ if(Photon_121!=NULL) return Photon_121->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_122()			{ if(Photon_122!=NULL) return Photon_122->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_123()			{ if(Photon_123!=NULL) return Photon_123->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_124()			{ if(Photon_124!=NULL) return Photon_124->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_201()			{ if(Photon_201!=NULL) return Photon_201->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_202()			{ if(Photon_202!=NULL) return Photon_202->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_203()			{ if(Photon_203!=NULL) return Photon_203->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_204()			{ if(Photon_204!=NULL) return Photon_204->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_211()			{ if(Photon_211!=NULL) return Photon_211->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_212()			{ if(Photon_212!=NULL) return Photon_212->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_213()			{ if(Photon_213!=NULL) return Photon_213->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_214()			{ if(Photon_214!=NULL) return Photon_214->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_221()			{ if(Photon_221!=NULL) return Photon_221->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_222()			{ if(Photon_222!=NULL) return Photon_222->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_223()			{ if(Photon_223!=NULL) return Photon_223->GetTimeChannel(); else return -99; };
      G4int GetTimeChannel_224()			{ if(Photon_224!=NULL) return Photon_224->GetTimeChannel(); else return -99; };

      G4ThreeVector GetPreStep(){ return m_preStep;}
  G4ThreeVector GetPostStep(){return m_postStep;}

  G4double GetXImpact(){ return m_xImpact;}
  G4double GetYImpact(){return m_yImpact;} 
	G4double GetZImpact(){return m_zImpact;}

	G4ThreeVector GetMomImpact(){return m_momImpact;}
	G4double GetPxImpact(){return m_PxImpact;}
	G4double GetPyImpact(){return m_PyImpact;}
	G4double GetPzImpact(){return m_PzImpact;}
	
  private:
  G4int         		m_TrackID;
  G4int 	    		m_CrystalNo;
      // total energy depostion per event
  G4double      		m_totEDep;
  // x and y impact from BesSteppingAction, used for reconstructing impact position
  G4double                      m_xImpact;
  G4double                      m_yImpact;
	G4double                      m_zImpact;
  

  G4bool m_firstHit; //! if first Hit in Event, then true, else false
  G4ThreeVector m_preStep; //! preStepPosition of First Hit
  G4ThreeVector m_postStep;//! postStepPosition of First Hit

	G4ThreeVector m_momImpact;
	G4double                      m_PxImpact;
  G4double                      m_PyImpact;
	G4double                      m_PzImpact;
	
private:
  BesZddHit* 		m_pHit;
	
	static	G4String materialZdd;
	static	G4String sensorZdd;
	static G4double lightYield; //! Theoretical number of photons per 1 MeV energy deposition 
  static G4int photonConstant;  //! number of photons per 1 MeV energy deposition -> has to be adjusted to real crystal data
  static G4double transL; //! absorption length of PbWO4, after which szintillation light is attenuated to 1/e
  static G4double thetaLoss; //! percentage of photons which are lost because of theta angle
  static G4double mismatchLoss; //! percentage of photons which are lost because of crystal PMT <-> scintillator crosssection mismatch
  static G4double angularAcceptanceLoss; //! percentage of photons which are lost because of angular acceptance of PMT
  static G4double QELoss; //! percentage of photons which are lost because of quantum efficiency of PMT;
  static G4double SiPMQE; // quantum efficiency of used SiPM at 440nm wavelength (PbWO4 emission wave-length)
  static G4int    SiPMPixels; // total number of pixels of all SiPMs
  static G4int    numberOfSiPMs; // how many SiPMs will be used for readout?
  static TRandom3* random; // random generator for simulating the "time_walk" of the ADC sampling point, static because distribution should not be deleted after destructor call
  static G4double photonsPerMeV; //! average number of Photons per MeV

  static G4int    eventCounter; //! how many events where simulated
/*  
  static TRandom* uniRand1; //! generator for random numbers 1
  static TRandom* uniRand2; //! generator for random numbers 2 
  static TRandom* uniRand3; //! generator for random numbers 3    
  static TRandom* uniRand4; //! generator for random numbers 4 
  static TRandom* uniRand5; //! generator for random numbers 5   

//! 5 generators because distributions should be totally indepedent => 5 different, independent photon killing sources

  static const G4int m_seed1; //! seed of random generator 1
  static const G4int m_seed2; //! seed of random generator 2
  static const G4int m_seed3; //! seed of random generator 3 
  static const G4int m_seed4; //! seed of random generator 4
  static const G4int m_seed5; //! seed of random generator 5
*/
//! histo for saving photon creation positions  
//   TH3D* hXYZpositions;
//! histo for saving Time Trajectory
//   TH1D* hTimeTraj;
//! histo for saving Time Emission
//   TH1D* hTimeEmit;
//! histo for saving Time Propagation
//   TH1D* hTimeProp;
//! histo for saving Time Transur
//   TH1D* hTimeTrans;
//   TH1D* hADCwave;
//! histo for saving something for debugging
//   TH1D* hTemp1;
//   TH1D* hTemp2;
  
//! histo for saving Edep <-> Charge Channel distribution
  static TH2D* hEDepChargeChannel; //static, because should not always be created/deleted, when digit is created/deleted 

//! function, which describes distribution of TimeEmit  
  TF1* fTimeEmit;
//! function of single photon electron PMT output signal
  TF1* fPE;
  TF1* fTot;    
  //! function of total PMT output signal 
  TF1* fPMT;
  
  
  Photon* Photon101; //! photon collection of crystal 101
  Photon* Photon102; //! photon collection of crystal 102
  Photon* Photon103; //! photon collection of crystal 103
  Photon* Photon104; //! photon collection of crystal 104
  Photon* Photon111; //! photon collection of crystal 111
  Photon* Photon112; //! photon collection of crystal 112
  Photon* Photon113; //! photon collection of crystal 113
  Photon* Photon114; //! photon collection of crystal 114 
  Photon* Photon121; //! photon collection of crystal 121
  Photon* Photon122; //! photon collection of crystal 122
  Photon* Photon123; //! photon collection of crystal 123
  Photon* Photon124; //! photon collection of crystal 124

  Photon* Photon201; //! photon collection of crystal 201
  Photon* Photon202; //! photon collection of crystal 202
  Photon* Photon203; //! photon collection of crystal 203
  Photon* Photon204; //! photon collection of crystal 204 
  Photon* Photon211; //! photon collection of crystal 211
  Photon* Photon212; //! photon collection of crystal 212
  Photon* Photon213; //! photon collection of crystal 213
  Photon* Photon214; //! photon collection of crystal 214
  Photon* Photon221; //! photon collection of crystal 221
  Photon* Photon222; //! photon collection of crystal 222
  Photon* Photon223; //! photon collection of crystal 223
  Photon* Photon224; //! photon collection of crystal 224

  Photon* Photon_101; //! photon collection of crystal -101
  Photon* Photon_102; //! photon collection of crystal -102
  Photon* Photon_103; //! photon collection of crystal -103
  Photon* Photon_104; //! photon collection of crystal -104
  Photon* Photon_111; //! photon collection of crystal -111
  Photon* Photon_112; //! photon collection of crystal -112
  Photon* Photon_113; //! photon collection of crystal -113
  Photon* Photon_114; //! photon collection of crystal -114 
  Photon* Photon_121; //! photon collection of crystal -121
  Photon* Photon_122; //! photon collection of crystal -122
  Photon* Photon_123; //! photon collection of crystal -123
  Photon* Photon_124; //! photon collection of crystal -124

  Photon* Photon_201; //! photon collection of crystal -201
  Photon* Photon_202; //! photon collection of crystal -202
  Photon* Photon_203; //! photon collection of crystal -203
  Photon* Photon_204; //! photon collection of crystal -204 
  Photon* Photon_211; //! photon collection of crystal -211
  Photon* Photon_212; //! photon collection of crystal -212
  Photon* Photon_213; //! photon collection of crystal -213
  Photon* Photon_214; //! photon collection of crystal -214
  Photon* Photon_221; //! photon collection of crystal -221
  Photon* Photon_222; //! photon collection of crystal -222
  Photon* Photon_223; //! photon collection of crystal -223
  Photon* Photon_224; //! photon collection of crystal -224	

  G4int m_index[48]; //! index for number of photons in single crystals

  G4int m_destroyFlag[6]; //! how many photons were destroyed by what process -> see photon destroyFlags

public:  
  G4double totEDep101,totEDep102,totEDep103,totEDep104,totEDep111,totEDep112,totEDep113,totEDep114,totEDep121,totEDep122,totEDep123,totEDep124;
  G4double totEDep201,totEDep202,totEDep203,totEDep204,totEDep211,totEDep212,totEDep213,totEDep214,totEDep221,totEDep222,totEDep223,totEDep224;

  G4double totEDep_101,totEDep_102,totEDep_103,totEDep_104,totEDep_111,totEDep_112,totEDep_113,totEDep_114,totEDep_121,totEDep_122,totEDep_123,totEDep_124;
  G4double totEDep_201,totEDep_202,totEDep_203,totEDep_204,totEDep_211,totEDep_212,totEDep_213,totEDep_214,totEDep_221,totEDep_222,totEDep_223,totEDep_224;

private:
  G4int wrongCalculation;
  static G4double timeOffset; //! Simulation Start Time (BOOST = 648 +-8 ns) 

  static std::vector<double> fX;
  static std::vector<double> fY;

  static G4double integral; //! integral of single photoelectron signal [0,80] -> needs to be changed if gain and/or collection factor is changed
  static G4bool waveFound;
  static G4bool initFunction; 
};

#endif
