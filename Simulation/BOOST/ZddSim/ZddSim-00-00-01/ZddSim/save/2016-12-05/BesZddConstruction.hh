//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Descpirtion: Muon Chamber as a concrete class
//             it's better to define an envelope then position it in BES
//Author:  Youzy      Peking University      mail: youzy@hep.pku.cn
//Created: Nov, 2003
//Comment:
//---------------------------------------------------------------------------//
//
#ifndef BesZddConstruction_h
#define BesZddConstruction_h 1

#include "BesSubdetector.hh"
#include "BesSensitiveDetector.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "globals.hh"

class G4Box;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4UniformMagField;
//class SensitiveDetector;

class BesZddConstruction : public BesSubdetector
{  
public:
  
  BesZddConstruction();
  virtual ~BesZddConstruction();

  void Construct( G4LogicalVolume* logicBes );

  const G4VPhysicalVolume* GetPhysicalZdd(){return physicZdd;};

  //! Check in which crystal given position is in     
  static G4int CheckPosition(G4double x, G4double y, G4double z);
  static G4double GetDistanceToPMT(G4double z);

 public:
     
  void SetCrystalMaterial (G4String);     
      
  void SetMagField(G4double);
     
  void UpdateGeometry();
     
public:
                      
//  G4double GetWorldSizeX() {return WorldSizeX;}; 
//  G4double GetWorldSizeY() {return WorldSizeY;};
//  G4double GetWorldSizeZ() {return WorldSizeZ;};

  static G4ThreeVector GetLeftUpperCrystalBox() {return slUpperCrystalBox;};
  static G4ThreeVector GetLeftLowerCrystalBox() {return slLowerCrystalBox;};

  static G4ThreeVector GetRightUpperCrystalBox() {return srUpperCrystalBox;};
  static G4ThreeVector GetRightLowerCrystalBox() {return srLowerCrystalBox;};
     
  static G4double GetCrystalBoxSizeX() {return sCrystalBoxSizeX;};
  static G4double GetCrystalBoxSizeY() {return sCrystalBoxSizeY;};
  static G4double GetCrystalBoxSizeZ() {return sCrystalBoxSizeZ;};

  static G4double GetCrystalX() {return sCrystalX;};
  static G4double GetCrystalY() {return sCrystalY;};
  static G4double GetCrystalZ() {return sCrystalZ;};

  G4Material* GetCrystalMaterial() {return CrystalMaterial;};
     
  BesSensitiveDetector* GetSD() {return SD;};

  void SetNOfCrystals(G4int n){nOfCrystals = n;};
  G4int GetNOfCrystals(){return nOfCrystals;};


private:
  G4Material*        CrystalMaterial;
  BesSensitiveDetector* SD;
     
  G4Material*        defaultMaterial;
 
  static const G4double sCrystalBoxSizeX; 
  static const G4double sCrystalBoxSizeY; 
  static const G4double sCrystalBoxSizeZ;
     
  static const G4double sCrystalX;
  static const G4double sCrystalY;
  static const G4double sCrystalZ;
     
  static const G4ThreeVector slUpperCrystalBox;
  static const G4ThreeVector slLowerCrystalBox;

  static const G4ThreeVector srUpperCrystalBox;
  static const G4ThreeVector srLowerCrystalBox;

  G4double           WorldSizeX;
  G4double           WorldSizeY;
  G4double           WorldSizeZ;
     
  G4int nOfCrystals;
     
//  G4Box*             solidWorld;    //pointer to the solid world
  G4VSolid*             solidWorld;    //pointer to the solid world
  G4LogicalVolume*   logicZdd;    //pointer to the logical World
  G4VPhysicalVolume* physicZdd;    //pointer to the physical World

  G4LogicalVolume*   crystal_log;
  G4VPhysicalVolume* crystal_phys;

private:
  void DefineMaterials();
  
};
#endif

   
     





























