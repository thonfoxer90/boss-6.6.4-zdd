//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description: Sensitive detector of Zdd
//Author:  Youzy      Peking University      mail: youzy@hep.pku.cn
//Created: Nov, 2003
//Modified:
//Comment:
//---------------------------------------------------------------------------//
//
//
// $Id: BesZddSD.hh,v 1.5 2009/08/25 13:33:54 xieyg Exp $
// GEANT4 tag $Name: ZddSim-00-01-01 $
//

#ifndef BesZddSD_h
#define BesZddSD_h 1

#include "G4Event.hh"
#include "G4TrajectoryContainer.hh"
#include "BesSensitiveDetector.hh"
#include "BesZddHit.hh"
#include "BesZddConstruction.hh"
#include <vector>
//#include "BesZddEfficiency.hh"
#include "BesSensitiveManager.hh"
#include "BesTruthTrack.hh"

using namespace std;

class G4Step;
class G4HCofThisEvent;
class G4Svc;

class BesZddSD : public BesSensitiveDetector
{
public:
  BesZddSD(G4String, BesZddConstruction*);
  ~BesZddSD();
  
  void Initialize(G4HCofThisEvent*);
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);
  void EndOfEvent(G4HCofThisEvent*);

  //  void BeginOfTruthEvent(const G4Event*);
  //  void EndOfTruthEvent(const G4Event*);

  void IncreaseTotenergyDeposit( G4double edep) { TotEnergyDeposit += edep; };
  G4double GetTotEnergyDeposit() { return TotEnergyDeposit; };
    
private:

  G4Svc* m_G4Svc;
  
  BesZddHitsCollection* ZddHitsCollection;
  //  BesZddHitsCollection* ZddHitList; // for MC-Truth?!
  G4int m_trackIndex;
  vector<G4int> m_trackIndexes;
  BesZddConstruction*  detector;
  //  G4int*   HitID;
  G4double TotEnergyDeposit;
  
};

#endif












