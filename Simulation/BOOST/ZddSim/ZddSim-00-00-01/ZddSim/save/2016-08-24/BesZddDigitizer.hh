//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description:
//Author: Youzy
//Created: Mar, 2004
//Modified:
//Comment:
//---------------------------------------------------------------------------//
// $Id: BesZddDigitizer.hh

#ifndef BesZddDigitizer_h
#define BesZddDigitizer_h 1

#include "GaudiKernel/NTuple.h"

#include "G4VDigitizerModule.hh"
#include "BesZddDigi.hh"
#include "globals.hh"
#include "TFile.h"
#include "TTree.h"
#include "BesSensitiveManager.hh"
#include "BesTruthTrack.hh"
#include "G4ThreeVector.hh"

class G4Svc;

class BesZddDigitizer : public G4VDigitizerModule
{
public:
  BesZddDigitizer(G4String modName);
  ~BesZddDigitizer();
  
  //necessary digi collection object must be constructed and set to 
  //G4DCofThisEvent by StoreDigiCollection protected method.
  virtual void Digitize();
private:
  void Initialize();
  G4double Smear(G4double var);

private:
  BesZddDigisCollection* m_besZddDigisCollection;    
 
  G4bool smearFlag;
  
  G4bool impactReco;

  G4Svc* m_G4Svc;

  TFile* fFile;
  //TFile* chargeChannelFile;
  //TTree* tEDepChargeChannel;
  TTree* tImpactReco;

  G4double m_eDep;
  G4int m_chargeChannel;

  G4double m_eDep101;
  G4double m_eDep102;
  G4double m_eDep103;
  G4double m_eDep104;
  G4double m_eDep111;
  G4double m_eDep112;
  G4double m_eDep113;
  G4double m_eDep114;
  G4double m_eDep121;
  G4double m_eDep122;
  G4double m_eDep123;
  G4double m_eDep124;

  G4double m_eDep201;
  G4double m_eDep202;
  G4double m_eDep203;
  G4double m_eDep204;
  G4double m_eDep211;
  G4double m_eDep212;
  G4double m_eDep213;
  G4double m_eDep214;
  G4double m_eDep221;
  G4double m_eDep222;
  G4double m_eDep223;
  G4double m_eDep224;

  G4int m_chargeChannel101;
  G4int m_chargeChannel102;
  G4int m_chargeChannel103;
  G4int m_chargeChannel104;
  G4int m_chargeChannel111;
  G4int m_chargeChannel112;
  G4int m_chargeChannel113;
  G4int m_chargeChannel114;
  G4int m_chargeChannel121;
  G4int m_chargeChannel122;
  G4int m_chargeChannel123;
  G4int m_chargeChannel124;

  G4int m_chargeChannel201;
  G4int m_chargeChannel202;
  G4int m_chargeChannel203;
  G4int m_chargeChannel204;
  G4int m_chargeChannel211;
  G4int m_chargeChannel212;
  G4int m_chargeChannel213;
  G4int m_chargeChannel214;
  G4int m_chargeChannel221;
  G4int m_chargeChannel222;
  G4int m_chargeChannel223;
  G4int m_chargeChannel224;
  
  G4double m_xImpact;
  G4double m_yImpact;
	G4double m_zImpact;
	
	//G4ThreeVector m_momImpact; //Seg fault even when G4ThreeVector included
	
	G4double m_PxImpact;
	G4double m_PyImpact;
	G4double m_PzImpact;
	
	G4int m_trackID;
	
	int m_Ntracks;
	
	int m_event;
	/*
	G4double m_xImpacts[50];
  G4double m_yImpacts[50];
	G4double m_zImpacts[50];
	//int 	 		m_pids[50];
	G4double 	 		m_pids[50];
	*/
	
	double m_xImpacts[50];
  double m_yImpacts[50];
	double m_zImpacts[50];
	//int 	 		m_pids[50];
	double 	 		m_pids[50];
	
  /*
  NTuple::Tuple* m_tupleZdd;
  NTuple::Item<double> m_partID;
  NTuple::Item<double> m_detectorID;
  NTuple::Item<double> m_crystalNo;
  NTuple::Item<double> m_ChargeChannel;
  NTuple::Item<double> m_TimeChannel;
  */
};


#endif

