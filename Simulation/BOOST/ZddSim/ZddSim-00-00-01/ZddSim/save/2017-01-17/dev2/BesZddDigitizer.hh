//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description:
//Author: Youzy
//Created: Mar, 2004
//Modified:
//Comment:
//---------------------------------------------------------------------------//
// $Id: BesZddDigitizer.hh

#ifndef BesZddDigitizer_h
#define BesZddDigitizer_h 1

#include "GaudiKernel/NTuple.h"

#include "G4VDigitizerModule.hh"
#include "BesZddDigi.hh"
#include "BesZddDigit.hh"
#include "globals.hh"
#include "TFile.h"
#include "TTree.h"
#include "BesSensitiveManager.hh"
#include "BesTruthTrack.hh"
#include "G4ThreeVector.hh"

class G4Svc;

class BesZddDigitizer : public G4VDigitizerModule
{
public:
  BesZddDigitizer(G4String modName);
  ~BesZddDigitizer();
  
  //necessary digi collection object must be constructed and set to 
  //G4DCofThisEvent by StoreDigiCollection protected method.
  virtual void Digitize();
private:
  void Initialize();
  G4double Smear(G4double var);

private:
  BesZddDigisCollection* m_besZddDigisCollection;    
 
  G4bool smearFlag;
  
  G4bool impactReco;

  G4Svc* m_G4Svc;

  TFile* fFile;
  //TFile* chargeChannelFile;
  //TTree* tEDepChargeChannel;
  TTree* tImpactReco;

  G4double m_eDep;
  G4int m_chargeChannel;
	/*
  G4double m_eDep101Right;
  G4double m_eDep102Right;
  G4double m_eDep103Right;
  G4double m_eDep104Right;
  G4double m_eDep111Right;
  G4double m_eDep112Right;
  G4double m_eDep113Right;
  G4double m_eDep114Right;
  G4double m_eDep121Right;
  G4double m_eDep122Right;
  G4double m_eDep123Right;
  G4double m_eDep124Right;

  G4double m_eDep201Right;
  G4double m_eDep202Right;
  G4double m_eDep203Right;
  G4double m_eDep204Right;
  G4double m_eDep211Right;
  G4double m_eDep212Right;
  G4double m_eDep213Right;
  G4double m_eDep214Right;
  G4double m_eDep221Right;
  G4double m_eDep222Right;
  G4double m_eDep223Right;
  G4double m_eDep224Right;
	
  G4int m_chargeChannel101Right;
  G4int m_chargeChannel102Right;
  G4int m_chargeChannel103Right;
  G4int m_chargeChannel104Right;
  G4int m_chargeChannel111Right;
  G4int m_chargeChannel112Right;
  G4int m_chargeChannel113Right;
  G4int m_chargeChannel114Right;
  G4int m_chargeChannel121Right;
  G4int m_chargeChannel122Right;
  G4int m_chargeChannel123Right;
  G4int m_chargeChannel124Right;

  G4int m_chargeChannel201Right;
  G4int m_chargeChannel202Right;
  G4int m_chargeChannel203Right;
  G4int m_chargeChannel204Right;
  G4int m_chargeChannel211Right;
  G4int m_chargeChannel212Right;
  G4int m_chargeChannel213Right;
  G4int m_chargeChannel214Right;
  G4int m_chargeChannel221Right;
  G4int m_chargeChannel222Right;
  G4int m_chargeChannel223Right;
  G4int m_chargeChannel224Right;
  
	G4double m_eDep101Left;
  G4double m_eDep102Left;
	//Seg fault past this point
  G4double m_eDep103Left; 
  G4double m_eDep104Left;
  G4double m_eDep111Left;
  G4double m_eDep112Left;
  G4double m_eDep113Left;
  G4double m_eDep114Left;
  G4double m_eDep121Left;
  G4double m_eDep122Left;
  G4double m_eDep123Left;
  G4double m_eDep124Left;
	
  G4double m_eDep201Left;
  G4double m_eDep202Left;
  G4double m_eDep203Left;
  G4double m_eDep204Left;
  G4double m_eDep211Left;
  G4double m_eDep212Left;
  G4double m_eDep213Left;
  G4double m_eDep214Left;
  G4double m_eDep221Left;
  G4double m_eDep222Left;
  G4double m_eDep223Left;
  G4double m_eDep224Left;
	
  G4int m_chargeChannel101Left;
  G4int m_chargeChannel102Left;
  G4int m_chargeChannel103Left;
  G4int m_chargeChannel104Left;
  G4int m_chargeChannel111Left;
  G4int m_chargeChannel112Left;
  G4int m_chargeChannel113Left;
  G4int m_chargeChannel114Left;
  G4int m_chargeChannel121Left;
  G4int m_chargeChannel122Left;
  G4int m_chargeChannel123Left;
  G4int m_chargeChannel124Left;

  G4int m_chargeChannel201Left;
  G4int m_chargeChannel202Left;
  G4int m_chargeChannel203Left;
  G4int m_chargeChannel204Left;
  G4int m_chargeChannel211Left;
  G4int m_chargeChannel212Left;
  G4int m_chargeChannel213Left;
  G4int m_chargeChannel214Left;
  G4int m_chargeChannel221Left;
  G4int m_chargeChannel222Left;
  G4int m_chargeChannel223Left;
  G4int m_chargeChannel224Left;
	*/
	
	double m_eDep101Right;
  double m_eDep102Right;
  double m_eDep103Right;
  double m_eDep104Right;
  double m_eDep111Right;
  double m_eDep112Right;
  double m_eDep113Right;
  double m_eDep114Right;
  double m_eDep121Right;
  double m_eDep122Right;
  double m_eDep123Right;
  double m_eDep124Right;

  double m_eDep201Right;
  double m_eDep202Right;
  double m_eDep203Right;
  double m_eDep204Right;
  double m_eDep211Right;
  double m_eDep212Right;
  double m_eDep213Right;
  double m_eDep214Right;
  double m_eDep221Right;
  double m_eDep222Right;
  double m_eDep223Right;
  double m_eDep224Right;
	
  int m_chargeChannel101Right;
  int m_chargeChannel102Right;
  int m_chargeChannel103Right;
  int m_chargeChannel104Right;
  int m_chargeChannel111Right;
  int m_chargeChannel112Right;
  int m_chargeChannel113Right;
  int m_chargeChannel114Right;
  int m_chargeChannel121Right;
  int m_chargeChannel122Right;
  int m_chargeChannel123Right;
  int m_chargeChannel124Right;

  int m_chargeChannel201Right;
  int m_chargeChannel202Right;
  int m_chargeChannel203Right;
  int m_chargeChannel204Right;
  int m_chargeChannel211Right;
  int m_chargeChannel212Right;
  int m_chargeChannel213Right;
  int m_chargeChannel214Right;
  int m_chargeChannel221Right;
  int m_chargeChannel222Right;
  int m_chargeChannel223Right;
  int m_chargeChannel224Right;
  
	double m_eDep101Left;
  double m_eDep102Left;
	//Seg fault past this point
  double m_eDep103Left; 
  double m_eDep104Left;
  double m_eDep111Left;
  double m_eDep112Left;
  double m_eDep113Left;
  double m_eDep114Left;
  double m_eDep121Left;
  double m_eDep122Left;
  double m_eDep123Left;
  double m_eDep124Left;
	
  double m_eDep201Left;
  double m_eDep202Left;
  double m_eDep203Left;
  double m_eDep204Left;
  double m_eDep211Left;
  double m_eDep212Left;
  double m_eDep213Left;
  double m_eDep214Left;
  double m_eDep221Left;
  double m_eDep222Left;
  double m_eDep223Left;
  double m_eDep224Left;
	
  int m_chargeChannel101Left;
  int m_chargeChannel102Left;
  int m_chargeChannel103Left;
  int m_chargeChannel104Left;
  int m_chargeChannel111Left;
  int m_chargeChannel112Left;
  int m_chargeChannel113Left;
  int m_chargeChannel114Left;
  int m_chargeChannel121Left;
  int m_chargeChannel122Left;
  int m_chargeChannel123Left;
  int m_chargeChannel124Left;

  int m_chargeChannel201Left;
  int m_chargeChannel202Left;
  int m_chargeChannel203Left;
  int m_chargeChannel204Left;
  int m_chargeChannel211Left;
  int m_chargeChannel212Left;
  int m_chargeChannel213Left;
  int m_chargeChannel214Left;
  int m_chargeChannel221Left;
  int m_chargeChannel222Left;
  int m_chargeChannel223Left;
  int m_chargeChannel224Left;
	
  G4double m_xImpact;
  G4double m_yImpact;
	G4double m_zImpact;
	
	//G4ThreeVector m_momImpact; //Seg fault even when G4ThreeVector included
	
	G4double m_PxImpact;
	G4double m_PyImpact;
	G4double m_PzImpact;
	
	G4int m_trackID;
	
	int m_Ntracks;
	
	int m_event;
	/*
	G4double m_xImpacts[50];
  G4double m_yImpacts[50];
	G4double m_zImpacts[50];
	//int 	 		m_pids[50];
	G4double 	 		m_pids[50];
	*/
	
	double m_xImpacts[50];
  double m_yImpacts[50];
	double m_zImpacts[50];
	//int 	 		m_pids[50];
	//double 	 		m_pids[50]; //Commented out because its declaration produces segfault at execution
	//Stack size seems very limited
	
	double m_xImpactElec;
	double m_yImpactElec;
	
	double m_xImpactPositron;
	double m_yImpactPositron;
	
  /*
  NTuple::Tuple* m_tupleZdd;
  NTuple::Item<double> m_partID;
  NTuple::Item<double> m_detectorID;
  NTuple::Item<double> m_crystalNo;
  NTuple::Item<double> m_ChargeChannel;
  NTuple::Item<double> m_TimeChannel;
  */
	
	BesZddDigit *m_aDigit;
};


#endif

