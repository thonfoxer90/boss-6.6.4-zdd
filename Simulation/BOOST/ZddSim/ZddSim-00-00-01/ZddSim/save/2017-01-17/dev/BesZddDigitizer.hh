//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description:
//Author: Youzy
//Created: Mar, 2004
//Modified:
//Comment:
//---------------------------------------------------------------------------//
// $Id: BesZddDigitizer.hh

#ifndef BesZddDigitizer_h
#define BesZddDigitizer_h 1

#include "GaudiKernel/NTuple.h"

#include "G4VDigitizerModule.hh"
#include "BesZddDigi.hh"
#include "BesZddDigit.hh"
#include "globals.hh"
#include "TFile.h"
#include "TTree.h"
#include "BesSensitiveManager.hh"
#include "BesTruthTrack.hh"
#include "G4ThreeVector.hh"

class G4Svc;

class BesZddDigitizer : public G4VDigitizerModule
{
public:
  BesZddDigitizer(G4String modName);
  ~BesZddDigitizer();
  
  //necessary digi collection object must be constructed and set to 
  //G4DCofThisEvent by StoreDigiCollection protected method.
  virtual void Digitize();
private:
  void Initialize();
  G4double Smear(G4double var);

private:
  BesZddDigisCollection* m_besZddDigisCollection;    
 
  G4bool smearFlag;
  
  G4bool impactReco;

  G4Svc* m_G4Svc;

  TFile* fFile;
  //TFile* chargeChannelFile;
  //TTree* tEDepChargeChannel;
  TTree* tImpactReco;

  G4double m_eDep;
  G4int m_chargeChannel;

  G4double m_eDep101Right;
  G4double m_eDep102Right;
  G4double m_eDep103Right;
  G4double m_eDep104Right;
  G4double m_eDep111Right;
  G4double m_eDep112Right;
  G4double m_eDep113Right;
  G4double m_eDep114Right;
  G4double m_eDep121Right;
  G4double m_eDep122Right;
  G4double m_eDep123Right;
  G4double m_eDep124Right;

  G4double m_eDep201Right;
  G4double m_eDep202Right;
  G4double m_eDep203Right;
  G4double m_eDep204Right;
  G4double m_eDep211Right;
  G4double m_eDep212Right;
  G4double m_eDep213Right;
  G4double m_eDep214Right;
  G4double m_eDep221Right;
  G4double m_eDep222Right;
  G4double m_eDep223Right;
  G4double m_eDep224Right;
	
  G4int m_chargeChannel101Right;
  G4int m_chargeChannel102Right;
  G4int m_chargeChannel103Right;
  G4int m_chargeChannel104Right;
  G4int m_chargeChannel111Right;
  G4int m_chargeChannel112Right;
  G4int m_chargeChannel113Right;
  G4int m_chargeChannel114Right;
  G4int m_chargeChannel121Right;
  G4int m_chargeChannel122Right;
  G4int m_chargeChannel123Right;
  G4int m_chargeChannel124Right;

  G4int m_chargeChannel201Right;
  G4int m_chargeChannel202Right;
  G4int m_chargeChannel203Right;
  G4int m_chargeChannel204Right;
  G4int m_chargeChannel211Right;
  G4int m_chargeChannel212Right;
  G4int m_chargeChannel213Right;
  G4int m_chargeChannel214Right;
  G4int m_chargeChannel221Right;
  G4int m_chargeChannel222Right;
  G4int m_chargeChannel223Right;
  G4int m_chargeChannel224Right;
  
  G4double m_xImpact;
  G4double m_yImpact;
	G4double m_zImpact;
	
	//G4ThreeVector m_momImpact; //Seg fault even when G4ThreeVector included
	
	G4double m_PxImpact;
	G4double m_PyImpact;
	G4double m_PzImpact;
	
	G4int m_trackID;
	
	int m_Ntracks;
	
	int m_event;
	/*
	G4double m_xImpacts[50];
  G4double m_yImpacts[50];
	G4double m_zImpacts[50];
	//int 	 		m_pids[50];
	G4double 	 		m_pids[50];
	*/
	
	double m_xImpacts[50];
  double m_yImpacts[50];
	double m_zImpacts[50];
	//int 	 		m_pids[50];
	double 	 		m_pids[50];
	
	double m_xImpactElec;
	double m_yImpactElec;
	
	double m_xImpactPositron;
	double m_yImpactPositron;
	
  /*
  NTuple::Tuple* m_tupleZdd;
  NTuple::Item<double> m_partID;
  NTuple::Item<double> m_detectorID;
  NTuple::Item<double> m_crystalNo;
  NTuple::Item<double> m_ChargeChannel;
  NTuple::Item<double> m_TimeChannel;
  */
	
	BesZddDigit *m_aDigit;
};


#endif

