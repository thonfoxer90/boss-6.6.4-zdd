# echo "Setting ZddSim ZddSim-00-00-01 in /home/bgarillo/boss-6.6.4-ZDD/Simulation/BOOST"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cluster/him/bes3/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=ZddSim -version=ZddSim-00-00-01 -path=/home/bgarillo/boss-6.6.4-ZDD/Simulation/BOOST  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

