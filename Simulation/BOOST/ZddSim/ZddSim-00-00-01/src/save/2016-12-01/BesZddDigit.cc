//
// $Id: BesZddDigit

#include "BesZddDigit.hh"
#include "BesZddConstruction.hh"
#include "G4ios.hh"
#include "G4Box.hh"
#include "G4VSolid.hh"
#include "strstream"
#include "G4ThreeVector.hh"
#include "G4UnitsTable.hh"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TGraph.h"
#include "TObject.h"
#include "TTree.h"
//#include "TRandom3.h"

#include "Randomize.hh"
#include <CLHEP/Random/Randomize.h>

//! Write out histos?
//#define HISTOS

//! initialize static variables/objects

G4double BesZddDigit::thetaLoss = 0.47201/1.570797; // some photons are lost because of total inner reflection
G4double BesZddDigit::transL = 400.*cm; // length after which szintillation photons are attenuated to 1/e

G4int    BesZddDigit::numberOfSiPMs = 2; //4 //9
G4int    BesZddDigit::SiPMPixels = 3560 * numberOfSiPMs; //each SiPM has 3600 pixels, 1% in firing state and not available => ~3560 pixels
G4double BesZddDigit::mismatchLoss = numberOfSiPMs * 9./100. * 0.615;//0.8; // active area of SiPMs at readout end of scintillator: missmatchLoss = (SiPMArea/totalReadoutAreaOfScintillator)*FillFactor 
G4double BesZddDigit::angularAcceptanceLoss = 1.;// no data available here;
G4double BesZddDigit::QELoss = 1.;// no PDE taken into account here, PDE is taken into account when calculating sum output of SiPM (=final number of fired pixels)
G4double BesZddDigit::SiPMQE = 0.5; //50% QE of SiPM


G4bool   BesZddDigit::waveFound = false;
G4double BesZddDigit::timeOffset = 0.*ns; //648.*ns;

// comment this in for realistic output!
// photon constant: ~18 ph/MeV energy deposition can be seen by normal PMT, QE ~ 20%, no missmatch loss... keep in mind attenuation, which needs to be considered during simulation and affects mostly those photons, which have a longer propagation length inside the crystal...

//G4int BesZddDigit::photonConstant = int(1350. * BesZddDigit::mismatchLoss * BesZddDigit::angularAcceptanceLoss * BesZddDigit::QELoss * BesZddDigit::thetaLoss);
G4int BesZddDigit::photonConstant = int(200. * BesZddDigit::mismatchLoss * BesZddDigit::angularAcceptanceLoss * BesZddDigit::QELoss * BesZddDigit::thetaLoss);

TRandom3* BesZddDigit::random = new TRandom3();

// for testing purposes only!!!
//G4int BesZddDigit::photonConstant = 5000;

G4bool UseSiPM = true; // use SiPM simulation(=true) or usual PMT(=false)

G4bool debug = false; // debug mode on?
G4int BesZddDigit::eventCounter = 0;

TH2D* BesZddDigit::hEDepChargeChannel = new TH2D("hEDepChargeChannel","",1400,0.,700.,4000,0,20000);

/*
BesZddDigit::tEDepChargeChannel->Branch("eDep",&BesZddDigit::treeEDep);
BesZddDigit::tEDepChargeChannel->Branch("chargeChannel",&BesZddDigit::treeChargeChannel);
BesZddDigit::tEDepChargeChannel->Branch("crystal_no",&BesZddDigit::treeCrystalNo);
*/

TProfile* hProfile =  new TProfile("hProfile","",400,0.,400.);//! random generators
TH1D* hADCsmear = new TH1D("hADCsmear","",100.,0.,0.);
TH2D* hADCpoint_vs_smear = new TH2D("hADCpoint_vs_smear", "", 100,0,16, 100,81000.,86000.); 

//hADCwave->SetMarkerStyle(2);
/*
G4int const BesZddDigit::m_seed1 = 12345; 
G4int const BesZddDigit::m_seed2 = 23456;
G4int const BesZddDigit::m_seed3 = 34567;
G4int const BesZddDigit::m_seed4 = 45678;
G4int const BesZddDigit::m_seed5 = 56789;

TRandom* BesZddDigit::uniRand1 = new TRandom(BesZddDigit::m_seed1);
TRandom* BesZddDigit::uniRand2 = new TRandom(BesZddDigit::m_seed2);
TRandom* BesZddDigit::uniRand3 = new TRandom(BesZddDigit::m_seed3);
TRandom* BesZddDigit::uniRand4 = new TRandom(BesZddDigit::m_seed4);
TRandom* BesZddDigit::uniRand5 = new TRandom(BesZddDigit::m_seed5);
*/

G4bool BesZddDigit::initFunction = true;
std::vector<double> BesZddDigit::fX;
std::vector<double> BesZddDigit::fY;

//! integral of single photoelectron function
G4double BesZddDigit::integral = 95.71250795;

/*
G4double peFunction(G4double* x,G4double* par)
{
  G4double tau = 6; //! rise time in [ns]
  G4double t = x[0];
  return t*t*TMath::Exp(-t*t/(tau*tau));
}
*/
G4double peFunction(G4double x)
{
  G4double tau = 6; //! rise time in [ns]
  G4double t = x;
  return t*t*TMath::Exp(-t*t/(tau*tau));
}

//unchanged C5 capacity, 72.3V bias, 5V power supply
/*
G4double pixelFunction(G4double x)
{
  // x in ns!
  G4double par[6] = {-7.618,-0.3659,-0.3486,-7.959,-0.03347,10.19};
  if(x<0) return 0;
  else if(x<9) return par[0] * (1 - TMath::Exp(par[1]*(x-par[2])));
  else if(x<=70)return par[3]*TMath::Exp(par[4]*(x-par[5]))+1;
  else return 0;
}
*/

 //capcity C5+22nF, 72.5V bias, 5V power supply
G4double pixelFunction(G4double x)                                                                                                                                          {
  G4double penalty = 0.3; //=1 at 72.5V bias, reduce waveform height by changing SiPM bias voltage
  // x in ns!
  G4double par[6] = {-9.459,-0.2478,0.02189,-7.744,-0.01978,11.94};
  if(x<0) return 0;
  else if(x<9) return penalty * par[0] * (1 - TMath::Exp(par[1]*(x-par[2])));
  else if(x<250)return penalty * par[3]*TMath::Exp(par[4]*(x-par[5]));
  else return 0;
}

G4double peTotal(G4double x, G4double integral)
{
  G4double gain = 1.;
  G4double colFac = 1.;

  G4double value = -1. * gain * colFac * peFunction(x) / integral ;
  return value;
}

//scintillator function
G4double f1(G4double* x, G4double* par)
{
  G4double R = 3./97.;
  G4double tau1 = 6.4;
  G4double tau2 = 6.5;
  G4double tau3 = 30.4;
  
  return 1./(1.+R) * ( (exp(-x[0]/tau2) - exp(-x[0]/tau1))/(tau2 - tau1) + R/tau3 * exp(-x[0]/tau3));
}

BesZddDigit::BesZddDigit()
{
 
  if( initFunction )
 {
  for(int i = 0; i <= 2000; i++)
    {
      BesZddDigit::fX.push_back( i/100. ); 
 //     std::cout << "BesZddDigit: fX[" << i << "]: " << fX.at(i) << std::endl;
      BesZddDigit::fY.push_back( peTotal(i/100.,BesZddDigit::integral) ); 
 //     std::cout << "BesZddDigit: fY[" << i << "]: " << fY.at(i) << std::endl;
    }
    initFunction = false;
 }

	totEDep101 = 0;totEDep102 = 0;totEDep103 = 0;totEDep104 = 0;totEDep111 = 0;totEDep112 = 0;totEDep113 = 0;totEDep114 = 0;totEDep121 = 0;totEDep122 = 0;totEDep123 = 0;totEDep124 = 0;
	totEDep201 = 0;totEDep202 = 0;totEDep203 = 0;totEDep204 = 0;totEDep211 = 0;totEDep212 = 0;totEDep213 = 0;totEDep214 = 0;totEDep221 = 0;totEDep222 = 0;totEDep223 = 0;totEDep224 = 0;

	totEDep_101 = 0;totEDep_102 = 0;totEDep_103 = 0;totEDep_104 = 0;totEDep_111 = 0;totEDep_112 = 0;totEDep_113 = 0;totEDep_114 = 0;totEDep_121 = 0;totEDep_122 = 0;totEDep_123 = 0;totEDep_124 = 0;
	totEDep_201 = 0;totEDep_202 = 0;totEDep_203 = 0;totEDep_204 = 0;totEDep_211 = 0;totEDep_212 = 0;totEDep_213 = 0;totEDep_214 = 0;totEDep_221 = 0;totEDep_222 = 0;totEDep_223 = 0;totEDep_224 = 0;
	
	wrongCalculation = 0;	
	
	m_TrackID = 0;
     	m_CrystalNo = 0;
    // total energy depostion per event
       	m_totEDep = 0.;	
	m_firstHit = true;

	m_preStep = G4ThreeVector(-99.,-99.,-99.);
	m_postStep = G4ThreeVector(-99.,-99.,-99.);

	//! initialize pointers with 0
	Photon101 = NULL;
	Photon102 = NULL;
	Photon103 = NULL;	
	Photon104 = NULL;
	Photon111 = NULL;
	Photon112 = NULL;
	Photon113 = NULL;	
	Photon114 = NULL;
	Photon121 = NULL;
	Photon122 = NULL;
	Photon123 = NULL;	
	Photon124 = NULL;
	Photon201 = NULL;
	Photon202 = NULL;
	Photon203 = NULL;	
	Photon204 = NULL;
	Photon211 = NULL;
	Photon212 = NULL;
	Photon213 = NULL;	
	Photon214 = NULL;
	Photon221 = NULL;
	Photon222 = NULL;
	Photon223 = NULL;	
	Photon224 = NULL;	

	Photon_101 = NULL;
	Photon_102 = NULL;
	Photon_103 = NULL;	
	Photon_104 = NULL;
	Photon_111 = NULL;
	Photon_112 = NULL;
	Photon_113 = NULL;	
	Photon_114 = NULL;
	Photon_121 = NULL;
	Photon_122 = NULL;
	Photon_123 = NULL;	
	Photon_124 = NULL;
	Photon_201 = NULL;
	Photon_202 = NULL;
	Photon_203 = NULL;	
	Photon_204 = NULL;
	Photon_211 = NULL;
	Photon_212 = NULL;
	Photon_213 = NULL;	
	Photon_214 = NULL;
	Photon_221 = NULL;
	Photon_222 = NULL;
	Photon_223 = NULL;	
	Photon_224 = NULL;

#ifdef HISTOS
	/* G4double x_hist_l = BesZddConstruction::GetUpperCrystalBox().x()/cm-BesZddConstruction::GetCrystalBoxSizeX()/cm/2.+1.*BesZddConstruction::GetCrystalX()/cm;
  G4double x_hist_u = BesZddConstruction::GetUpperCrystalBox().x()/cm-BesZddConstruction::GetCrystalBoxSizeX()/cm/2.+2.*BesZddConstruction::GetCrystalX()/cm;

  G4double y_hist_l = BesZddConstruction::GetUpperCrystalBox().y()/cm-BesZddConstruction::GetCrystalBoxSizeY()/cm/2.+1.*BesZddConstruction::GetCrystalY()/cm;
  G4double y_hist_u = BesZddConstruction::GetUpperCrystalBox().y()/cm-BesZddConstruction::GetCrystalBoxSizeY()/cm/2.+2.*BesZddConstruction::GetCrystalY()/cm;
  
  G4double z_hist_l = BesZddConstruction::GetUpperCrystalBox().z()/cm-BesZddConstruction::GetCrystalBoxSizeZ()/cm/2.;
  G4double z_hist_u = BesZddConstruction::GetUpperCrystalBox().z()/cm-BesZddConstruction::GetCrystalBoxSizeZ()/cm/2.+1.*BesZddConstruction::GetCrystalZ()/cm;
  
  G4int bins = 50;
  
  hXYZpositions = new TH3D("hXYZpositions","",bins,x_hist_l,x_hist_u, bins,y_hist_l,y_hist_u, bins,z_hist_l,z_hist_u);	
  hXYZpositions = new TH3D("hXYZpositions","",bins,z_hist_l,z_hist_u, bins,x_hist_l,x_hist_u, bins,y_hist_l,y_hist_u);	
  hXYZpositions->GetXaxis()->SetTitle("z");
  hXYZpositions->GetYaxis()->SetTitle("x"); 
  hXYZpositions->GetZaxis()->SetTitle("y");
	*/
//   hTimeTraj = new TH1D("hTimeTraj","",40,2.,3.);
//   hTimeEmit = new TH1D("hTimeEmit","",2000,0,50);
//   hTimeProp = new TH1D("hTimeProp","",40,0.2,1.2);
//   hTimeTrans = new TH1D("hTimeTrans","",160,3,7);
//   hTemp1 = new TH1D("hTemp1", "",3000,5,80);
//   hTemp2 = new TH1D("hTemp2", "", 400,0,400);
#endif
//   hADCwave = new TH1D("hADCwave","",350,0.,350.);
//   hTemp1 = new TH1D("hTemp1", "", 800,0.,80.);
//   hTemp2 = new TH1D("hTemp2", "", 350,0.,350.);  
//   hTimeTraj = new TH1D("hTimeTraj","",10,11.,12.);
//   hTimeEmit = new TH1D("hTimeEmit","",500,0,50);
//   hTimeProp = new TH1D("hTimeProp","",15,0.,1.5);
  //hTemp1 = new TH1D("hTemp1", "",3000,0,80);
  //  hTemp2 = new TH1D("hTemp1", "",3000,0,80);

 fTimeEmit = new TF1("fTimeEmit",f1,0,55.,0);

  for(int i = 0; i<=5; i++) m_destroyFlag[i] = 0;
  for(int i = 0; i<48; i++) m_index[i] = 0;

}

BesZddDigit::~BesZddDigit() 
{
/*
    TH2D* chargeChannel = new TH2D("chargeChannel","",400,300.,700.,3000,0.,30000.);
    chargeChannel->Fill(totEDep, Photon112->GetChargeChannel() );
    TFile f("digitization.root","recreate");
    chargeChannel->Write();
    f.Close();
    delete chargeChannel;

    
//  std::cout << "BesZddDigit::~BesZddDigit() : EDep in Crystal 112 [MeV] : " << totEDep << std::endl; 
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 0 :             " << m_destroyFlag[0] << std::endl;
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 1 :             " << m_destroyFlag[1] << std::endl; 
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 2 :             " << m_destroyFlag[2] << std::endl;
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 3 :             " << m_destroyFlag[3] << std::endl;
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 4 :             " << m_destroyFlag[4] << std::endl;
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 5 :             " << m_destroyFlag[5] << std::endl;
//  std::cout << "----------------------------------------------------------------------------" << std::endl;
eventCounter++;
// photonsPerMeV += (m_destroyFlag[0]/totEDep112);
// std::cout << "BesZddDigit::~BesZddDigit() : #Photons/MeV (Crystal 112): " << m_destroyFlag[0]/totEDep112 << std::endl;
// std::cout << "BesZddDigit::~BesZddDigit() : Average #Photons/MeV (Crystal 112): " << photonsPerMeV/eventCounter << std::endl;
// std::cout << "############################################################################" << std::endl;
 
 if(eventCounter%100 == 0)
 {
   TFile f("EDepChargeChannel.root","recreate");
   hEDepChargeChannel->Write();
   f.Close();
 }  
*/
 #ifdef HISTOS
  TFile f("digitization.root","recreate");
  //  chargeChannel->Write();
//  std::cout << "test1" << std::endl;
//  hXYZpositions->Write();
//  std::cout << "test1a" << std::endl;
//   hTimeTraj->Write();
//  std::cout << "test1b" << std::endl;
//   hTimeEmit->Write();
//  std::cout << "test1c" << std::endl;
//   hTimeProp->Write();
//  std::cout << "test1d" << std::endl;
//   hTimeTrans->Write();
//  std::cout << "test1e" << std::endl;
//  fPE->Write();
  //  fTot->Write();
//    std::cout << "test2" << std::endl;
//   hTemp1->Write();
//    std::cout << "test3" << std::endl;
//   hTemp2->Write();
//   std::cout << "test4" << std::endl;
//  fPE->Write();
//  fPMT->Write();
  f.Close();
//    std::cout << "test5" << std::endl;
//  std::cout << "totalHits:          " << totalHits << std::endl;
//  std::cout << "wrong Calculations: " << wrongCalculation << std::endl;
//  delete ChargeChannel;
//   delete hXYZpositions;
//   delete hTimeTraj;
//   delete hTimeEmit;
//   delete hTimeProp;
//   delete hTimeTrans;
//   delete hTemp1;
//   delete hTemp2;
  delete fTimeEmit;
  delete fTot;
  delete fPE;
//  delete fPMT;
#endif
  /*
      TFile f("digitization.root","recreate");

      f.cd();
      hTimeTraj->Write();
      hTimeEmit->Write();
      hTimeProp->Write();
      hTemp1->Write();
      hTemp2->Write();
      hEDepChargeChannel->Write();
    // hEDepChargeChannel->Write("",TObject::kOverwrite);  
  //    hProfile->Write();
      hADCwave->Write();
    
//    hADCsmear->Write();
  //    hADCpoint_vs_smear->Write();
    f.Close();
*/  
 if(Photon101)  delete Photon101; 
 if(Photon102)  delete Photon102; 
 if(Photon103)  delete Photon103; 
 if(Photon104)  delete Photon104; 
 if(Photon111)  delete Photon111; 
 if(Photon112)  delete Photon112; 
 if(Photon113)  delete Photon113; 
 if(Photon114)  delete Photon114; 
 if(Photon121)  delete Photon121;
 if(Photon122)  delete Photon122; 
 if(Photon123)  delete Photon123; 
 if(Photon124)  delete Photon124; 

 if(Photon201)  delete Photon201; 
 if(Photon202)  delete Photon202; 
 if(Photon203)  delete Photon203; 
 if(Photon204)  delete Photon204; 
 if(Photon211)  delete Photon211;
 if(Photon212)  delete Photon212;
 if(Photon213)  delete Photon213;
 if(Photon214)  delete Photon214; 
 if(Photon221)  delete Photon221;
 if(Photon222)  delete Photon222; 
 if(Photon223)  delete Photon223; 
 if(Photon224)  delete Photon224;

 if(Photon_101)  delete Photon_101;
 if(Photon_102)  delete Photon_102; 
 if(Photon_103)  delete Photon_103; 
 if(Photon_104)  delete Photon_104; 
 if(Photon_111)  delete Photon_111; 
 if(Photon_112)  delete Photon_112; 
 if(Photon_113)  delete Photon_113; 
 if(Photon_114)  delete Photon_114; 
 if(Photon_121)  delete Photon_121;
 if(Photon_122)  delete Photon_122;
 if(Photon_123)  delete Photon_123; 
 if(Photon_124)  delete Photon_124;

 if(Photon_201)  delete Photon_201; 
 if(Photon_202)  delete Photon_202; 
 if(Photon_203)  delete Photon_203;
 if(Photon_204)  delete Photon_204; 
 if(Photon_211)  delete Photon_211; 
 if(Photon_212)  delete Photon_212; 
 if(Photon_213)  delete Photon_213;
 if(Photon_214)  delete Photon_214; 
 if(Photon_221)  delete Photon_221;
 if(Photon_222)  delete Photon_222; 
 if(Photon_223)  delete Photon_223; 
 if(Photon_224)  delete Photon_224;	
/*
  delete uniRand1;
  delete uniRand2;  
  delete uniRand3;
  delete uniRand4;  
  delete uniRand5;
  */
}

void BesZddDigit::SetHit(BesZddHit* hit)
{
 
  m_pHit = hit;
 
//  totalHits++;
//  G4int trackID = m_pHit->GetTrackID();   
  G4int preCrystalNo = m_pHit->GetPreCrystalNo();  	
  //    std::cout << "BesZddDigit.cc :: SetHit : preCrystalNo : " << preCrystalNo << std::endl;
  G4int postCrystalNo = m_pHit->GetPostCrystalNo();  			   
//if(preCrystalNo != postCrystalNo) std::cout << "edep between different crystals!!! " << std::endl;
  G4int pdg = m_pHit->GetPDGCode();				
  G4double edep = m_pHit->GetEDep(); 
  //  std::cout << "edep per hit " << m_pHit->GetEDep() << " MeV " <<  std::endl;
  //G4ThreeVector momentum = m_pHit->GetMomentum();			
  G4double preTime = m_pHit->GetPreTime();				
  G4double postTime = m_pHit->GetPostTime();
  G4double deltaT = postTime - preTime;
  
  G4ThreeVector prePos = m_pHit->GetPrePos();				
  G4ThreeVector postPos = m_pHit->GetPostPos();

  if(m_firstHit == true)
    {
      m_preStep = prePos;
      m_postStep = postPos;

      m_xImpact = m_pHit->GetXImpact();
      m_yImpact = m_pHit->GetYImpact();
			m_zImpact = (m_pHit->GetPostPos()).z();
	
			m_momImpact = m_pHit->GetMomentum();
			m_PxImpact  = m_momImpact.x();
			m_PyImpact  = m_momImpact.y();
			m_PzImpact  = m_momImpact.z();

			printf("m_momImpact.x() %f m_momImpact.y() %f m_momImpact.z() %f \n", m_momImpact.x(),m_momImpact.y(),m_momImpact.z());
      if( m_xImpact == -99. || m_yImpact == -99.) std::cout << "strange: first hit xImpact/yImpact  is -99. ?!?!?!? " << std::endl;
      //      else std::cout << "ZddSim::BesZddDigit::SetHit xImpact: " << m_xImpact << "\t, yImpact: " << m_yImpact << std::endl;
      m_firstHit = false;
      //      std::cout << "ZddSim :: BesZddDigit.cc : preStep Hit: x y z : " << prePos.x() << "\t" << prePos.y() << "\t" << prePos.z() << std::endl; 
      //      std::cout << "ZddSim :: BesZddDigit.cc : postStepHit: x y z : " << postPos.x() << "\t" << postPos.y() << "\t" << postPos.z() << std::endl;
    }


  G4double deltaX = postPos.x()/cm - prePos.x()/cm;
  G4double deltaY = postPos.y()/cm - prePos.y()/cm;
  G4double deltaZ = postPos.z()/cm - prePos.z()/cm;  
  
//if(postCrystalNo == 112)  hXYZpositions->Fill(postPos.z()/cm,postPos.x()/cm,postPos.y()/cm);
  if(debug)  std::cout << "BesZddDigit.cc::SetHit : PrePos X,Y,Z: " << prePos.x()/cm << ", " <<  prePos.y()/cm << ", "  << prePos.z()/cm << std::endl;
  if(debug)  std::cout << "BesZddDigit.cc::SetHit : PostPos X,Y,Z: " << postPos.x()/cm << ", " <<  postPos.y()/cm << ", "  << postPos.z()/cm << std::endl;
  
  if(debug) std::cout << "BesZddDigit.cc::SetHit : crystalNo PreStep: " << preCrystalNo << ", calculated crystal: " << BesZddConstruction::CheckPosition(prePos.x()/cm,prePos.y()/cm,prePos.z()/cm) << std::endl;
  if(debug)  std::cout << "BesZddDigit.cc::SetHit : crystalNo PostStep: " << postCrystalNo << ", calculated crystal: " << BesZddConstruction::CheckPosition(postPos.x()/cm,postPos.y()/cm,postPos.z()/cm) << std::endl;
//  if(BesZddConstruction::CheckPosition(prePos.x(),prePos.y(),prePos.z()) < 0) wrongCalculation++;
//  if(BesZddConstruction::CheckPosition(postPos.x(),postPos.y(),postPos.z()) < 0) wrongCalculation++;

/*
  if(preCrystalNo != BesZddConstruction::CheckPosition(prePos.x(),prePos.y(),prePos.z())) 
  {
//   wrongCalculation++;
//    std::cout << "BesZddDigit.cc :: error: real preCrystal " << preCrystalNo << " != calculated crystal " << BesZddConstruction::CheckPosition(prePos.x(),prePos.y(),prePos.z()) << std::endl;
  }
  if(postCrystalNo != BesZddConstruction::CheckPosition(postPos.x(),postPos.y(),postPos.z()))
  {
//   wrongCalculation++;
//  std::cout << "BesZddDigit.cc :: error: real postCrystal " << postCrystalNo << " != calculated crystal " << BesZddConstruction::CheckPosition(postPos.x(),postPos.y(),postPos.z()) << std::endl << "================================================================================================" << std::endl;
  }
*/  
  G4int numberOfPhotons = G4int(edep*BesZddDigit::photonConstant);
  if(debug)  std::cout << "BesZddDigit:: SetHit : number of photons: " << numberOfPhotons << std::endl;
  switch(preCrystalNo)
  {
//! part 1
    case -101:
    totEDep_101+= edep;
      break;
    case -102:
    totEDep_102+= edep;
      break;
    case -103:
    totEDep_103+= edep;
      break;
    case -104:
    totEDep_104+= edep;
      break;      
    case -111:
    totEDep_111+= edep;
      break;
    case -112:
    totEDep_112+= edep;
      break;
    case -113:
    totEDep_113+= edep;
      break;
    case -114:
    totEDep_114+= edep;
      break;    
    case -121:
    totEDep_121+= edep;
      break;
    case -122:
    totEDep_122+= edep;
      break;
    case -123:
    totEDep_123+= edep;
      break;
    case -124:
    totEDep_124+= edep;
      break;          

    case -201:
    totEDep_201+= edep;
      break;
    case -202:
    totEDep_202+= edep;
    //  std::cout << "BesZddDigit.cc : SetHit :: totEDep_202 : " << totEDep_202 << std::endl;
      break;
    case -203:
    totEDep_203+= edep;
      break;
    case -204:
    totEDep_204+= edep;
      break;      
    case -211:
    totEDep_211+= edep;
      break;
    case -212:
    totEDep_212+= edep;
      break;
    case -213:
    totEDep_213+= edep;
      break;
    case -214:
    totEDep_214+= edep;
      break;    
    case -221:
    totEDep_221+= edep;
      break;
    case -222:
    totEDep_222+= edep;
      break;
    case -223:
    totEDep_223+= edep;
      break;
    case -224:
    totEDep_224+= edep;
      break;

//! part 2 
    case 101:
    totEDep101+= edep;
      break;
    case 102:
    totEDep102+= edep;
      break;
    case 103:
    totEDep103+= edep;
      break;
    case 104:
    totEDep104+= edep;
      break;      
    case 111:
    totEDep111+= edep;
      break;
    case 112:
    totEDep112+= edep;
      break;
    case 113:
    totEDep113+= edep;
      break;
    case 114:
    totEDep114+= edep;
      break;    
    case 121:
    totEDep121+= edep;
      break;
    case 122:
    totEDep122+= edep;
      break;
    case 123:
    totEDep123+= edep;
      break;
    case 124:
    totEDep124+= edep;
      break;          

    case 201:
    totEDep201+= edep;
      break;
    case 202:
    totEDep202+= edep;
    //    std::cout << "BesZddDigit.cc :: SetHit : edep, totEDep 202 : " << edep << "/t" << totEDep202 << std::endl;
      break;
    case 203:
    totEDep203+= edep;
      break;
    case 204:
    totEDep204+= edep;
      break;      
    case 211:
    totEDep211+= edep;
      break;
    case 212:
    totEDep212+= edep;
      break;
    case 213:
    totEDep213+= edep;
      break;
    case 214:
    totEDep214+= edep;
      break;    
    case 221:
    totEDep221+= edep;
      break;
    case 222:
    totEDep222+= edep;
      break;
    case 223:
    totEDep223+= edep;
      break;
    case 224:
    totEDep224+= edep;
      break;    
    default:
      std::cout << "BesZddDigit.cc :: SetHit : Failure!! Strange Crystal Number!! " << std::endl;
      break;
  } // end switch(preCrystalNo)
  //      std::cout << "BesZddDigit.cc : SetHit : numberOfPhotons : " << numberOfPhotons << std::endl;  
  
for(int i = 0; i < numberOfPhotons; i++)
{
  // if(i == 0)std::cout << "1last iteration in for(numberOfPhotons)" << std::endl; 

  double theta = G4UniformRand()*0.47201; //=TMath::Pi()/2.
  //  double theta = uniRand1->Uniform(TMath::Pi()/2.);
  //! loss through theta angle
  /*  if( theta > asin(1.000293/2.2) )  
      {
      m_destroyFlag[1]++;
      }
      else
      {
      //! loss through mismatch crystal<->PMT 
      if(G4UniformRand() > 0.8 )
      //  if(uniRand3->Uniform(1) > 0.8 )
      {
      m_destroyFlag[3]++;
      }
      else
      {
      //! loss through PMT angular acceptance
      //      if(uniRand4->Uniform(1) > 0.66)
      if(G4UniformRand() > 0.66)
      {  
      m_destroyFlag[4]++;
      }
      else
      {
      //! loss through  PMT quantum efficiency
      //	if(uniRand5->Uniform(1) > 0.21)
      if(G4UniformRand() > 0.21)
      {
      m_destroyFlag[5]++;
      }
      else
      {
*/  
	   G4double random = G4UniformRand();
	   G4double x = prePos.x()/cm + random*deltaX; //std::cout << "PreX: " << prePos.x()/cm<< ", PostX: " << postPos.x()/cm << ", x : " << x << std::endl;
	   G4double y = prePos.y()/cm + random*deltaY; //std::cout << "PreY: " << prePos.y()/cm<< ", PostY: " << postPos.y()/cm << ", y : " << y << std::endl;
	   G4double z = prePos.z()/cm + random*deltaZ; //std::cout << "PreZ: " << prePos.z()/cm<< ", PostZ: " << postPos.z()/cm << ", z : " << z << std::endl;  
	   G4double t_traj = preTime + random*deltaT;
    
	   if(debug)
	     {
	       std::cout<< "BesZddDigit.cc:: SetHit : x:      " << x << std::endl;
	       std::cout<< "BesZddDigit.cc:: SetHit : y:      " << y << std::endl;
	       std::cout<< "BesZddDigit.cc:: SetHit : z:      " << z << std::endl;
	       std::cout<< "BesZddDigit.cc:: SetHit : theta:  " << theta << std::endl;
	       std::cout<< "BesZddDigit.cc:: SetHit : t_traj: " << t_traj << std::endl;
	     }//if(debug)   
 //     std::cout << asin(1.000293/2.2) << std::endl;
	   //if(i == 0)	   std::cout << " BesZddConstruction::CheckPosition(x,y,z): " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
		   // if(i == 0)std::cout << "2last iteration in for(numberOfPhotons)" << std::endl;
	      
    switch( BesZddConstruction::CheckPosition(x,y,z) )
    {
//! part 1
      //! upper crystals
      case -101:
	if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_101 == NULL)
	{  
	  Photon_101 = new Photon();
          Photon_101->SetCrystalNo(-101);
	}
	m_index[24]++;
	Photon_101->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta	
	BesZddDigit::PhotonCalculations(m_index[24], *Photon_101 );
	  break;
      case -102:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_102 == NULL)
	{  
	  Photon_102 = new Photon();
          Photon_102->SetCrystalNo(-102);
	}
	m_index[25]++;
	Photon_102->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[25], *Photon_102 );
	break;
      case -103:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_103 == NULL)
	{  
	  Photon_103 = new Photon();
          Photon_103->SetCrystalNo(-103);
	}
	m_index[26]++;
	Photon_103->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[26], *Photon_103 );
	break;
      case -104:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_104 == NULL) 
	{  
	  Photon_104 = new Photon();
          Photon_104->SetCrystalNo(-104);
	}
	m_index[27]++;
	Photon_104->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[27], *Photon_104 );
	break;
      case -111:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_111 == NULL)
	{  
	  Photon_111 = new Photon();
          Photon_111->SetCrystalNo(-111);
	}
	m_index[28]++;
	Photon_111->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[28], *Photon_111 );
	break; 
      case -112:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_112 == NULL) 
	{  
	  Photon_112 = new Photon();
          Photon_112->SetCrystalNo(-112);
	}
#ifdef HISTOS
	//	hXYZpositions->Fill(z,x,y);
	//	hTimeTraj->Fill(t_traj);
#endif
	m_index[29]++;
	Photon_112->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations( m_index[29], *Photon_112 );
	break;
      case -113:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_113 == NULL)
	{  
	  Photon_113 = new Photon();
          Photon_113->SetCrystalNo(-113);
	}
	m_index[30]++;
	Photon_113->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[30], *Photon_113 );
	break;
      case -114:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_114 == NULL) 
	{  
	  Photon_114 = new Photon();
          Photon_114->SetCrystalNo(-114);
	}
	m_index[31]++;
	Photon_114->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[31], *Photon_114 );
	break;
      case -121:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_121 == NULL)
	{  
	  Photon_121 = new Photon();
          Photon_121->SetCrystalNo(-121);
	}	
	m_index[32]++;
	Photon_121->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[32], *Photon_121 );
	break;
      case -122:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_122 == NULL)
	{  
	  Photon_122 = new Photon();
          Photon_122->SetCrystalNo(-122);
	}
	m_index[33]++;
	Photon_122->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[33], *Photon_122 );
	break;
      case -123:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_123 == NULL)
	{  
	  Photon_123 = new Photon();
          Photon_123->SetCrystalNo(-123);
	}
	m_index[34]++;
	Photon_123->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[34], *Photon_123 );
	break;
      case -124:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_124 == NULL)
	{  
	  Photon_124 = new Photon();
          Photon_124->SetCrystalNo(-124);
	}
	m_index[35]++;
	Photon_124->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[35], *Photon_124 );
	break;
	
      //! lower crystals
      case -201:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_201 == NULL)
	{  
	  Photon_201 = new Photon();
          Photon_201->SetCrystalNo(-201);
	}
	m_index[36]++;
	Photon_201->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta	
	BesZddDigit::PhotonCalculations(m_index[36], *Photon_201 );
	  break;
      case -202:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_202 == NULL)
	{  
	  Photon_202 = new Photon();
          Photon_202->SetCrystalNo(-202);
	}
	m_index[37]++;
	Photon_202->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[37], *Photon_202 );
	break;
      case -203:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_203 == NULL)
	{  
	  Photon_203 = new Photon();
          Photon_203->SetCrystalNo(-203);
	}
	m_index[38]++;
	Photon_203->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[38], *Photon_203 );
	break;
      case -204:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_204 == NULL) 
	{  
	  Photon_204 = new Photon();
          Photon_204->SetCrystalNo(-204);
	}
	m_index[39]++;
	Photon_204->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[39], *Photon_204 );
	break;
      case -211:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_211 == NULL)
	{  
	  Photon_211 = new Photon();
          Photon_211->SetCrystalNo(-211);
	}
	m_index[40]++;
	Photon_211->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[40], *Photon_211 );
	break;
      case -212:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_212 == NULL) 
	{  
	  Photon_212 = new Photon();
          Photon_212->SetCrystalNo(-212);
	}
	m_index[41]++;
	Photon_212->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[41], *Photon_212 );
	break;
      case -213:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_213 == NULL)
	{  
	  Photon_213 = new Photon();
          Photon_213->SetCrystalNo(-213);
	}
	m_index[42]++;
	Photon_213->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[42], *Photon_213 );
	break;
      case -214:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_214 == NULL) 
	{  
	  Photon_214 = new Photon();
          Photon_214->SetCrystalNo(-214);
	}
	m_index[43]++;
	Photon_214->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[43], *Photon_214 );
	break;
      case -221:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_221 == NULL)
	{  
	  Photon_221 = new Photon();
          Photon_221->SetCrystalNo(-221);
	}
	m_index[44]++;
	Photon_221->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[44], *Photon_221 );
	break;
      case -222:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_222 == NULL)
	{  
	  Photon_222 = new Photon();
          Photon_222->SetCrystalNo(-222);
	}
	m_index[45]++;
	Photon_222->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[45], *Photon_222 );
	break;
      case -223:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_223 == NULL)
	{  
	  Photon_223 = new Photon();
          Photon_223->SetCrystalNo(-223);
	}
	m_index[46]++;
	Photon_223->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[46], *Photon_223 );
	break;
      case -224:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon_224 == NULL)
	{  
	  Photon_224 = new Photon();
          Photon_224->SetCrystalNo(-224);
	}
	m_index[47]++;
	Photon_224->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[47], *Photon_224 );
	break;

//! part 2      
      //! upper crystals
      case 101:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon101 == NULL)
	{  
	  Photon101 = new Photon();
          Photon101->SetCrystalNo(101);
	}
	m_index[0]++;
	Photon101->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta	
	BesZddDigit::PhotonCalculations(m_index[0], *Photon101 );
	  break;
      case 102:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon102 == NULL)
	{  
	  Photon102 = new Photon();
          Photon102->SetCrystalNo(102);
	}
	m_index[1]++;
	Photon102->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[1], *Photon102 );
	break;
      case 103:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon103 == NULL)
	{  
	  Photon103 = new Photon();
          Photon103->SetCrystalNo(103);
	}
	m_index[2]++;
	Photon103->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[2], *Photon103 );
	break;
      case 104:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon104 == NULL) 
	{  
	  Photon104 = new Photon();
          Photon104->SetCrystalNo(104);
	}
	m_index[3]++;
	Photon104->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[3], *Photon104 );
	break;
      case 111:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon111 == NULL)
	{  
	  Photon111 = new Photon();
          Photon111->SetCrystalNo(111);
	}
	m_index[4]++;
	Photon111->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[4], *Photon111 );
	break; 
      case 112:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon112 == NULL) 
	{  
          if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal 112 1 " << std::endl;
	  Photon112 = new Photon();
	  if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal 112 2 " << std::endl;
          Photon112->SetCrystalNo(112);
          if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal 112 3 " << std::endl;
	  
	}
#ifdef HISTOS
	//	hXYZpositions->Fill(z,x,y);
	//	hTimeTraj->Fill(t_traj);
#endif
	m_index[5]++;
	//	hTimeTraj->Fill(t_traj);
	if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal 112 4 " << std::endl;
	Photon112->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal 112 5 " << std::endl;
	BesZddDigit::PhotonCalculations( m_index[5], *Photon112 );
	break;
      case 113:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon113 == NULL)
	{  
	  Photon113 = new Photon();
          Photon113->SetCrystalNo(113);
	}
	m_index[6]++;
	Photon113->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[6], *Photon113 );
	break;
      case 114:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon114 == NULL) 
	{  
	  Photon114 = new Photon();
          Photon114->SetCrystalNo(114);
	}
	m_index[7]++;
	Photon114->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[7], *Photon114 );
	break;
      case 121:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon121 == NULL)
	{  
	  Photon121 = new Photon();
          Photon121->SetCrystalNo(121);
	}	
	m_index[8]++;
	Photon121->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[8], *Photon121 );
	break;
      case 122:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon122 == NULL)
	{  
	  Photon122 = new Photon();
          Photon122->SetCrystalNo(122);
	}
	m_index[9]++;
	Photon122->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[9], *Photon122 );
	break;
      case 123:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon123 == NULL)
	{  
	  Photon123 = new Photon();
          Photon123->SetCrystalNo(123);
	}
	m_index[10]++;
	Photon123->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[10], *Photon123 );
	break;
      case 124:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon124 == NULL)
	{  
	  Photon124 = new Photon();
          Photon124->SetCrystalNo(124);
	}
	m_index[11]++;
	Photon124->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[11], *Photon124 );
	break;
	
      //! lower crystals
      case 201:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon201 == NULL)
	{  
	  Photon201 = new Photon();
          Photon201->SetCrystalNo(201);
	}
	m_index[12]++;
	Photon201->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta	
	BesZddDigit::PhotonCalculations(m_index[12], *Photon201 );
	  break;
      case 202:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon202 == NULL)
	{  
	  Photon202 = new Photon();
          Photon202->SetCrystalNo(202);
	}
	m_index[13]++;
	Photon202->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[13], *Photon202 );
	break;
      case 203:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon203 == NULL)
	{  
	  Photon203 = new Photon();
          Photon203->SetCrystalNo(203);
	}
	m_index[14]++;
	Photon203->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[14], *Photon203 );
	break;
      case 204:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon204 == NULL) 
	{  
	  Photon204 = new Photon();
          Photon204->SetCrystalNo(204);
	}
	m_index[15]++;
	Photon204->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[15], *Photon204 );
	break;
      case 211:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon211 == NULL)
	{  
	  Photon211 = new Photon();
          Photon211->SetCrystalNo(211);
	}
	m_index[16]++;
	Photon211->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[16], *Photon211 );
	break;
      case 212:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon212 == NULL) 
	{  
	  Photon212 = new Photon();
          Photon212->SetCrystalNo(212);
	}
	m_index[17]++;
	Photon212->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[17], *Photon212 );
	break;
      case 213:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon213 == NULL)
	{  
	  Photon213 = new Photon();
          Photon213->SetCrystalNo(213);
	}
	m_index[18]++;
	Photon213->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[18], *Photon213 );
	break;
      case 214:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon214 == NULL) 
	{  
	  Photon214 = new Photon();
          Photon214->SetCrystalNo(214);
	}
	m_index[19]++;
	Photon214->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[19], *Photon214 );
	break;
      case 221:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon221 == NULL)
	{  
	  Photon221 = new Photon();
          Photon221->SetCrystalNo(221);
	}
	m_index[20]++;
	Photon221->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[20], *Photon221 );
	break;
      case 222:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon222 == NULL)
	{  
	  Photon222 = new Photon();
          Photon222->SetCrystalNo(222);
	}
	m_index[21]++;
	Photon222->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[21], *Photon222 );
	break;
      case 223:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon223 == NULL)
	{  
	  Photon223 = new Photon();
          Photon223->SetCrystalNo(223);
	}
	m_index[22]++;
	Photon223->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[22], *Photon223 );
	break;
      case 224:
        if(debug) std::cout << "BesZddDigit.cc::SetHit : switch, crystal : " << BesZddConstruction::CheckPosition(x,y,z) << std::endl;
	if(Photon224 == NULL)
	{  
	  Photon224 = new Photon();
          Photon224->SetCrystalNo(224);
	}
	m_index[23]++;
	Photon224->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[23], *Photon224 );
	break;
      default:
	std::cout << "very strange: no crystal hit between two hits!! / wrong crystal No !!! " << std::endl;
	break;
    }// end switch
    if(debug)  std::cout << "BesZddDigit::SetHit after switch command!" << std::endl;
    //  if(i ==0)std::cout << "last iteration in for(numberOfPhotons) end" << std::endl;
 }// end for(numberOfPhotons)
 if(debug) std::cout << "BesZddDigit::SetHit after for(numberOfPhotons) command!" << std::endl;

}// end SetHit(..)



void BesZddDigit::PhotonCalculations(G4int& a, Photon& photon )
{
  if(debug)    std::cout << "entering photoncalculations" << std::endl;
  G4int i = a-1;
  /* 
    std::cout << "BesZddDigit::PhotonCalculations : crystalNo:      " << photon.GetCrystalNo() << std::endl;  

    std::cout << "BesZddDigit::PhotonCalculations : m_index i:      " << i << std::endl;
    std::cout << "BesZddDigit::PhotonCalculations : right m_index : " << photon.GetPosXSize()-1 << std::endl;

    std::cout << "BesZddDigit::PhotonCalculations : PosXSize :      " << photon.GetPosXSize() << std::endl;
    std::cout << "BesZddDigit::PhotonCalculations : PosYsize :      " << photon.GetPosYSize() << std::endl;
    std::cout << "BesZddDigit::PhotonCalculations : PosZsize :      " << photon.GetPosZSize() << std::endl;    
    std::cout << "BesZddDigit::PhotonCalculations : ThetaSize :     " << photon.GetThetaSize() << std::endl << "================================================" << std::endl;      
    
    std::cout << "BesZddDigit::PhotonCalculations : PosX :          " << photon.GetPosX( photon.GetPosXSize()-1 ) << std::endl;
    std::cout << "BesZddDigit::PhotonCalculations : PosY :          " << photon.GetPosY( photon.GetPosXSize()-1 ) << std::endl;
    std::cout << "BesZddDigit::PhotonCalculations : PosZ :          " << photon.GetPosZ( photon.GetPosXSize()-1 ) << std::endl;    
    std::cout << "BesZddDigit::PhotonCalculations : Theta :         " << photon.GetTheta( photon.GetPosXSize()-1 ) << std::endl << "================================================" << std::endl;    
*/  

#ifdef HISTOS
  	  if( photon.GetCrystalNo() == 112 )  hTimeEmit->Fill(emitTime); //! Fill histo only for crystal 112
#endif
          if(debug) std::cout << "BesZddDigit::PhotonCalculations : crystalNo: " << photon.GetCrystalNo() << ", i: " << i << ", vectorsize: " << photon.GetThetaSize() << std::endl;
	  G4double distance = BesZddConstruction::GetDistanceToPMT( photon.GetPosZ(i) ); 
//	  G4double distance = 10.;
	  if(debug) std::cout << "BesZddDigit::PhotonCalculations : PosZ :        " << photon.GetPosZ(i) << std::endl;
	  if(debug) std::cout << "BesZddDigit::PhotonCalculations : distance :    " << distance << std::endl;
	  G4double propLength = distance/cos( photon.GetTheta(i) );
//	  G4double propLength = 10.;
	  //	  if( photon.GetCrystalNo() == 112 )  hTemp1->Fill(propLength); //! Fill histo only for crystal 112
	  //	  std::cout << "BesZddDigit::PhotonCalculations : Theta :       " << photon.GetTheta(i) << std::endl;
	  if(debug) std::cout << "BesZddDigit::PhotonCalculations : propLength :  " << propLength << std::endl;
  
//	  G4double attenuation = exp(-propLength/0.89); //! probability of photon loss because of attenuation //! 0.89 = radiation length of PbWO4
	  G4double attenuation = exp(-propLength/BesZddDigit::transL); //! probability of photon loss because of attenuation //! increased radiation length	  
	  //G4double attenuation = 1.;
	  //std::cout << "BesZddDigit :: PhotonCalculations: warning: attenuation OFF !! " << std::endl; 
//	  G4double attenuation = 0.8;
	  if(debug)	  std::cout << "BesZddDigit::PhotonCalculations : attenuation : " << attenuation << std::endl << "=========================================" << std::endl;

	  //! loss through attenuation
	  //	  std::cout << "BesZddDigit::PhotonCalculations : s " << s << std::endl;
	  if( G4UniformRand() > attenuation) 
//	  if( uniRand2->Uniform(1) > attenuation) 
	  {
	    m_destroyFlag[2]++;
	    photon.SetEmitTime(0.);
	    photon.SetPropTime(0.);
	    photon.SetTransTime(0.);
	    if(debug)
	      {
		std::cout << "BesZddDigit :: PhotonCalculations1 : index i         : " << i << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations1 : posXSize        : " << photon.GetPosXSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations1 : posYSize        : " << photon.GetPosYSize() << std::endl; 
		std::cout << "BesZddDigit :: PhotonCalculations1 : posZSize        : " << photon.GetPosZSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations1 : ThetaSize       : " << photon.GetThetaSize() << std::endl; 
		std::cout << "BesZddDigit :: PhotonCalculations1 : TrajTimeSize    : " << photon.GetTrajTimeSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations1 : EmitTimeSize    : " << photon.GetEmitTimeSize() << std::endl;    
		std::cout << "BesZddDigit :: PhotonCalculations1 : PropTimeSize    : " << photon.GetPropTimeSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations1 : TransTimeSize   : " << photon.GetTransTimeSize() << std::endl;
	     }//if(debug)
	    if( !(photon.EraseVectorElement()) ) std::cout << "BesZddDigit::PhotonCalculations : problem with erasing vector element!!" << std::endl;
	    a += -1;
	    if(debug)
	      {
		std::cout << "BesZddDigit :: PhotonCalculations2 : index i         : " << i << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations2 : posXSize        : " << photon.GetPosXSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations2 : posYSize        : " << photon.GetPosYSize() << std::endl; 
		std::cout << "BesZddDigit :: PhotonCalculations2 : posZSize        : " << photon.GetPosZSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations2 : ThetaSize       : " << photon.GetThetaSize() << std::endl; 
		std::cout << "BesZddDigit :: PhotonCalculations2 : TrajTimeSize    : " << photon.GetTrajTimeSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations2 : EmitTimeSize    : " << photon.GetEmitTimeSize() << std::endl;    
		std::cout << "BesZddDigit :: PhotonCalculations2 : PropTimeSize    : " << photon.GetPropTimeSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations2 : TransTimeSize   : " << photon.GetTransTimeSize() << std::endl; 
	      }//if(debug)
	  } // if(attenuation)
	  else
	  {
	    if(photon.GetCrystalNo() == 112) m_destroyFlag[0]++;
	    
	    G4double t_prop = propLength/(2.99792458*10./2.2); //! propagation time in ns!!
	    photon.SetPropTime(t_prop);	 
	    G4double emitTime = GetRandomTimeEmit();
	  //  G4double emitTime = G4UniformRand();
	    photon.SetEmitTime(emitTime);

//	        hTemp1->Fill(0);
// do not set trans time here, because it is a push_back function
//	    double t_trans = G4RandGauss::shoot(5.,0.3);//! mean ~ 5ns, sigma ~0.3ns
//	    photon.SetTransTime(0.);

//#ifdef HISTOS
	    //	    hTemp1->Fill( photon.GetTrajTime(i) + photon.GetEmitTime(i)+ photon.GetPropTime(i) );

	if( photon.GetCrystalNo() == 112 )
	{
	  //	  hTimeEmit->Fill(emitTime);

	  //  hTimeTrans->Fill(t_trans); //! Fill histo only for crystal 112
	     //  hTemp2->Fill(propLength); //! Fill histo only for crystal 112
	  //	  hTimeProp->Fill(t_prop); //! Fill histo only for crystal 112
	  
	}
	//#endif


	if(debug)
	  {
	    std::cout << "BesZddDigit :: PhotonCalculations3 : index i         : " << i << std::endl;
	    std::cout << "BesZddDigit :: PhotonCalculations3 : posXSize        : " << photon.GetPosXSize() << std::endl;
	    std::cout << "BesZddDigit :: PhotonCalculations3 : posYSize        : " << photon.GetPosYSize() << std::endl; 
	    std::cout << "BesZddDigit :: PhotonCalculations3 : posZSize        : " << photon.GetPosZSize() << std::endl;
	    std::cout << "BesZddDigit :: PhotonCalculations3 : ThetaSize       : " << photon.GetThetaSize() << std::endl; 
	    std::cout << "BesZddDigit :: PhotonCalculations3 : TrajTimeSize    : " << photon.GetTrajTimeSize() << std::endl;
	    std::cout << "BesZddDigit :: PhotonCalculations3 : EmitTimeSize    : " << photon.GetEmitTimeSize() << std::endl;    
	    std::cout << "BesZddDigit :: PhotonCalculations3 : PropTimeSize    : " << photon.GetPropTimeSize() << std::endl;
	    std::cout << "BesZddDigit :: PhotonCalculations3 : TransTimeSize   : " << photon.GetTransTimeSize() << std::endl;  
	  }//if(debug)    
    } // else attenuation
	  //	  std::cout << "end of BesZddDigit :: PhotonCalculations" << std::endl;
}


void BesZddDigit::EndOfEvent()
{
  /*
  fPE = new TF1("fPE",peTotal,0,20,1);
  fPE->SetParameter(0,0.);

  for(int i = 0; i<2000; i++) //! save function values in vector
  {
    fX.push_back( i/100.);
    fY.push_back( fPE->Eval(i/100.) );  
  }
  */
//  delete fPE;
  /*
  for(int i = 0; i<2000; i++) //! save function values in vector
  {
    hTemp2->Fill( fX.at(i), fY.at(i) );
  }*/ 

  //  std::cout << "BesZddDigit.cc :: EndOfEvent : totEDep 202 : " << totEDep202 << std::endl;
  //charge channel is the sum of all sample heights of the ADC, which runs at a frequency of 62.5MHz, collecting a sample every 16ns (2*8), therefore, the charge channel can be d\ifferent depending on which part of the wave the ADC will collect data, it is important, that all channels have the same smearing for one simulated event, because the input of each ADC channel should be read out synchronizedly

    double adc_smear = random->Uniform(8.); //smearing of ADC 
  // double adc_smear = 0.;
  if(UseSiPM)
    {
      if(Photon_101!=NULL) CalculateSiPMoutput( *Photon_101, adc_smear );
      if(Photon_102!=NULL) CalculateSiPMoutput( *Photon_102, adc_smear );
      if(Photon_103!=NULL) CalculateSiPMoutput( *Photon_103, adc_smear );
      if(Photon_104!=NULL) CalculateSiPMoutput( *Photon_104, adc_smear );
      if(Photon_111!=NULL) CalculateSiPMoutput( *Photon_111, adc_smear );
      if(Photon_112!=NULL) CalculateSiPMoutput( *Photon_112, adc_smear );
      if(Photon_113!=NULL) CalculateSiPMoutput( *Photon_113, adc_smear );
      if(Photon_114!=NULL) CalculateSiPMoutput( *Photon_114, adc_smear );
      if(Photon_121!=NULL) CalculateSiPMoutput( *Photon_121, adc_smear );
      if(Photon_122!=NULL) CalculateSiPMoutput( *Photon_122, adc_smear );
      if(Photon_123!=NULL) CalculateSiPMoutput( *Photon_123, adc_smear );
      if(Photon_124!=NULL) CalculateSiPMoutput( *Photon_124, adc_smear );
      if(Photon_201!=NULL) CalculateSiPMoutput( *Photon_201, adc_smear );
      if(Photon_202!=NULL) CalculateSiPMoutput( *Photon_202, adc_smear );
      if(Photon_203!=NULL) CalculateSiPMoutput( *Photon_203, adc_smear );
      if(Photon_204!=NULL) CalculateSiPMoutput( *Photon_204, adc_smear );
      if(Photon_211!=NULL) CalculateSiPMoutput( *Photon_211, adc_smear );
      if(Photon_212!=NULL) CalculateSiPMoutput( *Photon_212, adc_smear );
      if(Photon_213!=NULL) CalculateSiPMoutput( *Photon_213, adc_smear );
      if(Photon_214!=NULL) CalculateSiPMoutput( *Photon_214, adc_smear );
      if(Photon_221!=NULL) CalculateSiPMoutput( *Photon_221, adc_smear );
      if(Photon_222!=NULL) CalculateSiPMoutput( *Photon_222, adc_smear );
      if(Photon_223!=NULL) CalculateSiPMoutput( *Photon_223, adc_smear );
      if(Photon_224!=NULL) CalculateSiPMoutput( *Photon_224, adc_smear );
      
      if(Photon101!=NULL) CalculateSiPMoutput( *Photon101, adc_smear );
      if(Photon102!=NULL) CalculateSiPMoutput( *Photon102, adc_smear );
      if(Photon103!=NULL) CalculateSiPMoutput( *Photon103, adc_smear );
      if(Photon104!=NULL) CalculateSiPMoutput( *Photon104, adc_smear );
      if(Photon111!=NULL) CalculateSiPMoutput( *Photon111, adc_smear );
      if(Photon112!=NULL) CalculateSiPMoutput( *Photon112, adc_smear );
      if(Photon113!=NULL) CalculateSiPMoutput( *Photon113, adc_smear );
      if(Photon114!=NULL) CalculateSiPMoutput( *Photon114, adc_smear );
      if(Photon121!=NULL) CalculateSiPMoutput( *Photon121, adc_smear );
      if(Photon122!=NULL) CalculateSiPMoutput( *Photon122, adc_smear );
      if(Photon123!=NULL) CalculateSiPMoutput( *Photon123, adc_smear );
      if(Photon124!=NULL) CalculateSiPMoutput( *Photon124, adc_smear );
      if(Photon201!=NULL) CalculateSiPMoutput( *Photon201, adc_smear );
      if(Photon202!=NULL) CalculateSiPMoutput( *Photon202, adc_smear );
      if(Photon203!=NULL) CalculateSiPMoutput( *Photon203, adc_smear );
      if(Photon204!=NULL) CalculateSiPMoutput( *Photon204, adc_smear );
      if(Photon211!=NULL) CalculateSiPMoutput( *Photon211, adc_smear );
      if(Photon212!=NULL) CalculateSiPMoutput( *Photon212, adc_smear );
      if(Photon213!=NULL) CalculateSiPMoutput( *Photon213, adc_smear );
      if(Photon214!=NULL) CalculateSiPMoutput( *Photon214, adc_smear );
      if(Photon221!=NULL) CalculateSiPMoutput( *Photon221, adc_smear );
      if(Photon222!=NULL) CalculateSiPMoutput( *Photon222, adc_smear );
      if(Photon223!=NULL) CalculateSiPMoutput( *Photon223, adc_smear );
      if(Photon224!=NULL) CalculateSiPMoutput( *Photon224, adc_smear );
//    std::cout << "BesZddDigit :: End Of Event" << std::endl;
    }//if UseSiPM
 else
   {
//usual PMT output
     if(Photon_101!=NULL) CalculatePMToutput( *Photon_101 );
     if(Photon_102!=NULL) CalculatePMToutput( *Photon_102 );
     if(Photon_103!=NULL) CalculatePMToutput( *Photon_103 );
     if(Photon_104!=NULL) CalculatePMToutput( *Photon_104 );
     if(Photon_111!=NULL) CalculatePMToutput( *Photon_111 );
     if(Photon_112!=NULL) CalculatePMToutput( *Photon_112 );
     if(Photon_113!=NULL) CalculatePMToutput( *Photon_113 );
     if(Photon_114!=NULL) CalculatePMToutput( *Photon_114 );
     if(Photon_121!=NULL) CalculatePMToutput( *Photon_121 );
     if(Photon_122!=NULL) CalculatePMToutput( *Photon_122 );
     if(Photon_123!=NULL) CalculatePMToutput( *Photon_123 );
     if(Photon_124!=NULL) CalculatePMToutput( *Photon_124 );
     if(Photon_201!=NULL) CalculatePMToutput( *Photon_201 );
     if(Photon_202!=NULL) CalculatePMToutput( *Photon_202 );
     if(Photon_203!=NULL) CalculatePMToutput( *Photon_203 );
     if(Photon_204!=NULL) CalculatePMToutput( *Photon_204 );
     if(Photon_211!=NULL) CalculatePMToutput( *Photon_211 );
     if(Photon_212!=NULL) CalculatePMToutput( *Photon_212 );
     if(Photon_213!=NULL) CalculatePMToutput( *Photon_213 );
     if(Photon_214!=NULL) CalculatePMToutput( *Photon_214 );
     if(Photon_221!=NULL) CalculatePMToutput( *Photon_221 );
     if(Photon_222!=NULL) CalculatePMToutput( *Photon_222 );
     if(Photon_223!=NULL) CalculatePMToutput( *Photon_223 );
     if(Photon_224!=NULL) CalculatePMToutput( *Photon_224 );
     
     if(Photon101!=NULL) CalculatePMToutput( *Photon101 );
     if(Photon102!=NULL) CalculatePMToutput( *Photon102 );
     if(Photon103!=NULL) CalculatePMToutput( *Photon103 );
     if(Photon104!=NULL) CalculatePMToutput( *Photon104 );
     if(Photon111!=NULL) CalculatePMToutput( *Photon111 );
     if(Photon112!=NULL) CalculatePMToutput( *Photon112 );
     if(Photon113!=NULL) CalculatePMToutput( *Photon113 );
     if(Photon114!=NULL) CalculatePMToutput( *Photon114 );
     if(Photon121!=NULL) CalculatePMToutput( *Photon121 );
     if(Photon122!=NULL) CalculatePMToutput( *Photon122 );
     if(Photon123!=NULL) CalculatePMToutput( *Photon123 );
     if(Photon124!=NULL) CalculatePMToutput( *Photon124 );
     if(Photon201!=NULL) CalculatePMToutput( *Photon201 );
     if(Photon202!=NULL) CalculatePMToutput( *Photon202 );
     if(Photon203!=NULL) CalculatePMToutput( *Photon203 );
     if(Photon204!=NULL) CalculatePMToutput( *Photon204 );
     if(Photon211!=NULL) CalculatePMToutput( *Photon211 );
     if(Photon212!=NULL) CalculatePMToutput( *Photon212 );
     if(Photon213!=NULL) CalculatePMToutput( *Photon213 );
     if(Photon214!=NULL) CalculatePMToutput( *Photon214 );
     if(Photon221!=NULL) CalculatePMToutput( *Photon221 );
     if(Photon222!=NULL) CalculatePMToutput( *Photon222 );
     if(Photon223!=NULL) CalculatePMToutput( *Photon223 );
     if(Photon224!=NULL) CalculatePMToutput( *Photon224 );
   }//else usual PMT output

}

/*
void BesZddDigit::ResizeVectors(Photon& photon)
{
   G4int size = photon.GetThetaSize();
   std::cout << "BesZddDigit::ResizeVectors : VectorSize before erasing: " << size << std::endl;
   
   int element = 0;
   for(int i = 0; i< size-element;)
   {
      //! save all 
      if( photon.GetDestroyFlag(i) != 0) 
      {
	element++;
	if( !(photon.EraseVectorElement(i)) ) std::cout << "BesZddDigit::ResizeVectors : problem with erasing vector element!!" << std::endl;
      }
      else i++;
   }
   
   size = photon.GetThetaSize();
   std::cout << "BesZddDigit::ResizeVectors : VectorSize after erasing: " << size << std::endl;  
}
*/
										     
//SiPM output
void BesZddDigit::CalculateSiPMoutput(Photon& photon, double adc_smear)
{

  G4int size = photon.GetPosXSize();
  for(int u = 0; u < size; u++)
  {
    photon.SetTransTime(0.);
  }


  if(size!=photon.GetPosYSize() || size!=photon.GetPosZSize() || size!=photon.GetTrajTimeSize() || size!=photon.GetEmitTimeSize() || size!=photon.GetPropTimeSize() || size!= photon.GetTransTimeSize()) std::cout << "BesZddDigit::CalculateSiPMoutput : ERROR: photon vector sizes not all equal!!! " << std::endl;
  //  if(size > 3600) std::cout << "BesZddDigit.cc::CalculateSiPMoutput :: #photons: " << size << std::endl;                                                                
  double edep = 0;
  switch(photon.GetCrystalNo())
    {
    case -101: edep = totEDep_101; break;
    case -102: edep = totEDep_102; break;
    case -103: edep = totEDep_103; break;
    case -104: edep = totEDep_104; break;
    case -111: edep = totEDep_111; break;
    case -112: edep = totEDep_112; break;
    case -113: edep = totEDep_113; break;
    case -114: edep = totEDep_114; break;
    case -121: edep = totEDep_121; break;
    case -122: edep = totEDep_122; break;
    case -123: edep = totEDep_123; break;
    case -124: edep = totEDep_124; break;
    case -201: edep = totEDep_201; break;
    case -202: edep = totEDep_202; break;
    case -203: edep = totEDep_203; break;
    case -204: edep = totEDep_204; break;
    case -211: edep = totEDep_211; break;
    case -212: edep = totEDep_212; break;
    case -213: edep = totEDep_213; break;
    case -214: edep = totEDep_214; break;
    case -221: edep = totEDep_221; break;
    case -222: edep = totEDep_222; break;
    case -223: edep = totEDep_223; break;
    case -224: edep = totEDep_224; break;
      
    case 101: edep = totEDep101; break;
    case 102: edep = totEDep102; break;
    case 103: edep = totEDep103; break;
    case 104: edep = totEDep104; break;
    case 111: edep = totEDep111; break;
    case 112: edep = totEDep112; break;
    case 113: edep = totEDep113; break;
    case 114: edep = totEDep114; break;
    case 121: edep = totEDep121; break;
    case 122: edep = totEDep122; break;
    case 123: edep = totEDep123; break;
    case 124: edep = totEDep124; break;
    case 201: edep = totEDep201; break;
    case 202: edep = totEDep202; break;
    case 203: edep = totEDep203; break;
    case 204: edep = totEDep204; break;

    case 211: edep = totEDep211; break;
    case 212: edep = totEDep212; break;
    case 213: edep = totEDep213; break;
    case 214: edep = totEDep214; break;
    case 221: edep = totEDep221; break;
    case 222: edep = totEDep222; break;
    case 223: edep = totEDep223; break;
    case 224: edep = totEDep224; break;
    default: std::cout << "BesZddDigit.cc :: CalculateSiPMoutput : Failure !!! wrong crystal No!! " << std::endl; break;
    }

  //   std::cout << "BesZddDigit::CalculateSiPMOutput()::edep = " << edep << " MeV" << std::endl;



  //  calculate number of fired pixels depending on number of incoming photons and quantum efficiency:                                                                   
  G4double numberOfPixels = BesZddDigit::SiPMPixels * (1 - TMath::Exp(-1.*size * BesZddDigit::SiPMQE/BesZddDigit::SiPMPixels)); //N_fired = N_total(SiPM) * (1 - exp(N_photon * photon_detection_efficiency(SiPM)/N_total(SiPM))            
  // std::cout << "BesZddDigit.cc :: CalculateSiPMoutput: numberOfPhotons: " << size << ", numberOfPixels : " << numberOfPixels << ", " << size-numberOfPixels << " photons LOST due to logarithmic compression of SiPM" << std::endl;
  
  for(int i = 0; i < size; i++)
    {
     G4double pTime = photon.GetTotalTime(i); // in [ns]!
     //     if(photon.GetCrystalNo()==112)hTemp1->Fill(pTime);
     if(edep >= 200)
       {
	 //	 std::cout << "BesZddDigit.cc :: CalculateSiPMoutput :: edep (hTemp1) : " << edep << std::endl;
	 //	 hTemp1->Fill(pTime-BesZddDigit::timeOffset);     
       }
     //  std::cout << "BesZddDigit.cc :: CalculateSiPMoutput : photon total time: " << pTime << std::endl;                                                             
     double pixelRandom = G4UniformRand(); //flat random number distribution between 0 and 1; 
     if(pixelRandom <= (numberOfPixels/size)) // to add only pixels, which fire according to the number of firing pixels
      {
	//	for(int k = 0; k <= 80; k++)  //! add up spectra of the single pixel, each singal is 80ns long, unchanged c5 capacity    
	for(int k = 0; k <= 270; k++)  //! add up spectra of the single pixel, each singal is 270ns long
	{
	  	  if(photon.GetCrystalNo()==112)
	    {
	      //   /* if(edep >= 200.);//*/ hTemp2->Fill(k+pTime-BesZddDigit::timeOffset,pixelFunction(k));
	     }
          int x = (int)(k + pTime - BesZddDigit::timeOffset); //int x, because x is the array position (=time in [ns]), where the value pixelFunction(2k) is stored 

	  //	  	  std::cout << "BesZddDigit::CalculateSiPMOutput() : x :" << x << std::endl;                                                                                    
	  // is x smaller than maximal pulse length?! arbitrarily set to 400ns;

	  if( x < 400. )
	    {
	      photon.AddOutput( x , pixelFunction(k) );
	      //    std::cout << "BesZddDigit.cc::CalculateSiPMOutput:: x = " << x << ",\t k = " << k << ",/t, pixelfunction(k) = " << pixelFunction(k) << std::endl;
	    }
	}// for(int k)
      }// if(pixelRandom)
    }//for(int i = size)
  /*
  for(int k = 0; k<400; k++)
    {
            if(photon.GetCrystalNo()==112) hProfile->Fill(k,hTemp2->GetBinContent(k+1));// See TH1::GetBin(int i), why we need k+1 here
    }
*/

  //  std::cout << "BesZddDigit.cc::CalculateSiPMOutput : test1 " << std::endl;
  G4double charge = 0.;
  G4bool threshold = false;
  G4double time = 0.;
  //charge channel is the sum of all sample heights of the ADC, which runs at a frequency of 62.5MHz, collecting a sample every 16ns, therefore, the charge channel can be different depending on which part of the wave the ADC will collect data, it is important, that all channels have the same smearing for one simulated event, because the input of each ADC channel is read out synchronously
  //hADCwave->SetMarkerStyle(2);
  //  hADCwave->GetXaxis()->SetTitle("time [ns]");
  //hADCwave->GetYaxis()->SetTitle("sample height [mV]");
  
  G4bool wFound = false;

  //   std::cout << "BesZddDigit.cc::CalculateSiPMOutput : test2 " << std::endl;

for(int i = 0; i < 400;)
    {
      if(i+(int)adc_smear <= 400)
	{
	  double output = photon.GetOutput(i+(int)adc_smear);
	  //    std::cout << "BesZddDigit.cc::CalculateSiPMOutput : photon.GetOutput : " << output << std::endl;
	  charge += output; //! integral = sum(y)                                                                                             

// 	  	  if(output != 0 && photon.GetCrystalNo()==112) hADCwave->Fill(i+(int)adc_smear, output);
          if(edep >= 200.)
	    {
	      //  std::cout << "Calculate SiPM output, wave found, edep = " << edep << " MeV" << std::endl;
	      waveFound = true;
	      //	      std::cout << "eDep : " << edep << " MeV" << std::endl;
	      //      hADCwave->Fill(i+(int)adc_smear, output);
	    }
	  if( fabs( photon.GetOutput(i) ) > 10. && threshold == true) //! threshold arbitrarily set to 10
	    {
	      time = i/1000. + BesZddDigit::timeOffset; //! shift comma by 3 for better precision                                                                           	      
	      //      std::cout << "BesZddDigit::CalculateSiPMOutput : time: " << time << std::endl;
	      threshold = false;
	    }
	  else time = timeOffset;
	}//if(i+adc_smear)
      i+=16;
    }//for(int i)

// if(wFound==true) waveFound = true;
  /* 
  //loop for testing smearing through adc because of sampling points of waveform shift
  TRandom3 rand3_smear;
  // if(photon.GetCrystalNo()==112)
  //  {
  //      for(int o = 0; o <= 10000000; o++)
  //	{
	  //	  if(o%100000==0) std::cout<< "BesZddDigit::CalculateSiPMoutput : o = " << o << std::endl;
	  double rand_shift = rand3_smear.Uniform(16);
	  charge = 0.;
	  for(int i = 0; i <= 350;) // 350 is the ADC window
	    {
	      if(i+(int)rand_shift <= 350)
		{
		  double output = fabs(photon.GetOutput(i+(int)rand_shift));
		  charge+= output;
		  //		  std::cout << "BesZddDigit::CalculateSiPMoutput : sample point: " << i+(int)rand_shift << " , output: " << output << std::endl;
		  //	      i+=16;
		}//if(i+(int)rand_shift)
	      i+=16;
	    }//for(int i)
	  hADCsmear->Fill(charge);
	  hADCpoint_vs_smear->Fill((int)rand_shift, charge);
	  //}//for(int o)
      //    }//if(photon.CrystalNo)
      */
// std::cout << "BesZddDigit :: Calculate SiPM output : Charge: " << abs((int)charge) << std::endl;                                                                         //  std::cout << "BesZddDigit :: Calculate SiPM output" << std::endl;                                                                                                         
//charge channel is the sum of all sample heights of the ADC, which runs at a frequency of 62.5MHz, collecting a sample every 16ns, therefore, the charge channel can be different depending on which part of the wave the ADC will collect data, 
      
  photon.SetChargeChannel( abs((int)charge) );
  photon.SetTimeChannel( abs((int)time) );
  /*
  double edep = 0;
  switch(photon.GetCrystalNo())
    {
    case -101: edep = totEDep_101; break;
    case -102: edep = totEDep_102; break;
    case -103: edep = totEDep_103; break;
    case -104: edep = totEDep_104; break;
    case -111: edep = totEDep_111; break;
    case -112: edep = totEDep_112; break;
    case -113: edep = totEDep_113; break;
    case -114: edep = totEDep_114; break;
    case -121: edep = totEDep_121; break;
    case -122: edep = totEDep_122; break;
    case -123: edep = totEDep_123; break;
    case -124: edep = totEDep_124; break;
    case -201: edep = totEDep_201; break;
    case -202: edep = totEDep_202; break;
    case -203: edep = totEDep_203; break;
    case -204: edep = totEDep_204; break;
    case -211: edep = totEDep_211; break;
    case -212: edep = totEDep_212; break;
    case -213: edep = totEDep_213; break;
    case -214: edep = totEDep_214; break;
    case -221: edep = totEDep_221; break;
    case -222: edep = totEDep_222; break;
    case -223: edep = totEDep_223; break;
    case -224: edep = totEDep_224; break;

    case 101: edep = totEDep101; break;
    case 102: edep = totEDep102; break;
    case 103: edep = totEDep103; break;
    case 104: edep = totEDep104; break;
    case 111: edep = totEDep111; break;
    case 112: edep = totEDep112; break;
    case 113: edep = totEDep113; break;
    case 114: edep = totEDep114; break;
    case 121: edep = totEDep121; break;
    case 122: edep = totEDep122; break; 
    case 123: edep = totEDep123; break;
    case 124: edep = totEDep124; break;
    case 201: edep = totEDep201; break;
    case 202: edep = totEDep202; break;
    case 203: edep = totEDep203; break;
    case 204: edep = totEDep204; break;
    
    case 211: edep = totEDep211; break;
    case 212: edep = totEDep212; break;
    case 213: edep = totEDep213; break;
    case 214: edep = totEDep214; break;
    case 221: edep = totEDep221; break;
    case 222: edep = totEDep222; break;
    case 223: edep = totEDep223; break;
    case 224: edep = totEDep224; break;
    default: std::cout << "BesZddDigit.cc :: CalculateSiPMoutput : Failure !!! wrong crystal No!! " << std::endl; break;
    }
  */
  BesZddDigit::hEDepChargeChannel->Fill(edep,abs((int)charge));
  
  //  std::cout << "BesZddDigit.cc :: CalculateSiPMoutput: CrystalNo - eDep[MeV] - chargeChannel: " << photon.GetCrystalNo() << "\t" << edep << "\t" << abs((int)charge) << s\td::endl;                                                                                                                                                               
}


 //usual PMT
void BesZddDigit::CalculatePMToutput(Photon& photon)
{  


  G4int size = photon.GetPosXSize();
  for(int u=0; u<size; u++)
    {
      double t_trans = G4RandGauss::shoot(5.,0.3);//! mean ~ 5ns, sigma ~0.3ns                                                                                         
      photon.SetTransTime(t_trans);
    }

  if(size!=photon.GetPosYSize() || size!=photon.GetPosZSize() || size!=photon.GetTrajTimeSize() || size!=photon.GetEmitTimeSize() || size!=photon.GetPropTimeSize() || size!=photon.GetTransTimeSize()) std::cout << "BesZddDigit::CalculatePMToutput : ERROR: photon vector sizes not all equal!!! " << std::endl;

  //  if(size > 3600) std::cout << "BesZddDigit.cc::CalculatePMToutput :: #photons: " << size << std::endl;
  for(int i = 0; i < size; i++)
  {
    G4double pTime = photon.GetTotalTime(i);

    //        std::cout << "BesZddDigit.cc :: CalculatePMToutput : photon total time: " << pTime << std::endl;

    for(int k = 0; k < 2000; k++)  //! add up spectra of the single photoelectrons
    {
//      hTemp2->Fill(fX.at(k)+pTime,fY.at(k));
	int x = (int)(BesZddDigit::fX.at(k) + pTime - BesZddDigit::timeOffset)/0.025;

	//	std::cout << "BesZddDigit::CalculatePMTOutput() : x :" << x << std::endl;

	if( x < 3200 ) photon.AddOutput( x , BesZddDigit::fY.at(k) ); //! 25ps bins for x-axis, max time is 80ns
    }
  }
  
 G4double charge = 0.;
 G4bool threshold = false;
 G4double time = 0.;
 for(int i = 0; i < 3200; i++)
  {
    charge += photon.GetOutput(i) * 0.025; //! integral = y * delta T (=0.025ns)
    if( fabs( photon.GetOutput(i) ) > 200. && threshold == true) //! threshold arbitrarily set to 200
    {
        time = i*25./1000. + BesZddDigit::timeOffset; //! shift comma by 3 for better precision 
//	std::cout << "BesZddDigit::CalculatePMTOutput : time: " << time << std::endl;
	threshold = false;
    }
    else time = timeOffset;
  }

// std::cout << "BesZddDigit :: Calculate PMT output : Charge: " << abs((int)charge) << std::endl;
//  std::cout << "BesZddDigit :: Calculate PMT output" << std::endl;
  photon.SetChargeChannel( abs((int)charge) );
  photon.SetTimeChannel( abs((int)time) );

  double edep = 0;
  switch(photon.GetCrystalNo())
  {
    case -101: edep = totEDep_101; break;
    case -102: edep = totEDep_102; break;
    case -103: edep = totEDep_103; break;
    case -104: edep = totEDep_104; break;    
    case -111: edep = totEDep_111; break;
    case -112: edep = totEDep_112; break;
    case -113: edep = totEDep_113; break;
    case -114: edep = totEDep_114; break; 
    case -121: edep = totEDep_121; break;
    case -122: edep = totEDep_122; break;
    case -123: edep = totEDep_123; break;
    case -124: edep = totEDep_124; break;     
    case -201: edep = totEDep_201; break;
    case -202: edep = totEDep_202; break;
    case -203: edep = totEDep_203; break;
    case -204: edep = totEDep_204; break;    
    case -211: edep = totEDep_211; break;
    case -212: edep = totEDep_212; break;
    case -213: edep = totEDep_213; break;
    case -214: edep = totEDep_214; break; 
    case -221: edep = totEDep_221; break;
    case -222: edep = totEDep_222; break;
    case -223: edep = totEDep_223; break;
    case -224: edep = totEDep_224; break;  
    
    case 101: edep = totEDep101; break;
    case 102: edep = totEDep102; break;
    case 103: edep = totEDep103; break;
    case 104: edep = totEDep104; break;    
    case 111: edep = totEDep111; break;
    case 112: edep = totEDep112; break;
    case 113: edep = totEDep113; break;
    case 114: edep = totEDep114; break; 
    case 121: edep = totEDep121; break;
    case 122: edep = totEDep122; break;
    case 123: edep = totEDep123; break;
    case 124: edep = totEDep124; break;     
    case 201: edep = totEDep201; break;
    case 202: edep = totEDep202; break;
    case 203: edep = totEDep203; break;
    case 204: edep = totEDep204; break;    
    case 211: edep = totEDep211; break;
    case 212: edep = totEDep212; break;
    case 213: edep = totEDep213; break;
    case 214: edep = totEDep214; break; 
    case 221: edep = totEDep221; break;
    case 222: edep = totEDep222; break;
    case 223: edep = totEDep223; break;
    case 224: edep = totEDep224; break;  
    default: std::cout << "BesZddDigit.cc :: CalculatePMToutput : Failure !!! wrong crystal No!! " << std::endl; break;
  }
    
  BesZddDigit::hEDepChargeChannel->Fill(edep,abs((int)charge));
  //  std::cout << "BesZddDigit.cc :: CalculatePMToutput: CrystalNo - eDep[MeV] - chargeChannel: " << photon.GetCrystalNo() << "\t" << edep << "\t" << abs((int)charge) << std::endl;
    
}
