// test

//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description:
//Author: Youzy
//Created: Mar, 2004
//Modified:
//Comment:
//---------------------------------------------------------------------------//
//$Id: BesZddDigitizer.cc

#include "BesZddDigitizer.hh"
#include "BesZddDigit.hh"
#include "BesZddDigi.hh"
#include "BesZddHit.hh"
#include "G4DigiManager.hh"
#include "Randomize.hh"
#include "G4UnitsTable.hh"
#include <CLHEP/Random/Randomize.h>

#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "G4Svc/IG4Svc.h"
#include "G4Svc/G4Svc.h"

using namespace std;

BesZddDigitizer::BesZddDigitizer(G4String modName)
:G4VDigitizerModule(modName)
{
  G4cout << "BesZddDigitizer::Digitization initialization" << G4endl;
	
  collectionName.push_back("BesZddDigisCollection");
  m_besZddDigisCollection=0;

	
  //Standard unit: length in mm; time in ns; energy in MeV; att in mm;
  //Here: att in mm; vel in m/s; threshold in MeV; 
  
  
  //retrieve G4Svc
  ISvcLocator* svcLocator = Gaudi::svcLocator();
  IG4Svc* tmpSvc;
  StatusCode sc=svcLocator->service("G4Svc", tmpSvc);
  m_G4Svc=dynamic_cast<G4Svc *>(tmpSvc);
  /*
  //get Zdd Ntuple from G4Svc
  if(m_G4Svc->ZddRootFlag())
  {
    m_tupleZdd = m_G4Svc->GetTupleZdd();
    sc = m_tupleZdd->addItem("partID",m_partID);
    sc = m_tupleZdd->addItem("detectorID",m_detectorID);
    sc = m_tupleZdd->addItem("crystalNo",m_crystalNo);
  }
  */
  /*
  fFile = new TFile("EDepChargeChannel.root","recreate");
  tEDepChargeChannel = new TTree("tEDepChargeChannel","");
  tEDepChargeChannel->Branch("eDep",&m_eDep,"m_eDep/D");
  tEDepChargeChannel->Branch("chargeChannel",&m_chargeChannel,"m_chargeChannel/I");
  */
  //  Initialize();

  //  impactReco = true; //true; //! output impact reconstruction data?

//   chargeChannelFile = new TFile("EDepChargeChannel.root","recreate");
//   tEDepChargeChannel = new TTree("tEDepChargeChannel","");
//   tEDepChargeChannel->Branch("eDep",&m_eDep,"m_eDep/D");
//   tEDepChargeChannel->Branch("chargeChannel",&m_chargeChannel,"m_chargeChannel/I");

//   impactReco = false;

  impactReco = true;
  if(impactReco)
    {
      const int NMax=64;
			m_xImpacts = new double[NMax];
			
      fFile = new TFile("Impact_reco.root","recreate");
      //      tEDepChargeChannel = new TTree("tEDepChargeChannel","");
      //tEDepChargeChannel->Branch("eDep",&m_eDep,"m_eDep/D");
      //tEDepChargeChannel->Branch("chargeChannel",&m_chargeChannel,"m_chargeChannel/I");

      tImpactReco = new TTree("Impact_reco","");
			tImpactReco->Branch("zdd_energy",&m_zdd_energy,"m_zdd_energy/D");
			tImpactReco->Branch("zddRight_energy",&m_zddRight_energy,"m_zddRight_energy/D");
			tImpactReco->Branch("zddLeft_energy",&m_zddLeft_energy,"m_zddLeft_energy/D");
			
      tImpactReco->Branch("eDep101Right",&m_eDep101Right,"m_eDep101Right/D");
      tImpactReco->Branch("chargeChannel101Right",&m_chargeChannel101Right,"m_chargeChannel101Right/I");
      tImpactReco->Branch("eDep102Right",&m_eDep102Right,"m_eDep102Right/D");
      tImpactReco->Branch("chargeChannel102Right",&m_chargeChannel102Right,"m_chargeChannel102Right/I");
      tImpactReco->Branch("eDep103Right",&m_eDep103Right,"m_eDep103Right/D");
      tImpactReco->Branch("chargeChannel103Right",&m_chargeChannel103Right,"m_chargeChannel103Right/I");
      tImpactReco->Branch("eDep104Right",&m_eDep104Right,"m_eDep104Right/D");
      tImpactReco->Branch("chargeChannel104Right",&m_chargeChannel104Right,"m_chargeChannel104Right/I");
      tImpactReco->Branch("eDep111Right",&m_eDep111Right,"m_eDep111Right/D");
      tImpactReco->Branch("chargeChannel111Right",&m_chargeChannel111Right,"m_chargeChannel111Right/I");
      tImpactReco->Branch("eDep112Right",&m_eDep112Right,"m_eDep112Right/D");
      tImpactReco->Branch("chargeChannel112Right",&m_chargeChannel112Right,"m_chargeChannel112Right/I");
      tImpactReco->Branch("eDep113Right",&m_eDep113Right,"m_eDep113Right/D");
      tImpactReco->Branch("chargeChannel113Right",&m_chargeChannel113Right,"m_chargeChannel113Right/I");
      tImpactReco->Branch("eDep114Right",&m_eDep114Right,"m_eDep114Right/D");
      tImpactReco->Branch("chargeChannel114Right",&m_chargeChannel114Right,"m_chargeChannel114Right/I");
      tImpactReco->Branch("eDep121Right",&m_eDep121Right,"m_eDep121Right/D");
      tImpactReco->Branch("chargeChannel121Right",&m_chargeChannel121Right,"m_chargeChannel121Right/I");
      tImpactReco->Branch("eDep122Right",&m_eDep122Right,"m_eDep122Right/D");
      tImpactReco->Branch("chargeChannel122Right",&m_chargeChannel122Right,"m_chargeChannel122Right/I");
      tImpactReco->Branch("eDep123Right",&m_eDep123Right,"m_eDep123Right/D");
      tImpactReco->Branch("chargeChannel123Right",&m_chargeChannel123Right,"m_chargeChannel123Right/I");
      tImpactReco->Branch("eDep124Right",&m_eDep124Right,"m_eDep124Right/D");
      tImpactReco->Branch("chargeChannel124Right",&m_chargeChannel124Right,"m_chargeChannel124Right/I");
      
      tImpactReco->Branch("eDep201Right",&m_eDep201Right,"m_eDep201Right/D");
      tImpactReco->Branch("chargeChannel201Right",&m_chargeChannel201Right,"m_chargeChannel201Right/I");
      tImpactReco->Branch("eDep202Right",&m_eDep202Right,"m_eDep202Right/D");
      tImpactReco->Branch("chargeChannel202Right",&m_chargeChannel202Right,"m_chargeChannel202Right/I");
      tImpactReco->Branch("eDep203Right",&m_eDep203Right,"m_eDep203Right/D");
      tImpactReco->Branch("chargeChannel203Right",&m_chargeChannel203Right,"m_chargeChannel203Right/I");
      tImpactReco->Branch("eDep204Right",&m_eDep204Right,"m_eDep204Right/D");
      tImpactReco->Branch("chargeChannel204Right",&m_chargeChannel204Right,"m_chargeChannel204Right/I");
      tImpactReco->Branch("eDep211Right",&m_eDep211Right,"m_eDep211Right/D");
      tImpactReco->Branch("chargeChannel211Right",&m_chargeChannel211Right,"m_chargeChannel211Right/I");
      tImpactReco->Branch("eDep212Right",&m_eDep212Right,"m_eDep212Right/D");
      tImpactReco->Branch("chargeChannel212Right",&m_chargeChannel212Right,"m_chargeChannel212Right/I");
      tImpactReco->Branch("eDep213Right",&m_eDep213Right,"m_eDep213Right/D");
      tImpactReco->Branch("chargeChannel213Right",&m_chargeChannel213Right,"m_chargeChannel213Right/I");
      tImpactReco->Branch("eDep214Right",&m_eDep214Right,"m_eDep214Right/D");
      tImpactReco->Branch("chargeChannel214Right",&m_chargeChannel214Right,"m_chargeChannel214Right/I");
      tImpactReco->Branch("eDep221Right",&m_eDep221Right,"m_eDep221Right/D");
      tImpactReco->Branch("chargeChannel221Right",&m_chargeChannel221Right,"m_chargeChannel221Right/I");
      tImpactReco->Branch("eDep222Right",&m_eDep222Right,"m_eDep222Right/D");
      tImpactReco->Branch("chargeChannel222Right",&m_chargeChannel222Right,"m_chargeChannel222Right/I");
      tImpactReco->Branch("eDep223Right",&m_eDep223Right,"m_eDep223Right/D");
      tImpactReco->Branch("chargeChannel223Right",&m_chargeChannel223Right,"m_chargeChannel223Right/I");
      tImpactReco->Branch("eDep224Right",&m_eDep224Right,"m_eDep224Right/D");
      tImpactReco->Branch("chargeChannel224Right",&m_chargeChannel224Right,"m_chargeChannel224Right/I"); 
      
			 tImpactReco->Branch("eDep101Left",&m_eDep101Left,"m_eDep101Left/D");
      tImpactReco->Branch("chargeChannel101Left",&m_chargeChannel101Left,"m_chargeChannel101Left/I");
      tImpactReco->Branch("eDep102Left",&m_eDep102Left,"m_eDep102Left/D");
      tImpactReco->Branch("chargeChannel102Left",&m_chargeChannel102Left,"m_chargeChannel102Left/I");
      tImpactReco->Branch("eDep103Left",&m_eDep103Left,"m_eDep103Left/D");
      tImpactReco->Branch("chargeChannel103Left",&m_chargeChannel103Left,"m_chargeChannel103Left/I");
      tImpactReco->Branch("eDep104Left",&m_eDep104Left,"m_eDep104Left/D");
      tImpactReco->Branch("chargeChannel104Left",&m_chargeChannel104Left,"m_chargeChannel104Left/I");
      tImpactReco->Branch("eDep111Left",&m_eDep111Left,"m_eDep111Left/D");
      tImpactReco->Branch("chargeChannel111Left",&m_chargeChannel111Left,"m_chargeChannel111Left/I");
      tImpactReco->Branch("eDep112Left",&m_eDep112Left,"m_eDep112Left/D");
      tImpactReco->Branch("chargeChannel112Left",&m_chargeChannel112Left,"m_chargeChannel112Left/I");
      tImpactReco->Branch("eDep113Left",&m_eDep113Left,"m_eDep113Left/D");
      tImpactReco->Branch("chargeChannel113Left",&m_chargeChannel113Left,"m_chargeChannel113Left/I");
      tImpactReco->Branch("eDep114Left",&m_eDep114Left,"m_eDep114Left/D");
      tImpactReco->Branch("chargeChannel114Left",&m_chargeChannel114Left,"m_chargeChannel114Left/I");
      tImpactReco->Branch("eDep121Left",&m_eDep121Left,"m_eDep121Left/D");
      tImpactReco->Branch("chargeChannel121Left",&m_chargeChannel121Left,"m_chargeChannel121Left/I");
      tImpactReco->Branch("eDep122Left",&m_eDep122Left,"m_eDep122Left/D");
      tImpactReco->Branch("chargeChannel122Left",&m_chargeChannel122Left,"m_chargeChannel122Left/I");
      tImpactReco->Branch("eDep123Left",&m_eDep123Left,"m_eDep123Left/D");
      tImpactReco->Branch("chargeChannel123Left",&m_chargeChannel123Left,"m_chargeChannel123Left/I");
      tImpactReco->Branch("eDep124Left",&m_eDep124Left,"m_eDep124Left/D");
      tImpactReco->Branch("chargeChannel124Left",&m_chargeChannel124Left,"m_chargeChannel124Left/I");
      
      tImpactReco->Branch("eDep201Left",&m_eDep201Left,"m_eDep201Left/D");
      tImpactReco->Branch("chargeChannel201Left",&m_chargeChannel201Left,"m_chargeChannel201Left/I");
      tImpactReco->Branch("eDep202Left",&m_eDep202Left,"m_eDep202Left/D");
      tImpactReco->Branch("chargeChannel202Left",&m_chargeChannel202Left,"m_chargeChannel202Left/I");
      tImpactReco->Branch("eDep203Left",&m_eDep203Left,"m_eDep203Left/D");
      tImpactReco->Branch("chargeChannel203Left",&m_chargeChannel203Left,"m_chargeChannel203Left/I");
      tImpactReco->Branch("eDep204Left",&m_eDep204Left,"m_eDep204Left/D");
      tImpactReco->Branch("chargeChannel204Left",&m_chargeChannel204Left,"m_chargeChannel204Left/I");
      tImpactReco->Branch("eDep211Left",&m_eDep211Left,"m_eDep211Left/D");
      tImpactReco->Branch("chargeChannel211Left",&m_chargeChannel211Left,"m_chargeChannel211Left/I");
      tImpactReco->Branch("eDep212Left",&m_eDep212Left,"m_eDep212Left/D");
      tImpactReco->Branch("chargeChannel212Left",&m_chargeChannel212Left,"m_chargeChannel212Left/I");
      tImpactReco->Branch("eDep213Left",&m_eDep213Left,"m_eDep213Left/D");
      tImpactReco->Branch("chargeChannel213Left",&m_chargeChannel213Left,"m_chargeChannel213Left/I");
      tImpactReco->Branch("eDep214Left",&m_eDep214Left,"m_eDep214Left/D");
      tImpactReco->Branch("chargeChannel214Left",&m_chargeChannel214Left,"m_chargeChannel214Left/I");
      tImpactReco->Branch("eDep221Left",&m_eDep221Left,"m_eDep221Left/D");
      tImpactReco->Branch("chargeChannel221Left",&m_chargeChannel221Left,"m_chargeChannel221Left/I");
      tImpactReco->Branch("eDep222Left",&m_eDep222Left,"m_eDep222Left/D");
      tImpactReco->Branch("chargeChannel222Left",&m_chargeChannel222Left,"m_chargeChannel222Left/I");
      tImpactReco->Branch("eDep223Left",&m_eDep223Left,"m_eDep223Left/D");
      tImpactReco->Branch("chargeChannel223Left",&m_chargeChannel223Left,"m_chargeChannel223Left/I");
      tImpactReco->Branch("eDep224Left",&m_eDep224Left,"m_eDep224Left/D");
      tImpactReco->Branch("chargeChannel224Left",&m_chargeChannel224Left,"m_chargeChannel224Left/I");
			
			 tImpactReco->Branch("eDep101",&m_eDep101,"m_eDep101/D");
      tImpactReco->Branch("chargeChannel101",&m_chargeChannel101,"m_chargeChannel101/I");
      tImpactReco->Branch("eDep102",&m_eDep102,"m_eDep102/D");
      tImpactReco->Branch("chargeChannel102",&m_chargeChannel102,"m_chargeChannel102/I");
      tImpactReco->Branch("eDep103",&m_eDep103,"m_eDep103/D");
      tImpactReco->Branch("chargeChannel103",&m_chargeChannel103,"m_chargeChannel103/I");
      tImpactReco->Branch("eDep104",&m_eDep104,"m_eDep104/D");
      tImpactReco->Branch("chargeChannel104",&m_chargeChannel104,"m_chargeChannel104/I");
      tImpactReco->Branch("eDep111",&m_eDep111,"m_eDep111/D");
      tImpactReco->Branch("chargeChannel111",&m_chargeChannel111,"m_chargeChannel111/I");
      tImpactReco->Branch("eDep112",&m_eDep112,"m_eDep112/D");
      tImpactReco->Branch("chargeChannel112",&m_chargeChannel112,"m_chargeChannel112/I");
      tImpactReco->Branch("eDep113",&m_eDep113,"m_eDep113/D");
      tImpactReco->Branch("chargeChannel113",&m_chargeChannel113,"m_chargeChannel113/I");
      tImpactReco->Branch("eDep114",&m_eDep114,"m_eDep114/D");
      tImpactReco->Branch("chargeChannel114",&m_chargeChannel114,"m_chargeChannel114/I");
      tImpactReco->Branch("eDep121",&m_eDep121,"m_eDep121/D");
      tImpactReco->Branch("chargeChannel121",&m_chargeChannel121,"m_chargeChannel121/I");
      tImpactReco->Branch("eDep122",&m_eDep122,"m_eDep122/D");
      tImpactReco->Branch("chargeChannel122",&m_chargeChannel122,"m_chargeChannel122/I");
      tImpactReco->Branch("eDep123",&m_eDep123,"m_eDep123/D");
      tImpactReco->Branch("chargeChannel123",&m_chargeChannel123,"m_chargeChannel123/I");
      tImpactReco->Branch("eDep124",&m_eDep124,"m_eDep124/D");
      tImpactReco->Branch("chargeChannel124",&m_chargeChannel124,"m_chargeChannel124/I");
      
      tImpactReco->Branch("eDep201",&m_eDep201,"m_eDep201/D");
      tImpactReco->Branch("chargeChannel201",&m_chargeChannel201,"m_chargeChannel201/I");
      tImpactReco->Branch("eDep202",&m_eDep202,"m_eDep202/D");
      tImpactReco->Branch("chargeChannel202",&m_chargeChannel202,"m_chargeChannel202/I");
      tImpactReco->Branch("eDep203",&m_eDep203,"m_eDep203/D");
      tImpactReco->Branch("chargeChannel203",&m_chargeChannel203,"m_chargeChannel203/I");
      tImpactReco->Branch("eDep204",&m_eDep204,"m_eDep204/D");
      tImpactReco->Branch("chargeChannel204",&m_chargeChannel204,"m_chargeChannel204/I");
      tImpactReco->Branch("eDep211",&m_eDep211,"m_eDep211/D");
      tImpactReco->Branch("chargeChannel211",&m_chargeChannel211,"m_chargeChannel211/I");
      tImpactReco->Branch("eDep212",&m_eDep212,"m_eDep212/D");
      tImpactReco->Branch("chargeChannel212",&m_chargeChannel212,"m_chargeChannel212/I");
      tImpactReco->Branch("eDep213",&m_eDep213,"m_eDep213/D");
      tImpactReco->Branch("chargeChannel213",&m_chargeChannel213,"m_chargeChannel213/I");
      tImpactReco->Branch("eDep214",&m_eDep214,"m_eDep214/D");
      tImpactReco->Branch("chargeChannel214",&m_chargeChannel214,"m_chargeChannel214/I");
      tImpactReco->Branch("eDep221",&m_eDep221,"m_eDep221/D");
      tImpactReco->Branch("chargeChannel221",&m_chargeChannel221,"m_chargeChannel221/I");
      tImpactReco->Branch("eDep222",&m_eDep222,"m_eDep222/D");
      tImpactReco->Branch("chargeChannel222",&m_chargeChannel222,"m_chargeChannel222/I");
      tImpactReco->Branch("eDep223",&m_eDep223,"m_eDep223/D");
      tImpactReco->Branch("chargeChannel223",&m_chargeChannel223,"m_chargeChannel223/I");
      tImpactReco->Branch("eDep224",&m_eDep224,"m_eDep224/D");
      tImpactReco->Branch("chargeChannel224",&m_chargeChannel224,"m_chargeChannel224/I");
			
      tImpactReco->Branch("xImpact",&m_xImpact,"m_xImpact/D");
      tImpactReco->Branch("yImpact",&m_yImpact,"m_yImpact/D");
			tImpactReco->Branch("zImpact",&m_zImpact,"m_zImpact/D");
			
			//tImpactReco->Branch("xImpact",&m_xImpact,"m_xImpact/D");
			
			tImpactReco->Branch("PxImpact",&m_PxImpact,"m_PxImpact/D");
			tImpactReco->Branch("PyImpact",&m_PyImpact,"m_PyImpact/D");
			tImpactReco->Branch("PzImpact",&m_PzImpact,"m_PzImpact/D");
			
			//tImpactReco->Branch("m_Ntracks",&m_Ntracks,"m_Ntracks/I");			
			//tImpactReco->Branch("m_xImpacts",m_xImpacts,"m_xImpacts[m_Ntracks]/D");
			/*
      tImpactReco->Branch("m_yImpacts",m_yImpacts,"m_yImpacts[m_Ntracks]/D");
			
			//tImpactReco->Branch("zImpacts",m_zImpacts,"m_zImpacts[m_Ntracks]/D");
			
			//tImpactReco->Branch("pids",m_pids,"m_pids[m_Ntracks]/I");
			//tImpactReco->Branch("pids",m_pids,"m_pids[m_Ntracks]/D");
			*/
			tImpactReco->Branch("FirstImpactPID",&m_FirstImpactPID,"m_FirstImpactPID/I");
			tImpactReco->Branch("xImpactElec",&m_xImpactElec,"m_xImpactElec/D");
      tImpactReco->Branch("yImpactElec",&m_yImpactElec,"m_yImpactElec/D");
			tImpactReco->Branch("xImpactPositron",&m_xImpactPositron,"m_xImpactPositron/D");
      tImpactReco->Branch("yImpactPositron",&m_yImpactPositron,"m_yImpactPositron/D");
    }

	m_event=0;
}

BesZddDigitizer::~BesZddDigitizer()
{
  std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 0" << std::endl;
  
//   chargeChannelFile->cd();
//   tEDepChargeChannel->Write();
//   chargeChannelFile->Close();
//   tEDepChargeChannel=NULL;
	
  if(impactReco)
    {
      fFile->cd();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 1" << std::endl;
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 2" << std::endl;
      tImpactReco->Write();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 3" << std::endl;
      fFile->Write();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 4" << std::endl;
      fFile->Close();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 5" << std::endl;

      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 6" << std::endl;
      tImpactReco = NULL;
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 7" << std::endl;
      fFile = NULL;
    }
  std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer end " << std::endl;
	
/*	
if(impactReco)
    {
      fFile->cd();
      fFile->Write();
      fFile->Close();
    }
*/		
}

void BesZddDigitizer::Initialize()
{
/*
  m_partID = 0;
  m_detectorID = 0;
  m_crystalNo = 0;
*/
  m_eDep = 0.;
  m_chargeChannel = 0.;
  
	m_zdd_energy=0.;m_zddRight_energy=0.;m_zddLeft_energy=0.;
	
  m_eDep101Right = 0.; m_eDep102Right = 0.; m_eDep103Right = 0.; m_eDep104Right = 0.;
  m_eDep111Right = 0.; m_eDep112Right = 0.; m_eDep113Right = 0.; m_eDep114Right = 0.;
  m_eDep121Right = 0.; m_eDep122Right = 0.; m_eDep123Right = 0.; m_eDep124Right = 0.;
  m_eDep201Right = 0.; m_eDep202Right = 0.; m_eDep203Right = 0.; m_eDep204Right = 0.;
  m_eDep211Right = 0.; m_eDep212Right = 0.; m_eDep213Right = 0.; m_eDep214Right = 0.;
  m_eDep221Right = 0.; m_eDep222Right = 0.; m_eDep223Right = 0.; m_eDep224Right = 0.;
  m_chargeChannel101Right = 0; m_chargeChannel102Right = 0; m_chargeChannel103Right = 0; m_chargeChannel104Right = 0;
  m_chargeChannel111Right = 0; m_chargeChannel112Right = 0; m_chargeChannel113Right = 0; m_chargeChannel114Right = 0;
  m_chargeChannel121Right = 0; m_chargeChannel122Right = 0; m_chargeChannel123Right = 0; m_chargeChannel124Right = 0;
  m_chargeChannel201Right = 0; m_chargeChannel202Right = 0; m_chargeChannel203Right = 0; m_chargeChannel204Right = 0;
  m_chargeChannel211Right = 0; m_chargeChannel212Right = 0; m_chargeChannel213Right = 0; m_chargeChannel214Right = 0;
  m_chargeChannel221Right = 0; m_chargeChannel222Right = 0; m_chargeChannel223Right = 0; m_chargeChannel224Right = 0;

	m_eDep101Left = 0.; m_eDep102Left = 0.; m_eDep103Left = 0.; m_eDep104Left = 0.;
  m_eDep111Left = 0.; m_eDep112Left = 0.; m_eDep113Left = 0.; m_eDep114Left = 0.;
  m_eDep121Left = 0.; m_eDep122Left = 0.; m_eDep123Left = 0.; m_eDep124Left = 0.;
  m_eDep201Left = 0.; m_eDep202Left = 0.; m_eDep203Left = 0.; m_eDep204Left = 0.;
  m_eDep211Left = 0.; m_eDep212Left = 0.; m_eDep213Left = 0.; m_eDep214Left = 0.;
  m_eDep221Left = 0.; m_eDep222Left = 0.; m_eDep223Left = 0.; m_eDep224Left = 0.;
  m_chargeChannel101Left = 0; m_chargeChannel102Left = 0; m_chargeChannel103Left = 0; m_chargeChannel104Left = 0;
  m_chargeChannel111Left = 0; m_chargeChannel112Left = 0; m_chargeChannel113Left = 0; m_chargeChannel114Left = 0;
  m_chargeChannel121Left = 0; m_chargeChannel122Left = 0; m_chargeChannel123Left = 0; m_chargeChannel124Left = 0;
  m_chargeChannel201Left = 0; m_chargeChannel202Left = 0; m_chargeChannel203Left = 0; m_chargeChannel204Left = 0;
  m_chargeChannel211Left = 0; m_chargeChannel212Left = 0; m_chargeChannel213Left = 0; m_chargeChannel214Left = 0;
  m_chargeChannel221Left = 0; m_chargeChannel222Left = 0; m_chargeChannel223Left = 0; m_chargeChannel224Left = 0;
	
	m_eDep101 = 0.; m_eDep102 = 0.; m_eDep103 = 0.; m_eDep104 = 0.;
  m_eDep111 = 0.; m_eDep112 = 0.; m_eDep113 = 0.; m_eDep114 = 0.;
  m_eDep121 = 0.; m_eDep122 = 0.; m_eDep123 = 0.; m_eDep124 = 0.;
  m_eDep201 = 0.; m_eDep202 = 0.; m_eDep203 = 0.; m_eDep204 = 0.;
  m_eDep211 = 0.; m_eDep212 = 0.; m_eDep213 = 0.; m_eDep214 = 0.;
  m_eDep221 = 0.; m_eDep222 = 0.; m_eDep223 = 0.; m_eDep224 = 0.;
  m_chargeChannel101 = 0; m_chargeChannel102 = 0; m_chargeChannel103 = 0; m_chargeChannel104 = 0;
  m_chargeChannel111 = 0; m_chargeChannel112 = 0; m_chargeChannel113 = 0; m_chargeChannel114 = 0;
  m_chargeChannel121 = 0; m_chargeChannel122 = 0; m_chargeChannel123 = 0; m_chargeChannel124 = 0;
  m_chargeChannel201 = 0; m_chargeChannel202 = 0; m_chargeChannel203 = 0; m_chargeChannel204 = 0;
  m_chargeChannel211 = 0; m_chargeChannel212 = 0; m_chargeChannel213 = 0; m_chargeChannel214 = 0;
  m_chargeChannel221 = 0; m_chargeChannel222 = 0; m_chargeChannel223 = 0; m_chargeChannel224 = 0;
	
	m_zImpact=0;
	 
	m_xImpactElec=-99;  m_yImpactElec=-99;
	m_xImpactPositron=-99;  m_yImpactPositron=-99;
 smearFlag = false; // energy/time smearing on ?

 if(smearFlag) G4cout << "ZDDSIM:: BesZddDigitizer: digi smearing ON! " << G4endl;
 // else G4cout << "ZDDSIM:: BesZddDigitizer: digi smearing OFF! " << G4endl;
  
}

void BesZddDigitizer::Digitize()
{
	//printf("BesZddDigitizer::Event #%d \n", m_event);
	m_event++;
//   std::cout<<"BesZddDigitizer::Digitize()\n";
  G4bool debug = false; // couts in Digitizer?
  
  Initialize();

	//MC-Truth informations
	BesSensitiveManager* sensitiveManager = BesSensitiveManager::GetSensitiveManager();
	vector<BesTruthTrack*> *trackList = sensitiveManager->GetTrackList();
	G4int nTrack = trackList->size();
	BesTruthTrack* track;
	
	vector<BesTruthVertex*>* vertexList =  sensitiveManager->GetVertexList();
  G4int nVertex = vertexList->size();
  BesTruthVertex* vertex;
	
	//arrange TruthTrack in trackList in order of trackIndex
  for(int i=0;i<nTrack-1;i++){
  	for(int j=i+1;j<nTrack;j++){
    	if((*trackList)[i]->GetIndex()>(*trackList)[j]->GetIndex())
    	{
      track=(*trackList)[i];
      (*trackList)[i]=(*trackList)[j];
      (*trackList)[j]=track;
    	}
		}
	}	
	
	/*
	Event::McParticleCol *particleCol = new Event::McParticleCol;
  
  //loop over tracks
  for(int i=0;i<nTrack;i++)
  {
    track = (*trackList) [i];
    
    // find out the start point
    bool isPrimary = false;
    bool startPointFound = false;
    BesTruthVertex* startPoint;
    
    for (int i=0;i<nVertex;i++) 
    {
      vertex = (*vertexList) [i];
      if ( track->GetVertex()->GetIndex() == vertex->GetIndex() ) 
      {
        if (!vertex->GetParentTrack()) isPrimary = true;
        startPointFound = true;
        startPoint = vertex;
        break;
      }
    }
	*/
			
  //chargeChannel threshold per crystal for digitization
  G4double threshold = 0.0;
  
  m_besZddDigisCollection = new BesZddDigisCollection(moduleName, collectionName[0]);
  G4DigiManager* DigiMan = G4DigiManager::GetDMpointer();
  
  //hits collection ID
  G4int THCID;
  THCID = DigiMan->GetHitsCollectionID("BesZddHitsCollection");
 
  //  G4cout << "ZddSim :: BesZddDigitizer : THCID: " << THCID << " " << &THCID <<  G4endl;
  
  //hits collection
  BesZddHitsCollection* THC = 0;
  THC = (BesZddHitsCollection*) (DigiMan->GetHitsCollection(THCID));

  //  G4cout << " ZddSim :: BesZddDigitizer : THC: " << THC << " " << &THC << G4endl;
	int index=0;
	int index2=0;
  if(THC) {

    /*     
      //fill zdd Ntuple
      if(m_G4Svc->ZddRootFlag())
	{
	  m_PartID = partID;
	  m_DetectorID = detectorID;
	  m_CrystalNo = crystalNo;
	  m_ChargeChannel = chargeChannel;
	  m_TimeChannel = timeChannel;
	  m_tupleZdd->write();
	}
    */
 
    G4cout << " if(THC) 1 " << G4endl;

    BesZddDigit *m_aDigit = new BesZddDigit();
    //    G4cout << " if(THC) 2 " << G4endl;
    G4int n_hit = THC->entries();
    //G4cout << " if(THC) 3 " << G4endl;
    
		//m_Ntracks=(int)trackList->size();
		m_Ntracks=0;
		int hitPDGcode=0;
		int hitTrackID=0;
    for(G4int i = 0; i < n_hit; i++) 
      {
	BesZddHit* hit = (*THC)[i];	
	m_aDigit->SetHit( hit );
	//printf("%d %d %d \n", i,hit->GetTrackID(),hit->GetPDGCode());
			
			if(i==0) m_trackID=hit->GetTrackID();
			//cout << hit->GetXImpact() << endl;
			/*
			track=(*trackList)[hit->GetTrackID()];
			vertex=track->GetVertex();
			//printf("PRIMARY \n");
			//if (!vertex->GetParentTrack()) printf("PRIMARY \n");
			//Check if primary;
			//int Isprimary=0;
			printf("index Hit collection : %d hit->GetTrackID() : %d hit->GetPDGCode() : %d track->GetPDGCode () : %d \n", i,hit->GetTrackID(),hit->GetPDGCode(),track->GetPDGCode ());
			*/
			
			bool bFirstHitTrack=false;
			if(i==0)
				{
				bFirstHitTrack=true;				
				}
			//else if(i>0 && hitTrackID!=hit->GetTrackID())
			//	{
			//	bFirstHitTrack=true;
			//	}
			else{bFirstHitTrack=false;}
			hitTrackID=hit->GetTrackID();
			hitPDGcode=hit->GetPDGCode();					
			//printf("%d %d %d \n",i,hit->GetTrackID(),hit->GetPDGCode());			
			//cout << "trackList->size()"<< trackList->size() << endl;

			for(int k=0;k<trackList->size();k++)
				{
				track=(*trackList)[k];
				//printf("%d %d %d \n",track->GetIndex(),hit->GetTrackID(),hit->GetPDGCode());
				//printf("index Hit collection : %d hit->GetTrackID() : %d hit->GetPDGCode() : %d track->GetG4TrackId() : %d track->GetPDGCode () : %d \n", i,hit->GetTrackID(),hit->GetPDGCode(),track->GetG4TrackId(),track->GetPDGCode ());
				//if(track->GetIndex()==hit->GetTrackID())				
				//m_xImpacts[k]=-99;
				if(bFirstHitTrack && track->GetG4TrackId()==hit->GetTrackID())
					{
					//printf("index Hit collection : %d hit->GetTrackID() : %d hit->GetPDGCode() : %d track->GetPDGCode () : %d \n", i,hit->GetTrackID(),hit->GetPDGCode(),track->GetPDGCode ());
					
					//printf("%f %f %d\n",hit->GetXImpact(),hit->GetYImpact(),hit->GetPDGCode());
					double x = (double)hit->GetXImpact();
					double y = (double)hit->GetYImpact();
					int		pid = (int)hit->GetPDGCode();
					//m_xImpacts[k]=hit->GetXImpact();
					
					//m_xImpacts[index]=hit->GetXImpact();
					//m_yImpacts[index]=hit->GetYImpact();
					//m_pids[index]=hit->GetPDGCode();
					
					//m_xImpacts[index]=x;
					//m_yImpacts[index]=y;
					//m_pids[index]=pid;
					//cout << index << endl;
					
					
					vertex=track->GetVertex();
					if (!vertex->GetParentTrack()) 
						{
						//printf("");
						if(pid==11)
							{
							m_FirstImpactPID=pid;
							m_xImpactElec=x;
							m_yImpactElec=y;
							}
						else if(pid==-11)
							{
							m_FirstImpactPID=pid;
							m_xImpactPositron=x;
							m_yImpactPositron=y;
							}
						else if(pid==22)
							{
							m_FirstImpactPID=pid;
							m_xImpact=x;
							m_yImpact=y;
							}	
						}
					
					index++;
					}
				}
				
				
			
				
      }
		/*	
		m_Ntracks=(int)trackList->size();
		for(int k=0;k<trackList->size();k++)
				{
				track=(*trackList)[k];
				BesZddHit* hit = (*THC)[0];
					if(track->GetG4TrackId()==hit->GetTrackID())
						{
						//m_xImpacts[k]=hit->GetXImpact();
						//printf("%d %f \n",k,hit->GetXImpact());
						}	
				}
		*/		
    m_aDigit->EndOfEvent();
    //    G4cout << " if(THC) 4 " << G4endl;

    if( m_aDigit->GetChargeChannel_101() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 1 );
      digi->SetDetectorID( 1 );
      digi->SetCrystalNo( 101 );
      G4int chargeChannel = m_aDigit->GetChargeChannel_101() ;
      G4int timeChannel = m_aDigit->GetTimeChannel_101() ;
      
      m_chargeChannel = chargeChannel;
      m_eDep = m_aDigit->totEDep_101;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " -101 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
      digi->SetChargeChannel( chargeChannel );
      digi->SetTimeChannel( timeChannel ) ;
			if(impactReco)
				{
	  		m_eDep101Left = m_eDep;
	  		m_chargeChannel101Left = m_chargeChannel;
				m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}
      //Smear();  
      m_besZddDigisCollection->insert(digi);
	
     }

    if( m_aDigit->GetChargeChannel_102() >= threshold) // if chargeChannel above channel threshold  
    {
            G4cout << "EDep crystal -102: " << m_aDigit->GetChargeChannel_102() << G4endl;
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 102 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_102() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_102() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_102;
// //tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -102 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
				{
	  		m_eDep102Left = m_eDep;
	  		m_chargeChannel102Left = m_chargeChannel;
				m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}
			
	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( m_aDigit->GetChargeChannel_103() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 103 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_103() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_103() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_103;
// //tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -103 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
				
	if(impactReco)
			{
	  	m_eDep103Left = m_eDep;
	  	m_chargeChannel103Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}
			
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_104() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 104 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_104() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_104() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_104;
// //tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -104 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
				
	if(impactReco)
			{
	  	m_eDep104Left = m_eDep;
	  	m_chargeChannel104Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}
	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_111() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID ( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 111 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_111() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_111() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_111;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -111 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep111Left = m_eDep;
	  	m_chargeChannel111Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}
			
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel_112() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 112 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_112() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_112() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_112;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -112 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep112Left = m_eDep;
	  	m_chargeChannel112Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_113() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 113 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_113() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_113() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_113;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -113 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep113Left = m_eDep;
	  	m_chargeChannel113Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}	
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_114() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 114 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_114() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_114() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_114;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -114 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep114Left = m_eDep;
	  	m_chargeChannel114Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_121() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 121 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_121() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_121() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_121;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -121 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep121Left = m_eDep;
	  	m_chargeChannel121Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel_122() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 122 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_122() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_122() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_122;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -122 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep122Left = m_eDep;
	  	m_chargeChannel122Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_123() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 123 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_123() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_123() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_123;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -123 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep123Left = m_eDep;
	  	m_chargeChannel123Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_124() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 124 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_124() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_124() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_124;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -124 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep124Left = m_eDep;
	  	m_chargeChannel124Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    // detectorID = 2 (lower crystals)
    if( m_aDigit->GetChargeChannel_201() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 1 );
      digi->SetDetectorID( 2 );
      digi->SetCrystalNo( 201 );
      G4int chargeChannel = m_aDigit->GetChargeChannel_201() ;
      G4int timeChannel = m_aDigit->GetTimeChannel_201() ;

      m_chargeChannel = chargeChannel;
      m_eDep = m_aDigit->totEDep_201;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " -201 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
			if(impactReco)
			{
	  	m_eDep201Left = m_eDep;
	  	m_chargeChannel201Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}	
      //Smear();  
      m_besZddDigisCollection->insert(digi);
    }

    if( m_aDigit->GetChargeChannel_202() >= threshold) // if chargeChannel above channel threshold   
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 202 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_202() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_202() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_202;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -202 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep202Left = m_eDep;
	  	m_chargeChannel202Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( m_aDigit->GetChargeChannel_203() >= threshold) // if chargeChannel above channel threshold 
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 203 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_203() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_203() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_203;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -203 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep203Left = m_eDep;
	  	m_chargeChannel203Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_204() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 204 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_204() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_204() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_204;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -204 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep204Left = m_eDep;
	  	m_chargeChannel204Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_211() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 211 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_211() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_211() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_211;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -211 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep211Left = m_eDep;
	  	m_chargeChannel211Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel_212() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 212 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_212() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_212() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_212;
//tEDepChargeChannel->Fill();

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(debug){ std::cout << " -212 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	
	if(impactReco)
			{
	  	m_eDep212Left = m_eDep;
	  	m_chargeChannel212Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}
			
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_213() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 213 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_213() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_213() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_213;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -213 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep213Left = m_eDep;
	  	m_chargeChannel213Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_214() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 214 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_214() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_214() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_214;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -214 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep214Left = m_eDep;
	  	m_chargeChannel214Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
  m_besZddDigisCollection->insert(digi);
						
      }
    if( m_aDigit->GetChargeChannel_221() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 221 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_221() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_221() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_221;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -221 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep221Left = m_eDep;
	  	m_chargeChannel221Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			 
	m_besZddDigisCollection->insert(digi);
			
      }
			
					
    if( m_aDigit->GetChargeChannel_222() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 222 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_222() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_222() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_222;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -222 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	if(impactReco)
			{
	  	m_eDep222Left = m_eDep;
	  	m_chargeChannel222Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			
	m_besZddDigisCollection->insert(digi);
			
      }
    if( m_aDigit->GetChargeChannel_223() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 223 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_223() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_223() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_223;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -223 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep223Left = m_eDep;
	  	m_chargeChannel223Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			 
	m_besZddDigisCollection->insert(digi);
      }      
      if( m_aDigit->GetChargeChannel_224() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 224 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_224() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_224() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_224;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -224 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(impactReco)
			{
	  	m_eDep224Left = m_eDep;
	  	m_chargeChannel224Left = m_chargeChannel;
			m_zddLeft_energy = m_zddLeft_energy+m_eDep;
			}			 
	m_besZddDigisCollection->insert(digi);
      }
      //! part = 2 => right part
    if( m_aDigit->GetChargeChannel101() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 2 );
      digi->SetDetectorID( 1 );
      digi->SetCrystalNo( 101 );
      G4int chargeChannel = m_aDigit->GetChargeChannel101() ;
      G4int timeChannel = m_aDigit->GetTimeChannel101() ;

      m_chargeChannel = chargeChannel;
      m_eDep = m_aDigit->totEDep101;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " 101 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
      digi->SetChargeChannel( chargeChannel );
      digi->SetTimeChannel( timeChannel ) ;

      if(impactReco)
	{
	  m_eDep101Right = m_eDep;
	  m_chargeChannel101Right = m_chargeChannel;
		m_zddRight_energy = m_zddRight_energy+m_eDep;
	}


      //Smear();  
      m_besZddDigisCollection->insert(digi);

     }

    if( m_aDigit->GetChargeChannel102() >= threshold) // if chargeChannel above channel threshold  
    {
      //      G4cout << "EDep crystal -102: " << m_aDigit->GetChargeChannel102() << G4endl;
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 102 );
	G4int chargeChannel = m_aDigit->GetChargeChannel102() ;
	G4int timeChannel = m_aDigit->GetTimeChannel102() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep102;

//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 102 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep102Right = m_eDep;
	    m_chargeChannel102Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( m_aDigit->GetChargeChannel103() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 103 );
	G4int chargeChannel = m_aDigit->GetChargeChannel103() ;
	G4int timeChannel = m_aDigit->GetTimeChannel103() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep103;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 103 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep103Right = m_eDep;
	    m_chargeChannel103Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel104() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 104 );
	G4int chargeChannel = m_aDigit->GetChargeChannel104() ;
	G4int timeChannel = m_aDigit->GetTimeChannel104() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep104;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 104 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep104Right = m_eDep;
	    m_chargeChannel104Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel111() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID ( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 111 );
	G4int chargeChannel = m_aDigit->GetChargeChannel111() ;
	G4int timeChannel = m_aDigit->GetTimeChannel111() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep111;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 111 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep111Right = m_eDep;
	    m_chargeChannel111Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel112() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 112 );
        G4int chargeChannel = m_aDigit->GetChargeChannel112() ;
        G4int timeChannel = m_aDigit->GetTimeChannel112() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep112;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 112 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep112Right = m_eDep;
	    m_chargeChannel112Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel113() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 113 );
        G4int chargeChannel = m_aDigit->GetChargeChannel113() ;
        G4int timeChannel = m_aDigit->GetTimeChannel113() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep113;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 113 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep113Right = m_eDep;
	    m_chargeChannel113Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel114() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 114 );
        G4int chargeChannel = m_aDigit->GetChargeChannel114() ;
        G4int timeChannel = m_aDigit->GetTimeChannel114() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep114;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 114 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep114Right = m_eDep;
	    m_chargeChannel114Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel121() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 121 );
	G4int chargeChannel = m_aDigit->GetChargeChannel121() ;
	G4int timeChannel = m_aDigit->GetTimeChannel121() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep121;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 121 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep121Right = m_eDep;
	    m_chargeChannel121Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel122() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 122 );
        G4int chargeChannel = m_aDigit->GetChargeChannel122() ;
        G4int timeChannel = m_aDigit->GetTimeChannel122() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep122;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 122 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep122Right = m_eDep;
	    m_chargeChannel122Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel123() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 123 );
        G4int chargeChannel = m_aDigit->GetChargeChannel123() ;
        G4int timeChannel = m_aDigit->GetTimeChannel123() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep123;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 123 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep123Right = m_eDep;
	    m_chargeChannel123Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel124() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 124 );
        G4int chargeChannel = m_aDigit->GetChargeChannel124() ;
        G4int timeChannel = m_aDigit->GetTimeChannel124() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep124;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 124 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep124Right = m_eDep;
	    m_chargeChannel124Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
        // detectorID = 2 (lower crystals)
    if( m_aDigit->GetChargeChannel201() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 2 );
      digi->SetDetectorID( 2 );
      digi->SetCrystalNo( 201 );
      G4int chargeChannel = m_aDigit->GetChargeChannel201() ;
      G4int timeChannel = m_aDigit->GetTimeChannel201() ;

      m_chargeChannel = chargeChannel;
      m_eDep = m_aDigit->totEDep201;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " 201 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep201Right = m_eDep;
	    m_chargeChannel201Right = m_chargeChannel;
			m_zddRight_energy = m_zddRight_energy+m_eDep;
	  }

      //Smear();  
      m_besZddDigisCollection->insert(digi);
    }

    if( m_aDigit->GetChargeChannel202() >= threshold) // if chargeChannel above channel threshold   
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 202 );
	G4int chargeChannel = m_aDigit->GetChargeChannel202() ;
	G4int timeChannel = m_aDigit->GetTimeChannel202() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep202;
//tEDepChargeChannel->Fill();

	if(debug) std::cout << " BesZddDigitizer.cc : totEdep202 : " << m_eDep << std::endl;

	if(debug){ std::cout << " 202 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep202Right = m_eDep;
            m_chargeChannel202Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( m_aDigit->GetChargeChannel203() >= threshold) // if chargeChannel above channel threshold 
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 203 );
	G4int chargeChannel = m_aDigit->GetChargeChannel203() ;
	G4int timeChannel = m_aDigit->GetTimeChannel203() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep203;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 203 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep203Right = m_eDep;
            m_chargeChannel203Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel204() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 204 );
	G4int chargeChannel = m_aDigit->GetChargeChannel204() ;
	G4int timeChannel = m_aDigit->GetTimeChannel204() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep204;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 204 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep204Right = m_eDep;
            m_chargeChannel204Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel211() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 211 );
	G4int chargeChannel = m_aDigit->GetChargeChannel211() ;
	G4int timeChannel = m_aDigit->GetTimeChannel211() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep211;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 211 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep211Right = m_eDep;
            m_chargeChannel211Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel212() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 212 );
        G4int chargeChannel = m_aDigit->GetChargeChannel212() ;
        G4int timeChannel = m_aDigit->GetTimeChannel212() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep212;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 212 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep212Right = m_eDep;
            m_chargeChannel212Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel213() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 213 );
        G4int chargeChannel = m_aDigit->GetChargeChannel213() ;
        G4int timeChannel = m_aDigit->GetTimeChannel213() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep213;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 213 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep213Right = m_eDep;
            m_chargeChannel213Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel214() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 214 );
        G4int chargeChannel = m_aDigit->GetChargeChannel214() ;
        G4int timeChannel = m_aDigit->GetTimeChannel214() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep214;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 214 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep214Right = m_eDep;
            m_chargeChannel214Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	//	Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel221() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 221 );
	G4int chargeChannel = m_aDigit->GetChargeChannel221() ;
	G4int timeChannel = m_aDigit->GetTimeChannel221() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep221;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 221 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep221Right = m_eDep;
            m_chargeChannel221Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	m_besZddDigisCollection->insert(digi);
      }
      if( m_aDigit->GetChargeChannel222() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 222 );
	G4int chargeChannel = m_aDigit->GetChargeChannel222() ;
	G4int timeChannel = m_aDigit->GetTimeChannel222() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep222;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 222 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep222Right = m_eDep;
            m_chargeChannel222Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel223() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 223 );
	G4int chargeChannel = m_aDigit->GetChargeChannel223() ;
	G4int timeChannel = m_aDigit->GetTimeChannel223() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep223;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 223 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep223Right = m_eDep;
            m_chargeChannel223Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	m_besZddDigisCollection->insert(digi);
      }      
      if( m_aDigit->GetChargeChannel224() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 224 );
	G4int chargeChannel = m_aDigit->GetChargeChannel224() ;
	G4int timeChannel = m_aDigit->GetTimeChannel224() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep224;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 224 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep224Right = m_eDep;
            m_chargeChannel224Right = m_chargeChannel;
						m_zddRight_energy = m_zddRight_energy+m_eDep;
          }

	m_besZddDigisCollection->insert(digi);
      }
        
    StoreDigiCollection(m_besZddDigisCollection);	

    if(impactReco)
      {
	m_xImpact = m_aDigit->GetXImpact();
	m_yImpact = m_aDigit->GetYImpact();
	m_zImpact = m_aDigit->GetZImpact();

	m_PxImpact = m_aDigit->GetPxImpact();
	m_PyImpact = m_aDigit->GetPyImpact();
	m_PzImpact = m_aDigit->GetPzImpact();
	
	m_eDep101=m_eDep101Left+m_eDep101Right;
	m_eDep102=m_eDep102Left+m_eDep102Right;
	m_eDep103=m_eDep103Left+m_eDep103Right;
	m_eDep104=m_eDep104Left+m_eDep104Right;
	
	m_eDep111=m_eDep111Left+m_eDep111Right;
	m_eDep112=m_eDep112Left+m_eDep112Right;
	m_eDep113=m_eDep113Left+m_eDep113Right;
	m_eDep114=m_eDep114Left+m_eDep114Right;
	
	m_eDep121=m_eDep121Left+m_eDep121Right;
	m_eDep122=m_eDep122Left+m_eDep122Right;
	m_eDep123=m_eDep123Left+m_eDep123Right;
	m_eDep124=m_eDep124Left+m_eDep124Right;
	
	m_eDep201=m_eDep201Left+m_eDep201Right;
	m_eDep202=m_eDep202Left+m_eDep202Right;
	m_eDep203=m_eDep203Left+m_eDep203Right;
	m_eDep204=m_eDep204Left+m_eDep204Right;
	
	m_eDep211=m_eDep211Left+m_eDep211Right;
	m_eDep212=m_eDep212Left+m_eDep212Right;
	m_eDep213=m_eDep213Left+m_eDep213Right;
	m_eDep214=m_eDep214Left+m_eDep214Right;
	
	m_eDep221=m_eDep221Left+m_eDep221Right;
	m_eDep222=m_eDep222Left+m_eDep222Right;
	m_eDep223=m_eDep223Left+m_eDep223Right;
	m_eDep224=m_eDep224Left+m_eDep224Right;
		
	m_chargeChannel101=m_chargeChannel101Left+m_chargeChannel101Right;
	m_chargeChannel102=m_chargeChannel102Left+m_chargeChannel102Right;
	m_chargeChannel103=m_chargeChannel103Left+m_chargeChannel103Right;
	m_chargeChannel104=m_chargeChannel104Left+m_chargeChannel104Right;
	
	m_chargeChannel111=m_chargeChannel111Left+m_chargeChannel111Right;
	m_chargeChannel112=m_chargeChannel112Left+m_chargeChannel112Right;
	m_chargeChannel113=m_chargeChannel113Left+m_chargeChannel113Right;
	m_chargeChannel114=m_chargeChannel114Left+m_chargeChannel114Right;
	
	m_chargeChannel121=m_chargeChannel121Left+m_chargeChannel121Right;
	m_chargeChannel122=m_chargeChannel122Left+m_chargeChannel122Right;
	m_chargeChannel123=m_chargeChannel123Left+m_chargeChannel123Right;
	m_chargeChannel124=m_chargeChannel124Left+m_chargeChannel124Right;
	
	m_chargeChannel201=m_chargeChannel201Left+m_chargeChannel201Right;
	m_chargeChannel202=m_chargeChannel202Left+m_chargeChannel202Right;
	m_chargeChannel203=m_chargeChannel203Left+m_chargeChannel203Right;
	m_chargeChannel204=m_chargeChannel204Left+m_chargeChannel204Right;
	
	m_chargeChannel211=m_chargeChannel211Left+m_chargeChannel211Right;
	m_chargeChannel212=m_chargeChannel212Left+m_chargeChannel212Right;
	m_chargeChannel213=m_chargeChannel213Left+m_chargeChannel213Right;
	m_chargeChannel214=m_chargeChannel214Left+m_chargeChannel214Right;
	
	m_chargeChannel221=m_chargeChannel221Left+m_chargeChannel221Right;
	m_chargeChannel222=m_chargeChannel222Left+m_chargeChannel222Right;
	m_chargeChannel223=m_chargeChannel223Left+m_chargeChannel223Right;
	m_chargeChannel224=m_chargeChannel224Left+m_chargeChannel224Right;
	
	m_zdd_energy=m_zddRight_energy+m_zddLeft_energy;
	
	tImpactReco->Fill();

	//	fFile->cd();
	//	tEDepChargeChannel->Write();
	//	tImpactReco->Write();
      }      
    delete m_aDigit;
  } 
  //  G4cout << "ZddSim :: BesZddDigitizer : End of Digitization" << G4endl;
  //  int test;
  //  G4cin >> test;
}

G4double BesZddDigitizer::Smear(G4double var)
{ 
//   G4cout << "eDep before smearing: " << G4BestUnit(edep,"Energy") << G4endl;
//   G4cout << "time before smearing: " << G4BestUnit(time,"Time") << G4endl;
  
  G4double gaussrand = G4RandGauss::shoot(0.,0.5);
  return var+(gaussrand*var);
  
//   G4cout << "eDep after smearing:  " << G4BestUnit(edep,"Energy") << G4endl;
//   G4cout << "time after smearing:  " << G4BestUnit(time,"Time") << G4endl;
//   
//   G4cout << "gaussrand1:           " << gaussrand1 << G4endl;
//   G4cout << "gaussrand2:           " << gaussrand2 << G4endl << G4endl;
}

