
//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oreiented Simulation Tool                    //
//---------------------------------------------------------------------------//
//Description: RPC detector 
//Author:  Youzy      Peking University      mail: youzy@hep.pku.cn
//Created: Nov, 2003
//Comment:
//---------------------------------------------------------------------------//
//
#include "BesZddConstruction.hh"
#include "BesZddSD.hh"
#include "ReadBoostRoot.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Color.hh"
#include "G4ios.hh"
#include "globals.hh"
#include <cstdlib>

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <cstdlib>
#include <fstream>

#include "G4DigiManager.hh"
#include "G4VDigitizerModule.hh"
#include "BesZddDigitizer.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4SubtractionSolid.hh"

G4double const BesZddConstruction::sCrystalBoxSizeX = 4.*cm; 
G4double const BesZddConstruction::sCrystalBoxSizeY = 3.*cm; 
G4double const BesZddConstruction::sCrystalBoxSizeZ = 14.*cm; //14
    
G4double const BesZddConstruction::sCrystalX = 1.*cm;
G4double const BesZddConstruction::sCrystalY = 1.*cm;
G4double const BesZddConstruction::sCrystalZ = 14.*cm; //14, there is a space of maximum 18cm

//LEO: shift position in x direction (was: 0)
G4ThreeVector const BesZddConstruction::slUpperCrystalBox = G4ThreeVector(1.*cm,1.5*cm,-342.*cm);  // 1.*cm,2.*cm,+-342.*cm is the center of the
G4ThreeVector const BesZddConstruction::slLowerCrystalBox = G4ThreeVector(1.*cm,-1.5*cm,-342.*cm); // ISR photon angular distribution, 0,2.,+-342 the
G4ThreeVector const BesZddConstruction::srUpperCrystalBox = G4ThreeVector(1.*cm,1.5*cm,342.*cm);   // the center of the ZDD
G4ThreeVector const BesZddConstruction::srLowerCrystalBox = G4ThreeVector(1.*cm,-1.5*cm,342.*cm);

std::string make_ObjectName( const std::string& basename, G4int i, G4int k, const std::string& nameaende)
{
  std::ostringstream result;
  result << basename << i << k << nameaende;

  return result.str();
}

BesZddConstruction::BesZddConstruction()
  : solidWorld(0),logicZdd(0),physicZdd(0)
{
  // default parameter values of the crystal box volume

  // materials
  DefineMaterials();
  //SetCrystalMaterial("PbWO4");
	SetCrystalMaterial("LYSO");
  // create commands for interactive definition of the calorimeter
  //detectorMessenger = new DetectorMessenger(this);
  WorldSizeX = 50.*cm;
  WorldSizeY = 50.*cm;
  WorldSizeZ = 720.*cm;//720.*cm;

}


BesZddConstruction::~BesZddConstruction()
{
  //  delete aZddSD;
  //  G4DigiManager* G4_DM = G4DigiManager::GetDMpointer();
  //  G4VDigitizerModule* ZddDigitizer = G4_DM->FindDigitizerModule("BesZddDigitizer");
  //  delete ZddDigitizer;
}

void BesZddConstruction::DefineMaterials()
{
  //defining Vacuum properties
  G4double atomicNumber = 1.;
  G4double massOfMole = 1.008*g/mole;
  G4double density = 1.e-25*g/cm3;
  G4double temperature = 2.73*kelvin;
  G4double pressure = 3.e-18*pascal;
  
  G4Material* Vacuum = new G4Material("Vacuum", atomicNumber, massOfMole, density, kStateGas, temperature, pressure);

  // defining Materials
  G4double a;  // atomic mass
  G4double z;  // atomic number
  G4double weightRatio; //
  G4String name, symbol;
  G4int ncomponents, natoms;

  // Elements
  G4Element* elO = new G4Element(name="Oxygen", symbol="O", z=8., a=15.9994*g/mole);
  G4Element* elW = new G4Element(name="Tungsten", symbol="W", z=74., a=183.84*g/mole);
  G4Element* elPb = new G4Element(name="Lead", symbol="Pb" ,z=82., a=207.19*g/mole);
	G4Element* elLu = new G4Element(name="Lutetium", symbol="Lu", z=71.,a=174.97*g/mole);
	G4Element* elY = new G4Element(name="Yttrium", symbol="Y", z=39., a=88.91*g/mole);
	G4Element* elSi = new G4Element(name="Silicon", symbol="Si", z=14.,a=28.09*g/mole);
	
  // PbWO4
  density = 8.280*g/cm3;
  G4Material* PbWO4 = new G4Material("PbWO4", density,ncomponents=3);
  
  PbWO4->AddElement(elPb, natoms=1);
  PbWO4->AddElement(elW, natoms=1);
  PbWO4->AddElement(elO, natoms=4);
  
	//LYSO
	density = 7.4*g/cm3;
	G4double aLYSO=1.9*(elLu->GetA())+0.1*(elY->GetA())+(elSi->GetA())+5*(elO->GetA());
	G4double fracmassLu=1.9*(elLu->GetA())/aLYSO;
	G4double fracmassY=0.1*(elY->GetA())/aLYSO;
	G4double fracmassSi=(elSi->GetA())/aLYSO;
	G4double fracmassO=5*(elO->GetA())/aLYSO;
	G4Material* LYSO = new G4Material("LYSO", density, ncomponents=4);
	LYSO->AddElement(elLu,fracmassLu);
	LYSO->AddElement(elY,fracmassY);
	LYSO->AddElement(elSi,fracmassSi);
	LYSO->AddElement(elO,fracmassO);
	
  defaultMaterial = Vacuum;

  const G4int NUMENTRIES = 9;

}

void BesZddConstruction::Construct( G4LogicalVolume* logicBes )
{
  //construct Zdd
 
  //     
  // ZDD World
  //
//  G4Box* solidWorld_all = new G4Box("World_all",//its name
//			 WorldSizeX/2.,WorldSizeY/2.,WorldSizeZ/2.);//its size
  G4Box* solidWorld_all = new G4Box("World_all",//its name
			 25*cm, 25*cm, 360*cm);//its size
 

  G4Box* solidWorld_sub = new G4Box ("World_sub",//its name
			25*cm, 25*cm, 335*cm);

  solidWorld = new G4SubtractionSolid("World", solidWorld_all, solidWorld_sub);


/*

G4UnionSolid b1UnionC1("Box+Cylinder", &box1, &Cylinder1); 
  G4IntersectionSolid b1IntersC1("Box Intersect Cylinder", &box1, &Cylinder1); 
  G4SubtractionSolid b1minusC1("Box-Cylinder", &box1, &Cylinder1);
*/ 

                       
  logicZdd = new G4LogicalVolume(solidWorld,//its solid
                                   defaultMaterial,//its material
                                   "logicZdd");//its name

 logicZdd->SetVisAttributes (G4VisAttributes::Invisible);

                                   
  physicZdd = new G4PVPlacement(0,//no rotation
				 G4ThreeVector(),//at (0,0,0)
                                 logicZdd,//its logical volume 
                                 "physicZdd",//its name
                                 logicBes, //its mother  volume
                                 false,//no boolean operation
                                 0);//copy number

  G4Colour Yellow(1.,1.,0.);
  G4Colour Blue(0.,0.,1.);
  G4Colour Red(1.,0.,0.);

  // -----------------------------------------------------------------------------------------------
  // ---------------------------------------crystals------------------------------------------------
  // -----------------------------------------------------------------------------------------------

  G4Box* crystalBox = new G4Box("crystalBox",sCrystalBoxSizeX/2.,sCrystalBoxSizeY/2.,sCrystalBoxSizeZ/2.);


  G4LogicalVolume* rightUpperCrystalBox_log = new G4LogicalVolume(crystalBox, //shape and dimension 
							 defaultMaterial, //material
							 "rightUpperCrystalBox_log", //name
							 0, //User Limits
							 0, //User Limits
							 0);//User Limits
  rightUpperCrystalBox_log->SetVisAttributes (G4VisAttributes::Invisible);


  G4LogicalVolume* rightLowerCrystalBox_log = new G4LogicalVolume(crystalBox, //shape and dimension 
							 defaultMaterial, //material
							 "rightLowerCrystalBox_log", //name
							 0, //User Limits
							 0, //User Limits
							 0);//User Limits      
  rightLowerCrystalBox_log->SetVisAttributes (G4VisAttributes::Invisible);

  G4LogicalVolume* leftUpperCrystalBox_log = new G4LogicalVolume(crystalBox, //shape and dimension                                                                                                                                                   
                                                         defaultMaterial, //material                                                                                                                                                         
                                                         "leftUpperCrystalBox_log", //name                                                                                                                                                           
                                                         0, //User Limits                                                                                                                                                                    
                                                         0, //User Limits                                                                                                                                                                    
                                                         0);//User Limits                                                                                                                                                                    
  leftUpperCrystalBox_log->SetVisAttributes (G4VisAttributes::Invisible);

  G4LogicalVolume* leftLowerCrystalBox_log = new G4LogicalVolume(crystalBox, //shape and dimension                                                                                                                                                   
                                                         defaultMaterial, //material                                                                                                                                                         
                                                         "leftLowerCrystalBox_log", //name                                                                                                                                                           
                                                         0, //User Limits                                                                                                                                                                    
                                                         0, //User Limits                                                                                                                                                                    
                                                         0);//User Limits                                                                                                                                                                    
  leftLowerCrystalBox_log->SetVisAttributes (G4VisAttributes::Invisible);

  // default parameter values of the crystal box volume
  G4double CrystalBoxSizeX = sCrystalBoxSizeX;
  G4double CrystalBoxSizeY = sCrystalBoxSizeY;
  G4double CrystalBoxSizeZ = sCrystalBoxSizeZ;

  // dimensions of a single crystal
  G4double crystalX = sCrystalX; //cm
  G4double crystalY = sCrystalY; //cm 
  G4double crystalZ = sCrystalZ; //cm

 
  G4Box* crystal = new G4Box("crystal",sCrystalX/2.,sCrystalY/2.,sCrystalZ/2.);

  crystal_log = new G4LogicalVolume(crystal,       // shape and dimension
				    CrystalMaterial,         // material
				    "crystal_log",// name
				    0,// User Limits...
				    0,
				    0);
     
  crystal_log->SetVisAttributes(new G4VisAttributes(Yellow));
  //crystal_log->SetVisAttributes (G4VisAttributes::Invisible);

  //  * * * * * * * * * * * positioning optimization for crystals * * * * * * * * * * * * * *  * 

  G4double crystalCrossArea = sCrystalX * sCrystalY;
  const G4float availableArea = 13.5; // in cm2
  G4int nOfCrystals = (G4int)availableArea/crystalCrossArea;
  const G4int nOfRows = (G4int) sCrystalBoxSizeX/sCrystalX;
  const G4int nOfLines = (G4int) sCrystalBoxSizeY/sCrystalY;

  G4double crystalPosX[4]; // number of rows
  G4double crystalPosY[3]; // number of lines

  SetNOfCrystals(nOfRows*nOfLines*2);

  for(G4int i = 0; i < nOfRows; i++)  crystalPosX[i] = -sCrystalBoxSizeX/2. + (1./2. + i) * sCrystalX;
  for(G4int k = 0; k < nOfLines; k++) crystalPosY[k] = -sCrystalBoxSizeY/2. + (1./2. + k) * sCrystalY;


  std::vector<G4PVPlacement*> upperRightPlacements;
  std::vector<G4PVPlacement*> lowerRightPlacements;

  std::vector<G4PVPlacement*> upperLeftPlacements;
  std::vector<G4PVPlacement*> lowerLeftPlacements;

  G4int test = 0;
  for(G4int k = 0; k < nOfLines; k++)
    {
      for(G4int i = 0; i < nOfRows; i++)
	{
      
	  upperRightPlacements.push_back(      new G4PVPlacement(  0,                                      //rotation
							      G4ThreeVector(crystalPosX[i],crystalPosY[k],0.),   //translation position
							      crystal_log,                                //its logical volume
							      (G4String)make_ObjectName("crystal_1",k,i+1,"_phys"),       //its name,
							      rightUpperCrystalBox_log,                     //its mother volume (=0 for world volume)
							      false,                                  //boolean operations? 
							     (100+10*k+i+1) ) );//copyNo
	  
	  //G4cout << G4endl << "CopyNumber of Crystal: " << upperPlacements[test]->GetCopyNo() << G4endl;
	  test++;
	}
    }
  test = 0;
  for(G4int k = 0; k < nOfLines; k++)
    {
      for(G4int i = 0; i < nOfRows; i++)
	{
	  lowerRightPlacements.push_back(      new G4PVPlacement(  0,                                      //rotation
							      G4ThreeVector(crystalPosX[i],crystalPosY[k],0.),   //translation position
							      crystal_log,                                //its logical volume
							      (G4String)make_ObjectName("crystal_2",nOfLines-1-k,i+1,"_phys"),  //its name
							      rightLowerCrystalBox_log,                     //its mother volume (=0 for world volume)
							      false,                                  //boolean operations? 
							      (200+10*(nOfLines-1-k)+i+1) ) );  //copyNo
	  
	  //G4cout << G4endl << "CopyNumber of Crystal: " << lowerPlacements[test]->GetCopyNo() << G4endl;
	  test++;
	}
    }
  test = 0;   
  for(G4int k = 0; k < nOfLines; k++)
    {
      for(G4int i = 0; i < nOfRows; i++)
        {

          upperLeftPlacements.push_back(      new G4PVPlacement(  0,                                      //rotation                                                                                                                        
								   G4ThreeVector(crystalPosX[i],crystalPosY[k],0.),   //translation position                                                                                                      
								   crystal_log,                                //its logical volume                                                                                                               
								   (G4String)make_ObjectName("crystal_1",k,i+1,"_phys"),       //its name,                                                                                                        
								   leftUpperCrystalBox_log,                     //its mother volume (=0 for world volume)                                                                                                 
								   false,                                  //boolean operations?                                                                                                                  
								   (100+10*k+i+1)*(-1) ));//copyNo                                                                                                                                                        

          //G4cout << G4endl << "CopyNumber of Crystal: " << upperPlacements[test]->GetCopyNo() << G4endl;                                                                                                                                   
          test++;
        }
    }
  test = 0;
  for(G4int k = 0; k < nOfLines; k++)
    {
      for(G4int i = 0; i < nOfRows; i++)
        {
          lowerLeftPlacements.push_back(      new G4PVPlacement(  0,                                      //rotation                                                                                                                        
								   G4ThreeVector(crystalPosX[i],crystalPosY[k],0.),   //translation position                                                                                                      
								   crystal_log,                                //its logical volume                                                                                                               
								   (G4String)make_ObjectName("crystal_2",nOfLines-1-k,i+1,"_phys"),  //its name                                                                                                   
								   leftLowerCrystalBox_log,                     //its mother volume (=0 for world volume)                                                                                                 
								   false,                                  //boolean operations?                                                                                                                  
								   (200+10*(nOfLines-1-k)+i+1)*(-1) ));  //copyNo                                                                                                                                         

          //G4cout << G4endl << "CopyNumber of Crystal: " << lowerPlacements[test]->GetCopyNo() << G4endl;                                                                                                                                   
          test++;
        }
    }

  //!left crystals
  G4PVPlacement* crystalBox1_phys = new G4PVPlacement(0,                                                  //rotation
						      slUpperCrystalBox,                   //translation position
						      leftUpperCrystalBox_log,                                     //its logical volume
						      "crystalBox1_phys",                                  //its name
						      logicZdd,                                      //its mother volume (=0 for world volume)
						      false,                                               //boolean operations? 
						      0);       
 
  G4PVPlacement* crystalBox2_phys = new G4PVPlacement(0,                                                  //rotation
						      slLowerCrystalBox,                   //translation position
						      leftLowerCrystalBox_log,                                     //its logical volume
						      "crystalBox2_phys",                                  //its name
						      logicZdd,                                           //its mother volume (=0 for world volume)
						      false,                                               //boolean operations? 
						      0);     
  
  //! right crystals   
  G4PVPlacement* crystalBox3_phys = new G4PVPlacement(0,                                                  //rotation
						      srUpperCrystalBox,                   //translation position
						      rightUpperCrystalBox_log,                                     //its logical volume
						      "crystalBox3_phys",                                  //its name
						      logicZdd,                                    //its mother volume (=0 for world volume)
						      false,                                               //boolean operations? 
						      0);       
 
  G4PVPlacement* crystalBox4_phys = new G4PVPlacement(0,                                                  //rotation
						      srLowerCrystalBox,                   //translation position
						      rightLowerCrystalBox_log,                                     //its logical volume
						      "crystalBox4_phys",                                  //its name
						      logicZdd,                                    //its mother volume (=0 for world volume)
						      false,                                               //boolean operations? 
						      0);  
 
  //                                        
  // Visualization attributes
  //
  //  logicWorld->SetVisAttributes (G4VisAttributes::Invisible);

  // ---------------------------------------------------------------------------------------------------------
  // ==================================== sensitive detectors ================================================
  // ---------------------------------------------------------------------------------------------------------


  //sensitive detectors
  G4SDManager* aSDman = G4SDManager::GetSDMpointer();
  BesZddSD*    aZddSD = new BesZddSD("BesZddSD", this);
  aSDman->AddNewDetector(aZddSD);
  crystal_log->SetSensitiveDetector( aZddSD );
}


void BesZddConstruction::SetCrystalMaterial(G4String materialChoice)
{
     // search the material by its name 
     G4Material* cryMat = G4Material::GetMaterial(materialChoice);     
     if (cryMat) CrystalMaterial = cryMat;
}

G4int BesZddConstruction::CheckPosition(G4double x, G4double y, G4double z)
{
  //! center coordinates of left upperCrystalBox;
  G4double lUpper_x = slUpperCrystalBox.x()/cm;
  G4double lUpper_y = slUpperCrystalBox.y()/cm;
  G4double lUpper_z = slUpperCrystalBox.z()/cm;
  
  //! center coordianates of left lowerCrystalBox;
  G4double lLower_x = slLowerCrystalBox.x()/cm;
  G4double lLower_y = slLowerCrystalBox.y()/cm;
  G4double lLower_z = slLowerCrystalBox.z()/cm;

  //! center coordinates of right lUpperCrystalBox;
  G4double rUpper_x = srUpperCrystalBox.x()/cm;
  G4double rUpper_y = srUpperCrystalBox.y()/cm;
  G4double rUpper_z = srUpperCrystalBox.z()/cm;
  
  //! center coordianates of right lowerCrystalBox;
  G4double rLower_x = srLowerCrystalBox.x()/cm;
  G4double rLower_y = srLowerCrystalBox.y()/cm;
  G4double rLower_z = srLowerCrystalBox.z()/cm;
  
  if( z < -300.)
    {
      //! lUpper crystals  
      if( z >= lUpper_z - sCrystalBoxSizeZ/cm/2. && z <= lUpper_z - sCrystalBoxSizeZ/cm/2. +  sCrystalZ/cm && y >= 0. )
	{  
	  //! sCrystals -101,-111,-121
	  if( x >= lUpper_x - sCrystalBoxSizeX/cm/2. && x < lUpper_x - sCrystalBoxSizeX/cm/2. +  1.*sCrystalX/cm)
	    {
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. && y < lUpper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return -101;
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lUpper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return -111;
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lUpper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return -121;
	      else
		{ 
		  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number -1 !!!" << std::endl;
		  return -1;
		}
	    } 
	  //! sCrystals -102,-112,-122
	  if( x >= lUpper_x - sCrystalBoxSizeX/cm/2. +1.*sCrystalX/cm && x < lUpper_x - sCrystalBoxSizeX/cm/2. +  2.*sCrystalX/cm)
	    {
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. && y < lUpper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return -102;
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lUpper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return -112;
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lUpper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return -122;
	      else
                {
		  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number -2 !!!" << std::endl;
		  return -2;
		}
	    }
	  //! sCrystals -103,-113,-123
	  if( x >= lUpper_x - sCrystalBoxSizeX/cm/2. +2.*sCrystalX/cm && x < lUpper_x - sCrystalBoxSizeX/cm/2. +  3.*sCrystalX/cm)
	    {
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. && y < lUpper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return -103;
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lUpper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return -113;
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lUpper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return -123;
	      else
		{
		  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number -3 !!!" << std::endl;
		  return -3;
		}
	    }
	  //! sCrystals -104,-114,-124
	  if( x >= lUpper_x - sCrystalBoxSizeX/cm/2. +3.*sCrystalX/cm && x <= lUpper_x - sCrystalBoxSizeX/cm/2. +  4.*sCrystalX/cm)
	    {
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. && y < lUpper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return -104;
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lUpper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return -114;
	      if( y >= lUpper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lUpper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return -124;
	      else
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number -4 !!!" << std::endl;
		  return -4;
		}
	    }
	}
      //! lLower crystals
      else if( z >= lLower_z - sCrystalBoxSizeZ/cm/2. && z <= lLower_z - sCrystalBoxSizeZ/cm/2. +  sCrystalZ/cm && y < 0.)
	{
	  //! sCrystals -201,-211,-221
	  if( x >= lLower_x - sCrystalBoxSizeX/cm/2. && x < lLower_x - sCrystalBoxSizeX/cm/2. +  1.*sCrystalX/cm)
	    {
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. && y < lLower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return -221;
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lLower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return -211;
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lLower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return -201;
	      else 
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number -10 !!!" << std::endl;
		  return -10;
		}
	    } 
	  //! sCrystals -202,-212,-222
	  if( x >= lLower_x - sCrystalBoxSizeX/cm/2. +1.*sCrystalX/cm && x < lLower_x - sCrystalBoxSizeX/cm/2. +  2.*sCrystalX/cm)
	    {
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. && y < lLower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return -222;
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lLower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm) return -212;
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lLower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return -202;
	      else 
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number -20 !!!" << std::endl;
		  return -20;
		}
	    }
	  //! sCrystals -203,-213,-223
	  if( x >= lLower_x - sCrystalBoxSizeX/cm/2. +2.*sCrystalX/cm && x < lLower_x - sCrystalBoxSizeX/cm/2. +  3.*sCrystalX/cm)
	    {
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. && y < lLower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return -223;
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lLower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return -213;
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lLower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return -203;
	      else
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number -30 !!!" << std::endl;
		  return -30;
		}
	    }	    
	  //! sCrystals -204,-214,-224
	  if( x >= lLower_x - sCrystalBoxSizeX/cm/2. +3.*sCrystalX/cm && x <= lLower_x - sCrystalBoxSizeX/cm/2. +  4.*sCrystalX/cm)
	    {
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. && y < lLower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return -224;
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lLower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return -214;
	      if( y >= lLower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lLower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return -204;
	      else 
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number -40 !!!" << std::endl;
		  return -40;
		}
	    }
	}//! else if (z >= lower_z)

      else
	{
	  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: totally wrong crystal number part 1!!!" << std::endl;
	  return -50;
	}
    } //! if(z < - 300)
  else if( z > 300 )
    {
      //! rUpper crystals  
      if( z >= rUpper_z - sCrystalBoxSizeZ/cm/2. && z <= rUpper_z - sCrystalBoxSizeZ/cm/2. +  sCrystalZ/cm && y >= 0. )
	{  
	  //! sCrystals 101,111,121
	  if( x >= rUpper_x - sCrystalBoxSizeX/cm/2. && x < rUpper_x - sCrystalBoxSizeX/cm/2. +  1.*sCrystalX/cm)
	    {
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. && y < rUpper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 101;
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < rUpper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 111;
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= rUpper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 121;
	      else
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number 1 !!!" << std::endl;
		  return 1;
		}
	    } 
	  //! sCrystals 102,112,122
	  if( x >= rUpper_x - sCrystalBoxSizeX/cm/2. +1.*sCrystalX/cm && x < rUpper_x - sCrystalBoxSizeX/cm/2. +  2.*sCrystalX/cm)
	    {
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. && y < rUpper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 102;
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < rUpper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 112;
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= rUpper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 122;
	      else
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number 2 !!!" << std::endl;
		  return 2;
		}
	    }
	  //! sCrystals 103,113,123
	  if( x >= rUpper_x - sCrystalBoxSizeX/cm/2. +2.*sCrystalX/cm && x < rUpper_x - sCrystalBoxSizeX/cm/2. +  3.*sCrystalX/cm)
	    {
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. && y < rUpper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 103;
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < rUpper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 113;
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= rUpper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 123;
	      else 
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number 3 !!!" << std::endl;
		  return 3;
		}
	    }
	  //! sCrystals 104,114,124
	  if( x >= rUpper_x - sCrystalBoxSizeX/cm/2. +3.*sCrystalX/cm && x <= rUpper_x - sCrystalBoxSizeX/cm/2. +  4.*sCrystalX/cm)
	    {
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. && y < rUpper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 104;
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < rUpper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 114;
	      if( y >= rUpper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= rUpper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 124;
	      else 
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number 4 !!!" << std::endl;
		  return 4;
		}
	    }
	}
      //! rLower crystals
      else if( z >= rLower_z - sCrystalBoxSizeZ/cm/2. && z <= rLower_z - sCrystalBoxSizeZ/cm/2. +  sCrystalZ/cm && y < 0.)
	{
	  //! sCrystals 201,211,221
	  if( x >= rLower_x - sCrystalBoxSizeX/cm/2. && x < rLower_x - sCrystalBoxSizeX/cm/2. +  1.*sCrystalX/cm)
	    {
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. && y < rLower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 221;
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < rLower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 211;
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= rLower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 201;
	      else
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number 10 !!!" << std::endl;
		  return 10;
		}
	    } 
	  //! sCrystals 202,212,222
	  if( x >= rLower_x - sCrystalBoxSizeX/cm/2. +1.*sCrystalX/cm && x < rLower_x - sCrystalBoxSizeX/cm/2. +  2.*sCrystalX/cm)
	    {
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. && y < rLower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 222;
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < rLower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm) return 212;
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= rLower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 202;
	      else
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number 20 !!!" << std::endl;
		  return 20;
		}
	    }
	  //! sCrystals 203,213,223
	  if( x >= rLower_x - sCrystalBoxSizeX/cm/2. +2.*sCrystalX/cm && x < rLower_x - sCrystalBoxSizeX/cm/2. +  3.*sCrystalX/cm)
	    {
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. && y < rLower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 223;
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < rLower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 213;
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= rLower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 203;
	      else
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number 30 !!!" << std::endl;
		  return 30;
		}
	    }
	  //! sCrystals 204,214,224
	  if( x >= rLower_x - sCrystalBoxSizeX/cm/2. +3.*sCrystalX/cm && x <= rLower_x - sCrystalBoxSizeX/cm/2. +  4.*sCrystalX/cm)
	    {
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. && y < rLower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 224;
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < rLower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 214;
	      if( y >= rLower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= rLower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 204;
	      else 
                {
                  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: wrong crystal number 40 !!!" << std::endl;
		  return 40;
		}
	    }
	}//! else if (z >= lower_z)
      else
	{
	  std::cout << "BesZddConstruction.cc :: CheckPosition : ERROR: totally wrong crystal number part 2!!!" << std::endl;
	  return 50;
	}
    }// else if(z > 300)
}// G4int BesZddConstruction::CheckPosition

G4double BesZddConstruction::GetDistanceToPMT( G4double z )
{
  if(z <= -335. && z >= -349.)     return fabs(slUpperCrystalBox.z()/cm - z - sCrystalZ/cm/2.) ;
  else if(z >= 335. && z <= 349.) return fabs(srUpperCrystalBox.z()/cm - z + sCrystalZ/cm/2.) ;
  else return -99.;
}
