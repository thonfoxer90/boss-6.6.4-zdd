//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description:
//Author:  Youzy      Peking University      mail: youzy@hep.pku.cn
//Created:  Nov, 2003
//Modified:
//Comment:
//---------------------------------------------------------------------------//

//
// $Id: BesZddHit 

#include "BesZddHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4ios.hh"
#include "strstream"

G4Allocator<BesZddHit> BesZddHitAllocator;

BesZddHit::BesZddHit()
{ }

BesZddHit::BesZddHit(int trID, int preCrystalNo, int postCrystalNo, double eDep, double preTime,double postTime, G4ThreeVector preXYZ, G4ThreeVector postXYZ, G4double xImpact, G4double yImpact)
{
  m_TrackID  = trID;
  m_preCrystalNo  = preCrystalNo;
  m_postCrystalNo = postCrystalNo;
  m_EDep  = eDep;
  m_preTime  = preTime;
  m_postTime   = postTime;
  m_prePos     = preXYZ;
  m_postPos    = postXYZ;
  m_xImpact = xImpact;
  m_yImpact = yImpact;
  //    m_Volume  = volume;
}

BesZddHit::~BesZddHit() 
{ }

BesZddHit::BesZddHit(const BesZddHit& right)
  : G4VHit()
{
  m_TrackID   = right.m_TrackID;
  m_preCrystalNo = right.m_preCrystalNo;
  m_postCrystalNo = right.m_postCrystalNo;
  m_PDGCode   = right.m_PDGCode;
  m_EDep      = right.m_EDep;
  m_Momentum  = right.m_Momentum;
  m_preTime     = right.m_preTime;
  m_postTime    = right.m_postTime;
  m_prePos      = right.m_prePos;
  m_postPos= right.m_postPos;
  m_xImpact = right.m_xImpact;
  m_yImpact = right.m_yImpact;
  //  m_PosLocal  = right.m_PosLocal;
  m_Volume    = right.m_Volume;
  //  m_VolumeName= right.m_VolumeName;
}

const BesZddHit& BesZddHit::operator=(const BesZddHit& right)
{
  m_TrackID   = right.m_TrackID;
  // m_TrackIndex= right.m_TrackIndex;
  // m_Part      = right.m_Part;
  m_preCrystalNo = right.m_preCrystalNo;
  m_postCrystalNo = right.m_postCrystalNo;
  m_PDGCode   = right.m_PDGCode;
  m_EDep      = right.m_EDep;
  m_Momentum  = right.m_Momentum;
  m_preTime    = right.m_preTime;
  m_postTime   = right.m_postTime;
  m_prePos     = right.m_prePos;
  m_postPos    = right.m_postPos; 
  m_xImpact    = right.m_xImpact;
  m_yImpact    = right.m_yImpact;
  m_Volume    = right.m_Volume;
  return *this;
}

int BesZddHit::operator==(const BesZddHit& right) const
{
  return (this==&right) ? 1 : 0;
}

void BesZddHit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
    {
      G4Circle circle(m_postPos);
      circle.SetScreenSize(2.);
      circle.SetFillStyle(G4Circle::filled);
      G4Colour colour(1.,0.,0.);
      G4VisAttributes attribs(colour);
      circle.SetVisAttributes(attribs);
      pVVisManager->Draw(circle);
    }
}

void BesZddHit::Print()
{
  G4cout << "  trackID: " << m_TrackID  
         << "  preCrystal: " << m_preCrystalNo 
         << "  postCrystal: " << m_postCrystalNo
         << "  energy deposit: " << G4BestUnit(m_EDep,"Energy")
         << "  momentum: " << m_Momentum/MeV
	 << "  prePosition: " << G4BestUnit(m_prePos,"Length") 
	 << "  postPosition: " << G4BestUnit(m_postPos,"Length")<< G4endl
         << "  xImpact: " << m_xImpact << G4endl << " yImpact : " << m_yImpact << G4endl << "============" << G4endl;

}

