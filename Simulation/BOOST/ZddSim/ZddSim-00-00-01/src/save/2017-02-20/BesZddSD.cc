//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description:
//Author:  Youzy      Peking University      mail: youzy@hep.pku.cn
//Created: Nov, 2003
//Modified:
//Comment:
//  2006.10.17 update to new gdml, structure changed
//  new gdml structure: gap<---aluminumbox<---gaschamber  
//  but still use the former simulation structure  
//---------------------------------------------------------------------------//

//
// $Id: BesZddSD.cc,v 1.13 2009/08/25 13:33:54 xieyg Exp $
// GEANT4 tag $Name: ZddSim-00-01-01 $
//

#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"
#include "G4Svc/G4Svc.h"

#include "BesZddSD.hh"
#include "BesZddDigit.hh"
#include "/home/bgarillo/boss-6.6.4-ZDD/Simulation/BOOST/BesSim/BesSim-00-01-20/BesSim/BesSteppingAction.hh" //home/lkoch/wa/Simulation/BOOST/BesSim/BesSim-00-01-20/BesSim/
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4EventManager.hh"
#include "G4ios.hh"
#include "G4UnitsTable.hh"
//#include "BesZddEfficiency.hh"
#include "ReadBoostRoot.hh"
#include "Randomize.hh"
#include "strstream"
#include "G4Box.hh"
#include "stdlib.h"

  BesZddSD::BesZddSD(G4String name,BesZddConstruction* Det)
:BesSensitiveDetector(name),detector(Det)
{
//   std::cout<<"BesZddSD::BesZddSD()\n";
  collectionName.insert("BesZddHitsCollection");
  //  collectionName.insert("BesZddHitsList"); //for MC-Truth
  //  HitID = new G4int[500];

   //retrieve G4Svc
  ISvcLocator* svcLocator = Gaudi::svcLocator();
  IG4Svc* iG4Svc;
  StatusCode sc=svcLocator->service("G4Svc", iG4Svc);
  m_G4Svc = dynamic_cast<G4Svc *>(iG4Svc);
    
}

BesZddSD::~BesZddSD()
{ 
  //  delete [] HitID;
}

void BesZddSD::Initialize(G4HCofThisEvent* HCE)
{
  // HitList 
  ZddHitsCollection = new BesZddHitsCollection(SensitiveDetectorName, collectionName[0]); 
  static G4int HCID = -1;
  TotEnergyDeposit = 0;
  if(HCID<0)
  { 
    HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
   }
  
  HCE->AddHitsCollection( HCID, ZddHitsCollection );
}

/*
void BesZddSD::BeginOfTruthEvent(const G4Event* )
{
  ZddHitList = new BesZddHitsCollection(SensitiveDetectorName, collectionName[1]);
  m_TrackID = -99;
  m_CrystalNo = -99;
  m_EDep = 0.;
  m_Time = -99.;
  m_Volume = 0;

  //G4cout<<"---in BesZddSD::BeginOfTruthEvent()---"<<ZddHitCollection->entries()<<" "<<ZddHitList->entries()<<G4endl;
  
}


void BesZddSD::EndOfTruthEvent(const G4Event* evt)
{
  static G4int HLID=-1;
  if(HLID<0)
    HLID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[1]);
  G4HCofThisEvent* HCE = evt->GetHCofThisEvent();
  HCE->AddHitsCollection(HLID,ZddHitList);
}
*/

G4bool BesZddSD::ProcessHits(G4Step* aStep,G4TouchableHistory*)
{

const G4Event* event=m_G4Svc->GetCurrentEvent();
//printf("****** m_G4Svc->GetCurrentEvent()->GetEventID() = %d ****** \n",m_G4Svc->GetCurrentEvent()->GetEventID());

G4PrimaryVertex *primvertex = event->GetPrimaryVertex(0);
G4PrimaryParticle *primpart = primvertex->GetPrimary(0);
G4TrajectoryContainer *trajcontainer = event->GetTrajectoryContainer();

BesSensitiveManager* sensitiveManager = BesSensitiveManager::GetSensitiveManager();
vector<BesTruthTrack*> *trackList = sensitiveManager->GetTrackList();
	G4int nTrack = trackList->size();
	BesTruthTrack* track;
	
	vector<BesTruthVertex*>* vertexList =  sensitiveManager->GetVertexList();
  G4int nVertex = vertexList->size();
  BesTruthVertex* besvertex;
	
	//arrange TruthTrack in trackList in order of trackIndex
  for(int i=0;i<nTrack-1;i++){
  	for(int j=i+1;j<nTrack;j++){
    	if((*trackList)[i]->GetIndex()>(*trackList)[j]->GetIndex())
    	{
      track=(*trackList)[i];
      (*trackList)[i]=(*trackList)[j];
      (*trackList)[j]=track;
    	}
		}
	}
	
//   std::cout<<"BesZddSD::ProcessHits()\n";
  G4Track *curTrack = aStep->GetTrack();
   //  if (curTrack->GetDefinition()->GetPDGCharge() == 0.) return false;
	G4StepPoint* postStep = aStep->GetPostStepPoint();
	G4StepPoint* uPreStep = aStep->GetPreStepPoint();
	
	G4ThreeVector preStepPosition = uPreStep->GetPosition();
	
	const G4ThreeVector& vertex = curTrack->GetVertexPosition();
			
  G4double edep = aStep->GetTotalEnergyDeposit();
	bool FromIP = vertex.z()==0; 
	/*
	//if( postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() )
	//if( preStepPosition.z()/cm <=335.  )
	//	{
		//printf("BesZddSD::ProcessHits step enters/exit ZDD \n");
		printf("	uPreStep->GetTouchableHandle()->GetVolume()->GetName() = %s \n", uPreStep->GetTouchableHandle()->GetVolume()->GetName().data());
		printf("	postStep->GetTouchableHandle()->GetVolume()->GetName() = %s \n", postStep->GetTouchableHandle()->GetVolume()->GetName().data());
		printf("	curTrack->GetTrackID()= %d curTrack->GetParentID()= %d curTrack->GetParticleDefinition()->GetPDGEncoding() = %d \n",curTrack->GetTrackID(),curTrack->GetParentID(),curTrack->GetDefinition()->GetPDGEncoding());
		
		//G4ThreeVector momentum = curTrack->GetMomentum();
		//printf("momentum.mag() = %f  momentum.theta() = %f momentum.phi() = %f  energy = %f \n",momentum.mag(),momentum.theta(),momentum.phi(),curTrack->GetTotalEnergy());
		
		G4LorentzVector lv = curTrack->GetDynamicParticle()->Get4Momentum(); 
		printf("lv.rho() = %f  lv.theta() = %f lv.phi() = %f  energy = %f \n",lv.mag(),lv.rho(),lv.phi(),lv.e());
		printf("curTrack->GetKineticEnergy() = %f \n",curTrack->GetKineticEnergy());
		printf("	preStepPosition.x() = %f preStepPosition.y() = %f preStepPosition.z() = %f\n", preStepPosition.x(),preStepPosition.y(),preStepPosition.z());
		G4ThreeVector PreStepMom = uPreStep->GetMomentum();
		printf("	PreStepMom.x() = %f pPreStepMom.y() = %f PreStepMom.z() = %f uPreStep->GetTotalEnergy() = %f \n", PreStepMom.x(),PreStepMom.y(),PreStepMom.z(),uPreStep->GetTotalEnergy());
		printf("	vertex.x() = %f vertex.y() = %f vertex.z() = %f \n", vertex.x(),vertex.y(),vertex.z());
		printf("\n");
	//	}
	*/	
	/*	
	printf("BesZddSD::ProcessHits curTrack->GetTrackID()= %d curTrack->GetDefinition()->GetPDGEncoding()=%d aStep->GetStepLength()=%f aStep->GetTotalEnergyDeposit()= %f curTrack->GetParentID()=%d   FromIP = %d \n",curTrack->GetTrackID(),curTrack->GetDefinition()->GetPDGEncoding(),aStep->GetStepLength(),aStep->GetTotalEnergyDeposit(),curTrack->GetParentID(),FromIP);
	printf("vertex.x() = %f vertex.y() = %f vertex.z() = %f \n",vertex.x(),vertex.y(),vertex.z());
	printf("\n");
	//First hit does not deposit energy. As photon immediatly annihilates, the hit collection only contains the decay particles.
	printf("");
*/	
	//if(edep == 0.) return false;
  //if(edep == 0. && curTrack->GetTrackID()!=1) return false;
	
	//if(edep == 0. && vertex.z()>0) return false;
	if(edep == 0. && !FromIP) return false;
		
  //TotEneDeposit+=edep;

  G4TouchableHistory* theTouchable
    = (G4TouchableHistory*)(aStep->GetPreStepPoint()->GetTouchable());

  BesZddHit *newHit = new BesZddHit();
  //  G4cout << "ZddSim :: BesZddSD.cc: Zdd hit !!!" << G4endl;
	newHit->SetIsTrackFromIP(FromIP);
  newHit->SetTrackID  (curTrack->GetTrackID());

  G4int preCrystalNo = aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber();
  G4int postCrystalNo = aStep->GetPostStepPoint()->GetTouchableHandle()->GetCopyNumber();
  newHit->SetPreCrystalNo(preCrystalNo);
  newHit->SetPostCrystalNo(postCrystalNo);      
  //      G4cout << G4endl << "CrystalNo: " << CrystalNo << G4endl;
  
  G4int pdg = curTrack->GetDefinition()->GetPDGEncoding();       
  newHit->SetPDGCode(pdg);
         
  newHit->SetEDep     (edep);
  G4ThreeVector PostPosition = aStep->GetPostStepPoint()->GetPosition();

  newHit->SetPostPos      (PostPosition);
  G4ThreeVector PrePosition = aStep->GetPreStepPoint()->GetPosition();
  newHit->SetPrePos       (PrePosition);

  G4ThreeVector Momentum = aStep->GetPreStepPoint()->GetMomentum();
  newHit->SetMomentum (Momentum);

  G4double xImpact = BesSteppingAction::GetXImpact();
  newHit->SetXImpact(xImpact);
  G4double yImpact = BesSteppingAction::GetYImpact();
  newHit->SetYImpact(yImpact);

  G4double PostTime = aStep->GetPostStepPoint()->GetGlobalTime(); 
  newHit->SetPostTime     (PostTime);

  G4double PreTime = aStep->GetPreStepPoint()->GetGlobalTime(); 
  newHit->SetPreTime     (PreTime);     

  ZddHitsCollection->insert(newHit);
  // Hits  

 /*
 // for MC Truth: 
  if (ZddHitList) {


    // it is kept in trackIndex untill next primary track generated. So IsChildof is uncessary
    //if ( g4TrackId == trackID ) { //|| IsChildOf(curTrack, g4TrackId) ) {     // This track is the primary track & will appear in MC truth
    G4int newTrackFlag = 0;
    newHit->SetTrackIndex(trackIndex);
    if(m_trackIndex != trackIndex) {
      m_trackIndex = trackIndex;
      G4int size = m_trackIndexes.size();
      newTrackFlag = 1;
      if (size > 0) {
        for(G4int i=0;i<size;i++)
          if(m_trackIndexes[i] == trackIndex ) {
            newTrackFlag = 0; 
            break;
          }
      }
    }

    if (newTrackFlag) {
      m_trackIndexes.push_back(trackIndex);
      m_prePart  = -99;
      m_preSeg   = -99;
      m_preGap   = -99;
      m_preStrip = -99;
    }
    BesZddHit* truHit = new BesZddHit();
    *truHit = *newHit;
    if (g4TrackId != trackID) {
      //	trackIndex += 1000; // a sencondary track
      trackIndex += 0; //2006.12.22 do not indicate secondary track now
      truHit->SetTrackIndex(trackIndex);
    }
    //cout << "trackIndex " << trackIndex << endl;

    BesZddDigit aDigit;
    aDigit.SetHit(truHit);
    G4int curPart, curSeg, curGap, curStrip;
    curPart  = aDigit.GetPart();
    curSeg   = aDigit.GetSeg();
    curGap   = aDigit.GetGap();
    curStrip = aDigit.GetNearestStripNo();

    m_effi->CheckCalibSvc();
    m_effi->SetHit(truHit);
    G4double need_eff = m_effi->GetEfficiency();

    //G4cout<<"in SD effi= "<<need_eff<<endl;
    //need_eff = 1.0;  //2006.12.28
    if (curPart == m_prePart && curSeg == m_preSeg &&
        curGap  == m_preGap && curStrip == m_preStrip) {
      //cout<<ZddHitList->entries()<<" "<<ZddHitCollection->entries()<<" "<<need_eff<<"---if----curPart "<<curPart<<"curSeg "<<curSeg<<"curGap "<<curGap<<"curStrip "<<curStrip<<" momentum "<<momentum.x()<<" "<<momentum.y()<<" "<<momentum.z()<<" "<<endl;
      delete truHit;delete newHit;
    }
    else {
      //cout<<ZddHitList->entries()<<"----else--Part "<<curPart<<" Seg "<<curSeg<<" Gap "<<curGap<<" Strip "<<curStrip<<endl;
      truHit->SetPart(curPart);
      truHit->SetSeg(curSeg);
      truHit->SetGap(curGap);
      truHit->SetStrip(curStrip);

      // if a truHit with the same id(part, seg, gap, strip) and trackIndex(%1000) exist,
      // they belong to the same primary track,(maybe one is primary, the other is secondary), dont add.
      bool truHitExist = false;
      G4int n_hit = ZddHitList->entries();
      for(G4int iTru=0;iTru<n_hit;iTru++) {
        BesZddHit* aTruHit = (*ZddHitList)[iTru];
        if ( aTruHit->GetTrackIndex()%1000 == truHit->GetTrackIndex()%1000 &&
             aTruHit->GetPart()  == truHit->GetPart() &&
             aTruHit->GetSeg()   == truHit->GetSeg()  &&
             aTruHit->GetGap()   == truHit->GetGap()  &&
             aTruHit->GetStrip() == truHit->GetStrip() ) 
        {
          truHitExist = true;
          break;
        }
      }
      G4float random=G4UniformRand();   // *** use other random
      // G4float random=(rand()%100000)/100000.0;
      // G4cout<<"---in SD---"<<random<<endl;
      if (random<=need_eff){ ZddHitCollection->insert(newHit);}
      else delete newHit;
      if (!truHitExist&&random<=need_eff)
      { ZddHitList->insert(truHit);}
      else delete truHit;
      m_prePart  = curPart;
      m_preSeg   = curSeg;
      m_preGap   = curGap;
      m_preStrip = curStrip;
    }
    //}
  }
  */

  return true;
  }
  
  void BesZddSD::EndOfEvent(G4HCofThisEvent* HCE)
  {
    static G4int HCID=-1;
    if(HCID<0)
    {HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); }
    HCE->AddHitsCollection(HCID, ZddHitsCollection);
  }

