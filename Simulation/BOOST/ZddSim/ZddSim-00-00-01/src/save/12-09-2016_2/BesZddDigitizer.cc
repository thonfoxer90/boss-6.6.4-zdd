// test

//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description:
//Author: Youzy
//Created: Mar, 2004
//Modified:
//Comment:
//---------------------------------------------------------------------------//
//$Id: BesZddDigitizer.cc

#include "BesZddDigitizer.hh"
#include "BesZddDigit.hh"
#include "BesZddDigi.hh"
#include "BesZddHit.hh"
#include "G4DigiManager.hh"
#include "Randomize.hh"
#include "G4UnitsTable.hh"
#include <CLHEP/Random/Randomize.h>

#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "G4Svc/IG4Svc.h"
#include "G4Svc/G4Svc.h"

using namespace std;

BesZddDigitizer::BesZddDigitizer(G4String modName)
:G4VDigitizerModule(modName)
{
  G4cout << "Digitization initialization" << G4endl;
	
  collectionName.push_back("BesZddDigisCollection");
  m_besZddDigisCollection=0;

	
  //Standard unit: length in mm; time in ns; energy in MeV; att in mm;
  //Here: att in mm; vel in m/s; threshold in MeV; 
  
  
  //retrieve G4Svc
  ISvcLocator* svcLocator = Gaudi::svcLocator();
  IG4Svc* tmpSvc;
  StatusCode sc=svcLocator->service("G4Svc", tmpSvc);
  m_G4Svc=dynamic_cast<G4Svc *>(tmpSvc);
  /*
  //get Zdd Ntuple from G4Svc
  if(m_G4Svc->ZddRootFlag())
  {
    m_tupleZdd = m_G4Svc->GetTupleZdd();
    sc = m_tupleZdd->addItem("partID",m_partID);
    sc = m_tupleZdd->addItem("detectorID",m_detectorID);
    sc = m_tupleZdd->addItem("crystalNo",m_crystalNo);
  }
  */
  /*
  fFile = new TFile("EDepChargeChannel.root","recreate");
  tEDepChargeChannel = new TTree("tEDepChargeChannel","");
  tEDepChargeChannel->Branch("eDep",&m_eDep,"m_eDep/D");
  tEDepChargeChannel->Branch("chargeChannel",&m_chargeChannel,"m_chargeChannel/I");
  */
  //  Initialize();

  //  impactReco = true; //true; //! output impact reconstruction data?

//   chargeChannelFile = new TFile("EDepChargeChannel.root","recreate");
//   tEDepChargeChannel = new TTree("tEDepChargeChannel","");
//   tEDepChargeChannel->Branch("eDep",&m_eDep,"m_eDep/D");
//   tEDepChargeChannel->Branch("chargeChannel",&m_chargeChannel,"m_chargeChannel/I");

//   impactReco = false;

  impactReco = true;
  if(impactReco)
    {
      
      fFile = new TFile("Impact_reco.root","recreate");
      //      tEDepChargeChannel = new TTree("tEDepChargeChannel","");
      //tEDepChargeChannel->Branch("eDep",&m_eDep,"m_eDep/D");
      //tEDepChargeChannel->Branch("chargeChannel",&m_chargeChannel,"m_chargeChannel/I");

      tImpactReco = new TTree("Impact_reco","");
      tImpactReco->Branch("eDep101",&m_eDep101,"m_eDep101/D");
      tImpactReco->Branch("chargeChannel101",&m_chargeChannel101,"m_chargeChannel101/I");
      tImpactReco->Branch("eDep102",&m_eDep102,"m_eDep102/D");
      tImpactReco->Branch("chargeChannel102",&m_chargeChannel102,"m_chargeChannel102/I");
      tImpactReco->Branch("eDep103",&m_eDep103,"m_eDep103/D");
      tImpactReco->Branch("chargeChannel103",&m_chargeChannel103,"m_chargeChannel103/I");
      tImpactReco->Branch("eDep104",&m_eDep104,"m_eDep104/D");
      tImpactReco->Branch("chargeChannel104",&m_chargeChannel104,"m_chargeChannel104/I");
      tImpactReco->Branch("eDep111",&m_eDep111,"m_eDep111/D");
      tImpactReco->Branch("chargeChannel111",&m_chargeChannel111,"m_chargeChannel111/I");
      tImpactReco->Branch("eDep112",&m_eDep112,"m_eDep112/D");
      tImpactReco->Branch("chargeChannel112",&m_chargeChannel112,"m_chargeChannel112/I");
      tImpactReco->Branch("eDep113",&m_eDep113,"m_eDep113/D");
      tImpactReco->Branch("chargeChannel113",&m_chargeChannel113,"m_chargeChannel113/I");
      tImpactReco->Branch("eDep114",&m_eDep114,"m_eDep114/D");
      tImpactReco->Branch("chargeChannel114",&m_chargeChannel114,"m_chargeChannel114/I");
      tImpactReco->Branch("eDep121",&m_eDep121,"m_eDep121/D");
      tImpactReco->Branch("chargeChannel121",&m_chargeChannel121,"m_chargeChannel121/I");
      tImpactReco->Branch("eDep122",&m_eDep122,"m_eDep122/D");
      tImpactReco->Branch("chargeChannel122",&m_chargeChannel122,"m_chargeChannel122/I");
      tImpactReco->Branch("eDep123",&m_eDep123,"m_eDep123/D");
      tImpactReco->Branch("chargeChannel123",&m_chargeChannel123,"m_chargeChannel123/I");
      tImpactReco->Branch("eDep124",&m_eDep124,"m_eDep124/D");
      tImpactReco->Branch("chargeChannel124",&m_chargeChannel124,"m_chargeChannel124/I");
      
      tImpactReco->Branch("eDep201",&m_eDep201,"m_eDep201/D");
      tImpactReco->Branch("chargeChannel201",&m_chargeChannel201,"m_chargeChannel201/I");
      tImpactReco->Branch("eDep202",&m_eDep202,"m_eDep202/D");
      tImpactReco->Branch("chargeChannel202",&m_chargeChannel202,"m_chargeChannel202/I");
      tImpactReco->Branch("eDep203",&m_eDep203,"m_eDep203/D");
      tImpactReco->Branch("chargeChannel203",&m_chargeChannel203,"m_chargeChannel203/I");
      tImpactReco->Branch("eDep204",&m_eDep204,"m_eDep204/D");
      tImpactReco->Branch("chargeChannel204",&m_chargeChannel204,"m_chargeChannel204/I");
      tImpactReco->Branch("eDep211",&m_eDep211,"m_eDep211/D");
      tImpactReco->Branch("chargeChannel211",&m_chargeChannel211,"m_chargeChannel211/I");
      tImpactReco->Branch("eDep212",&m_eDep212,"m_eDep212/D");
      tImpactReco->Branch("chargeChannel212",&m_chargeChannel212,"m_chargeChannel212/I");
      tImpactReco->Branch("eDep213",&m_eDep213,"m_eDep213/D");
      tImpactReco->Branch("chargeChannel213",&m_chargeChannel213,"m_chargeChannel213/I");
      tImpactReco->Branch("eDep214",&m_eDep214,"m_eDep214/D");
      tImpactReco->Branch("chargeChannel214",&m_chargeChannel214,"m_chargeChannel214/I");
      tImpactReco->Branch("eDep221",&m_eDep221,"m_eDep221/D");
      tImpactReco->Branch("chargeChannel221",&m_chargeChannel221,"m_chargeChannel221/I");
      tImpactReco->Branch("eDep222",&m_eDep222,"m_eDep222/D");
      tImpactReco->Branch("chargeChannel222",&m_chargeChannel222,"m_chargeChannel222/I");
      tImpactReco->Branch("eDep223",&m_eDep223,"m_eDep223/D");
      tImpactReco->Branch("chargeChannel223",&m_chargeChannel223,"m_chargeChannel223/I");
      tImpactReco->Branch("eDep224",&m_eDep224,"m_eDep224/D");
      tImpactReco->Branch("chargeChannel224",&m_chargeChannel224,"m_chargeChannel224/I"); 
      
      tImpactReco->Branch("xImpact",&m_xImpact,"m_xImpact/D");
      tImpactReco->Branch("yImpact",&m_yImpact,"m_yImpact/D");
			tImpactReco->Branch("zImpact",&m_zImpact,"m_zImpact/D");
			
			//tImpactReco->Branch("xImpact",&m_xImpact,"m_xImpact/D");
			
			tImpactReco->Branch("PxImpact",&m_PxImpact,"m_PxImpact/D");
			tImpactReco->Branch("PyImpact",&m_PyImpact,"m_PyImpact/D");
			tImpactReco->Branch("PzImpact",&m_PzImpact,"m_PzImpact/D");
						
			tImpactReco->Branch("xImpactElec",&m_xImpactElec,"m_xImpactElec/D");
      tImpactReco->Branch("yImpactElec",&m_yImpactElec,"m_yImpactElec/D");
			tImpactReco->Branch("xImpactPositron",&m_xImpactPositron,"m_xImpactPositron/D");
      tImpactReco->Branch("yImpactPositron",&m_yImpactPositron,"m_yImpactPositron/D");
			
			
			tImpactReco->Branch("m_Ntracks",&m_Ntracks,"m_Ntracks/I");			
			tImpactReco->Branch("m_xImpacts",m_xImpacts,"m_xImpacts[m_Ntracks]/D");
      tImpactReco->Branch("m_yImpacts",m_yImpacts,"m_yImpacts[m_Ntracks]/D");
			
			//tImpactReco->Branch("zImpacts",m_zImpacts,"m_zImpacts[m_Ntracks]/D");
			
			//tImpactReco->Branch("pids",m_pids,"m_pids[m_Ntracks]/I");
			//tImpactReco->Branch("pids",m_pids,"m_pids[m_Ntracks]/D");
			
    }

	m_event=0;
}

BesZddDigitizer::~BesZddDigitizer()
{
  std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 0" << std::endl;
  
//   chargeChannelFile->cd();
//   tEDepChargeChannel->Write();
//   chargeChannelFile->Close();
//   tEDepChargeChannel=NULL;
	
  if(impactReco)
    {
      fFile->cd();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 1" << std::endl;
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 2" << std::endl;
      tImpactReco->Write();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 3" << std::endl;
      fFile->Write();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 4" << std::endl;
      fFile->Close();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 5" << std::endl;

      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 6" << std::endl;
      tImpactReco = NULL;
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 7" << std::endl;
      fFile = NULL;
    }
  std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer end " << std::endl;
	
/*	
if(impactReco)
    {
      fFile->cd();
      fFile->Write();
      fFile->Close();
    }
*/		
}

void BesZddDigitizer::Initialize()
{
/*
  m_partID = 0;
  m_detectorID = 0;
  m_crystalNo = 0;
*/
  m_eDep = 0.;
  m_chargeChannel = 0.;
  
  m_eDep101 = 0.; m_eDep102 = 0.; m_eDep103 = 0.; m_eDep104 = 0.;
  m_eDep111 = 0.; m_eDep112 = 0.; m_eDep113 = 0.; m_eDep114 = 0.;
  m_eDep121 = 0.; m_eDep122 = 0.; m_eDep123 = 0.; m_eDep124 = 0.;
  m_eDep201 = 0.; m_eDep202 = 0.; m_eDep203 = 0.; m_eDep204 = 0.;
  m_eDep211 = 0.; m_eDep212 = 0.; m_eDep213 = 0.; m_eDep214 = 0.;
  m_eDep221 = 0.; m_eDep222 = 0.; m_eDep223 = 0.; m_eDep224 = 0.;
  m_chargeChannel101 = 0; m_chargeChannel102 = 0; m_chargeChannel103 = 0; m_chargeChannel104 = 0;
  m_chargeChannel111 = 0; m_chargeChannel112 = 0; m_chargeChannel113 = 0; m_chargeChannel114 = 0;
  m_chargeChannel121 = 0; m_chargeChannel122 = 0; m_chargeChannel123 = 0; m_chargeChannel124 = 0;
  m_chargeChannel201 = 0; m_chargeChannel202 = 0; m_chargeChannel203 = 0; m_chargeChannel204 = 0;
  m_chargeChannel211 = 0; m_chargeChannel212 = 0; m_chargeChannel213 = 0; m_chargeChannel214 = 0;
  m_chargeChannel221 = 0; m_chargeChannel222 = 0; m_chargeChannel223 = 0; m_chargeChannel224 = 0;

	m_zImpact=0;
	 
	m_xImpactElec=-99;  m_yImpactElec=-99;
	m_xImpactPositron=-99;  m_yImpactPositron=-99;
 smearFlag = false; // energy/time smearing on ?

 if(smearFlag) G4cout << "ZDDSIM:: BesZddDigitizer: digi smearing ON! " << G4endl;
 // else G4cout << "ZDDSIM:: BesZddDigitizer: digi smearing OFF! " << G4endl;
  
}

void BesZddDigitizer::Digitize()
{
	printf("Event #%d \n", m_event);
	m_event++;
//   std::cout<<"BesZddDigitizer::Digitize()\n";
  G4bool debug = false; // couts in Digitizer?
  
  Initialize();

	//MC-Truth informations
	BesSensitiveManager* sensitiveManager = BesSensitiveManager::GetSensitiveManager();
	vector<BesTruthTrack*> *trackList = sensitiveManager->GetTrackList();
	G4int nTrack = trackList->size();
	BesTruthTrack* track;
	
	vector<BesTruthVertex*>* vertexList =  sensitiveManager->GetVertexList();
  G4int nVertex = vertexList->size();
  BesTruthVertex* vertex;
	
	//arrange TruthTrack in trackList in order of trackIndex
  for(int i=0;i<nTrack-1;i++){
  	for(int j=i+1;j<nTrack;j++){
    	if((*trackList)[i]->GetIndex()>(*trackList)[j]->GetIndex())
    	{
      track=(*trackList)[i];
      (*trackList)[i]=(*trackList)[j];
      (*trackList)[j]=track;
    	}
		}
	}	
	
	/*
	Event::McParticleCol *particleCol = new Event::McParticleCol;
  
  //loop over tracks
  for(int i=0;i<nTrack;i++)
  {
    track = (*trackList) [i];
    
    // find out the start point
    bool isPrimary = false;
    bool startPointFound = false;
    BesTruthVertex* startPoint;
    
    for (int i=0;i<nVertex;i++) 
    {
      vertex = (*vertexList) [i];
      if ( track->GetVertex()->GetIndex() == vertex->GetIndex() ) 
      {
        if (!vertex->GetParentTrack()) isPrimary = true;
        startPointFound = true;
        startPoint = vertex;
        break;
      }
    }
	*/
			
  //chargeChannel threshold per crystal for digitization
  G4double threshold = 0.0;
  
  m_besZddDigisCollection = new BesZddDigisCollection(moduleName, collectionName[0]);
  G4DigiManager* DigiMan = G4DigiManager::GetDMpointer();
  
  //hits collection ID
  G4int THCID;
  THCID = DigiMan->GetHitsCollectionID("BesZddHitsCollection");
 
  //  G4cout << "ZddSim :: BesZddDigitizer : THCID: " << THCID << " " << &THCID <<  G4endl;
  
  //hits collection
  BesZddHitsCollection* THC = 0;
  THC = (BesZddHitsCollection*) (DigiMan->GetHitsCollection(THCID));

  //  G4cout << " ZddSim :: BesZddDigitizer : THC: " << THC << " " << &THC << G4endl;
	int index=0;
  if(THC) {

    /*     
      //fill zdd Ntuple
      if(m_G4Svc->ZddRootFlag())
	{
	  m_PartID = partID;
	  m_DetectorID = detectorID;
	  m_CrystalNo = crystalNo;
	  m_ChargeChannel = chargeChannel;
	  m_TimeChannel = timeChannel;
	  m_tupleZdd->write();
	}
    */
 
    G4cout << " if(THC) 1 " << G4endl;

    BesZddDigit *aDigit = new BesZddDigit();
    //    G4cout << " if(THC) 2 " << G4endl;
    G4int n_hit = THC->entries();
    //G4cout << " if(THC) 3 " << G4endl;
    
		//m_Ntracks=(int)trackList->size();
		m_Ntracks=0;
		int hitPDGcode=0;
		int hitTrackID=0;
    for(G4int i = 0; i < n_hit; i++) 
      {
	BesZddHit* hit = (*THC)[i];	
	aDigit->SetHit( hit );
	printf("i : %d hit->GetTrackID() : %d hit->GetPDGCode() : %d\n", i,hit->GetTrackID(),hit->GetPDGCode());
			if(i==0) m_trackID=hit->GetTrackID();
			//cout << hit->GetXImpact() << endl;
			/*
			track=(*trackList)[hit->GetTrackID()];
			vertex=track->GetVertex();
			//printf("PRIMARY \n");
			//if (!vertex->GetParentTrack()) printf("PRIMARY \n");
			//Check if primary;
			//int Isprimary=0;
			//printf("index Hit collection : %d hit->GetTrackID() : %d hit->GetPDGCode() : %d track->GetPDGCode () : %d \n", i,hit->GetTrackID(),hit->GetPDGCode(),track->GetPDGCode ());
			*/
			
			
			bool bFirstHitTrack=false;
			if(i==0)
				{
				bFirstHitTrack=true;				
				}
			else if(i>0 && hitTrackID!=hit->GetTrackID())
				{
				bFirstHitTrack=true;
				}
			else{bFirstHitTrack=false;}
			hitTrackID=hit->GetTrackID();
			hitPDGcode=hit->GetPDGCode();					
			//printf("%d %d %d \n",i,hit->GetTrackID(),hit->GetPDGCode());			
			//cout << "trackList->size()"<< trackList->size() << endl;
			for(int k=0;k<trackList->size();k++)
				{
				track=(*trackList)[k];
				//printf("track->GetIndex() : %d track->GetG4TrackId() : %d hit->GetTrackID(): %d hit->GetPDGCode(): %d \n",track->GetIndex(),track->GetG4TrackId(),hit->GetTrackID(),hit->GetPDGCode());
				//if(track->GetIndex()==hit->GetTrackID())
				
				//m_xImpacts[k]=-99;
				//if(bFirstHitTrack && track->GetG4TrackId()==hit->GetTrackID())
				if(bFirstHitTrack)
					{
				//	printf("index Hit collection : %d hit->GetTrackID() : %d hit->GetPDGCode() : %d track->GetPDGCode () : %d \n", i,hit->GetTrackID(),hit->GetPDGCode(),track->GetPDGCode ());
					
					//printf("%f %f %d\n",hit->GetXImpact(),hit->GetYImpact(),hit->GetPDGCode());
					double x = (double)hit->GetXImpact();
					double y = (double)hit->GetYImpact();
					int		pid = (int)hit->GetPDGCode();
					//m_xImpacts[k]=hit->GetXImpact();
					
					//m_xImpacts[index]=hit->GetXImpact();
					//m_yImpacts[index]=hit->GetYImpact();
					//m_pids[index]=hit->GetPDGCode();
					
					//m_xImpacts[m_Ntracks]=x;
					//m_yImpacts[m_Ntracks]=y;
					//m_pids[m_Ntracks]=pid;
					//m_Ntracks++;
					//cout << index << endl;
					
					vertex=track->GetVertex();
					if (!vertex->GetParentTrack()) 
						{
						printf("index Hit collection : %d hit->GetTrackID() : %d hit->GetPDGCode() : %d track->GetPDGCode () : %d (hit->GetPrePos()).z() : %f (hit->GetPostPos()).z() : %f\n", i,hit->GetTrackID(),hit->GetPDGCode(),track->GetPDGCode (), (hit->GetPrePos()).z(),(hit->GetPostPos()).z());
						//m_xImpacts[m_Ntracks]=x;
						//m_yImpacts[m_Ntracks]=y;
						//m_pids[m_Ntracks]=pid;
						//m_Ntracks++;
						if(pid==11)
							{
							m_xImpactElec=x;
							m_yImpactElec=y;
							}
						else if(pid==-11)
							{
							m_xImpactPositron=x;
							m_yImpactPositron=y;
							}	
						}
					
					index++;
					}
				}
				
				
			
				
      }
		/*	
		m_Ntracks=(int)trackList->size();
		for(int k=0;k<trackList->size();k++)
				{
				track=(*trackList)[k];
				BesZddHit* hit = (*THC)[0];
					if(track->GetG4TrackId()==hit->GetTrackID())
						{
						//m_xImpacts[k]=hit->GetXImpact();
						//printf("%d %f \n",k,hit->GetXImpact());
						}	
				}
		*/
		
		/*
		m_Ntracks=0;		
		for(int k=0;k<trackList->size();k++)
				{
				track=(*trackList)[k];
				BesZddHit* hit = (*THC)[0];
					if(track->GetG4TrackId()==hit->GetTrackID())
						{
						m_xImpacts[m_Ntracks]=hit->GetXImpact();
						//printf("%d %f \n",k,hit->GetXImpact());
						m_Ntracks++;
						}	
				}
		*/
    aDigit->EndOfEvent();
    //    G4cout << " if(THC) 4 " << G4endl;

    if( aDigit->GetChargeChannel_101() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 1 );
      digi->SetDetectorID( 1 );
      digi->SetCrystalNo( 101 );
      G4int chargeChannel = aDigit->GetChargeChannel_101() ;
      G4int timeChannel = aDigit->GetTimeChannel_101() ;
      
      m_chargeChannel = chargeChannel;
      m_eDep = aDigit->totEDep_101;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " -101 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
      digi->SetChargeChannel( chargeChannel );
      digi->SetTimeChannel( timeChannel ) ;
      //Smear();  
      m_besZddDigisCollection->insert(digi);

     }

    if( aDigit->GetChargeChannel_102() >= threshold) // if chargeChannel above channel threshold  
    {
            G4cout << "EDep crystal -102: " << aDigit->GetChargeChannel_102() << G4endl;
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 102 );
	G4int chargeChannel = aDigit->GetChargeChannel_102() ;
	G4int timeChannel = aDigit->GetTimeChannel_102() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_102;
// //tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -102 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( aDigit->GetChargeChannel_103() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 103 );
	G4int chargeChannel = aDigit->GetChargeChannel_103() ;
	G4int timeChannel = aDigit->GetTimeChannel_103() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_103;
// //tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -103 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_104() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 104 );
	G4int chargeChannel = aDigit->GetChargeChannel_104() ;
	G4int timeChannel = aDigit->GetTimeChannel_104() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_104;
// //tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -104 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_111() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID ( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 111 );
	G4int chargeChannel = aDigit->GetChargeChannel_111() ;
	G4int timeChannel = aDigit->GetTimeChannel_111() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_111;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -111 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel_112() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 112 );
        G4int chargeChannel = aDigit->GetChargeChannel_112() ;
        G4int timeChannel = aDigit->GetTimeChannel_112() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_112;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -112 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_113() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 113 );
        G4int chargeChannel = aDigit->GetChargeChannel_113() ;
        G4int timeChannel = aDigit->GetTimeChannel_113() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_113;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -113 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_114() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 114 );
        G4int chargeChannel = aDigit->GetChargeChannel_114() ;
        G4int timeChannel = aDigit->GetTimeChannel_114() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_114;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -114 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_121() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 121 );
	G4int chargeChannel = aDigit->GetChargeChannel_121() ;
	G4int timeChannel = aDigit->GetTimeChannel_121() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_121;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -121 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel_122() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 122 );
        G4int chargeChannel = aDigit->GetChargeChannel_122() ;
        G4int timeChannel = aDigit->GetTimeChannel_122() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_122;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -122 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_123() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 123 );
        G4int chargeChannel = aDigit->GetChargeChannel_123() ;
        G4int timeChannel = aDigit->GetTimeChannel_123() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_123;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -123 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_124() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 124 );
        G4int chargeChannel = aDigit->GetChargeChannel_124() ;
        G4int timeChannel = aDigit->GetTimeChannel_124() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_124;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -124 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    // detectorID = 2 (lower crystals)
    if( aDigit->GetChargeChannel_201() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 1 );
      digi->SetDetectorID( 2 );
      digi->SetCrystalNo( 201 );
      G4int chargeChannel = aDigit->GetChargeChannel_201() ;
      G4int timeChannel = aDigit->GetTimeChannel_201() ;

      m_chargeChannel = chargeChannel;
      m_eDep = aDigit->totEDep_201;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " -201 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
      //Smear();  
      m_besZddDigisCollection->insert(digi);
    }

    if( aDigit->GetChargeChannel_202() >= threshold) // if chargeChannel above channel threshold   
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 202 );
	G4int chargeChannel = aDigit->GetChargeChannel_202() ;
	G4int timeChannel = aDigit->GetTimeChannel_202() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_202;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -202 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( aDigit->GetChargeChannel_203() >= threshold) // if chargeChannel above channel threshold 
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 203 );
	G4int chargeChannel = aDigit->GetChargeChannel_203() ;
	G4int timeChannel = aDigit->GetTimeChannel_203() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_203;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -203 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_204() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 204 );
	G4int chargeChannel = aDigit->GetChargeChannel_204() ;
	G4int timeChannel = aDigit->GetTimeChannel_204() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_204;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -204 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_211() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 211 );
	G4int chargeChannel = aDigit->GetChargeChannel_211() ;
	G4int timeChannel = aDigit->GetTimeChannel_211() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_211;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -211 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel_212() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 212 );
        G4int chargeChannel = aDigit->GetChargeChannel_212() ;
        G4int timeChannel = aDigit->GetTimeChannel_212() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_212;
//tEDepChargeChannel->Fill();

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(debug){ std::cout << " -212 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_213() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 213 );
        G4int chargeChannel = aDigit->GetChargeChannel_213() ;
        G4int timeChannel = aDigit->GetTimeChannel_213() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_213;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -213 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_214() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 214 );
        G4int chargeChannel = aDigit->GetChargeChannel_214() ;
        G4int timeChannel = aDigit->GetTimeChannel_214() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_214;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -214 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_221() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 221 );
	G4int chargeChannel = aDigit->GetChargeChannel_221() ;
	G4int timeChannel = aDigit->GetTimeChannel_221() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_221;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -221 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_222() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 222 );
	G4int chargeChannel = aDigit->GetChargeChannel_222() ;
	G4int timeChannel = aDigit->GetTimeChannel_222() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_222;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -222 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_223() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 223 );
	G4int chargeChannel = aDigit->GetChargeChannel_223() ;
	G4int timeChannel = aDigit->GetTimeChannel_223() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_223;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -223 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	m_besZddDigisCollection->insert(digi);
      }      
      if( aDigit->GetChargeChannel_224() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 224 );
	G4int chargeChannel = aDigit->GetChargeChannel_224() ;
	G4int timeChannel = aDigit->GetTimeChannel_224() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep_224;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -224 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	m_besZddDigisCollection->insert(digi);
      }
      //! part = 2 => right part
    if( aDigit->GetChargeChannel101() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 2 );
      digi->SetDetectorID( 1 );
      digi->SetCrystalNo( 101 );
      G4int chargeChannel = aDigit->GetChargeChannel101() ;
      G4int timeChannel = aDigit->GetTimeChannel101() ;

      m_chargeChannel = chargeChannel;
      m_eDep = aDigit->totEDep101;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " 101 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
      digi->SetChargeChannel( chargeChannel );
      digi->SetTimeChannel( timeChannel ) ;

      if(impactReco)
	{
	  m_eDep101 = m_eDep;
	  m_chargeChannel101 = m_chargeChannel;
	}


      //Smear();  
      m_besZddDigisCollection->insert(digi);

     }

    if( aDigit->GetChargeChannel102() >= threshold) // if chargeChannel above channel threshold  
    {
      //      G4cout << "EDep crystal -102: " << aDigit->GetChargeChannel102() << G4endl;
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 102 );
	G4int chargeChannel = aDigit->GetChargeChannel102() ;
	G4int timeChannel = aDigit->GetTimeChannel102() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep102;

//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 102 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep102 = m_eDep;
	    m_chargeChannel102 = m_chargeChannel;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( aDigit->GetChargeChannel103() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 103 );
	G4int chargeChannel = aDigit->GetChargeChannel103() ;
	G4int timeChannel = aDigit->GetTimeChannel103() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep103;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 103 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep103 = m_eDep;
	    m_chargeChannel103 = m_chargeChannel;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel104() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 104 );
	G4int chargeChannel = aDigit->GetChargeChannel104() ;
	G4int timeChannel = aDigit->GetTimeChannel104() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep104;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 104 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep104 = m_eDep;
	    m_chargeChannel104 = m_chargeChannel;
	  }

	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel111() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID ( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 111 );
	G4int chargeChannel = aDigit->GetChargeChannel111() ;
	G4int timeChannel = aDigit->GetTimeChannel111() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep111;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 111 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep111 = m_eDep;
	    m_chargeChannel111 = m_chargeChannel;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel112() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 112 );
        G4int chargeChannel = aDigit->GetChargeChannel112() ;
        G4int timeChannel = aDigit->GetTimeChannel112() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep112;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 112 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep112 = m_eDep;
	    m_chargeChannel112 = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel113() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 113 );
        G4int chargeChannel = aDigit->GetChargeChannel113() ;
        G4int timeChannel = aDigit->GetTimeChannel113() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep113;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 113 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep113 = m_eDep;
	    m_chargeChannel113 = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel114() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 114 );
        G4int chargeChannel = aDigit->GetChargeChannel114() ;
        G4int timeChannel = aDigit->GetTimeChannel114() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep114;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 114 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep114 = m_eDep;
	    m_chargeChannel114 = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel121() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 121 );
	G4int chargeChannel = aDigit->GetChargeChannel121() ;
	G4int timeChannel = aDigit->GetTimeChannel121() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep121;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 121 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep121 = m_eDep;
	    m_chargeChannel121 = m_chargeChannel;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel122() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 122 );
        G4int chargeChannel = aDigit->GetChargeChannel122() ;
        G4int timeChannel = aDigit->GetTimeChannel122() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep122;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 122 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep122 = m_eDep;
	    m_chargeChannel122 = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel123() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 123 );
        G4int chargeChannel = aDigit->GetChargeChannel123() ;
        G4int timeChannel = aDigit->GetTimeChannel123() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep123;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 123 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep123 = m_eDep;
	    m_chargeChannel123 = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel124() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 124 );
        G4int chargeChannel = aDigit->GetChargeChannel124() ;
        G4int timeChannel = aDigit->GetTimeChannel124() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep124;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 124 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep124 = m_eDep;
	    m_chargeChannel124 = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
        // detectorID = 2 (lower crystals)
    if( aDigit->GetChargeChannel201() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 2 );
      digi->SetDetectorID( 2 );
      digi->SetCrystalNo( 201 );
      G4int chargeChannel = aDigit->GetChargeChannel201() ;
      G4int timeChannel = aDigit->GetTimeChannel201() ;

      m_chargeChannel = chargeChannel;
      m_eDep = aDigit->totEDep201;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " 201 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep201 = m_eDep;
	    m_chargeChannel201 = m_chargeChannel;
	  }

      //Smear();  
      m_besZddDigisCollection->insert(digi);
    }

    if( aDigit->GetChargeChannel202() >= threshold) // if chargeChannel above channel threshold   
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 202 );
	G4int chargeChannel = aDigit->GetChargeChannel202() ;
	G4int timeChannel = aDigit->GetTimeChannel202() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep202;
//tEDepChargeChannel->Fill();

	if(debug) std::cout << " BesZddDigitizer.cc : totEdep202 : " << m_eDep << std::endl;

	if(debug){ std::cout << " 202 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep202 = m_eDep;
            m_chargeChannel202 = m_chargeChannel;
          }

	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( aDigit->GetChargeChannel203() >= threshold) // if chargeChannel above channel threshold 
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 203 );
	G4int chargeChannel = aDigit->GetChargeChannel203() ;
	G4int timeChannel = aDigit->GetTimeChannel203() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep203;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 203 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep203 = m_eDep;
            m_chargeChannel203 = m_chargeChannel;
          }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel204() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 204 );
	G4int chargeChannel = aDigit->GetChargeChannel204() ;
	G4int timeChannel = aDigit->GetTimeChannel204() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep204;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 204 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep204 = m_eDep;
            m_chargeChannel204 = m_chargeChannel;
          }

	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel211() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 211 );
	G4int chargeChannel = aDigit->GetChargeChannel211() ;
	G4int timeChannel = aDigit->GetTimeChannel211() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep211;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 211 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep211 = m_eDep;
            m_chargeChannel211 = m_chargeChannel;
          }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel212() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 212 );
        G4int chargeChannel = aDigit->GetChargeChannel212() ;
        G4int timeChannel = aDigit->GetTimeChannel212() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep212;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 212 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep212 = m_eDep;
            m_chargeChannel212 = m_chargeChannel;
          }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel213() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 213 );
        G4int chargeChannel = aDigit->GetChargeChannel213() ;
        G4int timeChannel = aDigit->GetTimeChannel213() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep213;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 213 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep213 = m_eDep;
            m_chargeChannel213 = m_chargeChannel;
          }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel214() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 214 );
        G4int chargeChannel = aDigit->GetChargeChannel214() ;
        G4int timeChannel = aDigit->GetTimeChannel214() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep214;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 214 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep214 = m_eDep;
            m_chargeChannel214 = m_chargeChannel;
          }

	//	Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel221() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 221 );
	G4int chargeChannel = aDigit->GetChargeChannel221() ;
	G4int timeChannel = aDigit->GetTimeChannel221() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep221;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 221 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep221 = m_eDep;
            m_chargeChannel221 = m_chargeChannel;
          }

	m_besZddDigisCollection->insert(digi);
      }
      if( aDigit->GetChargeChannel222() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 222 );
	G4int chargeChannel = aDigit->GetChargeChannel222() ;
	G4int timeChannel = aDigit->GetTimeChannel222() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep222;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 222 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep222 = m_eDep;
            m_chargeChannel222 = m_chargeChannel;
          }

	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel223() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 223 );
	G4int chargeChannel = aDigit->GetChargeChannel223() ;
	G4int timeChannel = aDigit->GetTimeChannel223() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep223;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 223 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep223 = m_eDep;
            m_chargeChannel223 = m_chargeChannel;
          }

	m_besZddDigisCollection->insert(digi);
      }      
      if( aDigit->GetChargeChannel224() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 224 );
	G4int chargeChannel = aDigit->GetChargeChannel224() ;
	G4int timeChannel = aDigit->GetTimeChannel224() ;

	m_chargeChannel = chargeChannel;
	m_eDep = aDigit->totEDep224;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 224 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep224 = m_eDep;
            m_chargeChannel224 = m_chargeChannel;
          }

	m_besZddDigisCollection->insert(digi);
      }
        
    StoreDigiCollection(m_besZddDigisCollection);	

    if(impactReco)
      {
	m_xImpact = aDigit->GetXImpact();
	m_yImpact = aDigit->GetYImpact();
	m_zImpact = aDigit->GetZImpact();

	m_PxImpact = aDigit->GetPxImpact();
	m_PyImpact = aDigit->GetPyImpact();
	m_PzImpact = aDigit->GetPzImpact();
	
	tImpactReco->Fill();

	//	fFile->cd();
	//	tEDepChargeChannel->Write();
	//	tImpactReco->Write();
      }      
    delete aDigit;
  } 
  //  G4cout << "ZddSim :: BesZddDigitizer : End of Digitization" << G4endl;
  //  int test;
  //  G4cin >> test;
}

G4double BesZddDigitizer::Smear(G4double var)
{ 
//   G4cout << "eDep before smearing: " << G4BestUnit(edep,"Energy") << G4endl;
//   G4cout << "time before smearing: " << G4BestUnit(time,"Time") << G4endl;
  
  G4double gaussrand = G4RandGauss::shoot(0.,0.5);
  return var+(gaussrand*var);
  
//   G4cout << "eDep after smearing:  " << G4BestUnit(edep,"Energy") << G4endl;
//   G4cout << "time after smearing:  " << G4BestUnit(time,"Time") << G4endl;
//   
//   G4cout << "gaussrand1:           " << gaussrand1 << G4endl;
//   G4cout << "gaussrand2:           " << gaussrand2 << G4endl << G4endl;
}

