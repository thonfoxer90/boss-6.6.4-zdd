// test

//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description:
//Author: Youzy
//Created: Mar, 2004
//Modified:
//Comment:
//---------------------------------------------------------------------------//
//$Id: BesZddDigitizer.cc

#include "BesZddDigitizer.hh"
#include "BesZddDigit.hh"
#include "BesZddDigi.hh"
#include "BesZddHit.hh"
#include "G4DigiManager.hh"
#include "Randomize.hh"
#include "G4UnitsTable.hh"
#include <CLHEP/Random/Randomize.h>

#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "G4Svc/IG4Svc.h"
#include "G4Svc/G4Svc.h"

using namespace std;

BesZddDigitizer::BesZddDigitizer(G4String modName)
:G4VDigitizerModule(modName)
{
  G4cout << "Digitization initialization" << G4endl;
	
  collectionName.push_back("BesZddDigisCollection");
  m_besZddDigisCollection=0;

	
  //Standard unit: length in mm; time in ns; energy in MeV; att in mm;
  //Here: att in mm; vel in m/s; threshold in MeV; 
  
  
  //retrieve G4Svc
  ISvcLocator* svcLocator = Gaudi::svcLocator();
  IG4Svc* tmpSvc;
  StatusCode sc=svcLocator->service("G4Svc", tmpSvc);
  m_G4Svc=dynamic_cast<G4Svc *>(tmpSvc);
  /*
  //get Zdd Ntuple from G4Svc
  if(m_G4Svc->ZddRootFlag())
  {
    m_tupleZdd = m_G4Svc->GetTupleZdd();
    sc = m_tupleZdd->addItem("partID",m_partID);
    sc = m_tupleZdd->addItem("detectorID",m_detectorID);
    sc = m_tupleZdd->addItem("crystalNo",m_crystalNo);
  }
  */
  /*
  fFile = new TFile("EDepChargeChannel.root","recreate");
  tEDepChargeChannel = new TTree("tEDepChargeChannel","");
  tEDepChargeChannel->Branch("eDep",&m_eDep,"m_eDep/D");
  tEDepChargeChannel->Branch("chargeChannel",&m_chargeChannel,"m_chargeChannel/I");
  */
  //  Initialize();

  //  impactReco = true; //true; //! output impact reconstruction data?

//   chargeChannelFile = new TFile("EDepChargeChannel.root","recreate");
//   tEDepChargeChannel = new TTree("tEDepChargeChannel","");
//   tEDepChargeChannel->Branch("eDep",&m_eDep,"m_eDep/D");
//   tEDepChargeChannel->Branch("chargeChannel",&m_chargeChannel,"m_chargeChannel/I");

//   impactReco = false;

  impactReco = true;
  if(impactReco)
    {
      
      fFile = new TFile("Impact_reco.root","recreate");
      //      tEDepChargeChannel = new TTree("tEDepChargeChannel","");
      //tEDepChargeChannel->Branch("eDep",&m_eDep,"m_eDep/D");
      //tEDepChargeChannel->Branch("chargeChannel",&m_chargeChannel,"m_chargeChannel/I");

      tImpactReco = new TTree("Impact_reco","");
      tImpactReco->Branch("eDep101Right",&m_eDep101Right,"m_eDep101Right/D");
      tImpactReco->Branch("chargeChannel101Right",&m_chargeChannel101Right,"m_chargeChannel101Right/I");
      tImpactReco->Branch("eDep102RightRight",&m_eDep102Right,"m_eDep102Right/D");
      tImpactReco->Branch("chargeChannel102Right",&m_chargeChannel102Right,"m_chargeChannel102Right/I");
      tImpactReco->Branch("eDep103RightRight",&m_eDep103Right,"m_eDep103Right/D");
      tImpactReco->Branch("chargeChannel103Right",&m_chargeChannel103Right,"m_chargeChannel103Right/I");
      tImpactReco->Branch("eDep104Right",&m_eDep104Right,"m_eDep104Right/D");
      tImpactReco->Branch("chargeChannel104Right",&m_chargeChannel104Right,"m_chargeChannel104Right/I");
      tImpactReco->Branch("eDep111Right",&m_eDep111Right,"m_eDep111Right/D");
      tImpactReco->Branch("chargeChannel111Right",&m_chargeChannel111Right,"m_chargeChannel111Right/I");
      tImpactReco->Branch("eDep112Right",&m_eDep112Right,"m_eDep112Right/D");
      tImpactReco->Branch("chargeChannel112Right",&m_chargeChannel112Right,"m_chargeChannel112Right/I");
      tImpactReco->Branch("eDep113Right",&m_eDep113Right,"m_eDep113Right/D");
      tImpactReco->Branch("chargeChannel113Right",&m_chargeChannel113Right,"m_chargeChannel113Right/I");
      tImpactReco->Branch("eDep114Right",&m_eDep114Right,"m_eDep114Right/D");
      tImpactReco->Branch("chargeChannel114Right",&m_chargeChannel114Right,"m_chargeChannel114Right/I");
      tImpactReco->Branch("eDep121Right",&m_eDep121Right,"m_eDep121Right/D");
      tImpactReco->Branch("chargeChannel121Right",&m_chargeChannel121Right,"m_chargeChannel121Right/I");
      tImpactReco->Branch("eDep122Right",&m_eDep122Right,"m_eDep122Right/D");
      tImpactReco->Branch("chargeChannel122Right",&m_chargeChannel122Right,"m_chargeChannel122Right/I");
      tImpactReco->Branch("eDep123Right",&m_eDep123Right,"m_eDep123Right/D");
      tImpactReco->Branch("chargeChannel123Right",&m_chargeChannel123Right,"m_chargeChannel123Right/I");
      tImpactReco->Branch("eDep124Right",&m_eDep124Right,"m_eDep124Right/D");
      tImpactReco->Branch("chargeChannel124Right",&m_chargeChannel124Right,"m_chargeChannel124Right/I");
      
      tImpactReco->Branch("eDep201Right",&m_eDep201Right,"m_eDep201Right/D");
      tImpactReco->Branch("chargeChannel201Right",&m_chargeChannel201Right,"m_chargeChannel201Right/I");
      tImpactReco->Branch("eDep202Right",&m_eDep202Right,"m_eDep202Right/D");
      tImpactReco->Branch("chargeChannel202Right",&m_chargeChannel202Right,"m_chargeChannel202Right/I");
      tImpactReco->Branch("eDep203Right",&m_eDep203Right,"m_eDep203Right/D");
      tImpactReco->Branch("chargeChannel203Right",&m_chargeChannel203Right,"m_chargeChannel203Right/I");
      tImpactReco->Branch("eDep204Right",&m_eDep204Right,"m_eDep204Right/D");
      tImpactReco->Branch("chargeChannel204Right",&m_chargeChannel204Right,"m_chargeChannel204Right/I");
      tImpactReco->Branch("eDep211Right",&m_eDep211Right,"m_eDep211Right/D");
      tImpactReco->Branch("chargeChannel211Right",&m_chargeChannel211Right,"m_chargeChannel211Right/I");
      tImpactReco->Branch("eDep212Right",&m_eDep212Right,"m_eDep212Right/D");
      tImpactReco->Branch("chargeChannel212Right",&m_chargeChannel212Right,"m_chargeChannel212Right/I");
      tImpactReco->Branch("eDep213Right",&m_eDep213Right,"m_eDep213Right/D");
      tImpactReco->Branch("chargeChannel213Right",&m_chargeChannel213Right,"m_chargeChannel213Right/I");
      tImpactReco->Branch("eDep214Right",&m_eDep214Right,"m_eDep214Right/D");
      tImpactReco->Branch("chargeChannel214Right",&m_chargeChannel214Right,"m_chargeChannel214Right/I");
      tImpactReco->Branch("eDep221Right",&m_eDep221Right,"m_eDep221Right/D");
      tImpactReco->Branch("chargeChannel221Right",&m_chargeChannel221Right,"m_chargeChannel221Right/I");
      tImpactReco->Branch("eDep222Right",&m_eDep222Right,"m_eDep222Right/D");
      tImpactReco->Branch("chargeChannel222Right",&m_chargeChannel222Right,"m_chargeChannel222Right/I");
      tImpactReco->Branch("eDep223Right",&m_eDep223Right,"m_eDep223Right/D");
      tImpactReco->Branch("chargeChannel223Right",&m_chargeChannel223Right,"m_chargeChannel223Right/I");
      tImpactReco->Branch("eDep224Right",&m_eDep224Right,"m_eDep224Right/D");
      tImpactReco->Branch("chargeChannel224Right",&m_chargeChannel224Right,"m_chargeChannel224Right/I"); 
      
      tImpactReco->Branch("xImpact",&m_xImpact,"m_xImpact/D");
      tImpactReco->Branch("yImpact",&m_yImpact,"m_yImpact/D");
			tImpactReco->Branch("zImpact",&m_zImpact,"m_zImpact/D");
			
			//tImpactReco->Branch("xImpact",&m_xImpact,"m_xImpact/D");
			
			tImpactReco->Branch("PxImpact",&m_PxImpact,"m_PxImpact/D");
			tImpactReco->Branch("PyImpact",&m_PyImpact,"m_PyImpact/D");
			tImpactReco->Branch("PzImpact",&m_PzImpact,"m_PzImpact/D");
			/*
			tImpactReco->Branch("m_Ntracks",&m_Ntracks,"m_Ntracks/I");			
			tImpactReco->Branch("m_xImpacts",m_xImpacts,"m_xImpacts[m_Ntracks]/D");
      tImpactReco->Branch("m_yImpacts",m_yImpacts,"m_yImpacts[m_Ntracks]/D");
			
			//tImpactReco->Branch("zImpacts",m_zImpacts,"m_zImpacts[m_Ntracks]/D");
			
			//tImpactReco->Branch("pids",m_pids,"m_pids[m_Ntracks]/I");
			//tImpactReco->Branch("pids",m_pids,"m_pids[m_Ntracks]/D");
			*/
			
			tImpactReco->Branch("xImpactElec",&m_xImpactElec,"m_xImpactElec/D");
      tImpactReco->Branch("yImpactElec",&m_yImpactElec,"m_yImpactElec/D");
			tImpactReco->Branch("xImpactPositron",&m_xImpactPositron,"m_xImpactPositron/D");
      tImpactReco->Branch("yImpactPositron",&m_yImpactPositron,"m_yImpactPositron/D");
    }

	m_event=0;
}

BesZddDigitizer::~BesZddDigitizer()
{
  std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 0" << std::endl;
  
//   chargeChannelFile->cd();
//   tEDepChargeChannel->Write();
//   chargeChannelFile->Close();
//   tEDepChargeChannel=NULL;
	
  if(impactReco)
    {
      fFile->cd();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 1" << std::endl;
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 2" << std::endl;
      tImpactReco->Write();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 3" << std::endl;
      fFile->Write();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 4" << std::endl;
      fFile->Close();
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 5" << std::endl;

      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 6" << std::endl;
      tImpactReco = NULL;
      std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer 7" << std::endl;
      fFile = NULL;
    }
  std::cout << "BesZddDigitizer.cc :: ~BesZddDigitizer end " << std::endl;
	
/*	
if(impactReco)
    {
      fFile->cd();
      fFile->Write();
      fFile->Close();
    }
*/		
}

void BesZddDigitizer::Initialize()
{
/*
  m_partID = 0;
  m_detectorID = 0;
  m_crystalNo = 0;
*/
  m_eDep = 0.;
  m_chargeChannel = 0.;
  
  m_eDep101Right = 0.; m_eDep102Right = 0.; m_eDep103Right = 0.; m_eDep104Right = 0.;
  m_eDep111Right = 0.; m_eDep112Right = 0.; m_eDep113Right = 0.; m_eDep114Right = 0.;
  m_eDep121Right = 0.; m_eDep122Right = 0.; m_eDep123Right = 0.; m_eDep124Right = 0.;
  m_eDep201Right = 0.; m_eDep202Right = 0.; m_eDep203Right = 0.; m_eDep204Right = 0.;
  m_eDep211Right = 0.; m_eDep212Right = 0.; m_eDep213Right = 0.; m_eDep214Right = 0.;
  m_eDep221Right = 0.; m_eDep222Right = 0.; m_eDep223Right = 0.; m_eDep224Right = 0.;
  m_chargeChannel101Right = 0; m_chargeChannel102Right = 0; m_chargeChannel103Right = 0; m_chargeChannel104Right = 0;
  m_chargeChannel111Right = 0; m_chargeChannel112Right = 0; m_chargeChannel113Right = 0; m_chargeChannel114Right = 0;
  m_chargeChannel121Right = 0; m_chargeChannel122Right = 0; m_chargeChannel123Right = 0; m_chargeChannel124Right = 0;
  m_chargeChannel201Right = 0; m_chargeChannel202Right = 0; m_chargeChannel203Right = 0; m_chargeChannel204Right = 0;
  m_chargeChannel211Right = 0; m_chargeChannel212Right = 0; m_chargeChannel213Right = 0; m_chargeChannel214Right = 0;
  m_chargeChannel221Right = 0; m_chargeChannel222Right = 0; m_chargeChannel223Right = 0; m_chargeChannel224Right = 0;

	m_zImpact=0;
	 
	m_xImpactElec=-99;  m_yImpactElec=-99;
	m_xImpactPositron=-99;  m_yImpactPositron=-99;
 smearFlag = false; // energy/time smearing on ?

 if(smearFlag) G4cout << "ZDDSIM:: BesZddDigitizer: digi smearing ON! " << G4endl;
 // else G4cout << "ZDDSIM:: BesZddDigitizer: digi smearing OFF! " << G4endl;
  
}

void BesZddDigitizer::Digitize()
{
	printf("Event #%d \n", m_event);
	m_event++;
//   std::cout<<"BesZddDigitizer::Digitize()\n";
  G4bool debug = false; // couts in Digitizer?
  
  Initialize();

	//MC-Truth informations
	BesSensitiveManager* sensitiveManager = BesSensitiveManager::GetSensitiveManager();
	vector<BesTruthTrack*> *trackList = sensitiveManager->GetTrackList();
	G4int nTrack = trackList->size();
	BesTruthTrack* track;
	
	vector<BesTruthVertex*>* vertexList =  sensitiveManager->GetVertexList();
  G4int nVertex = vertexList->size();
  BesTruthVertex* vertex;
	
	//arrange TruthTrack in trackList in order of trackIndex
  for(int i=0;i<nTrack-1;i++){
  	for(int j=i+1;j<nTrack;j++){
    	if((*trackList)[i]->GetIndex()>(*trackList)[j]->GetIndex())
    	{
      track=(*trackList)[i];
      (*trackList)[i]=(*trackList)[j];
      (*trackList)[j]=track;
    	}
		}
	}	
	
	/*
	Event::McParticleCol *particleCol = new Event::McParticleCol;
  
  //loop over tracks
  for(int i=0;i<nTrack;i++)
  {
    track = (*trackList) [i];
    
    // find out the start point
    bool isPrimary = false;
    bool startPointFound = false;
    BesTruthVertex* startPoint;
    
    for (int i=0;i<nVertex;i++) 
    {
      vertex = (*vertexList) [i];
      if ( track->GetVertex()->GetIndex() == vertex->GetIndex() ) 
      {
        if (!vertex->GetParentTrack()) isPrimary = true;
        startPointFound = true;
        startPoint = vertex;
        break;
      }
    }
	*/
			
  //chargeChannel threshold per crystal for digitization
  G4double threshold = 0.0;
  
  m_besZddDigisCollection = new BesZddDigisCollection(moduleName, collectionName[0]);
  G4DigiManager* DigiMan = G4DigiManager::GetDMpointer();
  
  //hits collection ID
  G4int THCID;
  THCID = DigiMan->GetHitsCollectionID("BesZddHitsCollection");
 
  //  G4cout << "ZddSim :: BesZddDigitizer : THCID: " << THCID << " " << &THCID <<  G4endl;
  
  //hits collection
  BesZddHitsCollection* THC = 0;
  THC = (BesZddHitsCollection*) (DigiMan->GetHitsCollection(THCID));

  //  G4cout << " ZddSim :: BesZddDigitizer : THC: " << THC << " " << &THC << G4endl;
	int index=0;
  if(THC) {

    /*     
      //fill zdd Ntuple
      if(m_G4Svc->ZddRootFlag())
	{
	  m_PartID = partID;
	  m_DetectorID = detectorID;
	  m_CrystalNo = crystalNo;
	  m_ChargeChannel = chargeChannel;
	  m_TimeChannel = timeChannel;
	  m_tupleZdd->write();
	}
    */
 
    G4cout << " if(THC) 1 " << G4endl;

    BesZddDigit *m_aDigit = new BesZddDigit();
    //    G4cout << " if(THC) 2 " << G4endl;
    G4int n_hit = THC->entries();
    //G4cout << " if(THC) 3 " << G4endl;
    
		m_Ntracks=(int)trackList->size();
		int hitPDGcode=0;
		int hitTrackID=0;
    for(G4int i = 0; i < n_hit; i++) 
      {
	BesZddHit* hit = (*THC)[i];	
	m_aDigit->SetHit( hit );
	//printf("%d %d %d \n", i,hit->GetTrackID(),hit->GetPDGCode());
			if(i==0) m_trackID=hit->GetTrackID();
			//cout << hit->GetXImpact() << endl;
			/*
			track=(*trackList)[hit->GetTrackID()];
			vertex=track->GetVertex();
			//printf("PRIMARY \n");
			//if (!vertex->GetParentTrack()) printf("PRIMARY \n");
			//Check if primary;
			//int Isprimary=0;
			printf("index Hit collection : %d hit->GetTrackID() : %d hit->GetPDGCode() : %d track->GetPDGCode () : %d \n", i,hit->GetTrackID(),hit->GetPDGCode(),track->GetPDGCode ());
			*/
			
			bool bFirstHitTrack=false;
			if(i==0)
				{
				bFirstHitTrack=true;				
				}
			else if(i>0 && hitTrackID!=hit->GetTrackID())
				{
				bFirstHitTrack=true;
				}
			else{bFirstHitTrack=false;}
			hitTrackID=hit->GetTrackID();
			hitPDGcode=hit->GetPDGCode();					
			//printf("%d %d %d \n",i,hit->GetTrackID(),hit->GetPDGCode());			
			//cout << "trackList->size()"<< trackList->size() << endl;
			for(int k=0;k<trackList->size();k++)
				{
				track=(*trackList)[k];
				//printf("%d %d %d \n",track->GetIndex(),hit->GetTrackID(),hit->GetPDGCode());
				//if(track->GetIndex()==hit->GetTrackID())
				
				//m_xImpacts[k]=-99;
				if(bFirstHitTrack && track->GetG4TrackId()==hit->GetTrackID())
					{
					//printf("index Hit collection : %d hit->GetTrackID() : %d hit->GetPDGCode() : %d track->GetPDGCode () : %d \n", i,hit->GetTrackID(),hit->GetPDGCode(),track->GetPDGCode ());
					
					//printf("%f %f %d\n",hit->GetXImpact(),hit->GetYImpact(),hit->GetPDGCode());
					double x = (double)hit->GetXImpact();
					double y = (double)hit->GetYImpact();
					int		pid = (int)hit->GetPDGCode();
					//m_xImpacts[k]=hit->GetXImpact();
					
					//m_xImpacts[index]=hit->GetXImpact();
					//m_yImpacts[index]=hit->GetYImpact();
					//m_pids[index]=hit->GetPDGCode();
					
					//m_xImpacts[index]=x;
					//m_yImpacts[index]=y;
					//m_pids[index]=pid;
					//cout << index << endl;
					
					vertex=track->GetVertex();
					if (!vertex->GetParentTrack()) 
						{
						if(pid==11)
							{
							m_xImpactElec=x;
							m_yImpactElec=y;
							}
						else if(pid==-11)
							{
							m_xImpactPositron=x;
							m_yImpactPositron=y;
							}	
						}
					
					index++;
					}
				}
				
				
			
				
      }
		/*	
		m_Ntracks=(int)trackList->size();
		for(int k=0;k<trackList->size();k++)
				{
				track=(*trackList)[k];
				BesZddHit* hit = (*THC)[0];
					if(track->GetG4TrackId()==hit->GetTrackID())
						{
						//m_xImpacts[k]=hit->GetXImpact();
						//printf("%d %f \n",k,hit->GetXImpact());
						}	
				}
		*/		
    m_aDigit->EndOfEvent();
    //    G4cout << " if(THC) 4 " << G4endl;

    if( m_aDigit->GetChargeChannel_101() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 1 );
      digi->SetDetectorID( 1 );
      digi->SetCrystalNo( 101 );
      G4int chargeChannel = m_aDigit->GetChargeChannel_101() ;
      G4int timeChannel = m_aDigit->GetTimeChannel_101() ;
      
      m_chargeChannel = chargeChannel;
      m_eDep = m_aDigit->totEDep_101;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " -101 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
      digi->SetChargeChannel( chargeChannel );
      digi->SetTimeChannel( timeChannel ) ;
      //Smear();  
      m_besZddDigisCollection->insert(digi);

     }

    if( m_aDigit->GetChargeChannel_102() >= threshold) // if chargeChannel above channel threshold  
    {
            G4cout << "EDep crystal -102: " << m_aDigit->GetChargeChannel_102() << G4endl;
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 102 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_102() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_102() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_102;
// //tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -102 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( m_aDigit->GetChargeChannel_103() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 103 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_103() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_103() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_103;
// //tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -103 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_104() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 104 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_104() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_104() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_104;
// //tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -104 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_111() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID ( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 111 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_111() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_111() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_111;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -111 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel_112() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 112 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_112() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_112() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_112;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -112 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_113() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 113 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_113() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_113() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_113;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -113 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_114() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 114 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_114() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_114() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_114;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -114 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_121() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 121 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_121() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_121() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_121;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -121 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel_122() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 122 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_122() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_122() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_122;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -122 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_123() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 123 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_123() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_123() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_123;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -123 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_124() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 124 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_124() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_124() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_124;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -124 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    // detectorID = 2 (lower crystals)
    if( m_aDigit->GetChargeChannel_201() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 1 );
      digi->SetDetectorID( 2 );
      digi->SetCrystalNo( 201 );
      G4int chargeChannel = m_aDigit->GetChargeChannel_201() ;
      G4int timeChannel = m_aDigit->GetTimeChannel_201() ;

      m_chargeChannel = chargeChannel;
      m_eDep = m_aDigit->totEDep_201;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " -201 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
      //Smear();  
      m_besZddDigisCollection->insert(digi);
    }

    if( m_aDigit->GetChargeChannel_202() >= threshold) // if chargeChannel above channel threshold   
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 202 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_202() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_202() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_202;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -202 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( m_aDigit->GetChargeChannel_203() >= threshold) // if chargeChannel above channel threshold 
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 203 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_203() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_203() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_203;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -203 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_204() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 204 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_204() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_204() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_204;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -204 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_211() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 211 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_211() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_211() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_211;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -211 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel_212() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 212 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_212() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_212() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_212;
//tEDepChargeChannel->Fill();

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	if(debug){ std::cout << " -212 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_213() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 213 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_213() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_213() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_213;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -213 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_214() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 214 );
        G4int chargeChannel = m_aDigit->GetChargeChannel_214() ;
        G4int timeChannel = m_aDigit->GetTimeChannel_214() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_214;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -214 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_221() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 221 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_221() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_221() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_221;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -221 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_222() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 222 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_222() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_222() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_222;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -222 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel_223() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 223 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_223() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_223() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_223;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -223 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	m_besZddDigisCollection->insert(digi);
      }      
      if( m_aDigit->GetChargeChannel_224() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 224 );
	G4int chargeChannel = m_aDigit->GetChargeChannel_224() ;
	G4int timeChannel = m_aDigit->GetTimeChannel_224() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep_224;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " -224 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	m_besZddDigisCollection->insert(digi);
      }
      //! part = 2 => right part
    if( m_aDigit->GetChargeChannel101() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 2 );
      digi->SetDetectorID( 1 );
      digi->SetCrystalNo( 101 );
      G4int chargeChannel = m_aDigit->GetChargeChannel101() ;
      G4int timeChannel = m_aDigit->GetTimeChannel101() ;

      m_chargeChannel = chargeChannel;
      m_eDep = m_aDigit->totEDep101;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " 101 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
      digi->SetChargeChannel( chargeChannel );
      digi->SetTimeChannel( timeChannel ) ;

      if(impactReco)
	{
	  m_eDep101Right = m_eDep;
	  m_chargeChannel101Right = m_chargeChannel;
	}


      //Smear();  
      m_besZddDigisCollection->insert(digi);

     }

    if( m_aDigit->GetChargeChannel102() >= threshold) // if chargeChannel above channel threshold  
    {
      //      G4cout << "EDep crystal -102: " << m_aDigit->GetChargeChannel102() << G4endl;
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 102 );
	G4int chargeChannel = m_aDigit->GetChargeChannel102() ;
	G4int timeChannel = m_aDigit->GetTimeChannel102() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep102;

//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 102 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep102Right = m_eDep;
	    m_chargeChannel102Right = m_chargeChannel;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( m_aDigit->GetChargeChannel103() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 103 );
	G4int chargeChannel = m_aDigit->GetChargeChannel103() ;
	G4int timeChannel = m_aDigit->GetTimeChannel103() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep103;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 103 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep103Right = m_eDep;
	    m_chargeChannel103Right = m_chargeChannel;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel104() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 104 );
	G4int chargeChannel = m_aDigit->GetChargeChannel104() ;
	G4int timeChannel = m_aDigit->GetTimeChannel104() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep104;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 104 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep104Right = m_eDep;
	    m_chargeChannel104Right = m_chargeChannel;
	  }

	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel111() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID ( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 111 );
	G4int chargeChannel = m_aDigit->GetChargeChannel111() ;
	G4int timeChannel = m_aDigit->GetTimeChannel111() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep111;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 111 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep111Right = m_eDep;
	    m_chargeChannel111Right = m_chargeChannel;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel112() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 112 );
        G4int chargeChannel = m_aDigit->GetChargeChannel112() ;
        G4int timeChannel = m_aDigit->GetTimeChannel112() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep112;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 112 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep112Right = m_eDep;
	    m_chargeChannel112Right = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel113() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 113 );
        G4int chargeChannel = m_aDigit->GetChargeChannel113() ;
        G4int timeChannel = m_aDigit->GetTimeChannel113() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep113;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 113 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep113Right = m_eDep;
	    m_chargeChannel113Right = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel114() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 114 );
        G4int chargeChannel = m_aDigit->GetChargeChannel114() ;
        G4int timeChannel = m_aDigit->GetTimeChannel114() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep114;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 114 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep114Right = m_eDep;
	    m_chargeChannel114Right = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel121() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 121 );
	G4int chargeChannel = m_aDigit->GetChargeChannel121() ;
	G4int timeChannel = m_aDigit->GetTimeChannel121() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep121;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 121 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep121Right = m_eDep;
	    m_chargeChannel121Right = m_chargeChannel;
	  }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel122() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 122 );
        G4int chargeChannel = m_aDigit->GetChargeChannel122() ;
        G4int timeChannel = m_aDigit->GetTimeChannel122() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep122;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 122 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep122Right = m_eDep;
	    m_chargeChannel122Right = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel123() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 123 );
        G4int chargeChannel = m_aDigit->GetChargeChannel123() ;
        G4int timeChannel = m_aDigit->GetTimeChannel123() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep123;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 123 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep123Right = m_eDep;
	    m_chargeChannel123Right = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel124() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 124 );
        G4int chargeChannel = m_aDigit->GetChargeChannel124() ;
        G4int timeChannel = m_aDigit->GetTimeChannel124() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep124;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 124 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep124Right = m_eDep;
	    m_chargeChannel124Right = m_chargeChannel;
	  }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
        // detectorID = 2 (lower crystals)
    if( m_aDigit->GetChargeChannel201() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 2 );
      digi->SetDetectorID( 2 );
      digi->SetCrystalNo( 201 );
      G4int chargeChannel = m_aDigit->GetChargeChannel201() ;
      G4int timeChannel = m_aDigit->GetTimeChannel201() ;

      m_chargeChannel = chargeChannel;
      m_eDep = m_aDigit->totEDep201;
//       tEDepChargeChannel->Fill();

      if(debug){ std::cout << " 201 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

	if(impactReco)
	  {
	    m_eDep201Right = m_eDep;
	    m_chargeChannel201Right = m_chargeChannel;
	  }

      //Smear();  
      m_besZddDigisCollection->insert(digi);
    }

    if( m_aDigit->GetChargeChannel202() >= threshold) // if chargeChannel above channel threshold   
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 202 );
	G4int chargeChannel = m_aDigit->GetChargeChannel202() ;
	G4int timeChannel = m_aDigit->GetTimeChannel202() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep202;
//tEDepChargeChannel->Fill();

	if(debug) std::cout << " BesZddDigitizer.cc : totEdep202 : " << m_eDep << std::endl;

	if(debug){ std::cout << " 202 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep202Right = m_eDep;
            m_chargeChannel202Right = m_chargeChannel;
          }

	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( m_aDigit->GetChargeChannel203() >= threshold) // if chargeChannel above channel threshold 
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 203 );
	G4int chargeChannel = m_aDigit->GetChargeChannel203() ;
	G4int timeChannel = m_aDigit->GetTimeChannel203() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep203;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 203 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep203Right = m_eDep;
            m_chargeChannel203Right = m_chargeChannel;
          }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel204() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 204 );
	G4int chargeChannel = m_aDigit->GetChargeChannel204() ;
	G4int timeChannel = m_aDigit->GetTimeChannel204() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep204;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 204 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep204Right = m_eDep;
            m_chargeChannel204Right = m_chargeChannel;
          }

	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel211() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 211 );
	G4int chargeChannel = m_aDigit->GetChargeChannel211() ;
	G4int timeChannel = m_aDigit->GetTimeChannel211() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep211;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 211 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep211Right = m_eDep;
            m_chargeChannel211Right = m_chargeChannel;
          }

	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( m_aDigit->GetChargeChannel212() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 212 );
        G4int chargeChannel = m_aDigit->GetChargeChannel212() ;
        G4int timeChannel = m_aDigit->GetTimeChannel212() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep212;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 212 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep212Right = m_eDep;
            m_chargeChannel212Right = m_chargeChannel;
          }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel213() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 213 );
        G4int chargeChannel = m_aDigit->GetChargeChannel213() ;
        G4int timeChannel = m_aDigit->GetTimeChannel213() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep213;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 213 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep213Right = m_eDep;
            m_chargeChannel213Right = m_chargeChannel;
          }

	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel214() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 214 );
        G4int chargeChannel = m_aDigit->GetChargeChannel214() ;
        G4int timeChannel = m_aDigit->GetTimeChannel214() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep214;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 214 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;

        if(impactReco)
          {
            m_eDep214Right = m_eDep;
            m_chargeChannel214Right = m_chargeChannel;
          }

	//	Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel221() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 221 );
	G4int chargeChannel = m_aDigit->GetChargeChannel221() ;
	G4int timeChannel = m_aDigit->GetTimeChannel221() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep221;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 221 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep221Right = m_eDep;
            m_chargeChannel221Right = m_chargeChannel;
          }

	m_besZddDigisCollection->insert(digi);
      }
      if( m_aDigit->GetChargeChannel222() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 222 );
	G4int chargeChannel = m_aDigit->GetChargeChannel222() ;
	G4int timeChannel = m_aDigit->GetTimeChannel222() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep222;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 222 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep222Right = m_eDep;
            m_chargeChannel222Right = m_chargeChannel;
          }

	m_besZddDigisCollection->insert(digi);
      }
    if( m_aDigit->GetChargeChannel223() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 223 );
	G4int chargeChannel = m_aDigit->GetChargeChannel223() ;
	G4int timeChannel = m_aDigit->GetTimeChannel223() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep223;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 223 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep223Right = m_eDep;
            m_chargeChannel223Right = m_chargeChannel;
          }

	m_besZddDigisCollection->insert(digi);
      }      
      if( m_aDigit->GetChargeChannel224() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 224 );
	G4int chargeChannel = m_aDigit->GetChargeChannel224() ;
	G4int timeChannel = m_aDigit->GetTimeChannel224() ;

	m_chargeChannel = chargeChannel;
	m_eDep = m_aDigit->totEDep224;
//tEDepChargeChannel->Fill();

	if(debug){ std::cout << " 224 BesZddDigitizer: ChargeChannel, TimeChannel : " << chargeChannel << "\t" << timeChannel << std::endl;}
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 

        if(impactReco)
          {
            m_eDep224Right = m_eDep;
            m_chargeChannel224Right = m_chargeChannel;
          }

	m_besZddDigisCollection->insert(digi);
      }
        
    StoreDigiCollection(m_besZddDigisCollection);	

    if(impactReco)
      {
	m_xImpact = m_aDigit->GetXImpact();
	m_yImpact = m_aDigit->GetYImpact();
	m_zImpact = m_aDigit->GetZImpact();

	m_PxImpact = m_aDigit->GetPxImpact();
	m_PyImpact = m_aDigit->GetPyImpact();
	m_PzImpact = m_aDigit->GetPzImpact();
	
	tImpactReco->Fill();

	//	fFile->cd();
	//	tEDepChargeChannel->Write();
	//	tImpactReco->Write();
      }      
    delete m_aDigit;
  } 
  //  G4cout << "ZddSim :: BesZddDigitizer : End of Digitization" << G4endl;
  //  int test;
  //  G4cin >> test;
}

G4double BesZddDigitizer::Smear(G4double var)
{ 
//   G4cout << "eDep before smearing: " << G4BestUnit(edep,"Energy") << G4endl;
//   G4cout << "time before smearing: " << G4BestUnit(time,"Time") << G4endl;
  
  G4double gaussrand = G4RandGauss::shoot(0.,0.5);
  return var+(gaussrand*var);
  
//   G4cout << "eDep after smearing:  " << G4BestUnit(edep,"Energy") << G4endl;
//   G4cout << "time after smearing:  " << G4BestUnit(time,"Time") << G4endl;
//   
//   G4cout << "gaussrand1:           " << gaussrand1 << G4endl;
//   G4cout << "gaussrand2:           " << gaussrand2 << G4endl << G4endl;
}

