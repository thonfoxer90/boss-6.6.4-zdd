//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description:
//Author: Youzy
//Created: July, 2004
//Modified:
//Comment:
//---------------------------------------------------------------------------//
// $Id: BesZddDigi.cc 

#include "BesZddDigi.hh"
#include "G4UnitsTable.hh"

G4Allocator<BesZddDigi> BesZddDigiAllocator;

BesZddDigi::BesZddDigi()
{
  m_PartID = -99;
  m_CrystalNo = -99;
  m_ChargeChannel  = -99.;
  m_TimeChannel = -99.;
}

BesZddDigi::~BesZddDigi() {}

BesZddDigi::BesZddDigi(const BesZddDigi& right)
:G4VDigi()
{
  m_PartID        = right.m_PartID;
  m_CrystalNo     = right.m_CrystalNo;
  m_ChargeChannel = right.m_ChargeChannel;
  m_TimeChannel   = right.m_TimeChannel;
}


const BesZddDigi& BesZddDigi::operator=(const BesZddDigi& right)
{
  m_PartID        = right.m_PartID;
  m_CrystalNo     = right.m_CrystalNo;
  m_ChargeChannel = right.m_ChargeChannel;
  m_TimeChannel   = right.m_TimeChannel;
   return *this;
}


int BesZddDigi::operator==(const BesZddDigi& right) const
{
  return (this==&right) ? 1 : 0;
}

void BesZddDigi::Draw()
{ }

void BesZddDigi::Print()
{
  G4cout << "  ZddSim :: BesZddDigi.cc " << G4endl;
  G4cout << "  partID:                 " << m_PartID << G4endl
         << "  crystalNo:              " << m_CrystalNo << G4endl    
         << "  chargeChannel: (energy) " << m_ChargeChannel << G4endl
	 << "  timeChannel:   (time)   " << m_TimeChannel << G4endl
         << "===================================================" << G4endl;
}


