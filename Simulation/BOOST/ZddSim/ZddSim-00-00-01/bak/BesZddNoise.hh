//
//
//
//


#ifndef BesZddNoise_h
#define BesZddNoise_h 1

#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/MsgStream.h"

#include "BesZddHit.hh"
#include "G4Step.hh"
#include "G4Event.hh"
#include "G4LogicalVolume.hh"

#include "TMath.h"

#include "ZddCalibAlg/ZddStructConst.h"
#include "ZddCalibAlg/ZddIdTransform.h"
#include "ZddCalibAlg/ZddMark.h"
#include "ZddCalibConstSvc/IZddCalibConstSvc.h"
#include "ZddCalibConstSvc/ZddCalibConstSvc.h"


using namespace std;

class BesZddNoise
{
public:
  BesZddNoise();
  ~BesZddNoise();

  static BesZddNoise* Instance(void);  
  void Initialize(G4String filename,G4LogicalVolume* logicalZdd);
  void Initialize(G4String filename,G4LogicalVolume* logicalZdd,G4String temp);

  void CheckCalibSvc();	  
  G4int AddNoise( int model, BesZddHitsCollection* ZddHitCollection,BesZddHitsCollection* ZddHitList);
  G4int NoiseByCnt(BesZddHitsCollection* ZddHitCollection,BesZddHitsCollection* ZddHitList);
  G4int NoiseByNosRatio(BesZddHitsCollection* ZddHitCollection,BesZddHitsCollection* ZddHitList);
  G4int NoiseSampling(int level, int prt, int seg, int lay, int strip);
  G4int GetStripNo(G4int,G4int,G4int);
  G4int IsNearestStrip(G4int,G4int,G4int,G4int,G4float);
  bool IsExist(BesZddHit* aNoiseHit, BesZddHitsCollection* aZddHitList);  
  G4float Factorial(G4int i);
  void InitProb();

private:

  static const int m_kPart            =3;
  static const int m_kSegment[m_kPart]  ;
  static const int m_kAbsorber[m_kPart] ;
  static const int m_kGap[m_kPart]      ;
  static const int m_kPanel[m_kPart]    ;
  static const int m_kGasChamber      =2;
    
  G4double m_noise[3][8][9];
  G4double m_noise_strip[3][8][9][112];
  G4float area[3][8][9][112];
  G4float strip_area[3][8][9][112];
  G4float box_area[3][8][9];
  double m_HitMean;
  double m_Prob[20][2];
  static BesZddNoise * fPointer;

  int m_noiseLevel; //2: box;  3: strip 
  IZddCalibConstSvc*  m_ptrCalibSvc;   // pointer of calibration constants service
  ZddIdTransform*     m_ptrIdTr;       // pointer of zdd id transform 

};
#endif
