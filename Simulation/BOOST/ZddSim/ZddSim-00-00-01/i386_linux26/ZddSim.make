#-- start of make_header -----------------

#====================================
#  Library ZddSim
#
#   Generated Mon Oct 22 18:33:38 2012  by mwerner
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ZddSim_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ZddSim_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ZddSim

ZddSim_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddSim = /tmp/CMT_$(ZddSim_tag)_ZddSim.make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddSim = $(ZddSim_tag)_ZddSim.make
endif

else

tags      = $(tag),$(CMTEXTRATAGS)

ZddSim_tag = $(tag)

ifdef READONLY
cmt_local_tagfile_ZddSim = /tmp/CMT_$(ZddSim_tag).make$(cmt_lock_pid)
else
cmt_local_tagfile_ZddSim = $(ZddSim_tag).make
endif

endif

-include $(cmt_local_tagfile_ZddSim)

ifdef cmt_ZddSim_has_target_tag

ifdef READONLY
cmt_final_setup_ZddSim = /tmp/CMT_ZddSim_ZddSimsetup.make
cmt_local_ZddSim_makefile = /tmp/CMT_ZddSim$(cmt_lock_pid).make
else
cmt_final_setup_ZddSim = $(bin)ZddSim_ZddSimsetup.make
cmt_local_ZddSim_makefile = $(bin)ZddSim.make
endif

else

ifdef READONLY
cmt_final_setup_ZddSim = /tmp/CMT_ZddSimsetup.make
cmt_local_ZddSim_makefile = /tmp/CMT_ZddSim$(cmt_lock_pid).make
else
cmt_final_setup_ZddSim = $(bin)ZddSimsetup.make
cmt_local_ZddSim_makefile = $(bin)ZddSim.make
endif

endif

ifdef READONLY
cmt_final_setup = /tmp/CMT_ZddSimsetup.make
else
cmt_final_setup = $(bin)ZddSimsetup.make
endif

ZddSim ::


ifdef READONLY
ZddSim ::
	@echo tags=$(tags)
	@echo cmt_local_tagfile=$(cmt_local_tagfile)
endif


dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	@echo 'ZddSim'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ZddSim/
ZddSim::
	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
	@echo "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

#-- end of make_header ------------------

#-- start of libary_header ---------------

ZddSimlibname   = $(bin)$(library_prefix)ZddSim$(library_suffix)
ZddSimlib       = $(ZddSimlibname).a
ZddSimstamp     = $(bin)ZddSim.stamp
ZddSimshstamp   = $(bin)ZddSim.shstamp

ZddSim :: dirs  ZddSimLIB
	@/bin/echo "------> ZddSim ok"

#-- end of libary_header ----------------

ZddSimLIB :: $(ZddSimlib) $(ZddSimshstamp)
	@/bin/echo "------> ZddSim : library ok"

$(ZddSimlib) :: $(bin)BesZddDigi.o $(bin)BesZddDigit.o $(bin)BesZddSD.o $(bin)BesZddDigitizer.o $(bin)BesZddHit.o $(bin)BesZddConstruction.o $(bin)Photon.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(ZddSimlib) $?
	$(lib_silent) $(ranlib) $(ZddSimlib)
	$(lib_silent) cat /dev/null >$(ZddSimstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(ZddSimlibname).$(shlibsuffix) :: $(ZddSimlib) $(ZddSimstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" ZddSim $(ZddSim_shlibflags)

$(ZddSimshstamp) :: $(ZddSimlibname).$(shlibsuffix)
	@if test -f $(ZddSimlibname).$(shlibsuffix) ; then cat /dev/null >$(ZddSimshstamp) ; fi

ZddSimclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)BesZddDigi.o $(bin)BesZddDigit.o $(bin)BesZddSD.o $(bin)BesZddDigitizer.o $(bin)BesZddHit.o $(bin)BesZddConstruction.o $(bin)Photon.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
ZddSiminstallname = $(library_prefix)ZddSim$(library_suffix).$(shlibsuffix)

ZddSim :: ZddSiminstall

install :: ZddSiminstall

ZddSiminstall :: $(install_dir)/$(ZddSiminstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(ZddSiminstallname) :: $(bin)$(ZddSiminstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(ZddSiminstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(ZddSiminstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddSiminstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(ZddSiminstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(ZddSiminstallname) $(install_dir)/$(ZddSiminstallname); \
	      echo `pwd`/$(ZddSiminstallname) >$(install_dir)/$(ZddSiminstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(ZddSiminstallname), no installation directory specified"; \
	  fi; \
	fi

ZddSimclean :: ZddSimuninstall

uninstall :: ZddSimuninstall

ZddSimuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(ZddSiminstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddSiminstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(ZddSiminstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(ZddSiminstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------

$(bin)ZddSim_dependencies.make :: dirs

ifndef QUICK
$(bin)ZddSim_dependencies.make :: $(src)*.cc  requirements $(use_requirements) $(cmt_final_setup_ZddSim)
	@echo "------> (ZddSim.make) Rebuilding $@"; \
	  args=`echo $? | sed -e "s#requirements.*##"`; \
	  $(build_dependencies) ZddSim -all_sources $${args}
endif

#$(ZddSim_dependencies)

-include $(bin)ZddSim_dependencies.make

#-- end of dependency -------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)BesZddDigi.o : $(BesZddDigi_cc_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)BesZddDigi.o $(use_pp_cppflags) $(ZddSim_pp_cppflags) $(lib_ZddSim_pp_cppflags) $(BesZddDigi_pp_cppflags) $(use_cppflags) $(ZddSim_cppflags) $(lib_ZddSim_cppflags) $(BesZddDigi_cppflags) $(BesZddDigi_cc_cppflags)  $(src)BesZddDigi.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)BesZddDigit.o : $(BesZddDigit_cc_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)BesZddDigit.o $(use_pp_cppflags) $(ZddSim_pp_cppflags) $(lib_ZddSim_pp_cppflags) $(BesZddDigit_pp_cppflags) $(use_cppflags) $(ZddSim_cppflags) $(lib_ZddSim_cppflags) $(BesZddDigit_cppflags) $(BesZddDigit_cc_cppflags)  $(src)BesZddDigit.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)BesZddSD.o : $(BesZddSD_cc_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)BesZddSD.o $(use_pp_cppflags) $(ZddSim_pp_cppflags) $(lib_ZddSim_pp_cppflags) $(BesZddSD_pp_cppflags) $(use_cppflags) $(ZddSim_cppflags) $(lib_ZddSim_cppflags) $(BesZddSD_cppflags) $(BesZddSD_cc_cppflags)  $(src)BesZddSD.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)BesZddDigitizer.o : $(BesZddDigitizer_cc_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)BesZddDigitizer.o $(use_pp_cppflags) $(ZddSim_pp_cppflags) $(lib_ZddSim_pp_cppflags) $(BesZddDigitizer_pp_cppflags) $(use_cppflags) $(ZddSim_cppflags) $(lib_ZddSim_cppflags) $(BesZddDigitizer_cppflags) $(BesZddDigitizer_cc_cppflags)  $(src)BesZddDigitizer.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)BesZddHit.o : $(BesZddHit_cc_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)BesZddHit.o $(use_pp_cppflags) $(ZddSim_pp_cppflags) $(lib_ZddSim_pp_cppflags) $(BesZddHit_pp_cppflags) $(use_cppflags) $(ZddSim_cppflags) $(lib_ZddSim_cppflags) $(BesZddHit_cppflags) $(BesZddHit_cc_cppflags)  $(src)BesZddHit.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)BesZddConstruction.o : $(BesZddConstruction_cc_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)BesZddConstruction.o $(use_pp_cppflags) $(ZddSim_pp_cppflags) $(lib_ZddSim_pp_cppflags) $(BesZddConstruction_pp_cppflags) $(use_cppflags) $(ZddSim_cppflags) $(lib_ZddSim_cppflags) $(BesZddConstruction_cppflags) $(BesZddConstruction_cc_cppflags)  $(src)BesZddConstruction.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------
#
$(bin)$(binobj)Photon.o : $(Photon_cc_dependencies)
	$(cpp_echo) $@
	$(cpp_silent) cd $(bin); $(cppcomp) -o $(binobj)Photon.o $(use_pp_cppflags) $(ZddSim_pp_cppflags) $(lib_ZddSim_pp_cppflags) $(Photon_pp_cppflags) $(use_cppflags) $(ZddSim_cppflags) $(lib_ZddSim_cppflags) $(Photon_cppflags) $(Photon_cc_cppflags)  $(src)Photon.cc

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: ZddSimclean
	@cd .

ifndef PEDANTIC
.DEFAULT::
	@echo "WARNING >> You should provide a target for $@"
else
.DEFAULT::
	@echo "PEDANTIC MODE >> You should provide a target for $@"
	@if test `expr index $@ '.'` != 0 ; \
	then  echo "PEDANTIC MODE >> This target seems to be a missing file, please check"; exit -1 ;\
	else echo "PEDANTIC MODE >> Just ignore it ; as it seems to be just a fake target due to some pattern" ; exit 0; fi; 		
endif

ZddSimclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(binobj)$(library_prefix)ZddSim$(library_suffix).a $(binobj)$(library_prefix)ZddSim$(library_suffix).s? $(binobj)ZddSim.stamp $(binobj)ZddSim.shstamp
#-- end of cleanup_library ---------------

