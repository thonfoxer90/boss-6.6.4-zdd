#ifndef BesZddDigitizer_h
#define BesZddDigitizer_h 1

#include "G4VDigitizerModule.hh"
#include "BesZddDigi.hh"
#include "globals.hh"
#include "G4VPhysicalVolume.hh"

class BesZddDigitizer : public G4VDigitizerModule
{
public:
  BesZddDigitizer(G4String modName);
  ~BesZddDigitizer();
  
  //necessary digi collection object must be constructed and set to 
  //G4DCofThisEvent by StoreDigiCollection protected method.
  virtual void Digitize();
  
private:
  void Initialize();
  G4double Smear(G4double var);

private:
  BesZddDigisCollection* m_besZddDigisCollection;
};


#endif

