
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef BesZddDigi_h
#define BesZddDigi_h 1

#include "G4VDigi.hh"
#include "G4TDigiCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
//#include "G4VPhysicalVolume.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


class BesZddDigi : public G4VDigi
{
public:
  BesZddDigi();
  virtual ~BesZddDigi();
  
  BesZddDigi(const BesZddDigi&);
  const BesZddDigi& operator=(const BesZddDigi&);
  
  virtual G4int operator==(const BesZddDigi&) const;
  inline void* operator new(size_t);
  inline void  operator delete(void*);
  
  virtual void Draw();
  virtual void Print();
  
public:

  // Set part ID // 1 = east zdd; // 2 = west zdd 
  void SetPartID (G4int partID) {m_PartID = partID; };

  // Set detector ID // 1 = upper crystals, 2 = lower crystals
  void SetDetectorID (G4int detectorID) {m_DetectorID = detectorID; };

  // Set Crystal No
  void SetCrystalNo        (G4int crystalNo) {m_CrystalNo = crystalNo; };

  // Set Energy deposition // bigger charge means more energy
  void SetChargeChannel          (G4double chargeChannel){ m_ChargeChannel = chargeChannel; };

  // set time, when signal was triggered
  void SetTimeChannel           (G4double timeChannel) { m_TimeChannel = timeChannel;  };
  
  // Get method
  G4int GetPartID() {return m_PartID; };

  G4int GetDetectorID() {return m_DetectorID; };

  G4int GetCrystalNo() { return m_CrystalNo; };

  G4double GetChargeChannel() { return m_ChargeChannel; };
 
  G4double GetTimeChannel() { return m_TimeChannel; };

private:

  // PartID: 1 = left Zdd, 2 = right Zdd // viewpoint from BesIII control room 
  G4int         m_PartID;
  // DetectorID: 1 = upper crystals, 2 = lower crystals
  G4int         m_DetectorID;
  // CrystalNo
  G4int         m_CrystalNo;
  // charge Channel
  G4double      m_ChargeChannel;
  // time channel
  G4double      m_TimeChannel;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

typedef G4TDigiCollection<BesZddDigi> BesZddDigisCollection;

extern G4Allocator<BesZddDigi> BesZddDigiAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* BesZddDigi::operator new(size_t)
{
  void *aDigi;
  aDigi = (void *) BesZddDigiAllocator.MallocSingle();
  return aDigi;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void BesZddDigi::operator delete(void *aDigi)
{
  BesZddDigiAllocator.FreeSingle((BesZddDigi*) aDigi);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
