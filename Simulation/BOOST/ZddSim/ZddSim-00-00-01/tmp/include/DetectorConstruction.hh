//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: DetectorConstruction.hh,v 1.1 2010/10/18 15:56:17 maire Exp $
// GEANT4 tag $Name: geant4-09-04 $
//
// 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"

class G4Box;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4UniformMagField;
class DetectorMessenger;
class SensitiveDetector;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
  
    DetectorConstruction();
   ~DetectorConstruction();

    public:
     
     void SetCrystalMaterial (G4String);     
     
//! Check in which crystal given position is in     
     static G4int CheckPosition(G4double x, G4double y, G4double z);
     static G4double GetDistanceToPMT(G4double z);
     
     void SetMagField(G4double);
     
     G4VPhysicalVolume* Construct();

     void UpdateGeometry();
     
  public:
  
                    
     G4double GetWorldSizeX()           {return WorldSizeX;}; 
     G4double GetWorldSizeY()           {return WorldSizeY;};
     G4double GetWorldSizeZ()		{return WorldSizeZ;};
     
static G4ThreeVector GetUpperCrystalBox() {return sUpperCrystalBox;};
static G4ThreeVector GetLowerCrystalBox() {return sLowerCrystalBox;};
     
static G4double GetCrystalBoxSizeX()      {return sCrystalBoxSizeX;};
static G4double GetCrystalBoxSizeY()      {return sCrystalBoxSizeY;};
static G4double GetCrystalBoxSizeZ()      {return sCrystalBoxSizeZ;};

static G4double GetCrystalX()      {return sCrystalX;};
static G4double GetCrystalY()      {return sCrystalY;};
static G4double GetCrystalZ()      {return sCrystalZ;};

     G4Material* GetCrystalMaterial()   {return CrystalMaterial;};
     
     const G4VPhysicalVolume* GetphysiWorld() {return physiWorld;};
     SensitiveDetector* GetSD() {return SD;};

     void SetNOfCrystals(G4int n){nOfCrystals = n;};
     G4int GetNOfCrystals(){return nOfCrystals;};

  private:
     
     G4Material*        CrystalMaterial;
     SensitiveDetector* SD;
     
     G4Material*        defaultMaterial;
    
     G4double           WorldSizeX;
     G4double           WorldSizeY;
     G4double           WorldSizeZ;
    
     G4int 		nOfCrystals;
     
     G4Box*             solidWorld;    //pointer to the solid World 
     G4LogicalVolume*   logicWorld;    //pointer to the logical World
     G4VPhysicalVolume* physiWorld;    //pointer to the physical World

     G4LogicalVolume*   crystal_log;
     G4VPhysicalVolume* crystal_phys;
     
     G4UniformMagField* magField;      //pointer to the magnetic field
     
     static const G4double sCrystalBoxSizeX; 
     static const G4double sCrystalBoxSizeY; 
     static const G4double sCrystalBoxSizeZ;
     
     static const G4double sCrystalX;
     static const G4double sCrystalY;
     static const G4double sCrystalZ;
     
     static const G4ThreeVector sUpperCrystalBox;
     static const G4ThreeVector sLowerCrystalBox;

  private:
    
     void DefineMaterials();
     void ComputeWorldSize();
     G4VPhysicalVolume* ConstructCrystals();     
};
      
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void DetectorConstruction::ComputeWorldSize()
{
  // Compute World Size
     WorldSizeX = 10.*sCrystalBoxSizeX;
     WorldSizeY = 5.*sCrystalBoxSizeY;
     WorldSizeZ = 10.*sCrystalBoxSizeZ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

