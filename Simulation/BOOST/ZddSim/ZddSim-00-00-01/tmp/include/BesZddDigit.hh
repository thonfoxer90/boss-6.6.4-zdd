//Description: Digitization of BesZdd Hits

#ifndef BesZddDigit_h
#define BesZddDigit_h 1

#include "BesZddHit.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "TH1D.h"
#include "TH3D.h"
#include "TF1.h"
#include "TMath.h"
#include "TRandom.h"
#include "Photon.hh"
#include <vector>

class BesZddDigit
{
public:
  BesZddDigit();
  ~BesZddDigit();
  //BesZddDigit(const BesZddDigit&);
  //const BesZddDigit$ operator=(const BesZddDigit&);
  //int operator==(const BesZddDigit&) const;

  //inline void* operator new(size_t);
  //inline void operator delete(void*);
  void SetHit(BesZddHit* hit);
  void PhotonCalculations( G4int& a, Photon& photon );

  void EndOfEvent();
//  void ResizeVectors(Photon& photon);
  void CalculatePMToutput(Photon& photon); 
  
  // Get energy deposited in certain crystal
      G4double GetTotEDep()    					{ return m_totEDep; };
      G4int GetTrackID()					{ return m_TrackID; };
      
      G4double GetRandomTimeEmit()				{ return fTimeEmit->GetRandom(); };  //! Get random variable distributed as function fTimeEmit
      
      G4double GetChargeChannel101()			{ if(Photon101!=NULL) return Photon101->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel102()			{ if(Photon102!=NULL) return Photon102->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel103()			{ if(Photon103!=NULL) return Photon103->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel104()			{ if(Photon104!=NULL) return Photon104->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel111()			{ if(Photon111!=NULL) return Photon111->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel112()			{ if(Photon112!=NULL) return Photon112->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel113()			{ if(Photon113!=NULL) return Photon113->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel114()			{ if(Photon114!=NULL) return Photon114->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel121()			{ if(Photon121!=NULL) return Photon121->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel122()			{ if(Photon122!=NULL) return Photon122->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel123()			{ if(Photon123!=NULL) return Photon123->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel124()			{ if(Photon124!=NULL) return Photon124->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel201()			{ if(Photon201!=NULL) return Photon201->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel202()			{ if(Photon202!=NULL) return Photon202->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel203()			{ if(Photon203!=NULL) return Photon203->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel204()			{ if(Photon204!=NULL) return Photon204->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel211()			{ if(Photon211!=NULL) return Photon211->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel212()			{ if(Photon212!=NULL) return Photon212->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel213()			{ if(Photon213!=NULL) return Photon213->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel214()			{ if(Photon214!=NULL) return Photon214->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel221()			{ if(Photon221!=NULL) return Photon221->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel222()			{ if(Photon222!=NULL) return Photon222->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel223()			{ if(Photon223!=NULL) return Photon223->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel224()			{ if(Photon224!=NULL) return Photon224->GetChargeChannel(); else return -99.; };

      // Get timing information of certain crystal
      G4double GetTimeChannel101()			{ if(Photon101!=NULL) return Photon101->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel102()			{ if(Photon102!=NULL) return Photon102->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel103()			{ if(Photon103!=NULL) return Photon103->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel104()			{ if(Photon104!=NULL) return Photon104->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel111()			{ if(Photon111!=NULL) return Photon111->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel112()			{ if(Photon112!=NULL) return Photon112->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel113()			{ if(Photon113!=NULL) return Photon113->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel114()			{ if(Photon114!=NULL) return Photon114->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel121()			{ if(Photon121!=NULL) return Photon121->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel122()			{ if(Photon122!=NULL) return Photon122->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel123()			{ if(Photon123!=NULL) return Photon123->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel124()			{ if(Photon124!=NULL) return Photon124->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel201()			{ if(Photon201!=NULL) return Photon201->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel202()			{ if(Photon202!=NULL) return Photon202->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel203()			{ if(Photon203!=NULL) return Photon203->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel204()			{ if(Photon204!=NULL) return Photon204->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel211()			{ if(Photon211!=NULL) return Photon211->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel212()			{ if(Photon212!=NULL) return Photon212->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel213()			{ if(Photon213!=NULL) return Photon213->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel214()			{ if(Photon214!=NULL) return Photon214->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel221()			{ if(Photon221!=NULL) return Photon221->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel222()			{ if(Photon222!=NULL) return Photon222->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel223()			{ if(Photon223!=NULL) return Photon223->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel224()			{ if(Photon224!=NULL) return Photon224->GetTimeChannel(); else return -99.; };

      G4double GetChargeChannel_101()			{ if(Photon_101!=NULL) return Photon_101->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_102()			{ if(Photon_102!=NULL) return Photon_102->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_103()			{ if(Photon_103!=NULL) return Photon_103->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_104()			{ if(Photon_104!=NULL) return Photon_104->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_111()			{ if(Photon_111!=NULL) return Photon_111->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_112()			{ if(Photon_112!=NULL) return Photon_112->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_113()			{ if(Photon_113!=NULL) return Photon_113->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_114()			{ if(Photon_114!=NULL) return Photon_114->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_121()			{ if(Photon_121!=NULL) return Photon_121->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_122()			{ if(Photon_122!=NULL) return Photon_122->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_123()			{ if(Photon_123!=NULL) return Photon_123->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_124()			{ if(Photon_124!=NULL) return Photon_124->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_201()			{ if(Photon_201!=NULL) return Photon_201->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_202()			{ if(Photon_202!=NULL) return Photon_202->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_203()			{ if(Photon_203!=NULL) return Photon_203->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_204()			{ if(Photon_204!=NULL) return Photon_204->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_211()			{ if(Photon_211!=NULL) return Photon_211->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_212()			{ if(Photon_212!=NULL) return Photon_212->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_213()			{ if(Photon_213!=NULL) return Photon_213->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_214()			{ if(Photon_214!=NULL) return Photon_214->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_221()			{ if(Photon_221!=NULL) return Photon_221->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_222()			{ if(Photon_222!=NULL) return Photon_222->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_223()			{ if(Photon_223!=NULL) return Photon_223->GetChargeChannel(); else return -99.; };
      G4double GetChargeChannel_224()			{ if(Photon_224!=NULL) return Photon_224->GetChargeChannel(); else return -99.; };

      // Get timing information of certain crystal
      G4double GetTimeChannel_101()			{ if(Photon_101!=NULL) return Photon_101->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_102()			{ if(Photon_102!=NULL) return Photon_102->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_103()			{ if(Photon_103!=NULL) return Photon_103->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_104()			{ if(Photon_104!=NULL) return Photon_104->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_111()			{ if(Photon_111!=NULL) return Photon_111->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_112()			{ if(Photon_112!=NULL) return Photon_112->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_113()			{ if(Photon_113!=NULL) return Photon_113->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_114()			{ if(Photon_114!=NULL) return Photon_114->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_121()			{ if(Photon_121!=NULL) return Photon_121->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_122()			{ if(Photon_122!=NULL) return Photon_122->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_123()			{ if(Photon_123!=NULL) return Photon_123->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_124()			{ if(Photon_124!=NULL) return Photon_124->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_201()			{ if(Photon_201!=NULL) return Photon_201->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_202()			{ if(Photon_202!=NULL) return Photon_202->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_203()			{ if(Photon_203!=NULL) return Photon_203->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_204()			{ if(Photon_204!=NULL) return Photon_204->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_211()			{ if(Photon_211!=NULL) return Photon_211->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_212()			{ if(Photon_212!=NULL) return Photon_212->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_213()			{ if(Photon_213!=NULL) return Photon_213->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_214()			{ if(Photon_214!=NULL) return Photon_214->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_221()			{ if(Photon_221!=NULL) return Photon_221->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_222()			{ if(Photon_222!=NULL) return Photon_222->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_223()			{ if(Photon_223!=NULL) return Photon_223->GetTimeChannel(); else return -99.; };
      G4double GetTimeChannel_224()			{ if(Photon_224!=NULL) return Photon_224->GetTimeChannel(); else return -99.; };
  private:
      G4int         		m_TrackID;
      G4int 	    		m_CrystalNo;
      // total energy depostion per event
      G4double      		m_totEDep;

private:
  BesZddHit* 		m_pHit;

  static G4int photonConstant;  //! number of photons per 1 MeV energy deposition -> has to be adjusted to real crystal data
  static G4double thetaLoss; //! percentage of photons which are lost because of theta angle
  static G4double mismatchLoss; //! percentage of photons which are lost because of crystal PMT <-> scintillator crosssection mismatch
  static G4double angularAcceptanceLoss; //! percentage of photons which are lost because of angular acceptance of PMT
  static G4double QELoss; //! percentage of photons which are lost because of quantum efficiency of PMT;
  
  
  static G4double photonsPerMeV; //! average number of Photons per MeV

  static G4int    eventCounter; //! how many events where simulated
/*  
  static TRandom* uniRand1; //! generator for random numbers 1
  static TRandom* uniRand2; //! generator for random numbers 2 
  static TRandom* uniRand3; //! generator for random numbers 3    
  static TRandom* uniRand4; //! generator for random numbers 4 
  static TRandom* uniRand5; //! generator for random numbers 5   

//! 5 generators because distributions should be totally indepedent => 5 different, independent photon killing sources

  static const G4int m_seed1; //! seed of random generator 1
  static const G4int m_seed2; //! seed of random generator 2
  static const G4int m_seed3; //! seed of random generator 3 
  static const G4int m_seed4; //! seed of random generator 4
  static const G4int m_seed5; //! seed of random generator 5
*/
//! histo for saving photon creation positions  
  TH3D* hXYZpositions;
//! histo for saving Time Trajectory
  TH1D* hTimeTraj;
//! histo for saving Time Emission
  TH1D* hTimeEmit;
//! histo for saving Time Propagation
  TH1D* hTimeProp;
//! histo for saving Time Transur
  TH1D* hTimeTrans;
  
//! histo for saving something for debugging
  TH1D* hTemp1;
  TH1D* hTemp2;
  
//! histo for saving Edep <-> Charge Channel distribution
  static TH2D* hEDepChargeChannel;

//! function, which describes distribution of TimeEmit  
  TF1* fTimeEmit;
//! function of single photon electron PMT output signal
  TF1* fPE;
  TF1* fTot;    
  //! function of total PMT output signal 
  TF1* fPMT;
  
  
  Photon* Photon101; //! photon collection of crystal 101
  Photon* Photon102; //! photon collection of crystal 102
  Photon* Photon103; //! photon collection of crystal 103
  Photon* Photon104; //! photon collection of crystal 104
  Photon* Photon111; //! photon collection of crystal 111
  Photon* Photon112; //! photon collection of crystal 112
  Photon* Photon113; //! photon collection of crystal 113
  Photon* Photon114; //! photon collection of crystal 114 
  Photon* Photon121; //! photon collection of crystal 121
  Photon* Photon122; //! photon collection of crystal 122
  Photon* Photon123; //! photon collection of crystal 123
  Photon* Photon124; //! photon collection of crystal 124

  Photon* Photon201; //! photon collection of crystal 201
  Photon* Photon202; //! photon collection of crystal 202
  Photon* Photon203; //! photon collection of crystal 203
  Photon* Photon204; //! photon collection of crystal 204 
  Photon* Photon211; //! photon collection of crystal 211
  Photon* Photon212; //! photon collection of crystal 212
  Photon* Photon213; //! photon collection of crystal 213
  Photon* Photon214; //! photon collection of crystal 214
  Photon* Photon221; //! photon collection of crystal 221
  Photon* Photon222; //! photon collection of crystal 222
  Photon* Photon223; //! photon collection of crystal 223
  Photon* Photon224; //! photon collection of crystal 224

  Photon* Photon_101; //! photon collection of crystal -101
  Photon* Photon_102; //! photon collection of crystal -102
  Photon* Photon_103; //! photon collection of crystal -103
  Photon* Photon_104; //! photon collection of crystal -104
  Photon* Photon_111; //! photon collection of crystal -111
  Photon* Photon_112; //! photon collection of crystal -112
  Photon* Photon_113; //! photon collection of crystal -113
  Photon* Photon_114; //! photon collection of crystal -114 
  Photon* Photon_121; //! photon collection of crystal -121
  Photon* Photon_122; //! photon collection of crystal -122
  Photon* Photon_123; //! photon collection of crystal -123
  Photon* Photon_124; //! photon collection of crystal -124

  Photon* Photon_201; //! photon collection of crystal -201
  Photon* Photon_202; //! photon collection of crystal -202
  Photon* Photon_203; //! photon collection of crystal -203
  Photon* Photon_204; //! photon collection of crystal -204 
  Photon* Photon_211; //! photon collection of crystal -211
  Photon* Photon_212; //! photon collection of crystal -212
  Photon* Photon_213; //! photon collection of crystal -213
  Photon* Photon_214; //! photon collection of crystal -214
  Photon* Photon_221; //! photon collection of crystal -221
  Photon* Photon_222; //! photon collection of crystal -222
  Photon* Photon_223; //! photon collection of crystal -223
  Photon* Photon_224; //! photon collection of crystal -224	

  G4int m_index[48]; //! index for number of photons in single crystals

  G4int m_destroyFlag[6]; //! how many photons were destroyed by what process -> see photon destroyFlags
  
  G4double totEDep101,totEDep102,totEDep103,totEDep104,totEDep111,totEDep112,totEDep113,totEDep114,totEDep121,totEDep122,totEDep123,totEDep124;
  G4double totEDep201,totEDep202,totEDep203,totEDep204,totEDep211,totEDep212,totEDep213,totEDep214,totEDep221,totEDep222,totEDep223,totEDep224;
  
  G4int wrongCalculation;

  static std::vector<double> fX;
  static std::vector<double> fY;

  static G4double integral; //! integral of single photoelectron signal [0,80] -> needs to be changed if gain and/or collection factor is changed

  static G4bool initFunction; 
};

#endif
