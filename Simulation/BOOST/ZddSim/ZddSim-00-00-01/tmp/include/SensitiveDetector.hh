
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef SensitiveDetector_h
#define SensitiveDetector_h 1

#include "G4VSensitiveDetector.hh"
//#include "PrimaryGeneratorAction.hh"
//#include "G4Event.hh"
//#include "DetectorConstruction.hh"
//#include "G4PrimaryParticle.hh"
#include <vector>
#include "BesZddHit.hh"
#include "G4UnitsTable.hh"

class G4Step;
//class G4Event;
class G4HCofThisEvent;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class SensitiveDetector : public G4VSensitiveDetector
{
  public:
      SensitiveDetector(G4String);
     ~SensitiveDetector();

      void Initialize(G4HCofThisEvent*);
      G4bool ProcessHits(G4Step*, G4TouchableHistory*);
      void EndOfEvent(G4HCofThisEvent*);
 //     void BeginOfTruthEvent(const G4Event*);
 //     void EndOfTruthEvent(const G4Event*);
      void IncreaseTotEnergyDeposit(G4double edep){ TotEnergyDeposit += edep;};
  
      
      G4double GetTotEnergyDeposit(){ return TotEnergyDeposit;}; 

  private:
      BesZddHitsCollection* BesZddHitCollection;
      BesZddHitsCollection* BesZddHitList;
      
      G4double TotEnergyDeposit;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

