#include <iostream>
#include "Photon.hh"
#include "TMath.h"

Photon::Photon()
{
//  std::cout << "Photon.cc : Hallo, ich bin ein Photon " << std::endl;
  m_n_scint = 2.2;
  m_chargeChannel = 0;
  m_timeChannel = 0;
/*
  m_destroyFlg0 = 0;
  m_destroyFlg1 = 0;
  m_destroyFlg2 = 0;
  m_destroyFlg3 = 0;
  m_destroyFlg4 = 0;
  m_destroyFlg5 = 0;
*/  
  for(int i = 0;i<3200; i++)
  {
    m_output[i][0] = 0.;
  }
}

Photon::Photon(double pos_x, double pos_y, double pos_z, double t_traj, double theta)
{
  m_entries = 1;
  m_n_scint = 2.2;
  m_x.push_back(pos_x);
  m_y.push_back(pos_y);
  m_z.push_back(pos_z);
  m_t_traj.push_back(t_traj);
  m_theta.push_back(theta); //! theta should be a uniformly distributed random variable between -pi/2 and pi/2
}

void Photon::AddPhoton(double pos_x, double pos_y, double pos_z, double t_traj, double theta)
{
//  std::cout << "Photon:: AddPhoton : CrystalNo: " << m_crystalNo << std::endl;
//  std::cout << "Photon:: AddPhoton 1: " << " x: " << pos_x << ", y: " << pos_y << ", z: " << pos_z << std::endl;
  m_entries++;
  m_x.push_back(pos_x);
  m_y.push_back(pos_y);
  m_z.push_back(pos_z);
  m_t_traj.push_back(t_traj);
  m_theta.push_back(theta);
}


Photon::~Photon()
{
//  std::cout << "Photon.cc : Tja, jetzt war ich mal ein Photon" << std::endl;
}

G4bool Photon::EraseVectorElement()
{
    m_theta.pop_back();    //! emission angle of photon
    m_x.pop_back();        //! xPosition of creation
    m_y.pop_back();        //! yPosition of creation
    m_z.pop_back();        //! zPosition of creation
    m_t_traj.pop_back();   //! first time: t_trajectory
    m_t_emit.pop_back();   //! emission time
    m_t_prop.pop_back();   //! propagation time
    m_t_trans.pop_back();  //! transit time in PMT
//    std::cout << "Photon::EraseVectorElement() : Photon killed !!! " << std::endl;
//    m_destroyFlag.pop_back(); //! =0 not destroyed
 
    if( m_theta.size() != m_x.size() || m_theta.size() != m_y.size() ||  m_theta.size() != m_z.size() ||  m_theta.size() != m_t_traj.size() ||  m_theta.size() != m_t_emit.size() ||  m_theta.size() != m_t_prop.size() ||  m_theta.size() != m_t_trans.size() ) return false;   
    else return true;
}

G4bool Photon::EraseVectorElement(int i)
{
  m_theta.erase(m_theta.begin()+i);    //! emission angle of photon
  m_x.erase(m_x.begin()+i);        //! xPosition of creation
  m_y.erase(m_y.begin()+i);        //! yPosition of creation
  m_z.erase(m_z.begin()+i);        //! zPosition of creation
  m_t_traj.erase(m_t_traj.begin()+i);   //! first time: t_trajectory
  m_t_emit.erase(m_t_emit.begin()+i);   //! emission time
  m_t_prop.erase(m_t_prop.begin()+i);   //! propagation time
  m_t_trans.erase(m_t_trans.begin()+i);  //! transit time in PMT
 
  if( m_theta.size() != m_x.size() || m_theta.size() != m_y.size() ||  m_theta.size() != m_z.size() ||  m_theta.size() != m_t_traj.size() ||  m_theta.size() != m_t_emit.size() ||  m_theta.size() != m_t_prop.size() ||  m_theta.size() != m_t_trans.size() ) return false;   
  else return true;
}

/*
void Photon::SetDestroyFlag(int i, int destroyFlag)
{     
  m_destroyFlag.at(i) = destroyFlag;
  switch(destroyFlag)
  {
    case 0:
      m_destroyFlg0++;
      break;
    case 1:
      m_destroyFlg1++;
      break;
    case 2:
      m_destroyFlg0 += -1;
      m_destroyFlg2++;
      break;      
    case 3:
      m_destroyFlg0 += -1;
      m_destroyFlg3++;
      break;  
    case 4:
      m_destroyFlg0 += -1;
      m_destroyFlg4++;
      break;  
    case 5:
      m_destroyFlg0 += -1;
      m_destroyFlg5++;
      break;
  }
}

G4int Photon::GetDestroyFlgs(int i)
{
  switch(i)
  {
    case 0:
      return m_destroyFlg0;
    case 1:
      return m_destroyFlg1;  
    case 2:
      return m_destroyFlg2;
    case 3:
      return m_destroyFlg3;  
    case 4:
      return m_destroyFlg4;  
    case 5:
      return m_destroyFlg5;  
  }
}
*/