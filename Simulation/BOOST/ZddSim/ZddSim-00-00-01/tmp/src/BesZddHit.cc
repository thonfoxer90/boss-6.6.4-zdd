
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "BesZddHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "TMath.h"

G4Allocator<BesZddHit> BesZddHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BesZddHit::BesZddHit()
{
//  std::cout << "pi: " << TMath::Pi() << std::endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BesZddHit::BesZddHit(int trID, int preCrystalNo, int postCrystalNo, double eDep, double preTime,double postTime, G4ThreeVector preXYZ, G4ThreeVector postXYZ)
{
    m_TrackID 	 = trID;
    m_preCrystalNo  = preCrystalNo;
    m_postCrystalNo = postCrystalNo;
    m_EDep 	 = eDep;
    m_preTime 	 = preTime;
    m_postTime   = postTime;
    m_prePos     = preXYZ;
    m_postPos    = postXYZ;
//    m_Volume 	 = volume;
}


BesZddHit::~BesZddHit() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BesZddHit::BesZddHit(const BesZddHit& right)
  : G4VHit()
{
  m_TrackID   = right.m_TrackID;
  m_preCrystalNo = right.m_preCrystalNo;
  m_postCrystalNo = right.m_postCrystalNo;
  m_PDGCode   = right.m_PDGCode;
  m_EDep      = right.m_EDep;
  m_Momentum  = right.m_Momentum;
  m_preTime     = right.m_preTime;
  m_postTime    = right.m_postTime;
  m_prePos      = right.m_prePos;
  m_postPos	= right.m_postPos;
//  m_PosLocal  = right.m_PosLocal;
  m_Volume    = right.m_Volume;
//  m_VolumeName= right.m_VolumeName;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const BesZddHit& BesZddHit::operator=(const BesZddHit& right)
{
  m_TrackID   = right.m_TrackID;
 // m_TrackIndex= right.m_TrackIndex;
 // m_Part      = right.m_Part;	
  m_preCrystalNo = right.m_preCrystalNo;
  m_postCrystalNo = right.m_postCrystalNo;
  m_PDGCode   = right.m_PDGCode;
  m_EDep      = right.m_EDep;
  m_Momentum  = right.m_Momentum;
  m_preTime    = right.m_preTime;
  m_postTime   = right.m_postTime;
  m_prePos     = right.m_prePos;
  m_postPos    = right.m_postPos; 
  m_Volume    = right.m_Volume;
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int BesZddHit::operator==(const BesZddHit& right) const
{
  return (this==&right) ? 1 : 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//void BesZddHit::SetVolume(G4VPhysicalVolume* pv)
//{
//  m_Volume = pv;
//  m_VolumeName = pv->GetLogicalVolume()->GetName();
//}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BesZddHit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
  {
    G4Circle circle(m_postPos);
    circle.SetScreenSize(2.);
    circle.SetFillStyle(G4Circle::filled);
    G4Colour colour(1.,0.,0.);
    G4VisAttributes attribs(colour);
    circle.SetVisAttributes(attribs);
    pVVisManager->Draw(circle);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BesZddHit::Print()
{
  G4cout << "  trackID: " << m_TrackID << /*"  crystal: " << m_Crystal
         << */"  energy deposit: " << G4BestUnit(m_EDep,"Energy")
         << "  momentum: " << m_Momentum/MeV
	 << "  position: " << G4BestUnit(m_postPos,"Length") << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

