
//$Id: BesZddDigitizer.cc

#include "BesZddDigitizer.hh"
#include "BesZddDigit.hh"
#include "G4DigiManager.hh"
#include "Randomize.hh"
#include "G4UnitsTable.hh"
#include <CLHEP/Random/Randomize.h>

BesZddDigitizer::BesZddDigitizer(G4String modName)
:G4VDigitizerModule(modName)
{
  G4cout << "Digitalizaton initalization" << G4endl;
  collectionName.push_back("BesZddDigisCollection");

  m_besZddDigisCollection=0;

  //Standard unit: length in mm; time in ns; energy in MeV; att in mm;
  //Here: att in mm; vel in m/s; threshold in MeV; mucRes in ps; 

  Initialize();  
}

BesZddDigitizer::~BesZddDigitizer()
{
}

void BesZddDigitizer::Initialize()
{
}


void BesZddDigitizer::Digitize()
{
  Initialize();
  
  m_besZddDigisCollection = new BesZddDigisCollection(moduleName, collectionName[0]);
  G4DigiManager* DigiMan = G4DigiManager::GetDMpointer();
  
//  G4cout << "Digitization routine 1" << G4endl;
  //hits collection ID
  G4int THCID;
  THCID = DigiMan->GetHitsCollectionID("BesZddHitCollection");

//  G4cout << "Digitization routine 2" << G4endl;
  
  //hits collection
  BesZddHitsCollection* THC = 0;
  THC = (BesZddHitsCollection*)(DigiMan->GetHitsCollection(THCID));
 // G4cout << "Digitization routine 3" << G4endl;
  if(THC) {
//  G4cout << "Digitization routine 4" << G4endl;
    
    BesZddDigit *aDigit = new BesZddDigit();

    G4int n_hit = THC->entries();
    for(G4int i = 0; i < n_hit; i++) 
    {
      BesZddHit* hit = (*THC)[i];
      aDigit->SetHit( hit );
    }

    aDigit->EndOfEvent();
//        std::cout << "BesZddDigitizer.cc : Digitize : test" << std::endl;
  
    
  //! adc channel threshold per crystal for digitization
  G4int threshold = 0;
    /*     
      //fill zdd Ntuple
      if(m_G4Svc->ZddRootFlag())
	{
	  m_PartID = partID;
	  m_DetectorID = detectorID;
	  m_CrystalNo = crystalNo;
	  m_ChargeChannel = chargeChannel;
	  m_TimeChannel = timeChannel;
	  m_tupleZdd->write();
	}
    */
    //G4cout << " if(THC) 1 " << G4endl;
/*
    // left Zdd (partID = 1) detectorID = 1  (upper crystals)
    if( aDigit->GetChargeChannel_101() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 1 );
      digi->SetDetectorID( 1 );
      digi->SetCrystalNo( 101 );
      G4double chargeChannel = aDigit->GetChargeChannel_101() ;
      G4double timeChannel = aDigit->GetTimeChannel_101() ;

      digi->SetChargeChannel( chargeChannel );
      digi->SetTimeChannel( timeChannel ) ;
      //Smear();  
      m_besZddDigisCollection->insert(digi);

     }

    if( aDigit->GetChargeChannel_102() >= threshold) // if chargeChannel above channel threshold  
    {
      //      G4cout << "EDep crystal -102: " << aDigit->GetChargeChannel_102() << G4endl;
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 102 );
	G4double chargeChannel = aDigit->GetChargeChannel_102() ;
	G4double timeChannel = aDigit->GetTimeChannel_102() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( aDigit->GetChargeChannel_103() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 103 );
	G4double chargeChannel = aDigit->GetChargeChannel_103() ;
	G4double timeChannel = aDigit->GetTimeChannel_103() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_104() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 104 );
	G4double chargeChannel = aDigit->GetChargeChannel_104() ;
	G4double timeChannel = aDigit->GetTimeChannel_104() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_111() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID ( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 111 );
	G4double chargeChannel = aDigit->GetChargeChannel_111() ;
	G4double timeChannel = aDigit->GetTimeChannel_111() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel_112() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 112 );
        G4double chargeChannel = aDigit->GetChargeChannel_112() ;
        G4double timeChannel = aDigit->GetTimeChannel_112() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_113() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 113 );
        G4double chargeChannel = aDigit->GetChargeChannel_113() ;
        G4double timeChannel = aDigit->GetTimeChannel_113() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_114() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 114 );
        G4double chargeChannel = aDigit->GetChargeChannel_114() ;
        G4double timeChannel = aDigit->GetTimeChannel_114() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_121() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 121 );
	G4double chargeChannel = aDigit->GetChargeChannel_121() ;
	G4double timeChannel = aDigit->GetTimeChannel_121() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel_122() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 122 );
        G4double chargeChannel = aDigit->GetChargeChannel_122() ;
        G4double timeChannel = aDigit->GetTimeChannel_122() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_123() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 123 );
        G4double chargeChannel = aDigit->GetChargeChannel_123() ;
        G4double timeChannel = aDigit->GetTimeChannel_123() ;
 
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_124() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 124 );
        G4double chargeChannel = aDigit->GetChargeChannel_124() ;
        G4double timeChannel = aDigit->GetTimeChannel_124() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    // detectorID = 2 (lower crystals)
    if( aDigit->GetChargeChannel_201() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 1 );
      digi->SetDetectorID( 2 );
      digi->SetCrystalNo( 201 );
      G4double chargeChannel = aDigit->GetChargeChannel_201() ;
      G4double timeChannel = aDigit->GetTimeChannel_201() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
      //Smear();  
      m_besZddDigisCollection->insert(digi);
    }

    if( aDigit->GetChargeChannel_202() >= threshold) // if chargeChannel above channel threshold   
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 202 );
	G4double chargeChannel = aDigit->GetChargeChannel_202() ;
	G4double timeChannel = aDigit->GetTimeChannel_202() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( aDigit->GetChargeChannel_203() >= threshold) // if chargeChannel above channel threshold 
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 203 );
	G4double chargeChannel = aDigit->GetChargeChannel_203() ;
	G4double timeChannel = aDigit->GetTimeChannel_203() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_204() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 204 );
	G4double chargeChannel = aDigit->GetChargeChannel_204() ;
	G4double timeChannel = aDigit->GetTimeChannel_204() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_211() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 211 );
	G4double chargeChannel = aDigit->GetChargeChannel_211() ;
	G4double timeChannel = aDigit->GetTimeChannel_211() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel_212() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 212 );
        G4double chargeChannel = aDigit->GetChargeChannel_212() ;
        G4double timeChannel = aDigit->GetTimeChannel_212() ;
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_213() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 213 );
        G4double chargeChannel = aDigit->GetChargeChannel_213() ;
        G4double timeChannel = aDigit->GetTimeChannel_213() ;
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_214() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 214 );
        G4double chargeChannel = aDigit->GetChargeChannel_214() ;
        G4double timeChannel = aDigit->GetTimeChannel_214() ;
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel_221() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 1 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 221 );
	G4double chargeChannel = aDigit->GetChargeChannel_221() ;
	G4double timeChannel = aDigit->GetTimeChannel_221() ;
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel101() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 2 );
      digi->SetDetectorID( 1 );
      digi->SetCrystalNo( 101 );
      G4double chargeChannel = aDigit->GetChargeChannel101() ;
      G4double timeChannel = aDigit->GetTimeChannel101() ;

      digi->SetChargeChannel( chargeChannel );
      digi->SetTimeChannel( timeChannel ) ;
      //Smear();  
      m_besZddDigisCollection->insert(digi);

     }

    if( aDigit->GetChargeChannel102() >= threshold) // if chargeChannel above channel threshold  
    {
      //      G4cout << "EDep crystal -102: " << aDigit->GetChargeChannel102() << G4endl;
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 102 );
	G4double chargeChannel = aDigit->GetChargeChannel102() ;
	G4double timeChannel = aDigit->GetTimeChannel102() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( aDigit->GetChargeChannel103() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 103 );
	G4double chargeChannel = aDigit->GetChargeChannel103() ;
	G4double timeChannel = aDigit->GetTimeChannel103() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel104() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 104 );
	G4double chargeChannel = aDigit->GetChargeChannel104() ;
	G4double timeChannel = aDigit->GetTimeChannel104() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel111() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID ( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 111 );
	G4double chargeChannel = aDigit->GetChargeChannel111() ;
	G4double timeChannel = aDigit->GetTimeChannel111() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel112() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 112 );
        G4double chargeChannel = aDigit->GetChargeChannel112() ;
        G4double timeChannel = aDigit->GetTimeChannel112() ;

//	std::cout << "BesZddDigitizer.cc :: Digitize () : crystal 112: ChargeChannel: " << chargeChannel << std::endl;
//	std::cout << "BesZddDigitizer.cc :: Digitize () : crystal 112: timeChannel: " << timeChannel << std::endl << "=================================================================" << std::endl;
	
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel113() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 113 );
        G4double chargeChannel = aDigit->GetChargeChannel113() ;
        G4double timeChannel = aDigit->GetTimeChannel113() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel114() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 114 );
        G4double chargeChannel = aDigit->GetChargeChannel114() ;
        G4double timeChannel = aDigit->GetTimeChannel114() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel121() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
	digi->SetCrystalNo( 121 );
	G4double chargeChannel = aDigit->GetChargeChannel121() ;
	G4double timeChannel = aDigit->GetTimeChannel121() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel122() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 122 );
        G4double chargeChannel = aDigit->GetChargeChannel122() ;
        G4double timeChannel = aDigit->GetTimeChannel122() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel123() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 123 );
        G4double chargeChannel = aDigit->GetChargeChannel123() ;
        G4double timeChannel = aDigit->GetTimeChannel123() ;
 
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel124() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 1 );
        digi->SetCrystalNo( 124 );
        G4double chargeChannel = aDigit->GetChargeChannel124() ;
        G4double timeChannel = aDigit->GetTimeChannel124() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    // detectorID = 2 (lower crystals)
    if( aDigit->GetChargeChannel201() >= threshold) // if chargeChannel above channel threshold
    {  
      BesZddDigi *digi = new BesZddDigi();
      digi->SetPartID( 2 );
      digi->SetDetectorID( 2 );
      digi->SetCrystalNo( 201 );
      G4double chargeChannel = aDigit->GetChargeChannel201() ;
      G4double timeChannel = aDigit->GetTimeChannel201() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
      //Smear();  
      m_besZddDigisCollection->insert(digi);
    }

    if( aDigit->GetChargeChannel202() >= threshold) // if chargeChannel above channel threshold   
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 202 );
	G4double chargeChannel = aDigit->GetChargeChannel202() ;
	G4double timeChannel = aDigit->GetTimeChannel202() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);    
     }
    if( aDigit->GetChargeChannel203() >= threshold) // if chargeChannel above channel threshold 
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 203 );
	G4double chargeChannel = aDigit->GetChargeChannel203() ;
	G4double timeChannel = aDigit->GetTimeChannel203() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel204() >= threshold) // if chargeChannel above channel threshold  
    {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 204 );
	G4double chargeChannel = aDigit->GetChargeChannel204() ;
	G4double timeChannel = aDigit->GetTimeChannel204() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
    	m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel211() >= threshold) // if chargeChannel above channel threshold  
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 211 );
	G4double chargeChannel = aDigit->GetChargeChannel211() ;
	G4double timeChannel = aDigit->GetTimeChannel211() ;

	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
	m_besZddDigisCollection->insert(digi);
      }

    if( aDigit->GetChargeChannel212() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 212 );
        G4double chargeChannel = aDigit->GetChargeChannel212() ;
        G4double timeChannel = aDigit->GetTimeChannel212() ;
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel213() >= threshold) // if chargeChannel above channel threshold  
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 213 );
        G4double chargeChannel = aDigit->GetChargeChannel213() ;
        G4double timeChannel = aDigit->GetTimeChannel213() ;
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
	//Smear();
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel214() >= threshold) // if chargeChannel above channel threshold   
      {
        BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
        digi->SetCrystalNo( 214 );
        G4double chargeChannel = aDigit->GetChargeChannel214() ;
        G4double timeChannel = aDigit->GetTimeChannel214() ;
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ;
        m_besZddDigisCollection->insert(digi);
      }
    if( aDigit->GetChargeChannel221() >= threshold) // if chargeChannel above channel threshold   
      {
	BesZddDigi *digi = new BesZddDigi();
	digi->SetPartID( 2 );
	digi->SetDetectorID( 2 );
	digi->SetCrystalNo( 221 );
	G4double chargeChannel = aDigit->GetChargeChannel221() ;
	G4double timeChannel = aDigit->GetTimeChannel221() ;
	digi->SetChargeChannel( chargeChannel );
        digi->SetTimeChannel( timeChannel ) ; 
	m_besZddDigisCollection->insert(digi);
      }
 //     std::cout << "BesZddDigitizer.cc : Digitize : test2" << std::endl;

      StoreDigiCollection(m_besZddDigisCollection);
*/
      delete aDigit;
      
    } //if(THC) 

//    G4int DigiCollID = 23;
//    StoreDigiCollection(DigiCollID, m_ZddDigisCollection);	

}// end Digitize


G4double BesZddDigitizer::Smear(G4double var)
{ 
//   G4cout << "eDep before smearing: " << G4BestUnit(edep,"Energy") << G4endl;
//   G4cout << "time before smearing: " << G4BestUnit(time,"Time") << G4endl;
  
 G4double gaussrand = G4RandGauss::shoot(0.,0.5);
 return var+(gaussrand*var);
  
//   G4cout << "eDep after smearing:  " << G4BestUnit(edep,"Energy") << G4endl;
//   G4cout << "time after smearing:  " << G4BestUnit(time,"Time") << G4endl;
//   
//   G4cout << "gaussrand1:           " << gaussrand1 << G4endl;
//   G4cout << "gaussrand2:           " << gaussrand2 << G4endl << G4endl;
}
