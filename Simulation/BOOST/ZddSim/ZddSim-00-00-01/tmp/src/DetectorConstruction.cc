//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <cstdlib>
#include <fstream>

#include "DetectorConstruction.hh"
#include "SensitiveDetector.hh"
#include "BesZddDigitizer.hh"
//#include "DetectorMessenger.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4SDManager.hh"
#include "G4DigiManager.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"


G4double const DetectorConstruction::sCrystalBoxSizeX = 4.*cm; 
G4double const DetectorConstruction::sCrystalBoxSizeY = 3.*cm; 
G4double const DetectorConstruction::sCrystalBoxSizeZ = 14.*cm; //14
    
G4double const DetectorConstruction::sCrystalX = 1.*cm;
G4double const DetectorConstruction::sCrystalY = 1.*cm;
G4double const DetectorConstruction::sCrystalZ = 14.*cm; //14

G4ThreeVector const DetectorConstruction::sUpperCrystalBox = G4ThreeVector(1.25*cm,2.*cm,10.*cm);
G4ThreeVector const DetectorConstruction::sLowerCrystalBox = G4ThreeVector(1.25*cm,-2.*cm,10.*cm); 


    std::string make_ObjectName( const std::string& basename, G4int i, G4int k, const std::string& nameaende)
    {
    std::ostringstream result;
    result << basename << i << k << nameaende;

    return result.str();
    }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
:
 solidWorld(0),logicWorld(0),physiWorld(0)
{

  ComputeWorldSize();
   
  // materials
  DefineMaterials();
  SetCrystalMaterial("PbWO4");
  // create commands for interactive definition of the calorimeter
  //detectorMessenger = new DetectorMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ 
//  delete detectorMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  return ConstructCrystals();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::DefineMaterials()
{ 
 //This function illustrates the possible ways to define materials
 
  //defining Vacuum properties
  G4double atomicNumber = 1.;
  G4double massOfMole = 1.008*g/mole;
  G4double density = 1.e-25*g/cm3;
  G4double temperature = 2.73*kelvin;
  G4double pressure = 3.e-18*pascal;
  
  G4Material* Vacuum = new G4Material("Vacuum", atomicNumber, massOfMole, density, kStateGas, temperature, pressure);

  // defining PbWO4
  G4double a;  // atomic mass
  G4double z;  // atomic number
  G4String name, symbol;
  G4int ncomponents, natoms;
  
  G4Element* elPb = 
    new G4Element(name="Lead", symbol="Pb" ,z=82., a=207.19*g/mole);

  G4Element* elW = 
    new G4Element(name="Tungsten", symbol="W", z=74., a=183.84*g/mole);

  G4Element* elO = 
    new G4Element(name="Oxygen", symbol="O", z=8., a=15.9994*g/mole);

  density = 8.280*g/cm3;
  G4Material* PbWO4 = new G4Material("PbWO4", density,ncomponents=3);
  
  PbWO4->AddElement(elPb, natoms=1);
  PbWO4->AddElement(elW, natoms=1);
  PbWO4->AddElement(elO, natoms=4);
  
  defaultMaterial = Vacuum;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::ConstructCrystals()
{
  // Clean old geometry, if any
  //
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();

  // complete the Calor parameters definition
  ComputeWorldSize();
   
  //     
  // World
  //
  solidWorld = new G4Box("World",				//its name
                   WorldSizeX/2.,WorldSizeY/2.,WorldSizeZ/2.);	//its size
                         
  logicWorld = new G4LogicalVolume(solidWorld,		//its solid
                                   defaultMaterial,	//its material
                                   "World");		//its name
                                   
  physiWorld = new G4PVPlacement(0,			//no rotation
  				 G4ThreeVector(),	//at (0,0,0)
                                 logicWorld,		//its logical volume				 
                                 "World",		//its name
                                 0,			//its mother  volume
                                 false,			//no boolean operation
                                 0);			//copy number
  
  // default parameter values of the crystal box volume
  G4double CrystalBoxSizeX = sCrystalBoxSizeX;
  G4double CrystalBoxSizeY = sCrystalBoxSizeY;
  G4double CrystalBoxSizeZ = sCrystalBoxSizeZ;

  
   // dimensions of a single crystal
  G4double crystalX = sCrystalZ; //cm
  G4double crystalY = sCrystalY; //cm 
  G4double crystalZ = sCrystalZ; //cm   
   
   
G4Colour Yellow(1.,1.,0);

G4Box* crystalBox = new G4Box("crystalBox", sCrystalBoxSizeX/2.,sCrystalBoxSizeY/2.,sCrystalBoxSizeZ/2.);


G4LogicalVolume* crystalBox1_log = new G4LogicalVolume(crystalBox, 					//shape and dimension 
						      defaultMaterial, 						//material
						      "crystalBox1_log", 				//name
						      0, 						//User Limits
						      0, 						//User Limits
						      0);						//User Limits
crystalBox1_log->SetVisAttributes (G4VisAttributes::Invisible);

G4LogicalVolume* crystalBox2_log = new G4LogicalVolume(crystalBox, 					//shape and dimension 
						      defaultMaterial, 						//material
						      "crystalBox2_log", 				//name
						      0, 						//User Limits
						      0, 						//User Limits
						      0);						//User Limits      
crystalBox2_log->SetVisAttributes (G4VisAttributes::Invisible);


G4Box* crystal = new G4Box("crystal",sCrystalX/2.,sCrystalY/2.,sCrystalZ/2.);

crystal_log = new G4LogicalVolume(crystal,       					// shape and dimension
						   CrystalMaterial,         					// material
						   "crystal_log",					// name
						   0,							// User Limits...
						   0,
						   0);
						   
crystal_log->SetVisAttributes(new G4VisAttributes(Yellow));		   
//crystal_log->SetVisAttributes (G4VisAttributes::Invisible);

//  * * * * * * * * * * * positioning optimization for crystals * * * * * * * * * * * * * *  * 

G4double crystalCrossArea = sCrystalX * sCrystalY;
const G4float availableArea = 13.5; // in cm2
G4int nOfCrystals = (G4int)availableArea/crystalCrossArea;
const G4int nOfRows = (G4int) sCrystalBoxSizeX/sCrystalX;
const G4int nOfLines = (G4int) sCrystalBoxSizeY/sCrystalY;

G4double crystalPosX[nOfRows];
G4double crystalPosY[nOfLines];

SetNOfCrystals(nOfRows*nOfLines*2);

for(G4int i = 0; i < nOfRows; i++)  crystalPosX[i] = -sCrystalBoxSizeX/2. + (1./2. + i) * sCrystalX;
for(G4int k = 0; k < nOfLines; k++) crystalPosY[k] = -sCrystalBoxSizeY/2. + (1./2. + k) * sCrystalY;


std::vector<G4PVPlacement*> upperPlacements;
std::vector<G4PVPlacement*> lowerPlacements;

G4int test = 0;
for(G4int k = 0; k < nOfLines; k++)
{
    for(G4int i = 0; i < nOfRows; i++)
    {
      
 	upperPlacements.push_back(      new G4PVPlacement(  0,                                      			//rotation
							    G4ThreeVector(crystalPosX[i],crystalPosY[k],0.),   		//translation position
							    crystal_log,                                		//its logical volume
							    (G4String)make_ObjectName("crystal_1",k,i+1,"_phys"),       //its name,
							    crystalBox1_log,                     			//its mother volume (=0 for world volume)
							    false,                                  			//boolean operations? 
							    100+10*k+i+1)	);					//copyNo
	
//	G4cout << G4endl << "CopyNumber of Crystal: " << upperPlacements[test]->GetCopyNo() << G4endl;
	test++;
    }	
}
test = 0;
for(G4int k = 0; k < nOfLines; k++)
{
    for(G4int i = 0; i < nOfRows; i++)
    {
 	lowerPlacements.push_back(      new G4PVPlacement(  0,                                      				//rotation
							    G4ThreeVector(crystalPosX[i],crystalPosY[k],0.),   			//translation position
							    crystal_log,                                			//its logical volume
							    (G4String)make_ObjectName("crystal_2",nOfLines-1-k,i+1,"_phys"),  	//its name
							    crystalBox2_log,                     				//its mother volume (=0 for world volume)
							    false,                                  				//boolean operations? 
							    200+10*(nOfLines-1-k)+i+1)	);  					//copyNo
	
//	G4cout << G4endl << "CopyNumber of Crystal: " << lowerPlacements[test]->GetCopyNo() << G4endl;
	test++;
    }
}

G4ThreeVector upperCrystalBox = G4ThreeVector(1.25*cm,2.*cm,0.*cm);;
G4ThreeVector lowerCrystalBox = G4ThreeVector(1.25*cm,-2.*cm,0.*cm);;


G4PVPlacement* crystalBox1_phys = new G4PVPlacement(0,                                                  //rotation
						   sUpperCrystalBox,   		//translation position
						   crystalBox1_log,                                      //its logical volume
						   "crystalBox1_phys",                                  //its name
						   logicWorld,                                   	//its mother volume (=0 for world volume)
						   false,                                               //boolean operations? 
						   0);       
 
G4PVPlacement* crystalBox2_phys = new G4PVPlacement(0,                                                  //rotation
						   sLowerCrystalBox,   		//translation position
						   crystalBox2_log,                                      //its logical volume
						   "crystalBox2_phys",                                  //its name
						   logicWorld,                                   	//its mother volume (=0 for world volume)
						   false,                                               //boolean operations? 
						   0);  						   
						   
						       
  
  //                                        
  // Visualization attributes
  //
logicWorld->SetVisAttributes (G4VisAttributes::Invisible);

// ---------------------------------------------------------------------------------------------------------
// ==================================== sensitive detectors ================================================
// ---------------------------------------------------------------------------------------------------------

G4SDManager* SDman = G4SDManager::GetSDMpointer();

G4String crystalSDname = "Zdd/crystalSD";
SD = new SensitiveDetector( crystalSDname );
SDman->AddNewDetector( SD );
crystal_log->SetSensitiveDetector( SD );

G4DigiManager* DigiMan = G4DigiManager::GetDMpointer();
G4String DigiName = "Zdd/Digitizer";
BesZddDigitizer* myDigitizer = new BesZddDigitizer(DigiName);
DigiMan->AddNewModule(myDigitizer);

 /*
  // Below are vis attributes that permits someone to test / play 
  // with the interactive expansion / contraction geometry system of the
  // vis/OpenInventor driver :
 {G4VisAttributes* simpleBoxVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,0.0));
  simpleBoxVisAtt->SetVisibility(true);
  delete logicCalor->GetVisAttributes();
  logicCalor->SetVisAttributes(simpleBoxVisAtt);}

 {G4VisAttributes* atb= new G4VisAttributes(G4Colour(1.0,0.0,0.0));
  logicLayer->SetVisAttributes(atb);}
  
 {G4VisAttributes* atb= new G4VisAttributes(G4Colour(0.0,1.0,0.0));
  atb->SetForceSolid(true);
  logicAbsorber->SetVisAttributes(atb);}
  
 {//Set opacity = 0.2 then transparency = 1 - 0.2 = 0.8
  G4VisAttributes* atb= new G4VisAttributes(G4Colour(0.0,0.0,1.0,0.2));
  atb->SetForceSolid(true);
  logicGap->SetVisAttributes(atb);}
  */

  //
  //always return the physical World
  //
  return physiWorld;
}


void DetectorConstruction::SetCrystalMaterial(G4String materialChoice)
{
  // search the material by its name   
  G4Material* cryMat = G4Material::GetMaterial(materialChoice);     
  if (cryMat) CrystalMaterial = cryMat;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"

void DetectorConstruction::SetMagField(G4double fieldValue)
{
  //apply a global uniform magne     G4double 		crystalSize_x;tic field along Z axis
  G4FieldManager* fieldMgr
   = G4TransportationManager::GetTransportationManager()->GetFieldManager();

  if(magField) delete magField;		//delete the existing magn field

  if(fieldValue!=0.)			// create a new one if non nul
  { magField = new G4UniformMagField(G4ThreeVector(0.,0.,fieldValue));
    fieldMgr->SetDetectorField(magField);
    fieldMgr->CreateChordFinder(magField);
  } else {
    magField = 0;
    fieldMgr->SetDetectorField(magField);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "G4RunManager.hh"

void DetectorConstruction::UpdateGeometry()
{
  G4RunManager::GetRunManager()->DefineWorldVolume(ConstructCrystals());
}


G4int DetectorConstruction::CheckPosition(G4double x, G4double y, G4double z)
{
  //! center coordinates of upperCrystalBox;
  G4double upper_x = sUpperCrystalBox.x()/cm;
  G4double upper_y = sUpperCrystalBox.y()/cm;
  G4double upper_z = sUpperCrystalBox.z()/cm;
  
  //! center coordianates of lowerCrystalBox;
  G4double lower_x = sLowerCrystalBox.x()/cm;
  G4double lower_y = sLowerCrystalBox.y()/cm;
  G4double lower_z = sLowerCrystalBox.z()/cm;

if( z >= upper_z - sCrystalBoxSizeZ/cm/2. && z <= upper_z - sCrystalBoxSizeZ/cm/2. +  sCrystalZ/cm && y >= 0. )
{  
  //! sCrystals 101,111,121
  if( x >= upper_x - sCrystalBoxSizeX/cm/2. && x < upper_x - sCrystalBoxSizeX/cm/2. +  1.*sCrystalX/cm)
  {
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. && y < upper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 101;
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < upper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 111;
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= upper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 121;
    else return -1;
  } 
  //! sCrystals 102,112,122
  if( x >= upper_x - sCrystalBoxSizeX/cm/2. +1.*sCrystalX/cm && x < upper_x - sCrystalBoxSizeX/cm/2. +  2.*sCrystalX/cm)
  {
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. && y < upper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 102;
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < upper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 112;
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= upper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 122;
    else return -2;
  }
  //! sCrystals 103,113,123
  if( x >= upper_x - sCrystalBoxSizeX/cm/2. +2.*sCrystalX/cm && x < upper_x - sCrystalBoxSizeX/cm/2. +  3.*sCrystalX/cm)
  {
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. && y < upper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 103;
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < upper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 113;
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= upper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 123;
    else return -3;
  }
  //! sCrystals 104,114,124
  if( x >= upper_x - sCrystalBoxSizeX/cm/2. +3.*sCrystalX/cm && x <= upper_x - sCrystalBoxSizeX/cm/2. +  4.*sCrystalX/cm)
  {
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. && y < upper_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 104;
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < upper_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 114;
    if( y >= upper_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= upper_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 124;
    else return -4;
  }
}

else if( z >= lower_z - sCrystalBoxSizeZ/cm/2. && z <= lower_z - sCrystalBoxSizeZ/cm/2. +  sCrystalZ/cm && y < 0.)
{
  //! sCrystals 201,211,221
  if( x >= lower_x - sCrystalBoxSizeX/cm/2. && x < lower_x - sCrystalBoxSizeX/cm/2. +  1.*sCrystalX/cm)
  {
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. && y < lower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 221;
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 211;
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 201;
    else return -10;
  } 
  //! sCrystals 202,212,222
  if( x >= lower_x - sCrystalBoxSizeX/cm/2. +1.*sCrystalX/cm && x < lower_x - sCrystalBoxSizeX/cm/2. +  2.*sCrystalX/cm)
  {
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. && y < lower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 222;
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm) return 212;
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 202;
    else return -20;
  }
  //! sCrystals 203,213,223
  if( x >= lower_x - sCrystalBoxSizeX/cm/2. +2.*sCrystalX/cm && x < lower_x - sCrystalBoxSizeX/cm/2. +  3.*sCrystalX/cm)
  {
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. && y < lower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 223;
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 213;
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 203;
    else return -30;
  }
  //! sCrystals 204,214,224
  if( x >= lower_x - sCrystalBoxSizeX/cm/2. +3.*sCrystalX/cm && x <= lower_x - sCrystalBoxSizeX/cm/2. +  4.*sCrystalX/cm)
  {
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. && y < lower_y - sCrystalBoxSizeY/cm/2. +  1.*sCrystalY/cm ) return 224;
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. + 1.*sCrystalY/cm && y < lower_y - sCrystalBoxSizeY/cm/2. +  2.*sCrystalY/cm ) return 214;
    if( y >= lower_y - sCrystalBoxSizeY/cm/2. + 2.*sCrystalY/cm && y <= lower_y - sCrystalBoxSizeY/cm/2. +  3.*sCrystalY/cm ) return 204;
    else return -40;
  }
}

else return -50;
}

G4double DetectorConstruction::GetDistanceToPMT( G4double z )
{
  if(z > 0)     return sUpperCrystalBox.z()/cm - z + sCrystalZ/cm/2. ;
  else		return fabs(sUpperCrystalBox.z()/cm - z - sCrystalZ/cm/2.);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
