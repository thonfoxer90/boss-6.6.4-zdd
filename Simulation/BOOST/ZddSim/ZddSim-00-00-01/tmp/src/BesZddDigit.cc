
//
// $Id: BesZddDigit

#include "BesZddDigit.hh"
#include "DetectorConstruction.hh"
#include "G4ios.hh"
#include "G4Box.hh"
#include "G4VSolid.hh"
#include "strstream"
#include "G4ThreeVector.hh"
#include "G4UnitsTable.hh"
#include "TFile.h"
#include "TH2D.h"

#include "Randomize.hh"
#include <CLHEP/Random/Randomize.h>

//! Write out histos?
//#define HISTOS

//! initialize static variables/objects



G4double BesZddDigit::thetaLoss = 0.47201/1.570797; 
G4double BesZddDigit::mismatchLoss = 0.8;
G4double BesZddDigit::angularAcceptanceLoss = 0.66;
G4double BesZddDigit::QELoss = 0.21;

G4int BesZddDigit::photonConstant = int(1350. * BesZddDigit::mismatchLoss * BesZddDigit::angularAcceptanceLoss * BesZddDigit::QELoss * BesZddDigit::thetaLoss);


G4double BesZddDigit::photonsPerMeV = 0;
G4int BesZddDigit::eventCounter = 0;

TH2D* BesZddDigit::hEDepChargeChannel = new TH2D("hEDepChargeChannel","",1400,0.,700.,4000,0,20000);
//! random generators
/*
G4int const BesZddDigit::m_seed1 = 12345; 
G4int const BesZddDigit::m_seed2 = 23456;
G4int const BesZddDigit::m_seed3 = 34567;
G4int const BesZddDigit::m_seed4 = 45678;
G4int const BesZddDigit::m_seed5 = 56789;

TRandom* BesZddDigit::uniRand1 = new TRandom(BesZddDigit::m_seed1);
TRandom* BesZddDigit::uniRand2 = new TRandom(BesZddDigit::m_seed2);
TRandom* BesZddDigit::uniRand3 = new TRandom(BesZddDigit::m_seed3);
TRandom* BesZddDigit::uniRand4 = new TRandom(BesZddDigit::m_seed4);
TRandom* BesZddDigit::uniRand5 = new TRandom(BesZddDigit::m_seed5);
*/

G4bool BesZddDigit::initFunction = true;
std::vector<double> BesZddDigit::fX;
std::vector<double> BesZddDigit::fY;

//! integral of single photoelectron function
G4double BesZddDigit::integral = 95.71250795;

/*
G4double peFunction(G4double* x,G4double* par)
{
  G4double tau = 6; //! rise time in [ns]
  G4double t = x[0];
  return t*t*TMath::Exp(-t*t/(tau*tau));
}
*/
G4double peFunction(G4double x)
{
  G4double tau = 6; //! rise time in [ns]
  G4double t = x;
  return t*t*TMath::Exp(-t*t/(tau*tau));
}

G4double peTotal(G4double x, G4double integral)
{
  G4double gain = 1.;
  G4double colFac = 1.;

  G4double value = -1. * gain * colFac * peFunction(x) / integral ;
  return value;
}

/*
G4double peTotal(G4double* x, G4double* par)
{
  G4double gain = 1.;
  G4double colFac = 1.;
  
  TF1 f ("f",peFunction,0.,80.,1);
  f.SetParameter(0,par[0]);
  G4double integral = f.Integral(0.,80.);
  std::cout.precision(10);
  std::cout << "integral: " << integral << std::endl;
  
  G4double value = -1. * gain * colFac * peFunction(x,par) / BesZddDigit::integral ;
  return value;
}
*/

G4double f1(G4double* x, G4double* par)
{
  G4double R = 3./97.;
  G4double tau1 = 6.4;
  G4double tau2 = 6.5;
  G4double tau3 = 30.4;
  
  return 1./(1.+R) * ( (exp(-x[0]/tau2) - exp(-x[0]/tau1))/(tau2 - tau1) + R/tau3 * exp(-x[0]/tau3));
}

BesZddDigit::BesZddDigit()
{
 
  if( initFunction )
 {
  for(int i = 0; i <= 2000; i++)
    {
      BesZddDigit::fX.push_back( i/100. ); 
 //     std::cout << "BesZddDigit: fX[" << i << "]: " << fX.at(i) << std::endl;
      BesZddDigit::fY.push_back( peTotal(i/100.,BesZddDigit::integral) ); 
 //     std::cout << "BesZddDigit: fY[" << i << "]: " << fY.at(i) << std::endl;
    }
    initFunction = false;
 }
	totEDep101 = 0;totEDep102 = 0;totEDep103 = 0;totEDep104 = 0;totEDep111 = 0;totEDep112 = 0;totEDep113 = 0;totEDep114 = 0;totEDep121 = 0;totEDep122 = 0;totEDep123 = 0;totEDep124 = 0;
	totEDep201 = 0;totEDep202 = 0;totEDep203 = 0;totEDep204 = 0;totEDep211 = 0;totEDep212 = 0;totEDep213 = 0;totEDep214 = 0;totEDep221 = 0;totEDep222 = 0;totEDep223 = 0;totEDep224 = 0;
	
	wrongCalculation = 0;	
	
	m_TrackID = 0;
     	m_CrystalNo = 0;
    // total energy depostion per event
       	m_totEDep = 0.;

	
	
	//! initialize pointers with 0
	Photon101 = NULL;
	Photon102 = NULL;
	Photon103 = NULL;	
	Photon104 = NULL;
	Photon111 = NULL;
	Photon112 = NULL;
	Photon113 = NULL;	
	Photon114 = NULL;
	Photon121 = NULL;
	Photon122 = NULL;
	Photon123 = NULL;	
	Photon124 = NULL;
	Photon201 = NULL;
	Photon202 = NULL;
	Photon203 = NULL;	
	Photon204 = NULL;
	Photon211 = NULL;
	Photon212 = NULL;
	Photon213 = NULL;	
	Photon214 = NULL;
	Photon221 = NULL;
	Photon222 = NULL;
	Photon223 = NULL;	
	Photon224 = NULL;	

	Photon_101 = NULL;
	Photon_102 = NULL;
	Photon_103 = NULL;	
	Photon_104 = NULL;
	Photon_111 = NULL;
	Photon_112 = NULL;
	Photon_113 = NULL;	
	Photon_114 = NULL;
	Photon_121 = NULL;
	Photon_122 = NULL;
	Photon_123 = NULL;	
	Photon_124 = NULL;
	Photon_201 = NULL;
	Photon_202 = NULL;
	Photon_203 = NULL;	
	Photon_204 = NULL;
	Photon_211 = NULL;
	Photon_212 = NULL;
	Photon_213 = NULL;	
	Photon_214 = NULL;
	Photon_221 = NULL;
	Photon_222 = NULL;
	Photon_223 = NULL;	
	Photon_224 = NULL;

#ifdef HISTOS
  G4double x_hist_l = DetectorConstruction::GetUpperCrystalBox().x()/cm-DetectorConstruction::GetCrystalBoxSizeX()/cm/2.+1.*DetectorConstruction::GetCrystalX()/cm;
  G4double x_hist_u = DetectorConstruction::GetUpperCrystalBox().x()/cm-DetectorConstruction::GetCrystalBoxSizeX()/cm/2.+2.*DetectorConstruction::GetCrystalX()/cm;

  G4double y_hist_l = DetectorConstruction::GetUpperCrystalBox().y()/cm-DetectorConstruction::GetCrystalBoxSizeY()/cm/2.+1.*DetectorConstruction::GetCrystalY()/cm;
  G4double y_hist_u = DetectorConstruction::GetUpperCrystalBox().y()/cm-DetectorConstruction::GetCrystalBoxSizeY()/cm/2.+2.*DetectorConstruction::GetCrystalY()/cm;
  
  G4double z_hist_l = DetectorConstruction::GetUpperCrystalBox().z()/cm-DetectorConstruction::GetCrystalBoxSizeZ()/cm/2.;
  G4double z_hist_u = DetectorConstruction::GetUpperCrystalBox().z()/cm-DetectorConstruction::GetCrystalBoxSizeZ()/cm/2.+1.*DetectorConstruction::GetCrystalZ()/cm;
  
  G4int bins = 50;
  
  hXYZpositions = new TH3D("hXYZpositions","",bins,x_hist_l,x_hist_u, bins,y_hist_l,y_hist_u, bins,z_hist_l,z_hist_u);	
  hXYZpositions = new TH3D("hXYZpositions","",bins,z_hist_l,z_hist_u, bins,x_hist_l,x_hist_u, bins,y_hist_l,y_hist_u);	
  hXYZpositions->GetXaxis()->SetTitle("z");
  hXYZpositions->GetYaxis()->SetTitle("x"); 
  hXYZpositions->GetZaxis()->SetTitle("y");
  
  hTimeTraj = new TH1D("hTimeTraj","",40,2.,3.);
  hTimeEmit = new TH1D("hTimeEmit","",2000,0,50);
  hTimeProp = new TH1D("hTimeProp","",40,0.2,1.2);
  hTimeTrans = new TH1D("hTimeTrans","",160,3,7);
  hTemp1 = new TH1D("hTemp1", "",3000,5,80);
  hTemp2 = new TH1D("hTemp2", "", 8000,0,80);
#endif
  
  fTimeEmit = new TF1("fTimeEmit",f1,0,55.,0);

  for(int i = 0; i<=5; i++) m_destroyFlag[i] = 0;
  for(int i = 0; i<48; i++) m_index[i] = 0;
}

BesZddDigit::~BesZddDigit() 
{
/*
    TH2D* chargeChannel = new TH2D("chargeChannel","",400,300.,700.,3000,0.,30000.);
    chargeChannel->Fill(totEDep, Photon112->GetChargeChannel() );
    TFile f("digitization.root","recreate");
    chargeChannel->Write();
    f.Close();
    delete chargeChannel;
*/
    
//  std::cout << "BesZddDigit::~BesZddDigit() : EDep in Crystal 112 [MeV] : " << totEDep << std::endl; 
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 0 :             " << m_destroyFlag[0] << std::endl;
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 1 :             " << m_destroyFlag[1] << std::endl; 
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 2 :             " << m_destroyFlag[2] << std::endl;
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 3 :             " << m_destroyFlag[3] << std::endl;
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 4 :             " << m_destroyFlag[4] << std::endl;
//  std::cout << "BesZddDigit::~BesZddDigit() : destroyFlag 5 :             " << m_destroyFlag[5] << std::endl;
//  std::cout << "----------------------------------------------------------------------------" << std::endl;
eventCounter++;
// photonsPerMeV += (m_destroyFlag[0]/totEDep112);
// std::cout << "BesZddDigit::~BesZddDigit() : #Photons/MeV (Crystal 112): " << m_destroyFlag[0]/totEDep112 << std::endl;
// std::cout << "BesZddDigit::~BesZddDigit() : Average #Photons/MeV (Crystal 112): " << photonsPerMeV/eventCounter << std::endl;
// std::cout << "############################################################################" << std::endl;
 
 if(eventCounter%100 == 0)
 {
   TFile f("EDepChargeChannel.root","recreate");
   hEDepChargeChannel->Write();
   f.Close();
 }  
 #ifdef HISTOS
  TFile f("digitization.root","recreate");
  chargeChannel->Write();
//  std::cout << "test1" << std::endl;
  hXYZpositions->Write();
//  std::cout << "test1a" << std::endl;
  hTimeTraj->Write();
//  std::cout << "test1b" << std::endl;
  hTimeEmit->Write();
//  std::cout << "test1c" << std::endl;
  hTimeProp->Write();
//  std::cout << "test1d" << std::endl;
  hTimeTrans->Write();
//  std::cout << "test1e" << std::endl;
//  fPE->Write();
  //  fTot->Write();
//    std::cout << "test2" << std::endl;
  hTemp1->Write();
//    std::cout << "test3" << std::endl;
  hTemp2->Write();
//   std::cout << "test4" << std::endl;
//  fPE->Write();
//  fPMT->Write();
  f.Close();
//    std::cout << "test5" << std::endl;
//  std::cout << "totalHits:          " << totalHits << std::endl;
//  std::cout << "wrong Calculations: " << wrongCalculation << std::endl;
  delete ChargeChannel;
  delete hXYZpositions;
  delete hTimeTraj;
  delete hTimeEmit;
  delete hTimeProp;
  delete hTimeTrans;
  delete hTemp1;
  delete hTemp2;
  delete fTimeEmit;
  delete fTot;
  delete fPE;
//  delete fPMT;
  
#endif
  
  delete Photon101; 
  delete Photon102; 
  delete Photon103; 
  delete Photon104; 
  delete Photon111; 
  delete Photon112; 
  delete Photon113; 
  delete Photon114; 
  delete Photon121;
  delete Photon122; 
  delete Photon123; 
  delete Photon124; 

  delete Photon201; 
  delete Photon202; 
  delete Photon203; 
  delete Photon204; 
  delete Photon211;
  delete Photon212;
  delete Photon213;
  delete Photon214; 
  delete Photon221;
  delete Photon222; 
  delete Photon223; 
  delete Photon224;

  delete Photon_101;
  delete Photon_102; 
  delete Photon_103; 
  delete Photon_104; 
  delete Photon_111; 
  delete Photon_112; 
  delete Photon_113; 
  delete Photon_114; 
  delete Photon_121;
  delete Photon_122;
  delete Photon_123; 
  delete Photon_124;

  delete Photon_201; 
  delete Photon_202; 
  delete Photon_203;
  delete Photon_204; 
  delete Photon_211; 
  delete Photon_212; 
  delete Photon_213;
  delete Photon_214; 
  delete Photon_221;
  delete Photon_222; 
  delete Photon_223; 
  delete Photon_224;	
/*
  delete uniRand1;
  delete uniRand2;  
  delete uniRand3;
  delete uniRand4;  
  delete uniRand5;
  */
}

void BesZddDigit::SetHit(BesZddHit* hit)
{
 
  m_pHit = hit;
 
//  totalHits++;
//  G4int trackID = m_pHit->GetTrackID();   
  G4int preCrystalNo = m_pHit->GetPreCrystalNo();  	
  G4int postCrystalNo = m_pHit->GetPostCrystalNo();  			   
//if(preCrystalNo != postCrystalNo) std::cout << "edep between different crystals!!! " << std::endl;
//  G4int pdg = m_pHit->GetPDGCode();				
  G4double edep = m_pHit->GetEDep(); 
//  std::cout << "edep per hit " << m_pHit->GetEDep() << " MeV " <<  std::endl;
//  G4ThreeVector momentum = m_pHit->GetMomentum();			
  G4double preTime = m_pHit->GetPreTime();				
  G4double postTime = m_pHit->GetPostTime();
  G4double deltaT = postTime - preTime;
  
  G4ThreeVector prePos = m_pHit->GetPrePos();				
  G4ThreeVector postPos = m_pHit->GetPostPos();
  G4double deltaX = postPos.x()/cm - prePos.x()/cm;
  G4double deltaY = postPos.y()/cm - prePos.y()/cm;
  G4double deltaZ = postPos.z()/cm - prePos.z()/cm;  
  
//if(postCrystalNo == 112)  hXYZpositions->Fill(postPos.z()/cm,postPos.x()/cm,postPos.y()/cm);
//  std::cout << "PrePos X,Y,Z: " << prePos.x()/cm << ", " <<  prePos.y()/cm << ", "  << prePos.z()/cm << std::endl;
//  std::cout << "PostPos X,Y,Z: " << postPos.x()/cm << ", " <<  postPos.y()/cm << ", "  << postPos.z()/cm << std::endl;
  
//  std::cout << "crystalNo PreStep: " << preCrystalNo << ", calculated crystal: " << DetectorConstruction::CheckPosition(prePos.x()/cm,prePos.y()/cm,prePos.z()/cm) << std::endl;
//  std::cout << "crystalNo PostStep: " << postCrystalNo << ", calculated crystal: " << DetectorConstruction::CheckPosition(postPos.x()/cm,postPos.y()/cm,postPos.z()/cm) << std::endl;
//  if(DetectorConstruction::CheckPosition(prePos.x(),prePos.y(),prePos.z()) < 0) wrongCalculation++;
//  if(DetectorConstruction::CheckPosition(postPos.x(),postPos.y(),postPos.z()) < 0) wrongCalculation++;

/*
  if(preCrystalNo != DetectorConstruction::CheckPosition(prePos.x(),prePos.y(),prePos.z())) 
  {
//   wrongCalculation++;
//    std::cout << "BesZddDigit.cc :: error: real preCrystal " << preCrystalNo << " != calculated crystal " << DetectorConstruction::CheckPosition(prePos.x(),prePos.y(),prePos.z()) << std::endl;
  }
  if(postCrystalNo != DetectorConstruction::CheckPosition(postPos.x(),postPos.y(),postPos.z()))
  {
//   wrongCalculation++;
//  std::cout << "BesZddDigit.cc :: error: real postCrystal " << postCrystalNo << " != calculated crystal " << DetectorConstruction::CheckPosition(postPos.x(),postPos.y(),postPos.z()) << std::endl << "================================================================================================" << std::endl;
  }
*/  
  G4int numberOfPhotons = G4int(edep*BesZddDigit::photonConstant);
//  std::cout << "BesZddDigit:: SetHit : number of photons: " << numberOfPhotons << std::endl;
  switch(preCrystalNo)
  {
    case 101:
    totEDep101+= edep;
      break;
    case 102:
    totEDep102+= edep;
      break;
    case 103:
    totEDep103+= edep;
      break;
    case 104:
    totEDep104+= edep;
      break;      
    case 111:
    totEDep111+= edep;
      break;
    case 112:
    totEDep112+= edep;
      break;
    case 113:
    totEDep113+= edep;
      break;
    case 114:
    totEDep114+= edep;
      break;    
    case 121:
    totEDep121+= edep;
      break;
    case 122:
    totEDep122+= edep;
      break;
    case 123:
    totEDep123+= edep;
      break;
    case 124:
    totEDep124+= edep;
      break;          

    case 201:
    totEDep201+= edep;
      break;
    case 202:
    totEDep202+= edep;
      break;
    case 203:
    totEDep203+= edep;
      break;
    case 204:
    totEDep204+= edep;
      break;      
    case 211:
    totEDep211+= edep;
      break;
    case 212:
    totEDep212+= edep;
      break;
    case 213:
    totEDep213+= edep;
      break;
    case 214:
    totEDep214+= edep;
      break;    
    case 221:
    totEDep221+= edep;
      break;
    case 222:
    totEDep222+= edep;
      break;
    case 223:
    totEDep223+= edep;
      break;
    case 224:
    totEDep224+= edep;
      break;    
    default:
      std::cout << "BesZddDigit.cc :: SetHit : Failure!! Strange Crystal Number!! " << std::endl;
      break;
  } // end switch(preCrystalNo)
  
  
for(int i = 0; i < numberOfPhotons; i++)
{
  double theta = G4UniformRand()*0.47201; //=TMath::Pi()/2.
//  double theta = uniRand1->Uniform(TMath::Pi()/2.);
  //! loss through theta angle
/*  if( theta > asin(1.000293/2.2) )  
  {
     m_destroyFlag[1]++;
  }
  else
  {
    //! loss through mismatch crystal<->PMT 
    if(G4UniformRand() > 0.8 )
  //  if(uniRand3->Uniform(1) > 0.8 )
    {
      m_destroyFlag[3]++;
    }
    else
    {
      //! loss through PMT angular acceptance
//      if(uniRand4->Uniform(1) > 0.66)
      if(G4UniformRand() > 0.66)
      {  
	m_destroyFlag[4]++;
      }
      else
      {
	//! loss through  PMT quantum efficiency
//	if(uniRand5->Uniform(1) > 0.21)
	if(G4UniformRand() > 0.21)
	{
	  m_destroyFlag[5]++;
	}
	else
	{
*/  
	   G4double random = G4UniformRand();
	   G4double x = prePos.x()/cm + random*deltaX; //std::cout << "PreX: " << prePos.x()/cm<< ", PostX: " << postPos.x()/cm << ", x : " << x << std::endl;
	   G4double y = prePos.y()/cm + random*deltaY; //std::cout << "PreY: " << prePos.y()/cm<< ", PostY: " << postPos.y()/cm << ", y : " << y << std::endl;
	   G4double z = prePos.z()/cm + random*deltaZ; //std::cout << "PreZ: " << prePos.z()/cm<< ", PostZ: " << postPos.z()/cm << ", z : " << z << std::endl;  
	   G4double t_traj = preTime + random*deltaT;
    
 //     std::cout<< "theta: " << theta << std::endl;
 //     std::cout << asin(1.000293/2.2) << std::endl;
    switch( DetectorConstruction::CheckPosition(x,y,z) )
    {
      //! upper crystals
      case 101:
	if(Photon101 == NULL)
	{  
	  Photon101 = new Photon();
          Photon101->SetCrystalNo(101);
	}
	m_index[0]++;
	Photon101->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta	
	BesZddDigit::PhotonCalculations(m_index[0], *Photon101 );
	  break;
      case 102:
	if(Photon102 == NULL)
	{  
	  Photon102 = new Photon();
          Photon102->SetCrystalNo(102);
	}
	m_index[1]++;
	Photon102->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[1], *Photon102 );
	break;
      case 103:
	if(Photon103 == NULL)
	{  
	  Photon103 = new Photon();
          Photon103->SetCrystalNo(103);
	}
	m_index[2]++;
	Photon103->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[2], *Photon103 );
	break;
      case 104:
	if(Photon104 == NULL) 
	{  
	  Photon104 = new Photon();
          Photon104->SetCrystalNo(104);
	}
	m_index[3]++;
	Photon104->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[3], *Photon104 );
	break;
      case 111:
	if(Photon111 == NULL)
	{  
	  Photon111 = new Photon();
          Photon111->SetCrystalNo(111);
	}
	m_index[4]++;
	Photon111->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[4], *Photon111 );
	break; 
      case 112:
	if(Photon112 == NULL) 
	{  
	  Photon112 = new Photon();
          Photon112->SetCrystalNo(112);
	}
#ifdef HISTOS
	hXYZpositions->Fill(z,x,y);
	hTimeTraj->Fill(t_traj);
#endif
	m_index[5]++;
	Photon112->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations( m_index[5], *Photon112 );
	break;
      case 113:
	if(Photon113 == NULL)
	{  
	  Photon113 = new Photon();
          Photon113->SetCrystalNo(113);
	}
	m_index[6]++;
	Photon113->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[6], *Photon113 );
	break;
      case 114:
	if(Photon114 == NULL) 
	{  
	  Photon114 = new Photon();
          Photon114->SetCrystalNo(114);
	}
	m_index[7]++;
	Photon114->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[7], *Photon114 );
	break;
      case 121:
	if(Photon121 == NULL)
	{  
	  Photon121 = new Photon();
          Photon121->SetCrystalNo(121);
	}	
	m_index[8]++;
	Photon121->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[8], *Photon121 );
	break;
      case 122:
	if(Photon122 == NULL)
	{  
	  Photon122 = new Photon();
          Photon122->SetCrystalNo(122);
	}
	m_index[9]++;
	Photon122->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[9], *Photon122 );
	break;
      case 123:
	if(Photon123 == NULL)
	{  
	  Photon123 = new Photon();
          Photon123->SetCrystalNo(123);
	}
	m_index[10]++;
	Photon123->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[10], *Photon123 );
	break;
      case 124:
	if(Photon124 == NULL)
	{  
	  Photon124 = new Photon();
          Photon124->SetCrystalNo(124);
	}
	m_index[11]++;
	Photon124->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[11], *Photon124 );
	break;
	
      //! lower crystals
      case 201:
	if(Photon201 == NULL)
	{  
	  Photon201 = new Photon();
          Photon201->SetCrystalNo(201);
	}
	m_index[12]++;
	Photon201->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta	
	BesZddDigit::PhotonCalculations(m_index[12], *Photon201 );
	  break;
      case 202:
	if(Photon202 == NULL)
	{  
	  Photon202 = new Photon();
          Photon202->SetCrystalNo(202);
	}
	m_index[13]++;
	Photon202->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[13], *Photon202 );
	break;
      case 203:
	if(Photon203 == NULL)
	{  
	  Photon203 = new Photon();
          Photon203->SetCrystalNo(203);
	}
	m_index[14]++;
	Photon203->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[14], *Photon203 );
	break;
      case 204:
	if(Photon204 == NULL) 
	{  
	  Photon204 = new Photon();
          Photon204->SetCrystalNo(204);
	}
	m_index[15]++;
	Photon204->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[15], *Photon204 );
	break;
      case 211:
	if(Photon211 == NULL)
	{  
	  Photon211 = new Photon();
          Photon211->SetCrystalNo(211);
	}
	m_index[16]++;
	Photon211->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[16], *Photon211 );
	break;
      case 212:
	if(Photon212 == NULL) 
	{  
	  Photon212 = new Photon();
          Photon212->SetCrystalNo(212);
	}
	m_index[17]++;
	Photon212->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[17], *Photon212 );
	break;
      case 213:
	if(Photon213 == NULL)
	{  
	  Photon213 = new Photon();
          Photon213->SetCrystalNo(213);
	}
	m_index[18]++;
	Photon213->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[18], *Photon213 );
	break;
      case 214:
	if(Photon214 == NULL) 
	{  
	  Photon214 = new Photon();
          Photon214->SetCrystalNo(214);
	}
	m_index[19]++;
	Photon214->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[19], *Photon214 );
	break;
      case 221:
	if(Photon221 == NULL)
	{  
	  Photon221 = new Photon();
          Photon221->SetCrystalNo(221);
	}
	m_index[20]++;
	Photon221->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[20], *Photon221 );
	break;
      case 222:
	if(Photon222 == NULL)
	{  
	  Photon222 = new Photon();
          Photon222->SetCrystalNo(222);
	}
	m_index[21]++;
	Photon222->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[21], *Photon222 );
	break;
      case 223:
	if(Photon223 == NULL)
	{  
	  Photon223 = new Photon();
          Photon223->SetCrystalNo(223);
	}
	m_index[22]++;
	Photon223->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[22], *Photon223 );
	break;
      case 224:
	if(Photon224 == NULL)
	{  
	  Photon224 = new Photon();
          Photon224->SetCrystalNo(224);
	}
	m_index[23]++;
	Photon224->AddPhoton( x, y, z, t_traj, theta ); //! pos_x, pos_y, pos_z, t_traj, theta
	BesZddDigit::PhotonCalculations(m_index[23], *Photon224 );
	break;
      default:
//	std::cout << "very strange: no crystal hit between two hits!! " << std::endl;
	break;
    }// end switch
    //  }// end else(uniRand5...)
    // }// end else (uniRand4...)
    // }// end else (uniRand3...)
   // }// end else (asin...)
  }// end for(numberOfPhotons)
}// end SetHit(..)



void BesZddDigit::PhotonCalculations(G4int& a, Photon& photon )
{
  
  G4int i = a-1;
 /*   
    std::cout << "BesZddDigit::PhotonCalculations : crystalNo:      " << photon.GetCrystalNo() << std::endl;  

    std::cout << "BesZddDigit::PhotonCalculations : m_index i:      " << i << std::endl;
    std::cout << "BesZddDigit::PhotonCalculations : right m_index : " << photon.GetPosXSize()-1 << std::endl;

    std::cout << "BesZddDigit::PhotonCalculations : PosXSize :      " << photon.GetPosXSize() << std::endl;
    std::cout << "BesZddDigit::PhotonCalculations : PosYsize :      " << photon.GetPosYSize() << std::endl;
    std::cout << "BesZddDigit::PhotonCalculations : PosZsize :      " << photon.GetPosZSize() << std::endl;    
    std::cout << "BesZddDigit::PhotonCalculations : ThetaSize :     " << photon.GetThetaSize() << std::endl << "================================================" << std::endl;      
    
    std::cout << "BesZddDigit::PhotonCalculations : PosX :          " << photon.GetPosX( photon.GetPosXSize()-1 ) << std::endl;
    std::cout << "BesZddDigit::PhotonCalculations : PosY :          " << photon.GetPosY( photon.GetPosXSize()-1 ) << std::endl;
    std::cout << "BesZddDigit::PhotonCalculations : PosZ :          " << photon.GetPosZ( photon.GetPosXSize()-1 ) << std::endl;    
    std::cout << "BesZddDigit::PhotonCalculations : Theta :         " << photon.GetTheta( photon.GetPosXSize()-1 ) << std::endl << "================================================" << std::endl;    
*/  

#ifdef HISTOS
	  if( photon.GetCrystalNo() == 112 )  hTimeEmit->Fill(emitTime); //! Fill histo only for crystal 112
#endif
//	  std::cout << "BesZddDigit::PhotonCalculations : crystalNo: " << photon.GetCrystalNo() << ", i: " << i << ", vectorsize: " << photon.GetThetaSize() << std::endl;
	  G4double distance = DetectorConstruction::GetDistanceToPMT( photon.GetPosZ(i) ); 
//	  G4double distance = 10.;
//std::cout << "BesZddDigit::PhotonCalculations : PosZ :        " << photon.GetPosZ(i) << std::endl;
//	  std::cout << "BesZddDigit::PhotonCalculations : distance :    " << distance << std::endl;
	  G4double propLength = distance/cos( photon.GetTheta(i) );
//	  G4double propLength = 10.;
	  //	  if( photon.GetCrystalNo() == 112 )  hTemp1->Fill(propLength); //! Fill histo only for crystal 112
	  //	  std::cout << "BesZddDigit::PhotonCalculations : Theta :       " << photon.GetTheta(i) << std::endl;
//	  std::cout << "BesZddDigit::PhotonCalculations : propLength :  " << propLength << std::endl;
  
//	  G4double attenuation = exp(-propLength/0.89); //! probability of photon loss because of attenuation //! 0.89 = radiation length of PbWO4
	  G4double attenuation = exp(-propLength/10.); //! probability of photon loss because of attenuation //! increased radiation length	  
//	  G4double attenuation = 0.8;
	  //	  std::cout << "BesZddDigit::PhotonCalculations : attenuation : " << attenuation << std::endl << "=========================================" << std::endl;

	  //! loss through attenuation
	  //	  std::cout << "BesZddDigit::PhotonCalculations : s " << s << std::endl;
	  if( G4UniformRand() > attenuation) 
//	  if( uniRand2->Uniform(1) > attenuation) 
	  {
	    m_destroyFlag[2]++;
	    photon.SetEmitTime(0.);
	    photon.SetPropTime(0.);
	    photon.SetTransTime(0.);
/*
		std::cout << "BesZddDigit :: PhotonCalculations1 : index i         : " << i << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations1 : posXSize        : " << photon.GetPosXSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations1 : posYSize        : " << photon.GetPosYSize() << std::endl; 
		std::cout << "BesZddDigit :: PhotonCalculations1 : posZSize        : " << photon.GetPosZSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations1 : ThetaSize       : " << photon.GetThetaSize() << std::endl; 
		std::cout << "BesZddDigit :: PhotonCalculations1 : TrajTimeSize    : " << photon.GetTrajTimeSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations1 : EmitTimeSize    : " << photon.GetEmitTimeSize() << std::endl;    
		std::cout << "BesZddDigit :: PhotonCalculations1 : PropTimeSize    : " << photon.GetPropTimeSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations1 : TransTimeSize   : " << photon.GetTransTimeSize() << std::endl;
*/
	    if( !(photon.EraseVectorElement()) ) std::cout << "BesZddDigit::PhotonCalculations : problem with erasing vector element!!" << std::endl;
	    a += -1;
/*
		std::cout << "BesZddDigit :: PhotonCalculations2 : index i         : " << i << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations2 : posXSize        : " << photon.GetPosXSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations2 : posYSize        : " << photon.GetPosYSize() << std::endl; 
		std::cout << "BesZddDigit :: PhotonCalculations2 : posZSize        : " << photon.GetPosZSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations2 : ThetaSize       : " << photon.GetThetaSize() << std::endl; 
		std::cout << "BesZddDigit :: PhotonCalculations2 : TrajTimeSize    : " << photon.GetTrajTimeSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations2 : EmitTimeSize    : " << photon.GetEmitTimeSize() << std::endl;    
		std::cout << "BesZddDigit :: PhotonCalculations2 : PropTimeSize    : " << photon.GetPropTimeSize() << std::endl;
		std::cout << "BesZddDigit :: PhotonCalculations2 : TransTimeSize   : " << photon.GetTransTimeSize() << std::endl; */
	  } // if(attenuation)
	  else
	  {
	    if(photon.GetCrystalNo() == 112) m_destroyFlag[0]++;
	    
	    G4double t_prop = propLength/(2.99792458*10./2.2); //! propagation time in ns!!
	    photon.SetPropTime(t_prop);	 
	    G4double emitTime = GetRandomTimeEmit();
	  //  G4double emitTime = G4UniformRand();
	    photon.SetEmitTime(emitTime);

//	        hTemp1->Fill(0);
	    double t_trans = G4RandGauss::shoot(5.,0.3);//! mean ~ 5ns, sigma ~0.3ns
	    photon.SetTransTime(t_trans);

#ifdef HISTOS
	if( photon.GetCrystalNo() == 112 )
	{
	    hTimeTrans->Fill(t_trans); //! Fill histo only for crystal 112
	    hTemp1->Fill( photon.GetTotalTime(i) );
	    hTemp2->Fill(propLength); //! Fill histo only for crystal 112
	    hTimeProp->Fill(t_prop); //! Fill histo only for crystal 112
	}
#endif


/*
    std::cout << "BesZddDigit :: PhotonCalculations3 : index i         : " << i << std::endl;
    std::cout << "BesZddDigit :: PhotonCalculations3 : posXSize        : " << photon.GetPosXSize() << std::endl;
    std::cout << "BesZddDigit :: PhotonCalculations3 : posYSize        : " << photon.GetPosYSize() << std::endl; 
    std::cout << "BesZddDigit :: PhotonCalculations3 : posZSize        : " << photon.GetPosZSize() << std::endl;
    std::cout << "BesZddDigit :: PhotonCalculations3 : ThetaSize       : " << photon.GetThetaSize() << std::endl; 
    std::cout << "BesZddDigit :: PhotonCalculations3 : TrajTimeSize    : " << photon.GetTrajTimeSize() << std::endl;
    std::cout << "BesZddDigit :: PhotonCalculations3 : EmitTimeSize    : " << photon.GetEmitTimeSize() << std::endl;    
    std::cout << "BesZddDigit :: PhotonCalculations3 : PropTimeSize    : " << photon.GetPropTimeSize() << std::endl;
    std::cout << "BesZddDigit :: PhotonCalculations3 : TransTimeSize   : " << photon.GetTransTimeSize() << std::endl;  
  */  
    } // else attenuation
}


void BesZddDigit::EndOfEvent()
{
  /*
  fPE = new TF1("fPE",peTotal,0,20,1);
  fPE->SetParameter(0,0.);

  for(int i = 0; i<2000; i++) //! save function values in vector
  {
    fX.push_back( i/100.);
    fY.push_back( fPE->Eval(i/100.) );  
  }
  */
//  delete fPE;
  /*
  for(int i = 0; i<2000; i++) //! save function values in vector
  {
    hTemp2->Fill( fX.at(i), fY.at(i) );
  }*/ 

if(Photon101!=NULL) CalculatePMToutput( *Photon101 );
if(Photon102!=NULL) CalculatePMToutput( *Photon102 );
if(Photon103!=NULL) CalculatePMToutput( *Photon103 );
if(Photon104!=NULL) CalculatePMToutput( *Photon104 );
if(Photon111!=NULL) CalculatePMToutput( *Photon111 );
if(Photon112!=NULL) CalculatePMToutput( *Photon112 );
if(Photon113!=NULL) CalculatePMToutput( *Photon113 );
if(Photon114!=NULL) CalculatePMToutput( *Photon114 );
if(Photon121!=NULL) CalculatePMToutput( *Photon121 );
if(Photon122!=NULL) CalculatePMToutput( *Photon122 );
if(Photon123!=NULL) CalculatePMToutput( *Photon123 );
if(Photon124!=NULL) CalculatePMToutput( *Photon124 );
if(Photon201!=NULL) CalculatePMToutput( *Photon201 );
if(Photon202!=NULL) CalculatePMToutput( *Photon202 );
if(Photon203!=NULL) CalculatePMToutput( *Photon203 );
if(Photon204!=NULL) CalculatePMToutput( *Photon204 );
if(Photon211!=NULL) CalculatePMToutput( *Photon211 );
if(Photon212!=NULL) CalculatePMToutput( *Photon212 );
if(Photon213!=NULL) CalculatePMToutput( *Photon213 );
if(Photon214!=NULL) CalculatePMToutput( *Photon214 );
if(Photon221!=NULL) CalculatePMToutput( *Photon221 );
if(Photon222!=NULL) CalculatePMToutput( *Photon222 );
if(Photon223!=NULL) CalculatePMToutput( *Photon223 );
if(Photon224!=NULL) CalculatePMToutput( *Photon224 );

  //  std::cout << "BesZddDigit :: End Of Event" << std::endl;
}

/*
void BesZddDigit::ResizeVectors(Photon& photon)
{
   G4int size = photon.GetThetaSize();
   std::cout << "BesZddDigit::ResizeVectors : VectorSize before erasing: " << size << std::endl;
   
   int element = 0;
   for(int i = 0; i< size-element;)
   {
      //! save all 
      if( photon.GetDestroyFlag(i) != 0) 
      {
	element++;
	if( !(photon.EraseVectorElement(i)) ) std::cout << "BesZddDigit::ResizeVectors : problem with erasing vector element!!" << std::endl;
      }
      else i++;
   }
   
   size = photon.GetThetaSize();
   std::cout << "BesZddDigit::ResizeVectors : VectorSize after erasing: " << size << std::endl;  
}
*/

void BesZddDigit::CalculatePMToutput(Photon& photon)
{  
  G4int size = photon.GetPosXSize();
  for(int i = 0; i < size; i++)
  {
    G4double pTime = photon.GetTotalTime(i);
    for(int k = 0; k <= 2000; k++)  //! add up spectra of the single photoelectrons
    {
//      hTemp2->Fill(fX.at(k)+pTime,fY.at(k));
	int x = (int)(BesZddDigit::fX.at(k)+pTime)/0.025;
//	std::cout << "BesZddDigit::CalculatePMTOutput() : x :" << x << std::endl;
	if( x <= 3200 ) photon.AddOutput( x , BesZddDigit::fY.at(k) ); //! 25ps bins for x-axis, max time is 80ns
    }
  }
  
 G4double charge = 0.;
 G4bool threshold = true;
 G4double time = 0.;
 for(int i = 0; i < 3200; i++)
  {
    charge += photon.GetOutput(i) * 0.025; //! integral = y * delta T (=0.025ns)
    if( fabs( photon.GetOutput(i) ) > 200. && threshold == true) //! threshold arbitrarily set to 200
    {
        time = i*25./1000.; //! shift comma by 3 for better precision 
//	std::cout << "BesZddDigit::CalculatePMTOutput : time: " << time << std::endl;
	threshold = false;
    }
  }

// std::cout << "BesZddDigit :: Calculate PMT output : Charge: " << abs((int)charge) << std::endl;
//  std::cout << "BesZddDigit :: Calculate PMT output" << std::endl;
  photon.SetChargeChannel( abs((int)charge) );
  photon.SetTimeChannel( abs((int)time) );

  double edep = 0;
  switch(photon.GetCrystalNo())
  {
    case 101: edep = totEDep101; break;
    case 102: edep = totEDep102; break;
    case 103: edep = totEDep103; break;
    case 104: edep = totEDep104; break;    
    case 111: edep = totEDep111; break;
    case 112: edep = totEDep112; break;
    case 113: edep = totEDep113; break;
    case 114: edep = totEDep114; break; 
    case 121: edep = totEDep121; break;
    case 122: edep = totEDep122; break;
    case 123: edep = totEDep123; break;
    case 124: edep = totEDep124; break;     
    case 201: edep = totEDep211; break;
    case 202: edep = totEDep202; break;
    case 203: edep = totEDep203; break;
    case 204: edep = totEDep204; break;    
    case 211: edep = totEDep211; break;
    case 212: edep = totEDep212; break;
    case 213: edep = totEDep213; break;
    case 214: edep = totEDep214; break; 
    case 221: edep = totEDep221; break;
    case 222: edep = totEDep222; break;
    case 223: edep = totEDep223; break;
    case 224: edep = totEDep224; break;  
    default: std::cout << "BesZddDigit.cc :: CalculatePMToutput : Failure !!! wrong crystal No!! " << std::endl; break;
  }
    
    hEDepChargeChannel->Fill(edep,abs((int)charge));
    
}
