
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "SensitiveDetector.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "G4UnitsTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SensitiveDetector::SensitiveDetector(G4String name)
:G4VSensitiveDetector(name)
{
  G4String HCname;
  collectionName.insert(HCname="BesZddHitCollection");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SensitiveDetector::~SensitiveDetector(){ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SensitiveDetector::Initialize(G4HCofThisEvent* HCE)
{
  BesZddHitCollection = new BesZddHitsCollection (SensitiveDetectorName,collectionName[0]); 
  static G4int HCID = -1;
  TotEnergyDeposit = 0.;
  //TotLostEnergy = 0.;
  if(HCID<0)
  { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); }
  HCE->AddHitsCollection( HCID, BesZddHitCollection ); 
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool SensitiveDetector::ProcessHits(G4Step* aStep,G4TouchableHistory*)
{
  G4Track *curTrack = aStep->GetTrack();
  
  G4double edep = aStep->GetTotalEnergyDeposit();

  if(edep==0.) return false;
  else
  {
      IncreaseTotEnergyDeposit(edep);
  
      BesZddHit* newHit = new BesZddHit();
  
      newHit->SetTrackID  (curTrack->GetTrackID());
      G4int preCrystalNo = aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber();
      G4int postCrystalNo = aStep->GetPostStepPoint()->GetTouchableHandle()->GetCopyNumber();
      newHit->SetPreCrystalNo(preCrystalNo);
      newHit->SetPostCrystalNo(postCrystalNo);      
//      G4cout << G4endl << "CrystalNo: " << CrystalNo << G4endl;
  
      G4int pdg = curTrack->GetDefinition()->GetPDGEncoding();			       
      newHit->SetPDGCode(pdg);
 					       
      newHit->SetEDep     (edep);
      G4ThreeVector PostPosition = aStep->GetPostStepPoint()->GetPosition();
      newHit->SetPostPos      (PostPosition);
      G4ThreeVector PrePosition = aStep->GetPreStepPoint()->GetPosition();
      newHit->SetPrePos       (PrePosition);
      G4ThreeVector Momentum = aStep->GetPreStepPoint()->GetMomentum();
      newHit->SetMomentum (Momentum);
      G4double PostTime = aStep->GetPostStepPoint()->GetGlobalTime(); 
      newHit->SetPostTime     (PostTime);
      G4double PreTime = aStep->GetPreStepPoint()->GetGlobalTime(); 
      newHit->SetPreTime     (PreTime);
      
  //  G4VPhysicalVolume* vl = (G4TouchableHistory*)(aStep->GetPreStepPoint()->GetTouchable()->GetVolume(0));
  //  newHit->SetVolume   (vl);
      BesZddHitCollection->insert( newHit );
      
 //     G4cout << "pdg code: " << pdg << "\t Global Time: " << G4BestUnit(GlobalTime, "Time") << G4endl;
  //  newHit->Print();
  //  newHit->Draw();
      return true;
  }
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SensitiveDetector::EndOfEvent(G4HCofThisEvent*)
{
  
  if (verboseLevel>0) { 
     G4int NbHits = BesZddHitCollection->entries();
     G4cout << "\n-------->Hits Collection: in this event they are " << NbHits 
            << " hits in the tracker chambers: " << G4endl;
     for (G4int i=0;i<NbHits;i++) (*BesZddHitCollection)[i]->Print();
    } 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

