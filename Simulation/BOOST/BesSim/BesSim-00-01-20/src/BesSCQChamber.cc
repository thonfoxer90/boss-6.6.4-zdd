//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oreiented Simulation Tool                    //
//---------------------------------------------------------------------------//
//Descpirtion: BES beam pipe 
//Author: Liuhm
//Created: May 21, 2003
//Comment:
//---------------------------------------------------------------------------//
//
#include "BesSCQChamber.hh"

#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4VisAttributes.hh"
#include "G4Color.hh"
#include "G4Cons.hh"
#include "G4RotationMatrix.hh"

BesSCQChamber::BesSCQChamber()
{

  logicalSCQChamber = 0;
  physicalSCQChamber1 = 0;
  physicalSCQChamber2 = 0;


  Au = 0;
  Ag = 0;
  Oil = 0;
}

void BesSCQChamber::DefineMaterial()
{
  G4double density, a, z,fractionmass;
  G4int nel,natoms;
  G4String name, symbol;

  density=19.32*g/cm3;
  a = 196.967*g/mole;
  Au= new G4Material("Gold",79,a,density);

  density=10.5*g/cm3;
  a = 107.9*g/mole;
  Ag= new G4Material("Silver",47,a,density);

  a = 1.01*g/mole;
  G4Element* H  = new G4Element(name="Hydrogen",symbol="H" , z= 1., a);
    
  a = 12.01*g/mole;
  G4Element* C  = new G4Element(name="Carbon"  ,symbol="C" , z= 6., a);

  density = 0.810*g/cm3;
  Oil = new G4Material(name="Oil",density,nel=2);
  Oil->AddElement(C, natoms=18);
  Oil->AddElement(H, natoms=38);


  G4double pressure;

a = 14.01*g/mole;
    G4Element* N  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a);
    
    a = 16.00*g/mole;
    G4Element* O  = new G4Element(name="Oxygen"  ,symbol="O" , z= 8., a);


  density = 1.290*mg/cm3;
    G4Material* Air = new G4Material(name="Air",density,nel=2);
    Air->AddElement(N, fractionmass = 0.7);
    Air->AddElement(O, fractionmass = 0.3);



      density     = 1.0e-5*g/cm3;
    pressure    = 2.e-2*bar;
    G4Material* Beam = new G4Material(name="Beam",density,nel=1,kStateGas,STP_Temperature,pressure);
    Beam->AddMaterial(Air,fractionmass=1.0);


density = 1.848*g/cm3;
    a = 9.012182*g/mole;
    G4Material* Beryllium = new G4Material(name="Beryllium",z=4.0,a,density);

    density = 2.70*g/cm3;
    a = 26.98*g/mole;
    G4Material* Aluminium = new G4Material(name="Aluminium",z=13.,a,density);


}

void BesSCQChamber::Construct(G4LogicalVolume* logicalbes)
{
  DefineMaterial();

   //G4RotationMatrix* xRot = new G4RotationMatrix;
  //xRot->rotateX(90*deg);

  //the logical volume of beam pipe
  G4Tubs* solidPip = new G4Tubs("solidPip",0,130./2,1365./2,0,360);  //length: 1.365m;  out diameter: 130mm
  logicalSCQChamber = new G4LogicalVolume(solidPip, G4Material::GetMaterial("Beam"),"logicalSCQChamber");
  physicalSCQChamber1 = new G4PVPlacement(0,G4ThreeVector(0,0, 500+1365./2),logicalSCQChamber,"physicalSCQChamber1",logicalbes,false,0);


  G4RotationMatrix* yRot = new G4RotationMatrix;
  yRot->rotateY(3.14159265);
  physicalSCQChamber2 = new G4PVPlacement(yRot,G4ThreeVector(0,0,-500-1365./2),logicalSCQChamber,"physicalSCQChamber2",logicalbes,false,0);

  //3 components in the 1365mm tube:  0-210 CF63, 210-410 a cone with inner diameter be 63 at one side and 110 at the other side,  410-1365 tube



  G4Tubs* solidSF63_1 = new G4Tubs("solidSF63_1", 63./2, 114./2, 20./2, 0, 2*3.14159265); //inner diameter: 63mm, outer diameter: 114mm,  length: 20mm
  G4LogicalVolume*  logicalSF63_1 = new G4LogicalVolume(solidSF63_1, G4Material::GetMaterial("StainlessSteel"),"logicalSF63_1");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  new G4PVPlacement(0,G4ThreeVector(0,0,-1365./2+20./2),logicalSF63_1,"physicalSF63_1",logicalSCQChamber,false,0);

  G4Tubs* solidSF63_2 = new G4Tubs("solidSF63_2", 63./2, (63.+14.)/2, 40./2, 0, 2*3.14159265); //inner diameter: 63mm, thickness: 14mm,  length: 40mm
  G4LogicalVolume*  logicalSF63_2 = new G4LogicalVolume(solidSF63_2, G4Material::GetMaterial("StainlessSteel"),"logicalSF63_2");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  new G4PVPlacement(0,G4ThreeVector(0,0,-1365./2+20 +40./2),logicalSF63_2,"physicalSF63_2",logicalSCQChamber,false,0);  


  //SF63 is connected to SCQ chamber?

  G4Tubs* solidChambertube_front = new G4Tubs("solidChambertube_front", 63./2, (63.+4.)/2, 150./2, 0, 2*3.14159265); //inner diameter: 63mm, thickness: 4mm,  length: 40mm
  G4LogicalVolume*  logicalChambertube_front = new G4LogicalVolume(solidChambertube_front, G4Material::GetMaterial("Copper"),"logicalChambertube_front");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  new G4PVPlacement(0,G4ThreeVector(0,0,-1365./2+20 +40+150./2),logicalChambertube_front,"physicalChambertube_front",logicalSCQChamber,false,0);
  

  //S-Bellows around the front chamber

  G4Tubs* solidS_Bellow = new G4Tubs("solidS_Bellow", (63.+4.)/2, 103./2, 50./2, 0, 2*3.14159265); //inner diameter: 63mm, thickness: 14mm,  length: 40mm
  G4LogicalVolume*  logicalS_Bellow = new G4LogicalVolume(solidS_Bellow, G4Material::GetMaterial("StainlessSteel"),"logicalS_Bellow");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  new G4PVPlacement(0,G4ThreeVector(0,0,-1365./2+20 +40 +150./2),logicalS_Bellow,"physicalSF63_2",logicalSCQChamber,false,0);





 
  G4Cons* solidCons = new G4Cons("solidCons", 63./2, (63.+4.)/2, 110./2, (110.+4.)/2, 200./2,  0, 360);
  G4LogicalVolume* logicalCons = new G4LogicalVolume(solidCons, G4Material::GetMaterial("Copper"),"logicalCons"); ////FIX ME PLEASE, what material???.  *********************BE Careful*********************.
  G4VPhysicalVolume* phycicalCons = new G4PVPlacement(0,G4ThreeVector(0,0,-1365./2+210+200./2),logicalCons,"physicalCons",logicalSCQChamber,false,0);
 

  G4Tubs* solidChambertube = new G4Tubs("solidChamberTube", 110./2, (110.+4.)/2, 955./2, 0, 360); //inner diameter: 63mm, thickness: 14mm,  length: 210mm
  G4LogicalVolume*  logicalChambertube = new G4LogicalVolume(solidChambertube, G4Material::GetMaterial("Copper"),"logicalChambertube");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  G4VPhysicalVolume *phycicalChambertube = new G4PVPlacement(0,G4ThreeVector(0,0,-1365./2+210.+200+955./2),logicalChambertube,"physicalChambertube",logicalSCQChamber,false,0);


  


  logicalSCQChamber->SetVisAttributes(G4VisAttributes::Invisible);
  G4VisAttributes* visSF63 = new G4VisAttributes(G4Colour(1.,1.,0.));

  G4VisAttributes* visPip = new G4VisAttributes(G4Colour(209./255,196./255, 33./255));  
  logicalCons->SetVisAttributes(visPip);
  logicalChambertube->SetVisAttributes(visPip);
  logicalChambertube_front->SetVisAttributes(visPip);

  G4VisAttributes* visSteel = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  logicalSF63_1->SetVisAttributes(visSteel);
  logicalSF63_2->SetVisAttributes(visSteel);
  logicalS_Bellow->SetVisAttributes(visSteel);  








}
