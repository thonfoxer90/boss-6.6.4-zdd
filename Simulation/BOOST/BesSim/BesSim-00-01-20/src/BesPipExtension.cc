//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oreiented Simulation Tool                    //
//---------------------------------------------------------------------------//
//Descpirtion: BES beam pipe 
//Author: Liuhm
//Created: May 21, 2003
//Comment:
//---------------------------------------------------------------------------//
//
#include "BesPipExtension.hh"

#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4VisAttributes.hh"
#include "G4Color.hh"

BesPipExtension::BesPipExtension()
{

  logicalPipExtension = 0;
  physicalPip1Extension = 0;
  physicalPip2Extension = 0;


  Au = 0;
  Ag = 0;
  Oil = 0;
}

void BesPipExtension::DefineMaterial()
{
  G4double density, a, z,fractionmass;
  G4int nel,natoms;
  G4String name, symbol;

  density=19.32*g/cm3;
  a = 196.967*g/mole;
  Au= new G4Material("Gold",79,a,density);

  density=10.5*g/cm3;
  a = 107.9*g/mole;
  Ag= new G4Material("Silver",47,a,density);

  a = 1.01*g/mole;
  G4Element* H  = new G4Element(name="Hydrogen",symbol="H" , z= 1., a);
    
  a = 12.01*g/mole;
  G4Element* C  = new G4Element(name="Carbon"  ,symbol="C" , z= 6., a);

  density = 0.810*g/cm3;
  Oil = new G4Material(name="Oil",density,nel=2);
  Oil->AddElement(C, natoms=18);
  Oil->AddElement(H, natoms=38);


  G4double pressure;

a = 14.01*g/mole;
    G4Element* N  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a);
    
    a = 16.00*g/mole;
    G4Element* O  = new G4Element(name="Oxygen"  ,symbol="O" , z= 8., a);


  density = 1.290*mg/cm3;
    G4Material* Air = new G4Material(name="Air",density,nel=2);
    Air->AddElement(N, fractionmass = 0.7);
    Air->AddElement(O, fractionmass = 0.3);



      density     = 1.0e-5*g/cm3;
    pressure    = 2.e-2*bar;
    G4Material* Beam = new G4Material(name="Beam",density,nel=1,kStateGas,STP_Temperature,pressure);
    Beam->AddMaterial(Air,fractionmass=1.0);


density = 1.848*g/cm3;
    a = 9.012182*g/mole;
    G4Material* Beryllium = new G4Material(name="Beryllium",z=4.0,a,density);

    density = 2.70*g/cm3;
    a = 26.98*g/mole;
    G4Material* Aluminium = new G4Material(name="Aluminium",z=13.,a,density);




	G4NistManager* man = G4NistManager::Instance();
	man->SetVerbose(1);
	// Define elements from NIST 
	man->FindOrBuildElement("Be");
	man->FindOrBuildElement("Al");
	G4Element* Si = man->FindOrBuildElement("Si");
	man->FindOrBuildElement("Ti");
	G4Element* Cr = man->FindOrBuildElement("Cr");
	G4Element* Mn = man->FindOrBuildElement("Mn");
	G4Element* Fe = man->FindOrBuildElement("Fe");
	G4Element* Ni = man->FindOrBuildElement("Ni");
	man->FindOrBuildElement("W");
	man->FindOrBuildElement("Au");
	man->FindOrBuildElement("Pb");

    G4int ncomponents;
    G4Material* StainlessSteel = new G4Material("StainlessSteel", density= 8.06*g/cm3, ncomponents=6);
	StainlessSteel->AddElement(C, fractionmass=0.001);
	StainlessSteel->AddElement(Si, fractionmass=0.007);
	StainlessSteel->AddElement(Cr, fractionmass=0.18);
	StainlessSteel->AddElement(Mn, fractionmass=0.01);
	StainlessSteel->AddElement(Fe, fractionmass=0.712);
	StainlessSteel->AddElement(Ni, fractionmass=0.09);


}

void BesPipExtension::Construct(G4LogicalVolume* logicalbes)
{
  DefineMaterial();

   //G4RotationMatrix* xRot = new G4RotationMatrix;
  //xRot->rotateX(90*deg);

  G4RotationMatrix* zRot_45 = new G4RotationMatrix;  zRot_45->rotateZ(3.14159265/4);
  G4RotationMatrix* zRot_90 = new G4RotationMatrix;  zRot_90->rotateZ(3.14159265/2);
  G4RotationMatrix* zRot_135 = new G4RotationMatrix;  zRot_135->rotateZ(3.14159265/4*3);
  G4RotationMatrix* zRot_180 = new G4RotationMatrix;  zRot_180->rotateZ(3.14159265);
  G4RotationMatrix* zRot_225 = new G4RotationMatrix;  zRot_225->rotateZ(3.14159265/4*5);
  G4RotationMatrix* zRot_270 = new G4RotationMatrix;  zRot_270->rotateZ(3.14159265/2*3);
  G4RotationMatrix* zRot_315 = new G4RotationMatrix;  zRot_315->rotateZ(3.14159265/4*7);
  G4RotationMatrix* yRot_180 = new G4RotationMatrix;  yRot_180->rotateY(3.14159265);
  G4RotationMatrix* xRot_180 = new G4RotationMatrix;  xRot_180->rotateX(3.14159265);



  //the logical volume of beam pipe
  G4Tubs* solidPip = new G4Tubs("solidPip",63./2,114/2,300./2,0,2*3.1415926);
  logicalPipExtension = new G4LogicalVolume(solidPip, G4Material::GetMaterial("Beam"),"logicalPipExtension");
  physicalPip1Extension = new G4PVPlacement(0,G4ThreeVector(0,0, 500-300./2),logicalPipExtension,"physicalPip1Extension",logicalbes,false,0);
  physicalPip2Extension = new G4PVPlacement(xRot_180,G4ThreeVector(0,0,-500+300./2),logicalPipExtension,"physicalPip2Extension",logicalbes,false,0);



  //Part1:  the Vacuum Flange (StainlessSteel)

  double r_in_flange = 69, r_out_flange = 114, width_flange = 16;  //mm
/*
  G4Tubs* solidFlange = new G4Tubs("solidFlange",r_in_flange/2,r_out_flange/2,width_flange/2,0,2*3.1415926);
  G4LogicalVolume* logicalFlange = new G4LogicalVolume(solidFlange, G4Material::GetMaterial("StainlessSteel"), "logicaFlange");
  new G4PVPlacement(0,G4ThreeVector(0,0, (300-width_flange)/2.),logicalFlange,"physicalFlange",logicalPipExtension,false,0);
*/

  G4Tubs* solidFlange = new G4Tubs("solidFlange", r_in_flange/2,r_out_flange/2,width_flange/2, 0, 0.25*3.1415926);
  G4LogicalVolume* logicalFlange_container_one_fourth = new G4LogicalVolume(solidFlange, G4Material::GetMaterial("Beam"), "logicalFlange_container_one_fourth");


  new G4PVPlacement(0,G4ThreeVector(0,0, (300-width_flange)/2),logicalFlange_container_one_fourth,"physicalFlange_container_1",logicalPipExtension,false,0);
  new G4PVPlacement(zRot_45,G4ThreeVector(0,0, (300-width_flange)/2),logicalFlange_container_one_fourth,"physicalFlange_container_2",logicalPipExtension,false,0);
  new G4PVPlacement(zRot_90,G4ThreeVector(0,0, (300-width_flange)/2),logicalFlange_container_one_fourth,"physicalFlange_container_3",logicalPipExtension,false,0);
  new G4PVPlacement(zRot_135,G4ThreeVector(0,0, (300-width_flange)/2),logicalFlange_container_one_fourth,"physicalFlange_container_4",logicalPipExtension,false,0);
  new G4PVPlacement(zRot_180,G4ThreeVector(0,0, (300-width_flange)/2),logicalFlange_container_one_fourth,"physicalFlange_container_5",logicalPipExtension,false,0);
  new G4PVPlacement(zRot_225,G4ThreeVector(0,0, (300-width_flange)/2),logicalFlange_container_one_fourth,"physicalFlange_container_6",logicalPipExtension,false,0);
  new G4PVPlacement(zRot_270,G4ThreeVector(0,0, (300-width_flange)/2),logicalFlange_container_one_fourth,"physicalFlange_container_7",logicalPipExtension,false,0);
  new G4PVPlacement(zRot_315,G4ThreeVector(0,0, (300-width_flange)/2),logicalFlange_container_one_fourth,"physicalFlange_container_8",logicalPipExtension,false,0);
  


  double part0_degree = 12.5/57.2958;  
  double part1_degree = 17.5/57.2958;  
  double part2_degree = 27.5/57.2958;  
  double part3_degree = 32.5/57.2958;  
  double part4_degree = 45./57.2958;  
  double part5_degree;


  G4Tubs* solid_part_Flange_0 = new G4Tubs("solid_part_Flange_0", 69./2,  83./2,  16./2, 0, part4_degree);
  G4Tubs* solid_part_Flange_1 = new G4Tubs("solid_part_Flange_1", 83./2,  100./2, 16./2, 0, part0_degree);
  G4Tubs* solid_part_Flange_2 = new G4Tubs("solid_part_Flange_2", 100./2, 114./2, 16./2, 0, part1_degree);
  G4Tubs* solid_part_Flange_3 = new G4Tubs("solid_part_Flange_3", 83./2,  100./2, 16./2, part3_degree, part4_degree-part3_degree);
  G4Tubs* solid_part_Flange_4 = new G4Tubs("solid_part_Flange_4", 100./2, 114./2, 16./2, part2_degree, part4_degree-part2_degree);

  G4LogicalVolume* logic_part_Flange_0 = new G4LogicalVolume(solid_part_Flange_0, G4Material::GetMaterial("StainlessSteel"),"logic_part_Flange_0");
  G4LogicalVolume* logic_part_Flange_1 = new G4LogicalVolume(solid_part_Flange_1, G4Material::GetMaterial("StainlessSteel"),"logic_part_Flange_1");
  G4LogicalVolume* logic_part_Flange_2 = new G4LogicalVolume(solid_part_Flange_2, G4Material::GetMaterial("StainlessSteel"),"logic_part_Flange_2");
  G4LogicalVolume* logic_part_Flange_3 = new G4LogicalVolume(solid_part_Flange_3, G4Material::GetMaterial("StainlessSteel"),"logic_part_Flange_3");
  G4LogicalVolume* logic_part_Flange_4 = new G4LogicalVolume(solid_part_Flange_4, G4Material::GetMaterial("StainlessSteel"),"logic_part_Flange_4");

  new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part_Flange_0,"physical_part_Flange_0",logicalFlange_container_one_fourth,false,0);
  new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part_Flange_1,"physical_part_Flange_1",logicalFlange_container_one_fourth,false,0);
  new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part_Flange_2,"physical_part_Flange_2",logicalFlange_container_one_fourth,false,0);
  new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part_Flange_3,"physical_part_Flange_3",logicalFlange_container_one_fourth,false,0);
  new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part_Flange_4,"physical_part_Flange_4",logicalFlange_container_one_fourth,false,0);






  //Part2: inner Cu
  G4Tubs* solidinnerCu = new G4Tubs("solidinnerCu", 63./2, 69./2, 300./2, 0, 2*3.1415926);
  G4LogicalVolume* logicalinnerCu = new G4LogicalVolume(solidinnerCu, G4Material::GetMaterial("Copper"), "logicalinnerCu");

  G4VPhysicalVolume* physicalinnerCu = new G4PVPlacement(0,G4ThreeVector(0,0, 0),logicalinnerCu,"physicalinnerCu",logicalPipExtension,false,0);

/*
  G4Tubs* solidouterCu = new G4Tubs("solidouterCu", 73./2, 112./2, 238./2, 0, 2*3.1415926);
  G4LogicalVolume* logicalouterCu = new G4LogicalVolume(solidouterCu, G4Material::GetMaterial("Copper"), "logicalouterCu");
  G4VPhysicalVolume* physicalouterCu = new G4PVPlacement(0,G4ThreeVector(0,0, (238.-300.)/2),logicalouterCu,"physicalouterCu",logicalbes,false,0);
*/


  //container of 1/4 of the outerCu
  G4Tubs* solidouterCu = new G4Tubs("solidouterCu", 73./2, 112./2, 238./2, 0, 0.5*3.1415926);
  G4LogicalVolume* logicalouterCu_container_one_fourth = new G4LogicalVolume(solidouterCu, G4Material::GetMaterial("Beam"), "logicalouterCu_container_one_fourth");
  new G4PVPlacement(0,G4ThreeVector(0,0, (238.-300.)/2),logicalouterCu_container_one_fourth,"physicalouterCu_container_1",logicalPipExtension,false,0);


  new G4PVPlacement(xRot_180,G4ThreeVector(0,0, (238.-300.)/2),logicalouterCu_container_one_fourth,"physicalouterCu_container_2",logicalPipExtension,false,0);
  new G4PVPlacement(zRot_180,G4ThreeVector(0,0, (238.-300.)/2),logicalouterCu_container_one_fourth,"physicalouterCu_container_3",logicalPipExtension,false,0);
  new G4PVPlacement(yRot_180,G4ThreeVector(0,0, (238.-300.)/2),logicalouterCu_container_one_fourth,"physicalouterCu_container_4",logicalPipExtension,false,0);



  //cut slots from the outerCu, by the approach of boolean operations
  //slot_1: at 20 degree;  slot_2: at 65 degree,

  part0_degree = 1.5/57.2958;  //1.5 degree
  part1_degree = 8./57.2958;  //16degree
  part2_degree = 30./57.2958;  //24degree
  part3_degree = 61./57.2958;  //63degree
  part4_degree = 68./57.2958;  //67degree
  part5_degree = 90./57.2958;//90./57.2958;  //90degree


  G4Tubs* solid_part0 = new G4Tubs("solid_part0", 78./2, 112./2, 238./2, 0, part0_degree);
  G4Tubs* solid_part1 = new G4Tubs("solid_part1", 73./2, 112./2, 238./2, part0_degree, part1_degree-part0_degree);
  G4Tubs* solid_part2 = new G4Tubs("solid_part2", 73./2, 94./2,  238./2, part1_degree, part2_degree-part1_degree);
  G4Tubs* solid_part3 = new G4Tubs("solid_part3", 73./2, 112./2, 238./2, part2_degree, part3_degree-part2_degree);
  G4Tubs* solid_part4 = new G4Tubs("solid_part4", 73./2, 94./2,  238./2, part3_degree, part4_degree-part3_degree);
  G4Tubs* solid_part5 = new G4Tubs("solid_part5", 73./2, 112./2, 238./2, part4_degree, part5_degree-part4_degree);

  G4LogicalVolume* logic_part0 = new G4LogicalVolume(solid_part0, G4Material::GetMaterial("Copper"),"logic_part0");
  G4LogicalVolume* logic_part1 = new G4LogicalVolume(solid_part1, G4Material::GetMaterial("Copper"),"logic_part1");
  G4LogicalVolume* logic_part2 = new G4LogicalVolume(solid_part2, G4Material::GetMaterial("Copper"),"logic_part2");
  G4LogicalVolume* logic_part3 = new G4LogicalVolume(solid_part3, G4Material::GetMaterial("Copper"),"logic_part3");
  G4LogicalVolume* logic_part4 = new G4LogicalVolume(solid_part4, G4Material::GetMaterial("Copper"),"logic_part4");
  G4LogicalVolume* logic_part5 = new G4LogicalVolume(solid_part5, G4Material::GetMaterial("Copper"),"logic_part5");

  G4VPhysicalVolume* physical_part0 = new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part0,"physical_part0",logicalouterCu_container_one_fourth,false,0);
  G4VPhysicalVolume* physical_part1 = new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part1,"physical_part1",logicalouterCu_container_one_fourth,false,0);
  G4VPhysicalVolume* physical_part2 = new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part2,"physical_part2",logicalouterCu_container_one_fourth,false,0);
  G4VPhysicalVolume* physical_part3 = new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part3,"physical_part3",logicalouterCu_container_one_fourth,false,0);
  G4VPhysicalVolume* physical_part4 = new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part4,"physical_part4",logicalouterCu_container_one_fourth,false,0);
  G4VPhysicalVolume* physical_part5 = new G4PVPlacement(0,G4ThreeVector(0,0, 0),logic_part5,"physical_part5",logicalouterCu_container_one_fourth,false,0);
  










  logicalPipExtension->SetVisAttributes(G4VisAttributes::Invisible);

  G4VisAttributes* visCopper = new G4VisAttributes(G4Colour(146./255, 83./255, 48./255));
//  G4VisAttributes* visCopper2 = new G4VisAttributes(G4Colour(1, 0, 0));
  G4VisAttributes* visSteel = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));

  logicalinnerCu->SetVisAttributes(visCopper);
  //logicalouterCu->SetVisAttributes(visCopper);
  logicalouterCu_container_one_fourth->SetVisAttributes(G4VisAttributes::Invisible);
  logicalFlange_container_one_fourth->SetVisAttributes(G4VisAttributes::Invisible);

  logic_part0->SetVisAttributes(visCopper);
  logic_part1->SetVisAttributes(visCopper);
  logic_part2->SetVisAttributes(visCopper);
  logic_part3->SetVisAttributes(visCopper);
  logic_part4->SetVisAttributes(visCopper);
  logic_part5->SetVisAttributes(visCopper);
  
  logic_part_Flange_0->SetVisAttributes(visSteel);
  logic_part_Flange_1->SetVisAttributes(visSteel);
  logic_part_Flange_2->SetVisAttributes(visSteel);
  logic_part_Flange_3->SetVisAttributes(visSteel);
  logic_part_Flange_4->SetVisAttributes(visSteel);






}
