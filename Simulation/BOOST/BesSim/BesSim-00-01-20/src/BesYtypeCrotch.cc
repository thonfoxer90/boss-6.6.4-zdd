//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oreiented Simulation Tool                    //
//---------------------------------------------------------------------------//
//Descpirtion: BES beam pipe 
//Author: Liuhm
//Created: May 21, 2003
//Comment:
//---------------------------------------------------------------------------//
//
#include "BesYtypeCrotch.hh"

#include "G4Tubs.hh"
#include "G4Para.hh"
#include "G4EllipticalTube.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4VisAttributes.hh"
#include "G4Color.hh"
#include "G4Cons.hh"
#include "G4Trd.hh"
#include "G4TwoVector.hh"
#include "G4RotationMatrix.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4ExtrudedSolid.hh"
#include "G4ReflectionFactory.hh"

BesYtypeCrotch::BesYtypeCrotch()
{

//   logicalYtypeCrotch = 0;
//   physicalYtypeCrotch1 = 0;
//   physicalYtypeCrotch2 = 0;


  Au = 0;
  Ag = 0;
  Oil = 0;
}

void BesYtypeCrotch::DefineMaterial()
{
  G4double density, a, z,fractionmass;
  G4int nel,natoms;
  G4String name, symbol;

  density=19.32*g/cm3;
  a = 196.967*g/mole;
  Au= new G4Material("Gold",79,a,density);

  density=10.5*g/cm3;
  a = 107.9*g/mole;
  Ag= new G4Material("Silver",47,a,density);

  a = 1.01*g/mole;
  G4Element* H  = new G4Element(name="Hydrogen",symbol="H" , z= 1., a);
    
  a = 12.01*g/mole;
  G4Element* C  = new G4Element(name="Carbon"  ,symbol="C" , z= 6., a);

  density = 0.810*g/cm3;
  Oil = new G4Material(name="Oil",density,nel=2);
  Oil->AddElement(C, natoms=18);
  Oil->AddElement(H, natoms=38);


  G4double pressure;

a = 14.01*g/mole;
    G4Element* N  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a);
    
    a = 16.00*g/mole;
    G4Element* O  = new G4Element(name="Oxygen"  ,symbol="O" , z= 8., a);
    


  density = 1.290*mg/cm3;
    G4Material* Air = new G4Material(name="Air",density,nel=2);
    Air->AddElement(N, fractionmass = 0.7);
    Air->AddElement(O, fractionmass = 0.3);



      density     = 1.0e-5*g/cm3;
    pressure    = 2.e-2*bar;
    G4Material* Beam = new G4Material(name="Beam",density,nel=1,kStateGas,STP_Temperature,pressure);
    Beam->AddMaterial(Air,fractionmass=1.0);


density = 1.848*g/cm3;
    a = 9.012182*g/mole;
    G4Material* Beryllium = new G4Material(name="Beryllium",z=4.0,a,density);

    density = 2.70*g/cm3;
    a = 26.98*g/mole;
    G4Material* Aluminium = new G4Material(name="Aluminium",z=13.,a,density);
    
    density = 8.96*g/cm3;
    a = 63.546*g/mole;
    G4Material* Copper = new G4Material(name="Copper",z=29.0,a,density);

    
    density = 1.0*g/cm3;
    G4Material* Water = new G4Material(name="Water", density, nel=2);
    Water->AddElement(H, natoms=2);
    Water->AddElement(O, natoms=1);
    
    density     = 0.3*mg/cm3;
    pressure    = 2.0*atmosphere;
    G4double temperature = 500.0*kelvin;
    G4Material* WaterSteam = new G4Material(name="WaterSteam",density,nel=1,kStateGas,temperature,pressure);
    WaterSteam->AddMaterial(Water,fractionmass=1);
    
    G4NistManager* man = G4NistManager::Instance();
	man->SetVerbose(1);
    G4Element* Si = man->FindOrBuildElement("Si");
	G4Element* Cr = man->FindOrBuildElement("Cr");
	G4Element* Mn = man->FindOrBuildElement("Mn");
	G4Element* Fe = man->FindOrBuildElement("Fe");
	G4Element* Ni = man->FindOrBuildElement("Ni");
    
    G4int ncomponents;
    G4Material* StainlessSteel = new G4Material("StainlessSteel", density= 8.06*g/cm3, ncomponents=6);
	StainlessSteel->AddElement(C, fractionmass=0.001);
	StainlessSteel->AddElement(Si, fractionmass=0.007);
	StainlessSteel->AddElement(Cr, fractionmass=0.18);
	StainlessSteel->AddElement(Mn, fractionmass=0.01);
	StainlessSteel->AddElement(Fe, fractionmass=0.712);
	StainlessSteel->AddElement(Ni, fractionmass=0.09);
    
    

}

void BesYtypeCrotch::Construct(G4LogicalVolume* logicalbes)
{
  DefineMaterial();

  G4RotationMatrix* zRot_90 = new G4RotationMatrix;  zRot_90->rotateZ(3.14159265/2);
  G4RotationMatrix* zRot_180 = new G4RotationMatrix;  zRot_180->rotateZ(3.14159265);
  G4RotationMatrix* zRot_270 = new G4RotationMatrix;  zRot_270->rotateZ(3.14159265/2*3);
  
  G4RotationMatrix* xRot_180 = new G4RotationMatrix; xRot_180->rotateX(M_PI);
  G4RotationMatrix* xRot_90 = new G4RotationMatrix; xRot_90->rotateX(M_PI/2.);
  G4RotationMatrix* xRot_270 = new G4RotationMatrix; xRot_270->rotateX(3.*M_PI/2.);
  G4RotationMatrix* xRot_10 = new G4RotationMatrix; xRot_10->rotateX(M_PI/18.);
  G4RotationMatrix* xRot_min10 = new G4RotationMatrix; xRot_min10->rotateX(-M_PI/18.);
  
  G4RotationMatrix* yRot_180 = new G4RotationMatrix; yRot_180->rotateY(M_PI);
  
  G4RotationMatrix* xyRot_180 = new G4RotationMatrix; xyRot_180->rotateX(M_PI); xyRot_180->rotateY(M_PI);
  
  G4RotationMatrix* Rot_cover = new G4RotationMatrix; Rot_cover->rotateZ(50./180.*M_PI); Rot_cover->rotateX(M_PI/2.);

  
  G4bool checkoverlap = false;
   //G4RotationMatrix* xRot = new G4RotationMatrix;
  //xRot->rotateX(90*deg);

  //the logical volume of beam pipe
//   G4Tubs* solidPip = new G4Tubs("solidPip",0,300./2,335./2,0,360);  //length: 335mm; 
//   logicalYtypeCrotch = new G4LogicalVolume(solidPip, G4Material::GetMaterial("Beam"),"logicalYtypeCrotch");
//   physicalYtypeCrotch1 = new G4PVPlacement(zRot_90,G4ThreeVector(0,0, 500+1365+335./2),logicalYtypeCrotch,"physicalYtypeCrotch1",logicalbes,false,0);
// 
// 
//   G4RotationMatrix* yzRot = new G4RotationMatrix;
//   yzRot->rotateZ(3.14159265/2);
//   yzRot->rotateY(3.14159265);
//   physicalYtypeCrotch2 = new G4PVPlacement(yzRot,G4ThreeVector(0,0,-500-1365-335./2),logicalYtypeCrotch,"physicalYtypeCrotch2",logicalbes,false,0);



  //5 components in the 335mm tube:  0-20 one side of CF110, 20-70 the rest of CF110,  70-154  complex stuff. 154-267 cons,   267-315 ???   315-335???


/*
  G4Tubs* solidtub1 = new G4Tubs("solidtube1", 110./2, 165/2, 20./2, 0, 360); ;// one side of the SF110   thickness=20 ???? check this!!!!!!!!1-----
  G4LogicalVolume*  logicaltub1 = new G4LogicalVolume(solidtub1, G4Material::GetMaterial("StainlessSteel"),"logicaltub1");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  G4VPhysicalVolume *phycicaltub1 = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20./2),logicaltub1,"physicaltub1",logicalYtypeCrotch,false,0);
  

  G4Tubs* solidtub2 = new G4Tubs("solidtube1", 110./2, 120/2, 50./2, 0, 360); ;// the rest part of SF110
  G4LogicalVolume*  logicaltub2 = new G4LogicalVolume(solidtub2, G4Material::GetMaterial("StainlessSteel"),"logicaltub2");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  G4VPhysicalVolume *phycicaltub2 = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20.+50./2),logicaltub2,"physicaltub2",logicalYtypeCrotch,false,0);


  G4Tubs* solidtub3 = new G4Tubs("solidtube1", 110./2, 180/2, 84./2, 0, 360); ;// the complex stuff with cooling passage
  G4LogicalVolume*  logicaltub3 = new G4LogicalVolume(solidtub3, G4Material::GetMaterial("StainlessSteel"),"logicaltub3");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  G4VPhysicalVolume *phycicaltub3 = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20.+50.+84./2),logicaltub3,"physicaltub3",logicalYtypeCrotch,false,0);


  G4Cons* solidCons = new G4Cons("solidCons", 110./2, (110.+4.)/2, 160./2, (160.+4.)/2, 109./2,  0, 360);   //140 is my guess...:)  correct it please
  G4LogicalVolume* logicalCons = new G4LogicalVolume(solidCons, G4Material::GetMaterial("Copper"),"logicalCons"); ////FIX ME PLEASE, what material???.  *********************BE Careful*********************.
  G4VPhysicalVolume* phycicalCons = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20.+50.+84.+109/2),logicalCons,"physicalCons",logicalYtypeCrotch,false,0);
 


/*
  G4Tubs* solidChambertube = new G4Tubs("solidChamberTube", 110./2, (110.+4.)/2, 955./2, 0, 360); //inner diameter: 63mm, thickness: 14mm,  length: 210mm
  G4LogicalVolume*  logicalChambertube = new G4LogicalVolume(solidChambertube, G4Material::GetMaterial("Aluminium"),"logicalChambertube");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  G4VPhysicalVolume *phycicalChambertube = new G4PVPlacement(0,G4ThreeVector(0,0,-1365./2+210.+200+955./2),logicalChambertube,"physicalChambertube",logicalYtypeCrotch,false,0);
*/


  //to combine the non-regular output pipe

//   double plate_thickness = 4; //*mm
//   double split_length = 48 + plate_thickness;// *mm;
// 
//   G4Box *solidChrotchContainer = new G4Box("solidChrotchContainer", 100./2, 200./2, split_length/2);
//   G4LogicalVolume* logicalChrotchContainer = new G4LogicalVolume(solidChrotchContainer, G4Material::GetMaterial("Beam"),"solidChrotchContainer");
//   G4VPhysicalVolume* phycicalChrotchContainer = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20.+50.+84.+109+split_length/2),logicalChrotchContainer,"phycicalChrotchContainer",logicalYtypeCrotch,false,0);
// 
// 
//   //a plate with thickness of 4mm, subtract two big holes.
// 
//   G4Tubs* solidplate = new G4Tubs("solidplate", 0, 160./2, plate_thickness/2., 0, 3.1415926);
//   G4Tubs* solidhole = new G4Tubs("solidhole", 0, 76./2, plate_thickness/2., 0., 3.1415926*2);
// 
//   G4SubtractionSolid* solidplate_tmp = new G4SubtractionSolid("solidplate_tmp", solidplate, solidhole, 0, G4ThreeVector(0, 38, 0));
//   //G4SubtractionSolid* solidplate_sub_holes     = new G4SubtractionSolid("solidplate_sub_holes", solidplate_tmp, solidhole, 0, G4ThreeVector(0, -38, 0));
// 
//   G4RotationMatrix* zRot = new G4RotationMatrix;  zRot->rotateZ(3.14159265);
// 
//   double pos_plate = -1 * (split_length - plate_thickness)/2;  
// 
//   G4LogicalVolume* logicalPlate = new G4LogicalVolume(solidplate_tmp, G4Material::GetMaterial("StainlessSteel"),"logicalPlate");
//   G4VPhysicalVolume* phycicalPlate = new G4PVPlacement(0,G4ThreeVector(0,0,pos_plate),logicalPlate,"phycicalPlate",logicalChrotchContainer,false,0);
//   G4VPhysicalVolume* phycicalPlate_2 = new G4PVPlacement(zRot,G4ThreeVector(0,0,pos_plate),logicalPlate,"phycicalPlate",logicalChrotchContainer,false,0);
// 
// 
// 
// /*
//   G4Box  *solidbox_middle = new G4Box("solidbox_middle", 40./2, 10./2, 20./2);
//   G4LogicalVolume* logicalBox_middle = new G4LogicalVolume(solidbox_middle, G4Material::GetMaterial("Aluminium"),"logicalBox_middle");
//   G4VPhysicalVolume* phycicalBox_middle = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicalBox_middle,"phycicalBox_middle",logicalChrotchContainer,false,0);
// */
// 
//   double split_angle = 0.03;
// 
//   G4Trd  *solidbox_middle = new G4Trd("solidbox_middle", 40./2, 40./2, 10./2, (10-2*sin(0.03)*(split_length - plate_thickness)), (split_length - plate_thickness)/2);
//   G4LogicalVolume* logicalBox_middle = new G4LogicalVolume(solidbox_middle, G4Material::GetMaterial("Copper"),"logicalBox_middle");
//   G4VPhysicalVolume* phycicalBox_middle = new G4PVPlacement(0,G4ThreeVector(0,0,plate_thickness/2.),logicalBox_middle,"phycicalBox_middle",logicalChrotchContainer,false,0);
// 
// 
//   
// 
//   G4Box *solidSplitContainer = new G4Box("solidSplitContainer", 100./2, 76./2, (split_length - plate_thickness)/2);
//   G4LogicalVolume* logicalSplitContainer = new G4LogicalVolume(solidSplitContainer, G4Material::GetMaterial("Beam"),"logicalSplitContainer");
// 
//   G4RotationMatrix* xRot_split_angle1 = new G4RotationMatrix;  xRot_split_angle1->rotateX(split_angle); //rough estimation: split angle of one beam pipe
//   G4RotationMatrix* xRot_split_angle2 = new G4RotationMatrix;  xRot_split_angle2->rotateX(-1*split_angle); //rough estimation: split angle of one beam pipe  
// 
//   G4VPhysicalVolume* phycicalSplitContainer1 = new G4PVPlacement(xRot_split_angle1,G4ThreeVector(0,43-sin(0.03)*(split_length - plate_thickness),plate_thickness/2.),logicalSplitContainer,"phycicalSplitContainer1",logicalChrotchContainer,false,0);
//   G4VPhysicalVolume* phycicalSplitContainer2 = new G4PVPlacement(xRot_split_angle2,G4ThreeVector(0,-43+sin(0.03)*(split_length - plate_thickness),plate_thickness/2.),logicalSplitContainer,"phycicalSplitContainer2",logicalChrotchContainer,false,0);
// 
// 
// 
//   G4Tubs *solidtub_onefourth = new G4Tubs("solidtub_onfourth", 60./2, 70/2, (split_length - plate_thickness)/2, 0, 3.14159265/2);// 1/4 tube
//   G4Box  *solidbox_side1 = new G4Box("solidbox_side1", 5./2, 6./2, (split_length - plate_thickness)/2);
//   G4Box  *solidbox_side2 = new G4Box("solidbox_side2", 16./2, 5./2, (split_length - plate_thickness)/2);
// 
//   G4LogicalVolume* logicalTub_onefourth = new G4LogicalVolume(solidtub_onefourth, G4Material::GetMaterial("Copper"),"logicalTub_onefourth");
//   G4LogicalVolume* logicalBox_side1 = new G4LogicalVolume(solidbox_side1, G4Material::GetMaterial("Copper"),"logicalBox_side1");
//   G4LogicalVolume* logicalBox_side2 = new G4LogicalVolume(solidbox_side2, G4Material::GetMaterial("Copper"),"logicalBox_side2");
// 
//  
//  
// /*
//   G4UnionSolid* solid_tmp1 = new G4UnionSolid("solid_tmp1",sol5tub_onefourth,solidbox_side1,0,G4ThreeVector(38+5./2,0,0));
//   G4UnionSolid* solid_tmp2 = new G4UnionSolid("solid_tmp2",solid_tmp1,solidtub_onefourth, zRot_90,G4ThreeVector(0,0,0));
//   G4UnionSolid* solid_tmp3 = new G4UnionSolid("solid_tmp3",solid_tmp2,solidbox_side2, 0,G4ThreeVector(0,33+5./2,0));
//   G4UnionSolid* solid_tmp4 = new G4UnionSolid("solid_tmp4",solid_tmp3,solidtub_onefourth, zRot_180,G4ThreeVector(0,0,0));
//   G4UnionSolid* solid_tmp5 = new G4UnionSolid("solid_tmp5",solid_tmp4,solidbox_side1, 0,G4ThreeVector(-38-5./2,0,0));
//   G4UnionSolid* solid_tmp6 = new G4UnionSolid("solid_tmp6",solid_tmp5,solidtub_onefourth, zRot_270,G4ThreeVector(0,0,0));
//   G4UnionSolid* solid_tmp7 = new G4UnionSolid("solid_tmp7",solid_tmp6,solidbox_side2, 0,G4ThreeVector(0, -33-5./2,0));
// 
//   G4LogicalVolume* logicalOut = new G4LogicalVolume(solid_tmp2, G4Material::GetMaterial("Aluminium"),"logicalOut");
// */
//   
// G4VPhysicalVolume*  physicalTub1 = new G4PVPlacement(0,G4ThreeVector(8,3,0),logicalTub_onefourth,"physicalTub1",logicalSplitContainer,false,0);
// G4VPhysicalVolume*  physicalTub2 = new G4PVPlacement(zRot_90,G4ThreeVector(8,-3,0),logicalTub_onefourth,"physicalTub2",logicalSplitContainer,false,0);
// G4VPhysicalVolume*  physicalTub3 = new G4PVPlacement(zRot_180,G4ThreeVector(-8,-3,0),logicalTub_onefourth,"physicalTub3",logicalSplitContainer,false,0);
// G4VPhysicalVolume*  physicalTub4 = new G4PVPlacement(zRot_270,G4ThreeVector(-8,3,0),logicalTub_onefourth,"physicalTub4",logicalSplitContainer,false,0);
// 
// G4VPhysicalVolume*  physicalBox1 = new G4PVPlacement(0,G4ThreeVector(38+5./2,0,0),logicalBox_side1,"physicalBox1",logicalSplitContainer,false,0);
// G4VPhysicalVolume*  physicalBox2 = new G4PVPlacement(0,G4ThreeVector(0, 33+5./2,0),logicalBox_side2,"physicalBox2",logicalSplitContainer,false,0);
// G4VPhysicalVolume*  physicalBox3 = new G4PVPlacement(0,G4ThreeVector(-38-5./2,0,0),logicalBox_side1,"physicalBox3",logicalSplitContainer,false,0);
// G4VPhysicalVolume*  physicalBox4 = new G4PVPlacement(0,G4ThreeVector(0, -33-5./2,0),logicalBox_side2,"physicalBox4",logicalSplitContainer,false,0);
// 
// 
// //the last part is the connection between Y crotch and the following two chambers.
// 
//   G4Box *solidConnectionContainer = new G4Box("solidConnectionContainer", 100./2, 200./2, 20./2);
//   G4LogicalVolume* logicalConnectionContainer = new G4LogicalVolume(solidConnectionContainer, G4Material::GetMaterial("Beam"),"logicalConnectionContainer");
// 
//   G4VPhysicalVolume* phycicalConnectionContainer = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20.+50.+84.+109+split_length + 20./2),logicalConnectionContainer,"phycicalChrotchContainer",logicalYtypeCrotch,false,0);
// 
// 
// 
// 
//   G4Box  *solidbox_middle_connection = new G4Box("solidbox_middle_connection", 40./2, 10./2, 20./2);
//   G4LogicalVolume* logicalBox_middle_connection = new G4LogicalVolume(solidbox_middle_connection, G4Material::GetMaterial("StainlessSteel"),"logicalBox_middle_connection");
//   G4VPhysicalVolume* phycicalBox_middle_connection = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicalBox_middle_connection,"phycicalBox_middle_connection",logicalConnectionContainer,false,0);
// 
// 
// 
// 
//   G4Box *solidConnection_split = new G4Box("solidConnection_split", 100./2, 76./2, 20./2);
//   G4LogicalVolume* logicalConnection_split = new G4LogicalVolume(solidConnection_split, G4Material::GetMaterial("Beam"),"logicalConnection_split");
// 
//   G4VPhysicalVolume* phycicalConnection_split1 = new G4PVPlacement(0,G4ThreeVector(0,43-sin(0.03)* 20, 0.),logicalConnection_split,"phycicalConnection_split1",logicalConnectionContainer,false,0);
// 
//   G4VPhysicalVolume* phycicalConnection_split2 = new G4PVPlacement(0,G4ThreeVector(0,-43+sin(0.03)* 20, 0.),logicalConnection_split,"phycicalConnection_split2",logicalConnectionContainer,false,0);
// 
// 
// 
// 
// 
// 
//   G4Tubs *solidtub_onefourth_connection = new G4Tubs("solidtub_onefourth_connection", 60./2, 70/2, 20./2, 0, 3.14159265/2);// 1/4 tube
//   G4Box  *solidbox_side1_connection = new G4Box("solidbox_side1_connection", 5./2, 6./2, 20./2);
//   G4Box  *solidbox_side2_connection = new G4Box("solidbox_side2_connection", 16./2, 5./2, 20./2);
// 
//   G4LogicalVolume* logicalTub_onefourth_connection = new G4LogicalVolume(solidtub_onefourth_connection, G4Material::GetMaterial("StainlessSteel"),"logicalTub_onefourth_connection");
//   G4LogicalVolume* logicalBox_side1_connection = new G4LogicalVolume(solidbox_side1_connection, G4Material::GetMaterial("StainlessSteel"),"logicalBox_side1_connection");
//   G4LogicalVolume* logicalBox_side2_connection = new G4LogicalVolume(solidbox_side2_connection, G4Material::GetMaterial("StainlessSteel"),"logicalBox_side2_connection");
// 
// G4VPhysicalVolume*  physicalTub1_connection = new G4PVPlacement(0,G4ThreeVector(8,3,0),logicalTub_onefourth_connection,"physicalTub1_connection",logicalConnection_split,false,0);
// G4VPhysicalVolume*  physicalTub2_connection = new G4PVPlacement(zRot_90,G4ThreeVector(8,-3,0),logicalTub_onefourth_connection,"physicalTub2",logicalConnection_split,false,0);
// G4VPhysicalVolume*  physicalTub3_connection = new G4PVPlacement(zRot_180,G4ThreeVector(-8,-3,0),logicalTub_onefourth_connection,"physicalTub3",logicalConnection_split,false,0);
// G4VPhysicalVolume*  physicalTub4_connection = new G4PVPlacement(zRot_270,G4ThreeVector(-8,3,0),logicalTub_onefourth_connection,"physicalTub4",logicalConnection_split,false,0);
// 
// G4VPhysicalVolume*  physicalBox1_connection = new G4PVPlacement(0,G4ThreeVector(38+5./2,0,0),logicalBox_side1_connection,"physicalBox1_connection",logicalConnection_split,false,0);
// G4VPhysicalVolume*  physicalBox2_connection = new G4PVPlacement(0,G4ThreeVector(0, 33+5./2,0),logicalBox_side2_connection,"physicalBox2_connection",logicalConnection_split,false,0);
// G4VPhysicalVolume*  physicalBox3_connection = new G4PVPlacement(0,G4ThreeVector(-38-5./2,0,0),logicalBox_side1_connection,"physicalBox3_connection",logicalConnection_split,false,0);
// G4VPhysicalVolume*  physicalBox4_connection = new G4PVPlacement(0,G4ThreeVector(0, -33-5./2,0),logicalBox_side2_connection,"physicalBox4_connection",logicalConnection_split,false,0);*/
// 
// 
// 
// 



//   logicalYtypeCrotch->SetVisAttributes(G4VisAttributes::Invisible);
//   logicalSplitContainer->SetVisAttributes(G4VisAttributes::Invisible);
//   logicalChrotchContainer->SetVisAttributes(G4VisAttributes::Invisible);
//   logicalConnectionContainer->SetVisAttributes(G4VisAttributes::Invisible);
//   logicalConnection_split->SetVisAttributes(G4VisAttributes::Invisible);

//   G4VisAttributes* visPip = new G4VisAttributes(G4Colour(209./255,196./255, 33./255));
  G4VisAttributes* visSteel = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  G4VisAttributes* visWater = new G4VisAttributes(G4Colour(0., 1., 1.));
  G4VisAttributes* visCopper = new G4VisAttributes(G4Colour(146./255, 83./255, 48./255));
//   G4VisAttributes* vissplit = new G4VisAttributes(G4Colour(209./255,196./255, 33./255));  

//   logicaltub1->SetVisAttributes(visSteel);  //CF110
//   logicaltub2->SetVisAttributes(visSteel);  //CF110
//   logicaltub3->SetVisAttributes(visSteel);  



//   logicalPlate->SetVisAttributes(visCopper);
//   logicalCons->SetVisAttributes(visCopper);
//   logicalTub_onefourth->SetVisAttributes(visCopper);
//   logicalBox_side1->SetVisAttributes(visCopper);
//   logicalBox_side2->SetVisAttributes(visCopper);
//   logicalBox_middle->SetVisAttributes(visCopper);
//   
//   logicalBox_side1_connection->SetVisAttributes(visSteel);
//   logicalBox_side2_connection->SetVisAttributes(visSteel);
//   logicalBox_middle_connection->SetVisAttributes(visSteel);
//   logicalTub_onefourth_connection->SetVisAttributes(visSteel);

  
  // NEW STUFF
  
  G4bool endplate = true;
  G4bool copper_outerring = true;
  G4bool copper_screen = true;
  G4bool copper_innerring = true;
  G4bool copper_tiltedpipe = true;
  G4bool copper_straightpipe = true;
  G4bool copper_betweenpipes = true;
  G4bool water = true;
  G4bool copper_cover = true;
  G4bool bellowpart = true;
  
  
  //the logical volume of beam pipe
  //length: 335mm;
  G4Tubs* solidCrotchWorld = new G4Tubs("solidCrotchWorld",0,300./2.,335./2.,0,360);
//   G4Tubs* solidCrotchWorld = new G4Tubs("solidCrotchWorld",0,300.,335.,0,2*M_PI);
  logicalCrotchWorld = new G4LogicalVolume(solidCrotchWorld, G4Material::GetMaterial("Beam"),"logicalCrotchWorld");
  
  
//   G4Tubs* dummytube = new G4Tubs("solidtube1", 110./2, 165/2, 20./2, 0, 360); ;// one side of the SF110   thickness=20 ???? check this!!!!!!!!1-----
//   G4LogicalVolume*  logicaldummytube = new G4LogicalVolume(dummytube, G4Material::GetMaterial("StainlessSteel"),"logicaldummytube");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
//   G4VPhysicalVolume *phycicaldummytube = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicaldummytube,"phycicaldummytube",logicalCrotchWorld,false,0);
  
  // ENDPLATE
  // OUTER PLATE
//   G4Tubs* solid_endplate_t1 = new G4Tubs("solid_endplate_t1", 0, 56, 7.5, 0, M_PI/2.);
//   G4Box* solid_endplate_hor = new G4Box("solid_endplate_hor", 100, 7, 7.5);
//   G4Box* solid_endplate_ver = new G4Box("solid_endplate_vert", 44, 63, 7.5);
//   
//   G4RotationMatrix* zRot = new G4RotationMatrix;
//   
//   G4UnionSolid* solid_endplate_outer = new G4UnionSolid("solid_endplate_outer", solid_endplate_hor, solid_endplate_ver);
//   solid_endplate_outer = new G4UnionSolid("solid_endplate_outer", solid_endplate_outer, solid_endplate_t1, zRot, G4ThreeVector(44, 7, 0));
//   zRot->rotateZ(-M_PI/2.*rad);
//   solid_endplate_outer = new G4UnionSolid("solid_endplate_outer", solid_endplate_outer, solid_endplate_t1, zRot, G4ThreeVector(-44, 7, 0));
//   zRot->rotateZ(-M_PI/2.*rad);
//   solid_endplate_outer = new G4UnionSolid("solid_endplate_outer", solid_endplate_outer, solid_endplate_t1, zRot, G4ThreeVector(-44, -7, 0));
//   zRot->rotateZ(-M_PI/2.*rad);
//   solid_endplate_outer = new G4UnionSolid("solid_endplate_outer", solid_endplate_outer, solid_endplate_t1, zRot, G4ThreeVector(44, -7, 0));
//   
//   // INNER HOLES
//   G4Tubs* solid_endplate_inner_t1 = new G4Tubs("solid_endplate_inner_t1", 0, 30, 10, 0, M_PI/2.);
//   G4Box* solid_endplate_inner_hor = new G4Box("solid_endplate_inner_hor", 31, 7, 10);
//   G4Box* solid_endplate_inner_ver = new G4Box("solid_endplate_inner_vert", 1, 37, 10);
//   
//   G4UnionSolid* solid_endplate_inner = new G4UnionSolid("solid_endplate_inner", solid_endplate_inner_hor, solid_endplate_inner_ver);
//   zRot->rotateZ(-M_PI/2.*rad);
//   solid_endplate_inner = new G4UnionSolid("solid_endplate_inner", solid_endplate_inner, solid_endplate_inner_t1, zRot, G4ThreeVector(1, 7, 0));
//   zRot->rotateZ(-M_PI/2.*rad);
//   solid_endplate_inner = new G4UnionSolid("solid_endplate_inner", solid_endplate_inner, solid_endplate_inner_t1, zRot, G4ThreeVector(-1, 7, 0));
//   zRot->rotateZ(-M_PI/2.*rad);
//   solid_endplate_inner = new G4UnionSolid("solid_endplate_inner", solid_endplate_inner, solid_endplate_inner_t1, zRot, G4ThreeVector(-1, -7, 0));
//   zRot->rotateZ(-M_PI/2.*rad);
//   solid_endplate_inner = new G4UnionSolid("solid_endplate_inner", solid_endplate_inner, solid_endplate_inner_t1, zRot, G4ThreeVector(1, -7, 0));
//   
//   G4SubtractionSolid* solid_endplate = new G4SubtractionSolid("solid_endplate", solid_endplate_outer, solid_endplate_inner);
//   
//   
//   G4LogicalVolume* logical_endplate_outer = new G4LogicalVolume(solid_endplate_inner, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_outer");
//   G4VPhysicalVolume *phycical_endplate_outer = new G4PVPlacement(0,G4ThreeVector(0,0,0),logical_endplate_outer,"phycical_endplate_outer",logicalCrotchWorld,false,0);
  
//   G4LogicalVolume* logical_endplate = new G4LogicalVolume(solid_endplate, G4Material::GetMaterial("StainlessSteel"),"logical_endplate");
//   G4VPhysicalVolume *phycical_endplate = new G4PVPlacement(0,G4ThreeVector(0,0,0),logical_endplate,"phycical_endplate",logicalCrotchWorld,false,0);
  const G4double endplate_anchor_z = 335.0/2.;
  
  
  if (endplate) {
  // ENDPLATE
  // DRILL HOLE
  G4Tubs* solid_endplate_drillhole = new G4Tubs("solid_endplate_drillhole", 0, 4, 10, 0, 2*M_PI);
  
  // ENDPLATE ONE HOLE
  const G4double endplate_onehole_anchor_z = endplate_anchor_z - 11.25;
  // SQUARE PART
  G4Box* solid_endplate_onehole_box = new G4Box("solid_endplate_onehole_box", 11.5, 7, 3.75);
  G4SubtractionSolid* solid_endplate_onehole_square = new G4SubtractionSolid("solid_endplate_onehole_square", solid_endplate_onehole_box, solid_endplate_drillhole, 0, G4ThreeVector(-1.5, 0, 0));
  
  G4LogicalVolume* logical_endplate_onehole_square = new G4LogicalVolume(solid_endplate_onehole_square, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_onehole_square");
  logical_endplate_onehole_square->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_onehole_square1 = new G4PVPlacement(0, G4ThreeVector(-88.5, 0, endplate_onehole_anchor_z), logical_endplate_onehole_square, "phycical_endplate_onehole_square1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_onehole_square2 = new G4PVPlacement(zRot_180, G4ThreeVector(88.5, 0, endplate_onehole_anchor_z), logical_endplate_onehole_square, "phycical_endplate_onehole_square2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // ARC PART
  G4Tubs* solid_endplate_onehole_tubs = new G4Tubs("solid_endplate_onehole_tubs", 33, 56, 3.75, M_PI/2., M_PI/2.);
  G4SubtractionSolid* solid_endplate_onehole_arc = new G4SubtractionSolid("solid_endplate_onehole_arc", solid_endplate_onehole_tubs, solid_endplate_drillhole, 0, G4ThreeVector(-sin(12.5/180.*M_PI) *46, cos(12.5/180.*M_PI) *46, 0));
  solid_endplate_onehole_arc = new G4SubtractionSolid("solid_endplate_onehole_arc", solid_endplate_onehole_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(42.5/180.*M_PI) *46, cos(42.5/180.*M_PI) *46, 0));
  solid_endplate_onehole_arc = new G4SubtractionSolid("solid_endplate_onehole_arc", solid_endplate_onehole_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(72.5/180.*M_PI) *46, cos(72.5/180.*M_PI) *46, 0));
  
  G4LogicalVolume* logical_endplate_onehole_arc = new G4LogicalVolume(solid_endplate_onehole_arc, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_onehole_arc");
  logical_endplate_onehole_arc->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_onehole_arc1 = new G4PVPlacement(0, G4ThreeVector(-44, 7, endplate_onehole_anchor_z), logical_endplate_onehole_arc, "phycical_endplate_onehole_arc1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_onehole_arc2 = new G4PVPlacement(xRot_180, G4ThreeVector(-44, -7, endplate_onehole_anchor_z), logical_endplate_onehole_arc, "phycical_endplate_onehole_arc2", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_onehole_arc3 = new G4PVPlacement(yRot_180, G4ThreeVector(44, 7, endplate_onehole_anchor_z), logical_endplate_onehole_arc, "phycical_endplate_onehole_arc3", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_onehole_arc4 = new G4PVPlacement(xyRot_180, G4ThreeVector(44, -7, endplate_onehole_anchor_z), logical_endplate_onehole_arc, "phycical_endplate_onehole_arc4", logicalCrotchWorld, false, 0, checkoverlap);
  
  // TSHAPE PART
  G4Box* solid_endplate_onehole_topbox = new G4Box("solid_endplate_onehole_topbox", 42, 21.5, 3.75);
  G4Tubs* solid_endplate_onehole_subarc = new G4Tubs("solid_endplate_onehole_subarc", 0, 33, 10, 0., M_PI);
  G4SubtractionSolid* solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_topbox, solid_endplate_onehole_subarc, 0,  G4ThreeVector(42, -34.5, 0));
  solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_onehole_subarc, 0,  G4ThreeVector(-42, -34.5, 0));
  solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42 + 46*sin(12.5/180.*M_PI), -34.5 + 46*cos(12.5/180.*M_PI), 0));
  solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42 + 46*sin(37.5/180.*M_PI), -34.5 + 46*cos(37.5/180.*M_PI), 0));
  solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42 - 46*sin(12.5/180.*M_PI), -34.5 + 46*cos(12.5/180.*M_PI), 0));
  solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42 - 46*sin(37.5/180.*M_PI), -34.5 + 46*cos(37.5/180.*M_PI), 0));
  solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(0., -11.5, 0));
  
  G4LogicalVolume* logical_endplate_onehole_tshape = new G4LogicalVolume(solid_endplate_onehole_tshape, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_onehole_tshape");
  logical_endplate_onehole_tshape->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_onehole_tshape1 = new G4PVPlacement(0, G4ThreeVector(0, 41.5, endplate_onehole_anchor_z), logical_endplate_onehole_tshape, "phycical_endplate_onehole_tshape1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_onehole_tshape2 = new G4PVPlacement(xRot_180, G4ThreeVector(0, -41.5, endplate_onehole_anchor_z), logical_endplate_onehole_tshape, "phycical_endplate_onehole_tshape2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // FILLBOX
  G4Box* solid_endplate_onehole_fillbox = new G4Box("solid_endplate_onehole_fillbox", 1, 11.5, 3.75);
  
  G4LogicalVolume* logical_endplate_onehole_fillbox = new G4LogicalVolume(solid_endplate_onehole_fillbox, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_onehole_fillbox");
  logical_endplate_onehole_fillbox->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *physical_endplate_onehole_fillbox1 = new G4PVPlacement(0, G4ThreeVector(-43, 51.5, endplate_onehole_anchor_z), logical_endplate_onehole_fillbox, "physical_endplate_onehole_fillbox1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_onehole_fillbox2 = new G4PVPlacement(0, G4ThreeVector(43, 51.5, endplate_onehole_anchor_z), logical_endplate_onehole_fillbox, "physical_endplate_onehole_fillbox2", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_onehole_fillbox3 = new G4PVPlacement(0, G4ThreeVector(43, -51.5, endplate_onehole_anchor_z), logical_endplate_onehole_fillbox, "physical_endplate_onehole_fillbox3", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_onehole_fillbox4 = new G4PVPlacement(0, G4ThreeVector(-43, -51.5, endplate_onehole_anchor_z), logical_endplate_onehole_fillbox, "physical_endplate_onehole_fillbox4", logicalCrotchWorld, false, 0, checkoverlap);
  
  
  
  // ENDPLATE TWO HOLES
  const G4double endplate_twohole_anchor_z = endplate_anchor_z -3.75;
  // SQUARE PART
  G4Box* solid_endplate_twoholes_box = new G4Box("solid_endplate_twoholes_box", 13, 7, 3.75);
  G4SubtractionSolid* solid_endplate_twoholes_square = new G4SubtractionSolid("solid_endplate_twoholes_square", solid_endplate_twoholes_box, solid_endplate_drillhole, 0, G4ThreeVector(-3, 0, 0));
  
  G4LogicalVolume* logical_endplate_twoholes_square = new G4LogicalVolume(solid_endplate_twoholes_square, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_square");
  logical_endplate_twoholes_square->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_twoholes_square1 = new G4PVPlacement(0, G4ThreeVector(-87, 0, endplate_twohole_anchor_z), logical_endplate_twoholes_square, "phycical_endplate_twoholes_square1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_square2 = new G4PVPlacement(zRot_180, G4ThreeVector(87, 0, endplate_twohole_anchor_z), logical_endplate_twoholes_square, "phycical_endplate_twoholes_square2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // ARC PART
  G4Tubs* solid_endplate_twoholes_tubs = new G4Tubs("solid_endplate_twoholes_tubs", 30, 56, 3.75, M_PI/2., M_PI/2.);
  G4SubtractionSolid* solid_endplate_twoholes_arc = new G4SubtractionSolid("solid_endplate_twoholes_arc", solid_endplate_twoholes_tubs, solid_endplate_drillhole, 0, G4ThreeVector(-sin(12.5/180.*M_PI) *46, cos(12.5/180.*M_PI) *46, 0));
  solid_endplate_twoholes_arc = new G4SubtractionSolid("solid_endplate_twoholes_arc", solid_endplate_twoholes_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(42.5/180.*M_PI) *46, cos(42.5/180.*M_PI) *46, 0));
  solid_endplate_twoholes_arc = new G4SubtractionSolid("solid_endplate_twoholes_arc", solid_endplate_twoholes_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(72.5/180.*M_PI) *46, cos(72.5/180.*M_PI) *46, 0));
  
  G4LogicalVolume* logical_endplate_twoholes_arc = new G4LogicalVolume(solid_endplate_twoholes_arc, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_arc");
  logical_endplate_twoholes_arc->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_twoholes_arc1 = new G4PVPlacement(0, G4ThreeVector(-44, 7, endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_arc2 = new G4PVPlacement(xRot_180, G4ThreeVector(-44, -7, endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc2", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_arc3 = new G4PVPlacement(yRot_180, G4ThreeVector(44, 7, endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc3", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_arc4 = new G4PVPlacement(xyRot_180, G4ThreeVector(44, -7, endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc4", logicalCrotchWorld, false, 0, checkoverlap);
  
  // TSHAPE PART
  G4Box* solid_endplate_twoholes_topbox = new G4Box("solid_endplate_twoholes_topbox", 42, 28, 3.75);
  G4Tubs* solid_endplate_twoholes_subarc = new G4Tubs("solid_endplate_twoholes_subarc", 0, 30, 10, 0., 2*M_PI);
  G4SubtractionSolid* solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_topbox, solid_endplate_twoholes_subarc, 0,  G4ThreeVector(42, -28, 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_twoholes_subarc, 0,  G4ThreeVector(-42, -28, 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42 + 46*sin(12.5/180.*M_PI), -28 + 46*cos(12.5/180.*M_PI), 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42 + 46*sin(37.5/180.*M_PI), -28 + 46*cos(37.5/180.*M_PI), 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42 - 46*sin(12.5/180.*M_PI), -28 + 46*cos(12.5/180.*M_PI), 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42 - 46*sin(37.5/180.*M_PI), -28 + 46*cos(37.5/180.*M_PI), 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(0., -5, 0));
  
  G4LogicalVolume* logical_endplate_twoholes_tshape = new G4LogicalVolume(solid_endplate_twoholes_tshape, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_tshape");
  logical_endplate_twoholes_tshape->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_twoholes_tshape1 = new G4PVPlacement(0, G4ThreeVector(0, 35, endplate_twohole_anchor_z), logical_endplate_twoholes_tshape, "phycical_endplate_twoholes_tshape1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_tshape2 = new G4PVPlacement(xRot_180, G4ThreeVector(0, -35, endplate_twohole_anchor_z), logical_endplate_twoholes_tshape, "phycical_endplate_twoholes_tshape2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // FILLBOX
  G4Box* solid_endplate_twoholes_fillbox = new G4Box("solid_endplate_twoholes_fillbox", 1, 13, 3.75);
  
  G4LogicalVolume* logical_endplate_twoholes_fillbox = new G4LogicalVolume(solid_endplate_twoholes_fillbox, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_fillbox");
  logical_endplate_twoholes_fillbox->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox1 = new G4PVPlacement(0, G4ThreeVector(-43, 50, endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox2 = new G4PVPlacement(0, G4ThreeVector(43, 50, endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox2", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox3 = new G4PVPlacement(0, G4ThreeVector(43, -50, endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox3", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox4 = new G4PVPlacement(0, G4ThreeVector(-43, -50, endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox4", logicalCrotchWorld, false, 0, checkoverlap);
  
  // CENTERBOX
  G4Box* solid_endplate_twoholes_centerbox = new G4Box("solid_endplate_twoholes_centerbox", 12, 7, 3.75);
  
  G4LogicalVolume* logical_endplate_twoholes_centerbox = new G4LogicalVolume(solid_endplate_twoholes_centerbox, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_centerbox");
  logical_endplate_twoholes_centerbox->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *physical_endplate_twoholes_centerbox = new G4PVPlacement(0, G4ThreeVector(0, 0, endplate_twohole_anchor_z), logical_endplate_twoholes_centerbox, "physical_endplate_twoholes_centerbox", logicalCrotchWorld, false, 0, checkoverlap);
  }
  
  // COPPER
  // OUTER RING
  // BASE
  const G4double copper_outerring_anchor = endplate_anchor_z - 200.5;
  
  if (copper_outerring) {
  G4Tubs* soild_copper_outerring_base = new G4Tubs("soild_copper_outerring_base", 63., 72., 1., 0., 2*M_PI);
  
  G4LogicalVolume* logical_copper_outerring_base = new G4LogicalVolume(soild_copper_outerring_base, G4Material::GetMaterial("Copper"),"logical_copper_outerring_base");
  logical_copper_outerring_base->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_outerring_base = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 1.), logical_copper_outerring_base, "physical_copper_outerring_base", logicalCrotchWorld, false, 0, checkoverlap);
  
  // FIRST RING
  G4Tubs* soild_copper_outerring_firstring = new G4Tubs("soild_copper_outerring_firstring", 60., 75., 2.5, 0., 2*M_PI);
  
  G4LogicalVolume* logical_copper_outerring_firstring = new G4LogicalVolume(soild_copper_outerring_firstring, G4Material::GetMaterial("Copper"),"logical_copper_outerring_firstring");
  logical_copper_outerring_firstring->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_outerring_firstring = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 4.5), logical_copper_outerring_firstring, "physical_copper_outerring_firstring", logicalCrotchWorld, false, 0, checkoverlap);
  
  // MIDDLE RING
  G4Tubs* soild_copper_outerring_middlering = new G4Tubs("soild_copper_outerring_middlering", 58., 65., 3, 0., 2*M_PI);
  
  G4LogicalVolume* logical_copper_outerring_middlering = new G4LogicalVolume(soild_copper_outerring_middlering, G4Material::GetMaterial("Copper"),"logical_copper_outerring_middlering");
  logical_copper_outerring_middlering->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_outerring_middlering = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 10.), logical_copper_outerring_middlering, "physical_copper_outerring_middlering", logicalCrotchWorld, false, 0, checkoverlap);
  
  // MIDDLE RING DIVIDER
  G4Tubs* soild_copper_outerring_divider = new G4Tubs("soild_copper_outerring_divider", 65., 75., 3, M_PI/2. - 0.01273277/2., 0.01273277);
  
  G4LogicalVolume* logical_copper_outerring_divider = new G4LogicalVolume(soild_copper_outerring_divider, G4Material::GetMaterial("Copper"),"logical_copper_outerring_divider");
  logical_copper_outerring_divider->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_outerring_divider = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 10.), logical_copper_outerring_divider, "physical_copper_outerring_divider", logicalCrotchWorld, false, 0, checkoverlap);
  
  // SECOND RING
  G4Tubs* soild_copper_outerring_secondring = new G4Tubs("soild_copper_outerring_secondring", 58., 75., 2, 0., 2*M_PI);
  
  G4LogicalVolume* logical_copper_outerring_secondring = new G4LogicalVolume(soild_copper_outerring_secondring, G4Material::GetMaterial("Copper"),"logical_copper_outerring_secondring");
  logical_copper_outerring_secondring->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_outerring_secondring = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 15.), logical_copper_outerring_secondring, "physical_copper_outerring_secondring", logicalCrotchWorld, false, 0, checkoverlap);
  
  // LAST RING
  G4Tubs* soild_copper_outerring_lastring = new G4Tubs("soild_copper_outerring_lastring", 58., 77., 1, 0., 2*M_PI);
  
  G4LogicalVolume* logical_copper_outerring_lastring = new G4LogicalVolume(soild_copper_outerring_lastring, G4Material::GetMaterial("Copper"),"logical_copper_outerring_lastring");
  logical_copper_outerring_lastring->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_outerring_lastring = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 18.), logical_copper_outerring_lastring, "physical_copper_outerring_lastring", logicalCrotchWorld, false, 0, checkoverlap);
  
  // VERY LAST RING
  G4Tubs* soild_copper_outerring_verylastring = new G4Tubs("soild_copper_outerring_verylastring", 58., 64., 1, 0., 2*M_PI);
  
  G4LogicalVolume* logical_copper_outerring_verylastring = new G4LogicalVolume(soild_copper_outerring_verylastring, G4Material::GetMaterial("Copper"),"logical_copper_outerring_verylastring");
  logical_copper_outerring_verylastring->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_outerring_verylastring = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 20.), logical_copper_outerring_verylastring, "physical_copper_outerring_verylastring", logicalCrotchWorld, false, 0, checkoverlap);
  }
  
  // SUBTRACTION PIPE
  // HORIZONTAL BOX
  G4Para* solid_copper_sub_horbox = new G4Para("solid_copper_sub_horbox",31.,7.,200,0,-atan((86-51.26)/(2.*193.)),0.);
  G4Para* solid_copper_sub_vertbox = new G4Para("solid_copper_sub_vertbox",1.,37.,200,0,-atan((86-51.26)/(2.*193.)),0.);
  G4EllipticalTube* solid_copper_subtube = new G4EllipticalTube("solid_copper_subtube", cos(atan((86-51.26)/(2.*193.))) *30.,30.,200.);
  
  G4RotationMatrix* yRot_split = new G4RotationMatrix; yRot_split->rotateY(atan((86-51.26)/(2.*193.)));
  G4RotationMatrix* yRot_min_split = new G4RotationMatrix; yRot_min_split->rotateY(-atan((86-51.26)/(2.*193.)));
  
  if (copper_screen) {
  // SCREEN
  // CONE
  G4Cons* solid_copper_screen_cone = new G4Cons("solid_copper_screen_cone", 57.82, 61.82, 38.62, 71, 50, M_PI/2., M_PI);
  G4Box* solid_copper_screen_subbox = new G4Box("solid_copper_screen_subbox", 100, 50, 100);
  G4SubtractionSolid* solid_copper_screen_cut = new G4SubtractionSolid("solid_copper_screen_cut", solid_copper_screen_cone, solid_copper_screen_subbox, xRot_min10, G4ThreeVector(0, 104.5, 0));
  solid_copper_screen_cut = new G4SubtractionSolid("solid_copper_screen_cut", solid_copper_screen_cut, solid_copper_screen_subbox, xRot_10, G4ThreeVector(0, -104.5, 0));
  G4Tubs* solid_copper_screen_hole = new G4Tubs("solid_copper_screen_hole", 0., 2.5, 100., 0., 2*M_PI);
  for (int index_rot = 0; index_rot < 25; index_rot++) {
      G4RotationMatrix rot_screen;
      rot_screen.rotateZ(index_rot*M_PI/25.);
      rot_screen.rotateX(M_PI/2.);
      for (int index_trans = 0; index_trans < 14; index_trans++) {
	    solid_copper_screen_cut = new G4SubtractionSolid("solid_copper_screen_cut", solid_copper_screen_cut, solid_copper_screen_hole, &rot_screen, G4ThreeVector(0, 0, -45.5 + index_trans*7.));
      }
  }
  solid_copper_screen_cut = new G4SubtractionSolid("solid_copper_screen_cut", solid_copper_screen_cut, solid_copper_subtube, yRot_split, G4ThreeVector(-44., 7., 122.));
  solid_copper_screen_cut = new G4SubtractionSolid("solid_copper_screen_cut", solid_copper_screen_cut, solid_copper_subtube, yRot_split, G4ThreeVector(-42., 7., 122.));
  solid_copper_screen_cut = new G4SubtractionSolid("solid_copper_screen_cut", solid_copper_screen_cut, solid_copper_subtube, yRot_split, G4ThreeVector(-44., -7., 122.));
  solid_copper_screen_cut = new G4SubtractionSolid("solid_copper_screen_cut", solid_copper_screen_cut, solid_copper_subtube, yRot_split, G4ThreeVector(-42., -7., 122.));
  solid_copper_screen_cut = new G4SubtractionSolid("solid_copper_screen_cut", solid_copper_screen_cut, solid_copper_sub_horbox, 0, G4ThreeVector(-43., 0., 122.));
  solid_copper_screen_cut = new G4SubtractionSolid("solid_copper_screen_cut", solid_copper_screen_cut, solid_copper_sub_vertbox, 0, G4ThreeVector(-43., 0., 122.));
  
  G4LogicalVolume* logical_copper_screen_cut = new G4LogicalVolume(solid_copper_screen_cut, G4Material::GetMaterial("Copper"),"logical_copper_screen_cut");
  logical_copper_screen_cut->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_screen_cut1 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 71.), logical_copper_screen_cut, "physical_copper_screen_cut1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_screen_cut2 = new G4PVPlacement(zRot_180, G4ThreeVector(0., 0., copper_outerring_anchor + 71.), logical_copper_screen_cut, "physical_copper_screen_cut2", logicalCrotchWorld, false, 0, checkoverlap);
  }
  
  if (copper_innerring) {
  // INNER RING
  // BASE
  G4Tubs* solid_copper_innerring_basem = new G4Tubs("solid_copper_innerring_basem", 38.62, 77, 1, M_PI/2., M_PI);
  G4SubtractionSolid* solid_copper_innerring_base = new G4SubtractionSolid("solid_copper_innerring_base", solid_copper_innerring_basem, solid_copper_subtube, yRot_split, G4ThreeVector(-44., 7., 71.));
  solid_copper_innerring_base = new G4SubtractionSolid("solid_copper_innerring_base", solid_copper_innerring_base, solid_copper_subtube, yRot_split, G4ThreeVector(-42., 7., 71.));
  solid_copper_innerring_base = new G4SubtractionSolid("solid_copper_innerring_base", solid_copper_innerring_base, solid_copper_subtube, yRot_split, G4ThreeVector(-44., -7., 71.));
  solid_copper_innerring_base = new G4SubtractionSolid("solid_copper_innerring_base", solid_copper_innerring_base, solid_copper_subtube, yRot_split, G4ThreeVector(-42., -7., 71.));
  solid_copper_innerring_base = new G4SubtractionSolid("solid_copper_innerring_base", solid_copper_innerring_base, solid_copper_sub_horbox, 0, G4ThreeVector(-43., 0., 71.));
  solid_copper_innerring_base = new G4SubtractionSolid("solid_copper_innerring_base", solid_copper_innerring_base, solid_copper_sub_vertbox, 0, G4ThreeVector(-43., 0., 71.));
  
  G4LogicalVolume* logical_copper_innerring_base = new G4LogicalVolume(solid_copper_innerring_base, G4Material::GetMaterial("Copper"),"logical_copper_innerring_base");
  logical_copper_innerring_base->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_innerring_base1 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 122.), logical_copper_innerring_base, "physical_copper_innerring_base1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_innerring_base2 = new G4PVPlacement(zRot_180, G4ThreeVector(0., 0., copper_outerring_anchor + 122.), logical_copper_innerring_base, "physical_copper_innerring_base2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // FIRST RING
  G4Tubs* solid_copper_innerring_firstringm = new G4Tubs("solid_copper_innerring_firstringm", 0, 80, 2, M_PI/2., M_PI);
  G4SubtractionSolid* solid_copper_innerring_firstring = new G4SubtractionSolid("solid_copper_innerring_firstring", solid_copper_innerring_firstringm, solid_copper_subtube, yRot_split, G4ThreeVector(-44., 7., 68.));
  solid_copper_innerring_firstring = new G4SubtractionSolid("solid_copper_innerring_firstring", solid_copper_innerring_firstring, solid_copper_subtube, yRot_split, G4ThreeVector(-42., 7., 68.));
  solid_copper_innerring_firstring = new G4SubtractionSolid("solid_copper_innerring_firstring", solid_copper_innerring_firstring, solid_copper_subtube, yRot_split, G4ThreeVector(-44., -7., 68.));
  solid_copper_innerring_firstring = new G4SubtractionSolid("solid_copper_innerring_firstring", solid_copper_innerring_firstring, solid_copper_subtube, yRot_split, G4ThreeVector(-42., -7., 68.));
  solid_copper_innerring_firstring = new G4SubtractionSolid("solid_copper_innerring_firstring", solid_copper_innerring_firstring, solid_copper_sub_horbox, 0, G4ThreeVector(-43., 0., 68.));
  solid_copper_innerring_firstring = new G4SubtractionSolid("solid_copper_innerring_firstring", solid_copper_innerring_firstring, solid_copper_sub_vertbox, 0, G4ThreeVector(-43., 0., 68.));
  
  G4LogicalVolume* logical_copper_innerring_firstring = new G4LogicalVolume(solid_copper_innerring_firstring, G4Material::GetMaterial("Copper"),"logical_copper_innerring_firstring");
  logical_copper_innerring_firstring->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_innerring_firstring1 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 125.), logical_copper_innerring_firstring, "physical_copper_innerring_firstring1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_innerring_firstring2 = new G4PVPlacement(zRot_180, G4ThreeVector(0., 0., copper_outerring_anchor + 125.), logical_copper_innerring_firstring, "physical_copper_innerring_firstring2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // MIDDLE RING
  G4Tubs* solid_copper_innerring_middleringm = new G4Tubs("solid_copper_innerring_middleringm", 0, 71, 3, M_PI/2., M_PI);
  G4SubtractionSolid* solid_copper_innerring_middlering = new G4SubtractionSolid("solid_copper_innerring_middlering", solid_copper_innerring_middleringm, solid_copper_subtube, yRot_split, G4ThreeVector(-44., 7., 63.));
  solid_copper_innerring_middlering = new G4SubtractionSolid("solid_copper_innerring_middlering", solid_copper_innerring_middlering, solid_copper_subtube, yRot_split, G4ThreeVector(-42., 7., 63.));
  solid_copper_innerring_middlering = new G4SubtractionSolid("solid_copper_innerring_middlering", solid_copper_innerring_middlering, solid_copper_subtube, yRot_split, G4ThreeVector(-44., -7., 63.));
  solid_copper_innerring_middlering = new G4SubtractionSolid("solid_copper_innerring_middlering", solid_copper_innerring_middlering, solid_copper_subtube, yRot_split, G4ThreeVector(-42., -7., 63.));
  solid_copper_innerring_middlering = new G4SubtractionSolid("solid_copper_innerring_middlering", solid_copper_innerring_middlering, solid_copper_sub_horbox, 0, G4ThreeVector(-43., 0., 63.));
  solid_copper_innerring_middlering = new G4SubtractionSolid("solid_copper_innerring_middlering", solid_copper_innerring_middlering, solid_copper_sub_vertbox, 0, G4ThreeVector(-43., 0., 63.));
  
  G4LogicalVolume* logical_copper_innerring_middlering = new G4LogicalVolume(solid_copper_innerring_middlering, G4Material::GetMaterial("Copper"),"logical_copper_innerring_middlering");
  logical_copper_innerring_middlering->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_innerring_middlering1 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 130.), logical_copper_innerring_middlering, "physical_copper_innerring_middlering1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_innerring_middlering2 = new G4PVPlacement(zRot_180, G4ThreeVector(0., 0., copper_outerring_anchor + 130.), logical_copper_innerring_middlering, "physical_copper_innerring_middlering2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // MIDDLE RING DIVIDER
  G4Tubs* soild_copper_innerring_divider = new G4Tubs("soild_copper_innerring_divider", 71., 80., 3, M_PI/2. - 0.01273277/2., 0.01273277);
  
  G4LogicalVolume* logical_copper_innerring_divider = new G4LogicalVolume(soild_copper_innerring_divider, G4Material::GetMaterial("Copper"),"logical_copper_innerring_divider");
  logical_copper_innerring_divider->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_innerring_divider = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 130.), logical_copper_innerring_divider, "physical_copper_innerring_divider", logicalCrotchWorld, false, 0, checkoverlap);
  
  // LAST RING
  G4Tubs* solid_copper_innerring_lastringm = new G4Tubs("solid_copper_innerring_lastringm", 0, 80, 2, M_PI/2., M_PI);
  G4SubtractionSolid* solid_copper_innerring_lastring = new G4SubtractionSolid("solid_copper_innerring_lastring", solid_copper_innerring_lastringm, solid_copper_subtube, yRot_split, G4ThreeVector(-44., 7., 58.));
  solid_copper_innerring_lastring = new G4SubtractionSolid("solid_copper_innerring_lastring", solid_copper_innerring_lastring, solid_copper_subtube, yRot_split, G4ThreeVector(-42., 7., 58.));
  solid_copper_innerring_lastring = new G4SubtractionSolid("solid_copper_innerring_lastring", solid_copper_innerring_lastring, solid_copper_subtube, yRot_split, G4ThreeVector(-44., -7., 58.));
  solid_copper_innerring_lastring = new G4SubtractionSolid("solid_copper_innerring_lastring", solid_copper_innerring_lastring, solid_copper_subtube, yRot_split, G4ThreeVector(-42., -7., 58.));
  solid_copper_innerring_lastring = new G4SubtractionSolid("solid_copper_innerring_lastring", solid_copper_innerring_lastring, solid_copper_sub_horbox, 0, G4ThreeVector(-43., 0., 58));
  solid_copper_innerring_lastring = new G4SubtractionSolid("solid_copper_innerring_lastring", solid_copper_innerring_lastring, solid_copper_sub_vertbox, 0, G4ThreeVector(-43., 0., 58));
  
  G4LogicalVolume* logical_copper_innerring_lastring = new G4LogicalVolume(solid_copper_innerring_lastring, G4Material::GetMaterial("Copper"),"logical_copper_innerring_lastring");
  logical_copper_innerring_lastring->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_innerring_lastring1 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 135.), logical_copper_innerring_lastring, "physical_copper_innerring_lastring1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_innerring_lastring2 = new G4PVPlacement(zRot_180, G4ThreeVector(0., 0., copper_outerring_anchor + 135.), logical_copper_innerring_lastring, "physical_copper_innerring_lastring2", logicalCrotchWorld, false, 0, checkoverlap);
  }
  
  if (copper_tiltedpipe) {
  // TILTED PIPE
  // INTERSECTION TUBE
  G4Tubs* solid_copper_tiltedpipe_intertube = new G4Tubs("solid_copper_tiltedpipe_intertube", 0., 82., 28., -M_PI/2., M_PI);
  // EXTRUSION
  std::vector<G4TwoVector> polygon;
  //  {{0., 29.5864}, {21.501, 42.}, {40.2575, 42.}, {83.5587, 17.}, {83.5587, -17.}, {40.2575, -42.}, {21.501, -42.}, {0., -29.5864}}
  polygon.push_back(G4TwoVector(0., 29.5864));
  polygon.push_back(G4TwoVector(21.501, 42.));
  polygon.push_back(G4TwoVector(40.2575, 42.));
  polygon.push_back(G4TwoVector(83.5587, 17.));
  polygon.push_back(G4TwoVector(83.5587, -17.));
  polygon.push_back(G4TwoVector(40.2575, -42.));
  polygon.push_back(G4TwoVector(21.501, -42.));
  polygon.push_back(G4TwoVector(0., -29.5864));
  G4ExtrudedSolid* solid_copper_tiltedpipe_extrusion = new G4ExtrudedSolid("solid_copper_tiltedpipe_extrusion", polygon, 30., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
  G4IntersectionSolid* solid_copper_tiltedpipe_outer = new G4IntersectionSolid("solid_copper_tiltedpipe_outer", solid_copper_tiltedpipe_intertube, solid_copper_tiltedpipe_extrusion, yRot_min_split, G4ThreeVector(9.965-30.*sin(5.1428/180.*M_PI), 0, +9.28-30.*cos(5.1428/180.*M_PI)));
  G4SubtractionSolid* solid_copper_tiltedpipe = new G4SubtractionSolid("solid_copper_tiltedpipe", solid_copper_tiltedpipe_outer, solid_copper_subtube, yRot_min_split, G4ThreeVector(44., 7., 28.));
  solid_copper_tiltedpipe = new G4SubtractionSolid("solid_copper_tiltedpipe", solid_copper_tiltedpipe, solid_copper_subtube, yRot_min_split, G4ThreeVector(42., 7., 28.));
  solid_copper_tiltedpipe = new G4SubtractionSolid("solid_copper_tiltedpipe", solid_copper_tiltedpipe, solid_copper_subtube, yRot_min_split, G4ThreeVector(44., -7., 28.));
  solid_copper_tiltedpipe = new G4SubtractionSolid("solid_copper_tiltedpipe", solid_copper_tiltedpipe, solid_copper_subtube, yRot_min_split, G4ThreeVector(42., -7., 28.));
  solid_copper_tiltedpipe = new G4SubtractionSolid("solid_copper_tiltedpipe", solid_copper_tiltedpipe, solid_copper_sub_horbox, zRot_180, G4ThreeVector(43., 0., 28));
  solid_copper_tiltedpipe = new G4SubtractionSolid("solid_copper_tiltedpipe", solid_copper_tiltedpipe, solid_copper_sub_vertbox, zRot_180, G4ThreeVector(43., 0., 28));
  
  G4LogicalVolume* logical_copper_tiltedpipe = new G4LogicalVolume(solid_copper_tiltedpipe,  G4Material::GetMaterial("Copper"),"logical_copper_tiltedpipe");
  logical_copper_tiltedpipe->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_tiltedpipe1 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 165.), logical_copper_tiltedpipe, "physical_copper_tiltedpipe1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_tiltedpipe2 = new G4PVPlacement(zRot_180, G4ThreeVector(0., 0., copper_outerring_anchor + 165.), logical_copper_tiltedpipe, "physical_copper_tiltedpipe2", logicalCrotchWorld, false, 0, checkoverlap);
  }
  
  
  
  
  if (copper_straightpipe) {
  // STRAIGHT PIPE
  // SUBTRACTION G4Box
  G4Box* solid_copper_straightpipe_subbox = new G4Box("solid_copper_straightpipe_subbox", 100., 100., 20.);
  
  // LEFT: LEFT UPPER ARC
  G4Tubs* solid_copper_straightpipe_leftuparcm = new G4Tubs("solid_copper_straightpipe_leftuparcm", 0, 33, 12.6, M_PI/2., M_PI/2.);
  G4SubtractionSolid* solid_copper_straightpipe_leftuparc = new G4SubtractionSolid("solid_copper_straightpipe_leftuparc", solid_copper_straightpipe_leftuparcm, solid_copper_subtube, yRot_split, G4ThreeVector(0., 0., 12.6));
  solid_copper_straightpipe_leftuparc = new G4SubtractionSolid("solid_copper_straightpipe_leftuparc", solid_copper_straightpipe_leftuparc, solid_copper_straightpipe_subbox, yRot_split, G4ThreeVector(-38., 0., -12.6 - 20./sin(84.8572/180.*M_PI)));
  
  G4LogicalVolume* logical_copper_straightpipe_leftuparc = new G4LogicalVolume(solid_copper_straightpipe_leftuparc, G4Material::GetMaterial("Copper"),"logical_copper_straightpipe_leftuparc");
  logical_copper_straightpipe_leftuparc->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_straightpipe_leftuparc1 = new G4PVPlacement(0, G4ThreeVector(-44., 7., copper_outerring_anchor + 180.4), logical_copper_straightpipe_leftuparc, "physical_copper_straightpipe_leftuparc1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_straightpipe_leftuparc2 = new G4PVPlacement(zRot_180, G4ThreeVector(44., -7., copper_outerring_anchor + 180.4), logical_copper_straightpipe_leftuparc, "physical_copper_straightpipe_leftuparc2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // LEFT: RIGHT UPPER ARC
  G4Tubs* solid_copper_straightpipe_rightuparcm = new G4Tubs("solid_copper_straightpipe_rightuparcm", 0, 33, 12.6, 0., M_PI/2.);
  G4SubtractionSolid* solid_copper_straightpipe_rightuparc = new G4SubtractionSolid("solid_copper_straightpipe_rightuparc", solid_copper_straightpipe_rightuparcm, solid_copper_subtube, yRot_split, G4ThreeVector(0., 0., 12.6));
  solid_copper_straightpipe_rightuparc = new G4SubtractionSolid("solid_copper_straightpipe_rightuparc", solid_copper_straightpipe_rightuparc, solid_copper_sub_vertbox, 0, G4ThreeVector(-1., -7., 12.6));
  solid_copper_straightpipe_rightuparc = new G4SubtractionSolid("solid_copper_straightpipe_rightuparc", solid_copper_straightpipe_rightuparc, solid_copper_straightpipe_subbox, yRot_split, G4ThreeVector(-40., 0., -12.6 - 20./sin(84.8572/180.*M_PI)));
  
  G4LogicalVolume* logical_copper_straightpipe_rightuparc = new G4LogicalVolume(solid_copper_straightpipe_rightuparc, G4Material::GetMaterial("Copper"),"logical_copper_straightpipe_rightuparc");
  logical_copper_straightpipe_rightuparc->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_straightpipe_rightuparc1 = new G4PVPlacement(0, G4ThreeVector(-42., 7., copper_outerring_anchor + 180.4), logical_copper_straightpipe_rightuparc, "physical_copper_straightpipe_rightuparc1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_straightpipe_rightuparc2 = new G4PVPlacement(zRot_180, G4ThreeVector(42., -7., copper_outerring_anchor + 180.4), logical_copper_straightpipe_rightuparc, "physical_copper_straightpipe_rightuparc2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // LEFT: RIGHT LOWER ARC
  G4Tubs* solid_copper_straightpipe_rightlowarcm = new G4Tubs("solid_copper_straightpipe_rightlowarcm", 0, 33, 12.6, -M_PI/2., M_PI/2.);
  G4SubtractionSolid* solid_copper_straightpipe_rightlowarc = new G4SubtractionSolid("solid_copper_straightpipe_rightlowarc", solid_copper_straightpipe_rightlowarcm, solid_copper_subtube, yRot_split, G4ThreeVector(0., 0., 12.6));
  solid_copper_straightpipe_rightlowarc = new G4SubtractionSolid("solid_copper_straightpipe_rightlowarc", solid_copper_straightpipe_rightlowarc, solid_copper_sub_vertbox, 0, G4ThreeVector(1., 7., 12.6));
  solid_copper_straightpipe_rightlowarc = new G4SubtractionSolid("solid_copper_straightpipe_rightlowarc", solid_copper_straightpipe_rightlowarc, solid_copper_straightpipe_subbox, yRot_split, G4ThreeVector(-40., 0., -12.6 - 20./sin(84.8572/180.*M_PI)));
  
  G4LogicalVolume* logical_copper_straightpipe_rightlowarc = new G4LogicalVolume(solid_copper_straightpipe_rightlowarc, G4Material::GetMaterial("Copper"),"logical_copper_straightpipe_rightlowarc");
  logical_copper_straightpipe_rightlowarc->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_straightpipe_rightlowarc1 = new G4PVPlacement(0, G4ThreeVector(-42., -7., copper_outerring_anchor + 180.4), logical_copper_straightpipe_rightlowarc, "physical_copper_straightpipe_rightlowarc1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_straightpipe_rightlowarc2 = new G4PVPlacement(zRot_180, G4ThreeVector(42., 7., copper_outerring_anchor + 180.4), logical_copper_straightpipe_rightlowarc, "physical_copper_straightpipe_rightlowarc2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // LEFT: LEFT LOWER ARC
  G4Tubs* solid_copper_straightpipe_leftlowarcm = new G4Tubs("solid_copper_straightpipe_leftlowarcm", 0, 33, 12.6, -M_PI, M_PI/2.);
  G4SubtractionSolid* solid_copper_straightpipe_leftlowarc = new G4SubtractionSolid("solid_copper_straightpipe_leftlowarc", solid_copper_straightpipe_leftlowarcm, solid_copper_subtube, yRot_split, G4ThreeVector(0., 0., 12.6));
  solid_copper_straightpipe_leftlowarc = new G4SubtractionSolid("solid_copper_straightpipe_leftlowarc", solid_copper_straightpipe_leftlowarc, solid_copper_straightpipe_subbox, yRot_split, G4ThreeVector(-38., 0., -12.6 - 20./sin(84.8572/180.*M_PI)));
  
  G4LogicalVolume* logical_copper_straightpipe_leftlowarc = new G4LogicalVolume(solid_copper_straightpipe_leftlowarc, G4Material::GetMaterial("Copper"),"logical_copper_straightpipe_leftlowarc");
  logical_copper_straightpipe_leftlowarc->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_straightpipe_leftlowarc1 = new G4PVPlacement(0, G4ThreeVector(-44., -7., copper_outerring_anchor + 180.4), logical_copper_straightpipe_leftlowarc, "physical_copper_straightpipe_leftlowarc1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_straightpipe_leftlowarc2 = new G4PVPlacement(zRot_180, G4ThreeVector(44., 7., copper_outerring_anchor + 180.4), logical_copper_straightpipe_leftlowarc, "physical_copper_straightpipe_leftlowarc2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // LEFT: LEFT BOX
  G4Box* solid_copper_straightpipe_leftboxm = new G4Box("solid_copper_straightpipe_leftboxm", 16, 7, 12.6);
  G4SubtractionSolid* solid_copper_straightpipe_leftbox = new G4SubtractionSolid("solid_copper_straightpipe_leftbox", solid_copper_straightpipe_leftboxm, solid_copper_sub_horbox, 0, G4ThreeVector(18., 0., 12.6));
  solid_copper_straightpipe_leftbox = new G4SubtractionSolid("solid_copper_straightpipe_leftbox", solid_copper_straightpipe_leftbox, solid_copper_straightpipe_subbox, yRot_split, G4ThreeVector(-21., 0., -12.6 - 20./sin(84.8572/180.*M_PI)));
  
  G4LogicalVolume* logical_copper_straightpipe_leftbox = new G4LogicalVolume(solid_copper_straightpipe_leftbox, G4Material::GetMaterial("Copper"),"logical_copper_straightpipe_leftbox");
  logical_copper_straightpipe_leftbox->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_straightpipe_leftbox1 = new G4PVPlacement(0, G4ThreeVector(-61., 0., copper_outerring_anchor + 180.4), logical_copper_straightpipe_leftbox, "physical_copper_straightpipe_leftbox1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_straightpipe_leftbox2 = new G4PVPlacement(zRot_180, G4ThreeVector(61., 0., copper_outerring_anchor + 180.4), logical_copper_straightpipe_leftbox, "physical_copper_straightpipe_leftbox2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // LEFT: RIGHT BOX
  G4Box* solid_copper_straightpipe_rightboxm = new G4Box("solid_copper_straightpipe_rightboxm", 16, 7, 12.6);
  G4SubtractionSolid* solid_copper_straightpipe_rightbox = new G4SubtractionSolid("solid_copper_straightpipe_rightbox", solid_copper_straightpipe_rightboxm, solid_copper_sub_horbox, 0, G4ThreeVector(-18., 0., 12.6));
  solid_copper_straightpipe_rightbox = new G4SubtractionSolid("solid_copper_straightpipe_rightbox", solid_copper_straightpipe_rightbox, solid_copper_straightpipe_subbox, yRot_split, G4ThreeVector(-59., 0., -12.6 - 20./sin(84.8572/180.*M_PI)));
  
  G4LogicalVolume* logical_copper_straightpipe_rightbox = new G4LogicalVolume(solid_copper_straightpipe_rightbox, G4Material::GetMaterial("Copper"),"logical_copper_straightpipe_rightbox");
  logical_copper_straightpipe_rightbox->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_straightpipe_rightbox1 = new G4PVPlacement(0, G4ThreeVector(-25., 0., copper_outerring_anchor + 180.4), logical_copper_straightpipe_rightbox, "physical_copper_straightpipe_rightbox1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_straightpipe_rightbox2 = new G4PVPlacement(zRot_180, G4ThreeVector(25., 0., copper_outerring_anchor + 180.4), logical_copper_straightpipe_rightbox, "physical_copper_straightpipe_rightbox2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // LEFT: UPPER BOX
  G4Box* solid_copper_straightpipe_upboxm = new G4Box("solid_copper_straightpipe_upboxm", 1, 16.5, 12.6);
  G4SubtractionSolid* solid_copper_straightpipe_upbox = new G4SubtractionSolid("solid_copper_straightpipe_upbox", solid_copper_straightpipe_upboxm, solid_copper_sub_vertbox, 0, G4ThreeVector(0., -23.5, 12.6));
  solid_copper_straightpipe_upbox = new G4SubtractionSolid("solid_copper_straightpipe_upbox", solid_copper_straightpipe_upbox, solid_copper_subtube, yRot_split, G4ThreeVector(-1., -16.5, 12.6));
  solid_copper_straightpipe_upbox = new G4SubtractionSolid("solid_copper_straightpipe_upbox", solid_copper_straightpipe_upbox, solid_copper_straightpipe_subbox, yRot_split, G4ThreeVector(-39., 0., -12.6 - 20./sin(84.8572/180.*M_PI)));
  
  G4LogicalVolume* logical_copper_straightpipe_upbox = new G4LogicalVolume(solid_copper_straightpipe_upbox, G4Material::GetMaterial("Copper"),"logical_copper_straightpipe_upbox");
  logical_copper_straightpipe_upbox->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_straightpipe_upbox1 = new G4PVPlacement(0, G4ThreeVector(-43., 23.5, copper_outerring_anchor + 180.4), logical_copper_straightpipe_upbox, "physical_copper_straightpipe_upbox1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_straightpipe_upbox2 = new G4PVPlacement(zRot_180, G4ThreeVector(43., -23.5, copper_outerring_anchor + 180.4), logical_copper_straightpipe_upbox, "physical_copper_straightpipe_upbox2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // LEFT: LOWER BOX
  G4Box* solid_copper_straightpipe_lowboxm = new G4Box("solid_copper_straightpipe_lowboxm", 1, 16.5, 12.6);
  G4SubtractionSolid* solid_copper_straightpipe_lowbox = new G4SubtractionSolid("solid_copper_straightpipe_lowbox", solid_copper_straightpipe_lowboxm, solid_copper_sub_vertbox, 0, G4ThreeVector(0., 23.5, 12.6));
  solid_copper_straightpipe_lowbox = new G4SubtractionSolid("solid_copper_straightpipe_lowbox", solid_copper_straightpipe_lowbox, solid_copper_subtube, yRot_split, G4ThreeVector(-1., 16.5, 12.6));
  solid_copper_straightpipe_lowbox = new G4SubtractionSolid("solid_copper_straightpipe_lowbox", solid_copper_straightpipe_lowbox, solid_copper_straightpipe_subbox, yRot_split, G4ThreeVector(-39., 0., -12.6 - 20./sin(84.8572/180.*M_PI)));
  
  G4LogicalVolume* logical_copper_straightpipe_lowbox = new G4LogicalVolume(solid_copper_straightpipe_lowbox, G4Material::GetMaterial("Copper"),"logical_copper_straightpipe_lowbox");
  logical_copper_straightpipe_lowbox->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_straightpipe_lowbox1 = new G4PVPlacement(0, G4ThreeVector(-43., -23.5, copper_outerring_anchor + 180.4), logical_copper_straightpipe_lowbox, "physical_copper_straightpipe_lowbox1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_straightpipe_lowbox2 = new G4PVPlacement(zRot_180, G4ThreeVector(43., 23.5, copper_outerring_anchor + 180.4), logical_copper_straightpipe_lowbox, "physical_copper_straightpipe_lowbox2", logicalCrotchWorld, false, 0, checkoverlap);
  
  
  
  
  }
  
  
  
  // PART BETWEEN THE TWO PIPES
  if (copper_betweenpipes) {
  
  G4Box* solid_copper_betweenpipes_box = new G4Box("solid_copper_betweenpipes_box", 15., 20., 28.);
  // SUBTRACT THE STRAIGHT PIPES
  G4Tubs* solid_copper_betweenpipes_subpipe = new G4Tubs("solid_copper_betweenpipes_subpipe", 0, 33, 12.6, 0., 2.*M_PI);
  G4Box* solid_copper_betweenpipes_subbox = new G4Box("solid_copper_betweenpipes_subbox", 33., 7., 12.6);
  G4SubtractionSolid* solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes_box, solid_copper_betweenpipes_subpipe, 0, G4ThreeVector(44., 7., 15.4));
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_subpipe, 0, G4ThreeVector(44., -7., 15.4));
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_subpipe, 0, G4ThreeVector(-44., 7., 15.4));
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_subpipe, 0, G4ThreeVector(-44., -7., 15.4));
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_subbox, 0, G4ThreeVector(44., 0., 15.4));
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_subbox, 0, G4ThreeVector(-44., 0., 15.4));
  // SUBTRACT THE TILTED PART
  G4Box* solid_copper_betweenpipes_subtilted = new G4Box("solid_copper_betweenpipes_subtilted", 15., 25., 25.);
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_subtilted, yRot_min_split, G4ThreeVector(9.965+15*cos(5.1428/180.*M_PI)-25.*sin(5.1428/180.*M_PI), 0., 9.28-15.*sin(5.1428/180.*M_PI)-25.*cos(5.1428/180.*M_PI)));
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_subtilted, yRot_split, G4ThreeVector(-9.965-15*cos(5.1428/180.*M_PI)+25.*sin(5.1428/180.*M_PI), 0., 9.28-15.*sin(5.1428/180.*M_PI)-25.*cos(5.1428/180.*M_PI)));
  // SUBTRACT THE SPACE FOR THE COOLING NEAR THE SURFACE
//   G4Tubs* solid_copper_betweenpipes_surfacehole = new G4Tubs("solid_copper_betweenpipes_surfacehole", 0., 7., 3., 0., 2.*M_PI);
//   G4Box* solid_copper_betweenpipes_surfacebox = new G4Box("solid_copper_betweenpipes_surfacebox", 7., 3., 18.);
//   solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_surfacehole, xRot_90, G4ThreeVector(0., 20., 18.));
//   solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_surfacehole, xRot_90, G4ThreeVector(0., -20., 18.));
//   solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_surfacehole, xRot_90, G4ThreeVector(0., 20., -18.));
//   solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_surfacehole, xRot_90, G4ThreeVector(0., -20., -18.));
//   solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_surfacebox, 0, G4ThreeVector(0., 20., 0.));
//   solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_surfacebox, 0, G4ThreeVector(0., -20., 0.));
  // SUBTRACT THE SPACE FOR THE COOLING IN THE BODY
  G4Tubs* solid_copper_betweenpipes_bodyhole1 = new G4Tubs("solid_copper_betweenpipes_bodyhole1", 0., 5., 17., 0., 2*M_PI);
  G4Tubs* solid_copper_betweenpipes_bodyhole2 = new G4Tubs("solid_copper_betweenpipes_bodyhole2", 0., 5., 10., 0., 2*M_PI);
  G4Box* solid_copper_betweenpipes_bodybox = new G4Box("solid_copper_betweenpipes_bodybox", 5., 7., 18.);
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_bodyhole1, xRot_90, G4ThreeVector(0., 0., 18.));
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_bodyhole2, xRot_90, G4ThreeVector(0., 13., -18.));
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_bodyhole2, xRot_90, G4ThreeVector(0., -13., -18.));
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_bodybox, 0, G4ThreeVector(0., 10., 0.));
  solid_copper_betweenpipes = new G4SubtractionSolid("solid_copper_betweenpipes", solid_copper_betweenpipes, solid_copper_betweenpipes_bodybox, 0, G4ThreeVector(0., -10., 0.));
  
  G4LogicalVolume* logical_copper_betweenpipes = new G4LogicalVolume(solid_copper_betweenpipes, G4Material::GetMaterial("Copper"),"logical_copper_betweenpipes");
  logical_copper_betweenpipes->SetVisAttributes(visCopper);

  G4VPhysicalVolume* physical_copper_betweenpipes = new G4PVPlacement(0, G4ThreeVector(0., -0., copper_outerring_anchor + 165.), logical_copper_betweenpipes, "physical_copper_betweenpipes", logicalCrotchWorld, false, 0, checkoverlap);
  
  // COOLING PIPE
  G4Tubs* solid_copper_betweenpipes_coolingpipe = new G4Tubs("solid_copper_betweenpipes_coolingpipe", 5., 7., 28., 0., 2.*M_PI);
  
  G4LogicalVolume* logical_copper_betweenpipes_coolingpipe = new G4LogicalVolume(solid_copper_betweenpipes_coolingpipe, G4Material::GetMaterial("Copper"),"logical_copper_betweenpipes_coolingpipe");
  logical_copper_betweenpipes_coolingpipe->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_betweenpipes_coolingpipe1 = new G4PVPlacement(xRot_90, G4ThreeVector(0., 48., copper_outerring_anchor + 147.), logical_copper_betweenpipes_coolingpipe, "physical_copper_betweenpipes_coolingpipe1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_copper_betweenpipes_coolingpipe2 = new G4PVPlacement(xRot_90, G4ThreeVector(0., -48., copper_outerring_anchor + 147.), logical_copper_betweenpipes_coolingpipe, "physical_copper_betweenpipes_coolingpipe2", logicalCrotchWorld, false, 0, checkoverlap);
}
  
  // WATER
  if (water) {
  // COOLING WATER AT OUTER RING
  G4Tubs* solid_water_outerring = new G4Tubs("solid_water_outerring", 65., 75., 3., M_PI/2. - 0.01273277/2., 2.*M_PI - 0.01273277);
  
  G4LogicalVolume* logical_water_outerring = new G4LogicalVolume(solid_water_outerring, G4Material::GetMaterial("Water"),"logical_water_outerring");
  logical_water_outerring->SetVisAttributes(visWater);
  
  G4VPhysicalVolume* physical_water_outerring = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 10.), logical_water_outerring, "physical_water_outerring", logicalCrotchWorld, false, 0, checkoverlap);
  
  // COOLING WATER AT INNER RING
  G4Tubs* solid_water_innerring = new G4Tubs("solid_water_innerring", 71., 80., 3., M_PI/2. - 0.01273277/2., 2.*M_PI - 0.01273277);
  
  G4LogicalVolume* logical_water_innerring = new G4LogicalVolume(solid_water_innerring, G4Material::GetMaterial("Water"),"logical_water_innerring");
  logical_water_innerring->SetVisAttributes(visWater);
  
  G4VPhysicalVolume* physical_water_innerring = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 130.), logical_water_innerring, "physical_water_innerring", logicalCrotchWorld, false, 0, checkoverlap);
  
  // COOLING WATER IN SPACER BETWEEN TWO PIPES
  // ROUND PARTS IN BODY
  G4Tubs* solid_water_betweenpipes_halfhole = new G4Tubs("solid_water_betweenpipes_halfhole", 0., 5., 7., 0., M_PI);
  
  G4LogicalVolume* logical_water_betweenpipes_halfhole = new G4LogicalVolume(solid_water_betweenpipes_halfhole, G4Material::GetMaterial("Water"),"logical_water_betweenpipes_halfhole");
  logical_water_betweenpipes_halfhole->SetVisAttributes(visWater);
  
  G4VPhysicalVolume* physical_water_betweenpipes_halfhole1 = new G4PVPlacement(xRot_90, G4ThreeVector(0., 10., copper_outerring_anchor + 147.), logical_water_betweenpipes_halfhole, "physical_water_betweenpipes_halfhole1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_water_betweenpipes_halfhole2 = new G4PVPlacement(xRot_270, G4ThreeVector(0., 10., copper_outerring_anchor + 183.), logical_water_betweenpipes_halfhole, "physical_water_betweenpipes_halfhole2", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_water_betweenpipes_halfhole3 = new G4PVPlacement(xRot_90, G4ThreeVector(0., -10., copper_outerring_anchor + 147.), logical_water_betweenpipes_halfhole, "physical_water_betweenpipes_halfhole3", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_water_betweenpipes_halfhole4 = new G4PVPlacement(xRot_270, G4ThreeVector(0., -10., copper_outerring_anchor + 183.), logical_water_betweenpipes_halfhole, "physical_water_betweenpipes_halfhole4", logicalCrotchWorld, false, 0, checkoverlap);
  
  // TUBE IN BODY
  G4Tubs* solid_water_betweenpipes_tube = new G4Tubs("solid_water_betweenpipes_tube", 0., 5., 3., 0., 2*M_PI);
  
  G4LogicalVolume* logical_water_betweenpipes_tube = new G4LogicalVolume(solid_water_betweenpipes_tube, G4Material::GetMaterial("Water"),"logical_water_betweenpipes_tube");
  logical_water_betweenpipes_tube->SetVisAttributes(visWater);
  
  G4VPhysicalVolume* physical_water_betweenpipes_tube = new G4PVPlacement(xRot_90, G4ThreeVector(0., 0., copper_outerring_anchor + 183.), logical_water_betweenpipes_tube, "physical_water_betweenpipes_tube", logicalCrotchWorld, false, 0, checkoverlap);
  
  // BOX IN BODY
  G4Box* solid_water_betweenpipes_box = new G4Box("solid_water_betweenpipes_box", 5., 7., 18.);
  
  G4LogicalVolume* logical_water_betweenpipes_box = new G4LogicalVolume(solid_water_betweenpipes_box, G4Material::GetMaterial("Water"),"logical_water_betweenpipes_box");
  logical_water_betweenpipes_box->SetVisAttributes(visWater);
  
  G4VPhysicalVolume* physical_water_betweenpipes_box1 = new G4PVPlacement(0, G4ThreeVector(0., 10., copper_outerring_anchor + 165.), logical_water_betweenpipes_box, "physical_water_betweenpipes_box1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_water_betweenpipes_box2 = new G4PVPlacement(0, G4ThreeVector(0., -10., copper_outerring_anchor + 165.), logical_water_betweenpipes_box, "physical_water_betweenpipes_box2", logicalCrotchWorld, false, 0, checkoverlap);
  
  // WATER SUPPORT FOR SPACER
  G4Tubs* solid_water_betweenpipes_extension = new G4Tubs("solid_water_betweenpipes_extension", 0., 5., 30., 0., 2.*M_PI);
  
  G4LogicalVolume* logical_water_betweenpipes_extension = new G4LogicalVolume(solid_water_betweenpipes_extension, G4Material::GetMaterial("Water"),"logical_water_betweenpipes_extension");
  logical_water_betweenpipes_extension->SetVisAttributes(visWater);
  
  G4VPhysicalVolume* physical_water_betweenpipes_extension1 = new G4PVPlacement(xRot_90, G4ThreeVector(0., 47., copper_outerring_anchor + 147.), logical_water_betweenpipes_extension, "physical_water_betweenpipes_extension1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_water_betweenpipes_extension2 = new G4PVPlacement(xRot_90, G4ThreeVector(0., -47., copper_outerring_anchor + 147.), logical_water_betweenpipes_extension, "physical_water_betweenpipes_extension2", logicalCrotchWorld, false, 0, checkoverlap);
  
  
  
  // ROUND PARTS AT SURFACE
//   G4Tubs* solid_water_betweenpipes_halfholesurf = new G4Tubs("solid_water_betweenpipes_halfholesurf", 0., 7., 1.5, 0., M_PI);
//   
//   G4LogicalVolume* logical_water_betweenpipes_halfholesurf = new G4LogicalVolume(solid_water_betweenpipes_halfholesurf, G4Material::GetMaterial("Water"),"logical_water_betweenpipes_halfholesurf");
//   logical_water_betweenpipes_halfholesurf->SetVisAttributes(visWater);
//   
//   G4VPhysicalVolume* physical_water_betweenpipes_halfholesurf1 = new G4PVPlacement(xRot_90, G4ThreeVector(0., 18.5, copper_outerring_anchor + 147.), logical_water_betweenpipes_halfholesurf, "physical_water_betweenpipes_halfholesurf1", logicalCrotchWorld, false, 0, checkoverlap);
//   G4VPhysicalVolume* physical_water_betweenpipes_halfholesurf2 = new G4PVPlacement(xRot_270, G4ThreeVector(0., 18.5, copper_outerring_anchor + 183.), logical_water_betweenpipes_halfholesurf, "physical_water_betweenpipes_halfholesurf2", logicalCrotchWorld, false, 0, checkoverlap);
//   G4VPhysicalVolume* physical_water_betweenpipes_halfholesurf3 = new G4PVPlacement(xRot_90, G4ThreeVector(0., -18.5, copper_outerring_anchor + 147.), logical_water_betweenpipes_halfholesurf, "physical_water_betweenpipes_halfholesurf3", logicalCrotchWorld, false, 0, checkoverlap);
//   G4VPhysicalVolume* physical_water_betweenpipes_halfholesurf4 = new G4PVPlacement(xRot_270, G4ThreeVector(0., -18.5, copper_outerring_anchor + 183.), logical_water_betweenpipes_halfholesurf, "physical_water_betweenpipes_halfholesurf4", logicalCrotchWorld, false, 0, checkoverlap);
  
  // BOX AT SURFACE
//   G4Box* solid_water_betweenpipes_boxsurf = new G4Box("solid_water_betweenpipes_boxsurf", 7., 1.5, 18.);
//   
//   G4LogicalVolume* logical_water_betweenpipes_boxsurf = new G4LogicalVolume(solid_water_betweenpipes_boxsurf, G4Material::GetMaterial("Water"),"logical_water_betweenpipes_boxsurf");
//   logical_water_betweenpipes_boxsurf->SetVisAttributes(visWater);
//   
//   G4VPhysicalVolume* physical_water_betweenpipes_boxsurf1 = new G4PVPlacement(0, G4ThreeVector(0., 18.5, copper_outerring_anchor + 165.), logical_water_betweenpipes_boxsurf, "physical_water_betweenpipes_boxsurf1", logicalCrotchWorld, false, 0, checkoverlap);
//   G4VPhysicalVolume* physical_water_betweenpipes_boxsurf2 = new G4PVPlacement(0, G4ThreeVector(0., -18.5, copper_outerring_anchor + 165.), logical_water_betweenpipes_boxsurf, "physical_water_betweenpipes_boxsurf2", logicalCrotchWorld, false, 0, checkoverlap);
}
  
  // COVERS FOR THE COOLING CIRCLES AND THE SCREEN
  if (copper_cover) {
  
  // OUTER RING
  G4Tubs* solid_copper_cover_outer = new G4Tubs("solid_copper_cover_outer", 75., 79., 6.5, 0., 2.*M_PI);
  
  G4LogicalVolume* logical_copper_cover_outer = new G4LogicalVolume(solid_copper_cover_outer, G4Material::GetMaterial("Copper"),"logical_copper_cover_outer");
  logical_copper_cover_outer->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_cover_outer = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 10.5), logical_copper_cover_outer, "physical_copper_cover_outer", logicalCrotchWorld, false, 0, checkoverlap);
  
  // INNER RING
  G4Tubs* solid_copper_cover_inner = new G4Tubs("solid_copper_cover_inner", 80., 84., 6.5, 0., 2.*M_PI);
  
  G4LogicalVolume* logical_copper_cover_inner = new G4LogicalVolume(solid_copper_cover_inner, G4Material::GetMaterial("Copper"),"logical_copper_cover_inner");
  logical_copper_cover_inner->SetVisAttributes(visCopper);
  
  G4VPhysicalVolume* physical_copper_cover_inner = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 130.5), logical_copper_cover_inner, "physical_copper_cover_inner", logicalCrotchWorld, false, 0, checkoverlap);
  
  // STAINLESS STEEL COVER SURROUNDING THE SCREEN
  // STRAIGHT
  G4Tubs* solid_screen_cover_tube = new G4Tubs("solid_screen_cover_tube", 77., 80, 53., 0., 2.*M_PI);
  G4Tubs* solid_screen_cover_subtube = new G4Tubs("solid_screen_cover_subtube", 0., 50., 50., 0., 2.*M_PI);
  G4SubtractionSolid* solid_screen_cover_straight = new G4SubtractionSolid("solid_screen_cover_straight", solid_screen_cover_tube, solid_screen_cover_subtube, Rot_cover, G4ThreeVector(50.*cos(40./180.*M_PI), 50.*sin(40./180.*M_PI), 0.));
  
  G4LogicalVolume* logical_screen_cover_straight = new G4LogicalVolume(solid_screen_cover_straight, G4Material::GetMaterial("StainlessSteel"),"logical_screen_cover_straight");
  logical_screen_cover_straight->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume* physical_screen_cover_straight = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor + 70.), logical_screen_cover_straight, "physical_screen_cover_straight", logicalCrotchWorld, false, 0, checkoverlap);
  
  
  
  G4Tubs* solid_screen_cover_tilttube = new G4Tubs("solid_screen_cover_tilttube", 50., 52., 50., 0., 2.*M_PI);
  G4Tubs* solid_screen_cover_tube2 = new G4Tubs("solid_screen_cover_tube2", 0., 80, 53., 0., 2.*M_PI);
  G4SubtractionSolid* solid_screen_cover_tilt = new G4SubtractionSolid("solid_screen_cover_tilt", solid_screen_cover_tilttube, solid_screen_cover_tube2, xRot_90, G4ThreeVector(0., 0., -80.));
  
  G4LogicalVolume* logical_screen_cover_tilt = new G4LogicalVolume(solid_screen_cover_tilt, G4Material::GetMaterial("StainlessSteel"),"logical_screen_cover_tilt");
  logical_screen_cover_tilt->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume* physical_screen_cover_tilt = new G4PVPlacement(Rot_cover, G4ThreeVector(80.*cos(40./180.*M_PI), 80.*sin(40./180.*M_PI), copper_outerring_anchor + 70.), logical_screen_cover_tilt, "physical_screen_cover_tilt", logicalCrotchWorld, false, 0, checkoverlap);
}
  
  
  if (bellowpart) {
  G4Tubs* solid_bellowpart_tube = new G4Tubs("solid_bellowpart_tube", 60., 65., 25., 0., 2*M_PI);
  
  G4LogicalVolume* logical_bellowpart_tube = new G4LogicalVolume(solid_bellowpart_tube, G4Material::GetMaterial("StainlessSteel"),"logical_bellowpart_tube");
  logical_bellowpart_tube->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume* physical_bellowpart_tube = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 25.), logical_bellowpart_tube, "physical_bellowpart_tube", logicalCrotchWorld, false, 0, checkoverlap);
  
  G4Cons* solid_bellowpart_cone = new G4Cons("solid_bellowpart_cone", 52., 62., 60., 62., 42.25, 0., 2.*M_PI);
  
  G4LogicalVolume* logical_bellowpart_cone = new G4LogicalVolume(solid_bellowpart_cone, G4Material::GetMaterial("StainlessSteel"),"logical_bellowpart_cone");
  logical_bellowpart_cone->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume* physical_bellowpart_cone = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 92.25), logical_bellowpart_cone, "physical_bellowpart_cone", logicalCrotchWorld, false, 0, checkoverlap);
  
  G4Tubs* solid_bellowpart_ring = new G4Tubs("solid_bellowpart_ring", 65., 79., 3., 0., 2.*M_PI);
  
  G4LogicalVolume* logical_bellowpart_ring = new G4LogicalVolume(solid_bellowpart_ring, G4Material::GetMaterial("StainlessSteel"),"logical_bellowpart_ring");
  logical_bellowpart_ring->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume* physical_bellowpart_ring1 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 11.), logical_bellowpart_ring, "physical_bellowpart_ring1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_ring2 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 47.), logical_bellowpart_ring, "physical_bellowpart_ring2", logicalCrotchWorld, false, 0, checkoverlap);
  
  G4Cons* solid_bellowpart_ringext = new G4Cons("solid_bellowpart_ringext", 79., 86., 79., 79.1, 3., 0., 2.*M_PI);
  
  G4LogicalVolume* logical_bellowpart_ringext = new G4LogicalVolume(solid_bellowpart_ringext, G4Material::GetMaterial("StainlessSteel"),"logical_bellowpart_ringext");
  logical_bellowpart_ringext->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume* physical_bellowpart_ringext1 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 11.), logical_bellowpart_ringext, "physical_bellowpart_ringext1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_ringext2 = new G4PVPlacement(xRot_180, G4ThreeVector(0., 0., copper_outerring_anchor - 47.), logical_bellowpart_ringext, "physical_bellowpart_ringext2", logicalCrotchWorld, false, 0, checkoverlap);
  
  G4Cons* solid_bellowpart_bellow = new G4Cons("solid_bellowpart_bellow", 65., 86., 65., 65.1, 1.5, 0., 2.*M_PI);
  
  G4LogicalVolume* logical_bellowpart_bellow = new G4LogicalVolume(solid_bellowpart_bellow, G4Material::GetMaterial("StainlessSteel"),"logical_bellowpart_bellow");
  logical_bellowpart_bellow->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume* physical_bellowpart_bellow1 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 15.5), logical_bellowpart_bellow, "physical_bellowpart_bellow1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_bellow2 = new G4PVPlacement(xRot_180, G4ThreeVector(0., 0., copper_outerring_anchor - 18.5), logical_bellowpart_bellow, "physical_bellowpart_bellow2", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_bellow3 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 21.5), logical_bellowpart_bellow, "physical_bellowpart_bellow3", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_bellow4 = new G4PVPlacement(xRot_180, G4ThreeVector(0., 0., copper_outerring_anchor - 24.5), logical_bellowpart_bellow, "physical_bellowpart_bellow4", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_bellow5 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 27.5), logical_bellowpart_bellow, "physical_bellowpart_bellow5", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_bellow6 = new G4PVPlacement(xRot_180, G4ThreeVector(0., 0., copper_outerring_anchor - 30.5), logical_bellowpart_bellow, "physical_bellowpart_bellow6", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_bellow7 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 33.5), logical_bellowpart_bellow, "physical_bellowpart_bellow7", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_bellow8 = new G4PVPlacement(xRot_180, G4ThreeVector(0., 0., copper_outerring_anchor - 36.5), logical_bellowpart_bellow, "physical_bellowpart_bellow8", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_bellow9 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 39.5), logical_bellowpart_bellow, "physical_bellowpart_bellow9", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_bellow10 = new G4PVPlacement(xRot_180, G4ThreeVector(0., 0., copper_outerring_anchor - 42.5), logical_bellowpart_bellow, "physical_bellowpart_bellow10", logicalCrotchWorld, false, 0, checkoverlap);
  
  G4Tubs* solid_bellowpart_outerring = new G4Tubs("solid_bellowpart_outerring", 62., 83., 12., 0., 2.*M_PI);
  
  G4LogicalVolume* logical_bellowpart_outerring = new G4LogicalVolume(solid_bellowpart_outerring, G4Material::GetMaterial("StainlessSteel"),"logical_bellowpart_outerring");
  logical_bellowpart_outerring->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume* physical_bellowpart_outerring = new G4PVPlacement(0, G4ThreeVector(0., 0., -335./2.+12.), logical_bellowpart_outerring, "physical_bellowpart_outerring", logicalCrotchWorld, false, 0, checkoverlap);
  
  G4Tubs* solid_bellowpart_coolingring = new G4Tubs("solid_bellowpart_coolingring", 62., 72., 4., 0., 2.*M_PI);
  
  G4LogicalVolume* logical_bellowpart_coolingring = new G4LogicalVolume(solid_bellowpart_coolingring, G4Material::GetMaterial("StainlessSteel"),"logical_bellowpart_coolingring");
  logical_bellowpart_coolingring->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume* physical_bellowpart_coolingring1 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 54.), logical_bellowpart_coolingring, "physical_bellowpart_coolingring1", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_coolingring2 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 62.), logical_bellowpart_coolingring, "physical_bellowpart_coolingring2", logicalCrotchWorld, false, 0, checkoverlap);
  G4VPhysicalVolume* physical_bellowpart_coolingring3 = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 78.), logical_bellowpart_coolingring, "physical_bellowpart_coolingring3", logicalCrotchWorld, false, 0, checkoverlap);
  
  G4Tubs* solid_bellowpart_coolingcover = new G4Tubs("solid_bellowpart_coolingcover", 72., 77., 12., 0., 2.*M_PI);
  
  G4LogicalVolume* logical_bellowpart_coolingcover = new G4LogicalVolume(solid_bellowpart_coolingcover, G4Material::GetMaterial("StainlessSteel"),"logical_bellowpart_coolingcover");
  logical_bellowpart_coolingcover->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume* physical_bellowpart_coolingcover = new G4PVPlacement(0, G4ThreeVector(0., 0., copper_outerring_anchor - 70.), logical_bellowpart_coolingcover, "physical_bellowpart_coolingcover", logicalCrotchWorld, false, 0, checkoverlap);
}
  
  
  
  
  
  
  logicalCrotchWorld->SetVisAttributes(G4VisAttributes::Invisible);
	
//RIGHT SIDE
physicalCrotchWorld = new G4PVPlacement(0,G4ThreeVector(0,0,2200.-335./2.),logicalCrotchWorld,"physicalCrotchWorld",logicalbes,false,0);
//LEFT SIDE
	G4ReflectZ3D  reflection;
	G4Translate3D translation(G4ThreeVector(0, 0, -2200.+335./2.));
 	G4Transform3D transform = translation*reflection;
	G4ReflectionFactory::Instance()
          ->Place(transform,     // the transformation with reflection
                  "physicalCrotchWorld", // the actual name
                  logicalCrotchWorld,    // the logical volume
                  logicalbes,     // the mother volume
                  true,         // no boolean operation
                  0);
									  
/*
  G4VisAttributes* visSF63 = new G4VisAttributes(G4Colour(1.,1.,0.));
  logicalSF63->SetVisAttributes(visSF63);

  G4VisAttributes* visPip = new G4VisAttributes(G4Colour(0.5,0.5,0.5));  
  logicalCons->SetVisAttributes(visPip);
  logicalChambertube->SetVisAttributes(visPip);
*/








}
