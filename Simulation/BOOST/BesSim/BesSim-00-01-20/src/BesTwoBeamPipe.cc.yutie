//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oreiented Simulation Tool                    //
//---------------------------------------------------------------------------//
//Descpirtion: BES beam pipe 
//Author: Liuhm
//Created: May 21, 2003
//Comment:
//---------------------------------------------------------------------------//
//
#include "BesTwoBeamPipe.hh"

#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4VisAttributes.hh"
#include "G4Color.hh"
#include "G4Cons.hh"
#include "G4Trd.hh"
#include "G4RotationMatrix.hh"
#include "G4SubtractionSolid.hh"

BesTwoBeamPipe::BesTwoBeamPipe()
{

  logicalTwoBeamPipe = 0;
  physicalTwoBeamPipe1 = 0;
  physicalTwoBeamPipe2 = 0;


  Au = 0;
  Ag = 0;
  Oil = 0;
}

void BesTwoBeamPipe::DefineMaterial()
{
  G4double density, a, z,fractionmass;
  G4int nel,natoms;
  G4String name, symbol;

  density=19.32*g/cm3;
  a = 196.967*g/mole;
  Au= new G4Material("Gold",79,a,density);

  density=10.5*g/cm3;
  a = 107.9*g/mole;
  Ag= new G4Material("Silver",47,a,density);

  a = 1.01*g/mole;
  G4Element* H  = new G4Element(name="Hydrogen",symbol="H" , z= 1., a);
    
  a = 12.01*g/mole;
  G4Element* C  = new G4Element(name="Carbon"  ,symbol="C" , z= 6., a);

  density = 0.810*g/cm3;
  Oil = new G4Material(name="Oil",density,nel=2);
  Oil->AddElement(C, natoms=18);
  Oil->AddElement(H, natoms=38);


  G4double pressure;

a = 14.01*g/mole;
    G4Element* N  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a);
    
    a = 16.00*g/mole;
    G4Element* O  = new G4Element(name="Oxygen"  ,symbol="O" , z= 8., a);


  density = 1.290*mg/cm3;
    G4Material* Air = new G4Material(name="Air",density,nel=2);
    Air->AddElement(N, fractionmass = 0.7);
    Air->AddElement(O, fractionmass = 0.3);



      density     = 1.0e-5*g/cm3;
    pressure    = 2.e-2*bar;
    G4Material* Beam = new G4Material(name="Beam",density,nel=1,kStateGas,STP_Temperature,pressure);
    Beam->AddMaterial(Air,fractionmass=1.0);


density = 1.848*g/cm3;
    a = 9.012182*g/mole;
    G4Material* Beryllium = new G4Material(name="Beryllium",z=4.0,a,density);

    density = 2.70*g/cm3;
    a = 26.98*g/mole;
    G4Material* Aluminium = new G4Material(name="Aluminium",z=13.,a,density);


}

void BesTwoBeamPipe::Construct(G4LogicalVolume* logicalbes)
{
  DefineMaterial();

  G4RotationMatrix* zRot_90 = new G4RotationMatrix;  zRot_90->rotateZ(3.14159265/2);
  G4RotationMatrix* zRot_180 = new G4RotationMatrix;  zRot_180->rotateZ(3.14159265);
  G4RotationMatrix* zRot_270 = new G4RotationMatrix;  zRot_270->rotateZ(3.14159265/2*3);

   //G4RotationMatrix* xRot = new G4RotationMatrix;
  //xRot->rotateX(90*deg);

  //the logical volume of beam pipe

  G4Box* solidTwoBeamPipe =  new G4Box("solidPip",300./2,300/2.,1000./2);  //length: 1000mm;
  logicalTwoBeamPipe = new G4LogicalVolume(solidTwoBeamPipe, G4Material::GetMaterial("Beam"),"logicalTwoBeamPipe");
  new G4PVPlacement(zRot_90,G4ThreeVector(0, 0, 500+1365+335+1000./2),logicalTwoBeamPipe,"physicalTwoBeamPipe1",logicalbes,false,0);

  G4RotationMatrix* zyRot = new G4RotationMatrix;
  zyRot->rotateZ(3.14159265/2);
  zyRot->rotateY(3.14159265);
  new G4PVPlacement(zyRot,G4ThreeVector(0, 0, -500-1365-335-1000./2),logicalTwoBeamPipe,"physicalTwoBeamPipe2",logicalbes,false,0);




  G4Box* solidPip = new G4Box("solidPip",76/2.,86/2.,1000./2);  //length: 1000mm; 
  G4LogicalVolume* logicalPip = new G4LogicalVolume(solidPip, G4Material::GetMaterial("Beam"),"logicalPip");



  double split_angle = 0.03;
  double pos_pipe_1 = 43+sin(split_angle) * 1000/2.;

  G4RotationMatrix* xRot_split_angle1 = new G4RotationMatrix;  xRot_split_angle1->rotateX(split_angle); //rough estimation: split angle of one beam pipe
  G4RotationMatrix* xRot_split_angle2 = new G4RotationMatrix;  xRot_split_angle2->rotateX(-1*split_angle); //rough estimation: split angle of one beam pipe  

  physicalTwoBeamPipe1 = new G4PVPlacement(xRot_split_angle1,G4ThreeVector(0,pos_pipe_1, 0),logicalPip,"physicalTwoBeamPipe1",logicalTwoBeamPipe,false,0);
  physicalTwoBeamPipe2 = new G4PVPlacement(xRot_split_angle2,G4ThreeVector(0,-1*pos_pipe_1, 0),logicalPip,"physicalTwoBeamPipe2",logicalTwoBeamPipe,false,0);

  //to combine the non-regular output pipe


  G4Tubs *solidtub_onefourth = new G4Tubs("solidtub_onfourth", 60./2, 70/2, 1000./2, 0, 3.14159265/2);// 1/4 tube
  G4Box  *solidbox_side1 = new G4Box("solidbox_side1", 5./2, 6./2, 1000./2);
  G4Box  *solidbox_side2 = new G4Box("solidbox_side2", 16./2, 5./2, 1000./2);

  G4LogicalVolume* logicalTub_onefourth = new G4LogicalVolume(solidtub_onefourth, G4Material::GetMaterial("Copper"),"logicalTub_onefourth");
  G4LogicalVolume* logicalBox_side1 = new G4LogicalVolume(solidbox_side1, G4Material::GetMaterial("Copper"),"logicalBox_side1");
  G4LogicalVolume* logicalBox_side2 = new G4LogicalVolume(solidbox_side2, G4Material::GetMaterial("Copper"),"logicalBox_side2");

 
G4VPhysicalVolume*  physicalTub1 = new G4PVPlacement(0,G4ThreeVector(8,3,0),logicalTub_onefourth,"physicalTub1",logicalPip,false,0);
G4VPhysicalVolume*  physicalTub2 = new G4PVPlacement(zRot_90,G4ThreeVector(8,-3,0),logicalTub_onefourth,"physicalTub2",logicalPip,false,0);
G4VPhysicalVolume*  physicalTub3 = new G4PVPlacement(zRot_180,G4ThreeVector(-8,-3,0),logicalTub_onefourth,"physicalTub3",logicalPip,false,0);
G4VPhysicalVolume*  physicalTub4 = new G4PVPlacement(zRot_270,G4ThreeVector(-8,3,0),logicalTub_onefourth,"physicalTub4",logicalPip,false,0);

G4VPhysicalVolume*  physicalBox1 = new G4PVPlacement(0,G4ThreeVector(38+5./2,0,0),logicalBox_side1,"physicalBox1",logicalPip,false,0);
G4VPhysicalVolume*  physicalBox2 = new G4PVPlacement(0,G4ThreeVector(0, 33+5./2,0),logicalBox_side2,"physicalBox2",logicalPip,false,0);
G4VPhysicalVolume*  physicalBox3 = new G4PVPlacement(0,G4ThreeVector(-38-5./2,0,0),logicalBox_side1,"physicalBox3",logicalPip,false,0);
G4VPhysicalVolume*  physicalBox4 = new G4PVPlacement(0,G4ThreeVector(0, -33-5./2,0),logicalBox_side2,"physicalBox4",logicalPip,false,0);


//the last part is the connection between Y crotch and the following two chambers.




  G4VisAttributes* visPip = new G4VisAttributes(G4Colour(209./255,196./255, 33./255));
  G4VisAttributes* vissplit = new G4VisAttributes(G4Colour(1,1,0));

  logicalTwoBeamPipe->SetVisAttributes(G4VisAttributes::Invisible);
  logicalPip->SetVisAttributes(G4VisAttributes::Invisible);

  logicalTub_onefourth->SetVisAttributes(visPip);
  logicalBox_side1->SetVisAttributes(visPip);
  logicalBox_side2->SetVisAttributes(visPip);

  

/*
  G4VisAttributes* visPip = new G4VisAttributes(G4Colour(0.5,0.5,0.5));
  G4VisAttributes* vissplit = new G4VisAttributes(G4Colour(1,1,0));  

  logicalPlate->SetVisAttributes(visPip);
  logicalCons->SetVisAttributes(visPip);
  logicalTub_onefourth->SetVisAttributes(vissplit);
  logicalBox_side1->SetVisAttributes(vissplit);
  logicalBox_side2->SetVisAttributes(vissplit);
  logicalBox_middle->SetVisAttributes(vissplit);
  
  logicalBox_side1_connection->SetVisAttributes(visPip);
  logicalBox_side2_connection->SetVisAttributes(visPip);
  logicalBox_middle_connection->SetVisAttributes(visPip);
  logicalTub_onefourth_connection->SetVisAttributes(visPip);
*/
  
/*
  G4VisAttributes* visSF63 = new G4VisAttributes(G4Colour(1.,1.,0.));
  logicalSF63->SetVisAttributes(visSF63);

  G4VisAttributes* visPip = new G4VisAttributes(G4Colour(0.5,0.5,0.5));  
  logicalCons->SetVisAttributes(visPip);
  logicalChambertube->SetVisAttributes(visPip);
*/








}
