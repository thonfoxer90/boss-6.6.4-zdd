#include "BesEventAction.hh"
#include "G4Event.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"

#include "G4DigiManager.hh"
#include "BesMdcDigitizer.hh"
#include "BesTofDigitizer.hh"
#include "BesMucDigitizer.hh"
#include "BesEmcDigitizer.hh"
#include "BesZddDigitizer.hh"

#include "BesRunAction.hh"
#include "G4RunManager.hh"
#include "BesAsciiIO.hh"
#include "BesMdcHit.hh"
#include "BesRootIO.hh"
#include "BesTDSWriter.hh"
#include "BesTuningIO.hh"

#include "BesSensitiveManager.hh"
#include "BesTruthTrack.hh"
#include "BesTruthVertex.hh"
#include <fstream>

#include "GaudiKernel/SvcFactory.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/Bootstrap.h"

using namespace std;

#include "ReadBoostRoot.hh"
#include "G4UImanager.hh"

/*
#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::string>+;
#endif
*/

BesEventAction::BesEventAction(BesRunAction* runAction)
        :m_runAction(runAction)
{
    ISvcLocator* svcLocator = Gaudi::svcLocator();
    IRealizationSvc *tmpReal;
    StatusCode sc = svcLocator->service("RealizationSvc",tmpReal);
    if (!sc.isSuccess())
    {
        std::cout << " Could not initialize Realization Service in BesEventAction" << std::endl;
    } else {
        m_RealizationSvc=dynamic_cast<RealizationSvc*>(tmpReal);
    }

    m_DM = G4DigiManager::GetDMpointer();

    if (ReadBoostRoot::GetMdc())
    {
        BesMdcDigitizer* mdcDM = new BesMdcDigitizer("BesMdcDigitizer");
        m_DM->AddNewModule(mdcDM);
    }
    if (ReadBoostRoot::GetTof())
    {
        BesTofDigitizer* tofDM = new BesTofDigitizer("BesTofDigitizer");
        m_DM->AddNewModule(tofDM);
    }
    if (ReadBoostRoot::GetEmc())
    {
        BesEmcDigitizer* emcDM = new BesEmcDigitizer("BesEmcDigitizer");
        m_DM->AddNewModule(emcDM);
    }
    if (ReadBoostRoot::GetMuc())
    {
        BesMucDigitizer* mucDM = new BesMucDigitizer("BesMucDigitizer");
        m_DM->AddNewModule(mucDM);
    }

    if (ReadBoostRoot::GetZdd())
    {
        BesZddDigitizer* zddDM = new BesZddDigitizer("BesZddDigitizer");
        m_DM->AddNewModule(zddDM);
    }
		
		m_File = new TFile("BesEventAction.root","RECREATE");
		m_NTuple = new TTree("NTEventAction","");
		m_NTuple->Branch("evtID",&m_evtID,"m_evtID/I");
		m_NTuple->Branch("zdd_energy",&m_zdd_energy,"m_zdd_energy/D");
			m_NTuple->Branch("zddRight_energy",&m_zddRight_energy,"m_zddRight_energy/D");
			m_NTuple->Branch("zddLeft_energy",&m_zddLeft_energy,"m_zddLeft_energy/D");
			
      m_NTuple->Branch("eDep101Right",&m_eDep101Right,"m_eDep101Right/D");
      m_NTuple->Branch("chargeChannel101Right",&m_chargeChannel101Right,"m_chargeChannel101Right/I");
      m_NTuple->Branch("eDep102Right",&m_eDep102Right,"m_eDep102Right/D");
      m_NTuple->Branch("chargeChannel102Right",&m_chargeChannel102Right,"m_chargeChannel102Right/I");
      m_NTuple->Branch("eDep103Right",&m_eDep103Right,"m_eDep103Right/D");
      m_NTuple->Branch("chargeChannel103Right",&m_chargeChannel103Right,"m_chargeChannel103Right/I");
      m_NTuple->Branch("eDep104Right",&m_eDep104Right,"m_eDep104Right/D");
      m_NTuple->Branch("chargeChannel104Right",&m_chargeChannel104Right,"m_chargeChannel104Right/I");
      m_NTuple->Branch("eDep111Right",&m_eDep111Right,"m_eDep111Right/D");
      m_NTuple->Branch("chargeChannel111Right",&m_chargeChannel111Right,"m_chargeChannel111Right/I");
      m_NTuple->Branch("eDep112Right",&m_eDep112Right,"m_eDep112Right/D");
      m_NTuple->Branch("chargeChannel112Right",&m_chargeChannel112Right,"m_chargeChannel112Right/I");
      m_NTuple->Branch("eDep113Right",&m_eDep113Right,"m_eDep113Right/D");
      m_NTuple->Branch("chargeChannel113Right",&m_chargeChannel113Right,"m_chargeChannel113Right/I");
      m_NTuple->Branch("eDep114Right",&m_eDep114Right,"m_eDep114Right/D");
      m_NTuple->Branch("chargeChannel114Right",&m_chargeChannel114Right,"m_chargeChannel114Right/I");
      m_NTuple->Branch("eDep121Right",&m_eDep121Right,"m_eDep121Right/D");
      m_NTuple->Branch("chargeChannel121Right",&m_chargeChannel121Right,"m_chargeChannel121Right/I");
      m_NTuple->Branch("eDep122Right",&m_eDep122Right,"m_eDep122Right/D");
      m_NTuple->Branch("chargeChannel122Right",&m_chargeChannel122Right,"m_chargeChannel122Right/I");
      m_NTuple->Branch("eDep123Right",&m_eDep123Right,"m_eDep123Right/D");
      m_NTuple->Branch("chargeChannel123Right",&m_chargeChannel123Right,"m_chargeChannel123Right/I");
      m_NTuple->Branch("eDep124Right",&m_eDep124Right,"m_eDep124Right/D");
      m_NTuple->Branch("chargeChannel124Right",&m_chargeChannel124Right,"m_chargeChannel124Right/I");
      
      m_NTuple->Branch("eDep201Right",&m_eDep201Right,"m_eDep201Right/D");
      m_NTuple->Branch("chargeChannel201Right",&m_chargeChannel201Right,"m_chargeChannel201Right/I");
      m_NTuple->Branch("eDep202Right",&m_eDep202Right,"m_eDep202Right/D");
      m_NTuple->Branch("chargeChannel202Right",&m_chargeChannel202Right,"m_chargeChannel202Right/I");
      m_NTuple->Branch("eDep203Right",&m_eDep203Right,"m_eDep203Right/D");
      m_NTuple->Branch("chargeChannel203Right",&m_chargeChannel203Right,"m_chargeChannel203Right/I");
      m_NTuple->Branch("eDep204Right",&m_eDep204Right,"m_eDep204Right/D");
      m_NTuple->Branch("chargeChannel204Right",&m_chargeChannel204Right,"m_chargeChannel204Right/I");
      m_NTuple->Branch("eDep211Right",&m_eDep211Right,"m_eDep211Right/D");
      m_NTuple->Branch("chargeChannel211Right",&m_chargeChannel211Right,"m_chargeChannel211Right/I");
      m_NTuple->Branch("eDep212Right",&m_eDep212Right,"m_eDep212Right/D");
      m_NTuple->Branch("chargeChannel212Right",&m_chargeChannel212Right,"m_chargeChannel212Right/I");
      m_NTuple->Branch("eDep213Right",&m_eDep213Right,"m_eDep213Right/D");
      m_NTuple->Branch("chargeChannel213Right",&m_chargeChannel213Right,"m_chargeChannel213Right/I");
      m_NTuple->Branch("eDep214Right",&m_eDep214Right,"m_eDep214Right/D");
      m_NTuple->Branch("chargeChannel214Right",&m_chargeChannel214Right,"m_chargeChannel214Right/I");
      m_NTuple->Branch("eDep221Right",&m_eDep221Right,"m_eDep221Right/D");
      m_NTuple->Branch("chargeChannel221Right",&m_chargeChannel221Right,"m_chargeChannel221Right/I");
      m_NTuple->Branch("eDep222Right",&m_eDep222Right,"m_eDep222Right/D");
      m_NTuple->Branch("chargeChannel222Right",&m_chargeChannel222Right,"m_chargeChannel222Right/I");
      m_NTuple->Branch("eDep223Right",&m_eDep223Right,"m_eDep223Right/D");
      m_NTuple->Branch("chargeChannel223Right",&m_chargeChannel223Right,"m_chargeChannel223Right/I");
      m_NTuple->Branch("eDep224Right",&m_eDep224Right,"m_eDep224Right/D");
      m_NTuple->Branch("chargeChannel224Right",&m_chargeChannel224Right,"m_chargeChannel224Right/I"); 
      
			 m_NTuple->Branch("eDep101Left",&m_eDep101Left,"m_eDep101Left/D");
      m_NTuple->Branch("chargeChannel101Left",&m_chargeChannel101Left,"m_chargeChannel101Left/I");
      m_NTuple->Branch("eDep102Left",&m_eDep102Left,"m_eDep102Left/D");
      m_NTuple->Branch("chargeChannel102Left",&m_chargeChannel102Left,"m_chargeChannel102Left/I");
      m_NTuple->Branch("eDep103Left",&m_eDep103Left,"m_eDep103Left/D");
      m_NTuple->Branch("chargeChannel103Left",&m_chargeChannel103Left,"m_chargeChannel103Left/I");
      m_NTuple->Branch("eDep104Left",&m_eDep104Left,"m_eDep104Left/D");
      m_NTuple->Branch("chargeChannel104Left",&m_chargeChannel104Left,"m_chargeChannel104Left/I");
      m_NTuple->Branch("eDep111Left",&m_eDep111Left,"m_eDep111Left/D");
      m_NTuple->Branch("chargeChannel111Left",&m_chargeChannel111Left,"m_chargeChannel111Left/I");
      m_NTuple->Branch("eDep112Left",&m_eDep112Left,"m_eDep112Left/D");
      m_NTuple->Branch("chargeChannel112Left",&m_chargeChannel112Left,"m_chargeChannel112Left/I");
      m_NTuple->Branch("eDep113Left",&m_eDep113Left,"m_eDep113Left/D");
      m_NTuple->Branch("chargeChannel113Left",&m_chargeChannel113Left,"m_chargeChannel113Left/I");
      m_NTuple->Branch("eDep114Left",&m_eDep114Left,"m_eDep114Left/D");
      m_NTuple->Branch("chargeChannel114Left",&m_chargeChannel114Left,"m_chargeChannel114Left/I");
      m_NTuple->Branch("eDep121Left",&m_eDep121Left,"m_eDep121Left/D");
      m_NTuple->Branch("chargeChannel121Left",&m_chargeChannel121Left,"m_chargeChannel121Left/I");
      m_NTuple->Branch("eDep122Left",&m_eDep122Left,"m_eDep122Left/D");
      m_NTuple->Branch("chargeChannel122Left",&m_chargeChannel122Left,"m_chargeChannel122Left/I");
      m_NTuple->Branch("eDep123Left",&m_eDep123Left,"m_eDep123Left/D");
      m_NTuple->Branch("chargeChannel123Left",&m_chargeChannel123Left,"m_chargeChannel123Left/I");
      m_NTuple->Branch("eDep124Left",&m_eDep124Left,"m_eDep124Left/D");
      m_NTuple->Branch("chargeChannel124Left",&m_chargeChannel124Left,"m_chargeChannel124Left/I");
      
      m_NTuple->Branch("eDep201Left",&m_eDep201Left,"m_eDep201Left/D");
      m_NTuple->Branch("chargeChannel201Left",&m_chargeChannel201Left,"m_chargeChannel201Left/I");
      m_NTuple->Branch("eDep202Left",&m_eDep202Left,"m_eDep202Left/D");
      m_NTuple->Branch("chargeChannel202Left",&m_chargeChannel202Left,"m_chargeChannel202Left/I");
      m_NTuple->Branch("eDep203Left",&m_eDep203Left,"m_eDep203Left/D");
      m_NTuple->Branch("chargeChannel203Left",&m_chargeChannel203Left,"m_chargeChannel203Left/I");
      m_NTuple->Branch("eDep204Left",&m_eDep204Left,"m_eDep204Left/D");
      m_NTuple->Branch("chargeChannel204Left",&m_chargeChannel204Left,"m_chargeChannel204Left/I");
      m_NTuple->Branch("eDep211Left",&m_eDep211Left,"m_eDep211Left/D");
      m_NTuple->Branch("chargeChannel211Left",&m_chargeChannel211Left,"m_chargeChannel211Left/I");
      m_NTuple->Branch("eDep212Left",&m_eDep212Left,"m_eDep212Left/D");
      m_NTuple->Branch("chargeChannel212Left",&m_chargeChannel212Left,"m_chargeChannel212Left/I");
      m_NTuple->Branch("eDep213Left",&m_eDep213Left,"m_eDep213Left/D");
      m_NTuple->Branch("chargeChannel213Left",&m_chargeChannel213Left,"m_chargeChannel213Left/I");
      m_NTuple->Branch("eDep214Left",&m_eDep214Left,"m_eDep214Left/D");
      m_NTuple->Branch("chargeChannel214Left",&m_chargeChannel214Left,"m_chargeChannel214Left/I");
      m_NTuple->Branch("eDep221Left",&m_eDep221Left,"m_eDep221Left/D");
      m_NTuple->Branch("chargeChannel221Left",&m_chargeChannel221Left,"m_chargeChannel221Left/I");
      m_NTuple->Branch("eDep222Left",&m_eDep222Left,"m_eDep222Left/D");
      m_NTuple->Branch("chargeChannel222Left",&m_chargeChannel222Left,"m_chargeChannel222Left/I");
      m_NTuple->Branch("eDep223Left",&m_eDep223Left,"m_eDep223Left/D");
      m_NTuple->Branch("chargeChannel223Left",&m_chargeChannel223Left,"m_chargeChannel223Left/I");
      m_NTuple->Branch("eDep224Left",&m_eDep224Left,"m_eDep224Left/D");
      m_NTuple->Branch("chargeChannel224Left",&m_chargeChannel224Left,"m_chargeChannel224Left/I");
			
			 m_NTuple->Branch("eDep101",&m_eDep101,"m_eDep101/D");
      m_NTuple->Branch("chargeChannel101",&m_chargeChannel101,"m_chargeChannel101/I");
      m_NTuple->Branch("eDep102",&m_eDep102,"m_eDep102/D");
      m_NTuple->Branch("chargeChannel102",&m_chargeChannel102,"m_chargeChannel102/I");
      m_NTuple->Branch("eDep103",&m_eDep103,"m_eDep103/D");
      m_NTuple->Branch("chargeChannel103",&m_chargeChannel103,"m_chargeChannel103/I");
      m_NTuple->Branch("eDep104",&m_eDep104,"m_eDep104/D");
      m_NTuple->Branch("chargeChannel104",&m_chargeChannel104,"m_chargeChannel104/I");
      m_NTuple->Branch("eDep111",&m_eDep111,"m_eDep111/D");
      m_NTuple->Branch("chargeChannel111",&m_chargeChannel111,"m_chargeChannel111/I");
      m_NTuple->Branch("eDep112",&m_eDep112,"m_eDep112/D");
      m_NTuple->Branch("chargeChannel112",&m_chargeChannel112,"m_chargeChannel112/I");
      m_NTuple->Branch("eDep113",&m_eDep113,"m_eDep113/D");
      m_NTuple->Branch("chargeChannel113",&m_chargeChannel113,"m_chargeChannel113/I");
      m_NTuple->Branch("eDep114",&m_eDep114,"m_eDep114/D");
      m_NTuple->Branch("chargeChannel114",&m_chargeChannel114,"m_chargeChannel114/I");
      m_NTuple->Branch("eDep121",&m_eDep121,"m_eDep121/D");
      m_NTuple->Branch("chargeChannel121",&m_chargeChannel121,"m_chargeChannel121/I");
      m_NTuple->Branch("eDep122",&m_eDep122,"m_eDep122/D");
      m_NTuple->Branch("chargeChannel122",&m_chargeChannel122,"m_chargeChannel122/I");
      m_NTuple->Branch("eDep123",&m_eDep123,"m_eDep123/D");
      m_NTuple->Branch("chargeChannel123",&m_chargeChannel123,"m_chargeChannel123/I");
      m_NTuple->Branch("eDep124",&m_eDep124,"m_eDep124/D");
      m_NTuple->Branch("chargeChannel124",&m_chargeChannel124,"m_chargeChannel124/I");
      
      m_NTuple->Branch("eDep201",&m_eDep201,"m_eDep201/D");
      m_NTuple->Branch("chargeChannel201",&m_chargeChannel201,"m_chargeChannel201/I");
      m_NTuple->Branch("eDep202",&m_eDep202,"m_eDep202/D");
      m_NTuple->Branch("chargeChannel202",&m_chargeChannel202,"m_chargeChannel202/I");
      m_NTuple->Branch("eDep203",&m_eDep203,"m_eDep203/D");
      m_NTuple->Branch("chargeChannel203",&m_chargeChannel203,"m_chargeChannel203/I");
      m_NTuple->Branch("eDep204",&m_eDep204,"m_eDep204/D");
      m_NTuple->Branch("chargeChannel204",&m_chargeChannel204,"m_chargeChannel204/I");
      m_NTuple->Branch("eDep211",&m_eDep211,"m_eDep211/D");
      m_NTuple->Branch("chargeChannel211",&m_chargeChannel211,"m_chargeChannel211/I");
      m_NTuple->Branch("eDep212",&m_eDep212,"m_eDep212/D");
      m_NTuple->Branch("chargeChannel212",&m_chargeChannel212,"m_chargeChannel212/I");
      m_NTuple->Branch("eDep213",&m_eDep213,"m_eDep213/D");
      m_NTuple->Branch("chargeChannel213",&m_chargeChannel213,"m_chargeChannel213/I");
      m_NTuple->Branch("eDep214",&m_eDep214,"m_eDep214/D");
      m_NTuple->Branch("chargeChannel214",&m_chargeChannel214,"m_chargeChannel214/I");
      m_NTuple->Branch("eDep221",&m_eDep221,"m_eDep221/D");
      m_NTuple->Branch("chargeChannel221",&m_chargeChannel221,"m_chargeChannel221/I");
      m_NTuple->Branch("eDep222",&m_eDep222,"m_eDep222/D");
      m_NTuple->Branch("chargeChannel222",&m_chargeChannel222,"m_chargeChannel222/I");
      m_NTuple->Branch("eDep223",&m_eDep223,"m_eDep223/D");
      m_NTuple->Branch("chargeChannel223",&m_chargeChannel223,"m_chargeChannel223/I");
      m_NTuple->Branch("eDep224",&m_eDep224,"m_eDep224/D");
      m_NTuple->Branch("chargeChannel224",&m_chargeChannel224,"m_chargeChannel224/I");
			
      m_NTuple->Branch("xImpact",&m_xImpact,"m_xImpact/D");
      m_NTuple->Branch("yImpact",&m_yImpact,"m_yImpact/D");
			m_NTuple->Branch("zImpact",&m_zImpact,"m_zImpact/D");
			
			m_NTuple->Branch("PxImpact",&m_PxImpact,"m_PxImpact/D");
			m_NTuple->Branch("PyImpact",&m_PyImpact,"m_PyImpact/D");
			m_NTuple->Branch("PzImpact",&m_PzImpact,"m_PzImpact/D");
			
			m_NTuple->Branch("FirstImpactPID",&m_FirstImpactPID,"m_FirstImpactPID/I");
			m_NTuple->Branch("xImpactElec",&m_xImpactElec,"m_xImpactElec/D");
      m_NTuple->Branch("yImpactElec",&m_yImpactElec,"m_yImpactElec/D");
			m_NTuple->Branch("xImpactPositron",&m_xImpactPositron,"m_xImpactPositron/D");
      m_NTuple->Branch("yImpactPositron",&m_yImpactPositron,"m_yImpactPositron/D");
			
		m_NTuple->Branch("Ntracks",&m_Ntracks,"m_Ntracks/I");
		m_NTuple->Branch("xImpacts",&m_xImpacts);
		m_NTuple->Branch("yImpacts",&m_yImpacts);
		m_NTuple->Branch("zImpacts",&m_zImpacts);
		
		//m_NTuple->Branch("Trajectories",&m_Trajectories,32000,99);
		//m_NTuple->Branch("Trajectory",&m_Trajectory);
		
		//m_NTuple->Branch("Trajectories","std::vector<TPolyLine3D>",&m_Trajectories,64000,-1);
		//m_NTuple->Branch("Trajectory","TPolyLine3D",&m_Trajectory);
		
		//m_NTuple->Branch("TrajTVector3","std::vector<std::vector<TVector3> >",&m_TrajTVector3);
		
		m_NTuple->Branch("trackIDStep",&m_trackIDStep);
		m_NTuple->Branch("parentIDStep",&m_parentIDStep);
		m_NTuple->Branch("stepID",&m_StepID);
		m_NTuple->Branch("PDGCodeStep",&m_PDGCodeStep);
		
		m_NTuple->Branch("xPreStep",&m_xPreStep);
		m_NTuple->Branch("yPreStep",&m_yPreStep);
		m_NTuple->Branch("zPreStep",&m_zPreStep);
		
		m_NTuple->Branch("PxPreStep",&m_PxPreStep);
		m_NTuple->Branch("PyPreStep",&m_PyPreStep);
		m_NTuple->Branch("PzPreStep",&m_PzPreStep);
		
		m_NTuple->Branch("xPostStep",&m_xPostStep);
		m_NTuple->Branch("yPosttep",&m_yPostStep);
		m_NTuple->Branch("zPostStep",&m_zPostStep);
		
		m_NTuple->Branch("PxPostStep",&m_PxPostStep);
		m_NTuple->Branch("PyPostStep",&m_PyPostStep);
		m_NTuple->Branch("PzPostStep",&m_PzPostStep);
		
		m_NTuple->Branch("eDepStep",&m_eDepStep);
		
		m_NTuple->Branch("VolumePreStep",&m_VolumePreStep);	
		m_NTuple->Branch("VolumePostStep",&m_VolumePostStep);
		
}

BesEventAction::~BesEventAction()
{

m_File->cd();
m_NTuple->Write();
m_File->Write();
m_File->Close();
m_NTuple = NULL;
m_File = NULL;

}

void BesEventAction::BeginOfEventAction(const G4Event* evt)
{

    G4int eventId = evt->GetEventID();
    //if (eventId%5000==0)
    //    G4cout<<"---> Begin of event: "<<eventId<<G4endl;

    G4UImanager* uiMgr = G4UImanager::GetUIpointer();
    //if(eventId == 46 || eventId == 75)
    // uiMgr->ApplyCommand("/tracking/verbose 1");
    //else
    //  uiMgr->ApplyCommand("/tracking/verbose 0");

    //mc truth
    if (m_runAction)
        if (m_runAction->GetMCTruthFlag()!=0)
        {
            BesSensitiveManager* sensitiveManager = BesSensitiveManager::GetSensitiveManager();
            sensitiveManager->BeginOfTruthEvent(evt);
        }
		
		InitializeNTupleVariables();

}

void BesEventAction::EndOfEventAction(const G4Event* evt)
{
    if (G4VVisManager::GetConcreteInstance())
    {
        G4TrajectoryContainer* trajectoryContainer = evt->GetTrajectoryContainer();
        G4int n_trajectories = 0;
        if (trajectoryContainer) n_trajectories = trajectoryContainer->entries();

        for (G4int i=0; i<n_trajectories; i++)
        {
            G4Trajectory* trj = (G4Trajectory*)
                                ((*(evt->GetTrajectoryContainer()))[i]);

            trj->DrawTrajectory(50);

        }
    }

    //mc truth
    BesSensitiveManager* sensitiveManager = BesSensitiveManager::GetSensitiveManager();
    if (m_runAction)
        if (m_runAction->GetMCTruthFlag()!=0)
        {
            sensitiveManager->EndOfTruthEvent(evt);
        }

    if (ReadBoostRoot::GetHitOut()){
        if (m_runAction)
        {
            //Ascii I/O, output hit collections
            BesAsciiIO* asciiIO = m_runAction->GetAsciiIO();
            if (asciiIO) {
                if (m_RealizationSvc->UseDBFlag() == true) asciiIO->SaveHitAsciiEvents(m_RealizationSvc->getRunId(), evt);
                else asciiIO->SaveHitAsciiEvents(m_runAction->GetRunId(), evt);
            }

            //Root I/O,  output MdcHit collections
            BesRootIO* rootIO = m_runAction->GetRootIO();
            if (rootIO){
                //emc digitization
                if (ReadBoostRoot::GetEmc()) m_DM->Digitize("BesEmcDigitizer");

                if (m_RealizationSvc->UseDBFlag() == true) rootIO->SaveHitRootEvent(m_RealizationSvc->getRunId(), evt);
                else rootIO->SaveHitRootEvent(m_runAction->GetRunId(), evt);
            }
        }
    }else{
        //Tuning I/O
        if (ReadBoostRoot::GetTuning()){
            if (m_runAction)
            {
                BesTuningIO* tuningIO = m_runAction->GetTuningIO();
                if (tuningIO){
                    if (ReadBoostRoot::GetFormatAR())
                    {
                        tuningIO->GetRootEvent(evt->GetEventID());
                    }
                    else{
                        tuningIO->GetNextEvents();
                    }
                }

            }
        }
        //mdc digitization

        if (ReadBoostRoot::GetMdc())
            m_DM->Digitize("BesMdcDigitizer");

        //tof digitization
        if (ReadBoostRoot::GetTof())
            m_DM->Digitize("BesTofDigitizer");

        //emc digitization
        if ((!ReadBoostRoot::GetTuning())&&ReadBoostRoot::GetEmc())
            m_DM->Digitize("BesEmcDigitizer");

        //muc digitization
        if (ReadBoostRoot::GetMuc())
            m_DM->Digitize("BesMucDigitizer");
	
	//zdd digitization
	if (ReadBoostRoot::GetZdd())
	    m_DM->Digitize("BesZddDigitizer");


        if (m_runAction)
        {
        //Ascii I/O, output digi collections
            BesAsciiIO* asciiIO = m_runAction->GetAsciiIO();
            if (asciiIO)
            {
                if (m_RealizationSvc->UseDBFlag() == true) asciiIO->SaveAsciiEvents(m_RealizationSvc->getRunId(), evt);
                else asciiIO->SaveAsciiEvents(m_runAction->GetRunId(), evt);
            }
              
        // Root I/O,output digi collections
            BesTDSWriter* tdsWriter =  m_runAction->GetTDSWriter();
            if (tdsWriter)
            {
                if (m_RealizationSvc->UseDBFlag() == true) tdsWriter->SaveAll(evt, m_RealizationSvc->getRunId() );
                else tdsWriter->SaveAll(evt, m_runAction->GetRunId() );
//                 std::cout<<"BesEventAction::EndOfEventAction(const G4Event* evt):    "<<ReadBoostRoot::GetZdd()<<std::endl;
            }
        }
    }
		 BesZddDigitizer* zddDM = (BesZddDigitizer*)m_DM->FindDigitizerModule("BesZddDigitizer");
		
		m_evtID=evt->GetEventID();
		
		m_zdd_energy=zddDM->m_zdd_energy;
		m_zddRight_energy=zddDM->m_zddRight_energy;
		m_zddLeft_energy=zddDM->m_zddLeft_energy;
		
		 m_eDep101=zddDM->m_eDep101;
   m_eDep102=zddDM->m_eDep102;
   m_eDep103=zddDM->m_eDep103;
   m_eDep104=zddDM->m_eDep104;
   m_eDep111=zddDM->m_eDep111;
   m_eDep112=zddDM->m_eDep112;
   m_eDep113=zddDM->m_eDep113;
   m_eDep114=zddDM->m_eDep114;
   m_eDep121=zddDM->m_eDep121;
   m_eDep122=zddDM->m_eDep122;
   m_eDep123=zddDM->m_eDep123;
   m_eDep124=zddDM->m_eDep124;

   m_eDep201=zddDM->m_eDep201;
   m_eDep202=zddDM->m_eDep202;
   m_eDep203=zddDM->m_eDep203;
   m_eDep204=zddDM->m_eDep204;
   m_eDep211=zddDM->m_eDep211;
   m_eDep212=zddDM->m_eDep212;
   m_eDep213=zddDM->m_eDep213;
   m_eDep214=zddDM->m_eDep214;
   m_eDep221=zddDM->m_eDep221;
   m_eDep222=zddDM->m_eDep222;
   m_eDep223=zddDM->m_eDep223;
   m_eDep224=zddDM->m_eDep224;
	 
	 m_chargeChannel101=zddDM->m_chargeChannel101;
   m_chargeChannel102=zddDM->m_chargeChannel102;
   m_chargeChannel103=zddDM->m_chargeChannel103;
   m_chargeChannel104=zddDM->m_chargeChannel104;
   m_chargeChannel111=zddDM->m_chargeChannel111;
   m_chargeChannel112=zddDM->m_chargeChannel112;
   m_chargeChannel113=zddDM->m_chargeChannel113;
   m_chargeChannel114=zddDM->m_chargeChannel114;
   m_chargeChannel121=zddDM->m_chargeChannel121;
   m_chargeChannel122=zddDM->m_chargeChannel122;
   m_chargeChannel123=zddDM->m_chargeChannel123;
   m_chargeChannel124=zddDM->m_chargeChannel124;

   m_chargeChannel201=zddDM->m_chargeChannel201;
   m_chargeChannel202=zddDM->m_chargeChannel202;
   m_chargeChannel203=zddDM->m_chargeChannel203;
   m_chargeChannel204=zddDM->m_chargeChannel204;
   m_chargeChannel211=zddDM->m_chargeChannel211;
   m_chargeChannel212=zddDM->m_chargeChannel212;
   m_chargeChannel213=zddDM->m_chargeChannel213;
   m_chargeChannel214=zddDM->m_chargeChannel214;
   m_chargeChannel221=zddDM->m_chargeChannel221;
   m_chargeChannel222=zddDM->m_chargeChannel222;
   m_chargeChannel223=zddDM->m_chargeChannel223;
   m_chargeChannel224=zddDM->m_chargeChannel224;
	 
		 m_eDep101Right=zddDM->m_eDep101Right;
   m_eDep102Right=zddDM->m_eDep102Right;
   m_eDep103Right=zddDM->m_eDep103Right;
   m_eDep104Right=zddDM->m_eDep104Right;
   m_eDep111Right=zddDM->m_eDep111Right;
   m_eDep112Right=zddDM->m_eDep112Right;
   m_eDep113Right=zddDM->m_eDep113Right;
   m_eDep114Right=zddDM->m_eDep114Right;
   m_eDep121Right=zddDM->m_eDep121Right;
   m_eDep122Right=zddDM->m_eDep122Right;
   m_eDep123Right=zddDM->m_eDep123Right;
   m_eDep124Right=zddDM->m_eDep124Right;

   m_eDep201Right=zddDM->m_eDep201Right;
   m_eDep202Right=zddDM->m_eDep202Right;
   m_eDep203Right=zddDM->m_eDep203Right;
   m_eDep204Right=zddDM->m_eDep204Right;
   m_eDep211Right=zddDM->m_eDep211Right;
   m_eDep212Right=zddDM->m_eDep212Right;
   m_eDep213Right=zddDM->m_eDep213Right;
   m_eDep214Right=zddDM->m_eDep214Right;
   m_eDep221Right=zddDM->m_eDep221Right;
   m_eDep222Right=zddDM->m_eDep222Right;
   m_eDep223Right=zddDM->m_eDep223Right;
   m_eDep224Right=zddDM->m_eDep224Right;
	 
	 m_chargeChannel101Right=zddDM->m_chargeChannel101Right;
   m_chargeChannel102Right=zddDM->m_chargeChannel102Right;
   m_chargeChannel103Right=zddDM->m_chargeChannel103Right;
   m_chargeChannel104Right=zddDM->m_chargeChannel104Right;
   m_chargeChannel111Right=zddDM->m_chargeChannel111Right;
   m_chargeChannel112Right=zddDM->m_chargeChannel112Right;
   m_chargeChannel113Right=zddDM->m_chargeChannel113Right;
   m_chargeChannel114Right=zddDM->m_chargeChannel114Right;
   m_chargeChannel121Right=zddDM->m_chargeChannel121Right;
   m_chargeChannel122Right=zddDM->m_chargeChannel122Right;
   m_chargeChannel123Right=zddDM->m_chargeChannel123Right;
   m_chargeChannel124Right=zddDM->m_chargeChannel124Right;

   m_chargeChannel201Right=zddDM->m_chargeChannel201Right;
   m_chargeChannel202Right=zddDM->m_chargeChannel202Right;
   m_chargeChannel203Right=zddDM->m_chargeChannel203Right;
   m_chargeChannel204Right=zddDM->m_chargeChannel204Right;
   m_chargeChannel211Right=zddDM->m_chargeChannel211Right;
   m_chargeChannel212Right=zddDM->m_chargeChannel212Right;
   m_chargeChannel213Right=zddDM->m_chargeChannel213Right;
   m_chargeChannel214Right=zddDM->m_chargeChannel214Right;
   m_chargeChannel221Right=zddDM->m_chargeChannel221Right;
   m_chargeChannel222Right=zddDM->m_chargeChannel222Right;
   m_chargeChannel223Right=zddDM->m_chargeChannel223Right;
   m_chargeChannel224Right=zddDM->m_chargeChannel224Right;
	
	m_eDep101Left=zddDM->m_eDep101Left;
   m_eDep102Left=zddDM->m_eDep102Left;
   m_eDep103Left=zddDM->m_eDep103Left;
   m_eDep104Left=zddDM->m_eDep104Left;
   m_eDep111Left=zddDM->m_eDep111Left;
   m_eDep112Left=zddDM->m_eDep112Left;
   m_eDep113Left=zddDM->m_eDep113Left;
   m_eDep114Left=zddDM->m_eDep114Left;
   m_eDep121Left=zddDM->m_eDep121Left;
   m_eDep122Left=zddDM->m_eDep122Left;
   m_eDep123Left=zddDM->m_eDep123Left;
   m_eDep124Left=zddDM->m_eDep124Left;

   m_eDep201Left=zddDM->m_eDep201Left;
   m_eDep202Left=zddDM->m_eDep202Left;
   m_eDep203Left=zddDM->m_eDep203Left;
   m_eDep204Left=zddDM->m_eDep204Left;
   m_eDep211Left=zddDM->m_eDep211Left;
   m_eDep212Left=zddDM->m_eDep212Left;
   m_eDep213Left=zddDM->m_eDep213Left;
   m_eDep214Left=zddDM->m_eDep214Left;
   m_eDep221Left=zddDM->m_eDep221Left;
   m_eDep222Left=zddDM->m_eDep222Left;
   m_eDep223Left=zddDM->m_eDep223Left;
   m_eDep224Left=zddDM->m_eDep224Left;
	 
	 m_chargeChannel101Left=zddDM->m_chargeChannel101Left;
   m_chargeChannel102Left=zddDM->m_chargeChannel102Left;
   m_chargeChannel103Left=zddDM->m_chargeChannel103Left;
   m_chargeChannel104Left=zddDM->m_chargeChannel104Left;
   m_chargeChannel111Left=zddDM->m_chargeChannel111Left;
   m_chargeChannel112Left=zddDM->m_chargeChannel112Left;
   m_chargeChannel113Left=zddDM->m_chargeChannel113Left;
   m_chargeChannel114Left=zddDM->m_chargeChannel114Left;
   m_chargeChannel121Left=zddDM->m_chargeChannel121Left;
   m_chargeChannel122Left=zddDM->m_chargeChannel122Left;
   m_chargeChannel123Left=zddDM->m_chargeChannel123Left;
   m_chargeChannel124Left=zddDM->m_chargeChannel124Left;

   m_chargeChannel201Left=zddDM->m_chargeChannel201Left;
   m_chargeChannel202Left=zddDM->m_chargeChannel202Left;
   m_chargeChannel203Left=zddDM->m_chargeChannel203Left;
   m_chargeChannel204Left=zddDM->m_chargeChannel204Left;
   m_chargeChannel211Left=zddDM->m_chargeChannel211Left;
   m_chargeChannel212Left=zddDM->m_chargeChannel212Left;
   m_chargeChannel213Left=zddDM->m_chargeChannel213Left;
   m_chargeChannel214Left=zddDM->m_chargeChannel214Left;
   m_chargeChannel221Left=zddDM->m_chargeChannel221Left;
   m_chargeChannel222Left=zddDM->m_chargeChannel222Left;
   m_chargeChannel223Left=zddDM->m_chargeChannel223Left;
   m_chargeChannel224Left=zddDM->m_chargeChannel224Left;	
	
	 m_xImpact=zddDM->m_xImpact;	
	 m_yImpact=zddDM->m_yImpact;	
	 m_zImpact=zddDM->m_zImpact;	
	 
	 m_PxImpact=zddDM->m_PxImpact;	
	 m_PyImpact=zddDM->m_PyImpact;	
	 m_PzImpact=zddDM->m_PzImpact;
	 
	 m_FirstImpactPID=zddDM->m_FirstImpactPID;	
	 m_xImpactElec=zddDM->m_xImpactElec;	
	 m_yImpactElec=zddDM->m_yImpactElec;
	 
	 m_xImpactPositron=zddDM->m_xImpactPositron;	
	 m_yImpactPositron=zddDM->m_yImpactPositron;
	 
		m_Ntracks=m_ZDDImpactPoints.size();
		for(int k=0;k<m_ZDDImpactPoints.size();k++)
			{
			m_xImpacts.push_back(m_ZDDImpactPoints[k].x());
			m_yImpacts.push_back(m_ZDDImpactPoints[k].y());	
			m_zImpacts.push_back(m_ZDDImpactPoints[k].z());			
			}
		/*
		int counter=0;
		for (std::map<G4int,std::vector<G4ThreeVector> > ::iterator it=m_mapTrackPoints.begin(); it!=m_mapTrackPoints.end(); ++it)
			{
			std::vector<G4ThreeVector> traj=it->second;
			TPolyLine3D poly3D = MakeTPolyLine3D(traj);
			m_Trajectories.push_back(poly3D);
			if(counter==0) m_Trajectory=m_Trajectories.back();
			
			std::vector<TVector3> TrajTVector3=MakeTrajectory(traj);
			m_TrajTVector3.push_back(TrajTVector3);
			
			FillTrajectoryPreStep(traj);
			
			counter++;
			}
			*/
			//m_Trajectory=m_Trajectories[0];
		//printf("BesEventAction::EndofEventAction m_Trajectories.size()=%d \n",m_Trajectories.size());
				
		m_NTuple->Fill();
		
    sensitiveManager->ClearEvent();
    //HepRandom::saveEngineStatus("EndSimCurrEvt.rndm");
}

void BesEventAction::InitializeNTupleVariables()
{
m_Ntracks=0;
		
		m_ZDDImpactPoints.clear();
		
		m_xImpacts.clear();
		m_yImpacts.clear();
		m_zImpacts.clear();
		
		m_evtIDs.clear();
			
		m_mapTrackPoints.clear();
		m_Trajectories.clear();
		
		m_TrajTVector3.clear();
		
		m_trackIDStep.clear();
		m_parentIDStep.clear();
		m_StepID.clear();
		m_PDGCodeStep.clear();
		
		m_xPreStep.clear();
		m_yPreStep.clear();
		m_zPreStep.clear();
		
		m_PxPreStep.clear();
		m_PyPreStep.clear();
		m_PzPreStep.clear();
		
		m_xPostStep.clear();
		m_yPostStep.clear();
		m_zPostStep.clear();
		
		m_PxPostStep.clear();
		m_PyPostStep.clear();
		m_PzPostStep.clear();
		
		m_eDepStep.clear();
		
		m_VolumePreStep.clear();
		m_VolumePostStep.clear();

}

void BesEventAction::AddPointToTrajectory(G4int trackID,G4ThreeVector vec)
{
m_mapTrackPoints[trackID].push_back(vec);	
}


TPolyLine3D BesEventAction::MakeTPolyLine3D(std::vector<G4ThreeVector> traj)
{
TPolyLine3D poly3D(traj.size());
for(int k=0;k<traj.size();k++)
	{
	poly3D.SetPoint(k,traj[k].x(),traj[k].y(),traj[k].z());
	}
return poly3D;	
}

std::vector<TVector3> BesEventAction::MakeTrajectory(std::vector<G4ThreeVector> traj)
{
std::vector<TVector3> vecTVector3;

for(int k=0;k<traj.size();k++)
	{
	vecTVector3.push_back(TVector3(traj[k].x(),traj[k].y(),traj[k].z()));
	//poly3D.SetPoint(k,traj[k].x(),traj[k].y(),traj[k].z());
	}
return vecTVector3;	
}

void BesEventAction::FillTrajectoryPreStep(std::vector<G4ThreeVector> traj)
{
for(int k=0;k<traj.size();k++)
	{
	m_xPreStep.push_back(traj[k].x());
	m_yPreStep.push_back(traj[k].y());
	m_zPreStep.push_back(traj[k].z());
	}
}



void BesEventAction::FillNTupleStepVariables(G4int trackID, G4int parentID, G4int stepID, G4int pdgCode, G4ThreeVector preStepVec, G4ThreeVector preStepMom)
{
this->FillStepTrackID(trackID);
this->FillStepParentID(parentID);

this->FillStepID(stepID);
this->FillStepPDG(pdgCode);
this->FillStepPreStepVec(preStepVec);
this->FillStepPreStepMom(preStepMom);

}

