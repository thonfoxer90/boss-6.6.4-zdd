
#include "BesSteppingAction.hh"
#include "G4StepPoint.hh"
#include "G4ThreeVector.hh"
#include "G4Track.hh"

#include "G4UnitsTable.hh"
#include "G4VProcess.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include <fstream>

G4double BesSteppingAction:: m_x = -99.;
G4double BesSteppingAction:: m_y = -99.;

BesSteppingAction::BesSteppingAction()
{
    m_dump_flag = false;
    m_dump_name = "test.root";
}

BesSteppingAction::BesSteppingAction(bool dump_flag = false, std::string dump_name = "test.root")
{
    m_dump_flag = dump_flag;
    m_dump_name = dump_name;
    
    if (m_dump_flag) {
	m_File = new TFile(m_dump_name.c_str(), "recreate");
	m_tree = new TTree("tree", "tree");
	
	m_tree->Branch("evtID", &m_evtID, "evtID/I");
	m_tree->Branch("trackID", &m_trackID, "trackID/I");
	m_tree->Branch("stepID", &m_stepID, "stepID/I");
	m_tree->Branch("pdgCode", &m_pdgCode, "pdgCode/I");
	
	m_tree->Branch("preX", &m_preX, "preX/D");
	m_tree->Branch("preY", &m_preY, "preY/D");
	m_tree->Branch("preZ", &m_preZ, "preZ/D");
	m_tree->Branch("postX", &m_postX, "postX/D");
	m_tree->Branch("postY", &m_postY, "postY/D");
	m_tree->Branch("postZ", &m_postZ, "postZ/D");
	
	m_tree->Branch("eDep", &m_eDep, "eDep/D");
    }
}

BesSteppingAction::~BesSteppingAction()
{
    if (m_dump_flag) {
	m_File->cd();
	m_tree->Write();
	m_File->Write();
	m_File->Close();
	m_tree = NULL;
	m_File = NULL;
    }
    std::cout << "BesSteppingAction::~BesSteppingAction()" << std::endl;
}


void BesSteppingAction::UserSteppingAction(const G4Step* currentStep)
{

  G4StepPoint* postStep = currentStep->GetPostStepPoint();
 
  // check for volume changes: particle enters Zdd
  G4int crystalNo = abs(postStep->GetTouchableHandle()->GetCopyNumber()); //absolute value of crystal No, forget if east or west part...
  //! marcel
  G4StepPoint* uPreStep = currentStep->GetPreStepPoint();
  G4ThreeVector currentPosition = postStep->GetPosition();
  G4Track* currentTrack = currentStep->GetTrack();
    if( fabs(currentPosition.z())/cm > 334.9995 && fabs(currentPosition.z())/cm < 335.0005 && fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() )
    //if(/* fabs(currentPosition.z())/cm > 180. && fabs(currentPosition.z())/cm < 220 && fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() */)
//    if(1)
    {


    

      //      double uTheta = atan( sqrt( pow(currentPosition.x()/cm, 2) + pow(currentPosition.y()/cm, 2) )/fabs(currentPosition.z()/cm) );// * 180. / 3.1415;
      BesSteppingAction::m_x = currentPosition.x()/cm;
      BesSteppingAction::m_y = currentPosition.y()/cm;
/*
      double px = postStep->GetMomentum().x()/MeV;
      double py = postStep->GetMomentum().y()/MeV;
      double pz = postStep->GetMomentum().z()/MeV;
      double e = sqrt( px*px + py*py + pz*pz);
      double momentum_theta = atan2(sqrt(px*px + py*py),pz);

      G4cout << "BesSim::BesSteppingAction::UserSteppingAction : primary particle px py pz e [MeV] theta : " << px << "\t" << py << "\t" << pz << "\t" << e << "\t" << momentum_theta << G4endl;
      G4cout << " x, y, z [cm] : " << currentPosition.x()/cm << " , " << currentPosition.y()/cm << " , " << currentPosition.z()/cm << G4endl;
      G4cout << " phi [rad]   :    " << atan2( currentPosition.y(), currentPosition.x() ) << G4endl; // * 180. / 3.1415 << G4endl;

      G4cout << " preStep Theta from p at ZDD [rad] :    " << uPreStep->GetMomentum().theta() << G4endl;
      G4cout << " postStep Theta from p at ZDD [rad] :    " << postStep->GetMomentum().theta() << G4endl; 
      G4cout << " preStep Phi from p at ZDD [rad] :    " << uPreStep->GetMomentum().phi() << G4endl;
      G4cout << " postStep Phi from p at ZDD [rad] :    " << postStep->GetMomentum().phi() << G4endl << "==========================" << G4endl;;

      G4cout << "Name:   "<<(postStep->GetTouchableHandle()->GetVolume())->GetName()<<G4endl; 
      G4cout  << "PDG:   "<<currentStep->GetTrack()->GetDefinition()->GetPDGEncoding()<<G4endl;
*/

       // if( fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && fabs(currentPosition.x())/cm <=2. && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() )
      //	{

      /*
      if(crystalNo == 101 || crystalNo == 102 || crystalNo == 103 || crystalNo == 104 || crystalNo == 111 || crystalNo == 112 || crystalNo == 113 || crystalNo == 114 || crystalNo == 121 || crystalNo == 122 || crystalNo == 123 || crystalNo == 124 || crystalNo == 201 || crystalNo == 202 || crystalNo == 203 || crystalNo == 204 || crystalNo == 211 || crystalNo == 212 || crystalNo == 213 || crystalNo == 214 || crystalNo == 221 || crystalNo == 222 || crystalNo == 223 || crystalNo == 224)
      {*/
      /*    
          double uTheta = atan( sqrt( pow(currentPosition.x()/cm, 2) + pow(currentPosition.y()/cm, 2) )/fabs(currentPosition.z()/cm) );// * 180. / 3.1415;
	  
	  double px = postStep->GetMomentum().x()/MeV;
	  double py = postStep->GetMomentum().y()/MeV;
	  double pz = postStep->GetMomentum().z()/MeV;
	  double e = sqrt( px*px + py*py + pz*pz);
	  double momentum_theta = atan2(sqrt(px*px + py*py),pz);
	  
	  G4cout << "BesSim::BesSteppingAction::UserSteppingAction : primary particle px py pz e [MeV] theta : " << px << "\t" << py << "\t" << pz << "\t" << e << "\t" << momentum_theta << G4endl;
	  G4cout << "BesSim::BesSteppingAction::UserSteppingAction : Particle enters the Zdd !!!" << G4endl;
	  G4cout << " x, y, z [cm] : " << currentPosition.x()/cm << " , " << currentPosition.y()/cm << " , " << currentPosition.z()/cm << G4endl;
	  G4cout << " theta [rad] :    " << uTheta << G4endl;
	  G4cout << " phi [rad]   :    " << atan2( currentPosition.y(), currentPosition.x() ) << G4endl; // * 180. / 3.1415 << G4endl;
          G4cout << " crystalNo : " << postStep->GetTouchableHandle()->GetCopyNumber() << G4endl << G4endl;

	  G4cout << " preStep Theta from p at ZDD [rad] :    " << uPreStep->GetMomentum().theta() << G4endl;
          G4cout << " postStep Theta from p at ZDD [rad] :    " << postStep->GetMomentum().theta() << G4endl; 
	  G4cout << " preStep Phi from p at ZDD [rad] :    " << uPreStep->GetMomentum().phi() << G4endl;
          G4cout << " postStep Phi from p at ZDD [rad] :    " << postStep->GetMomentum().phi() << G4endl << "==========================" << G4endl;;
*/       
	  //	}

    }
    else if( fabs(currentPosition.z())/cm >= 355. )
    {
      BesSteppingAction::m_x = -99.;
      BesSteppingAction::m_y   = -99.;
   //      G4cout << "Kill track because it is not in ZDD" << G4endl;
      //      currentTrack->SetTrackStatus(fKillTrackAndSecondaries);      
    }

  G4bool trackKilled = false;
  //! marcel : ATTENTION:  change this back to abs(currentPosition.z()) > 360.*cm to enable zdd readout
  //  G4Track* currentTrack = currentStep->GetTrack();
  if(std::abs(currentPosition.x()) > 263.5*cm||
     std::abs(currentPosition.y()) > 263.5*cm||
     std::abs(currentPosition.z()) > 360.*cm){
    //    G4cout<<"BesSim :: BesSteppingAction: Out of World!!! "<< G4endl;// << "x[cm] = " << abs(currentPosition.x())/cm << G4endl
    //	  << "y[cm] = " << abs(currentPosition.y())/cm << G4endl << "z[cm] = " << abs(currentPosition.z())/cm << G4endl
    //	  << "=====================" << G4endl;
    currentTrack->SetTrackStatus(fKillTrackAndSecondaries);
  }else if(currentTrack->GetCurrentStepNumber()>=20000){  //20000 before
    G4cout<<"BesSim :: BesSteppingAction: StepNumber>=20000 !!!"<<G4endl; 
    
    trackKilled = true;
    currentTrack->SetTrackStatus(fKillTrackAndSecondaries);
  }
  /*
  if(std::abs(currentPosition.x()) <5*cm&&
     std::abs(currentPosition.y()) < 5*cm&&
     std::abs(currentPosition.z()) > 330.*cm){
    G4cout<<"BesSim :: BesSteppingAction: close to Zdd! "<< G4endl << "x[cm] = " << abs(currentPosition.x())/cm << G4endl
          << "y[cm] = " << abs(currentPosition.y())/cm << G4endl << "z[cm] = " << abs(currentPosition.z())/cm << G4endl
          << "=====================" << G4endl;

G4cout<<"#Step# "<<currentTrack->GetCurrentStepNumber()<<" pName "<<currentTrack->GetDefinition()->GetParticleName()<<" prex prey prez "<<currentStep->GetPreStepPoint()->GetPosition().x()/mm<<" "<<currentStep->GetPreStepPoint()->GetPosition().y()/mm<<" "<<currentStep->GetPreStepPoint()->GetPosition().z()/mm<<G4endl;

     G4cout<<"prepx prepy prepz "<<currentStep->GetPreStepPoint()->GetMomentum().x()/GeV<<" "<<currentStep->GetPreStepPoint()->GetMomentum().y()/GeV<<" "<<currentStep->GetPreStepPoint()->GetMomentum().z()/GeV<<G4endl;
     G4cout<<"post step postx posty postz "<<postStep->GetPosition().x()/mm<<" "<<postStep->GetPosition().y()/mm<<" "<<postStep->GetPosition().z()/mm<<G4endl;
     G4cout<<"postpx postpy postpz "<<postStep->GetMomentum().x()/GeV<<" "<<postStep->GetMomentum().y()/GeV<<" "<<postStep->GetMomentum().z()/GeV<<G4endl;         
      G4cout << G4endl;
      G4cout << std::setw( 5) << "#Step#"     << " "
             << std::setw( 9) << "pName"      << " "
             << std::setw( 6) << "X"          << "    "
             << std::setw( 6) << "Y"          << "    "
             << std::setw( 6) << "Z"          << "    "
             << std::setw( 9) << "KineE"      << " "
             << std::setw( 9) << "dEStep"     << " "
             << std::setw(10) << "StepLeng"
             << std::setw(10) << "Volume"    << "  "
             << std::setw(10) << "Process"  
             << G4endl;
    

    G4cout << std::setw(5) << currentTrack->GetCurrentStepNumber() << " "
        << std::setw(9) << currentTrack->GetDefinition()->GetParticleName()
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().x(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().y(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().z(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetKineticEnergy(),"Energy")
        << std::setw(6) << G4BestUnit(currentStep->GetTotalEnergyDeposit(),"Energy")
	   << std::setw(6) << G4BestUnit(currentStep->GetStepLength(),"Length") << G4endl;

	   
    if( currentTrack->GetNextVolume() != 0 ) {
        G4cout << std::setw(10) << currentTrack->GetVolume()->GetName();
     } else {
       G4cout << std::setw(10) << "OutOfWorld";
     } 
     
    if(currentStep->GetPostStepPoint()->GetProcessDefinedStep() != NULL){
      G4cout << "  "
        << std::setw(10) << currentStep->GetPostStepPoint()->GetProcessDefinedStep()
                                        ->GetProcessName();
    } else {                            
      G4cout << "   UserLimit";
    } 


    currentTrack->SetTrackStatus(fKillTrackAndSecondaries);
  }else if(currentTrack->GetCurrentStepNumber()>=30000){  //20000 before
    G4cout<<"BesSim :: BesSteppingAction: StepNumber>=30000 !!!"<<G4endl;
    currentTrack->SetTrackStatus(fKillTrackAndSecondaries);
  }
  */
/*     G4cout.precision(15);
     G4cout<<"#Step# "<<currentTrack->GetCurrentStepNumber()<<" pName "<<currentTrack->GetDefinition()->GetParticleName()<<" prex prey prez "<<currentStep->GetPreStepPoint()->GetPosition().x()/mm<<" "<<currentStep->GetPreStepPoint()->GetPosition().y()/mm<<" "<<currentStep->GetPreStepPoint()->GetPosition().z()/mm<<G4endl;
     G4cout<<"prepx prepy prepz "<<currentStep->GetPreStepPoint()->GetMomentum().x()/GeV<<" "<<currentStep->GetPreStepPoint()->GetMomentum().y()/GeV<<" "<<currentStep->GetPreStepPoint()->GetMomentum().z()/GeV<<G4endl;
     G4cout<<"post step postx posty postz "<<postStep->GetPosition().x()/mm<<" "<<postStep->GetPosition().y()/mm<<" "<<postStep->GetPosition().z()/mm<<G4endl;
     G4cout<<"postpx postpy postpz "<<postStep->GetMomentum().x()/GeV<<" "<<postStep->GetMomentum().y()/GeV<<" "<<postStep->GetMomentum().z()/GeV<<G4endl;         
      G4cout << G4endl;
      G4cout << std::setw( 5) << "#Step#"     << " "
             << std::setw( 9) << "pName"      << " "
             << std::setw( 6) << "X"          << "    "
             << std::setw( 6) << "Y"          << "    "
             << std::setw( 6) << "Z"          << "    "
             << std::setw( 9) << "KineE"      << " "
             << std::setw( 9) << "dEStep"     << " "
             << std::setw(10) << "StepLeng"
             << std::setw(10) << "Volume"    << "  "
             << std::setw(10) << "Process"  
             << G4endl;
    

    G4cout << std::setw(5) << currentTrack->GetCurrentStepNumber() << " "
        << std::setw(9) << currentTrack->GetDefinition()->GetParticleName()
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().x(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().y(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().z(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetKineticEnergy(),"Energy")
        << std::setw(6) << G4BestUnit(currentStep->GetTotalEnergyDeposit(),"Energy")
        << std::setw(6) << G4BestUnit(currentStep->GetStepLength(),"Length");
    if( currentTrack->GetNextVolume() != 0 ) { 
        G4cout << std::setw(10) << currentTrack->GetVolume()->GetName();
     } else {
       G4cout << std::setw(10) << "OutOfWorld";
     }

    if(currentStep->GetPostStepPoint()->GetProcessDefinedStep() != NULL){
      G4cout << "  "
        << std::setw(10) << currentStep->GetPostStepPoint()->GetProcessDefinedStep()
                                        ->GetProcessName();
    } else {
      G4cout << "   UserLimit";
    }

    G4cout << G4endl;
   HepRandom::showEngineStatus();
*/


//     G4bool leo = true;
    
    if (m_dump_flag) {
	// EVENT ID
	m_evtID = -666;
	const G4Event* evt = G4RunManager::GetRunManager()->GetCurrentEvent();
	if (evt) m_evtID = evt->GetEventID();
     
	// TRACK ID
	m_trackID = -666;
	const G4Track* track = currentStep->GetTrack();
	if (track) m_trackID = track->GetTrackID();
     
	// STEP ID
	m_stepID = -666;
	if (track) m_stepID = track->GetCurrentStepNumber();
	
	// PDG CODE
	m_pdgCode = 0;
	if (track) m_pdgCode = track->GetDefinition()->GetPDGEncoding();
     
	// STEP POSITION
	const G4ThreeVector preStepPosition = currentStep->GetPreStepPoint()->GetPosition();
	const G4ThreeVector postStepPosition = currentStep->GetPostStepPoint()->GetPosition();
	
	m_preX = preStepPosition.x();
	m_preY = preStepPosition.y();
	m_preZ = preStepPosition.z();
	
	m_postX = postStepPosition.x();
	m_postY = postStepPosition.y();
	m_postZ = postStepPosition.z();
	
	
// 	G4double posX = 0.5 * (preStepPosition.x() + postStepPosition.x());
// 	G4double posY = 0.5 * (preStepPosition.y() + postStepPosition.y());
// 	G4double posZ = 0.5 * (preStepPosition.z() + postStepPosition.z());
	
	// ENERGY DEPOSITION
	m_eDep = currentStep->GetTotalEnergyDeposit();
	
	m_tree->Fill();
	
// 	std::ofstream outfile;
// 
// 	outfile.open(m_dump_name.c_str(), std::ios_base::app);
// // 	outfile << "Data";
// 	
// 	outfile << " " << evtID << " " << trackID << " " << stepNumber << " " << pdgCode << " " << posX << " " << posY << " " << posZ << " " << energyDeposition << "\n";
    }








}



