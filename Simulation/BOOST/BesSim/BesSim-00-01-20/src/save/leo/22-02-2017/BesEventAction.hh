
#ifndef BesEventAction_h
#define BesEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "RealizationSvc/RealizationSvc.h"
#include "RealizationSvc/IRealizationSvc.h"

class BesRunAction;
class G4DigiManager;
//class IDataProviderSvc;

class BesEventAction : public G4UserEventAction
{
  public:
    BesEventAction(BesRunAction* runAction, bool impactRecoFlag, std::string impactRecoFilename);
    virtual ~BesEventAction();

  public:
    virtual void   BeginOfEventAction(const G4Event*);
    virtual void   EndOfEventAction(const G4Event*);
    
  private:
    
    BesRunAction* m_runAction;
            
    G4DigiManager* m_DM;    
    // Reference to RealizationSvc
    RealizationSvc* m_RealizationSvc;
    
    // LEO:
    bool m_ImpactRecoFlag;
    std::string m_ImpactRecoFilename;
    
  public:
    void SetImpactRecoFlag (bool flag) {m_ImpactRecoFlag = flag;}
    void SetImpactRecoFilename (std::string name) {m_ImpactRecoFilename = name;}
};


#endif

    
