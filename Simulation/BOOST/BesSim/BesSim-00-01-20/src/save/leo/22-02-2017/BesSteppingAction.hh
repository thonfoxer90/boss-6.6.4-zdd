
#ifndef BesSteppingAction_h
#define BesSteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "G4StepPoint.hh"

#include "TFile.h"
#include "TTree.h"

class BesSteppingAction : public G4UserSteppingAction
{
  public:
    BesSteppingAction();
    BesSteppingAction(bool dump_flag, std::string dump_name);
   ~BesSteppingAction();
 
  static G4double GetXImpact(){return m_x;};
  static G4double GetYImpact(){return m_y;};


    void UserSteppingAction(const G4Step*);
 private:
    static G4double m_x;
    static G4double m_y;
    
    bool m_dump_flag;
    std::string m_dump_name;
    
    TFile* m_File;
    TTree* m_tree;
    
    G4int m_evtID;
    G4int m_trackID;
    G4int m_stepID;
    G4int m_pdgCode;
    
    G4double m_preX;
    G4double m_preY;
    G4double m_preZ;
    G4double m_postX;
    G4double m_postY;
    G4double m_postZ;
    
    G4double m_eDep;
    
    
};

#endif
