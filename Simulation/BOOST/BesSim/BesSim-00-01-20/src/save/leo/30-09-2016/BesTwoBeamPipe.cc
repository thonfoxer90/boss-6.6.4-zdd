//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oreiented Simulation Tool                    //
//---------------------------------------------------------------------------//
//Descpirtion: BES beam pipe 
//Author: Liuhm
//Created: May 21, 2003
//Comment:
//---------------------------------------------------------------------------//
//
#include "BesTwoBeamPipe.hh"

#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4VisAttributes.hh"
#include "G4Color.hh"
#include "G4Cons.hh"
#include "G4Trd.hh"
#include "G4RotationMatrix.hh"
#include "G4SubtractionSolid.hh"
#include "G4EllipticalTube.hh"
#include "G4IntersectionSolid.hh"
#include "G4ExtrudedSolid.hh"

double ybetween(double x1, double y1, double x2, double y2,  double xbet) {
    
    double t = (xbet - x1) / (x2 - x1);
    double ybet = y1 + (y2 - y1) * t;
    
    return ybet;
}

double averrage(double x1, double x2) {
    return 0.5 * (x1 + x2);
}

double dist(double x1, double y1, double x2, double y2) {
    return sqrt((x1-x2)* (x1-x2) + (y1-y2) * (y1-y2));
}

BesTwoBeamPipe::BesTwoBeamPipe()
{

  logicalTwoBeamPipe = 0;
  physicalTwoBeamPipe1 = 0;
  physicalTwoBeamPipe2 = 0;


  Au = 0;
  Ag = 0;
  Oil = 0;
}

void BesTwoBeamPipe::DefineMaterial()
{
  G4double density, a, z,fractionmass;
  G4int nel,natoms;
  G4String name, symbol;

  density=19.32*g/cm3;
  a = 196.967*g/mole;
  Au= new G4Material("Gold",79,a,density);

  density=10.5*g/cm3;
  a = 107.9*g/mole;
  Ag= new G4Material("Silver",47,a,density);

  a = 1.01*g/mole;
  G4Element* H  = new G4Element(name="Hydrogen",symbol="H" , z= 1., a);
    
  a = 12.01*g/mole;
  G4Element* C  = new G4Element(name="Carbon"  ,symbol="C" , z= 6., a);

  density = 0.810*g/cm3;
  Oil = new G4Material(name="Oil",density,nel=2);
  Oil->AddElement(C, natoms=18);
  Oil->AddElement(H, natoms=38);

//   G4NistManager* man = G4NistManager::Instance();
//   man->SetVerbose(1);
//   G4Material* Iron = man->FindOrBuildMaterial("G4_Fe");
  
  G4Material* Iron = new G4Material("Iron",      z=26., a=55.845*g/mole,   density=7.87*g/cm3);

  G4double pressure;

a = 14.01*g/mole;
    G4Element* N  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a);
    
    a = 16.00*g/mole;
    G4Element* O  = new G4Element(name="Oxygen"  ,symbol="O" , z= 8., a);
    
    density = 8.96*g/cm3;
    a = 63.546*g/mole;
    G4Material* Copper = new G4Material(name="Copper",z=29.0,a,density);


  density = 1.290*mg/cm3;
    G4Material* Air = new G4Material(name="Air",density,nel=2);
    Air->AddElement(N, fractionmass = 0.7);
    Air->AddElement(O, fractionmass = 0.3);



      density     = 1.0e-5*g/cm3;
    pressure    = 2.e-2*bar;
    G4Material* Beam = new G4Material(name="Beam",density,nel=1,kStateGas,STP_Temperature,pressure);
    Beam->AddMaterial(Air,fractionmass=1.0);


density = 1.848*g/cm3;
    a = 9.012182*g/mole;
    G4Material* Beryllium = new G4Material(name="Beryllium",z=4.0,a,density);

    density = 2.70*g/cm3;
    a = 26.98*g/mole;
    G4Material* Aluminium = new G4Material(name="Aluminium",z=13.,a,density);
    
    G4NistManager* man = G4NistManager::Instance();
	man->SetVerbose(1);
    G4Element* Si = man->FindOrBuildElement("Si");
	G4Element* Cr = man->FindOrBuildElement("Cr");
	G4Element* Mn = man->FindOrBuildElement("Mn");
	G4Element* Fe = man->FindOrBuildElement("Fe");
	G4Element* Ni = man->FindOrBuildElement("Ni");
    
    G4int ncomponents;
    G4Material* StainlessSteel = new G4Material("StainlessSteel", density= 8.06*g/cm3, ncomponents=6);
	StainlessSteel->AddElement(C, fractionmass=0.001);
	StainlessSteel->AddElement(Si, fractionmass=0.007);
	StainlessSteel->AddElement(Cr, fractionmass=0.18);
	StainlessSteel->AddElement(Mn, fractionmass=0.01);
	StainlessSteel->AddElement(Fe, fractionmass=0.712);
	StainlessSteel->AddElement(Ni, fractionmass=0.09);


}

void BesTwoBeamPipe::Construct(G4LogicalVolume* logicalbes)
{
  DefineMaterial();

  G4RotationMatrix* zRot_90 = new G4RotationMatrix;  zRot_90->rotateZ(3.14159265/2.);
  G4RotationMatrix* zRot_180 = new G4RotationMatrix;  zRot_180->rotateZ(3.14159265);
  G4RotationMatrix* zRot_270 = new G4RotationMatrix;  zRot_270->rotateZ(3.14159265/2.*3);
  
  
  G4RotationMatrix* yRot_180 = new G4RotationMatrix; yRot_180->rotateY(M_PI);
  G4RotationMatrix* yRot_90 = new G4RotationMatrix; yRot_90->rotateY(M_PI/2.);
  G4RotationMatrix* yRot_270 = new G4RotationMatrix; yRot_270->rotateY(3.*M_PI/2.);
  
  G4RotationMatrix* xRot_180 = new G4RotationMatrix; xRot_180->rotateX(M_PI);
  G4RotationMatrix* xRot_90 = new G4RotationMatrix; xRot_90->rotateX(M_PI/2.);
  G4RotationMatrix* xRot_270 = new G4RotationMatrix; xRot_270->rotateX(3.*M_PI/2.);
  
  G4RotationMatrix* xyRot_180 = new G4RotationMatrix; xyRot_180->rotateX(M_PI); xyRot_180->rotateY(M_PI);
  

   //G4RotationMatrix* xRot = new G4RotationMatrix;
  //xRot->rotateX(90*deg);

  //the logical volume of beam pipe

  //length: 1150mm;
  
  // ATTENTION only for visualistion,  need to be set to false in boss
  G4bool cZDDdummy = false;
//   G4bool cZDDdummy = true;
  
  G4Box* solidTwoBeamPipe;
  if (cZDDdummy) {
      solidTwoBeamPipe =  new G4Box("solidPip",300./2.,300/2.,1500./2.);
  }else{
      solidTwoBeamPipe =  new G4Box("solidPip",300./2.,300/2.,1150./2.);
  }
  
  
  logicalTwoBeamPipe = new G4LogicalVolume(solidTwoBeamPipe, G4Material::GetMaterial("Beam"),"logicalTwoBeamPipe");
  new G4PVPlacement(0,G4ThreeVector(0, 0, 2200. + 1150./2.),logicalTwoBeamPipe,"physicalTwoBeamPipe",logicalbes,false,0);
  // left side:
  new G4PVPlacement(xRot_180,G4ThreeVector(0, 0, -2200. - 1150./2.),logicalTwoBeamPipe,"physicalTwoBeamPipe1",logicalbes,false,0);

  
  
  G4RotationMatrix* zyRot = new G4RotationMatrix;
  zyRot->rotateZ(3.14159265/2.);
  zyRot->rotateY(3.14159265);

// 
  logicalTwoBeamPipe->SetVisAttributes(G4VisAttributes::Invisible);

  
  
  
  G4VisAttributes* visIron = new G4VisAttributes(G4Colour(0.3, 0.3, 0.3));
  G4VisAttributes* visCopper = new G4VisAttributes(G4Colour(146./255, 83./255, 48./255));
  G4VisAttributes* visSteel = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  G4VisAttributes* viscZDD = new G4VisAttributes(G4Colour(0.1, 0.1, 0.5));
  

  
  
  

  G4bool checkoverlap = false;
  
  // for debug:
  G4bool endplate = true;
  G4bool septum = true;
  G4bool incoming = true;
  G4bool outgoing = true;
  
  
  
  // FLANGE:
  // OUTER:
  
  const G4double endplate_anchor_z = -1150./2.;
    if (endplate) {
  // ENDPLATE
  // DRILL HOLE
  G4Tubs* solid_endplate_drillhole = new G4Tubs("solid_endplate_drillhole", 0., 4., 20., 0., 2.*M_PI);
  
  
  
  
  
  // ENDPLATE TWO HOLES -- copied from y-type crotch and modified
  const G4double endplate_twohole_anchor_z = endplate_anchor_z +4. + 7.;
  // SQUARE PART
  G4Box* solid_endplate_twoholes_box = new G4Box("solid_endplate_twoholes_box", 12, 7, 4.);
  G4SubtractionSolid* solid_endplate_twoholes_square = new G4SubtractionSolid("solid_endplate_twoholes_square", solid_endplate_twoholes_box, solid_endplate_drillhole, 0, G4ThreeVector(-2., 0., 0.));
  
  G4LogicalVolume* logical_endplate_twoholes_square = new G4LogicalVolume(solid_endplate_twoholes_square, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_square");
  logical_endplate_twoholes_square->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_twoholes_square1 = new G4PVPlacement(0, G4ThreeVector(-88., 0., endplate_twohole_anchor_z), logical_endplate_twoholes_square, "phycical_endplate_twoholes_square1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_square2 = new G4PVPlacement(zRot_180, G4ThreeVector(88., 0., endplate_twohole_anchor_z), logical_endplate_twoholes_square, "phycical_endplate_twoholes_square2", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // ARC PART
  G4Tubs* solid_endplate_twoholes_tubs = new G4Tubs("solid_endplate_twoholes_tubs", 32., 56., 4., M_PI/2.+0.0001, M_PI/2.-0.0002);
  G4SubtractionSolid* solid_endplate_twoholes_arc = new G4SubtractionSolid("solid_endplate_twoholes_arc", solid_endplate_twoholes_tubs, solid_endplate_drillhole, 0, G4ThreeVector(-sin(12.5/180.*M_PI) *46, cos(12.5/180.*M_PI) *46, 0));
  solid_endplate_twoholes_arc = new G4SubtractionSolid("solid_endplate_twoholes_arc", solid_endplate_twoholes_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(42.5/180.*M_PI) *46, cos(42.5/180.*M_PI) *46, 0));
  solid_endplate_twoholes_arc = new G4SubtractionSolid("solid_endplate_twoholes_arc", solid_endplate_twoholes_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(72.5/180.*M_PI) *46, cos(72.5/180.*M_PI) *46, 0));
  
  G4LogicalVolume* logical_endplate_twoholes_arc = new G4LogicalVolume(solid_endplate_twoholes_arc, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_arc");
  logical_endplate_twoholes_arc->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_twoholes_arc1 = new G4PVPlacement(0, G4ThreeVector(-44., 7., endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_arc2 = new G4PVPlacement(xRot_180, G4ThreeVector(-44., -7., endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc2", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_arc3 = new G4PVPlacement(yRot_180, G4ThreeVector(44., 7., endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc3", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_arc4 = new G4PVPlacement(xyRot_180, G4ThreeVector(44., -7., endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc4", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // TSHAPE PART
  G4Box* solid_endplate_twoholes_topbox = new G4Box("solid_endplate_twoholes_topbox", 42., 28., 4.);
  G4Tubs* solid_endplate_twoholes_subarc = new G4Tubs("solid_endplate_twoholes_subarc", 0., 32., 4., 0., 2*M_PI);
  G4SubtractionSolid* solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_topbox, solid_endplate_twoholes_subarc, 0,  G4ThreeVector(42., -28., 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_twoholes_subarc, 0,  G4ThreeVector(-42., -28., 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42. + 46.*sin(12.5/180.*M_PI), -28 + 46*cos(12.5/180.*M_PI), 0.));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42. + 46.*sin(37.5/180.*M_PI), -28 + 46*cos(37.5/180.*M_PI), 0.));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42. - 46.*sin(12.5/180.*M_PI), -28 + 46*cos(12.5/180.*M_PI), 0.));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42. - 46.*sin(37.5/180.*M_PI), -28 + 46*cos(37.5/180.*M_PI), 0.));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(0., -5., 0.));
  
  G4LogicalVolume* logical_endplate_twoholes_tshape = new G4LogicalVolume(solid_endplate_twoholes_tshape, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_tshape");
  logical_endplate_twoholes_tshape->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_twoholes_tshape1 = new G4PVPlacement(0, G4ThreeVector(0, 35, endplate_twohole_anchor_z), logical_endplate_twoholes_tshape, "phycical_endplate_twoholes_tshape1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_tshape2 = new G4PVPlacement(xRot_180, G4ThreeVector(0, -35, endplate_twohole_anchor_z), logical_endplate_twoholes_tshape, "phycical_endplate_twoholes_tshape2", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // FILLBOX
  G4Box* solid_endplate_twoholes_fillbox = new G4Box("solid_endplate_twoholes_fillbox", 1., 12., 4.);
  
  G4LogicalVolume* logical_endplate_twoholes_fillbox = new G4LogicalVolume(solid_endplate_twoholes_fillbox, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_fillbox");
  logical_endplate_twoholes_fillbox->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox1 = new G4PVPlacement(0, G4ThreeVector(-43., 51., endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox2 = new G4PVPlacement(0, G4ThreeVector(43., 51., endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox2", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox3 = new G4PVPlacement(0, G4ThreeVector(43., -51., endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox3", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox4 = new G4PVPlacement(0, G4ThreeVector(-43., -51., endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox4", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // CENTERBOX
  G4Box* solid_endplate_twoholes_centerbox = new G4Box("solid_endplate_twoholes_centerbox", 10., 7., 4.);
  
  G4LogicalVolume* logical_endplate_twoholes_centerbox = new G4LogicalVolume(solid_endplate_twoholes_centerbox, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_centerbox");
  logical_endplate_twoholes_centerbox->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *physical_endplate_twoholes_centerbox = new G4PVPlacement(0, G4ThreeVector(0., 0., endplate_twohole_anchor_z), logical_endplate_twoholes_centerbox, "physical_endplate_twoholes_centerbox", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // ENDPLATE CONNECTION TO CROTCH -- copied from y-type crotch and modified
  const G4double endplate_tocrotch_anchor_z = endplate_anchor_z + 3.5;
  // SQUARE PART
  G4Box* solid_endplate_tocrotch_box = new G4Box("solid_endplate_tocrotch_box", 13, 7, 3.5);
  G4SubtractionSolid* solid_endplate_tocrotch_square = new G4SubtractionSolid("solid_endplate_tocrotch_square", solid_endplate_tocrotch_box, solid_endplate_drillhole, 0, G4ThreeVector(-3., 0., 0.));
  
  G4LogicalVolume* logical_endplate_tocrotch_square = new G4LogicalVolume(solid_endplate_tocrotch_square, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_tocrotch_square");
  logical_endplate_tocrotch_square->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_tocrotch_square1 = new G4PVPlacement(0, G4ThreeVector(-87., 0., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_square, "phycical_endplate_tocrotch_square1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_tocrotch_square2 = new G4PVPlacement(zRot_180, G4ThreeVector(87., 0., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_square, "phycical_endplate_tocrotch_square2", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // ARC PART
  G4Tubs* solid_endplate_tocrotch_tubs = new G4Tubs("solid_endplate_tocrotch_tubs", 30., 56., 3.5, M_PI/2.+0.0001, M_PI/2.-0.0002);
  G4SubtractionSolid* solid_endplate_tocrotch_arc = new G4SubtractionSolid("solid_endplate_tocrotch_arc", solid_endplate_tocrotch_tubs, solid_endplate_drillhole, 0, G4ThreeVector(-sin(12.5/180.*M_PI) *46, cos(12.5/180.*M_PI) *46, 0));
  solid_endplate_tocrotch_arc = new G4SubtractionSolid("solid_endplate_tocrotch_arc", solid_endplate_tocrotch_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(42.5/180.*M_PI) *46, cos(42.5/180.*M_PI) *46, 0));
  solid_endplate_tocrotch_arc = new G4SubtractionSolid("solid_endplate_tocrotch_arc", solid_endplate_tocrotch_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(72.5/180.*M_PI) *46, cos(72.5/180.*M_PI) *46, 0));
  
  G4LogicalVolume* logical_endplate_tocrotch_arc = new G4LogicalVolume(solid_endplate_tocrotch_arc, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_tocrotch_arc");
  logical_endplate_tocrotch_arc->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_tocrotch_arc1 = new G4PVPlacement(0, G4ThreeVector(-44., 7., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_arc, "phycical_endplate_tocrotch_arc1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_tocrotch_arc2 = new G4PVPlacement(xRot_180, G4ThreeVector(-44., -7., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_arc, "phycical_endplate_tocrotch_arc2", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_tocrotch_arc3 = new G4PVPlacement(yRot_180, G4ThreeVector(44., 7., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_arc, "phycical_endplate_tocrotch_arc3", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_tocrotch_arc4 = new G4PVPlacement(xyRot_180, G4ThreeVector(44., -7., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_arc, "phycical_endplate_tocrotch_arc4", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // TSHAPE PART
  G4Box* solid_endplate_tocrotch_topbox = new G4Box("solid_endplate_tocrotch_topbox", 42., 28., 3.5);
  G4Tubs* solid_endplate_tocrotch_subarc = new G4Tubs("solid_endplate_tocrotch_subarc", 0., 30., 3.5, 0., 2*M_PI);
  G4SubtractionSolid* solid_endplate_tocrotch_tshape = new G4SubtractionSolid("solid_endplate_tocrotch_tshape", solid_endplate_tocrotch_topbox, solid_endplate_tocrotch_subarc, 0,  G4ThreeVector(42., -28., 0));
  solid_endplate_tocrotch_tshape = new G4SubtractionSolid("solid_endplate_tocrotch_tshape", solid_endplate_tocrotch_tshape, solid_endplate_tocrotch_subarc, 0,  G4ThreeVector(-42., -28., 0));
  solid_endplate_tocrotch_tshape = new G4SubtractionSolid("solid_endplate_tocrotch_tshape", solid_endplate_tocrotch_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42. + 46.*sin(12.5/180.*M_PI), -28 + 46*cos(12.5/180.*M_PI), 0.));
  solid_endplate_tocrotch_tshape = new G4SubtractionSolid("solid_endplate_tocrotch_tshape", solid_endplate_tocrotch_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42. + 46.*sin(37.5/180.*M_PI), -28 + 46*cos(37.5/180.*M_PI), 0.));
  solid_endplate_tocrotch_tshape = new G4SubtractionSolid("solid_endplate_tocrotch_tshape", solid_endplate_tocrotch_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42. - 46.*sin(12.5/180.*M_PI), -28 + 46*cos(12.5/180.*M_PI), 0.));
  solid_endplate_tocrotch_tshape = new G4SubtractionSolid("solid_endplate_tocrotch_tshape", solid_endplate_tocrotch_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42. - 46.*sin(37.5/180.*M_PI), -28 + 46*cos(37.5/180.*M_PI), 0.));
  solid_endplate_tocrotch_tshape = new G4SubtractionSolid("solid_endplate_tocrotch_tshape", solid_endplate_tocrotch_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(0., -5., 0.));
  
  G4LogicalVolume* logical_endplate_tocrotch_tshape = new G4LogicalVolume(solid_endplate_tocrotch_tshape, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_tocrotch_tshape");
  logical_endplate_tocrotch_tshape->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_tocrotch_tshape1 = new G4PVPlacement(0, G4ThreeVector(0, 35, endplate_tocrotch_anchor_z), logical_endplate_tocrotch_tshape, "phycical_endplate_tocrotch_tshape1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_tocrotch_tshape2 = new G4PVPlacement(xRot_180, G4ThreeVector(0, -35, endplate_tocrotch_anchor_z), logical_endplate_tocrotch_tshape, "phycical_endplate_tocrotch_tshape2", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // FILLBOX
  G4Box* solid_endplate_tocrotch_fillbox = new G4Box("solid_endplate_tocrotch_fillbox", 1., 13., 3.5);
  
  G4LogicalVolume* logical_endplate_tocrotch_fillbox = new G4LogicalVolume(solid_endplate_tocrotch_fillbox, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_tocrotch_fillbox");
  logical_endplate_tocrotch_fillbox->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *physical_endplate_tocrotch_fillbox1 = new G4PVPlacement(0, G4ThreeVector(-43., 50., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_fillbox, "physical_endplate_tocrotch_fillbox1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_tocrotch_fillbox2 = new G4PVPlacement(0, G4ThreeVector(43., 50., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_fillbox, "physical_endplate_tocrotch_fillbox2", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_tocrotch_fillbox3 = new G4PVPlacement(0, G4ThreeVector(43., -50., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_fillbox, "physical_endplate_tocrotch_fillbox3", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_tocrotch_fillbox4 = new G4PVPlacement(0, G4ThreeVector(-43., -50., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_fillbox, "physical_endplate_tocrotch_fillbox4", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // CENTERBOX
  G4Box* solid_endplate_tocrotch_centerbox = new G4Box("solid_endplate_tocrotch_centerbox", 12., 7., 3.5);
  
  G4LogicalVolume* logical_endplate_tocrotch_centerbox = new G4LogicalVolume(solid_endplate_tocrotch_centerbox, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_tocrotch_centerbox");
  logical_endplate_tocrotch_centerbox->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *physical_endplate_tocrotch_centerbox = new G4PVPlacement(0, G4ThreeVector(0., 0., endplate_tocrotch_anchor_z), logical_endplate_tocrotch_centerbox, "physical_endplate_tocrotch_centerbox", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  }
  
  
  
  
  // SEPTUM IN MIDDLE
  // box
  
  if (septum) {
    G4Box* solid_septum = new G4Box("solid_septum", 7.65, 42., 300.);
  
    G4LogicalVolume* logical_septum = new G4LogicalVolume(solid_septum, G4Material::GetMaterial("Iron"),"logical_septum");
    logical_septum->SetVisAttributes(visIron);
    
    G4VPhysicalVolume* physical_septum = new G4PVPlacement(0, G4ThreeVector(0., 0., endplate_anchor_z + 400.), logical_septum, "physical_septum", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    // yoke above and below outgoing beam
    G4Box* solid_septum_yoke = new G4Box("solid_septum_yoke", 35. + 15.15, 20., 300.);
    
    G4LogicalVolume* logical_septum_yoke = new G4LogicalVolume(solid_septum_yoke, G4Material::GetMaterial("Iron"),"logical_septum_yoke");
    logical_septum_yoke->SetVisAttributes(visIron);
    
    G4VPhysicalVolume* physical_septum_yoke_up = new G4PVPlacement(0, G4ThreeVector(42.65, 62., endplate_anchor_z + 400.), logical_septum_yoke, "physical_septum_yoke_up", logicalTwoBeamPipe, false, 0, checkoverlap);
    G4VPhysicalVolume* physical_septum_yoke_low = new G4PVPlacement(0, G4ThreeVector(42.65, -62., endplate_anchor_z + 400.), logical_septum_yoke, "physical_septum_yoke_low", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    // triangle whole part
//     std::vector<G4TwoVector> triangle;
//     triangle.push_back(G4TwoVector(-7.65, -70.));
//     triangle.push_back(G4TwoVector(-60., -70.));
//     triangle.push_back(G4TwoVector(-60., -50.));
//     triangle.push_back(G4TwoVector(-37.65, -50.));
//     triangle.push_back(G4TwoVector(-9.65, -10.));
//     triangle.push_back(G4TwoVector(-9.65, 10.));
//     triangle.push_back(G4TwoVector(-37.65, 50.));
//     triangle.push_back(G4TwoVector(-60., 50.));
//     triangle.push_back(G4TwoVector(-60., 70.));
//     triangle.push_back(G4TwoVector(-7.65, 70.));
//     G4ExtrudedSolid* solid_triangle = new G4ExtrudedSolid("solid_triangle", triangle, 300., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
//     
//     G4LogicalVolume* logical_triangle = new G4LogicalVolume(solid_triangle, G4Material::GetMaterial("Iron"),"logical_triangle");
//     logical_triangle->SetVisAttributes(visIron);
    
//     G4VPhysicalVolume *physical_triangle1 = new G4PVPlacement(0, G4ThreeVector(2., 0., endplate_anchor_z + 400.), logical_triangle, "physical_triangle1", logicalTwoBeamPipe, false, 0, checkoverlap);
  }
  
  // INCOMING
  if (incoming) {
      
    // cutoff box -- copied from outgoing,  see below
    std::vector<G4TwoVector> cutawayface;
    cutawayface.push_back(G4TwoVector(-80., 0.));
    cutawayface.push_back(G4TwoVector(-80., 80.));
    cutawayface.push_back(G4TwoVector(80., 80.));
    cutawayface.push_back(G4TwoVector(80., 0.));
    G4ExtrudedSolid* solid_cutaway = new G4ExtrudedSolid("solid_cutaway", cutawayface, 80., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    // [][0] : z component; [][1] : x component
    double incomingpoints[5][2] = {{0., 31.67}, {10., 31.67}, {901.55, 26.23}, {980.53, 24.17}, {978.94, -36.74}};
    
    // shift points by 7mm in z direction (flange offset)
    // shift points in x direction to correct position
    for (int idx = 0; idx < 5; idx++) {
	incomingpoints[idx][0] += 7.;
	incomingpoints[idx][1] += -31.67 - 12.;
    }
    
    // first 10mm : flange part
    double incoming_flange_length = (incomingpoints[1][0] - incomingpoints[0][0]);
    double incoming_flange_pos_z = endplate_anchor_z + averrage(incomingpoints[1][0], incomingpoints[0][0]);
    double incoming_flange_pos_x = averrage(incomingpoints[1][1], incomingpoints[0][1]);
    
    G4Tubs* solid_incoming_flange_uparc = new G4Tubs("solid_incoming_flange_uparc", 30., 32., 0.5 * incoming_flange_length, 0., M_PI/2.);
    G4Tubs* solid_incoming_flange_lowarc = new G4Tubs("solid_incoming_flange_lowarc", 30., 32., 0.5 * incoming_flange_length, 3.*M_PI/2., M_PI/2.);
    G4Box* solid_incoming_flange_straight = new G4Box("solid_incoming_flange_straight", 1., 7., 0.5 * incoming_flange_length);
    
    G4LogicalVolume* logical_incoming_flange_uparc = new G4LogicalVolume(solid_incoming_flange_uparc, G4Material::GetMaterial("Copper"),"logical_incoming_flange_uparc");
    G4LogicalVolume* logical_incoming_flange_lowarc = new G4LogicalVolume(solid_incoming_flange_lowarc, G4Material::GetMaterial("Copper"),"logical_incoming_flange_lowarc");
    G4LogicalVolume* logical_incoming_flange_straight = new G4LogicalVolume(solid_incoming_flange_straight, G4Material::GetMaterial("Copper"),"logical_incoming_flange_straight");
    logical_incoming_flange_uparc->SetVisAttributes(visCopper);
    logical_incoming_flange_lowarc->SetVisAttributes(visCopper);
    logical_incoming_flange_straight->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_incoming_flange_uparc = new G4PVPlacement(0, G4ThreeVector(incoming_flange_pos_x - 30., 7., incoming_flange_pos_z), logical_incoming_flange_uparc, "physical_incoming_flange_uparc", logicalTwoBeamPipe, false, 0, checkoverlap);
    G4VPhysicalVolume *physical_incoming_flange_lowarc = new G4PVPlacement(0, G4ThreeVector(incoming_flange_pos_x - 30., -7., incoming_flange_pos_z), logical_incoming_flange_lowarc, "physical_incoming_flange_lowarc", logicalTwoBeamPipe, false, 0, checkoverlap);
    G4VPhysicalVolume *physical_incoming_flange_straight = new G4PVPlacement(0, G4ThreeVector(incoming_flange_pos_x +1., 0., incoming_flange_pos_z), logical_incoming_flange_straight, "physical_incoming_flange_straight", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    // main part
    // positions
    double incoming_main_length = dist(incomingpoints[1][0], incomingpoints[1][1], incomingpoints[2][0], incomingpoints[2][1]);
    double incoming_main_pos_z = endplate_anchor_z + averrage(incomingpoints[1][0], incomingpoints[2][0]);
    double incoming_main_pos_x = averrage(incomingpoints[1][1], incomingpoints[2][1]);
    
    // angles
    double incoming_main_angle = atan((incomingpoints[1][1] - incomingpoints[2][1]) / (incomingpoints[1][0] - incomingpoints[2][0]));
    double incoming_end_angle = atan((incomingpoints[2][1] - incomingpoints[3][1]) / (incomingpoints[2][0] - incomingpoints[3][0]));
    
    double incoming_main_angle_near = incoming_main_angle;
    double incoming_main_angle_far = 0.5 * (incoming_main_angle - incoming_end_angle);
    
    // rotations
    G4RotationMatrix* yRot_incoming_main = new G4RotationMatrix();
    yRot_incoming_main->rotateY(-incoming_main_angle);
    
    G4RotationMatrix* Rot_incoming_main_cutaway_near = new G4RotationMatrix();
    Rot_incoming_main_cutaway_near->rotateY(incoming_main_angle_near);
    Rot_incoming_main_cutaway_near->rotateX(M_PI/2.);
    
    G4RotationMatrix* Rot_incoming_main_cutaway_far = new G4RotationMatrix();
    Rot_incoming_main_cutaway_far->rotateY(incoming_main_angle_far);
    Rot_incoming_main_cutaway_far->rotateX(-M_PI/2.);
    
    // extrusion
    std::vector<G4TwoVector> polygon_incoming_main;
    polygon_incoming_main.push_back(G4TwoVector(0., 7.));
    polygon_incoming_main.push_back(G4TwoVector(-30., 37.));
    polygon_incoming_main.push_back(G4TwoVector(-30., 41.6));
    polygon_incoming_main.push_back(G4TwoVector(-22.5, 41.6));
    polygon_incoming_main.push_back(G4TwoVector(-5.5, 29.9));
    polygon_incoming_main.push_back(G4TwoVector(2.5, 9.5));
    polygon_incoming_main.push_back(G4TwoVector(2.5, -9.5));
    polygon_incoming_main.push_back(G4TwoVector(-5.5, -29.9));
    polygon_incoming_main.push_back(G4TwoVector(-22.5, -41.6));
    polygon_incoming_main.push_back(G4TwoVector(-30., -41.6));
    polygon_incoming_main.push_back(G4TwoVector(-30., -37.));
    polygon_incoming_main.push_back(G4TwoVector(0., -7.));
    
    G4ExtrudedSolid* solid_incoming_main_extrusion = new G4ExtrudedSolid("solid_incoming_main_extrusion", polygon_incoming_main, incoming_main_length / 2. + 20., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    // cut tube
    G4Tubs* solid_incoming_main_cuttube = new G4Tubs("solid_incoming_main_cuttube", 0., 30., incoming_main_length + 30., 0., 2 * M_PI);
    
    G4SubtractionSolid* solid_incoming_main = new G4SubtractionSolid("solid_incoming_main", solid_incoming_main_extrusion, solid_incoming_main_cuttube, 0, G4ThreeVector(-30., 7., 0.));
    solid_incoming_main = new G4SubtractionSolid("solid_incoming_main", solid_incoming_main, solid_incoming_main_cuttube, 0, G4ThreeVector(-30., -7., 0.));
    
    // cut faces at end
    solid_incoming_main = new G4SubtractionSolid("solid_incoming_main", solid_incoming_main, solid_cutaway, Rot_incoming_main_cutaway_near, G4ThreeVector(0., 0., -0.5 * incoming_main_length));
    solid_incoming_main = new G4SubtractionSolid("solid_incoming_main", solid_incoming_main, solid_cutaway, Rot_incoming_main_cutaway_far, G4ThreeVector(0., 0., 0.5 * incoming_main_length));
    
    G4LogicalVolume* logical_incoming_main = new G4LogicalVolume(solid_incoming_main, G4Material::GetMaterial("Copper"),"logical_incoming_main");
    logical_incoming_main->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_incoming_main = new G4PVPlacement(yRot_incoming_main, G4ThreeVector(incoming_main_pos_x, 0., incoming_main_pos_z), logical_incoming_main, "physical_incoming_main", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    // end part
    // positions
    double incoming_end_length = dist(incomingpoints[2][0], incomingpoints[2][1], incomingpoints[3][0], incomingpoints[3][1]);
    double incoming_end_pos_z = endplate_anchor_z + averrage(incomingpoints[2][0], incomingpoints[3][0]);
    double incoming_end_pos_x = averrage(incomingpoints[2][1], incomingpoints[3][1]);
    
    // angles
    double incoming_afterend_angle = -M_PI/2. + atan((incomingpoints[3][1] - incomingpoints[4][1]) / (incomingpoints[3][0] - incomingpoints[4][0]));
    
    double incoming_end_angle_near = 0.5 * (incoming_end_angle - incoming_main_angle);
    double incoming_end_angle_far = 0.5 * (incoming_end_angle - incoming_afterend_angle);
    
    // rotations
    G4RotationMatrix* yRot_incoming_end = new G4RotationMatrix();
    yRot_incoming_end->rotateY(-incoming_end_angle);
    
    G4RotationMatrix* Rot_incoming_end_cutaway_near = new G4RotationMatrix();
    Rot_incoming_end_cutaway_near->rotateY(incoming_end_angle_near);
    Rot_incoming_end_cutaway_near->rotateX(M_PI/2.);
    
    G4RotationMatrix* Rot_incoming_end_cutaway_far = new G4RotationMatrix();
    Rot_incoming_end_cutaway_far->rotateY(incoming_end_angle_far);
    Rot_incoming_end_cutaway_far->rotateX(-M_PI/2.);
    
    // extrusion
    std::vector<G4TwoVector> polygon_incoming_end;
    polygon_incoming_end.push_back(G4TwoVector(0., 7.));
    polygon_incoming_end.push_back(G4TwoVector(-30., 37.));
    polygon_incoming_end.push_back(G4TwoVector(-30., 39.5));
    polygon_incoming_end.push_back(G4TwoVector(2.5, 39.5));
    polygon_incoming_end.push_back(G4TwoVector(2.5, -39.5));
    polygon_incoming_end.push_back(G4TwoVector(-30., -39.5));
    polygon_incoming_end.push_back(G4TwoVector(-30., -37.));
    polygon_incoming_end.push_back(G4TwoVector(0., -7.));
    
    G4ExtrudedSolid* solid_incoming_end_extrusion = new G4ExtrudedSolid("solid_incoming_end_extrusion", polygon_incoming_end, incoming_end_length / 2. + 20., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    // cut tube
    G4Tubs* solid_incoming_end_cuttube = new G4Tubs("solid_incoming_end_cuttube", 0., 30., incoming_end_length + 30., 0., 2 * M_PI);
    
    G4SubtractionSolid* solid_incoming_end = new G4SubtractionSolid("solid_incoming_end", solid_incoming_end_extrusion, solid_incoming_end_cuttube, 0, G4ThreeVector(-30., 7., 0.));
    solid_incoming_end = new G4SubtractionSolid("solid_incoming_end", solid_incoming_end, solid_incoming_end_cuttube, 0, G4ThreeVector(-30., -7., 0.));
    
    // cut arc
    G4Tubs* solid_incoming_end_cutuparc = new G4Tubs("solid_incoming_end_cutuparc", 32.5, 50., incoming_end_length + 30., 0., M_PI/2.);
    G4Tubs* solid_incoming_end_cutlowarc = new G4Tubs("solid_incoming_end_cutlowarc", 32.5, 50., incoming_end_length + 30., 3. * M_PI/2., M_PI/2.);
    solid_incoming_end = new G4SubtractionSolid("solid_incoming_end", solid_incoming_end, solid_incoming_end_cutuparc, 0, G4ThreeVector(-30., 7., 0.));
    solid_incoming_end = new G4SubtractionSolid("solid_incoming_end", solid_incoming_end, solid_incoming_end_cutlowarc, 0, G4ThreeVector(-30., -7., 0.));
    
    // cut faces at end
    solid_incoming_end = new G4SubtractionSolid("solid_incoming_end", solid_incoming_end, solid_cutaway, Rot_incoming_end_cutaway_near, G4ThreeVector(0., 0., -0.5 * incoming_end_length));
    solid_incoming_end = new G4SubtractionSolid("solid_incoming_end", solid_incoming_end, solid_cutaway, Rot_incoming_end_cutaway_far, G4ThreeVector(0., 0., 0.5 * incoming_end_length));
    
    G4LogicalVolume* logical_incoming_end = new G4LogicalVolume(solid_incoming_end, G4Material::GetMaterial("Copper"),"logical_incoming_end");
    logical_incoming_end->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_incoming_end = new G4PVPlacement(yRot_incoming_end, G4ThreeVector(incoming_end_pos_x, 0., incoming_end_pos_z), logical_incoming_end, "physical_incoming_end", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    // end flange
    // position
    double incoming_endflange_pos_z = endplate_anchor_z + incomingpoints[3][0];
    double incoming_endflange_pos_x = incomingpoints[3][1];
    
    // rotations
    G4RotationMatrix* yRot_incoming_endflange = new G4RotationMatrix();
    yRot_incoming_endflange->rotateY(-incoming_afterend_angle);
    
    
    // extrusion
    std::vector<G4TwoVector> polygon_incoming_endflange;
    polygon_incoming_endflange.push_back(G4TwoVector(20., 65.));
    polygon_incoming_endflange.push_back(G4TwoVector(20., -65.));
    polygon_incoming_endflange.push_back(G4TwoVector(-110., -65.));
    polygon_incoming_endflange.push_back(G4TwoVector(-110., 65.));
    
    G4ExtrudedSolid* solid_incoming_endflange_extrusion = new G4ExtrudedSolid("solid_incoming_endflange_extrusion", polygon_incoming_endflange, 10.4, G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    // cut away box at beginning
    G4Box* solid_incoming_endflange_cutbox = new G4Box("solid_incoming_endflange_cutbox", 120., 80., 10.);
    
    G4SubtractionSolid* solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange_extrusion, solid_incoming_endflange_cutbox, 0, G4ThreeVector(0., 0., -13.4));
    
    // cut to circular shape
    G4Tubs* solid_incoming_endflange_cuthole = new G4Tubs("solid_incoming_endflange_cuthole", 60., 150., 15., 0., 2. * M_PI);
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuthole, 0, G4ThreeVector(-45., 0., 0.));
    
    // cut away extrusion -- 90 * 74
    std::vector<G4TwoVector> polygon_incoming_endflange_cutinner;
    polygon_incoming_endflange_cutinner.push_back(G4TwoVector(0., 7.));
    polygon_incoming_endflange_cutinner.push_back(G4TwoVector(0., -7.));
    polygon_incoming_endflange_cutinner.push_back(G4TwoVector(-30., -37.));
    polygon_incoming_endflange_cutinner.push_back(G4TwoVector(-60., -37.));
    polygon_incoming_endflange_cutinner.push_back(G4TwoVector(-90., -7.));
    polygon_incoming_endflange_cutinner.push_back(G4TwoVector(-90., 7.));
    polygon_incoming_endflange_cutinner.push_back(G4TwoVector(-60., 37.));
    polygon_incoming_endflange_cutinner.push_back(G4TwoVector(-30., 37.));
    
    G4ExtrudedSolid* solid_incoming_endflange_cutinner_extrusion = new G4ExtrudedSolid("solid_incoming_endflange_cutinner_extrusion", polygon_incoming_endflange_cutinner, 15., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cutinner_extrusion, 0, G4ThreeVector(0., 0., 0.));
    
    // cut away extrusion -- 95 * 79
    std::vector<G4TwoVector> polygon_incoming_endflange_cutouter;
    polygon_incoming_endflange_cutouter.push_back(G4TwoVector(2.5, 7.));
    polygon_incoming_endflange_cutouter.push_back(G4TwoVector(2.5, -7.));
    polygon_incoming_endflange_cutouter.push_back(G4TwoVector(-30., -39.5));
    polygon_incoming_endflange_cutouter.push_back(G4TwoVector(-60., -39.5));
    polygon_incoming_endflange_cutouter.push_back(G4TwoVector(-92.5, -7.));
    polygon_incoming_endflange_cutouter.push_back(G4TwoVector(-92.5, 7.));
    polygon_incoming_endflange_cutouter.push_back(G4TwoVector(-60., 39.5));
    polygon_incoming_endflange_cutouter.push_back(G4TwoVector(-30., 39.5));
    
    G4ExtrudedSolid* solid_incoming_endflange_cutouter_extrusion = new G4ExtrudedSolid("solid_incoming_endflange_cutouter_extrusion", polygon_incoming_endflange_cutouter, 15., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cutouter_extrusion, 0, G4ThreeVector(0., 0., -15.));
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cutouter_extrusion, 0, G4ThreeVector(0., 0., 17.));
    
    // cut tubes
    G4Tubs* solid_incoming_endflange_cuttubeinner = new G4Tubs("solid_incoming_endflange_cuttubeinner", 0., 30., 15., 0., 2. * M_PI);
    G4Tubs* solid_incoming_endflange_cuttubeouter = new G4Tubs("solid_incoming_endflange_cuttubeouter", 0., 32.5, 15., 0., 2. * M_PI);
    
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeinner, 0, G4ThreeVector(-30., 7., 0.));
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeinner, 0, G4ThreeVector(-30., -7., 0.));
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeinner, 0, G4ThreeVector(-60., -7., 0.));
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeinner, 0, G4ThreeVector(-60., 7., 0.));
    
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeouter, 0, G4ThreeVector(-30., 7., -15.));
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeouter, 0, G4ThreeVector(-30., -7., -15.));
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeouter, 0, G4ThreeVector(-60., -7., -15.));
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeouter, 0, G4ThreeVector(-60., 7., -15.));
    
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeouter, 0, G4ThreeVector(-30., 7., 17.));
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeouter, 0, G4ThreeVector(-30., -7., 17.));
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeouter, 0, G4ThreeVector(-60., -7., 17.));
    solid_incoming_endflange = new G4SubtractionSolid("solid_incoming_endflange", solid_incoming_endflange, solid_incoming_endflange_cuttubeouter, 0, G4ThreeVector(-60., 7., 17.));
    
    
    
    
    G4LogicalVolume* logical_incoming_endflange = new G4LogicalVolume(solid_incoming_endflange, G4Material::GetMaterial("StainlessSteel"),"logical_incoming_endflange");
    logical_incoming_endflange->SetVisAttributes(visSteel);
    
    G4VPhysicalVolume *physical_incoming_endflange = new G4PVPlacement(yRot_incoming_endflange, G4ThreeVector(incoming_endflange_pos_x, 0., incoming_endflange_pos_z), logical_incoming_endflange, "physical_incoming_endflange", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    // CF100 part
    // extrusion
    std::vector<G4TwoVector> polygon_incoming_cf100;
    polygon_incoming_cf100.push_back(G4TwoVector(40., 80.));
    polygon_incoming_cf100.push_back(G4TwoVector(40., -80.));
    polygon_incoming_cf100.push_back(G4TwoVector(-120., -80.));
    polygon_incoming_cf100.push_back(G4TwoVector(-120., 80.));
    
    G4ExtrudedSolid* solid_incoming_cf100_extrusion = new G4ExtrudedSolid("solid_incoming_cf100_extrusion", polygon_incoming_cf100, 10.4, G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    // cut away box at beginning
    G4Box* solid_incoming_cf100_cutbox = new G4Box("solid_incoming_cf100_cutbox", 120., 90., 10.);
    
    G4SubtractionSolid* solid_incoming_cf100 = new G4SubtractionSolid("solid_incoming_cf100", solid_incoming_cf100_extrusion, solid_incoming_cf100_cutbox, 0, G4ThreeVector(-40., 0., -21.4));
    
    // cut to circular shape
    G4Tubs* solid_incoming_cf100_cuthole = new G4Tubs("solid_incoming_cf100_cuthole", 76., 150., 15., 0., 2. * M_PI);
    solid_incoming_cf100 = new G4SubtractionSolid("solid_incoming_cf100", solid_incoming_cf100, solid_incoming_cf100_cuthole, 0, G4ThreeVector(-45., 0., 0.));
    
    // cut 100mm hole
    G4Tubs* solid_incoming_cf100_cuttube100 = new G4Tubs("solid_incoming_cf100_cuttube100", 0., 50., 15., 0., 2. * M_PI);
    solid_incoming_cf100 = new G4SubtractionSolid("solid_incoming_cf100", solid_incoming_cf100, solid_incoming_cf100_cuttube100, 0, G4ThreeVector(-45., 0., 0.));
    
    // cut 120mm hole
    G4Tubs* solid_incoming_cf100_cuttube120 = new G4Tubs("solid_incoming_cf100_cuttube100", 0., 60., 10., 0., 2. * M_PI);
    solid_incoming_cf100 = new G4SubtractionSolid("solid_incoming_cf100", solid_incoming_cf100, solid_incoming_cf100_cuttube120, 0, G4ThreeVector(-45., 0., 6.6));
    
    
    
    G4LogicalVolume* logical_incoming_cf100 = new G4LogicalVolume(solid_incoming_cf100, G4Material::GetMaterial("StainlessSteel"),"logical_incoming_cf100");
    logical_incoming_cf100->SetVisAttributes(visSteel);
    
    G4VPhysicalVolume *physical_incoming_cf100 = new G4PVPlacement(yRot_incoming_endflange, G4ThreeVector(incoming_endflange_pos_x, 0., incoming_endflange_pos_z), logical_incoming_cf100, "physical_incoming_cf100", logicalTwoBeamPipe, false, 0, checkoverlap);
    
}
  
  
    // OUTGOING
  if (outgoing) {
      
      double xoffset = 43.;
      
    // cut-away-box:
    std::vector<G4TwoVector> cutawayface;
    cutawayface.push_back(G4TwoVector(-80., 0.));
    cutawayface.push_back(G4TwoVector(-80., 80.));
    cutawayface.push_back(G4TwoVector(80., 80.));
    cutawayface.push_back(G4TwoVector(80., 0.));
    G4ExtrudedSolid* solid_cutaway = new G4ExtrudedSolid("solid_cutaway", cutawayface, 80., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
      
    // top part
    
    // points from drawing:
    // part 1 - closer to crotch
    double points1[6][2] = {{1., 0.}, {1., 12.1}, {44.94, 800.}, {15.52, 800.}, {-1., 11.22}, {-1., 0.}};
    double points2[7][2] = {{14.71, 0.}, {17.15, 43.66}, {44.36625, 350.}, {10.24282, 350.}, {-1.36, 112.88}, {-13.78, 44.53}, {-14.71, 0.}};
    
    for (int idx = 0; idx < 6; idx++) {
	points1[idx][1] += 7.;
    }
    
    for (int idx = 0; idx < 7; idx++) {
	points2[idx][0] += 44.94 - 14.71;
	points2[idx][1] += 800. + 7.;
    }
    
    points2[3][0] = ybetween(points2[4][1], points2[4][0], points2[3][1], points2[3][0], 1150.);
    points2[3][1] = 1150.;
    points2[2][0] = ybetween(points2[1][1], points2[1][0], points2[2][1], points2[2][0], 1150);
    points2[2][1] = 1150.;
    
    std::vector<G4TwoVector> polygon;
    polygon.push_back(G4TwoVector(points1[5][0], points1[5][1]));
    polygon.push_back(G4TwoVector(points1[4][0], points1[4][1]));
    for (int idx = 6; idx >= 0; idx--) {
	polygon.push_back(G4TwoVector(points2[idx][0], points2[idx][1]));
    }
    polygon.push_back(G4TwoVector(points1[1][0], points1[1][1]));
    polygon.push_back(G4TwoVector(points1[0][0], points1[0][1]));
    
    G4ExtrudedSolid* solid_outgoing_top_extr = new G4ExtrudedSolid("solid_outgoing_top_extr", polygon, 2., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    // top is only 2 mm thick at flange
    G4Box* solid_outgoing_top_flangesub = new G4Box("solid_outgoing_top_flangesub", 10., 20., 10.);
    
    G4SubtractionSolid* solid_outgoing_top = new G4SubtractionSolid("solid_outgoing_top", solid_outgoing_top_extr, solid_outgoing_top_flangesub, 0, G4ThreeVector(0., 7., -10.));
    G4SubtractionSolid* solid_outgoing_bot = new G4SubtractionSolid("solid_outgoing_bot", solid_outgoing_top_extr, solid_outgoing_top_flangesub, 0, G4ThreeVector(0., 7., 10.));

    G4LogicalVolume* logical_outgoing_top = new G4LogicalVolume(solid_outgoing_top, G4Material::GetMaterial("Copper"),"logical_outgoing_top");
    logical_outgoing_top->SetVisAttributes(visCopper);
    G4LogicalVolume* logical_outgoing_bot = new G4LogicalVolume(solid_outgoing_bot, G4Material::GetMaterial("Copper"),"logical_outgoing_bot");
    logical_outgoing_bot->SetVisAttributes(visCopper);

    G4VPhysicalVolume *physical_outgoing_top = new G4PVPlacement(xRot_270, G4ThreeVector(43., 39., endplate_anchor_z), logical_outgoing_top, "physical_outgoing_top", logicalTwoBeamPipe, false, 0, checkoverlap);
    G4VPhysicalVolume *physical_outgoing_bottom = new G4PVPlacement(xRot_270, G4ThreeVector(43., -39., endplate_anchor_z), logical_outgoing_bot, "physical_outgoing_bottom", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    //  FLANGE part
    //  first part, closer to crotch (second part will be included in ISPB, see below)
    // upper arc
    G4Tubs* solid_flange_uparc = new G4Tubs("solid_flange_uparc", 30., 32., (points1[4][1] - points1[5][1])/2., M_PI/2., M_PI/2.);
    // lower arc
    G4Tubs* solid_flange_lowarc = new G4Tubs("solid_flange_lowarc", 30., 32., (points1[4][1] - points1[5][1])/2., M_PI, M_PI/2.);
    // side part
    G4Box* solid_flange_side = new G4Box("solid_flange_side", 1., 7., (points1[4][1] - points1[5][1])/2.);
    
    // logical volumes:
    G4LogicalVolume* logical_flange_uparc = new G4LogicalVolume(solid_flange_uparc, G4Material::GetMaterial("Copper"), "logical_flange_uparc");
    G4LogicalVolume* logical_flange_lowarc = new G4LogicalVolume(solid_flange_lowarc, G4Material::GetMaterial("Copper"), "logical_flange_lowarc");
    G4LogicalVolume* logical_flange_side = new G4LogicalVolume(solid_flange_side, G4Material::GetMaterial("Copper"), "logical_flange_side");
    
    logical_flange_uparc->SetVisAttributes(visCopper);
    logical_flange_lowarc->SetVisAttributes(visCopper);
    logical_flange_side->SetVisAttributes(visCopper);
    
    // placements:
    G4VPhysicalVolume *physical_flange_uparc = new G4PVPlacement(0, G4ThreeVector(42., 7., endplate_anchor_z + points1[4][1] - (points1[4][1] - points1[5][1])/2.), logical_flange_uparc, "physical_flange_uparc", logicalTwoBeamPipe, false, 0, checkoverlap);
    G4VPhysicalVolume *physical_flange_lowarc = new G4PVPlacement(0, G4ThreeVector(42., -7., endplate_anchor_z + points1[4][1] - (points1[4][1] - points1[5][1])/2.), logical_flange_lowarc, "physical_flange_lowarc", logicalTwoBeamPipe, false, 0, checkoverlap);
    G4VPhysicalVolume *physical_flange_side = new G4PVPlacement(0, G4ThreeVector(11., 0., endplate_anchor_z + points1[4][1] - (points1[4][1] - points1[5][1])/2.), logical_flange_side, "physical_flange_side", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    // ISPB
    
//      // x position at z = 20:
//     double start_of_ispb_z = 20;
//     double start_of_ispb_x = ybetween(points1[4][1], points1[4][0], points1[3][1], points1[3][0], start_of_ispb_z);

    double start_of_ispb_z = points1[4][1];
    double start_of_ispb_x = points1[4][0];
    
    double end_of_ispb_z = points1[3][1];
    double end_of_ispb_x = points1[3][0];
    
    double xpos_ispb = averrage(start_of_ispb_x, end_of_ispb_x);
    double zpos_ispb = averrage(start_of_ispb_z, end_of_ispb_z);
    
    // position of cutaway
    double ispb_cutaway_dist = 0.5*dist(start_of_ispb_x, start_of_ispb_z, end_of_ispb_x, end_of_ispb_z);
    
    // angles
    G4double angle_ispb = atan((start_of_ispb_x - end_of_ispb_x) / (start_of_ispb_z - end_of_ispb_z));
    
    G4RotationMatrix* yRot_ispb = new G4RotationMatrix();
    yRot_ispb->rotateY(-angle_ispb);
    G4RotationMatrix* yRot_ispb_neg = new G4RotationMatrix();
    yRot_ispb_neg->rotateY(angle_ispb);
    
    // cutaway rotation
    G4RotationMatrix* Rot_ispb_cutaway_far = new G4RotationMatrix();
    Rot_ispb_cutaway_far->rotateY(angle_ispb);
    Rot_ispb_cutaway_far->rotateX(-M_PI/2.);
    
    G4RotationMatrix* Rot_ispb_cutaway_near = new G4RotationMatrix();
    Rot_ispb_cutaway_near->rotateY(angle_ispb);
    Rot_ispb_cutaway_near->rotateX(M_PI/2.);
    
    std::vector<G4TwoVector> polygon_ispb;
    polygon_ispb.push_back(G4TwoVector(0., -41.));
    polygon_ispb.push_back(G4TwoVector(-16., -41.));
    polygon_ispb.push_back(G4TwoVector(-34., -23.));
    polygon_ispb.push_back(G4TwoVector(-34., 23.));
    polygon_ispb.push_back(G4TwoVector(-16., 41.));
    polygon_ispb.push_back(G4TwoVector(0., 41.));
    polygon_ispb.push_back(G4TwoVector(0., 37.));
    polygon_ispb.push_back(G4TwoVector(-30., 7.));
    polygon_ispb.push_back(G4TwoVector(-30., -7.));
    polygon_ispb.push_back(G4TwoVector(0., -37.));
    G4ExtrudedSolid* solid_ispb_extrusion = new G4ExtrudedSolid("solid_ispb_extrusion", polygon_ispb, 410., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    
    G4Tubs* solid_ispb_cuttube = new G4Tubs("solid_ispb_cuttube", 0., 30., 420., 0., 2.*M_PI);
    
    G4SubtractionSolid* solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb_extrusion, solid_ispb_cuttube, 0, G4ThreeVector(0., 7., 0.));
    solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb, solid_ispb_cuttube, 0, G4ThreeVector(0., -7., 0.));
    
    solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb, solid_cutaway, Rot_ispb_cutaway_far, G4ThreeVector(0., 0., ispb_cutaway_dist));
    solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb, solid_cutaway, Rot_ispb_cutaway_near, G4ThreeVector(0., 0., -ispb_cutaway_dist));
    
    // cut away arc (flange part)
    G4Tubs* solid_ispb_cutflangearc1 = new G4Tubs("solid_ispb_cutflangearc1", 32., 40., 27.-points1[4][1], M_PI/2. -0.1, M_PI/2. +0.1);
    G4Tubs* solid_ispb_cutflangearc2 = new G4Tubs("solid_ispb_cutflangearc2", 32., 40., 27.-points1[4][1], M_PI , M_PI/2. +0.1);
    solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb, solid_ispb_cutflangearc1, yRot_ispb_neg, G4ThreeVector(0., 7., -ispb_cutaway_dist));
    solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb, solid_ispb_cutflangearc2, yRot_ispb_neg, G4ThreeVector(0., -7., -ispb_cutaway_dist));
    
    // cut away straight part at beginning (narrowing of vertical wall from 4mm to 2mm)
    std::vector<G4TwoVector> polygon_ispb_narrowingcut;
    polygon_ispb_narrowingcut.push_back(G4TwoVector(-32., -40.));
    polygon_ispb_narrowingcut.push_back(G4TwoVector(-40., -40.));
    polygon_ispb_narrowingcut.push_back(G4TwoVector(-40., 40.));
    polygon_ispb_narrowingcut.push_back(G4TwoVector(-32., 40.));
    G4ExtrudedSolid* solid_ispb_narrowingcut = new G4ExtrudedSolid("solid_ispb_narrowingcut",  polygon_ispb_narrowingcut, 120., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb, solid_ispb_narrowingcut, yRot_ispb_neg, G4ThreeVector(0., 0., -ispb_cutaway_dist));
    
    G4LogicalVolume* logical_ispb = new G4LogicalVolume(solid_ispb, G4Material::GetMaterial("Copper"),"logical_ispb");
    logical_ispb->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_ispb = new G4PVPlacement(yRot_ispb, G4ThreeVector(xoffset + xpos_ispb, 0., endplate_anchor_z + zpos_ispb), logical_ispb, "physical_ispb", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    
    
    // PRE WINDOW
    // x position at beginning:
    double start_of_prewindow_z = points2[6][1];
    double start_of_prewindow_x = points2[6][0];
    
    double end_of_prewindow_z = points2[5][1];
    double end_of_prewindow_x = points2[5][0];
    
    double xpos_prewindow = averrage(start_of_prewindow_x, end_of_prewindow_x);
    double zpos_prewindow = averrage(start_of_prewindow_z, end_of_prewindow_z);
    
    // position of cutaway
    double prewindow_cutaway_dist = 0.5*dist(start_of_prewindow_x, start_of_prewindow_z, end_of_prewindow_x, end_of_prewindow_z);
    
    // angles
    G4double angle_prewindow = atan((start_of_prewindow_x - end_of_prewindow_x) / (start_of_prewindow_z - end_of_prewindow_z));
    G4double angle_window = atan((points2[5][0] - points2[4][0]) / (points2[5][1] - points2[4][1]));
    G4double angle_prewindow_far = 0.5 * (angle_prewindow - angle_window);
    
    G4RotationMatrix* yRot_prewindow = new G4RotationMatrix();
    yRot_prewindow->rotateY(-angle_prewindow);
    
    // cutaway rotation
    G4RotationMatrix* Rot_prewindow_cutaway_far = new G4RotationMatrix();
    Rot_prewindow_cutaway_far->rotateY(angle_prewindow_far);
    Rot_prewindow_cutaway_far->rotateX(-M_PI/2.);
    
    G4RotationMatrix* Rot_prewindow_cutaway_near = new G4RotationMatrix();
    Rot_prewindow_cutaway_near->rotateY(angle_prewindow);
    Rot_prewindow_cutaway_near->rotateX(M_PI/2.);
    
    // slit position
    double slitthickness = 2.5;
    double slitpos_prewindow_xlocal = - 34.;
    double slitpos_prewindow_zlocal = prewindow_cutaway_dist + 34. * tan(-angle_prewindow_far);
    
    // slit rotation
    G4RotationMatrix* yRot_prewindow_slit = new G4RotationMatrix();
    yRot_prewindow_slit->rotateY(2 * angle_prewindow_far);
    
    std::vector<G4TwoVector> polygon_prewindow;
    polygon_prewindow.push_back(G4TwoVector(0., -41.));
    polygon_prewindow.push_back(G4TwoVector(-16., -41.));
    polygon_prewindow.push_back(G4TwoVector(-34., -23.));
    polygon_prewindow.push_back(G4TwoVector(-34., 23.));
    polygon_prewindow.push_back(G4TwoVector(-16., 41.));
    polygon_prewindow.push_back(G4TwoVector(0., 41.));
    polygon_prewindow.push_back(G4TwoVector(0., 37.));
    polygon_prewindow.push_back(G4TwoVector(-30., 7.));
    polygon_prewindow.push_back(G4TwoVector(-30., -7.));
    polygon_prewindow.push_back(G4TwoVector(0., -37.));
    G4ExtrudedSolid* solid_prewindow_extrusion = new G4ExtrudedSolid("solid_prewindow_extrusion", polygon_prewindow, prewindow_cutaway_dist + 20., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    
    G4Tubs* solid_prewindow_cuttube = new G4Tubs("solid_prewindow_cuttube", 0., 30., prewindow_cutaway_dist + 30., 0., 2.*M_PI);
    
    G4SubtractionSolid* solid_prewindow = new G4SubtractionSolid("solid_prewindow", solid_prewindow_extrusion, solid_prewindow_cuttube, 0, G4ThreeVector(0., 7., 0.));
    solid_prewindow = new G4SubtractionSolid("solid_prewindow", solid_prewindow, solid_prewindow_cuttube, 0, G4ThreeVector(0., -7., 0.));
    
    solid_prewindow = new G4SubtractionSolid("solid_prewindow", solid_prewindow, solid_cutaway, Rot_prewindow_cutaway_far, G4ThreeVector(0., 0., prewindow_cutaway_dist));
    solid_prewindow = new G4SubtractionSolid("solid_prewindow", solid_prewindow, solid_cutaway, Rot_prewindow_cutaway_near, G4ThreeVector(0., 0., -prewindow_cutaway_dist));
    
    // slit:
    std::vector<G4TwoVector> prewindow_slit;
    prewindow_slit.push_back(G4TwoVector(-2.5, 2.5));
    prewindow_slit.push_back(G4TwoVector(2.5, 2.5));
    prewindow_slit.push_back(G4TwoVector(2.5, -2.5));
    prewindow_slit.push_back(G4TwoVector(-2.5, -2.5));
    G4ExtrudedSolid* solid_prewindow_slit = new G4ExtrudedSolid("solid_prewindow_slit", prewindow_slit, 20., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
     solid_prewindow = new G4SubtractionSolid("solid_prewindow", solid_prewindow, solid_prewindow_slit, yRot_prewindow_slit, G4ThreeVector(slitpos_prewindow_xlocal, 0., slitpos_prewindow_zlocal));
    
    G4LogicalVolume* logical_prewindow = new G4LogicalVolume(solid_prewindow, G4Material::GetMaterial("Copper"),"logical_prewindow");
    logical_prewindow->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_prewindow = new G4PVPlacement(yRot_prewindow, G4ThreeVector(xoffset + xpos_prewindow, 0., endplate_anchor_z + zpos_prewindow), logical_prewindow, "physical_prewindow", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    // WINDOW
    // x position at beginning:
    double start_of_window_z = points2[5][1];
    double start_of_window_x = points2[5][0];
    
    double end_of_window_z = points2[4][1];
    double end_of_window_x = points2[4][0];
    
    double xpos_window = averrage(start_of_window_x, end_of_window_x);
    double zpos_window = averrage(start_of_window_z, end_of_window_z);
    
    // position of cutaway
    double afterwindow_cutaway_dist = 0.5*dist(start_of_window_x, start_of_window_z, end_of_window_x, end_of_window_z);
    
    // angles
//     G4double angle_window = atan((start_of_window_x - end_of_window_x) / (start_of_window_z - end_of_window_z));
    G4double angle_afterwindow = atan((points2[4][0] - points2[3][0]) / (points2[4][1] - points2[3][1]));
    G4double angle_window_far = 0.5 * (angle_window - angle_afterwindow);
    G4double angle_window_near = 0.5 * (angle_window - angle_prewindow);
    
    G4RotationMatrix* yRot_window = new G4RotationMatrix();
    yRot_window->rotateY(-angle_window);
    
    // cutaway rotation
    G4RotationMatrix* Rot_window_cutaway_far = new G4RotationMatrix();
    Rot_window_cutaway_far->rotateY(angle_window_far);
    Rot_window_cutaway_far->rotateX(-M_PI/2.);
    
    G4RotationMatrix* Rot_window_cutaway_near = new G4RotationMatrix();
    Rot_window_cutaway_near->rotateY(angle_window_near);
    Rot_window_cutaway_near->rotateX(M_PI/2.);
    
    // slit position
    double slitpos_window_xlocal = - 34.;
    double slitpos_window_zlocal = 0.;
    
    // slit rotation
//     G4RotationMatrix* yRot_window_slit = new G4RotationMatrix();
//     yRot_window_slit->rotateY(2 * angle_window_far);
    
    std::vector<G4TwoVector> polygon_window;
    polygon_window.push_back(G4TwoVector(0., -55.75));
    polygon_window.push_back(G4TwoVector(-34., -55.75));
    polygon_window.push_back(G4TwoVector(-34., 55.75));
    polygon_window.push_back(G4TwoVector(0., 55.75));
    polygon_window.push_back(G4TwoVector(0., 37.));
    polygon_window.push_back(G4TwoVector(-30., 7.));
    polygon_window.push_back(G4TwoVector(-30., -7.));
    polygon_window.push_back(G4TwoVector(0., -37.));
    G4ExtrudedSolid* solid_window_extrusion = new G4ExtrudedSolid("solid_window_extrusion", polygon_window, afterwindow_cutaway_dist + 20., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    
    G4Tubs* solid_window_cuttube = new G4Tubs("solid_window_cuttube", 0., 30., afterwindow_cutaway_dist + 30., 0., 2.*M_PI);
    
    G4SubtractionSolid* solid_window = new G4SubtractionSolid("solid_window", solid_window_extrusion, solid_window_cuttube, 0, G4ThreeVector(0., 7., 0.));
    solid_window = new G4SubtractionSolid("solid_window", solid_window, solid_window_cuttube, 0, G4ThreeVector(0., -7., 0.));
    
    solid_window = new G4SubtractionSolid("solid_window", solid_window, solid_cutaway, Rot_window_cutaway_far, G4ThreeVector(0., 0., afterwindow_cutaway_dist));
    solid_window = new G4SubtractionSolid("solid_window", solid_window, solid_cutaway, Rot_window_cutaway_near, G4ThreeVector(0., 0., -afterwindow_cutaway_dist));
    
    // slit:
    std::vector<G4TwoVector> window_slit;
    window_slit.push_back(G4TwoVector(-2.5, 2.5));
    window_slit.push_back(G4TwoVector(2.5, 2.5));
    window_slit.push_back(G4TwoVector(2.5, -2.5));
    window_slit.push_back(G4TwoVector(-2.5, -2.5));
    G4ExtrudedSolid* solid_window_slit = new G4ExtrudedSolid("solid_window_slit", window_slit, 50., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    solid_window = new G4SubtractionSolid("solid_window", solid_window, solid_window_slit, 0, G4ThreeVector(slitpos_window_xlocal, 0., slitpos_window_zlocal));
    
    // cut away rectangular part at beginning of window
    
    // positions and angles:
    double window_cutaway_cut_pos_glob_z = 855. + 7.;
    double window_cutaway_cut_pos_glob_x = ybetween(points2[6][1], points2[6][0], points2[5][1], points2[5][0], window_cutaway_cut_pos_glob_z);
    
    double window_cutaway_cut_pos_loc_z = dist(points2[5][0], points2[5][1], window_cutaway_cut_pos_glob_x, window_cutaway_cut_pos_glob_z);
    double window_cutaway_cut_pos_loc_x = 0.;
    
    G4RotationMatrix* Rot_window_cutaway_cut = new G4RotationMatrix();
    Rot_window_cutaway_cut->rotateY(angle_prewindow);
    Rot_window_cutaway_cut->rotateX(-M_PI/2.);
    
    G4RotationMatrix* Rot_window_cutaway = new G4RotationMatrix();
    Rot_window_cutaway->rotateY(2. * angle_window_near);
    
    std::vector<G4TwoVector> window_cutaway_beginning;
    window_cutaway_beginning.push_back(G4TwoVector(10., 60.));
    window_cutaway_beginning.push_back(G4TwoVector(10., 41.));
    window_cutaway_beginning.push_back(G4TwoVector(-16., 41.));
    window_cutaway_beginning.push_back(G4TwoVector(-34., 23.));
    window_cutaway_beginning.push_back(G4TwoVector(-34., -23.));
    window_cutaway_beginning.push_back(G4TwoVector(-16., -41.));
    window_cutaway_beginning.push_back(G4TwoVector(10., -41.));
    window_cutaway_beginning.push_back(G4TwoVector(10., -60.));
    window_cutaway_beginning.push_back(G4TwoVector(-50., -60.));
    window_cutaway_beginning.push_back(G4TwoVector(-50., 60.));
    G4ExtrudedSolid* solid_window_cutaway_extr = new G4ExtrudedSolid("solid_window_cutaway_extr", window_cutaway_beginning, 30., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    G4SubtractionSolid* solid_window_cutaway = new G4SubtractionSolid("solid_window_cutaway", solid_window_cutaway_extr, solid_cutaway, Rot_window_cutaway_cut, G4ThreeVector(window_cutaway_cut_pos_loc_x, 0., window_cutaway_cut_pos_loc_z));
    
    solid_window = new G4SubtractionSolid("solid_window", solid_window, solid_window_cutaway, Rot_window_cutaway, G4ThreeVector(0., 0., -afterwindow_cutaway_dist));
    
    
    
    G4LogicalVolume* logical_window = new G4LogicalVolume(solid_window, G4Material::GetMaterial("Copper"),"logical_window");
    logical_window->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_window = new G4PVPlacement(yRot_window, G4ThreeVector(xoffset + xpos_window, 0., endplate_anchor_z + zpos_window), logical_window, "physical_window", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    // DUMMY WINDOW
//     G4Box* solid_dummyWindow = new G4Box("solid_dummyWindow", 60., 60., 20.);
    
//     G4LogicalVolume* logical_dummyWindow = new G4LogicalVolume(solid_dummyWindow, G4Material::GetMaterial("Copper"),"logical_dummyWindow");
//     logical_dummyWindow->SetVisAttributes(visCopper);
    
//     G4VPhysicalVolume *physical_dummyWindow = new G4PVPlacement(0, G4ThreeVector(65.501, 0., endplate_anchor_z + 869.598), logical_dummyWindow, "physical_dummyWindow", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    
    
    // AFTER WINDOW
// x position at beginning:
    double start_of_afterwindow_z = points2[4][1];
    double start_of_afterwindow_x = points2[4][0];
    
    double end_of_afterwindow_z = points2[3][1];
    double end_of_afterwindow_x = points2[3][0];
    
    double xpos_afterwindow = averrage(start_of_afterwindow_x, end_of_afterwindow_x);
    double zpos_afterwindow = averrage(start_of_afterwindow_z, end_of_afterwindow_z);
    
    // position of cutaway
    double window_cutaway_dist = 0.5*dist(start_of_afterwindow_x, start_of_afterwindow_z, end_of_afterwindow_x, end_of_afterwindow_z);
    
    // angles
//     G4double angle_afterwindow = atan((points2[4][0] - points2[3][0]) / (points2[4][1] - points2[3][1]));
//     G4double angle_afterwindow_far = 0.5 * (angle_afterwindow - angle_afterwindow);
    G4double angle_afterwindow_near = 0.5 * (angle_afterwindow - angle_window);
    
    G4RotationMatrix* yRot_afterwindow = new G4RotationMatrix();
    yRot_afterwindow->rotateY(-angle_afterwindow);
    
    // cutaway rotation
    G4RotationMatrix* Rot_afterwindow_cutaway_far = new G4RotationMatrix();
    Rot_afterwindow_cutaway_far->rotateY(angle_afterwindow);
    Rot_afterwindow_cutaway_far->rotateX(-M_PI/2.);
    
    G4RotationMatrix* Rot_afterwindow_cutaway_near = new G4RotationMatrix();
    Rot_afterwindow_cutaway_near->rotateY(angle_afterwindow_near);
    Rot_afterwindow_cutaway_near->rotateX(M_PI/2.);
    
    // slit position
    double slitpos_afterwindow_xlocal = - 31.5;
    double slitpos_afterwindow_zlocal = - window_cutaway_dist + 31.5 * tan(-angle_afterwindow_near);
    
    // slit rotation
    G4double angle_afterwindow_slit = angle_afterwindow + 0.63*M_PI/180.;
    G4RotationMatrix* yRot_afterwindow_slit = new G4RotationMatrix();
    yRot_afterwindow_slit->rotateY(angle_afterwindow_slit);
    
    std::vector<G4TwoVector> polygon_afterwindow;
    polygon_afterwindow.push_back(G4TwoVector(0., -55.75));
    polygon_afterwindow.push_back(G4TwoVector(-34., -55.75));
    polygon_afterwindow.push_back(G4TwoVector(-34., 55.75));
    polygon_afterwindow.push_back(G4TwoVector(0., 55.75));
    polygon_afterwindow.push_back(G4TwoVector(0., 37.));
    polygon_afterwindow.push_back(G4TwoVector(-30., 7.));
    polygon_afterwindow.push_back(G4TwoVector(-30., -7.));
    polygon_afterwindow.push_back(G4TwoVector(0., -37.));
    G4ExtrudedSolid* solid_afterwindow_extrusion = new G4ExtrudedSolid("solid_afterwindow_extrusion", polygon_afterwindow, window_cutaway_dist + 20., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    
    G4Tubs* solid_afterwindow_cuttube = new G4Tubs("solid_afterwindow_cuttube", 0., 30., window_cutaway_dist + 30., 0., 2.*M_PI);
    
    G4SubtractionSolid* solid_afterwindow = new G4SubtractionSolid("solid_afterwindow", solid_afterwindow_extrusion, solid_afterwindow_cuttube, 0, G4ThreeVector(0., 7., 0.));
    solid_afterwindow = new G4SubtractionSolid("solid_afterwindow", solid_afterwindow, solid_afterwindow_cuttube, 0, G4ThreeVector(0., -7., 0.));
    
    solid_afterwindow = new G4SubtractionSolid("solid_afterwindow", solid_afterwindow, solid_cutaway, Rot_afterwindow_cutaway_far, G4ThreeVector(0., 0., window_cutaway_dist));
    solid_afterwindow = new G4SubtractionSolid("solid_afterwindow", solid_afterwindow, solid_cutaway, Rot_afterwindow_cutaway_near, G4ThreeVector(0., 0., -window_cutaway_dist));
    
    // slit:
    std::vector<G4TwoVector> afterwindow_slit;
    afterwindow_slit.push_back(G4TwoVector(-5., 2.5));
    afterwindow_slit.push_back(G4TwoVector(0., 2.5));
    afterwindow_slit.push_back(G4TwoVector(0., -2.5));
    afterwindow_slit.push_back(G4TwoVector(-5., -2.5));
    G4ExtrudedSolid* solid_afterwindow_slit = new G4ExtrudedSolid("solid_afterwindow_slit", afterwindow_slit, 100., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    solid_afterwindow = new G4SubtractionSolid("solid_afterwindow", solid_afterwindow, solid_afterwindow_slit, yRot_afterwindow_slit, G4ThreeVector(slitpos_afterwindow_xlocal, 0., slitpos_afterwindow_zlocal));
    
    
    G4LogicalVolume* logical_afterwindow = new G4LogicalVolume(solid_afterwindow, G4Material::GetMaterial("Copper"),"logical_afterwindow");
    logical_afterwindow->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_afterwindow = new G4PVPlacement(yRot_afterwindow, G4ThreeVector(xoffset + xpos_afterwindow, 0., endplate_anchor_z + zpos_afterwindow), logical_afterwindow, "physical_afterwindow", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    // TOP AFTER WINDOW
    
    // outer start point at 855. :
//     double point1_of_topafterwindow_z = 855. + 7.;
//     double point1_of_topafterwindow_x = ybetween(points2[1][0], points2[1][1], points2[2][0], points2[2][1], point1_of_topafterwindow_z);
//     double point2_of_topafterwindow_z = 855. + 7.;
//     double point2_of_topafterwindow_x = ybetween(points2[5][0], points2[5][1], points2[4][0], points2[4][1], point2_of_topafterwindow_z);
    
    std::vector<G4TwoVector> polygon_topafterwindow;
    for (int idx = 6; idx >= 0; idx--) {
	polygon_topafterwindow.push_back(G4TwoVector(points2[idx][0], points2[idx][1]));
    }
    
    G4ExtrudedSolid* solid_topafterwindow_extr = new G4ExtrudedSolid("solid_topafterwindow_extr", polygon_topafterwindow, 7.375, G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    // cut at 855:
    G4Box *solid_topafterwindow_cutbox = new G4Box("solid_topafterwindow_cutbox", 100., 100., 100.);
    
    G4SubtractionSolid *solid_topafterwindow = new G4SubtractionSolid("solid_topafterwindow", solid_topafterwindow_extr, solid_topafterwindow_cutbox, 0, G4ThreeVector(0., 755. + 7., 0.));
    
    G4LogicalVolume* logical_topafterwindow = new G4LogicalVolume(solid_topafterwindow, G4Material::GetMaterial("Copper"),"logical_topafterwindow");
    logical_topafterwindow->SetVisAttributes(visCopper);

    G4VPhysicalVolume *physical_topafterwindow_top = new G4PVPlacement(xRot_270, G4ThreeVector(43., 39. + 7.375 + 2., endplate_anchor_z), logical_topafterwindow, "physical_topafterwindow_top", logicalTwoBeamPipe, false, 0, checkoverlap);
    G4VPhysicalVolume *physical_topafterwindow_bot = new G4PVPlacement(xRot_270, G4ThreeVector(43., -39. - 7.375 - 2., endplate_anchor_z), logical_topafterwindow, "physical_topafterwindow_bot", logicalTwoBeamPipe, false, 0, checkoverlap);



    
    // DUMMY cZDD
    if (cZDDdummy) {
	
	G4Box* solid_cZDD = new G4Box("solid_cZDD", 20., 15., 70.);
	
	G4LogicalVolume* logical_cZDD = new G4LogicalVolume(solid_cZDD, G4Material::GetMaterial("Copper"), "logical_cZDD");
	logical_cZDD->SetVisAttributes(viscZDD);
	
	G4VPhysicalVolume *physical_cZDD1 = new G4PVPlacement(0, G4ThreeVector(20., 20., endplate_anchor_z + 1150.+70.), logical_cZDD, "physical_cZDD1", logicalTwoBeamPipe, false, 0, checkoverlap);
	G4VPhysicalVolume *physical_cZDD2 = new G4PVPlacement(0, G4ThreeVector(20., -20., endplate_anchor_z + 1150.+70.), logical_cZDD, "physical_cZDD2", logicalTwoBeamPipe, false, 0, checkoverlap);
	
    }


    
}
  
  









}
