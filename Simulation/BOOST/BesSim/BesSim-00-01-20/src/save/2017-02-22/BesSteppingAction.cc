
#include "BesSteppingAction.hh"
#include "G4StepPoint.hh"
#include "G4ThreeVector.hh"
#include "G4Track.hh"

#include "G4UnitsTable.hh"
#include "G4VProcess.hh"

G4double BesSteppingAction:: m_x = -99.;
G4double BesSteppingAction:: m_y = -99.;
G4double BesSteppingAction:: m_z = -99.;

BesSteppingAction::BesSteppingAction()
{ }

BesSteppingAction::BesSteppingAction(BesEventAction* eventAction)
:m_EventAction(eventAction)
{}

BesSteppingAction::~BesSteppingAction()
{ }


void BesSteppingAction::UserSteppingAction(const G4Step* currentStep)
{
  G4StepPoint* postStep = currentStep->GetPostStepPoint();

  // check for volume changes: particle enters Zdd
  G4int crystalNo = abs(postStep->GetTouchableHandle()->GetCopyNumber()); //absolute value of crystal No, forget if east or west part...
  //! marcel
  G4StepPoint* uPreStep = currentStep->GetPreStepPoint();
  G4ThreeVector currentPosition = postStep->GetPosition();
	G4ThreeVector currentMomentum = postStep->GetMomentum();	
	G4ThreeVector preStepPosition = uPreStep->GetPosition();
	G4ThreeVector preStepMomentum = uPreStep->GetMomentum();
  G4Track* currentTrack = currentStep->GetTrack();
	
	//printf("	uPreStep->GetTouchableHandle()->GetVolume()->GetName() = %s \n", uPreStep->GetTouchableHandle()->GetVolume()->GetName().data());
	//printf("	postStep->GetTouchableHandle()->GetVolume()->GetName() = %s \n", postStep->GetTouchableHandle()->GetVolume()->GetName().data());
	//printf("\n");
	
    //if( fabs(currentPosition.z())/cm > 334.9995 && fabs(currentPosition.z())/cm < 335.0005 && fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() )
    
		//if(/* fabs(currentPosition.z())/cm > 180. && fabs(currentPosition.z())/cm < 220 && fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() */)

//    if(1)

//if( fabs(currentPosition.z())/cm > 334.9995 && fabs(currentPosition.z())/cm < 335.0005 && fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() && uPreStep->GetTouchableHandle()->GetVolume()->GetName()=="physicWorld")

//if( fabs(currentPosition.z())/cm > 334.9995 && fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() && uPreStep->GetTouchableHandle()->GetVolume()->GetName()=="physicWorld")

//if( fabs(currentPosition.z())/cm > 334.9995 && fabs(currentPosition.z())/cm < 348.9995 && fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() && uPreStep->GetTouchableHandle()->GetVolume()->GetName()=="physicWorld")

//if( fabs(currentPosition.z())/cm > 334.9995 && fabs(currentPosition.z())/cm < 348.9995 && fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume())

//if( fabs(currentPosition.z())/cm > 334.9995 && fabs(currentPosition.z())/cm < 348.9995 && fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() && uPreStep->GetTouchableHandle()->GetVolume()->GetName()=="physicWorld"
//&& (postStep->GetTouchableHandle()->GetVolume()->GetName().contains("physicZdd") || postStep->GetTouchableHandle()->GetVolume()->GetName().contains("crystal")))

if( fabs(currentPosition.z())/cm > 334.9995 && fabs(currentPosition.z())/cm < 355. && fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() && uPreStep->GetTouchableHandle()->GetVolume()->GetName()=="physicWorld"
&& (postStep->GetTouchableHandle()->GetVolume()->GetName().contains("physicZdd") || postStep->GetTouchableHandle()->GetVolume()->GetName().contains("crystal")))
    {
		
		//printf("BesSteppingAction::UserSteppingAction ZDD Hit \n");
		//printf("	uPreStep->GetTouchableHandle()->GetVolume()->GetName() = %s \n", uPreStep->GetTouchableHandle()->GetVolume()->GetName().data());
	//printf("	postStep->GetTouchableHandle()->GetVolume()->GetName() = %s \n", postStep->GetTouchableHandle()->GetVolume()->GetName().data());
    //printf("	currentTrack->GetParticleDefinition()->GetPDGEncoding()=%d \n",currentTrack->GetDefinition()->GetPDGEncoding());
		//printf("	preStepPosition.x()=%f preStepPosition.y()=%f preStepPosition.z()=%f \n",preStepPosition.x(),preStepPosition.y(),preStepPosition.z() );
		//printf("	preStepMomentum.x()=%f preStepMomentum.y()=%f preStepMomentum.z()=%f \n",preStepMomentum.x(),preStepMomentum.y(),preStepMomentum.z() );
		//printf("	currentPosition.x()=%f currentPosition.y()=%f currentPosition.z()=%f \n",currentPosition.x(),currentPosition.y(),currentPosition.z() );
		//printf("	currentMomentum.x()=%f currentMomentum.y()=%f currentMomentum.z()=%f \n",currentMomentum.x(),currentMomentum.y(),currentMomentum.z() );		
		//printf("\n");
		
      //      double uTheta = atan( sqrt( pow(currentPosition.x()/cm, 2) + pow(currentPosition.y()/cm, 2) )/fabs(currentPosition.z()/cm) );// * 180. / 3.1415;
      BesSteppingAction::m_x = currentPosition.x()/cm;
      BesSteppingAction::m_y = currentPosition.y()/cm;
			BesSteppingAction::m_z = currentPosition.z()/cm;
			
			m_EventAction->AddZddImpacts(G4ThreeVector(m_x,m_y,m_z));
			m_EventAction->AddZddPID(currentTrack->GetDefinition()->GetPDGEncoding());
/*
      double px = postStep->GetMomentum().x()/MeV;
      double py = postStep->GetMomentum().y()/MeV;
      double pz = postStep->GetMomentum().z()/MeV;
      double e = sqrt( px*px + py*py + pz*pz);
      double momentum_theta = atan2(sqrt(px*px + py*py),pz);

      G4cout << "BesSim::BesSteppingAction::UserSteppingAction : primary particle px py pz e [MeV] theta : " << px << "\t" << py << "\t" << pz << "\t" << e << "\t" << momentum_theta << G4endl;
      G4cout << " x, y, z [cm] : " << currentPosition.x()/cm << " , " << currentPosition.y()/cm << " , " << currentPosition.z()/cm << G4endl;
      G4cout << " phi [rad]   :    " << atan2( currentPosition.y(), currentPosition.x() ) << G4endl; // * 180. / 3.1415 << G4endl;

      G4cout << " preStep Theta from p at ZDD [rad] :    " << uPreStep->GetMomentum().theta() << G4endl;
      G4cout << " postStep Theta from p at ZDD [rad] :    " << postStep->GetMomentum().theta() << G4endl; 
      G4cout << " preStep Phi from p at ZDD [rad] :    " << uPreStep->GetMomentum().phi() << G4endl;
      G4cout << " postStep Phi from p at ZDD [rad] :    " << postStep->GetMomentum().phi() << G4endl << "==========================" << G4endl;;

      G4cout << "Name:   "<<(postStep->GetTouchableHandle()->GetVolume())->GetName()<<G4endl; 
      G4cout  << "PDG:   "<<currentStep->GetTrack()->GetDefinition()->GetPDGEncoding()<<G4endl;
*/

       // if( fabs(currentPosition.y())/cm >= 0.5 && fabs(currentPosition.y())/cm <= 3.5 && fabs(currentPosition.x())/cm <=2. && postStep->GetTouchableHandle()->GetVolume() != uPreStep->GetTouchableHandle()->GetVolume() )
      //	{

      /*
      if(crystalNo == 101 || crystalNo == 102 || crystalNo == 103 || crystalNo == 104 || crystalNo == 111 || crystalNo == 112 || crystalNo == 113 || crystalNo == 114 || crystalNo == 121 || crystalNo == 122 || crystalNo == 123 || crystalNo == 124 || crystalNo == 201 || crystalNo == 202 || crystalNo == 203 || crystalNo == 204 || crystalNo == 211 || crystalNo == 212 || crystalNo == 213 || crystalNo == 214 || crystalNo == 221 || crystalNo == 222 || crystalNo == 223 || crystalNo == 224)
      {*/
      /*    
          double uTheta = atan( sqrt( pow(currentPosition.x()/cm, 2) + pow(currentPosition.y()/cm, 2) )/fabs(currentPosition.z()/cm) );// * 180. / 3.1415;
	  
	  double px = postStep->GetMomentum().x()/MeV;
	  double py = postStep->GetMomentum().y()/MeV;
	  double pz = postStep->GetMomentum().z()/MeV;
	  double e = sqrt( px*px + py*py + pz*pz);
	  double momentum_theta = atan2(sqrt(px*px + py*py),pz);
	  
	  G4cout << "BesSim::BesSteppingAction::UserSteppingAction : primary particle px py pz e [MeV] theta : " << px << "\t" << py << "\t" << pz << "\t" << e << "\t" << momentum_theta << G4endl;
	  G4cout << "BesSim::BesSteppingAction::UserSteppingAction : Particle enters the Zdd !!!" << G4endl;
	  G4cout << " x, y, z [cm] : " << currentPosition.x()/cm << " , " << currentPosition.y()/cm << " , " << currentPosition.z()/cm << G4endl;
	  G4cout << " theta [rad] :    " << uTheta << G4endl;
	  G4cout << " phi [rad]   :    " << atan2( currentPosition.y(), currentPosition.x() ) << G4endl; // * 180. / 3.1415 << G4endl;
          G4cout << " crystalNo : " << postStep->GetTouchableHandle()->GetCopyNumber() << G4endl << G4endl;

	  G4cout << " preStep Theta from p at ZDD [rad] :    " << uPreStep->GetMomentum().theta() << G4endl;
          G4cout << " postStep Theta from p at ZDD [rad] :    " << postStep->GetMomentum().theta() << G4endl; 
	  G4cout << " preStep Phi from p at ZDD [rad] :    " << uPreStep->GetMomentum().phi() << G4endl;
          G4cout << " postStep Phi from p at ZDD [rad] :    " << postStep->GetMomentum().phi() << G4endl << "==========================" << G4endl;;
*/       
	  //	}

    }
    else if( fabs(currentPosition.z())/cm >= 355. )
    {
      BesSteppingAction::m_x = -99.;
      BesSteppingAction::m_y   = -99.;
			BesSteppingAction::m_z   = -99.;
			
			m_EventAction->AddZddImpacts(G4ThreeVector(m_x,m_y,m_z));
			m_EventAction->AddZddPID(currentTrack->GetDefinition()->GetPDGEncoding());
   //      G4cout << "Kill track because it is not in ZDD" << G4endl;
      //      currentTrack->SetTrackStatus(fKillTrackAndSecondaries);      
    }


  //! marcel : ATTENTION:  change this back to abs(currentPosition.z()) > 360.*cm to enable zdd readout
  //  G4Track* currentTrack = currentStep->GetTrack();
  if(std::abs(currentPosition.x()) > 263.5*cm||
     std::abs(currentPosition.y()) > 263.5*cm||
     std::abs(currentPosition.z()) > 360.*cm){
    //    G4cout<<"BesSim :: BesSteppingAction: Out of World!!! "<< G4endl;// << "x[cm] = " << abs(currentPosition.x())/cm << G4endl
    //	  << "y[cm] = " << abs(currentPosition.y())/cm << G4endl << "z[cm] = " << abs(currentPosition.z())/cm << G4endl
    //	  << "=====================" << G4endl;
    currentTrack->SetTrackStatus(fKillTrackAndSecondaries);
  }else if(currentTrack->GetCurrentStepNumber()>=20000){  //20000 before
    G4cout<<"BesSim :: BesSteppingAction: StepNumber>=20000 !!!"<<G4endl; 
    currentTrack->SetTrackStatus(fKillTrackAndSecondaries);
  }
  /*
  if(std::abs(currentPosition.x()) <5*cm&&
     std::abs(currentPosition.y()) < 5*cm&&
     std::abs(currentPosition.z()) > 330.*cm){
    G4cout<<"BesSim :: BesSteppingAction: close to Zdd! "<< G4endl << "x[cm] = " << abs(currentPosition.x())/cm << G4endl
          << "y[cm] = " << abs(currentPosition.y())/cm << G4endl << "z[cm] = " << abs(currentPosition.z())/cm << G4endl
          << "=====================" << G4endl;

G4cout<<"#Step# "<<currentTrack->GetCurrentStepNumber()<<" pName "<<currentTrack->GetDefinition()->GetParticleName()<<" prex prey prez "<<currentStep->GetPreStepPoint()->GetPosition().x()/mm<<" "<<currentStep->GetPreStepPoint()->GetPosition().y()/mm<<" "<<currentStep->GetPreStepPoint()->GetPosition().z()/mm<<G4endl;

     G4cout<<"prepx prepy prepz "<<currentStep->GetPreStepPoint()->GetMomentum().x()/GeV<<" "<<currentStep->GetPreStepPoint()->GetMomentum().y()/GeV<<" "<<currentStep->GetPreStepPoint()->GetMomentum().z()/GeV<<G4endl;
     G4cout<<"post step postx posty postz "<<postStep->GetPosition().x()/mm<<" "<<postStep->GetPosition().y()/mm<<" "<<postStep->GetPosition().z()/mm<<G4endl;
     G4cout<<"postpx postpy postpz "<<postStep->GetMomentum().x()/GeV<<" "<<postStep->GetMomentum().y()/GeV<<" "<<postStep->GetMomentum().z()/GeV<<G4endl;         
      G4cout << G4endl;
      G4cout << std::setw( 5) << "#Step#"     << " "
             << std::setw( 9) << "pName"      << " "
             << std::setw( 6) << "X"          << "    "
             << std::setw( 6) << "Y"          << "    "
             << std::setw( 6) << "Z"          << "    "
             << std::setw( 9) << "KineE"      << " "
             << std::setw( 9) << "dEStep"     << " "
             << std::setw(10) << "StepLeng"
             << std::setw(10) << "Volume"    << "  "
             << std::setw(10) << "Process"  
             << G4endl;
    

    G4cout << std::setw(5) << currentTrack->GetCurrentStepNumber() << " "
        << std::setw(9) << currentTrack->GetDefinition()->GetParticleName()
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().x(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().y(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().z(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetKineticEnergy(),"Energy")
        << std::setw(6) << G4BestUnit(currentStep->GetTotalEnergyDeposit(),"Energy")
	   << std::setw(6) << G4BestUnit(currentStep->GetStepLength(),"Length") << G4endl;

	   
    if( currentTrack->GetNextVolume() != 0 ) {
        G4cout << std::setw(10) << currentTrack->GetVolume()->GetName();
     } else {
       G4cout << std::setw(10) << "OutOfWorld";
     } 
     
    if(currentStep->GetPostStepPoint()->GetProcessDefinedStep() != NULL){
      G4cout << "  "
        << std::setw(10) << currentStep->GetPostStepPoint()->GetProcessDefinedStep()
                                        ->GetProcessName();
    } else {                            
      G4cout << "   UserLimit";
    } 


    currentTrack->SetTrackStatus(fKillTrackAndSecondaries);
  }else if(currentTrack->GetCurrentStepNumber()>=30000){  //20000 before
    G4cout<<"BesSim :: BesSteppingAction: StepNumber>=30000 !!!"<<G4endl;
    currentTrack->SetTrackStatus(fKillTrackAndSecondaries);
  }
  */
/*     G4cout.precision(15);
     G4cout<<"#Step# "<<currentTrack->GetCurrentStepNumber()<<" pName "<<currentTrack->GetDefinition()->GetParticleName()<<" prex prey prez "<<currentStep->GetPreStepPoint()->GetPosition().x()/mm<<" "<<currentStep->GetPreStepPoint()->GetPosition().y()/mm<<" "<<currentStep->GetPreStepPoint()->GetPosition().z()/mm<<G4endl;
     G4cout<<"prepx prepy prepz "<<currentStep->GetPreStepPoint()->GetMomentum().x()/GeV<<" "<<currentStep->GetPreStepPoint()->GetMomentum().y()/GeV<<" "<<currentStep->GetPreStepPoint()->GetMomentum().z()/GeV<<G4endl;
     G4cout<<"post step postx posty postz "<<postStep->GetPosition().x()/mm<<" "<<postStep->GetPosition().y()/mm<<" "<<postStep->GetPosition().z()/mm<<G4endl;
     G4cout<<"postpx postpy postpz "<<postStep->GetMomentum().x()/GeV<<" "<<postStep->GetMomentum().y()/GeV<<" "<<postStep->GetMomentum().z()/GeV<<G4endl;         
      G4cout << G4endl;
      G4cout << std::setw( 5) << "#Step#"     << " "
             << std::setw( 9) << "pName"      << " "
             << std::setw( 6) << "X"          << "    "
             << std::setw( 6) << "Y"          << "    "
             << std::setw( 6) << "Z"          << "    "
             << std::setw( 9) << "KineE"      << " "
             << std::setw( 9) << "dEStep"     << " "
             << std::setw(10) << "StepLeng"
             << std::setw(10) << "Volume"    << "  "
             << std::setw(10) << "Process"  
             << G4endl;
    

    G4cout << std::setw(5) << currentTrack->GetCurrentStepNumber() << " "
        << std::setw(9) << currentTrack->GetDefinition()->GetParticleName()
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().x(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().y(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetPosition().z(),"Length")
        << std::setw(6) << G4BestUnit(currentTrack->GetKineticEnergy(),"Energy")
        << std::setw(6) << G4BestUnit(currentStep->GetTotalEnergyDeposit(),"Energy")
        << std::setw(6) << G4BestUnit(currentStep->GetStepLength(),"Length");
    if( currentTrack->GetNextVolume() != 0 ) { 
        G4cout << std::setw(10) << currentTrack->GetVolume()->GetName();
     } else {
       G4cout << std::setw(10) << "OutOfWorld";
     }

    if(currentStep->GetPostStepPoint()->GetProcessDefinedStep() != NULL){
      G4cout << "  "
        << std::setw(10) << currentStep->GetPostStepPoint()->GetProcessDefinedStep()
                                        ->GetProcessName();
    } else {
      G4cout << "   UserLimit";
    }

    G4cout << G4endl;
   HepRandom::showEngineStatus();
*/
}



