//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oreiented Simulation Tool                    //
//---------------------------------------------------------------------------//
//Descpirtion: BES beam pipe 
//Author: Liuhm
//Created: May 21, 2003
//Comment:
//---------------------------------------------------------------------------//
//
#include "BesTwoBeamPipe.hh"

#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4VisAttributes.hh"
#include "G4Color.hh"
#include "G4Cons.hh"
#include "G4Trd.hh"
#include "G4RotationMatrix.hh"
#include "G4SubtractionSolid.hh"
#include "G4EllipticalTube.hh"
#include "G4IntersectionSolid.hh"
#include "G4ExtrudedSolid.hh"

BesTwoBeamPipe::BesTwoBeamPipe()
{

  logicalTwoBeamPipe = 0;
  physicalTwoBeamPipe1 = 0;
  physicalTwoBeamPipe2 = 0;


  Au = 0;
  Ag = 0;
  Oil = 0;
}

BesTwoBeamPipe::~BesTwoBeamPipe()
{
	delete logicalTwoBeamPipe;
	delete physicalTwoBeamPipe1;
	delete physicalTwoBeamPipe2;
	
	delete Au;
	delete Ag;
	delete Oil;
}

void BesTwoBeamPipe::DefineMaterial()
{
  G4double density, a, z,fractionmass;
  G4int nel,natoms;
  G4String name, symbol;

  density=19.32*g/cm3;
  a = 196.967*g/mole;
  Au= new G4Material("Gold",79,a,density);

  density=10.5*g/cm3;
  a = 107.9*g/mole;
  Ag= new G4Material("Silver",47,a,density);

  a = 1.01*g/mole;
  G4Element* H  = new G4Element(name="Hydrogen",symbol="H" , z= 1., a);
    
  a = 12.01*g/mole;
  G4Element* C  = new G4Element(name="Carbon"  ,symbol="C" , z= 6., a);

  density = 0.810*g/cm3;
  Oil = new G4Material(name="Oil",density,nel=2);
  Oil->AddElement(C, natoms=18);
  Oil->AddElement(H, natoms=38);

//   G4NistManager* man = G4NistManager::Instance();
//   man->SetVerbose(1);
//   G4Material* Iron = man->FindOrBuildMaterial("G4_Fe");
  
  G4Material* Iron = new G4Material("Iron",      z=26., a=55.845*g/mole,   density=7.87*g/cm3);

  G4double pressure;

a = 14.01*g/mole;
    G4Element* N  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a);
    
    a = 16.00*g/mole;
    G4Element* O  = new G4Element(name="Oxygen"  ,symbol="O" , z= 8., a);
    
    density = 8.96*g/cm3;
    a = 63.546*g/mole;
    G4Material* Copper = new G4Material(name="Copper",z=29.0,a,density);


  density = 1.290*mg/cm3;
    G4Material* Air = new G4Material(name="Air",density,nel=2);
    Air->AddElement(N, fractionmass = 0.7);
    Air->AddElement(O, fractionmass = 0.3);



      density     = 1.0e-5*g/cm3;
    pressure    = 2.e-2*bar;
    G4Material* Beam = new G4Material(name="Beam",density,nel=1,kStateGas,STP_Temperature,pressure);
    Beam->AddMaterial(Air,fractionmass=1.0);


density = 1.848*g/cm3;
    a = 9.012182*g/mole;
    G4Material* Beryllium = new G4Material(name="Beryllium",z=4.0,a,density);

    density = 2.70*g/cm3;
    a = 26.98*g/mole;
    G4Material* Aluminium = new G4Material(name="Aluminium",z=13.,a,density);
    
	 	density=19.25*g/cm3;
  	a = 183.84*g/mole;
  	Tungsten= new G4Material("Tungsten",74,a,density);

    G4NistManager* man = G4NistManager::Instance();
	man->SetVerbose(1);
    G4Element* Si = man->FindOrBuildElement("Si");
	G4Element* Cr = man->FindOrBuildElement("Cr");
	G4Element* Mn = man->FindOrBuildElement("Mn");
	G4Element* Fe = man->FindOrBuildElement("Fe");
	G4Element* Ni = man->FindOrBuildElement("Ni");
    
    G4int ncomponents;
    G4Material* StainlessSteel = new G4Material("StainlessSteel", density= 8.06*g/cm3, ncomponents=6);
	StainlessSteel->AddElement(C, fractionmass=0.001);
	StainlessSteel->AddElement(Si, fractionmass=0.007);
	StainlessSteel->AddElement(Cr, fractionmass=0.18);
	StainlessSteel->AddElement(Mn, fractionmass=0.01);
	StainlessSteel->AddElement(Fe, fractionmass=0.712);
	StainlessSteel->AddElement(Ni, fractionmass=0.09);


}

void BesTwoBeamPipe::Construct(G4LogicalVolume* logicalbes)
{
  DefineMaterial();

  G4RotationMatrix* zRot_90 = new G4RotationMatrix;  zRot_90->rotateZ(3.14159265/2.);
  G4RotationMatrix* zRot_180 = new G4RotationMatrix;  zRot_180->rotateZ(3.14159265);
  G4RotationMatrix* zRot_270 = new G4RotationMatrix;  zRot_270->rotateZ(3.14159265/2.*3);
  
  
  G4RotationMatrix* yRot_180 = new G4RotationMatrix; yRot_180->rotateY(M_PI);
  G4RotationMatrix* yRot_90 = new G4RotationMatrix; yRot_90->rotateY(M_PI/2.);
  G4RotationMatrix* yRot_270 = new G4RotationMatrix; yRot_270->rotateY(3.*M_PI/2.);
  
  G4RotationMatrix* xRot_180 = new G4RotationMatrix; xRot_180->rotateX(M_PI);
  G4RotationMatrix* xRot_90 = new G4RotationMatrix; xRot_90->rotateX(M_PI/2.);
  G4RotationMatrix* xRot_270 = new G4RotationMatrix; xRot_270->rotateX(3.*M_PI/2.);
  
  G4RotationMatrix* xyRot_180 = new G4RotationMatrix; xyRot_180->rotateX(M_PI); xyRot_180->rotateY(M_PI);
  

   //G4RotationMatrix* xRot = new G4RotationMatrix;
  //xRot->rotateX(90*deg);

  //the logical volume of beam pipe

  //length: 1150mm;
  
  
  G4bool cZDDdummy = false;
  
  G4Box* solidTwoBeamPipe;
  if (cZDDdummy) {
      solidTwoBeamPipe =  new G4Box("solidPip",300./2.,300/2.,1500./2.);
  }else{
      solidTwoBeamPipe =  new G4Box("solidPip",300./2.,300/2.,1150./2.);
  }
  
  
  logicalTwoBeamPipe = new G4LogicalVolume(solidTwoBeamPipe, G4Material::GetMaterial("Beam"),"logicalTwoBeamPipe");
  new G4PVPlacement(0,G4ThreeVector(0, 0, 2200. + 1150./2.),logicalTwoBeamPipe,"physicalTwoBeamPipe",logicalbes,false,0);
  new G4PVPlacement(xRot_180,G4ThreeVector(0, 0, -2200. - 1150./2.),logicalTwoBeamPipe,"physicalTwoBeamPipe1",logicalbes,false,0);

  
  
  G4RotationMatrix* zyRot = new G4RotationMatrix;
  zyRot->rotateZ(3.14159265/2.);
  zyRot->rotateY(3.14159265);

// 
  logicalTwoBeamPipe->SetVisAttributes(G4VisAttributes::Invisible);

  
  
  
  G4VisAttributes* visIron = new G4VisAttributes(G4Colour(0.3, 0.3, 0.3));
  G4VisAttributes* visCopper = new G4VisAttributes(G4Colour(146./255, 83./255, 48./255));
  G4VisAttributes* visSteel = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  G4VisAttributes* viscZDD = new G4VisAttributes(G4Colour(0.1, 0.1, 0.5));
  
  G4double angle_incoming = atan(3.6/600.);
  G4double angle_incoming2 = atan(3.6/600.);
  G4double angle_outgoing = 0.0208818;
  G4double angle_outgoing_prewindowcut = -0.0794344;
  G4double angle_window = 0.17975;
  G4double angle_afterwindow = 0.0488933;
  G4double angle_outgoing_windowcut = 0.0654286;
  G4double angle_outgoing2 = atan(32.1/600.);
  
  G4RotationMatrix* yRot_incoming = new G4RotationMatrix(); yRot_incoming->rotateY(angle_incoming);
  G4RotationMatrix* yRot_incoming_neg = new G4RotationMatrix(); yRot_incoming_neg->rotateY(-angle_incoming);
  G4RotationMatrix* yRot_outgoing = new G4RotationMatrix(); yRot_outgoing->rotateY(-angle_outgoing);
  G4RotationMatrix* yRot_window = new G4RotationMatrix(); yRot_window->rotateY(-angle_window);
  G4RotationMatrix* yRot_afterwindow = new G4RotationMatrix(); yRot_afterwindow->rotateY(-angle_afterwindow);
  G4RotationMatrix* yRot_outgoing_neg = new G4RotationMatrix(); yRot_outgoing_neg->rotateY(angle_outgoing);
  G4RotationMatrix* yRot_outgoing2 = new G4RotationMatrix(); yRot_outgoing2->rotateY(-angle_outgoing2);
  G4RotationMatrix* yRot_outgoing2_neg = new G4RotationMatrix(); yRot_outgoing2_neg->rotateY(angle_outgoing2);
  
  
  G4RotationMatrix* Rot_outgoing_cutaway_far = new G4RotationMatrix();
  Rot_outgoing_cutaway_far->rotateY(angle_outgoing);
  Rot_outgoing_cutaway_far->rotateX(-M_PI/2.);
  
  G4RotationMatrix* Rot_outgoing_cutaway_near = new G4RotationMatrix();
  Rot_outgoing_cutaway_near->rotateY(angle_outgoing);
  Rot_outgoing_cutaway_near->rotateX(M_PI/2.);
  
  G4RotationMatrix* Rot_outgoing_cutaway_prewindow_far = new G4RotationMatrix();
  Rot_outgoing_cutaway_prewindow_far->rotateY(angle_outgoing_prewindowcut);
  Rot_outgoing_cutaway_prewindow_far->rotateX(-M_PI/2.);
  
  G4RotationMatrix* Rot_outgoing_cutaway_window_far = new G4RotationMatrix();
  Rot_outgoing_cutaway_window_far->rotateY(angle_outgoing_windowcut);
  Rot_outgoing_cutaway_window_far->rotateX(-M_PI/2.);
  
  G4RotationMatrix* Rot_outgoing_cutaway_window_near = new G4RotationMatrix();
  Rot_outgoing_cutaway_window_near->rotateY(-angle_outgoing_prewindowcut);
  Rot_outgoing_cutaway_window_near->rotateX(M_PI/2.);
  
  G4RotationMatrix* Rot_outgoing_cutaway_afterwindow_near = new G4RotationMatrix();
  Rot_outgoing_cutaway_afterwindow_near->rotateY(-angle_outgoing_windowcut);
  Rot_outgoing_cutaway_afterwindow_near->rotateX(M_PI/2.);
  
  G4RotationMatrix* Rot_outgoing_cutaway_afterwindow_far = new G4RotationMatrix();
  Rot_outgoing_cutaway_afterwindow_far->rotateY(angle_afterwindow);
  Rot_outgoing_cutaway_afterwindow_far->rotateX(-M_PI/2.);
  
  
  

  G4bool checkoverlap = false;
//G4bool checkoverlap = true;
  
  // for debug:
  G4bool endplate = true;
  G4bool septum = true;
  G4bool incoming = false;
  G4bool outgoing = true;
  
  
  
  // FLANGE:
  // OUTER:
  
  const G4double endplate_anchor_z = -1150./2.;
    if (endplate) {
  // ENDPLATE
  // DRILL HOLE
  G4Tubs* solid_endplate_drillhole = new G4Tubs("solid_endplate_drillhole", 0, 4, 10, 0, 2*M_PI);
  
  
  
  
  
  // ENDPLATE TWO HOLES
  const G4double endplate_twohole_anchor_z = endplate_anchor_z +3.75;
  // SQUARE PART
  G4Box* solid_endplate_twoholes_box = new G4Box("solid_endplate_twoholes_box", 13, 7, 3.75);
  G4SubtractionSolid* solid_endplate_twoholes_square = new G4SubtractionSolid("solid_endplate_twoholes_square", solid_endplate_twoholes_box, solid_endplate_drillhole, 0, G4ThreeVector(-3, 0, 0));
  
  G4LogicalVolume* logical_endplate_twoholes_square = new G4LogicalVolume(solid_endplate_twoholes_square, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_square");
  logical_endplate_twoholes_square->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_twoholes_square1 = new G4PVPlacement(0, G4ThreeVector(-87, 0, endplate_twohole_anchor_z), logical_endplate_twoholes_square, "phycical_endplate_twoholes_square1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_square2 = new G4PVPlacement(zRot_180, G4ThreeVector(87, 0, endplate_twohole_anchor_z), logical_endplate_twoholes_square, "phycical_endplate_twoholes_square2", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // ARC PART
  G4Tubs* solid_endplate_twoholes_tubs = new G4Tubs("solid_endplate_twoholes_tubs", 30, 56, 3.75, M_PI/2.+0.0001, M_PI/2.-0.0002);
  G4SubtractionSolid* solid_endplate_twoholes_arc = new G4SubtractionSolid("solid_endplate_twoholes_arc", solid_endplate_twoholes_tubs, solid_endplate_drillhole, 0, G4ThreeVector(-sin(12.5/180.*M_PI) *46, cos(12.5/180.*M_PI) *46, 0));
  solid_endplate_twoholes_arc = new G4SubtractionSolid("solid_endplate_twoholes_arc", solid_endplate_twoholes_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(42.5/180.*M_PI) *46, cos(42.5/180.*M_PI) *46, 0));
  solid_endplate_twoholes_arc = new G4SubtractionSolid("solid_endplate_twoholes_arc", solid_endplate_twoholes_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(72.5/180.*M_PI) *46, cos(72.5/180.*M_PI) *46, 0));
  
  G4LogicalVolume* logical_endplate_twoholes_arc = new G4LogicalVolume(solid_endplate_twoholes_arc, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_arc");
  logical_endplate_twoholes_arc->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_twoholes_arc1 = new G4PVPlacement(0, G4ThreeVector(-44., 7., endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_arc2 = new G4PVPlacement(xRot_180, G4ThreeVector(-44., -7., endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc2", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_arc3 = new G4PVPlacement(yRot_180, G4ThreeVector(44., 7., endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc3", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_arc4 = new G4PVPlacement(xyRot_180, G4ThreeVector(44., -7., endplate_twohole_anchor_z), logical_endplate_twoholes_arc, "phycical_endplate_twoholes_arc4", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // TSHAPE PART
  G4Box* solid_endplate_twoholes_topbox = new G4Box("solid_endplate_twoholes_topbox", 42, 28, 3.75);
  G4Tubs* solid_endplate_twoholes_subarc = new G4Tubs("solid_endplate_twoholes_subarc", 0, 30, 10, 0., 2*M_PI);
  G4SubtractionSolid* solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_topbox, solid_endplate_twoholes_subarc, 0,  G4ThreeVector(42, -28, 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_twoholes_subarc, 0,  G4ThreeVector(-42, -28, 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42 + 46*sin(12.5/180.*M_PI), -28 + 46*cos(12.5/180.*M_PI), 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42 + 46*sin(37.5/180.*M_PI), -28 + 46*cos(37.5/180.*M_PI), 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42 - 46*sin(12.5/180.*M_PI), -28 + 46*cos(12.5/180.*M_PI), 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42 - 46*sin(37.5/180.*M_PI), -28 + 46*cos(37.5/180.*M_PI), 0));
  solid_endplate_twoholes_tshape = new G4SubtractionSolid("solid_endplate_twoholes_tshape", solid_endplate_twoholes_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(0., -5, 0));
  
  G4LogicalVolume* logical_endplate_twoholes_tshape = new G4LogicalVolume(solid_endplate_twoholes_tshape, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_tshape");
  logical_endplate_twoholes_tshape->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *phycical_endplate_twoholes_tshape1 = new G4PVPlacement(0, G4ThreeVector(0, 35, endplate_twohole_anchor_z), logical_endplate_twoholes_tshape, "phycical_endplate_twoholes_tshape1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *phycical_endplate_twoholes_tshape2 = new G4PVPlacement(xRot_180, G4ThreeVector(0, -35, endplate_twohole_anchor_z), logical_endplate_twoholes_tshape, "phycical_endplate_twoholes_tshape2", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // FILLBOX
  G4Box* solid_endplate_twoholes_fillbox = new G4Box("solid_endplate_twoholes_fillbox", 1, 13, 3.75);
  
  G4LogicalVolume* logical_endplate_twoholes_fillbox = new G4LogicalVolume(solid_endplate_twoholes_fillbox, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_fillbox");
  logical_endplate_twoholes_fillbox->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox1 = new G4PVPlacement(0, G4ThreeVector(-43, 50, endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox1", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox2 = new G4PVPlacement(0, G4ThreeVector(43, 50, endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox2", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox3 = new G4PVPlacement(0, G4ThreeVector(43, -50, endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox3", logicalTwoBeamPipe, false, 0, checkoverlap);
  G4VPhysicalVolume *physical_endplate_twoholes_fillbox4 = new G4PVPlacement(0, G4ThreeVector(-43, -50, endplate_twohole_anchor_z), logical_endplate_twoholes_fillbox, "physical_endplate_twoholes_fillbox4", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  // CENTERBOX
  G4Box* solid_endplate_twoholes_centerbox = new G4Box("solid_endplate_twoholes_centerbox", 12, 7, 3.75);
  
  G4LogicalVolume* logical_endplate_twoholes_centerbox = new G4LogicalVolume(solid_endplate_twoholes_centerbox, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_twoholes_centerbox");
  logical_endplate_twoholes_centerbox->SetVisAttributes(visSteel);
  
  G4VPhysicalVolume *physical_endplate_twoholes_centerbox = new G4PVPlacement(0, G4ThreeVector(0, 0, endplate_twohole_anchor_z), logical_endplate_twoholes_centerbox, "physical_endplate_twoholes_centerbox", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  
//   // ENDPLATE ONE HOLE
//   const G4double endplate_onehole_anchor_z = endplate_anchor_z - 11.25;
//   // SQUARE PART
//   G4Box* solid_endplate_onehole_box = new G4Box("solid_endplate_onehole_box", 11.5, 7, 3.75);
//   G4SubtractionSolid* solid_endplate_onehole_square = new G4SubtractionSolid("solid_endplate_onehole_square", solid_endplate_onehole_box, solid_endplate_drillhole, 0, G4ThreeVector(-1.5, 0, 0));
//   
//   G4LogicalVolume* logical_endplate_onehole_square = new G4LogicalVolume(solid_endplate_onehole_square, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_onehole_square");
//   logical_endplate_onehole_square->SetVisAttributes(visSteel);
//   
//   G4VPhysicalVolume *phycical_endplate_onehole_square1 = new G4PVPlacement(0, G4ThreeVector(-88.5, 0, endplate_onehole_anchor_z), logical_endplate_onehole_square, "phycical_endplate_onehole_square1", logicalTwoBeamPipe, false, 0, checkoverlap);
//   G4VPhysicalVolume *phycical_endplate_onehole_square2 = new G4PVPlacement(zRot_180, G4ThreeVector(88.5, 0, endplate_onehole_anchor_z), logical_endplate_onehole_square, "phycical_endplate_onehole_square2", logicalTwoBeamPipe, false, 0, checkoverlap);
//   
//   // ARC PART
//   G4Tubs* solid_endplate_onehole_tubs = new G4Tubs("solid_endplate_onehole_tubs", 33, 56, 3.75, M_PI/2., M_PI/2.);
//   G4SubtractionSolid* solid_endplate_onehole_arc = new G4SubtractionSolid("solid_endplate_onehole_arc", solid_endplate_onehole_tubs, solid_endplate_drillhole, 0, G4ThreeVector(-sin(12.5/180.*M_PI) *46, cos(12.5/180.*M_PI) *46, 0));
//   solid_endplate_onehole_arc = new G4SubtractionSolid("solid_endplate_onehole_arc", solid_endplate_onehole_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(42.5/180.*M_PI) *46, cos(42.5/180.*M_PI) *46, 0));
//   solid_endplate_onehole_arc = new G4SubtractionSolid("solid_endplate_onehole_arc", solid_endplate_onehole_arc, solid_endplate_drillhole, 0, G4ThreeVector(-sin(72.5/180.*M_PI) *46, cos(72.5/180.*M_PI) *46, 0));
//   
//   G4LogicalVolume* logical_endplate_onehole_arc = new G4LogicalVolume(solid_endplate_onehole_arc, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_onehole_arc");
//   logical_endplate_onehole_arc->SetVisAttributes(visSteel);
//   
//   G4VPhysicalVolume *phycical_endplate_onehole_arc1 = new G4PVPlacement(0, G4ThreeVector(-44, 7, endplate_onehole_anchor_z), logical_endplate_onehole_arc, "phycical_endplate_onehole_arc1", logicalTwoBeamPipe, false, 0, checkoverlap);
//   G4VPhysicalVolume *phycical_endplate_onehole_arc2 = new G4PVPlacement(xRot_180, G4ThreeVector(-44, -7, endplate_onehole_anchor_z), logical_endplate_onehole_arc, "phycical_endplate_onehole_arc2", logicalTwoBeamPipe, false, 0, checkoverlap);
//   G4VPhysicalVolume *phycical_endplate_onehole_arc3 = new G4PVPlacement(yRot_180, G4ThreeVector(44, 7, endplate_onehole_anchor_z), logical_endplate_onehole_arc, "phycical_endplate_onehole_arc3", logicalTwoBeamPipe, false, 0, checkoverlap);
//   G4VPhysicalVolume *phycical_endplate_onehole_arc4 = new G4PVPlacement(xyRot_180, G4ThreeVector(44, -7, endplate_onehole_anchor_z), logical_endplate_onehole_arc, "phycical_endplate_onehole_arc4", logicalTwoBeamPipe, false, 0, checkoverlap);
//   
//   // TSHAPE PART
//   G4Box* solid_endplate_onehole_topbox = new G4Box("solid_endplate_onehole_topbox", 42, 21.5, 3.75);
//   G4Tubs* solid_endplate_onehole_subarc = new G4Tubs("solid_endplate_onehole_subarc", 0, 33, 10, 0., M_PI);
//   G4SubtractionSolid* solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_topbox, solid_endplate_onehole_subarc, 0,  G4ThreeVector(42, -34.5, 0));
//   solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_onehole_subarc, 0,  G4ThreeVector(-42, -34.5, 0));
//   solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42 + 46*sin(12.5/180.*M_PI), -34.5 + 46*cos(12.5/180.*M_PI), 0));
//   solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(-42 + 46*sin(37.5/180.*M_PI), -34.5 + 46*cos(37.5/180.*M_PI), 0));
//   solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42 - 46*sin(12.5/180.*M_PI), -34.5 + 46*cos(12.5/180.*M_PI), 0));
//   solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(42 - 46*sin(37.5/180.*M_PI), -34.5 + 46*cos(37.5/180.*M_PI), 0));
//   solid_endplate_onehole_tshape = new G4SubtractionSolid("solid_endplate_onehole_tshape", solid_endplate_onehole_tshape, solid_endplate_drillhole, 0,  G4ThreeVector(0., -11.5, 0));
//   
//   G4LogicalVolume* logical_endplate_onehole_tshape = new G4LogicalVolume(solid_endplate_onehole_tshape, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_onehole_tshape");
//   logical_endplate_onehole_tshape->SetVisAttributes(visSteel);
//   
//   G4VPhysicalVolume *phycical_endplate_onehole_tshape1 = new G4PVPlacement(0, G4ThreeVector(0, 41.5, endplate_onehole_anchor_z), logical_endplate_onehole_tshape, "phycical_endplate_onehole_tshape1", logicalTwoBeamPipe, false, 0, checkoverlap);
//   G4VPhysicalVolume *phycical_endplate_onehole_tshape2 = new G4PVPlacement(xRot_180, G4ThreeVector(0, -41.5, endplate_onehole_anchor_z), logical_endplate_onehole_tshape, "phycical_endplate_onehole_tshape2", logicalTwoBeamPipe, false, 0, checkoverlap);
//   
//   // FILLBOX
//   G4Box* solid_endplate_onehole_fillbox = new G4Box("solid_endplate_onehole_fillbox", 1, 11.5, 3.75);
//   
//   G4LogicalVolume* logical_endplate_onehole_fillbox = new G4LogicalVolume(solid_endplate_onehole_fillbox, G4Material::GetMaterial("StainlessSteel"),"logical_endplate_onehole_fillbox");
//   logical_endplate_onehole_fillbox->SetVisAttributes(visSteel);
//   
//   G4VPhysicalVolume *physical_endplate_onehole_fillbox1 = new G4PVPlacement(0, G4ThreeVector(-43, 51.5, endplate_onehole_anchor_z), logical_endplate_onehole_fillbox, "physical_endplate_onehole_fillbox1", logicalTwoBeamPipe, false, 0, checkoverlap);
//   G4VPhysicalVolume *physical_endplate_onehole_fillbox2 = new G4PVPlacement(0, G4ThreeVector(43, 51.5, endplate_onehole_anchor_z), logical_endplate_onehole_fillbox, "physical_endplate_onehole_fillbox2", logicalTwoBeamPipe, false, 0, checkoverlap);
//   G4VPhysicalVolume *physical_endplate_onehole_fillbox3 = new G4PVPlacement(0, G4ThreeVector(43, -51.5, endplate_onehole_anchor_z), logical_endplate_onehole_fillbox, "physical_endplate_onehole_fillbox3", logicalTwoBeamPipe, false, 0, checkoverlap);
//   G4VPhysicalVolume *physical_endplate_onehole_fillbox4 = new G4PVPlacement(0, G4ThreeVector(-43, -51.5, endplate_onehole_anchor_z), logical_endplate_onehole_fillbox, "physical_endplate_onehole_fillbox4", logicalTwoBeamPipe, false, 0, checkoverlap);
  
  }
  
  
  
  
  // SEPTUM IN MIDDLE
  // box
  
  if (septum) {
    G4Box* solid_septum = new G4Box("solid_septum", 8.5, 41., 300.);
  
    G4LogicalVolume* logical_septum = new G4LogicalVolume(solid_septum, G4Material::GetMaterial("Iron"),"logical_septum");
    logical_septum->SetVisAttributes(visIron);
    
    G4VPhysicalVolume *physical_septum = new G4PVPlacement(0, G4ThreeVector(0., 0., endplate_anchor_z + 400.), logical_septum, "physical_septum", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    // triangle
    std::vector<G4TwoVector> triangle;
    triangle.push_back(G4TwoVector(0., 10.));
    triangle.push_back(G4TwoVector(-12.6, 41.));
    triangle.push_back(G4TwoVector(0., 41.));
    G4ExtrudedSolid* solid_triangle = new G4ExtrudedSolid("solid_triangle", triangle, 300., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    G4LogicalVolume* logical_triangle = new G4LogicalVolume(solid_triangle, G4Material::GetMaterial("Iron"),"logical_triangle");
    logical_triangle->SetVisAttributes(visIron);
    
    G4VPhysicalVolume *physical_triangle1 = new G4PVPlacement(0, G4ThreeVector(-8.5, 0., endplate_anchor_z + 400.), logical_triangle, "physical_triangle1", logicalTwoBeamPipe, false, 0, checkoverlap);
    G4VPhysicalVolume *physical_triangle2 = new G4PVPlacement(xRot_180, G4ThreeVector(-8.5, 0., endplate_anchor_z + 400.), logical_triangle, "physical_triangle2", logicalTwoBeamPipe, false, 0, checkoverlap);
  }
  
  
  if (incoming) {
    // INCOMING
    G4Box* solid_incoming_innerbox_sub = new G4Box("solid_incoming_innerbox_sub", 1.25/cos(angle_incoming), 10., 320.);
    G4Box* solid_incoming_subbox = new G4Box("solid_incoming_subbox", 200., 200., 100.);
    G4SubtractionSolid* solid_incoming_innerbox = new G4SubtractionSolid("solid_incoming_innerbox", solid_incoming_innerbox_sub, solid_incoming_subbox, yRot_incoming, G4ThreeVector(400.*sin(angle_incoming), 0., 400.*cos(angle_incoming)));
    solid_incoming_innerbox = new G4SubtractionSolid("solid_incoming_innerbox", solid_incoming_innerbox, solid_incoming_subbox, yRot_incoming, G4ThreeVector(-400.*sin(angle_incoming), 0., -400.*cos(angle_incoming)));
    
    G4LogicalVolume* logical_incoming_innerbox = new G4LogicalVolume(solid_incoming_innerbox, G4Material::GetMaterial("Copper"),"logical_incoming_innerbox");
    logical_incoming_innerbox->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_incoming_innerbox = new G4PVPlacement(yRot_incoming, G4ThreeVector(-8.5-1.8-1.25/cos(angle_incoming), 0., -100.), logical_incoming_innerbox, "physical_septum", logicalTwoBeamPipe, false, 0, checkoverlap);

}
  
  
    // OUTGOING
  if (outgoing) {
      
    // cut-away-box:
    std::vector<G4TwoVector> cutawayface;
    cutawayface.push_back(G4TwoVector(-50., 0.));
    cutawayface.push_back(G4TwoVector(-50., 50.));
    cutawayface.push_back(G4TwoVector(50., 50.));
    cutawayface.push_back(G4TwoVector(50., 0.));
    G4ExtrudedSolid* solid_cutaway = new G4ExtrudedSolid("solid_cutaway", cutawayface, 50., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
      
    // top part
    std::vector<G4TwoVector> polygon;
    //  {1., 7.5}, {44.781, 790.893}, {47.221, 834.553}, {75.2464, 1150.}, {40.7595, 1150.}, {28.711, 903.773}, {16.291, 835.423}, {15.361, 790.893}, {-1., 7.5} 
    polygon.push_back(G4TwoVector(-1., 7.5));  
    polygon.push_back(G4TwoVector(15.361, 790.893));  
    polygon.push_back(G4TwoVector(16.291, 835.423));
    polygon.push_back(G4TwoVector(28.711, 903.773));
    polygon.push_back(G4TwoVector(40.7595, 1150.));
    polygon.push_back(G4TwoVector(75.2464, 1150.));
    polygon.push_back(G4TwoVector(47.221, 834.553));
    polygon.push_back(G4TwoVector(44.781, 790.893));
    polygon.push_back(G4TwoVector(1., 7.5));
    G4ExtrudedSolid* solid_outgoing_top = new G4ExtrudedSolid("solid_outgoing_top", polygon, 2., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);

    G4LogicalVolume* logical_outgoing_top = new G4LogicalVolume(solid_outgoing_top, G4Material::GetMaterial("Copper"),"logical_outgoing_top");
    logical_outgoing_top->SetVisAttributes(visCopper);

    G4VPhysicalVolume *physical_outgoing_top = new G4PVPlacement(xRot_270, G4ThreeVector(43.+0.0001, 39., endplate_anchor_z), logical_outgoing_top, "physical_outgoing_top", logicalTwoBeamPipe, false, 0, checkoverlap);
    G4VPhysicalVolume *physical_outgoing_bottom = new G4PVPlacement(xRot_270, G4ThreeVector(43.+0.0001, -39., endplate_anchor_z), logical_outgoing_top, "physical_outgoing_bottom", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    // ISPB
    
    std::vector<G4TwoVector> polygon_pipe;
    polygon_pipe.push_back(G4TwoVector(0., -41.));
    polygon_pipe.push_back(G4TwoVector(-12.5, -41.));
    polygon_pipe.push_back(G4TwoVector(-32.5, -21.));
    polygon_pipe.push_back(G4TwoVector(-32.5, 21.));
    polygon_pipe.push_back(G4TwoVector(-12.5, 41.));
    polygon_pipe.push_back(G4TwoVector(0., 41.));
    G4ExtrudedSolid* solid_ispb_extrusion = new G4ExtrudedSolid("solid_ispb_extrusion", polygon_pipe, 400., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    G4Tubs* solid_ispb_edge = new G4Tubs("solid_ispb_edge", 0., 20., 400., M_PI/2., M_PI/2.);
    
    G4UnionSolid* solid_ispb_union = new G4UnionSolid("solid_ispb_union", solid_ispb_extrusion, solid_ispb_edge, 0, G4ThreeVector(-12.5, 21., 0.));
    solid_ispb_union = new G4UnionSolid("solid_ispb_union", solid_ispb_union, solid_ispb_edge, zRot_270, G4ThreeVector(-12.5, -21., 0.));
    
    G4Tubs* solid_ispb_cuttube = new G4Tubs("solid_ispb_cuttube", 0., 30., 420., 0., 2.*M_PI);
    
    G4SubtractionSolid* solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb_union, solid_ispb_cuttube, 0, G4ThreeVector(0., 7., 0.));
    solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb, solid_ispb_cuttube, 0, G4ThreeVector(0., -7., 0.));
    
    G4Box* solid_ispb_cutbox = new G4Box("solid_ispb_cutbox", 30., 7., 400.);
    
    solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb, solid_ispb_cutbox, 0, G4ThreeVector(0., 0., 0.));
    
    solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb, solid_cutaway, Rot_outgoing_cutaway_far, G4ThreeVector(0., 0., 783.564/2.-0.001));
    solid_ispb = new G4SubtractionSolid("solid_ispb", solid_ispb, solid_cutaway, Rot_outgoing_cutaway_near, G4ThreeVector(0., 0., -783.564/2.+0.001));
    
    G4LogicalVolume* logical_ispb = new G4LogicalVolume(solid_ispb, G4Material::GetMaterial("Copper"),"logical_ispb");
    logical_ispb->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_ispb = new G4PVPlacement(yRot_outgoing, G4ThreeVector(50.1805, 0., endplate_anchor_z + 399.196), logical_ispb, "physical_ispb", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    
    
    // PRE WINDOW
    std::vector<G4TwoVector> polygon_prewindow;
    polygon_prewindow.push_back(G4TwoVector(0., -41.));
    polygon_prewindow.push_back(G4TwoVector(-16., -41.));
    polygon_prewindow.push_back(G4TwoVector(-34., -23.));
    polygon_prewindow.push_back(G4TwoVector(-34., 23.));
    polygon_prewindow.push_back(G4TwoVector(-16., 41.));
    polygon_prewindow.push_back(G4TwoVector(0., 41.));
    G4ExtrudedSolid* solid_prewindow_extrusion = new G4ExtrudedSolid("solid_prewindow_extrusion", polygon_prewindow, 30., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    G4Tubs* solid_prewindow_cuttube = new G4Tubs("solid_prewindow_cuttube", 0., 30., 40., 0., 2.*M_PI);
    
    G4SubtractionSolid* solid_prewindow = new G4SubtractionSolid("solid_prewindow", solid_prewindow_extrusion, solid_prewindow_cuttube, 0, G4ThreeVector(0., 7., 0.));
    solid_prewindow = new G4SubtractionSolid("solid_prewindow", solid_prewindow, solid_prewindow_cuttube, 0, G4ThreeVector(0., -7., 0.));
    
    G4Box* solid_prewindow_cutbox = new G4Box("solid_prewindow_cutbox", 30., 7., 40.);
    
    solid_prewindow = new G4SubtractionSolid("solid_prewindow", solid_prewindow, solid_prewindow_cutbox, 0, G4ThreeVector(0., 0., 0.));
    
    solid_prewindow = new G4SubtractionSolid("solid_prewindow", solid_prewindow, solid_cutaway, Rot_outgoing_cutaway_prewindow_far, G4ThreeVector(0., 0., 44.5397/2.));
    solid_prewindow = new G4SubtractionSolid("solid_prewindow", solid_prewindow, solid_cutaway, Rot_outgoing_cutaway_near, G4ThreeVector(0., 0., -44.5397/2.));
    
    G4LogicalVolume* logical_prewindow = new G4LogicalVolume(solid_prewindow, G4Material::GetMaterial("Copper"),"logical_prewindow");
    logical_prewindow->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_prewindow = new G4PVPlacement(yRot_outgoing, G4ThreeVector(58.826, 0., endplate_anchor_z + 813.158), logical_prewindow, "physical_prewindow", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    // WINDOW
    std::vector<G4TwoVector> polygon_window;
    polygon_window.push_back(G4TwoVector(0., -41.));
    polygon_window.push_back(G4TwoVector(-16., -41.));
    polygon_window.push_back(G4TwoVector(-34., -23.));
    polygon_window.push_back(G4TwoVector(-34., 23.));
    polygon_window.push_back(G4TwoVector(-16., 41.));
    polygon_window.push_back(G4TwoVector(0., 41.));
    G4ExtrudedSolid* solid_window_extrusion = new G4ExtrudedSolid("solid_window_extrusion", polygon_window, 40., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    G4Tubs* solid_window_cuttube = new G4Tubs("solid_window_cuttube", 0., 30., 50., 0., 2.*M_PI);
    
    G4SubtractionSolid* solid_window = new G4SubtractionSolid("solid_window", solid_window_extrusion, solid_window_cuttube, 0, G4ThreeVector(0., 7., 0.));
    solid_window = new G4SubtractionSolid("solid_window", solid_window, solid_window_cuttube, 0, G4ThreeVector(0., -7., 0.));
    
    G4Box* solid_window_cutbox = new G4Box("solid_window_cutbox", 30., 7., 50.);
    
    solid_window = new G4SubtractionSolid("solid_window", solid_window, solid_window_cutbox, 0, G4ThreeVector(0., 0., 0.));
    
    solid_window = new G4SubtractionSolid("solid_window", solid_window, solid_cutaway, Rot_outgoing_cutaway_window_far, G4ThreeVector(0., 0., 69.4693/2.-0.0001));
    solid_window = new G4SubtractionSolid("solid_window", solid_window, solid_cutaway, Rot_outgoing_cutaway_window_near, G4ThreeVector(0., 0., -69.4693/2.+0.0001));
    
    G4LogicalVolume* logical_window = new G4LogicalVolume(solid_window, G4Material::GetMaterial("Copper"),"logical_window");
    logical_window->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_window = new G4PVPlacement(yRot_window, G4ThreeVector(65.501, 0., endplate_anchor_z + 869.598), logical_window, "physical_window", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    // DUMMY WINDOW
    G4Box* solid_dummyWindow = new G4Box("solid_dummyWindow", 60., 60., 20.);
    
    G4LogicalVolume* logical_dummyWindow = new G4LogicalVolume(solid_dummyWindow, G4Material::GetMaterial("Copper"),"logical_dummyWindow");
    logical_dummyWindow->SetVisAttributes(visCopper);
    
//     G4VPhysicalVolume *physical_dummyWindow = new G4PVPlacement(0, G4ThreeVector(65.501, 0., endplate_anchor_z + 869.598), logical_dummyWindow, "physical_dummyWindow", logicalTwoBeamPipe, false, 0, checkoverlap);
    
    
    
    
    // AFTER WINDOW
    std::vector<G4TwoVector> polygon_afterwindow;
    polygon_afterwindow.push_back(G4TwoVector(0., -41.));
    polygon_afterwindow.push_back(G4TwoVector(-16., -41.));
    polygon_afterwindow.push_back(G4TwoVector(-34., -23.));
    polygon_afterwindow.push_back(G4TwoVector(-34., 23.));
    polygon_afterwindow.push_back(G4TwoVector(-16., 41.));
    polygon_afterwindow.push_back(G4TwoVector(0., 41.));
    G4ExtrudedSolid* solid_afterwindow_extrusion = new G4ExtrudedSolid("solid_afterwindow_extrusion", polygon_afterwindow, 130., G4TwoVector(0., 0.), 1., G4TwoVector(0., 0.), 1.);
    
    G4Tubs* solid_afterwindow_cuttube = new G4Tubs("solid_afterwindow_cuttube", 0., 30., 140., 0., 2.*M_PI);
    
    G4SubtractionSolid* solid_afterwindow = new G4SubtractionSolid("solid_afterwindow", solid_afterwindow_extrusion, solid_afterwindow_cuttube, 0, G4ThreeVector(0., 7., 0.));
    solid_afterwindow = new G4SubtractionSolid("solid_afterwindow", solid_afterwindow, solid_afterwindow_cuttube, 0, G4ThreeVector(0., -7., 0.));
    
    G4Box* solid_afterwindow_cutbox = new G4Box("solid_afterwindow_cutbox", 30., 7., 140.);
    
    solid_afterwindow = new G4SubtractionSolid("solid_afterwindow", solid_afterwindow, solid_afterwindow_cutbox, 0, G4ThreeVector(0., 0., 0.));
    
    solid_afterwindow = new G4SubtractionSolid("solid_afterwindow", solid_afterwindow, solid_cutaway, Rot_outgoing_cutaway_afterwindow_far, G4ThreeVector(0., 0., 246.522/2.-0.005));
    solid_afterwindow = new G4SubtractionSolid("solid_afterwindow", solid_afterwindow, solid_cutaway, Rot_outgoing_cutaway_afterwindow_near, G4ThreeVector(0., 0., -246.522/2.));
    
    G4LogicalVolume* logical_afterwindow = new G4LogicalVolume(solid_afterwindow, G4Material::GetMaterial("Copper"),"logical_afterwindow");
    logical_afterwindow->SetVisAttributes(visCopper);
    
    G4VPhysicalVolume *physical_afterwindow = new G4PVPlacement(yRot_afterwindow, G4ThreeVector(77.7352, 0., endplate_anchor_z + 1026.89), logical_afterwindow, "physical_afterwindow", logicalTwoBeamPipe, false, 0, checkoverlap);
    

		//Absorber
		/*
		G4Box* solidAbsorber;
		solidAbsorber = new G4Box("solidAbsorber",14.,5.,27.);
//solidAbsorber = new G4Box("solidAbsorber",20.,50.,27.);		
		G4LogicalVolume* logicalAbsorber = new G4LogicalVolume(solidAbsorber, G4Material::GetMaterial("Tungsten"),"logicalAbsorber");
		//new G4PVPlacement(0,G4ThreeVector(2.5,0,endplate_anchor_z + 869.598+40),"physicalAbsorber",logicalbes,false,0);
		//new G4PVPlacement(0,G4ThreeVector(2.5,0,3100),logicalAbsorber,"physicalAbsorber",logicalbes,false,0);
		//new G4PVPlacement(0,G4ThreeVector(-5.,0,3180),logicalAbsorber,"physicalAbsorber",logicalbes,false,0);
G4VPhysicalVolume *physicalAbsorber =new G4PVPlacement(0,G4ThreeVector(25.,0,endplate_anchor_z + 869.598+40.+27.),logicalAbsorber,"physicalAbsorber",logicalTwoBeamPipe,false,0);
//G4VPhysicalVolume *physicalAbsorber =new G4PVPlacement(0,G4ThreeVector(20.,0,endplate_anchor_z + 869.598+40.+27.),logicalAbsorber,"physicalAbsorber",logicalTwoBeamPipe,false,0); 	
	*/

G4Box* solidAbsorberBox; 
solidAbsorberBox = new G4Box("solidAbsorber",14.,5.,27.);		  
//G4SubtractionSolid* solidAbsorber = new G4SubtractionSolid("solidAbsorber", solidAbsorberBox,  solid_afterwindow, 0, G4ThreeVector(0., 0., 0.));
//G4SubtractionSolid* solidAbsorber = new G4SubtractionSolid("solidAbsorber", solidAbsorberBox,  solid_afterwindow_extrusion, 0, G4ThreeVector(-10., 0., 0.));

//G4UnionSolid* solidAbsorber = new G4UnionSolid("solidAbsorber", solidAbsorberBox,  solid_afterwindow, yRot_afterwindow, G4ThreeVector(34.+14.*2-12., 0., 130.-27*2+16.));

G4SubtractionSolid* solidAbsorber = new G4SubtractionSolid("solidAbsorber", solidAbsorberBox,  solid_afterwindow, yRot_afterwindow, G4ThreeVector(34.+14.*2-12., 0., 130.-27*2+16.));

G4LogicalVolume* logicalAbsorber = new G4LogicalVolume(solidAbsorber, G4Material::GetMaterial("Tungsten"),"logicalAbsorber");
new G4PVPlacement(0,G4ThreeVector(27.,0,endplate_anchor_z + 1026.89-130.+27.+10.),logicalAbsorber,"physicalAbsorber",logicalTwoBeamPipe,false,0);

    // DUMMY cZDD
    if (cZDDdummy) {
	
	G4Box* solid_cZDD = new G4Box("solid_cZDD", 20., 15., 70.);
	
	G4LogicalVolume* logical_cZDD = new G4LogicalVolume(solid_cZDD, G4Material::GetMaterial("Copper"), "logical_cZDD");
	logical_cZDD->SetVisAttributes(viscZDD);
	
	G4VPhysicalVolume *physical_cZDD1 = new G4PVPlacement(0, G4ThreeVector(20., 20., endplate_anchor_z + 1150.+70.), logical_cZDD, "physical_cZDD1", logicalTwoBeamPipe, false, 0, checkoverlap);
	G4VPhysicalVolume *physical_cZDD2 = new G4PVPlacement(0, G4ThreeVector(20., -20., endplate_anchor_z + 1150.+70.), logical_cZDD, "physical_cZDD2", logicalTwoBeamPipe, false, 0, checkoverlap);
	
    }


    
}

}
