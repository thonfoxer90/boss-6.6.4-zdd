//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oreiented Simulation Tool                    //
//---------------------------------------------------------------------------//
//Descpirtion: BES beam pipe 
//Author: Liuhm
//Created: May 21, 2003
//Comment:
//---------------------------------------------------------------------------//
//
#include "BesYtypeCrotch.hh"

#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4VisAttributes.hh"
#include "G4Color.hh"
#include "G4Cons.hh"
#include "G4Trd.hh"
#include "G4RotationMatrix.hh"
#include "G4SubtractionSolid.hh"

BesYtypeCrotch::BesYtypeCrotch()
{

  logicalYtypeCrotch = 0;
  physicalYtypeCrotch1 = 0;
  physicalYtypeCrotch2 = 0;


  Au = 0;
  Ag = 0;
  Oil = 0;
}

void BesYtypeCrotch::DefineMaterial()
{
  G4double density, a, z,fractionmass;
  G4int nel,natoms;
  G4String name, symbol;

  density=19.32*g/cm3;
  a = 196.967*g/mole;
  Au= new G4Material("Gold",79,a,density);

  density=10.5*g/cm3;
  a = 107.9*g/mole;
  Ag= new G4Material("Silver",47,a,density);

  a = 1.01*g/mole;
  G4Element* H  = new G4Element(name="Hydrogen",symbol="H" , z= 1., a);
    
  a = 12.01*g/mole;
  G4Element* C  = new G4Element(name="Carbon"  ,symbol="C" , z= 6., a);

  density = 0.810*g/cm3;
  Oil = new G4Material(name="Oil",density,nel=2);
  Oil->AddElement(C, natoms=18);
  Oil->AddElement(H, natoms=38);


  G4double pressure;

a = 14.01*g/mole;
    G4Element* N  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a);
    
    a = 16.00*g/mole;
    G4Element* O  = new G4Element(name="Oxygen"  ,symbol="O" , z= 8., a);


  density = 1.290*mg/cm3;
    G4Material* Air = new G4Material(name="Air",density,nel=2);
    Air->AddElement(N, fractionmass = 0.7);
    Air->AddElement(O, fractionmass = 0.3);



      density     = 1.0e-5*g/cm3;
    pressure    = 2.e-2*bar;
    G4Material* Beam = new G4Material(name="Beam",density,nel=1,kStateGas,STP_Temperature,pressure);
    Beam->AddMaterial(Air,fractionmass=1.0);


density = 1.848*g/cm3;
    a = 9.012182*g/mole;
    G4Material* Beryllium = new G4Material(name="Beryllium",z=4.0,a,density);

    density = 2.70*g/cm3;
    a = 26.98*g/mole;
    G4Material* Aluminium = new G4Material(name="Aluminium",z=13.,a,density);


}

void BesYtypeCrotch::Construct(G4LogicalVolume* logicalbes)
{
  DefineMaterial();

  G4RotationMatrix* zRot_90 = new G4RotationMatrix;  zRot_90->rotateZ(3.14159265/2);
  G4RotationMatrix* zRot_180 = new G4RotationMatrix;  zRot_180->rotateZ(3.14159265);
  G4RotationMatrix* zRot_270 = new G4RotationMatrix;  zRot_270->rotateZ(3.14159265/2*3);

   //G4RotationMatrix* xRot = new G4RotationMatrix;
  //xRot->rotateX(90*deg);

  //the logical volume of beam pipe
  G4Tubs* solidPip = new G4Tubs("solidPip",0,300./2,335./2,0,360);  //length: 335mm; 
  logicalYtypeCrotch = new G4LogicalVolume(solidPip, G4Material::GetMaterial("Beam"),"logicalYtypeCrotch");
  physicalYtypeCrotch1 = new G4PVPlacement(zRot_90,G4ThreeVector(0,0, 500+1365+335./2),logicalYtypeCrotch,"physicalYtypeCrotch1",logicalbes,false,0);


  G4RotationMatrix* yzRot = new G4RotationMatrix;
  yzRot->rotateZ(3.14159265/2);
  yzRot->rotateY(3.14159265);
  physicalYtypeCrotch2 = new G4PVPlacement(yzRot,G4ThreeVector(0,0,-500-1365-335./2),logicalYtypeCrotch,"physicalYtypeCrotch2",logicalbes,false,0);







  //5 components in the 335mm tube:  0-20 one side of CF110, 20-70 the rest of CF110,  70-154  complex stuff. 154-267 cons,   267-315 ???   315-335???



  G4Tubs* solidtub1 = new G4Tubs("solidtube1", 110./2, 165/2, 20./2, 0, 360); ;// one side of the SF110   thickness=20 ???? check this!!!!!!!!1-----
  G4LogicalVolume*  logicaltub1 = new G4LogicalVolume(solidtub1, G4Material::GetMaterial("StainlessSteel"),"logicaltub1");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  G4VPhysicalVolume *phycicaltub1 = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20./2),logicaltub1,"physicaltub1",logicalYtypeCrotch,false,0);
  

  G4Tubs* solidtub2 = new G4Tubs("solidtube1", 110./2, 120/2, 50./2, 0, 360); ;// the rest part of SF110
  G4LogicalVolume*  logicaltub2 = new G4LogicalVolume(solidtub2, G4Material::GetMaterial("StainlessSteel"),"logicaltub2");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  G4VPhysicalVolume *phycicaltub2 = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20.+50./2),logicaltub2,"physicaltub2",logicalYtypeCrotch,false,0);


  G4Tubs* solidtub3 = new G4Tubs("solidtube1", 110./2, 180/2, 84./2, 0, 360); ;// the complex stuff with cooling passage
  G4LogicalVolume*  logicaltub3 = new G4LogicalVolume(solidtub3, G4Material::GetMaterial("StainlessSteel"),"logicaltub3");   //FIX ME PLEASE, change to Stainless steel when you have time.  *********************BE Careful*********************.
  G4VPhysicalVolume *phycicaltub3 = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20.+50.+84./2),logicaltub3,"physicaltub3",logicalYtypeCrotch,false,0);


  G4Cons* solidCons = new G4Cons("solidCons", 110./2, (110.+4.)/2, 160./2, (160.+4.)/2, 109./2,  0, 360);   //140 is my guess...:)  correct it please
  G4LogicalVolume* logicalCons = new G4LogicalVolume(solidCons, G4Material::GetMaterial("Copper"),"logicalCons"); ////FIX ME PLEASE, what material???.  *********************BE Careful*********************.
  G4VPhysicalVolume* phycicalCons = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20.+50.+84.+109/2),logicalCons,"physicalCons",logicalYtypeCrotch,false,0);
 




  //to combine the non-regular output pipe

  double plate_thickness = 4; //mm
  double split_length = 48 + plate_thickness;// *mm;

  G4Box *solidChrotchContainer = new G4Box("solidChrotchContainer", 100./2, 200./2, split_length/2);
  G4LogicalVolume* logicalChrotchContainer = new G4LogicalVolume(solidChrotchContainer, G4Material::GetMaterial("Beam"),"solidChrotchContainer");
  G4VPhysicalVolume* phycicalChrotchContainer = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20.+50.+84.+109+split_length/2),logicalChrotchContainer,"phycicalChrotchContainer",logicalYtypeCrotch,false,0);


  //a plate with thickness of 4mm, subtract two big holes.

  G4Tubs* solidplate = new G4Tubs("solidplate", 0, 160./2, plate_thickness/2., 0, 3.1415926);
  G4Tubs* solidhole = new G4Tubs("solidhole", 0, 76./2, plate_thickness/2., 0., 3.1415926*2);

  G4SubtractionSolid* solidplate_tmp = new G4SubtractionSolid("solidplate_tmp", solidplate, solidhole, 0, G4ThreeVector(0, 38, 0));
  //G4SubtractionSolid* solidplate_sub_holes     = new G4SubtractionSolid("solidplate_sub_holes", solidplate_tmp, solidhole, 0, G4ThreeVector(0, -38, 0));

  G4RotationMatrix* zRot = new G4RotationMatrix;  zRot->rotateZ(3.14159265);

  double pos_plate = -1 * (split_length - plate_thickness)/2;  

  G4LogicalVolume* logicalPlate = new G4LogicalVolume(solidplate_tmp, G4Material::GetMaterial("StainlessSteel"),"logicalPlate");
  G4VPhysicalVolume* phycicalPlate = new G4PVPlacement(0,G4ThreeVector(0,0,pos_plate),logicalPlate,"phycicalPlate",logicalChrotchContainer,false,0);
  G4VPhysicalVolume* phycicalPlate_2 = new G4PVPlacement(zRot,G4ThreeVector(0,0,pos_plate),logicalPlate,"phycicalPlate",logicalChrotchContainer,false,0);




  double split_angle = 0.03;

  G4Trd  *solidbox_middle = new G4Trd("solidbox_middle", 40./2, 40./2, 10./2, (10-2*sin(0.03)*(split_length - plate_thickness)), (split_length - plate_thickness)/2);
  G4LogicalVolume* logicalBox_middle = new G4LogicalVolume(solidbox_middle, G4Material::GetMaterial("Copper"),"logicalBox_middle");
  G4VPhysicalVolume* phycicalBox_middle = new G4PVPlacement(0,G4ThreeVector(0,0,plate_thickness/2.),logicalBox_middle,"phycicalBox_middle",logicalChrotchContainer,false,0);


  

  G4Box *solidSplitContainer = new G4Box("solidSplitContainer", 100./2, 76./2, (split_length - plate_thickness)/2);
  G4LogicalVolume* logicalSplitContainer = new G4LogicalVolume(solidSplitContainer, G4Material::GetMaterial("Beam"),"logicalSplitContainer");

  G4RotationMatrix* xRot_split_angle1 = new G4RotationMatrix;  xRot_split_angle1->rotateX(split_angle); //rough estimation: split angle of one beam pipe
  G4RotationMatrix* xRot_split_angle2 = new G4RotationMatrix;  xRot_split_angle2->rotateX(-1*split_angle); //rough estimation: split angle of one beam pipe  

  G4VPhysicalVolume* phycicalSplitContainer1 = new G4PVPlacement(xRot_split_angle1,G4ThreeVector(0,43-sin(0.03)*(split_length - plate_thickness),plate_thickness/2.),logicalSplitContainer,"phycicalSplitContainer1",logicalChrotchContainer,false,0);
  G4VPhysicalVolume* phycicalSplitContainer2 = new G4PVPlacement(xRot_split_angle2,G4ThreeVector(0,-43+sin(0.03)*(split_length - plate_thickness),plate_thickness/2.),logicalSplitContainer,"phycicalSplitContainer2",logicalChrotchContainer,false,0);



  G4Tubs *solidtub_onefourth = new G4Tubs("solidtub_onfourth", 60./2, 70/2, (split_length - plate_thickness)/2, 0, 3.14159265/2);// 1/4 tube
  G4Box  *solidbox_side1 = new G4Box("solidbox_side1", 5./2, 6./2, (split_length - plate_thickness)/2);
  G4Box  *solidbox_side2 = new G4Box("solidbox_side2", 16./2, 5./2, (split_length - plate_thickness)/2);

  G4LogicalVolume* logicalTub_onefourth = new G4LogicalVolume(solidtub_onefourth, G4Material::GetMaterial("Copper"),"logicalTub_onefourth");
  G4LogicalVolume* logicalBox_side1 = new G4LogicalVolume(solidbox_side1, G4Material::GetMaterial("Copper"),"logicalBox_side1");
  G4LogicalVolume* logicalBox_side2 = new G4LogicalVolume(solidbox_side2, G4Material::GetMaterial("Copper"),"logicalBox_side2");

 
 
G4VPhysicalVolume*  physicalTub2 = new G4PVPlacement(zRot_90,G4ThreeVector(8,-3,0),logicalTub_onefourth,"physicalTub2",logicalSplitContainer,false,0);
G4VPhysicalVolume*  physicalTub3 = new G4PVPlacement(zRot_180,G4ThreeVector(-8,-3,0),logicalTub_onefourth,"physicalTub3",logicalSplitContainer,false,0);
G4VPhysicalVolume*  physicalTub4 = new G4PVPlacement(zRot_270,G4ThreeVector(-8,3,0),logicalTub_onefourth,"physicalTub4",logicalSplitContainer,false,0);

G4VPhysicalVolume*  physicalBox1 = new G4PVPlacement(0,G4ThreeVector(38+5./2,0,0),logicalBox_side1,"physicalBox1",logicalSplitContainer,false,0);
G4VPhysicalVolume*  physicalBox2 = new G4PVPlacement(0,G4ThreeVector(0, 33+5./2,0),logicalBox_side2,"physicalBox2",logicalSplitContainer,false,0);
G4VPhysicalVolume*  physicalBox3 = new G4PVPlacement(0,G4ThreeVector(-38-5./2,0,0),logicalBox_side1,"physicalBox3",logicalSplitContainer,false,0);
G4VPhysicalVolume*  physicalBox4 = new G4PVPlacement(0,G4ThreeVector(0, -33-5./2,0),logicalBox_side2,"physicalBox4",logicalSplitContainer,false,0);


//the last part is the connection between Y crotch and the following two chambers.

  G4Box *solidConnectionContainer = new G4Box("solidConnectionContainer", 100./2, 200./2, 20./2);
  G4LogicalVolume* logicalConnectionContainer = new G4LogicalVolume(solidConnectionContainer, G4Material::GetMaterial("Beam"),"logicalConnectionContainer");

  G4VPhysicalVolume* phycicalConnectionContainer = new G4PVPlacement(0,G4ThreeVector(0,0,-335./2+20.+50.+84.+109+split_length + 20./2),logicalConnectionContainer,"phycicalChrotchContainer",logicalYtypeCrotch,false,0);




  G4Box  *solidbox_middle_connection = new G4Box("solidbox_middle_connection", 40./2, 10./2, 20./2);
  G4LogicalVolume* logicalBox_middle_connection = new G4LogicalVolume(solidbox_middle_connection, G4Material::GetMaterial("StainlessSteel"),"logicalBox_middle_connection");
  G4VPhysicalVolume* phycicalBox_middle_connection = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicalBox_middle_connection,"phycicalBox_middle_connection",logicalConnectionContainer,false,0);




  G4Box *solidConnection_split = new G4Box("solidConnection_split", 100./2, 76./2, 20./2);
  G4LogicalVolume* logicalConnection_split = new G4LogicalVolume(solidConnection_split, G4Material::GetMaterial("Beam"),"logicalConnection_split");

  G4VPhysicalVolume* phycicalConnection_split1 = new G4PVPlacement(0,G4ThreeVector(0,43-sin(0.03)* 20, 0.),logicalConnection_split,"phycicalConnection_split1",logicalConnectionContainer,false,0);

  G4VPhysicalVolume* phycicalConnection_split2 = new G4PVPlacement(0,G4ThreeVector(0,-43+sin(0.03)* 20, 0.),logicalConnection_split,"phycicalConnection_split2",logicalConnectionContainer,false,0);






  G4Tubs *solidtub_onefourth_connection = new G4Tubs("solidtub_onefourth_connection", 60./2, 70/2, 20./2, 0, 3.14159265/2);// 1/4 tube
  G4Box  *solidbox_side1_connection = new G4Box("solidbox_side1_connection", 5./2, 6./2, 20./2);
  G4Box  *solidbox_side2_connection = new G4Box("solidbox_side2_connection", 16./2, 5./2, 20./2);

  G4LogicalVolume* logicalTub_onefourth_connection = new G4LogicalVolume(solidtub_onefourth_connection, G4Material::GetMaterial("StainlessSteel"),"logicalTub_onefourth_connection");
  G4LogicalVolume* logicalBox_side1_connection = new G4LogicalVolume(solidbox_side1_connection, G4Material::GetMaterial("StainlessSteel"),"logicalBox_side1_connection");
  G4LogicalVolume* logicalBox_side2_connection = new G4LogicalVolume(solidbox_side2_connection, G4Material::GetMaterial("StainlessSteel"),"logicalBox_side2_connection");

G4VPhysicalVolume*  physicalTub1_connection = new G4PVPlacement(0,G4ThreeVector(8,3,0),logicalTub_onefourth_connection,"physicalTub1_connection",logicalConnection_split,false,0);
G4VPhysicalVolume*  physicalTub2_connection = new G4PVPlacement(zRot_90,G4ThreeVector(8,-3,0),logicalTub_onefourth_connection,"physicalTub2",logicalConnection_split,false,0);
G4VPhysicalVolume*  physicalTub3_connection = new G4PVPlacement(zRot_180,G4ThreeVector(-8,-3,0),logicalTub_onefourth_connection,"physicalTub3",logicalConnection_split,false,0);
G4VPhysicalVolume*  physicalTub4_connection = new G4PVPlacement(zRot_270,G4ThreeVector(-8,3,0),logicalTub_onefourth_connection,"physicalTub4",logicalConnection_split,false,0);

G4VPhysicalVolume*  physicalBox1_connection = new G4PVPlacement(0,G4ThreeVector(38+5./2,0,0),logicalBox_side1_connection,"physicalBox1_connection",logicalConnection_split,false,0);
G4VPhysicalVolume*  physicalBox2_connection = new G4PVPlacement(0,G4ThreeVector(0, 33+5./2,0),logicalBox_side2_connection,"physicalBox2_connection",logicalConnection_split,false,0);
G4VPhysicalVolume*  physicalBox3_connection = new G4PVPlacement(0,G4ThreeVector(-38-5./2,0,0),logicalBox_side1_connection,"physicalBox3_connection",logicalConnection_split,false,0);
G4VPhysicalVolume*  physicalBox4_connection = new G4PVPlacement(0,G4ThreeVector(0, -33-5./2,0),logicalBox_side2_connection,"physicalBox4_connection",logicalConnection_split,false,0);







  logicalYtypeCrotch->SetVisAttributes(G4VisAttributes::Invisible);
  logicalSplitContainer->SetVisAttributes(G4VisAttributes::Invisible);
  logicalChrotchContainer->SetVisAttributes(G4VisAttributes::Invisible);
  logicalConnectionContainer->SetVisAttributes(G4VisAttributes::Invisible);
  logicalConnection_split->SetVisAttributes(G4VisAttributes::Invisible);

  G4VisAttributes* visPip = new G4VisAttributes(G4Colour(209./255,196./255, 33./255));
  G4VisAttributes* visSteel = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  G4VisAttributes* visCopper = new G4VisAttributes(G4Colour(146./255, 83./255, 48./255));
  G4VisAttributes* vissplit = new G4VisAttributes(G4Colour(209./255,196./255, 33./255));  

  logicaltub1->SetVisAttributes(visSteel);  //CF110
  logicaltub2->SetVisAttributes(visSteel);  //CF110
  logicaltub3->SetVisAttributes(visSteel);  



  logicalPlate->SetVisAttributes(visCopper);
  logicalCons->SetVisAttributes(visCopper);
  logicalTub_onefourth->SetVisAttributes(visCopper);
  logicalBox_side1->SetVisAttributes(visCopper);
  logicalBox_side2->SetVisAttributes(visCopper);
  logicalBox_middle->SetVisAttributes(visCopper);
  
  logicalBox_side1_connection->SetVisAttributes(visSteel);
  logicalBox_side2_connection->SetVisAttributes(visSteel);
  logicalBox_middle_connection->SetVisAttributes(visSteel);
  logicalTub_onefourth_connection->SetVisAttributes(visSteel);

  







}
