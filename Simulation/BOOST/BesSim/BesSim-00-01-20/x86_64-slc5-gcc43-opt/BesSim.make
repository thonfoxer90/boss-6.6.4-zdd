#-- start of make_header -----------------

#====================================
#  Library BesSim
#
#   Generated Tue Jun 19 14:15:34 2018  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_BesSim_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_BesSim_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_BesSim

BesSim_tag = $(tag)

#cmt_local_tagfile_BesSim = $(BesSim_tag)_BesSim.make
cmt_local_tagfile_BesSim = $(bin)$(BesSim_tag)_BesSim.make

else

tags      = $(tag),$(CMTEXTRATAGS)

BesSim_tag = $(tag)

#cmt_local_tagfile_BesSim = $(BesSim_tag).make
cmt_local_tagfile_BesSim = $(bin)$(BesSim_tag).make

endif

include $(cmt_local_tagfile_BesSim)
#-include $(cmt_local_tagfile_BesSim)

ifdef cmt_BesSim_has_target_tag

cmt_final_setup_BesSim = $(bin)setup_BesSim.make
#cmt_final_setup_BesSim = $(bin)BesSim_BesSimsetup.make
cmt_local_BesSim_makefile = $(bin)BesSim.make

else

cmt_final_setup_BesSim = $(bin)setup.make
#cmt_final_setup_BesSim = $(bin)BesSimsetup.make
cmt_local_BesSim_makefile = $(bin)BesSim.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)BesSimsetup.make

#BesSim :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'BesSim'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = BesSim/
#BesSim::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

BesSimlibname   = $(bin)$(library_prefix)BesSim$(library_suffix)
BesSimlib       = $(BesSimlibname).a
BesSimstamp     = $(bin)BesSim.stamp
BesSimshstamp   = $(bin)BesSim.shstamp

BesSim :: dirs  BesSimLIB
	$(echo) "BesSim ok"

#-- end of libary_header ----------------

BesSimLIB :: $(BesSimlib) $(BesSimshstamp)
	@/bin/echo "------> BesSim : library ok"

$(BesSimlib) :: $(bin)BesDetectorConstruction.o $(bin)BesTwoBeamPipe.o $(bin)BesSCMParameter.o $(bin)BesSim.o $(bin)BesSCQChamber.o $(bin)BesSteppingAction.o $(bin)BesTuningIO.o $(bin)BesPipExtension.o $(bin)BesTDSWriter.o $(bin)BesPipParameter.o $(bin)BesAsciiIO.o $(bin)BesRunAction.o $(bin)BesMcTruthWriter.o $(bin)BesEventAction.o $(bin)BesSCM.o $(bin)BesTrackingAction.o $(bin)BesRawDataWriter.o $(bin)BesYtypeCrotch.o $(bin)BesMagneticField.o $(bin)BesPip.o $(bin)BesRootIO.o $(bin)BesLogSession.o $(bin)BesRunActionMessenger.o $(bin)G4MiscLHEPBuilder_CHIPS.o $(bin)BesMagneticFieldMessenger.o $(bin)G4SubtractionSolid.o $(bin)HadronPhysicsQGSP_BERT_CHIPS.o $(bin)BesSim_load.o $(bin)BesSim_entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(BesSimlib) $?
	$(lib_silent) $(ranlib) $(BesSimlib)
	$(lib_silent) cat /dev/null >$(BesSimstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(BesSimlibname).$(shlibsuffix) :: $(BesSimlib) $(BesSimstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" BesSim $(BesSim_shlibflags)

$(BesSimshstamp) :: $(BesSimlibname).$(shlibsuffix)
	@if test -f $(BesSimlibname).$(shlibsuffix) ; then cat /dev/null >$(BesSimshstamp) ; fi

BesSimclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)BesDetectorConstruction.o $(bin)BesTwoBeamPipe.o $(bin)BesSCMParameter.o $(bin)BesSim.o $(bin)BesSCQChamber.o $(bin)BesSteppingAction.o $(bin)BesTuningIO.o $(bin)BesPipExtension.o $(bin)BesTDSWriter.o $(bin)BesPipParameter.o $(bin)BesAsciiIO.o $(bin)BesRunAction.o $(bin)BesMcTruthWriter.o $(bin)BesEventAction.o $(bin)BesSCM.o $(bin)BesTrackingAction.o $(bin)BesRawDataWriter.o $(bin)BesYtypeCrotch.o $(bin)BesMagneticField.o $(bin)BesPip.o $(bin)BesRootIO.o $(bin)BesLogSession.o $(bin)BesRunActionMessenger.o $(bin)G4MiscLHEPBuilder_CHIPS.o $(bin)BesMagneticFieldMessenger.o $(bin)G4SubtractionSolid.o $(bin)HadronPhysicsQGSP_BERT_CHIPS.o $(bin)BesSim_load.o $(bin)BesSim_entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
BesSiminstallname = $(library_prefix)BesSim$(library_suffix).$(shlibsuffix)

BesSim :: BesSiminstall

install :: BesSiminstall

BesSiminstall :: $(install_dir)/$(BesSiminstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(BesSiminstallname) :: $(bin)$(BesSiminstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(BesSiminstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(BesSiminstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BesSiminstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(BesSiminstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(BesSiminstallname) $(install_dir)/$(BesSiminstallname); \
	      echo `pwd`/$(BesSiminstallname) >$(install_dir)/$(BesSiminstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(BesSiminstallname), no installation directory specified"; \
	  fi; \
	fi

BesSimclean :: BesSimuninstall

uninstall :: BesSimuninstall

BesSimuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(BesSiminstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BesSiminstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(BesSiminstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(BesSiminstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)BesSim_dependencies.make :: dirs

ifndef QUICK
$(bin)BesSim_dependencies.make : $(src)BesDetectorConstruction.cc $(src)BesTwoBeamPipe.cc $(src)BesSCMParameter.cc $(src)BesSim.cc $(src)BesSCQChamber.cc $(src)BesSteppingAction.cc $(src)BesTuningIO.cc $(src)BesPipExtension.cc $(src)BesTDSWriter.cc $(src)BesPipParameter.cc $(src)BesAsciiIO.cc $(src)BesRunAction.cc $(src)BesMcTruthWriter.cc $(src)BesEventAction.cc $(src)BesSCM.cc $(src)BesTrackingAction.cc $(src)BesRawDataWriter.cc $(src)BesYtypeCrotch.cc $(src)BesMagneticField.cc $(src)BesPip.cc $(src)BesRootIO.cc $(src)BesLogSession.cc $(src)BesRunActionMessenger.cc $(src)G4MiscLHEPBuilder_CHIPS.cc $(src)BesMagneticFieldMessenger.cc $(src)G4SubtractionSolid.cc $(src)HadronPhysicsQGSP_BERT_CHIPS.cc $(src)components/BesSim_load.cxx $(src)components/BesSim_entries.cxx $(use_requirements) $(cmt_final_setup_BesSim)
	$(echo) "(BesSim.make) Rebuilding $@"; \
	  $(build_dependencies) BesSim -all_sources -out=$@ $(src)BesDetectorConstruction.cc $(src)BesTwoBeamPipe.cc $(src)BesSCMParameter.cc $(src)BesSim.cc $(src)BesSCQChamber.cc $(src)BesSteppingAction.cc $(src)BesTuningIO.cc $(src)BesPipExtension.cc $(src)BesTDSWriter.cc $(src)BesPipParameter.cc $(src)BesAsciiIO.cc $(src)BesRunAction.cc $(src)BesMcTruthWriter.cc $(src)BesEventAction.cc $(src)BesSCM.cc $(src)BesTrackingAction.cc $(src)BesRawDataWriter.cc $(src)BesYtypeCrotch.cc $(src)BesMagneticField.cc $(src)BesPip.cc $(src)BesRootIO.cc $(src)BesLogSession.cc $(src)BesRunActionMessenger.cc $(src)G4MiscLHEPBuilder_CHIPS.cc $(src)BesMagneticFieldMessenger.cc $(src)G4SubtractionSolid.cc $(src)HadronPhysicsQGSP_BERT_CHIPS.cc $(src)components/BesSim_load.cxx $(src)components/BesSim_entries.cxx
endif

#$(BesSim_dependencies)

-include $(bin)BesSim_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesDetectorConstruction.d

$(bin)$(binobj)BesDetectorConstruction.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesDetectorConstruction.d : $(src)BesDetectorConstruction.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesDetectorConstruction.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesDetectorConstruction_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesDetectorConstruction_cppflags) $(BesDetectorConstruction_cc_cppflags)  $(src)BesDetectorConstruction.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesDetectorConstruction.o $(src)BesDetectorConstruction.cc $(@D)/BesDetectorConstruction.dep
endif
endif

$(bin)$(binobj)BesDetectorConstruction.o : $(src)BesDetectorConstruction.cc
else
$(bin)BesSim_dependencies.make : $(BesDetectorConstruction_cc_dependencies)

$(bin)$(binobj)BesDetectorConstruction.o : $(BesDetectorConstruction_cc_dependencies)
endif
	$(cpp_echo) $(src)BesDetectorConstruction.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesDetectorConstruction_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesDetectorConstruction_cppflags) $(BesDetectorConstruction_cc_cppflags)  $(src)BesDetectorConstruction.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesTwoBeamPipe.d

$(bin)$(binobj)BesTwoBeamPipe.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesTwoBeamPipe.d : $(src)BesTwoBeamPipe.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesTwoBeamPipe.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesTwoBeamPipe_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesTwoBeamPipe_cppflags) $(BesTwoBeamPipe_cc_cppflags)  $(src)BesTwoBeamPipe.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesTwoBeamPipe.o $(src)BesTwoBeamPipe.cc $(@D)/BesTwoBeamPipe.dep
endif
endif

$(bin)$(binobj)BesTwoBeamPipe.o : $(src)BesTwoBeamPipe.cc
else
$(bin)BesSim_dependencies.make : $(BesTwoBeamPipe_cc_dependencies)

$(bin)$(binobj)BesTwoBeamPipe.o : $(BesTwoBeamPipe_cc_dependencies)
endif
	$(cpp_echo) $(src)BesTwoBeamPipe.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesTwoBeamPipe_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesTwoBeamPipe_cppflags) $(BesTwoBeamPipe_cc_cppflags)  $(src)BesTwoBeamPipe.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesSCMParameter.d

$(bin)$(binobj)BesSCMParameter.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesSCMParameter.d : $(src)BesSCMParameter.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesSCMParameter.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSCMParameter_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSCMParameter_cppflags) $(BesSCMParameter_cc_cppflags)  $(src)BesSCMParameter.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesSCMParameter.o $(src)BesSCMParameter.cc $(@D)/BesSCMParameter.dep
endif
endif

$(bin)$(binobj)BesSCMParameter.o : $(src)BesSCMParameter.cc
else
$(bin)BesSim_dependencies.make : $(BesSCMParameter_cc_dependencies)

$(bin)$(binobj)BesSCMParameter.o : $(BesSCMParameter_cc_dependencies)
endif
	$(cpp_echo) $(src)BesSCMParameter.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSCMParameter_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSCMParameter_cppflags) $(BesSCMParameter_cc_cppflags)  $(src)BesSCMParameter.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesSim.d

$(bin)$(binobj)BesSim.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesSim.d : $(src)BesSim.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesSim.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSim_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSim_cppflags) $(BesSim_cc_cppflags)  $(src)BesSim.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesSim.o $(src)BesSim.cc $(@D)/BesSim.dep
endif
endif

$(bin)$(binobj)BesSim.o : $(src)BesSim.cc
else
$(bin)BesSim_dependencies.make : $(BesSim_cc_dependencies)

$(bin)$(binobj)BesSim.o : $(BesSim_cc_dependencies)
endif
	$(cpp_echo) $(src)BesSim.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSim_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSim_cppflags) $(BesSim_cc_cppflags)  $(src)BesSim.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesSCQChamber.d

$(bin)$(binobj)BesSCQChamber.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesSCQChamber.d : $(src)BesSCQChamber.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesSCQChamber.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSCQChamber_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSCQChamber_cppflags) $(BesSCQChamber_cc_cppflags)  $(src)BesSCQChamber.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesSCQChamber.o $(src)BesSCQChamber.cc $(@D)/BesSCQChamber.dep
endif
endif

$(bin)$(binobj)BesSCQChamber.o : $(src)BesSCQChamber.cc
else
$(bin)BesSim_dependencies.make : $(BesSCQChamber_cc_dependencies)

$(bin)$(binobj)BesSCQChamber.o : $(BesSCQChamber_cc_dependencies)
endif
	$(cpp_echo) $(src)BesSCQChamber.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSCQChamber_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSCQChamber_cppflags) $(BesSCQChamber_cc_cppflags)  $(src)BesSCQChamber.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesSteppingAction.d

$(bin)$(binobj)BesSteppingAction.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesSteppingAction.d : $(src)BesSteppingAction.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesSteppingAction.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSteppingAction_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSteppingAction_cppflags) $(BesSteppingAction_cc_cppflags)  $(src)BesSteppingAction.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesSteppingAction.o $(src)BesSteppingAction.cc $(@D)/BesSteppingAction.dep
endif
endif

$(bin)$(binobj)BesSteppingAction.o : $(src)BesSteppingAction.cc
else
$(bin)BesSim_dependencies.make : $(BesSteppingAction_cc_dependencies)

$(bin)$(binobj)BesSteppingAction.o : $(BesSteppingAction_cc_dependencies)
endif
	$(cpp_echo) $(src)BesSteppingAction.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSteppingAction_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSteppingAction_cppflags) $(BesSteppingAction_cc_cppflags)  $(src)BesSteppingAction.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesTuningIO.d

$(bin)$(binobj)BesTuningIO.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesTuningIO.d : $(src)BesTuningIO.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesTuningIO.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesTuningIO_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesTuningIO_cppflags) $(BesTuningIO_cc_cppflags)  $(src)BesTuningIO.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesTuningIO.o $(src)BesTuningIO.cc $(@D)/BesTuningIO.dep
endif
endif

$(bin)$(binobj)BesTuningIO.o : $(src)BesTuningIO.cc
else
$(bin)BesSim_dependencies.make : $(BesTuningIO_cc_dependencies)

$(bin)$(binobj)BesTuningIO.o : $(BesTuningIO_cc_dependencies)
endif
	$(cpp_echo) $(src)BesTuningIO.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesTuningIO_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesTuningIO_cppflags) $(BesTuningIO_cc_cppflags)  $(src)BesTuningIO.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesPipExtension.d

$(bin)$(binobj)BesPipExtension.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesPipExtension.d : $(src)BesPipExtension.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesPipExtension.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesPipExtension_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesPipExtension_cppflags) $(BesPipExtension_cc_cppflags)  $(src)BesPipExtension.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesPipExtension.o $(src)BesPipExtension.cc $(@D)/BesPipExtension.dep
endif
endif

$(bin)$(binobj)BesPipExtension.o : $(src)BesPipExtension.cc
else
$(bin)BesSim_dependencies.make : $(BesPipExtension_cc_dependencies)

$(bin)$(binobj)BesPipExtension.o : $(BesPipExtension_cc_dependencies)
endif
	$(cpp_echo) $(src)BesPipExtension.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesPipExtension_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesPipExtension_cppflags) $(BesPipExtension_cc_cppflags)  $(src)BesPipExtension.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesTDSWriter.d

$(bin)$(binobj)BesTDSWriter.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesTDSWriter.d : $(src)BesTDSWriter.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesTDSWriter.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesTDSWriter_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesTDSWriter_cppflags) $(BesTDSWriter_cc_cppflags)  $(src)BesTDSWriter.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesTDSWriter.o $(src)BesTDSWriter.cc $(@D)/BesTDSWriter.dep
endif
endif

$(bin)$(binobj)BesTDSWriter.o : $(src)BesTDSWriter.cc
else
$(bin)BesSim_dependencies.make : $(BesTDSWriter_cc_dependencies)

$(bin)$(binobj)BesTDSWriter.o : $(BesTDSWriter_cc_dependencies)
endif
	$(cpp_echo) $(src)BesTDSWriter.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesTDSWriter_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesTDSWriter_cppflags) $(BesTDSWriter_cc_cppflags)  $(src)BesTDSWriter.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesPipParameter.d

$(bin)$(binobj)BesPipParameter.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesPipParameter.d : $(src)BesPipParameter.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesPipParameter.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesPipParameter_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesPipParameter_cppflags) $(BesPipParameter_cc_cppflags)  $(src)BesPipParameter.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesPipParameter.o $(src)BesPipParameter.cc $(@D)/BesPipParameter.dep
endif
endif

$(bin)$(binobj)BesPipParameter.o : $(src)BesPipParameter.cc
else
$(bin)BesSim_dependencies.make : $(BesPipParameter_cc_dependencies)

$(bin)$(binobj)BesPipParameter.o : $(BesPipParameter_cc_dependencies)
endif
	$(cpp_echo) $(src)BesPipParameter.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesPipParameter_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesPipParameter_cppflags) $(BesPipParameter_cc_cppflags)  $(src)BesPipParameter.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesAsciiIO.d

$(bin)$(binobj)BesAsciiIO.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesAsciiIO.d : $(src)BesAsciiIO.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesAsciiIO.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesAsciiIO_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesAsciiIO_cppflags) $(BesAsciiIO_cc_cppflags)  $(src)BesAsciiIO.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesAsciiIO.o $(src)BesAsciiIO.cc $(@D)/BesAsciiIO.dep
endif
endif

$(bin)$(binobj)BesAsciiIO.o : $(src)BesAsciiIO.cc
else
$(bin)BesSim_dependencies.make : $(BesAsciiIO_cc_dependencies)

$(bin)$(binobj)BesAsciiIO.o : $(BesAsciiIO_cc_dependencies)
endif
	$(cpp_echo) $(src)BesAsciiIO.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesAsciiIO_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesAsciiIO_cppflags) $(BesAsciiIO_cc_cppflags)  $(src)BesAsciiIO.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesRunAction.d

$(bin)$(binobj)BesRunAction.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesRunAction.d : $(src)BesRunAction.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesRunAction.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesRunAction_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesRunAction_cppflags) $(BesRunAction_cc_cppflags)  $(src)BesRunAction.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesRunAction.o $(src)BesRunAction.cc $(@D)/BesRunAction.dep
endif
endif

$(bin)$(binobj)BesRunAction.o : $(src)BesRunAction.cc
else
$(bin)BesSim_dependencies.make : $(BesRunAction_cc_dependencies)

$(bin)$(binobj)BesRunAction.o : $(BesRunAction_cc_dependencies)
endif
	$(cpp_echo) $(src)BesRunAction.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesRunAction_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesRunAction_cppflags) $(BesRunAction_cc_cppflags)  $(src)BesRunAction.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesMcTruthWriter.d

$(bin)$(binobj)BesMcTruthWriter.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesMcTruthWriter.d : $(src)BesMcTruthWriter.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesMcTruthWriter.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesMcTruthWriter_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesMcTruthWriter_cppflags) $(BesMcTruthWriter_cc_cppflags)  $(src)BesMcTruthWriter.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesMcTruthWriter.o $(src)BesMcTruthWriter.cc $(@D)/BesMcTruthWriter.dep
endif
endif

$(bin)$(binobj)BesMcTruthWriter.o : $(src)BesMcTruthWriter.cc
else
$(bin)BesSim_dependencies.make : $(BesMcTruthWriter_cc_dependencies)

$(bin)$(binobj)BesMcTruthWriter.o : $(BesMcTruthWriter_cc_dependencies)
endif
	$(cpp_echo) $(src)BesMcTruthWriter.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesMcTruthWriter_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesMcTruthWriter_cppflags) $(BesMcTruthWriter_cc_cppflags)  $(src)BesMcTruthWriter.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesEventAction.d

$(bin)$(binobj)BesEventAction.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesEventAction.d : $(src)BesEventAction.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesEventAction.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesEventAction_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesEventAction_cppflags) $(BesEventAction_cc_cppflags)  $(src)BesEventAction.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesEventAction.o $(src)BesEventAction.cc $(@D)/BesEventAction.dep
endif
endif

$(bin)$(binobj)BesEventAction.o : $(src)BesEventAction.cc
else
$(bin)BesSim_dependencies.make : $(BesEventAction_cc_dependencies)

$(bin)$(binobj)BesEventAction.o : $(BesEventAction_cc_dependencies)
endif
	$(cpp_echo) $(src)BesEventAction.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesEventAction_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesEventAction_cppflags) $(BesEventAction_cc_cppflags)  $(src)BesEventAction.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesSCM.d

$(bin)$(binobj)BesSCM.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesSCM.d : $(src)BesSCM.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesSCM.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSCM_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSCM_cppflags) $(BesSCM_cc_cppflags)  $(src)BesSCM.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesSCM.o $(src)BesSCM.cc $(@D)/BesSCM.dep
endif
endif

$(bin)$(binobj)BesSCM.o : $(src)BesSCM.cc
else
$(bin)BesSim_dependencies.make : $(BesSCM_cc_dependencies)

$(bin)$(binobj)BesSCM.o : $(BesSCM_cc_dependencies)
endif
	$(cpp_echo) $(src)BesSCM.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSCM_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSCM_cppflags) $(BesSCM_cc_cppflags)  $(src)BesSCM.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesTrackingAction.d

$(bin)$(binobj)BesTrackingAction.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesTrackingAction.d : $(src)BesTrackingAction.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesTrackingAction.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesTrackingAction_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesTrackingAction_cppflags) $(BesTrackingAction_cc_cppflags)  $(src)BesTrackingAction.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesTrackingAction.o $(src)BesTrackingAction.cc $(@D)/BesTrackingAction.dep
endif
endif

$(bin)$(binobj)BesTrackingAction.o : $(src)BesTrackingAction.cc
else
$(bin)BesSim_dependencies.make : $(BesTrackingAction_cc_dependencies)

$(bin)$(binobj)BesTrackingAction.o : $(BesTrackingAction_cc_dependencies)
endif
	$(cpp_echo) $(src)BesTrackingAction.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesTrackingAction_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesTrackingAction_cppflags) $(BesTrackingAction_cc_cppflags)  $(src)BesTrackingAction.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesRawDataWriter.d

$(bin)$(binobj)BesRawDataWriter.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesRawDataWriter.d : $(src)BesRawDataWriter.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesRawDataWriter.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesRawDataWriter_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesRawDataWriter_cppflags) $(BesRawDataWriter_cc_cppflags)  $(src)BesRawDataWriter.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesRawDataWriter.o $(src)BesRawDataWriter.cc $(@D)/BesRawDataWriter.dep
endif
endif

$(bin)$(binobj)BesRawDataWriter.o : $(src)BesRawDataWriter.cc
else
$(bin)BesSim_dependencies.make : $(BesRawDataWriter_cc_dependencies)

$(bin)$(binobj)BesRawDataWriter.o : $(BesRawDataWriter_cc_dependencies)
endif
	$(cpp_echo) $(src)BesRawDataWriter.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesRawDataWriter_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesRawDataWriter_cppflags) $(BesRawDataWriter_cc_cppflags)  $(src)BesRawDataWriter.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesYtypeCrotch.d

$(bin)$(binobj)BesYtypeCrotch.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesYtypeCrotch.d : $(src)BesYtypeCrotch.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesYtypeCrotch.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesYtypeCrotch_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesYtypeCrotch_cppflags) $(BesYtypeCrotch_cc_cppflags)  $(src)BesYtypeCrotch.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesYtypeCrotch.o $(src)BesYtypeCrotch.cc $(@D)/BesYtypeCrotch.dep
endif
endif

$(bin)$(binobj)BesYtypeCrotch.o : $(src)BesYtypeCrotch.cc
else
$(bin)BesSim_dependencies.make : $(BesYtypeCrotch_cc_dependencies)

$(bin)$(binobj)BesYtypeCrotch.o : $(BesYtypeCrotch_cc_dependencies)
endif
	$(cpp_echo) $(src)BesYtypeCrotch.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesYtypeCrotch_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesYtypeCrotch_cppflags) $(BesYtypeCrotch_cc_cppflags)  $(src)BesYtypeCrotch.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesMagneticField.d

$(bin)$(binobj)BesMagneticField.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesMagneticField.d : $(src)BesMagneticField.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesMagneticField.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesMagneticField_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesMagneticField_cppflags) $(BesMagneticField_cc_cppflags)  $(src)BesMagneticField.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesMagneticField.o $(src)BesMagneticField.cc $(@D)/BesMagneticField.dep
endif
endif

$(bin)$(binobj)BesMagneticField.o : $(src)BesMagneticField.cc
else
$(bin)BesSim_dependencies.make : $(BesMagneticField_cc_dependencies)

$(bin)$(binobj)BesMagneticField.o : $(BesMagneticField_cc_dependencies)
endif
	$(cpp_echo) $(src)BesMagneticField.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesMagneticField_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesMagneticField_cppflags) $(BesMagneticField_cc_cppflags)  $(src)BesMagneticField.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesPip.d

$(bin)$(binobj)BesPip.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesPip.d : $(src)BesPip.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesPip.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesPip_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesPip_cppflags) $(BesPip_cc_cppflags)  $(src)BesPip.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesPip.o $(src)BesPip.cc $(@D)/BesPip.dep
endif
endif

$(bin)$(binobj)BesPip.o : $(src)BesPip.cc
else
$(bin)BesSim_dependencies.make : $(BesPip_cc_dependencies)

$(bin)$(binobj)BesPip.o : $(BesPip_cc_dependencies)
endif
	$(cpp_echo) $(src)BesPip.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesPip_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesPip_cppflags) $(BesPip_cc_cppflags)  $(src)BesPip.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesRootIO.d

$(bin)$(binobj)BesRootIO.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesRootIO.d : $(src)BesRootIO.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesRootIO.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesRootIO_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesRootIO_cppflags) $(BesRootIO_cc_cppflags)  $(src)BesRootIO.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesRootIO.o $(src)BesRootIO.cc $(@D)/BesRootIO.dep
endif
endif

$(bin)$(binobj)BesRootIO.o : $(src)BesRootIO.cc
else
$(bin)BesSim_dependencies.make : $(BesRootIO_cc_dependencies)

$(bin)$(binobj)BesRootIO.o : $(BesRootIO_cc_dependencies)
endif
	$(cpp_echo) $(src)BesRootIO.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesRootIO_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesRootIO_cppflags) $(BesRootIO_cc_cppflags)  $(src)BesRootIO.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesLogSession.d

$(bin)$(binobj)BesLogSession.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesLogSession.d : $(src)BesLogSession.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesLogSession.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesLogSession_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesLogSession_cppflags) $(BesLogSession_cc_cppflags)  $(src)BesLogSession.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesLogSession.o $(src)BesLogSession.cc $(@D)/BesLogSession.dep
endif
endif

$(bin)$(binobj)BesLogSession.o : $(src)BesLogSession.cc
else
$(bin)BesSim_dependencies.make : $(BesLogSession_cc_dependencies)

$(bin)$(binobj)BesLogSession.o : $(BesLogSession_cc_dependencies)
endif
	$(cpp_echo) $(src)BesLogSession.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesLogSession_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesLogSession_cppflags) $(BesLogSession_cc_cppflags)  $(src)BesLogSession.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesRunActionMessenger.d

$(bin)$(binobj)BesRunActionMessenger.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesRunActionMessenger.d : $(src)BesRunActionMessenger.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesRunActionMessenger.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesRunActionMessenger_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesRunActionMessenger_cppflags) $(BesRunActionMessenger_cc_cppflags)  $(src)BesRunActionMessenger.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesRunActionMessenger.o $(src)BesRunActionMessenger.cc $(@D)/BesRunActionMessenger.dep
endif
endif

$(bin)$(binobj)BesRunActionMessenger.o : $(src)BesRunActionMessenger.cc
else
$(bin)BesSim_dependencies.make : $(BesRunActionMessenger_cc_dependencies)

$(bin)$(binobj)BesRunActionMessenger.o : $(BesRunActionMessenger_cc_dependencies)
endif
	$(cpp_echo) $(src)BesRunActionMessenger.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesRunActionMessenger_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesRunActionMessenger_cppflags) $(BesRunActionMessenger_cc_cppflags)  $(src)BesRunActionMessenger.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)G4MiscLHEPBuilder_CHIPS.d

$(bin)$(binobj)G4MiscLHEPBuilder_CHIPS.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)G4MiscLHEPBuilder_CHIPS.d : $(src)G4MiscLHEPBuilder_CHIPS.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4MiscLHEPBuilder_CHIPS.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(G4MiscLHEPBuilder_CHIPS_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(G4MiscLHEPBuilder_CHIPS_cppflags) $(G4MiscLHEPBuilder_CHIPS_cc_cppflags)  $(src)G4MiscLHEPBuilder_CHIPS.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4MiscLHEPBuilder_CHIPS.o $(src)G4MiscLHEPBuilder_CHIPS.cc $(@D)/G4MiscLHEPBuilder_CHIPS.dep
endif
endif

$(bin)$(binobj)G4MiscLHEPBuilder_CHIPS.o : $(src)G4MiscLHEPBuilder_CHIPS.cc
else
$(bin)BesSim_dependencies.make : $(G4MiscLHEPBuilder_CHIPS_cc_dependencies)

$(bin)$(binobj)G4MiscLHEPBuilder_CHIPS.o : $(G4MiscLHEPBuilder_CHIPS_cc_dependencies)
endif
	$(cpp_echo) $(src)G4MiscLHEPBuilder_CHIPS.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(G4MiscLHEPBuilder_CHIPS_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(G4MiscLHEPBuilder_CHIPS_cppflags) $(G4MiscLHEPBuilder_CHIPS_cc_cppflags)  $(src)G4MiscLHEPBuilder_CHIPS.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesMagneticFieldMessenger.d

$(bin)$(binobj)BesMagneticFieldMessenger.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesMagneticFieldMessenger.d : $(src)BesMagneticFieldMessenger.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesMagneticFieldMessenger.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesMagneticFieldMessenger_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesMagneticFieldMessenger_cppflags) $(BesMagneticFieldMessenger_cc_cppflags)  $(src)BesMagneticFieldMessenger.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesMagneticFieldMessenger.o $(src)BesMagneticFieldMessenger.cc $(@D)/BesMagneticFieldMessenger.dep
endif
endif

$(bin)$(binobj)BesMagneticFieldMessenger.o : $(src)BesMagneticFieldMessenger.cc
else
$(bin)BesSim_dependencies.make : $(BesMagneticFieldMessenger_cc_dependencies)

$(bin)$(binobj)BesMagneticFieldMessenger.o : $(BesMagneticFieldMessenger_cc_dependencies)
endif
	$(cpp_echo) $(src)BesMagneticFieldMessenger.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesMagneticFieldMessenger_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesMagneticFieldMessenger_cppflags) $(BesMagneticFieldMessenger_cc_cppflags)  $(src)BesMagneticFieldMessenger.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)G4SubtractionSolid.d

$(bin)$(binobj)G4SubtractionSolid.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)G4SubtractionSolid.d : $(src)G4SubtractionSolid.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4SubtractionSolid.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(G4SubtractionSolid_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(G4SubtractionSolid_cppflags) $(G4SubtractionSolid_cc_cppflags)  $(src)G4SubtractionSolid.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4SubtractionSolid.o $(src)G4SubtractionSolid.cc $(@D)/G4SubtractionSolid.dep
endif
endif

$(bin)$(binobj)G4SubtractionSolid.o : $(src)G4SubtractionSolid.cc
else
$(bin)BesSim_dependencies.make : $(G4SubtractionSolid_cc_dependencies)

$(bin)$(binobj)G4SubtractionSolid.o : $(G4SubtractionSolid_cc_dependencies)
endif
	$(cpp_echo) $(src)G4SubtractionSolid.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(G4SubtractionSolid_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(G4SubtractionSolid_cppflags) $(G4SubtractionSolid_cc_cppflags)  $(src)G4SubtractionSolid.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)HadronPhysicsQGSP_BERT_CHIPS.d

$(bin)$(binobj)HadronPhysicsQGSP_BERT_CHIPS.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)HadronPhysicsQGSP_BERT_CHIPS.d : $(src)HadronPhysicsQGSP_BERT_CHIPS.cc
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/HadronPhysicsQGSP_BERT_CHIPS.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(HadronPhysicsQGSP_BERT_CHIPS_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(HadronPhysicsQGSP_BERT_CHIPS_cppflags) $(HadronPhysicsQGSP_BERT_CHIPS_cc_cppflags)  $(src)HadronPhysicsQGSP_BERT_CHIPS.cc
	$(cpp_silent) $(format_dependencies) $@ $(@D)/HadronPhysicsQGSP_BERT_CHIPS.o $(src)HadronPhysicsQGSP_BERT_CHIPS.cc $(@D)/HadronPhysicsQGSP_BERT_CHIPS.dep
endif
endif

$(bin)$(binobj)HadronPhysicsQGSP_BERT_CHIPS.o : $(src)HadronPhysicsQGSP_BERT_CHIPS.cc
else
$(bin)BesSim_dependencies.make : $(HadronPhysicsQGSP_BERT_CHIPS_cc_dependencies)

$(bin)$(binobj)HadronPhysicsQGSP_BERT_CHIPS.o : $(HadronPhysicsQGSP_BERT_CHIPS_cc_dependencies)
endif
	$(cpp_echo) $(src)HadronPhysicsQGSP_BERT_CHIPS.cc
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(HadronPhysicsQGSP_BERT_CHIPS_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(HadronPhysicsQGSP_BERT_CHIPS_cppflags) $(HadronPhysicsQGSP_BERT_CHIPS_cc_cppflags)  $(src)HadronPhysicsQGSP_BERT_CHIPS.cc

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesSim_load.d

$(bin)$(binobj)BesSim_load.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesSim_load.d : $(src)components/BesSim_load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesSim_load.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSim_load_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSim_load_cppflags) $(BesSim_load_cxx_cppflags) -I../src/components $(src)components/BesSim_load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesSim_load.o $(src)components/BesSim_load.cxx $(@D)/BesSim_load.dep
endif
endif

$(bin)$(binobj)BesSim_load.o : $(src)components/BesSim_load.cxx
else
$(bin)BesSim_dependencies.make : $(BesSim_load_cxx_dependencies)

$(bin)$(binobj)BesSim_load.o : $(BesSim_load_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/BesSim_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSim_load_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSim_load_cppflags) $(BesSim_load_cxx_cppflags) -I../src/components $(src)components/BesSim_load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),BesSimclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BesSim_entries.d

$(bin)$(binobj)BesSim_entries.d : $(use_requirements) $(cmt_final_setup_BesSim)

$(bin)$(binobj)BesSim_entries.d : $(src)components/BesSim_entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesSim_entries.dep $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSim_entries_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSim_entries_cppflags) $(BesSim_entries_cxx_cppflags) -I../src/components $(src)components/BesSim_entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesSim_entries.o $(src)components/BesSim_entries.cxx $(@D)/BesSim_entries.dep
endif
endif

$(bin)$(binobj)BesSim_entries.o : $(src)components/BesSim_entries.cxx
else
$(bin)BesSim_dependencies.make : $(BesSim_entries_cxx_dependencies)

$(bin)$(binobj)BesSim_entries.o : $(BesSim_entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/BesSim_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(BesSim_pp_cppflags) $(lib_BesSim_pp_cppflags) $(BesSim_entries_pp_cppflags) $(use_cppflags) $(BesSim_cppflags) $(lib_BesSim_cppflags) $(BesSim_entries_cppflags) $(BesSim_entries_cxx_cppflags) -I../src/components $(src)components/BesSim_entries.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: BesSimclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(BesSim.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(BesSim.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(BesSim.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(BesSim.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_BesSim)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BesSim.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BesSim.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(BesSim.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

BesSimclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library BesSim
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)BesSim$(library_suffix).a $(library_prefix)BesSim$(library_suffix).s? BesSim.stamp BesSim.shstamp
#-- end of cleanup_library ---------------
