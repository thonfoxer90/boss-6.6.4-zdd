
#ifndef BesEventAction_h
#define BesEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "RealizationSvc/RealizationSvc.h"
#include "RealizationSvc/IRealizationSvc.h"

#include "TFile.h"
#include "TTree.h"
#include "TPolyLine3D.h"
#include "TVector3.h"

#include "G4ThreeVector.hh"

#include <vector>
#include <map>
#include <string>

#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::vector<TVector3> > ;
#endif

class BesRunAction;
class G4DigiManager;
//class IDataProviderSvc;

class BesEventAction : public G4UserEventAction
{
  public:
    BesEventAction(BesRunAction* runAction, bool impactRecoFlag,bool dump_flag, std::string impactRecoFilename);
    virtual ~BesEventAction();

		void InitializeNTupleVariables();
		void AddZddImpacts(G4ThreeVector v){m_ZDDImpactPoints.push_back(v);};
		void AddZddPID(G4int pid){m_PDGCodeStep.push_back(pid);};
		void AddPointToTrajectory(G4int trackID,G4ThreeVector vec);
		TPolyLine3D MakeTPolyLine3D(std::vector<G4ThreeVector> traj);
		std::vector<TVector3> MakeTrajectory(std::vector<G4ThreeVector> traj);
		TTree* GetNTuple(){return m_NTuple;};
		void FillTrajectoryPreStep(std::vector<G4ThreeVector> traj);
		
		
		void FillStepTrackID(G4int trackID){m_trackIDStep.push_back(trackID);};
		void FillStepParentID(G4int parentID){m_parentIDStep.push_back(parentID);};
		void FillStepID(G4int stepID){m_StepID.push_back(stepID);};
		void FillStepPDG(G4int pdgCode){m_PDGCodeStep.push_back(pdgCode);};
		void FillStepPreStepVec(G4ThreeVector preStepVec){
			m_xPreStep.push_back(preStepVec.x());
			m_yPreStep.push_back(preStepVec.y());
			m_zPreStep.push_back(preStepVec.z());
			};
		void FillStepPreStepMom(G4ThreeVector preStepMom){
			m_PxPreStep.push_back(preStepMom.x());
			m_PyPreStep.push_back(preStepMom.y());
			m_PzPreStep.push_back(preStepMom.z());
			};
		
		void FillStepPostStepVec(G4ThreeVector preStepVec){
			m_xPostStep.push_back(preStepVec.x());
			m_yPostStep.push_back(preStepVec.y());
			m_zPostStep.push_back(preStepVec.z());
			};
		void FillStepPostStepMom(G4ThreeVector postStepMom){
			m_PxPostStep.push_back(postStepMom.x());
			m_PyPostStep.push_back(postStepMom.y());
			m_PzPostStep.push_back(postStepMom.z());
			};
				
		void FillStepEdep(G4double Edep){m_eDepStep.push_back(Edep);};
		void FillStepVolPreStep(G4String g4str){		
		m_VolumePreStep.push_back(std::string(g4str.data()));
		};
		void FillPostVolPreStep(G4String g4str){		
		m_VolumePostStep.push_back(std::string(g4str.data()));
		};
			
		void FillIsFirstStepinVolume(G4bool val){m_IsFirstStepinVolume.push_back((G4int) val);};
		void FillIsLastStepinVolume(G4bool val){m_IsLastStepinVolume.push_back((G4int) val);};
		
		void FillETotPreStep(G4double val){m_ETotPreStep.push_back(val);};
		void FillETotPostStep(G4double val){m_ETotPostStep.push_back(val);};
		
		void FillNTupleStepVariables(G4int trackID, G4int parentID,G4int stepID, G4int pdgCode, G4ThreeVector preStepVec, G4ThreeVector preStepMom);
			
		G4double m_eDep;	
  public:
    virtual void   BeginOfEventAction(const G4Event*);
    virtual void   EndOfEventAction(const G4Event*);
    
  private:
    
    BesRunAction* m_runAction;
            
    G4DigiManager* m_DM;    
    // Reference to RealizationSvc
    RealizationSvc* m_RealizationSvc;
		
		bool m_ImpactRecoFlag;
		bool m_dump_flag;
    std::string m_ImpactRecoFilename;
		
		//ZDD Analysis
		TFile* m_File;
		TTree* m_NTuple;
		
		//From Impact_reco
		G4double m_zdd_energy;
  	G4double m_zddRight_energy;
  	G4double m_zddLeft_energy;

  	//Right part						 
  	G4double m_eDep101Right; 
  	G4double m_eDep102Right; 
  	G4double m_eDep103Right; 
  	G4double m_eDep104Right; 
  	G4double m_eDep111Right; 
  	G4double m_eDep112Right; 
  	G4double m_eDep113Right; 
  	G4double m_eDep114Right; 
  	G4double m_eDep121Right; 
  	G4double m_eDep122Right; 
  	G4double m_eDep123Right; 
  	G4double m_eDep124Right; 

  	G4double m_eDep201Right; 
  	G4double m_eDep202Right; 
  	G4double m_eDep203Right; 
  	G4double m_eDep204Right; 
  	G4double m_eDep211Right; 
  	G4double m_eDep212Right; 
  	G4double m_eDep213Right; 
  	G4double m_eDep214Right; 
  	G4double m_eDep221Right; 
  	G4double m_eDep222Right; 
  	G4double m_eDep223Right; 
  	G4double m_eDep224Right; 

  	G4int m_chargeChannel101Right;
  	G4int m_chargeChannel102Right;
  	G4int m_chargeChannel103Right;
  	G4int m_chargeChannel104Right;
  	G4int m_chargeChannel111Right;
  	G4int m_chargeChannel112Right;
  	G4int m_chargeChannel113Right;
  	G4int m_chargeChannel114Right;
  	G4int m_chargeChannel121Right;
  	G4int m_chargeChannel122Right;
  	G4int m_chargeChannel123Right;
  	G4int m_chargeChannel124Right;

  	G4int m_chargeChannel201Right;
  	G4int m_chargeChannel202Right;
  	G4int m_chargeChannel203Right;
  	G4int m_chargeChannel204Right;
  	G4int m_chargeChannel211Right;
  	G4int m_chargeChannel212Right;
  	G4int m_chargeChannel213Right;
  	G4int m_chargeChannel214Right;
  	G4int m_chargeChannel221Right;
  	G4int m_chargeChannel222Right;
  	G4int m_chargeChannel223Right;
  	G4int m_chargeChannel224Right;

  	//Left part 						 
  	 G4double m_eDep101Left; 
  	G4double m_eDep102Left;  
  	G4double m_eDep103Left;  
  	G4double m_eDep104Left;  
  	G4double m_eDep111Left;  
  	G4double m_eDep112Left;  
  	G4double m_eDep113Left;  
  	G4double m_eDep114Left;  
  	G4double m_eDep121Left;  
  	G4double m_eDep122Left;  
  	G4double m_eDep123Left;  
  	G4double m_eDep124Left;  

  	G4double m_eDep201Left;  
  	G4double m_eDep202Left;  
  	G4double m_eDep203Left;  
  	G4double m_eDep204Left;  
  	G4double m_eDep211Left;  
  	G4double m_eDep212Left;  
  	G4double m_eDep213Left;  
  	G4double m_eDep214Left;  
  	G4double m_eDep221Left;  
  	G4double m_eDep222Left;  
  	G4double m_eDep223Left;  
  	G4double m_eDep224Left;  

  	G4int m_chargeChannel101Left;
  	G4int m_chargeChannel102Left;
  	G4int m_chargeChannel103Left;
  	G4int m_chargeChannel104Left;
  	G4int m_chargeChannel111Left;
  	G4int m_chargeChannel112Left;
  	G4int m_chargeChannel113Left;
  	G4int m_chargeChannel114Left;
  	G4int m_chargeChannel121Left;
  	G4int m_chargeChannel122Left;
  	G4int m_chargeChannel123Left;
  	G4int m_chargeChannel124Left;

  	G4int m_chargeChannel201Left;
  	G4int m_chargeChannel202Left;
  	G4int m_chargeChannel203Left;
  	G4int m_chargeChannel204Left;
  	G4int m_chargeChannel211Left;
  	G4int m_chargeChannel212Left;
  	G4int m_chargeChannel213Left;
  	G4int m_chargeChannel214Left;
  	G4int m_chargeChannel221Left;
  	G4int m_chargeChannel222Left;
  	G4int m_chargeChannel223Left;
  	G4int m_chargeChannel224Left;

  	//Total 								 
  	 G4double m_eDep101;		 
  	G4double m_eDep102; 		 
  	G4double m_eDep103; 		 
  	G4double m_eDep104; 		 
  	G4double m_eDep111; 		 
  	G4double m_eDep112; 		 
  	G4double m_eDep113; 		 
  	G4double m_eDep114; 		 
  	G4double m_eDep121; 		 
  	G4double m_eDep122; 		 
  	G4double m_eDep123; 		 
  	G4double m_eDep124; 		 

  	G4double m_eDep201; 		 
  	G4double m_eDep202; 		 
  	G4double m_eDep203; 		 
  	G4double m_eDep204; 		 
  	G4double m_eDep211; 		 
  	G4double m_eDep212; 		 
  	G4double m_eDep213; 		 
  	G4double m_eDep214; 		 
  	G4double m_eDep221; 		 
  	G4double m_eDep222; 		 
  	G4double m_eDep223; 		 
  	G4double m_eDep224; 		 

  	G4int m_chargeChannel101;
  	G4int m_chargeChannel102;
  	G4int m_chargeChannel103;
  	G4int m_chargeChannel104;
  	G4int m_chargeChannel111;
  	G4int m_chargeChannel112;
  	G4int m_chargeChannel113;
  	G4int m_chargeChannel114;
  	G4int m_chargeChannel121;
  	G4int m_chargeChannel122;
  	G4int m_chargeChannel123;
  	G4int m_chargeChannel124;

  	G4int m_chargeChannel201;
  	G4int m_chargeChannel202;
  	G4int m_chargeChannel203;
  	G4int m_chargeChannel204;
  	G4int m_chargeChannel211;
  	G4int m_chargeChannel212;
  	G4int m_chargeChannel213;
  	G4int m_chargeChannel214;
  	G4int m_chargeChannel221;
  	G4int m_chargeChannel222;
  	G4int m_chargeChannel223;
  	G4int m_chargeChannel224;
  												 
		G4double m_xImpact;		 
		G4double m_yImpact; 		 
		G4double m_zImpact; 		 

		G4double m_PxImpact;		 
		G4double m_PyImpact;		 
		G4double m_PzImpact;		 

		int m_FirstImpactPID; 	 

		double m_xImpactElec; 	 
		double m_yImpactElec; 	 

		double m_xImpactPositron;
		double m_yImpactPositron;
		
		//Impact points
		G4int m_evtID;
		G4int 	m_Ntracks;
		std::vector<G4double> m_xImpacts;
		std::vector<G4double> m_yImpacts;
		std::vector<G4double> m_zImpacts;
		
		std::vector<G4ThreeVector> m_ZDDImpactPoints;
		
		std::vector<G4int> m_evtIDs;		
		
		std::vector<TPolyLine3D>	m_Trajectories;
		std::vector<std::vector<TVector3> > m_TrajTVector3;
		std::map<G4int,std::vector<G4ThreeVector> > m_mapTrackPoints;
		TPolyLine3D m_Trajectory;
		
		//Step by step variables 
		std::vector<G4int> m_trackIDStep;
		std::vector<G4int> m_parentIDStep;
		std::vector<G4int> m_StepID;
		std::vector<G4int>	m_PDGCodeStep;
				
		std::vector<G4double> m_xPreStep;
		std::vector<G4double> m_yPreStep;
		std::vector<G4double> m_zPreStep;
		
		std::vector<G4double> m_PxPreStep;
		std::vector<G4double> m_PyPreStep;
		std::vector<G4double> m_PzPreStep;
		
		std::vector<G4double> m_xPostStep;
		std::vector<G4double> m_yPostStep;
		std::vector<G4double> m_zPostStep;
		
		std::vector<G4double> m_PxPostStep;
		std::vector<G4double> m_PyPostStep;
		std::vector<G4double> m_PzPostStep;
		
		std::vector<G4double> m_eDepStep;
		std::vector<std::string> m_VolumePreStep;
		std::vector<std::string> m_VolumePostStep;
		std::vector<G4int> m_IsFirstStepinVolume;
		std::vector<G4int> m_IsLastStepinVolume;
		
		std::vector<G4double> m_ETotPreStep;
		std::vector<G4double> m_ETotPostStep;
		
		
};


#endif

    
