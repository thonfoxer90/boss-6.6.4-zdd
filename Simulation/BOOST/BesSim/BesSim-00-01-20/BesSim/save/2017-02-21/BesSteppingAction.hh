
#ifndef BesSteppingAction_h
#define BesSteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "G4StepPoint.hh"

#include <vector>

class BesSteppingAction : public G4UserSteppingAction
{
  public:
    BesSteppingAction();
   ~BesSteppingAction();
 
  static G4double GetXImpact(){return m_x;};
  static G4double GetYImpact(){return m_y;};
	static G4double GetZImpact(){return m_z;};


    void UserSteppingAction(const G4Step*);
 private:
    static G4double m_x;
    static G4double m_y;
		static G4double m_z;
		
};

#endif
