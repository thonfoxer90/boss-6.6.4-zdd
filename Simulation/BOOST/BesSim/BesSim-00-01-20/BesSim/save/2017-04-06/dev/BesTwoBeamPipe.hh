//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Descpirtion: Beam Pipe detector as a concrete class
//             it's better to define an envelope then position it in BES
//Author: Liuhm
//Created: May 21, 2003
//Comment:
//---------------------------------------------------------------------------//
//
#ifndef BesTwoBeamPipe_H
#define BesTwoBeamPipe_H 1

//#include "BesSubdetector.hh"
#include "globals.hh"
#include "G4Material.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"
#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4PSDoseDeposit.hh"
#include "G4VisAttributes.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4ReflectionFactory.hh"

class G4LogicalVolume;

class BesTwoBeamPipe
{  
  public:
  
   BesTwoBeamPipe();
   virtual ~BesTwoBeamPipe(){;}

   //construct EMC detector
   
   void Construct(G4LogicalVolume* logicbes);
   void DefineMaterial();
   
  private: 
  //your private data member (geometry, etc) here:
  
  G4LogicalVolume* logicalTwoBeamPipe;
  G4VPhysicalVolume* physicalTwoBeamPipe1;
  G4VPhysicalVolume* physicalTwoBeamPipe2;

  G4Material* Oil;
  G4Material* Au;
  G4Material* Ag;
	G4Material* Tungsten;
};
#endif





