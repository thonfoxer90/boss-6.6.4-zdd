//---------------------------------------------------------------------------//
//      BOOST --- BESIII Object_Oriented Simulation Tool                     //
//---------------------------------------------------------------------------//
//Description: Using gdml in G4
//Author: Liuhm 
//Created: May 25, 2003
//Modified:
//Comment:
//---------------------------------------------------------------------------//
//
#ifndef BesDetectorConstruction_H
#define BesDetectorConstruction_H 1

#include "G4String.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"

#include "BesMdcConstruction.hh"
#include "BesTofConstruction.hh"
#include "BesEmcConstruction.hh"
#include "BesMucConstruction.hh"
#include "BesZddConstruction.hh"
#include "BesSCM.hh"
#include "BesPip.hh"

#include "BesPipExtension.hh"
#include "BesSCQChamber.hh"
#include "BesTwoBeamPipe.hh"
#include "BesYtypeCrotch.hh"

#include "BesMagneticField.hh"
#include "SAXProcessor.hh"
#include "ProcessingConfigurator.hh"

class BesDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    BesDetectorConstruction();
    ~BesDetectorConstruction();

  public:
         G4VPhysicalVolume* Construct();
         G4LogicalVolume*   FindLogicalVolume( const G4String& vn);
         void SetPipeSCM(G4int value) {m_pipeSCM = value;}
	 void SetVisAttributes(G4String volumeName);

         void SetFullBeamPipe(G4int value) {m_fullBeamPipe = value;}
				 void SetZDDMaterial(std::string material){G4String gstr(material);m_zddMaterial=gstr;}
				 BesZddConstruction* GetBesZddConstruction(){return zdd;}
  private:
   //SAXProcessor sxp;
   //ProcessingConfigurator config;
   BesMagneticField* besField; 
   BesMdcConstruction* mdc;
   BesTofConstruction* tof;
   BesEmcConstruction* emc;
   BesMucConstruction* muc;
   BesZddConstruction* zdd;
   BesSCM* m_SCM;
   BesPip* m_Pip;

   BesPipExtension* m_PipExtension;
   BesSCQChamber* m_SCQChamber;
   BesTwoBeamPipe* m_TwoBeamPipe;
   BesYtypeCrotch* m_YtypeCrotch;

   G4int m_fullBeamPipe;

   G4int m_pipeSCM;
   G4LogicalVolume* lWorld;
   G4VPhysicalVolume* fWorld;
	 
	 G4String m_zddMaterial;
};

#endif





