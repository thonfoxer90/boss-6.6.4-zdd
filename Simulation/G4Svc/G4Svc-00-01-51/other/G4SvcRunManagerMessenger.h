#ifndef G4SvcRunManagerMessenger_H
#define G4SvcRunManagerMessenger_H

#include "G4UImessenger.hh"
#include "G4Svc/G4SvcRunManager.h"
 
class G4UIcommand;
class G4UIcmdWithAString;
class G4SvcRunManager;
 
class G4SvcRunManagerMessenger: public G4UImessenger {
private:
        G4SvcRunManager *rm;
        G4UIcommand *readXML;
public:
        G4SvcRunManagerMessenger(G4SvcRunManager *v);
        ~G4SvcRunManagerMessenger();
        void SetNewValue(G4UIcommand * command,G4String newValues);
        G4String GetCurrentValue(G4UIcommand * command);
};

#endif
