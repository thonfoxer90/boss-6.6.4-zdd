#ifndef G4Svc_CLASSDEF_H
#define G4Svc_CLASSDEF_H

#include <vector>
#include <string>

#include "CLIDSvc/CLASS_DEF.h"

#include "G4VHit.hh"
#include "G4Trajectory.hh"

CLASS_DEF(std::vector<std::string>, 10123, 1);
CLASS_DEF(std::vector<G4VHit*>, 10124, 1);
CLASS_DEF(std::vector<G4VTrajectory*>, 10125, 1);

#endif
