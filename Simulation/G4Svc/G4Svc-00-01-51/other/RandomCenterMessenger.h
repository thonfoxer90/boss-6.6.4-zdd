#ifndef RandomCenterMessenger_H
#define RandomCenterMessenger_H

#include "G4UImessenger.hh"
 
class G4UIcommand;
class G4UIcmdWithAString;
class Goofy;
 
class RandomCenterMessenger: public G4UImessenger {
private:
	Goofy *rm;
	G4UIcommand *setSeed;
	G4UIcommand *saveStatus;
	G4UIcommand *readStatus;
	G4UIcommand *setEngine;
	G4UIcommand *resetEngine;
public:
	RandomCenterMessenger(Goofy *v);
	~RandomCenterMessenger();
	void SetNewValue(G4UIcommand * command,G4String newValues);
	G4String GetCurrentValue(G4UIcommand * command);
};

#endif
