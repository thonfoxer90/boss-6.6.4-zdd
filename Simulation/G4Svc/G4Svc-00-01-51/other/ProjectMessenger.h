#ifndef ProjectMessenger_H
#define ProjectMessenger_H

#include "G4UImessenger.hh"
 
class G4UIcommand;
class G4UIcmdWithAString;
class Goofy;
 
class ProjectMessenger: public G4UImessenger {
private:
	Goofy *rm;
	G4UIcommand *make;
	G4UIcommand *create;
	G4UIcommand *del;
	G4UIcommand *geo;
	G4UIcommand *act;
public:
	ProjectMessenger(Goofy *v);
	~ProjectMessenger();
	void SetNewValue(G4UIcommand * command,G4String newValues);
	G4String GetCurrentValue(G4UIcommand * command);
};

#endif
