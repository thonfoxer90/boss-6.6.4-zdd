//------------------------------------------------------------------
//
// ClassName:   AthenaHepMCtoG4EventAction
//  
// Description: PrimaryGeneratorAction to convert HepMC event to G4Event
//
// Author:      Charles Leggett
// 
// Date:        3-8-2001
// 
// $Id: AthenaHepMCtoG4EventAction.h,v 1.1 2005/08/17 06:45:46 dengzy Exp $
// Simulation/G4Sim/G4Svc tag $Name: G4Svc-00-01-51 $
//
//------------------------------------------------------------------

#ifndef AthenaHepMCtoG4EventAction_h
#define AthenaHepMCtoG4EventAction_h

#include "G4VUserPrimaryGeneratorAction.hh"

#include "GeneratorObject/McEvent.h"

class G4Event;
namespace HepMC {
  class GenEvent;
}

class AthenaHepMCtoG4EventAction : public G4VUserPrimaryGeneratorAction
{
public:
  AthenaHepMCtoG4EventAction();
  AthenaHepMCtoG4EventAction(int);
  ~AthenaHepMCtoG4EventAction();
  
public:
  void GeneratePrimaries(G4Event* anEvent);
  //  void SetHepMCEvent(const HepMC::GenEvent *p);
  void SetHepMCEvent(McEventCol::iterator);
  
private:
  const HepMC::GenEvent *p_evt;
  McEventCol::iterator p_evtCollItr;
  int m_logLevel;
};

#endif


