#ifndef Goofy_H
#define Goofy_H

#include <string>
#include <vector>

class GoofyMessenger;
class ProjectMessenger;
class RandomCenterMessenger;

enum RunMode {Batch, Interactive};

class Goofy {
public:
	Goofy();
	Goofy(int, char**);
	~Goofy();
	static int Shell(std::string s);
private:
	RunMode rm;
	std::string runmacro;
	std::string initmacro;
	std::vector<std::string> opts;
	void Banner();
	void SetJobOptions();
	GoofyMessenger *theMessenger;
	ProjectMessenger *theProject;
	RandomCenterMessenger *theRandomCenter;
};

#endif
