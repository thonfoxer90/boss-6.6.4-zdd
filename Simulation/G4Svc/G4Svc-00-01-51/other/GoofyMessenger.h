#ifndef GoofyMessenger_H
#define GoofyMessenger_H

#include "G4UImessenger.hh"
 
class G4UIcommand;
class G4UIcmdWithAString;
class Goofy;
 
class GoofyMessenger: public G4UImessenger {
private:
        Goofy *rm;
        G4UIcommand *shell;
	G4UIcommand *echo;
	G4UIcommand *edit;
	G4UIcommand *load;
	G4UIcommand *unload;
	G4UIcommand *quit;
public:
        GoofyMessenger(Goofy *v);
        ~GoofyMessenger();
        void SetNewValue(G4UIcommand * command,G4String newValues);
        G4String GetCurrentValue(G4UIcommand * command);
};

#endif
