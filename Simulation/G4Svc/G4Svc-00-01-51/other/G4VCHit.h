#ifndef G4VCHit_h
#define G4VCHit_h 1

#include "G4VHit.hh"

class G4VCHit: public G4VHit {

public:
  inline virtual G4VHit* Clone() const { return new G4VHit; }

  virtual ~G4VCHit(){};

};

#endif
  
