#ifndef G4SVC_G4SIMEVENT_H
#define G4SVC_G4SIMEVENT_H

//------------------------------------------------------------------
//
// ClassName:   G4SimEvent
//  
// Description: Algorithm to trigger G4Svc in event Generators not used
//
// Author:      Charles Leggett
// 
// Date:        04-22-2003
// 
// $Id: G4SimEvent.h,v 1.1 2005/08/17 06:45:46 dengzy Exp $
//
//------------------------------------------------------------------

#include "GaudiKernel/Algorithm.h"

class IG4Svc;
template <class ConcreteAlgorithm> class AlgFactory;

class G4SimEvent: public Algorithm {

friend class AlgFactory<G4SimEvent>;

public:
  G4SimEvent (const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~G4SimEvent(){};

  StatusCode initialize();
  StatusCode finalize();
  StatusCode execute();

private:
  
  IG4Svc* p_G4Svc;


};

#endif

