#-- start of make_header -----------------

#====================================
#  Library G4Svc
#
<<<<<<< HEAD
#   Generated Sat May 19 14:58:39 2018  by bgarillo
=======
#   Generated Sun May 20 13:54:10 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_G4Svc_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_G4Svc_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_G4Svc

G4Svc_tag = $(tag)

#cmt_local_tagfile_G4Svc = $(G4Svc_tag)_G4Svc.make
cmt_local_tagfile_G4Svc = $(bin)$(G4Svc_tag)_G4Svc.make

else

tags      = $(tag),$(CMTEXTRATAGS)

G4Svc_tag = $(tag)

#cmt_local_tagfile_G4Svc = $(G4Svc_tag).make
cmt_local_tagfile_G4Svc = $(bin)$(G4Svc_tag).make

endif

include $(cmt_local_tagfile_G4Svc)
#-include $(cmt_local_tagfile_G4Svc)

ifdef cmt_G4Svc_has_target_tag

cmt_final_setup_G4Svc = $(bin)setup_G4Svc.make
#cmt_final_setup_G4Svc = $(bin)G4Svc_G4Svcsetup.make
cmt_local_G4Svc_makefile = $(bin)G4Svc.make

else

cmt_final_setup_G4Svc = $(bin)setup.make
#cmt_final_setup_G4Svc = $(bin)G4Svcsetup.make
cmt_local_G4Svc_makefile = $(bin)G4Svc.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)G4Svcsetup.make

#G4Svc :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'G4Svc'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = G4Svc/
#G4Svc::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

G4Svclibname   = $(bin)$(library_prefix)G4Svc$(library_suffix)
G4Svclib       = $(G4Svclibname).a
G4Svcstamp     = $(bin)G4Svc.stamp
G4Svcshstamp   = $(bin)G4Svc.shstamp

G4Svc :: dirs  G4SvcLIB
	$(echo) "G4Svc ok"

#-- end of libary_header ----------------

G4SvcLIB :: $(G4Svclib) $(G4Svcshstamp)
	@/bin/echo "------> G4Svc : library ok"

<<<<<<< HEAD
$(G4Svclib) :: $(bin)G4Svc.o $(bin)G4SvcRunManager.o $(bin)G4HepMCInterface.o $(bin)G4Svc_entries.o $(bin)G4SvcVisManager.o $(bin)G4HepMCParticle.o $(bin)G4Svc_load.o $(bin)BesHepMCInterface.o $(bin)RandomNrCenter.o
=======
$(G4Svclib) :: $(bin)BesHepMCInterface.o $(bin)G4HepMCInterface.o $(bin)G4HepMCParticle.o $(bin)G4Svc.o $(bin)G4SvcRunManager.o $(bin)G4SvcVisManager.o $(bin)G4Svc_entries.o $(bin)G4Svc_load.o $(bin)RandomNrCenter.o
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(G4Svclib) $?
	$(lib_silent) $(ranlib) $(G4Svclib)
	$(lib_silent) cat /dev/null >$(G4Svcstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(G4Svclibname).$(shlibsuffix) :: $(G4Svclib) $(G4Svcstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" G4Svc $(G4Svc_shlibflags)

$(G4Svcshstamp) :: $(G4Svclibname).$(shlibsuffix)
	@if test -f $(G4Svclibname).$(shlibsuffix) ; then cat /dev/null >$(G4Svcshstamp) ; fi

G4Svcclean ::
	$(cleanup_echo) objects
<<<<<<< HEAD
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)G4Svc.o $(bin)G4SvcRunManager.o $(bin)G4HepMCInterface.o $(bin)G4Svc_entries.o $(bin)G4SvcVisManager.o $(bin)G4HepMCParticle.o $(bin)G4Svc_load.o $(bin)BesHepMCInterface.o $(bin)RandomNrCenter.o
=======
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)BesHepMCInterface.o $(bin)G4HepMCInterface.o $(bin)G4HepMCParticle.o $(bin)G4Svc.o $(bin)G4SvcRunManager.o $(bin)G4SvcVisManager.o $(bin)G4Svc_entries.o $(bin)G4Svc_load.o $(bin)RandomNrCenter.o
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
G4Svcinstallname = $(library_prefix)G4Svc$(library_suffix).$(shlibsuffix)

G4Svc :: G4Svcinstall

install :: G4Svcinstall

G4Svcinstall :: $(install_dir)/$(G4Svcinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(G4Svcinstallname) :: $(bin)$(G4Svcinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(G4Svcinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(G4Svcinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(G4Svcinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(G4Svcinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(G4Svcinstallname) $(install_dir)/$(G4Svcinstallname); \
	      echo `pwd`/$(G4Svcinstallname) >$(install_dir)/$(G4Svcinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(G4Svcinstallname), no installation directory specified"; \
	  fi; \
	fi

G4Svcclean :: G4Svcuninstall

uninstall :: G4Svcuninstall

G4Svcuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(G4Svcinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(G4Svcinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(G4Svcinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(G4Svcinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),G4Svcclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)G4Svc_dependencies.make :: dirs

ifndef QUICK
<<<<<<< HEAD
$(bin)G4Svc_dependencies.make : $(src)G4Svc.cpp $(src)G4SvcRunManager.cpp $(src)G4HepMCInterface.cpp $(src)G4Svc_entries.cpp $(src)G4SvcVisManager.cpp $(src)G4HepMCParticle.cpp $(src)G4Svc_load.cpp $(src)BesHepMCInterface.cpp $(src)RandomNrCenter.cpp $(use_requirements) $(cmt_final_setup_G4Svc)
	$(echo) "(G4Svc.make) Rebuilding $@"; \
	  $(build_dependencies) G4Svc -all_sources -out=$@ $(src)G4Svc.cpp $(src)G4SvcRunManager.cpp $(src)G4HepMCInterface.cpp $(src)G4Svc_entries.cpp $(src)G4SvcVisManager.cpp $(src)G4HepMCParticle.cpp $(src)G4Svc_load.cpp $(src)BesHepMCInterface.cpp $(src)RandomNrCenter.cpp
=======
$(bin)G4Svc_dependencies.make : $(src)BesHepMCInterface.cpp $(src)G4HepMCInterface.cpp $(src)G4HepMCParticle.cpp $(src)G4Svc.cpp $(src)G4SvcRunManager.cpp $(src)G4SvcVisManager.cpp $(src)G4Svc_entries.cpp $(src)G4Svc_load.cpp $(src)RandomNrCenter.cpp $(use_requirements) $(cmt_final_setup_G4Svc)
	$(echo) "(G4Svc.make) Rebuilding $@"; \
	  $(build_dependencies) G4Svc -all_sources -out=$@ $(src)BesHepMCInterface.cpp $(src)G4HepMCInterface.cpp $(src)G4HepMCParticle.cpp $(src)G4Svc.cpp $(src)G4SvcRunManager.cpp $(src)G4SvcVisManager.cpp $(src)G4Svc_entries.cpp $(src)G4Svc_load.cpp $(src)RandomNrCenter.cpp
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
endif

#$(G4Svc_dependencies)

-include $(bin)G4Svc_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4Svcclean)
ifneq ($(MAKECMDGOALS),uninstall)
<<<<<<< HEAD
-include $(bin)$(binobj)G4Svc.d

$(bin)$(binobj)G4Svc.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4Svc.d : $(src)G4Svc.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4Svc.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_cppflags) $(G4Svc_cpp_cppflags)  $(src)G4Svc.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4Svc.o $(src)G4Svc.cpp $(@D)/G4Svc.dep
endif
endif

$(bin)$(binobj)G4Svc.o : $(src)G4Svc.cpp
else
$(bin)G4Svc_dependencies.make : $(G4Svc_cpp_dependencies)

$(bin)$(binobj)G4Svc.o : $(G4Svc_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4Svc.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_cppflags) $(G4Svc_cpp_cppflags)  $(src)G4Svc.cpp
=======
-include $(bin)$(binobj)BesHepMCInterface.d

$(bin)$(binobj)BesHepMCInterface.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)BesHepMCInterface.d : $(src)BesHepMCInterface.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesHepMCInterface.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(BesHepMCInterface_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(BesHepMCInterface_cppflags) $(BesHepMCInterface_cpp_cppflags)  $(src)BesHepMCInterface.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesHepMCInterface.o $(src)BesHepMCInterface.cpp $(@D)/BesHepMCInterface.dep
endif
endif

$(bin)$(binobj)BesHepMCInterface.o : $(src)BesHepMCInterface.cpp
else
$(bin)G4Svc_dependencies.make : $(BesHepMCInterface_cpp_dependencies)

$(bin)$(binobj)BesHepMCInterface.o : $(BesHepMCInterface_cpp_dependencies)
endif
	$(cpp_echo) $(src)BesHepMCInterface.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(BesHepMCInterface_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(BesHepMCInterface_cppflags) $(BesHepMCInterface_cpp_cppflags)  $(src)BesHepMCInterface.cpp
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4Svcclean)
ifneq ($(MAKECMDGOALS),uninstall)
<<<<<<< HEAD
-include $(bin)$(binobj)G4SvcRunManager.d

$(bin)$(binobj)G4SvcRunManager.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4SvcRunManager.d : $(src)G4SvcRunManager.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4SvcRunManager.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4SvcRunManager_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4SvcRunManager_cppflags) $(G4SvcRunManager_cpp_cppflags)  $(src)G4SvcRunManager.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4SvcRunManager.o $(src)G4SvcRunManager.cpp $(@D)/G4SvcRunManager.dep
endif
endif

$(bin)$(binobj)G4SvcRunManager.o : $(src)G4SvcRunManager.cpp
else
$(bin)G4Svc_dependencies.make : $(G4SvcRunManager_cpp_dependencies)

$(bin)$(binobj)G4SvcRunManager.o : $(G4SvcRunManager_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4SvcRunManager.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4SvcRunManager_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4SvcRunManager_cppflags) $(G4SvcRunManager_cpp_cppflags)  $(src)G4SvcRunManager.cpp
=======
-include $(bin)$(binobj)G4HepMCInterface.d

$(bin)$(binobj)G4HepMCInterface.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4HepMCInterface.d : $(src)G4HepMCInterface.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4HepMCInterface.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4HepMCInterface_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4HepMCInterface_cppflags) $(G4HepMCInterface_cpp_cppflags)  $(src)G4HepMCInterface.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4HepMCInterface.o $(src)G4HepMCInterface.cpp $(@D)/G4HepMCInterface.dep
endif
endif

$(bin)$(binobj)G4HepMCInterface.o : $(src)G4HepMCInterface.cpp
else
$(bin)G4Svc_dependencies.make : $(G4HepMCInterface_cpp_dependencies)

$(bin)$(binobj)G4HepMCInterface.o : $(G4HepMCInterface_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4HepMCInterface.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4HepMCInterface_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4HepMCInterface_cppflags) $(G4HepMCInterface_cpp_cppflags)  $(src)G4HepMCInterface.cpp
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4Svcclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)G4HepMCParticle.d

$(bin)$(binobj)G4HepMCParticle.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4HepMCParticle.d : $(src)G4HepMCParticle.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4HepMCParticle.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4HepMCParticle_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4HepMCParticle_cppflags) $(G4HepMCParticle_cpp_cppflags)  $(src)G4HepMCParticle.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4HepMCParticle.o $(src)G4HepMCParticle.cpp $(@D)/G4HepMCParticle.dep
endif
endif

$(bin)$(binobj)G4HepMCParticle.o : $(src)G4HepMCParticle.cpp
else
$(bin)G4Svc_dependencies.make : $(G4HepMCParticle_cpp_dependencies)

$(bin)$(binobj)G4HepMCParticle.o : $(G4HepMCParticle_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4HepMCParticle.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4HepMCParticle_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4HepMCParticle_cppflags) $(G4HepMCParticle_cpp_cppflags)  $(src)G4HepMCParticle.cpp

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4Svcclean)
ifneq ($(MAKECMDGOALS),uninstall)
<<<<<<< HEAD
-include $(bin)$(binobj)G4Svc_entries.d

$(bin)$(binobj)G4Svc_entries.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4Svc_entries.d : $(src)G4Svc_entries.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4Svc_entries.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_entries_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_entries_cppflags) $(G4Svc_entries_cpp_cppflags)  $(src)G4Svc_entries.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4Svc_entries.o $(src)G4Svc_entries.cpp $(@D)/G4Svc_entries.dep
endif
endif

$(bin)$(binobj)G4Svc_entries.o : $(src)G4Svc_entries.cpp
else
$(bin)G4Svc_dependencies.make : $(G4Svc_entries_cpp_dependencies)

$(bin)$(binobj)G4Svc_entries.o : $(G4Svc_entries_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4Svc_entries.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_entries_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_entries_cppflags) $(G4Svc_entries_cpp_cppflags)  $(src)G4Svc_entries.cpp
=======
-include $(bin)$(binobj)G4Svc.d

$(bin)$(binobj)G4Svc.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4Svc.d : $(src)G4Svc.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4Svc.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_cppflags) $(G4Svc_cpp_cppflags)  $(src)G4Svc.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4Svc.o $(src)G4Svc.cpp $(@D)/G4Svc.dep
endif
endif

$(bin)$(binobj)G4Svc.o : $(src)G4Svc.cpp
else
$(bin)G4Svc_dependencies.make : $(G4Svc_cpp_dependencies)

$(bin)$(binobj)G4Svc.o : $(G4Svc_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4Svc.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_cppflags) $(G4Svc_cpp_cppflags)  $(src)G4Svc.cpp
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4Svcclean)
ifneq ($(MAKECMDGOALS),uninstall)
<<<<<<< HEAD
-include $(bin)$(binobj)G4SvcVisManager.d

$(bin)$(binobj)G4SvcVisManager.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4SvcVisManager.d : $(src)G4SvcVisManager.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4SvcVisManager.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4SvcVisManager_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4SvcVisManager_cppflags) $(G4SvcVisManager_cpp_cppflags)  $(src)G4SvcVisManager.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4SvcVisManager.o $(src)G4SvcVisManager.cpp $(@D)/G4SvcVisManager.dep
endif
endif

$(bin)$(binobj)G4SvcVisManager.o : $(src)G4SvcVisManager.cpp
else
$(bin)G4Svc_dependencies.make : $(G4SvcVisManager_cpp_dependencies)

$(bin)$(binobj)G4SvcVisManager.o : $(G4SvcVisManager_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4SvcVisManager.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4SvcVisManager_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4SvcVisManager_cppflags) $(G4SvcVisManager_cpp_cppflags)  $(src)G4SvcVisManager.cpp
=======
-include $(bin)$(binobj)G4SvcRunManager.d

$(bin)$(binobj)G4SvcRunManager.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4SvcRunManager.d : $(src)G4SvcRunManager.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4SvcRunManager.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4SvcRunManager_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4SvcRunManager_cppflags) $(G4SvcRunManager_cpp_cppflags)  $(src)G4SvcRunManager.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4SvcRunManager.o $(src)G4SvcRunManager.cpp $(@D)/G4SvcRunManager.dep
endif
endif

$(bin)$(binobj)G4SvcRunManager.o : $(src)G4SvcRunManager.cpp
else
$(bin)G4Svc_dependencies.make : $(G4SvcRunManager_cpp_dependencies)

$(bin)$(binobj)G4SvcRunManager.o : $(G4SvcRunManager_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4SvcRunManager.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4SvcRunManager_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4SvcRunManager_cppflags) $(G4SvcRunManager_cpp_cppflags)  $(src)G4SvcRunManager.cpp
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4Svcclean)
ifneq ($(MAKECMDGOALS),uninstall)
<<<<<<< HEAD
-include $(bin)$(binobj)G4HepMCParticle.d

$(bin)$(binobj)G4HepMCParticle.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4HepMCParticle.d : $(src)G4HepMCParticle.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4HepMCParticle.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4HepMCParticle_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4HepMCParticle_cppflags) $(G4HepMCParticle_cpp_cppflags)  $(src)G4HepMCParticle.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4HepMCParticle.o $(src)G4HepMCParticle.cpp $(@D)/G4HepMCParticle.dep
endif
endif

$(bin)$(binobj)G4HepMCParticle.o : $(src)G4HepMCParticle.cpp
else
$(bin)G4Svc_dependencies.make : $(G4HepMCParticle_cpp_dependencies)

$(bin)$(binobj)G4HepMCParticle.o : $(G4HepMCParticle_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4HepMCParticle.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4HepMCParticle_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4HepMCParticle_cppflags) $(G4HepMCParticle_cpp_cppflags)  $(src)G4HepMCParticle.cpp
=======
-include $(bin)$(binobj)G4SvcVisManager.d

$(bin)$(binobj)G4SvcVisManager.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4SvcVisManager.d : $(src)G4SvcVisManager.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4SvcVisManager.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4SvcVisManager_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4SvcVisManager_cppflags) $(G4SvcVisManager_cpp_cppflags)  $(src)G4SvcVisManager.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4SvcVisManager.o $(src)G4SvcVisManager.cpp $(@D)/G4SvcVisManager.dep
endif
endif

$(bin)$(binobj)G4SvcVisManager.o : $(src)G4SvcVisManager.cpp
else
$(bin)G4Svc_dependencies.make : $(G4SvcVisManager_cpp_dependencies)

$(bin)$(binobj)G4SvcVisManager.o : $(G4SvcVisManager_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4SvcVisManager.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4SvcVisManager_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4SvcVisManager_cppflags) $(G4SvcVisManager_cpp_cppflags)  $(src)G4SvcVisManager.cpp
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4Svcclean)
ifneq ($(MAKECMDGOALS),uninstall)
<<<<<<< HEAD
-include $(bin)$(binobj)G4Svc_load.d

$(bin)$(binobj)G4Svc_load.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4Svc_load.d : $(src)G4Svc_load.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4Svc_load.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_load_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_load_cppflags) $(G4Svc_load_cpp_cppflags)  $(src)G4Svc_load.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4Svc_load.o $(src)G4Svc_load.cpp $(@D)/G4Svc_load.dep
endif
endif

$(bin)$(binobj)G4Svc_load.o : $(src)G4Svc_load.cpp
else
$(bin)G4Svc_dependencies.make : $(G4Svc_load_cpp_dependencies)

$(bin)$(binobj)G4Svc_load.o : $(G4Svc_load_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4Svc_load.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_load_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_load_cppflags) $(G4Svc_load_cpp_cppflags)  $(src)G4Svc_load.cpp
=======
-include $(bin)$(binobj)G4Svc_entries.d

$(bin)$(binobj)G4Svc_entries.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4Svc_entries.d : $(src)G4Svc_entries.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4Svc_entries.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_entries_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_entries_cppflags) $(G4Svc_entries_cpp_cppflags)  $(src)G4Svc_entries.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4Svc_entries.o $(src)G4Svc_entries.cpp $(@D)/G4Svc_entries.dep
endif
endif

$(bin)$(binobj)G4Svc_entries.o : $(src)G4Svc_entries.cpp
else
$(bin)G4Svc_dependencies.make : $(G4Svc_entries_cpp_dependencies)

$(bin)$(binobj)G4Svc_entries.o : $(G4Svc_entries_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4Svc_entries.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_entries_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_entries_cppflags) $(G4Svc_entries_cpp_cppflags)  $(src)G4Svc_entries.cpp
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4Svcclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)G4Svc_load.d

$(bin)$(binobj)G4Svc_load.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)G4Svc_load.d : $(src)G4Svc_load.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/G4Svc_load.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_load_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_load_cppflags) $(G4Svc_load_cpp_cppflags)  $(src)G4Svc_load.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/G4Svc_load.o $(src)G4Svc_load.cpp $(@D)/G4Svc_load.dep
endif
endif

$(bin)$(binobj)G4Svc_load.o : $(src)G4Svc_load.cpp
else
$(bin)G4Svc_dependencies.make : $(G4Svc_load_cpp_dependencies)

$(bin)$(binobj)G4Svc_load.o : $(G4Svc_load_cpp_dependencies)
endif
	$(cpp_echo) $(src)G4Svc_load.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(G4Svc_load_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(G4Svc_load_cppflags) $(G4Svc_load_cpp_cppflags)  $(src)G4Svc_load.cpp

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4Svcclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)RandomNrCenter.d

$(bin)$(binobj)RandomNrCenter.d : $(use_requirements) $(cmt_final_setup_G4Svc)

$(bin)$(binobj)RandomNrCenter.d : $(src)RandomNrCenter.cpp
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/RandomNrCenter.dep $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(RandomNrCenter_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(RandomNrCenter_cppflags) $(RandomNrCenter_cpp_cppflags)  $(src)RandomNrCenter.cpp
	$(cpp_silent) $(format_dependencies) $@ $(@D)/RandomNrCenter.o $(src)RandomNrCenter.cpp $(@D)/RandomNrCenter.dep
endif
endif

$(bin)$(binobj)RandomNrCenter.o : $(src)RandomNrCenter.cpp
else
$(bin)G4Svc_dependencies.make : $(RandomNrCenter_cpp_dependencies)

$(bin)$(binobj)RandomNrCenter.o : $(RandomNrCenter_cpp_dependencies)
endif
	$(cpp_echo) $(src)RandomNrCenter.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4Svc_pp_cppflags) $(lib_G4Svc_pp_cppflags) $(RandomNrCenter_pp_cppflags) $(use_cppflags) $(G4Svc_cppflags) $(lib_G4Svc_cppflags) $(RandomNrCenter_cppflags) $(RandomNrCenter_cpp_cppflags)  $(src)RandomNrCenter.cpp

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: G4Svcclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(G4Svc.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(G4Svc.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(G4Svc.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(G4Svc.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_G4Svc)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(G4Svc.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(G4Svc.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(G4Svc.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

G4Svcclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library G4Svc
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)G4Svc$(library_suffix).a $(library_prefix)G4Svc$(library_suffix).s? G4Svc.stamp G4Svc.shstamp
#-- end of cleanup_library ---------------
