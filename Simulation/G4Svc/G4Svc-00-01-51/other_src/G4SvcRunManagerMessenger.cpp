#include "G4Svc/G4SvcRunManagerMessenger.h"
#include "G4Svc/G4SvcRunManager.h"

#include "G4UIcommand.hh"

G4SvcRunManagerMessenger::G4SvcRunManagerMessenger(G4SvcRunManager *v)
{
	rm=v;

	readXML=new G4UIcommand("/control/ReadXML",this);
	readXML->SetGuidance("Read in an XML file");
	G4UIparameter* parameter;
	G4bool omitable;
	parameter = new G4UIparameter ("FileName", 's', omitable = false);
	readXML->SetParameter(parameter);
}

G4SvcRunManagerMessenger::~G4SvcRunManagerMessenger()
{
	delete readXML;
}

// #include "FadsXMLParser/XMLReader.hh" bes hack

void G4SvcRunManagerMessenger::SetNewValue(G4UIcommand * command,G4String newValues)
{
	if (command==readXML)
	{
	  // XMLReader *temp=XMLReader::GetXMLReader(); bes hack
	  //	temp->Process(newValues); bes hack
	}
}

G4String G4SvcRunManagerMessenger::GetCurrentValue(G4UIcommand * command)
{
	G4String s="Undefined";
	return s;
}
