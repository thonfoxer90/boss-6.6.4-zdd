#include "G4Svc/G4SimEvent.h"
#include "G4Svc/IG4Svc.h"

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/AlgFactory.h"

/////////////////////////////////////////////////////////////////////////////
G4SimEvent::G4SimEvent(const std::string& name, ISvcLocator* pSvcLocator) :
  Algorithm(name, pSvcLocator)
{
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

StatusCode G4SimEvent::initialize() {

  MsgStream log(msgSvc(), name());
  StatusCode status = service("G4Svc",p_G4Svc);

  if (status.isSuccess()) {
    log << MSG::DEBUG << "got the G4Svc" << endreq;
  } else {
    log << MSG::ERROR << "could not get the G4Svc" << endreq;
  }

  return status;

}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

StatusCode G4SimEvent::finalize() {

  return StatusCode::SUCCESS;

}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

StatusCode G4SimEvent::execute() {

  MsgStream log(msgSvc(), name());
  log << MSG::VERBOSE << "Calling SimulateG4Event" << endreq;

  p_G4Svc->SimulateG4Event();

  return StatusCode::SUCCESS;

}

