#include "G4Svc/RandomCenterMessenger.h"
#include "G4Svc/Goofy.h"

#include "G4UIcommand.hh"
#include "G4UImanager.hh"

#include <string>
#include <fstream.h>

#include "G4Svc/RandomNrCenter.h"


RandomCenterMessenger::RandomCenterMessenger(Goofy *v)
{
	rm=v;

	setSeed=new G4UIcommand("/Random/SetSeed",this);
	setSeed->SetGuidance("Sets the seed to the current generator");
	G4UIparameter* parameter;
	G4bool omitable;
	parameter = new G4UIparameter ("Random", 'i', omitable = false);
	setSeed->SetParameter(parameter);
	
	saveStatus=new G4UIcommand("/Random/SaveStatus",this);
	saveStatus->SetGuidance("Saves the status of the current generator");
	G4UIparameter* filename;
	filename=new G4UIparameter ("Random", 's', omitable = false);
	saveStatus->SetParameter(filename);
	
	readStatus=new G4UIcommand("/Random/RetrieveStatus",this);
	readStatus->SetGuidance("Retrieves the status for the current generator");
	filename=new G4UIparameter ("Random", 's', omitable = false);
	readStatus->SetParameter(filename);
	
	setEngine=new G4UIcommand("/Random/SetEngine",this);
	setEngine->SetGuidance("Allows to choose a new Random Number Engine");
	
	resetEngine=new G4UIcommand("/Random/ResetEngine",this);
	resetEngine->SetGuidance("Resets the engine to its default");
	
}

RandomCenterMessenger::~RandomCenterMessenger()
{
	delete setSeed;
	delete saveStatus;
	delete readStatus;
	delete setEngine;
	delete resetEngine;
}

void RandomCenterMessenger::SetNewValue(G4UIcommand * command,G4String newValues)
{
	if (command==setSeed)
	{
		long int i;
		const char* s = newValues;
  		std::istrstream is ((char*)s);
		is>>i;
		RandomNrCenter rc;
		rc.SetEngineSeed(i);
	}
	else if (command==saveStatus)
	{
		RandomNrCenter rc;
		rc.SaveEngineStatus(newValues);
	}
	else if (command==readStatus)
	{
		RandomNrCenter rc;
		rc.RestoreEngineStatus(newValues);
	}
	else if (command==setEngine)
	{
		RandomNrCenter rc;
		rc.SetEngine();
	}
	else if (command==resetEngine)
	{
		
	}
}

G4String RandomCenterMessenger::GetCurrentValue(G4UIcommand * command)
{
	G4String s="Undefined";
	return s;
}
