#include "globals.hh"
#include "G4Svc/Goofy.h"
#include "G4Svc/GoofyMessenger.h"
#include "G4Svc/ProjectMessenger.h"
#include "G4Svc/RandomCenterMessenger.h"
#include <cstdlib>
#
#include "G4UImanager.hh"
#include "G4UIterminal.hh"


Goofy::Goofy()
{

	Banner();	
	
	theMessenger=new GoofyMessenger(this);
	theProject=new ProjectMessenger(this);
	theRandomCenter=new RandomCenterMessenger(this);
	
}

Goofy::Goofy(int npar, char* pars[])
{

	Banner();	

	if (npar>1) for (int i=1;i<npar;i++) opts.push_back(pars[i]);	
	SetJobOptions();
	
	// Construct the default run manager
	
	theMessenger=new GoofyMessenger(this);
	theProject=new ProjectMessenger(this);
	theRandomCenter=new RandomCenterMessenger(this);
	
  	G4UImanager * UI = G4UImanager::GetUIpointer(); 
	if (!initmacro.empty()) UI->ApplyCommand("/control/execute "+initmacro);
	
	else if (rm==Batch)
	{
	
	// check that a macro has been passed along
		if (runmacro.empty())
		{
			std::cout<<" Batch mode selected but no macro file "
				<<"to run; Aborting!!!!"<<std::endl;
			abort();
		}
		UI->ApplyCommand("/control/execute "+runmacro);
		exit(0);
	}
}

Goofy::~Goofy()
{
	delete theMessenger;
	delete theProject;
	delete theRandomCenter;
}

int Goofy::Shell(std::string s)
{
	return system(s.c_str());
}

void Goofy::Banner()
{
	std::cout<<std::endl<<std::endl<<
	"***********************************************************************"<<std::endl<<
	"***********************************************************************"<<std::endl<<	
	"**								      **"<<std::endl<<
	"**	 GGGGG      OOOOO      OOOOO    FFFFFFF    Y     Y            **"<<std::endl<<
	"**	G     G    O     O    O     O   F          Y     Y            **"<<std::endl<<
	"**	G	   O     O    O     O   F           Y   Y             **"<<std::endl<<
	"**	G	   O     O    O     O   FFFFF        YYY              **"<<std::endl<<
	"**	G   GGGG   O	 O    O     O   F             Y               **"<<std::endl<<
	"**	G     G    O     O    O     O   F             Y               **"<<std::endl<<
	"**	 GGGGG	    OOOOO      OOOOO    F             Y               **"<<std::endl<<
	"** 								      **"<<std::endl<<
	"** 								      **"<<std::endl<<
	"**	      The Geant4-based Object Oriented FollY		      **"<<std::endl<<
	"**	      							      **"<<std::endl<<
	"**	      					Jan 10th, 2000        **"<<std::endl<<
	"**								      **"<<std::endl<<
	"***********************************************************************"<<std::endl<<
	"***********************************************************************"<<std::endl<<	
	std::endl<<std::endl<<std::endl;
	
	char *greetings=getenv("GOOFY_GREETINGS");
	
	if (greetings)
	Shell("xv -wait 2 $GOOFY_GREETINGS");
}


void Goofy::SetJobOptions()
{
	rm=Interactive;
	std::vector<std::string>::const_iterator it;
	
	it=find(opts.begin(),opts.end(),"-b");
	if (it!=opts.end()) rm=Batch;
	
	it=find(opts.begin(),opts.end(),"-i");
	if (it!=opts.end())
	{
		it++;
		initmacro=*it;
	}
	
	it=find(opts.begin(),opts.end(),"-m");
	if (it!=opts.end())
	{
		it++;
		runmacro=*it;
	}
}
