#include "G4Svc/GoofyMessenger.h"
#include "G4Svc/Goofy.h"

#include "G4UIcommand.hh"
#include "G4UImanager.hh"

#include "FadsPackageLoader/PackageLoader.h"

GoofyMessenger::GoofyMessenger(Goofy *v)
{
	rm=v;

	shell=new G4UIcommand("/shell",this);
	shell->SetGuidance("Invokes a shell command");
	G4UIparameter* parameter;
	G4bool omitable;
	parameter = new G4UIparameter ("Command", 's', omitable = false);
	shell->SetParameter(parameter);

	echo=new G4UIcommand("/echo",this);
	echo->SetGuidance("Echoes a string");
	echo->SetParameter(parameter);
	
	edit=new G4UIcommand("/edit",this);
	edit->SetGuidance("Invokes the chosen editor");
	parameter = new G4UIparameter ("File to be edited", 's', omitable = false);
	edit->SetParameter(parameter);

	quit=new G4UIcommand("/quit",this);
	quit->SetGuidance("quits the application");
	
	load=new G4UIcommand("/load",this);
	load->SetGuidance("Dynamically loads a shared library");
	parameter = new G4UIparameter ("Library to be loaded", 's', omitable = false);
	load->SetParameter(parameter);
	
	unload=new G4UIcommand("/unload",this);
	unload->SetGuidance("Drops a shared library");
	parameter = new G4UIparameter ("Library to be dropped", 's', omitable = false);
	unload->SetParameter(parameter);
}

GoofyMessenger::~GoofyMessenger()
{
	delete shell;
	delete echo;
	delete edit;
	delete load;
	delete unload;
	delete quit;
}

void GoofyMessenger::SetNewValue(G4UIcommand * command,G4String newValues)
{
	if (command==shell)
	{
		std::string temp(newValues);
		unsigned int i=temp.find_first_not_of ('"');
		if (i != std::string::npos)
		{
			if (i>0) temp = temp.substr (i);
			i = temp.find_last_not_of ('"');
			if (i < temp.size ()) temp = temp.substr (0, i + 1);
		}
		i=Goofy::Shell(temp);
	}
	else if (command==echo)
	{
		std::string temp(newValues);
		unsigned int i=temp.find_first_not_of ('"');
		if (i != std::string::npos)
		{
			if (i>0) temp = temp.substr (i);
			i = temp.find_last_not_of ('"');
			if (i < temp.size ()) temp = temp.substr (0, i + 1);
		}
		i=Goofy::Shell("echo "+temp);
	}
	else if (command==quit)
	{
		G4UImanager *man=G4UImanager::GetUIpointer();
		man->ApplyCommand("/exit");
	}
	else if (command==edit)
	{
		std::string s="$GOOFY_EDITOR "+newValues+" &";
		Goofy::Shell(s);
	}
	else if (command==load)
	{
		PackageLoader a(newValues.c_str());
	}
	else if (command==unload)
	{
		bool test;
		PackageLoader a;
		test=a.unload(newValues);
	}
}

G4String GoofyMessenger::GetCurrentValue(G4UIcommand * command)
{
	G4String s="Undefined";
	return s;
}
