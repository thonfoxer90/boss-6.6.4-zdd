#include "G4Svc/ProjectMessenger.h"
#include "G4Svc/Goofy.h"

#include "G4UIcommand.hh"
#include "G4UImanager.hh"

#include "FadsPackageLoader/PackageLoader.h"

#include <string>
#include <fstream.h>

std::string getLine(ifstream& in)
{
	char buffer[200];
	if (in.getline(buffer,200))
	{
		std::string s=buffer;
		return s;
	}
	else return "eof";
}

void parseTemplate(std::string dir,std::string filename, std::string dname)
{
	std::string outname=dname;
	std::string outfile=dir+outname+".temp";
	ofstream out(outfile.c_str());
	ifstream in((dir+filename).c_str());

	unsigned int i=0;
	std::string buffer;
	while ((buffer=getLine(in))!="eof")
	{
		while ((i=buffer.find("@NAME"))!=std::string::npos)
		{
			if (i>0) buffer.replace(i,5,outname);
		}
		out<<buffer<<std::endl;
	}
} 

ProjectMessenger::ProjectMessenger(Goofy *v)
{
	rm=v;

	make=new G4UIcommand("/Project/make",this);
	make->SetGuidance("Builds the specified project");
	G4UIparameter* parameter;
	G4bool omitable;
	parameter = new G4UIparameter ("Project", 's', omitable = false);
	make->SetParameter(parameter);
	
	create=new G4UIcommand("/Project/create",this);
	create->SetGuidance("Creates an empty project");
	parameter = new G4UIparameter ("Project", 's', omitable = false);
	create->SetParameter(parameter);
	
	del=new G4UIcommand("/Project/delete",this);
	del->SetGuidance("Deletes a project");
	parameter = new G4UIparameter ("Project", 's', omitable = false);
	del->SetParameter(parameter);
	
	geo=new G4UIcommand("/Project/add/DetectorFacility",this);
	geo->SetGuidance("Generates sample code for a detector facility in a project");
	parameter = new G4UIparameter ("Project", 's', omitable = false);
	geo->SetParameter(parameter);
	
	G4UIparameter* parameter2;
	parameter2 = new G4UIparameter ("DetectorFacilityName", 's', omitable = false);
	geo->SetParameter(parameter2);

	act=new G4UIcommand("/Project/add/UserAction",this);
	act->SetGuidance("Generates sample code for an user action in a project");
	parameter = new G4UIparameter ("Project", 's', omitable = false);
	act->SetParameter(parameter);
	
	G4UIparameter* parameter3;
	parameter3 = new G4UIparameter ("UserAction Name", 's', omitable = false);
	act->SetParameter(parameter3);
}

ProjectMessenger::~ProjectMessenger()
{
	// delete make;
	// delete create;
	// delete del;
	// delete geo;
	// delete act;
}

void ProjectMessenger::SetNewValue(G4UIcommand * command,G4String newValues)
{
	if (command==make)
	{
		Goofy::Shell("cd "+newValues+"; gmake");
	}
	else if (command==create)
	{
		Goofy::Shell("mkdir "+newValues);
		Goofy::Shell("mkdir "+newValues+"/src");
		Goofy::Shell("mkdir "+newValues+"/include");
		Goofy::Shell("touch "+newValues+"/src/dummy.cc");
		std::cout<<"Looking for "<<std::flush;
		if (Goofy::Shell("find $FADS_CURRENT/config -name GNUmakefile_template")==0)
		{
			std::cout<<" Found! "<<std::endl;
			Goofy::Shell("cp $FADS_CURRENT/config/GNUmakefile_template "
						+newValues+"/GNUmakefile");
		}
		Goofy::Shell("chmod -R 755 "+newValues);
	}
	else if (command==del)
	{
		if (Goofy::Shell("find . -name "+newValues))
			Goofy::Shell("rm -rf "+newValues); 
	}
	else if (command==geo)
	{
		G4String detName,project;
		const char* s = newValues;
  		std::istrstream is ((char*)s);
		is>>project>>detName;
		
		if (Goofy::Shell("find . -name "+project)==0)
		{
			std::string where="$FADS_CURRENT/config/";
			std::string incl=project+"/include/";
			std::string src=project+"/src/";
			Goofy::Shell("cp "+where+"DetectorFacilityTemplate.hh "
					+incl);
			parseTemplate(incl,"DetectorFacilityTemplate.hh",detName);
			Goofy::Shell("mv "+incl+detName+".temp "+incl+
						+detName+"Facility.hh");
			Goofy::Shell("rm -f "+incl+"DetectorFacilityTemplate.hh");
			Goofy::Shell("cp "+where+"DetectorFacilityTemplate.cc "
						+src);
			parseTemplate(src,"DetectorFacilityTemplate.cc",detName);
			Goofy::Shell("mv "+src+detName+".temp "+src+
						+detName+"Facility.cc");
			Goofy::Shell("rm -f "+src+"DetectorFacilityTemplate.cc");
		}
	}
	else if (command==act)
	{
		G4String actName,project;
		const char* s = newValues;
  		std::istrstream is ((char*)s);
		is>>project>>actName;
		
		if (Goofy::Shell("find . -name "+project)==0)
		{
			std::string where="$FADS_CURRENT/config/";
			std::string incl=project+"/include/";
			std::string src=project+"/src/";
			Goofy::Shell("cp "+where+"UserActionTemplate.hh "
					+incl);
			parseTemplate(incl,"UserActionTemplate.hh",actName);
			Goofy::Shell("mv "+incl+actName+".temp "+incl+
						+actName+"Action.hh");
			Goofy::Shell("rm -f "+incl+"UserActionTemplate.hh");
			Goofy::Shell("cp "+where+"UserActionTemplate.cc "
						+src);
			parseTemplate(src,"UserActionTemplate.cc",actName);
			Goofy::Shell("mv "+src+actName+".temp "+src+
						+actName+"Action.cc");
			Goofy::Shell("rm -f "+src+"UserActionTemplate.cc");
		}
	}
}

G4String ProjectMessenger::GetCurrentValue(G4UIcommand * command)
{
	G4String s="Undefined";
	return s;
}
