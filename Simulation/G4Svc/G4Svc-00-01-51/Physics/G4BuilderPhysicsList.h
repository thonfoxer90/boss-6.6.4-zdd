/***************************************************************************
 Set the Physics Processes
 -------------------------
 ATLAS Collaboration
 ***************************************************************************/

// $Id: G4BuilderPhysicsList.h,v 1.1.1.1 2004/09/28 05:16:53 liwd Exp $


#ifndef G4BUILDER_PHYSICSLIST_H
#define G4BUILDER_PHYSICSLIST_H

#include "G4VUserPhysicsList.hh"
#include "globals.hh"

class G4BuilderPhysicsList: public G4VUserPhysicsList
{
  public:
    G4BuilderPhysicsList();
    ~G4BuilderPhysicsList();

  protected:
    // Construct particle and physics process
    void ConstructParticle();
    void ConstructProcess();
    void SetCuts();

};

#endif // G4BUILDER_PHYSICSLIST_H







