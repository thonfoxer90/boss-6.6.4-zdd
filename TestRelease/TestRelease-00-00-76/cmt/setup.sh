# echo "Setting TestRelease TestRelease-00-00-76 in /home/bgarillo/boss-6.6.4-ZDD"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cluster/him/bes3/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh

tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=TestRelease -version=TestRelease-00-00-76 -path=/home/bgarillo/boss-6.6.4-ZDD  -no_cleanup $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

