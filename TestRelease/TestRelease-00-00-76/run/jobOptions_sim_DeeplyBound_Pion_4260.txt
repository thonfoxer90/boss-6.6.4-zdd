//DENG Zi-yan 2008-03-17

#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"

//*************job options for generator (KKMC)***************
#include "$KKMCROOT/share/jobOptions_KKMC.txt"
//#include "jobOptions_KKMC.txt"
KKMC.CMSEnergy = 4.260;
KKMC.BeamEnergySpread=0.00134;
KKMC.NumberOfEventPrinted=1;
KKMC.GeneratePsi4260=true;
KKMC.ParticleDecayThroughEvtGen=true;
KKMC.RadiationCorrection = false;
//KKMC.ThresholdCut=4.223;

//*************job options for EvtGen***************
#include "$BESEVTGENROOT/share/BesEvtGen.txt"
EvtDecay.userDecayTableName = "$TESTRELEASEROOT/run/DeeplyBound_Pion.dec";
EvtDecay.PdtTableDir = "$TESTRELEASEROOT/run/pdt.table";

//**************job options for detector simulation******************
#include "$BESSIMROOT/share/G4Svc_BesSim.txt"

//configure for calibration constants
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

// run ID
RealizationSvc.RunIdList = {-31561};

#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"


// OUTPUT PRINTOUT LEVEL
// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 50;

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 3501;
RootCnvSvc.digiRootOutputFile = "~/wa/files/DeeplyBound/Pion/4260_3501_50.rtraw";

