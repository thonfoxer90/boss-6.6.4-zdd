#!/bin/sh

start=$1
end=$2
info=$3


Nevents=1000
#Nevents=10

scriptdir=$PWD/
outdir=/data/work/kphpbb/bgarillo/cZDD/Bhlumi/
#outdir=/home/bgarillo/boss-6.6.4-ZDD/TestRelease/TestRelease-00-00-76/run/Bhlumi/brice/Jobs/

for seed in `seq $start $end`;
do
	X=${info}_${seed}

	rm jobOptions_sim_Bhlumi_$X.txt jobOptions_rec_Bhlumi_$X.txt jobOptions_ana_Bhlumi_$X.txt SubScript_$X
	#Write job option file for simulation
	echo '#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"' >> jobOptions_sim_Bhlumi_$X.txt
	echo 'ApplicationMgr.DLLs += {"Bhlumi","BesServices", "RootCnvSvc" };' >> jobOptions_sim_Bhlumi_$X.txt
	echo 'ApplicationMgr.TopAlg += {"Bhlumi"};' >> jobOptions_sim_Bhlumi_$X.txt
	echo >> jobOptions_sim_Bhlumi_$X.txt
	echo 'Bhlumi.CMEnergy=3.773;' >> jobOptions_sim_Bhlumi_$X.txt
	echo 'Bhlumi.AngleMode=1;' >> jobOptions_sim_Bhlumi_$X.txt 
	#0: rad; 1: degree; 2: cos(theta);
	#echo 'Bhlumi.MinThetaAngle=0.08;' >> jobOptions_sim_Bhlumi_$X.txt
	#echo 'Bhlumi.MaxThetaAngle=179.92;' >> jobOptions_sim_Bhlumi_$X.txt
	
	echo 'Bhlumi.MinThetaAngle=0.08;' >> jobOptions_sim_Bhlumi_$X.txt
	
	#echo 'Bhlumi.MinThetaAngle=0.001;' >> jobOptions_sim_Bhlumi_$X.txt
	#echo 'Bhlumi.MinThetaAngle=0.;' >> jobOptions_sim_Bhlumi_$X.txt
	echo 'Bhlumi.MaxThetaAngle=5.;' >> jobOptions_sim_Bhlumi_$X.txt
	echo 'Bhlumi.SoftPhotonCut=1E-4;' >> jobOptions_sim_Bhlumi_$X.txt
	
	#echo 'Bhlumi.SoftPhotonCut=1E-2;' >> jobOptions_sim_Bhlumi_$X.txt
	
	echo >> jobOptions_sim_Bhlumi_$X.txt
	echo 'BesRndmGenSvc.RndmSeed = '"$seed"';' >> jobOptions_sim_Bhlumi_$X.txt
	echo >> jobOptions_sim_Bhlumi_$X.txt
	echo '#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"' >> jobOptions_sim_Bhlumi_$X.txt
	
	#echo 'BesSim.FullBeamPipe = 0;' >> jobOptions_sim_Bhlumi_$X.txt
	
	echo 'BesSim.FullBeamPipe = 1;' >> jobOptions_sim_Bhlumi_$X.txt
	
	##WARNING ; AT LEAST MDC, EMC AND TOF MUST BE SIMULATED OTHERWISE SIMULATION CRASHES
	#echo 'BesSim.Mdc = 0;' >> jobOptions_sim_Bhlumi_$X.txt
	#echo 'BesSim.Emc = 0;' >> jobOptions_sim_Bhlumi_$X.txt
	#echo 'BesSim.Muc = 0;' >> jobOptions_sim_Bhlumi_$X.txt
	#echo 'BesSim.Tof = 0;' >> jobOptions_sim_Bhlumi_$X.txt
	#echo 'BesSim.PipeSCM = 0;' >> jobOptions_sim_Bhlumi_$X.txt
	
	echo 'G4Svc.BoostLab = true;' >> jobOptions_sim_Bhlumi_$X.txt
	
	#echo 'G4Svc.BoostLab = false;' >> jobOptions_sim_Bhlumi_$X.txt
	
	echo '#include "$CALIBSVCROOT/share/calibConfig_sim.txt"' >> jobOptions_sim_Bhlumi_$X.txt
	echo 'RealizationSvc.RunIdList = {-20448};' >> jobOptions_sim_Bhlumi_$X.txt
	echo '#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"' >> jobOptions_sim_Bhlumi_$X.txt
	echo 'RootCnvSvc.digiRootOutputFile = "'"${outdir}"'rtraw/Bhlumi_sim_'"$X"'.rtraw";' >> jobOptions_sim_Bhlumi_$X.txt
	
#	echo 'MagneticFieldSvc.TurnOffField = true;' >> jobOptions_sim_Bhlumi_$X.txt
	echo 'MagneticFieldSvc.TurnOffISPB = false;' >> jobOptions_sim_Bhlumi_$X.txt
	
	echo 'MessageSvc.OutputLevel  = 5;' >> jobOptions_sim_Bhlumi_$X.txt
	echo "ApplicationMgr.EvtMax = $Nevents;" >> jobOptions_sim_Bhlumi_$X.txt
	
	#Write job option file for reconstruction
	echo '#include "$TESTRELEASEROOT/run/jobOptions_ReadRoot.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$CALIBSVCROOT/share/job-CalibData.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	#echo 'MixerAlg.ReplaceDataPath = "/data/group/bes3/bes3data/offline/data/fltrandomtrg/";' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$TRKEXTALGROOT/share/TrkExtAlgOption.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$TOFRECROOT/share/jobOptions_TofRec.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$TOFENERGYRECROOT/share/TofEnergyRecOptions_MC.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$EMCRECROOT/share/EmcRecOptions.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$MUCRECALGROOT/share/jobOptions_MucRec.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$PRIMARYVERTEXALGROOT/share/jobOptions_kalman.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$VEEVERTEXALGROOT/share/jobOptions_veeVertex.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$EVENTNAVIGATORROOT/share/EventNavigator.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$HLTMAKERALGROOT/share/jobOptions_HltMakerAlg.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$TESTRELEASEROOT/run/jobOptions_Dst2Root.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo '#include "$CALIBSVCROOT/share/calibConfig_rec_mc.txt"' >>jobOptions_rec_Bhlumi_$X.txt
	echo  >>jobOptions_rec_Bhlumi_$X.txt
	echo 'BesRndmGenSvc.RndmSeed = 100;' >>jobOptions_rec_Bhlumi_$X.txt
	echo  >>jobOptions_rec_Bhlumi_$X.txt
	echo 'MessageSvc.OutputLevel = 5;' >>jobOptions_rec_Bhlumi_$X.txt
	echo  >>jobOptions_rec_Bhlumi_$X.txt
	echo 'EventCnvSvc.digiRootInputFile = {"'"${outdir}rtraw/Bhlumi_sim_$X.rtraw"'"};' >>jobOptions_rec_Bhlumi_$X.txt
	echo  >>jobOptions_rec_Bhlumi_$X.txt
	echo 'EventCnvSvc.digiRootOutputFile ="'"${outdir}dst/Bhlumi_rec_$X.dst"'";' >>jobOptions_rec_Bhlumi_$X.txt
	echo  >>jobOptions_rec_Bhlumi_$X.txt
	echo 'ApplicationMgr.EvtMax = -1;' >>jobOptions_rec_Bhlumi_$X.txt
	
	#Write job option file for analysis
	echo '#include "$ROOTIOROOT/share/jobOptions_ReadRec.txt"' >>jobOptions_ana_Bhlumi_$X.txt
	echo '#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"' >>jobOptions_ana_Bhlumi_$X.txt
	echo '#include "$MAGNETICFIELDROOT/share/MagneticField.txt"' >>jobOptions_ana_Bhlumi_$X.txt
	echo '#include "$ABSCORROOT/share/jobOptions_AbsCor.txt"' >>jobOptions_ana_Bhlumi_$X.txt
	echo '#include "$ANALYSIS_CZDDROOT/share/jobOptions_analysis_cZDD.txt"' >>jobOptions_ana_Bhlumi_$X.txt
	echo  >>jobOptions_ana_Bhlumi_$X.txt
	echo 'EventCnvSvc.digiRootInputFile = {"'"${outdir}dst/Bhlumi_rec_$X.dst"'"};' >>jobOptions_ana_Bhlumi_$X.txt
	echo 'analysis_cZDD.InputImpact_reco = "'"${outdir}NTuples/Impact_reco_Bhlumi_$X.root"'";' >>jobOptions_ana_Bhlumi_$X.txt
	echo 'analysis_cZDD.OutFileName = "'"${outdir}NTuples/Bhlumi_ana_$X.root"'";' >>jobOptions_ana_Bhlumi_$X.txt
	echo  >>jobOptions_ana_Bhlumi_$X.txt
	echo 'MessageSvc.OutputLevel = 5;' >>jobOptions_ana_Bhlumi_$X.txt
	echo  >>jobOptions_ana_Bhlumi_$X.txt
	echo 'ApplicationMgr.EvtMax = -1;' >>jobOptions_ana_Bhlumi_$X.txt
	echo 'NTupleSvc.Output = { "FILE1 DATAFILE=phokhara.root OPT=NEW TYP=ROOT"};' >>jobOptions_ana_Bhlumi_$X.txt
	
	#Write 
	echo "#!/bin/bash" > SubScript_$X
  echo "#PBS -N ZDD_Bhlumi_$X" >> SubScript_$X
  echo "#PBS -q long" >> SubScript_$X
  echo "#PBS -l walltime=23:00:00" >> SubScript_$X
  echo "#PBS -j oe" >> SubScript_$X
  echo "#PBS -o ${outdir}Logs/ZDD_Bhlumi_$X.log" >> SubScript_$X
  echo "#PBS -V" >> SubScript_$X
  echo "#PBS -S /bin/bash" >> SubScript_$X
  echo >> SubScript_$X
	#echo 'echo "truc"' >> SubScript_$X
  echo "cd /home/bgarillo" >> SubScript_$X
	echo "cd cmthome-6.6.4-ZDD/" >> SubScript_$X
	echo "source setup.sh" >> SubScript_$X
  echo >> SubScript_$X
  echo "export MyLocalWorkDir=/tmp/\$PBS_JOBID" >> SubScript_$X
  echo "echo \$MyLocalWorkDir" >> SubScript_$X
  echo "if [ -d \"\$MyLocalWorkDir\" ];" >> SubScript_$X
  echo "then " >> SubScript_$X
  echo "rm -rf \$MyLocalWorkDir" >> SubScript_$X
  echo "fi" >> SubScript_$X
  echo "mkdir \$MyLocalWorkDir" >> SubScript_$X
  echo >> SubScript_$X
	echo "cd \$MyLocalWorkDir" >> SubScript_$X
	echo "boss.exe ${scriptdir}jobOptions_sim_Bhlumi_$X.txt"  >> SubScript_$X
	echo "cp Impact_reco.root ${outdir}NTuples/Impact_reco_Bhlumi_$X.root" >> SubScript_$X
	echo "cp Bhlumi_genout.root ${outdir}NTuples/Bhlumi_gen_$X.root" >> SubScript_$X
	echo "boss.exe ${scriptdir}jobOptions_rec_Bhlumi_$X.txt" >> SubScript_$X
	echo "boss.exe ${scriptdir}jobOptions_ana_Bhlumi_$X.txt" >> SubScript_$X
	echo "Submit $X"
  qsub SubScript_$X
	
done
