#!/bin/sh

start=$1
end=$2

#Nevents=40000
Nevents=100

scriptdir=$PWD/
outdir=/data/work/kphpbb/bgarillo/cZDD/BabayagaNLO/

for seed in `seq $start $end`;
do
	X=$seed
	#Write job option file for simulation
	echo '#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'ApplicationMgr.DLLs += {"BabayagaNLO","BesServices", "RootCnvSvc" };' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'ApplicationMgr.TopAlg += {"BabayagaNLO"};' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.Channel=0;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.CMSEnergy=3.773;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.ThetaMin=0.01;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.ThetaMax=179.09;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.AcollMax=180.0;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.Emin=0.04;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.Nsearch=10000;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.Verbose=0;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.RunningAlpha=1;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.RadiativeCorrection=0;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BabayagaNLO.PhotonNumber=0;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BesRndmGenSvc.RndmSeed = '"$seed"';' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo >> jobOptions_sim_BabayagaNLO_$X.txt
	echo '#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'BesSim.FullBeamPipe = 0;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo '#include "$CALIBSVCROOT/share/calibConfig_sim.txt"' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'RealizationSvc.RunIdList = {-20448};' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo '#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'RootCnvSvc.digiRootOutputFile = "'"${outdir}"'rtraw/BabayagaNLO_sim_'"$X"'.rtraw";' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'MessageSvc.OutputLevel  = 5;' >> jobOptions_sim_BabayagaNLO_$X.txt
	echo 'ApplicationMgr.EvtMax = 10000;' >> jobOptions_sim_BabayagaNLO_$X.txt
	
	#Write job option file for reconstruction
	echo '#include "$ROOTIOROOT/share/jobOptions_ReadRoot.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$BESEVENTMIXERROOT/share/jobOptions_EventMixer_rec.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo 'MixerAlg.ReplaceDataPath = "/data/group/bes3/bes3data/offline/data/fltrandomtrg/";' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$CALIBSVCROOT/share/job-CalibData.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$MAGNETICFIELDROOT/share/MagneticField.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$ESTIMEALGROOT/share/job_EsTimeAlg.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$MDCXRECOROOT/share/jobOptions_MdcPatTsfRec.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$KALFITALGROOT/share/job_kalfit_numf_data.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$MDCDEDXALGROOT/share/job_dedx_all.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$TRKEXTALGROOT/share/TrkExtAlgOption.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$TOFRECROOT/share/jobOptions_TofRec.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$TOFENERGYRECROOT/share/TofEnergyRecOptions_MC.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$EMCRECROOT/share/EmcRecOptions.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$MUCRECALGROOT/share/jobOptions_MucRec.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$VEEVERTEXALGROOT/share/jobOptions_veeVertex.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$HLTMAKERALGROOT/share/jobOptions_HltMakerAlg.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$EVENTNAVIGATORROOT/share/EventNavigator.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$ROOTIOROOT/share/jobOptions_Dst2Root.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo '#include "$CALIBSVCROOT/share/calibConfig_rec_mc.txt"' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo  >>jobOptions_rec_BabayagaNLO_$X.txt
	echo 'BesRndmGenSvc.RndmSeed = 100;' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo  >>jobOptions_rec_BabayagaNLO_$X.txt
	echo 'MessageSvc.OutputLevel = 5;' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo  >>jobOptions_rec_BabayagaNLO_$X.txt
#	echo 'EventCnvSvc.digiRootInputFile = {"/data/work/kphpbb/bgarillo/Galuga2.0/rtraw/'"galuga_$X.rtraw"'"};' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo 'EventCnvSvc.digiRootInputFile = {"'"${outdir}rtraw/BabayagaNLO_sim_$X.rtraw"'"};' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo  >>jobOptions_rec_BabayagaNLO_$X.txt
#	echo 'EventCnvSvc.digiRootOutputFile ="/data/work/kphpbb/bgarillo/Galuga2.0/dst/'"galuga_$X.dst"'";' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo 'EventCnvSvc.digiRootOutputFile ="'"${outdir}dst/BabayagaNLO_rec_$X.dst"'";' >>jobOptions_rec_BabayagaNLO_$X.txt
	echo  >>jobOptions_rec_BabayagaNLO_$X.txt
	echo 'ApplicationMgr.EvtMax = -1;' >>jobOptions_rec_BabayagaNLO_$X.txt
	
	#Write job option file for analysis
	echo '#include "$ROOTIOROOT/share/jobOptions_ReadRec.txt"' >>jobOptions_ana_BabayagaNLO_$X.txt
	echo '#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"' >>jobOptions_ana_BabayagaNLO_$X.txt
	echo '#include "$MAGNETICFIELDROOT/share/MagneticField.txt"' >>jobOptions_ana_BabayagaNLO_$X.txt
	echo '#include "$ANALYSIS_CZDDROOT/share/jobOptions_analysis_cZDD.txt"' >>jobOptions_ana_BabayagaNLO_$X.txt
	echo  >>jobOptions_ana_BabayagaNLO_$X.txt
	echo 'EventCnvSvc.digiRootInputFile = {"'"${outdir}rtraw/BabayagaNLO_sim_$X.rtraw"'"};' >>jobOptions_ana_BabayagaNLO_$X.txt
	echo 'analysis_cZDD.OutFileName = "'"${outdir}dst/BabayagaNLO_ana_$X.root"'";' >>jobOptions_ana_BabayagaNLO_$X.txt
	echo  >>jobOptions_ana_BabayagaNLO_$X.txt
	echo 'MessageSvc.OutputLevel = 5;' >>jobOptions_ana_BabayagaNLO_$X.txt
	echo  >>jobOptions_ana_BabayagaNLO_$X.txt
	echo 'ApplicationMgr.EvtMax = -1;' >>jobOptions_ana_BabayagaNLO_$X.txt
	echo 'NTupleSvc.Output = { "FILE1 DATAFILE=phokhara.root OPT=NEW TYP=ROOT"};' >>jobOptions_ana_BabayagaNLO_$X.txt
	
	#Write 
	echo "#!/bin/bash" > SubScript_$X
  echo "#PBS -N ZDD_BabayagaNLO_$X" >> SubScript_$X
  echo "#PBS -q long" >> SubScript_$X
  echo "#PBS -l walltime=23:00:00" >> SubScript_$X
  echo "#PBS -j oe" >> SubScript_$X
  echo "#PBS -o ${outdir}Logs/ZDD_BabayagaNLO_$X.log" >> SubScript_$X
  echo "#PBS -V" >> SubScript_$X
  echo "#PBS -S /bin/bash" >> SubScript_$X
  echo >> SubScript_$X
	#echo 'echo "truc"' >> SubScript_$X
  echo "cd /home/bgarillo" >> SubScript_$X
	echo "cd cmthome-6.6.4-ZDD/" >> SubScript_$X
	echo "source setup.sh" >> SubScript_$X
  echo >> SubScript_$X
  echo "export MyLocalWorkDir=/tmp/\$PBS_JOBID" >> SubScript_$X
  echo "echo \$MyLocalWorkDir" >> SubScript_$X
  echo "if [ -d \"\$MyLocalWorkDir\" ];" >> SubScript_$X
  echo "then " >> SubScript_$X
  echo "rm -rf \$MyLocalWorkDir" >> SubScript_$X
  echo "fi" >> SubScript_$X
  echo "mkdir \$MyLocalWorkDir" >> SubScript_$X
  echo >> SubScript_$X
	echo "cd \$MyLocalWorkDir" >> SubScript_$X
	echo "boss.exe ${scriptdir}jobOptions_sim_BabayagaNLO_$X.txt"  >> SubScript_$X
	echo "cp Impact_reco.root ${outdir}NTuples/Impact_reco_BabayagaNLO_$X.root" >> SubScript_$X
	echo "boss.exe ${scriptdir}jobOptions_rec_BabayagaNLO_$X.txt" >> SubScript_$X
	echo "boss.exe ${scriptdir}jobOptions_ana_BabayagaNLO_$X.txt" >> SubScript_$X
	echo "Submit $X"
  qsub SubScript_$X
	
done
