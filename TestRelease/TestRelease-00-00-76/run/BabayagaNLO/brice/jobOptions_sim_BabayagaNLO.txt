#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"

//*************job options for Babayaga ***************
ApplicationMgr.DLLs += {"BabayagaNLO","BesServices", "RootCnvSvc" };
ApplicationMgr.TopAlg += {"BabayagaNLO"};
////////options for BABAYAGANLO
BabayagaNLO.Channel=0;              // 0: e+e-->e+e-;1:e+e_->mu+mu-;2:e+e-->gamma gamma
BabayagaNLO.CMSEnergy=3.773;
BabayagaNLO.BeamEnergySpread=0.001;
//BabayagaNLO.ThetaMin=1;
//BabayagaNLO.ThetaMax=179;
//BabayagaNLO.ThetaMin=0.0573; //1 mrad
//BabayagaNLO.ThetaMax=0.860; //15 mrad
//BabayagaNLO.ThetaMin=1.;
//BabayagaNLO.ThetaMax=10.;
//BabayagaNLO.ThetaMin=0.5;
//BabayagaNLO.ThetaMax=1.;
//BabayagaNLO.ThetaMin=0.0573;
//BabayagaNLO.ThetaMax=10.;
//BabayagaNLO.ThetaMin=0.0573; 
//BabayagaNLO.ThetaMin=0.; 
//BabayagaNLO.ThetaMin=0.086; 
//BabayagaNLO.ThetaMax=180;
//BabayagaNLO.ThetaMin=60; 
BabayagaNLO.ThetaMin=0.01; 
BabayagaNLO.ThetaMax=179.99;
BabayagaNLO.AcollMax=180.0; 
BabayagaNLO.Emin=0.04;
BabayagaNLO.Nsearch=10000;
//BabayagaNLO.Nsearch=100;
BabayagaNLO.Verbose=0;
BabayagaNLO.RunningAlpha=1; //Energy dependent electromagnetic running coupling constant
BabayagaNLO.RadiativeCorrection=0; //LO diagram
//BabayagaNLO.PhotonNumber=-1; //Max number of photons. -1:All possible photons
BabayagaNLO.PhotonNumber=0;

////**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

////**************job options for detector simulation******************

//#include "$BESSIMROOT/share/G4Svc_BesSim.txt"
#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

/// run ID
//RealizationSvc.RunIdList = {-20448};//, 0, -23454}; // 2011 data of psi''
RealizationSvc.RunIdList = {-11414, 0, -13988, -14395, 0, -14604, -20448, 0, -23454}; //RUN Number for psipp data

//#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"
#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"
//RootCnvSvc.digiRootOutputFile = "/data/work/kphpbb/bgarillo/cZDD/BabayagaNLO/rtraw/ee.rtraw";
RootCnvSvc.digiRootOutputFile = "ee.rtraw";

//// OUTPUT PRINTOUT LEVEL
//// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 5;

//// Number of events to be processed (default is 10)
//ApplicationMgr.EvtMax = 1000;
ApplicationMgr.EvtMax = 100;
