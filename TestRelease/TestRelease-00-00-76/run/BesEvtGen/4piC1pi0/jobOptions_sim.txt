//DENG Zi-yan 2008-03-17

#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"

//*************job options for generator (KKMC)***************
#include "$KKMCROOT/share/jobOptions_KKMC.txt"
KKMC.CMSEnergy = 3.770;
KKMC.BeamEnergySpread=0.0008;
KKMC.NumberOfEventPrinted=1;
KKMC.GeneratePsi3770=true;

//*************job options for EvtGen***************
#include "$BESEVTGENROOT/share/BesEvtGen.txt"
EvtDecay.userDecayTableName = "4piC1pi0.dec";

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

//**************job options for detector simulation******************
#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"
BesSim.Mdc=1;
BesSim.Tof=1;
BesSim.Emc=1;
BesSim.Muc=1;
BesSim.PipeSCM = 1;
BesSim.ZddMaterial = "LYSO";
BesSim.ZddSensor = "SenslC";
BesSim.FullBeamPipe = 1;
G4Svc.BoostLab = true;

//configure for calibration constants
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

// run ID
RealizationSvc.RunIdList = {-20448};

#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "4piC1pi0.rtraw";


// OUTPUT PRINTOUT LEVEL
// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 50;

