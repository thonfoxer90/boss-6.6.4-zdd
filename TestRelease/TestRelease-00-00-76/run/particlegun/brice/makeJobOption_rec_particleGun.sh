#!/bin/sh

indir=$1
outdir=$2

rm $outdir*

for Y in `ls $indir*.txt `;
	do
	#Create thejob option file into outdir directory
	jobOpt="${Y//$indir/$outdir}"
	jobOpt="${jobOpt//_sim/_rec}"
	#echo $jobOpt
	#Write the jobb option file
	echo '#include "$TESTRELEASEROOT/run/jobOptions_ReadRoot.txt"' >> $jobOpt
	echo '#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"' >> $jobOpt
	echo '#include "$CALIBSVCROOT/share/job-CalibData.txt"' >> $jobOpt
	echo '#include "$MAGNETICFIELDROOT/share/MagneticField.txt"' >> $jobOpt
	echo '#include "$ESTIMEALGROOT/share/job_EsTimeAlg.txt"' >> $jobOpt
	echo '#include "$MDCXRECOROOT/share/jobOptions_MdcPatTsfRec.txt"' >> $jobOpt
	echo '#include "$KALFITALGROOT/share/job_kalfit_numf_data.txt"' >> $jobOpt
	echo '#include "$MDCDEDXALGROOT/share/job_dedx_all.txt"' >> $jobOpt
	echo '#include "$TRKEXTALGROOT/share/TrkExtAlgOption.txt"' >> $jobOpt
	echo '#include "$TOFRECROOT/share/jobOptions_TofRec.txt"' >> $jobOpt
	echo '#include "$TOFENERGYRECROOT/share/TofEnergyRecOptions_MC.txt"' >> $jobOpt
	echo '#include "$EMCRECROOT/share/EmcRecOptions.txt"' >> $jobOpt
	echo '#include "$MUCRECALGROOT/share/jobOptions_MucRec.txt"' >> $jobOpt
	echo '#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"' >> $jobOpt
	echo '#include "$PRIMARYVERTEXALGROOT/share/jobOptions_kalman.txt"' >> $jobOpt
	echo '#include "$VEEVERTEXALGROOT/share/jobOptions_veeVertex.txt"' >> $jobOpt
	echo '#include "$EVENTNAVIGATORROOT/share/EventNavigator.txt"' >> $jobOpt
	echo '#include "$HLTMAKERALGROOT/share/jobOptions_HltMakerAlg.txt"' >> $jobOpt
	echo '#include "$TESTRELEASEROOT/run/jobOptions_Dst2Root.txt"' >> $jobOpt
	echo '#include "$CALIBSVCROOT/share/calibConfig_rec_mc.txt"' >> $jobOpt
	echo 'BesRndmGenSvc.RndmSeed = 35000;' >> $jobOpt
	echo 'MessageSvc.OutputLevel = 5;' >> $jobOpt
	#Write the input path
	inpath=$(grep -i 'RootCnvSvc.digiRootOutputFile' $Y) #Store the output command string from sim file into outpath	
	inpath="${inpath//RootCnvSvc.digiRootOutputFile/EventCnvSvc.digiRootInputFile}" #Replace "RootCnvSvc.digiRootOutputFile" by "EventCnvSvc.digiRootInputFile"
	inpath=${inpath//;/\};} #Replace ";" ending by "};"
	inpath=${inpath//=/=\{} #Replace "=" by "= {"
#echo $inpath
	echo $inpath >> $jobOpt
	#Write the output path
	outpath=$(grep -i 'RootCnvSvc.digiRootOutputFile' $Y) #Store the output command string from sim file into outpath
	outpath="${outpath//RootCnvSvc.digiRootOutputFile/EventCnvSvc.digiRootOutputFile}" #Replace "RootCnvSvc.digiRootOutputFile" by "EventCnvSvc.digiRootOutputFile"
	outpath="${outpath//rtraw/dst}"

	#inpath="${inpath%\";}"
	echo $outpath >> $jobOpt
	echo 'ApplicationMgr.EvtMax = -1;' >> $jobOpt
	done
