#include "$TESTRELEASEROOT/run/jobOptions_ReadRoot.txt"
#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"
#include "$CALIBSVCROOT/share/job-CalibData.txt"
#include "$MAGNETICFIELDROOT/share/MagneticField.txt"
#include "$ESTIMEALGROOT/share/job_EsTimeAlg.txt"
#include "$MDCXRECOROOT/share/jobOptions_MdcPatTsfRec.txt"
#include "$KALFITALGROOT/share/job_kalfit_numf_data.txt"
#include "$MDCDEDXALGROOT/share/job_dedx_all.txt"
#include "$TRKEXTALGROOT/share/TrkExtAlgOption.txt"
#include "$TOFRECROOT/share/jobOptions_TofRec.txt"
#include "$TOFENERGYRECROOT/share/TofEnergyRecOptions_MC.txt"
#include "$EMCRECROOT/share/EmcRecOptions.txt"
#include "$MUCRECALGROOT/share/jobOptions_MucRec.txt"
#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"
#include "$PRIMARYVERTEXALGROOT/share/jobOptions_kalman.txt"
#include "$VEEVERTEXALGROOT/share/jobOptions_veeVertex.txt"
#include "$EVENTNAVIGATORROOT/share/EventNavigator.txt"
#include "$HLTMAKERALGROOT/share/jobOptions_HltMakerAlg.txt"
#include "$TESTRELEASEROOT/run/jobOptions_Dst2Root.txt"
#include "$CALIBSVCROOT/share/calibConfig_rec_mc.txt"
BesRndmGenSvc.RndmSeed = 35000;
MessageSvc.OutputLevel = 5;
EventCnvSvc.digiRootInputFile ={ "/data/work/kphpbb/bgarillo/cZDD/particlegun/rtraw/particleGun_114_500MeV_Pipe.rtraw"};
EventCnvSvc.digiRootOutputFile = "/data/work/kphpbb/bgarillo/cZDD/particlegun/dst/particleGun_114_500MeV_Pipe.dst";
ApplicationMgr.EvtMax = -1;
