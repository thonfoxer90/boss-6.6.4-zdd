#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"



ApplicationMgr.DLLs += { "SingleParticleGun", "BesServices" , "RootCnvSvc" };
ApplicationMgr.TopAlg += { "SingleParticleGun" };

// then override any you need specificially for this job
//
SingleParticleGun.Mode = 1; // EMode = 1, PtMode = 2
SingleParticleGun.ModeE  = 1;    // 1 = fixed, 2 = gaus , 3 = flat
SingleParticleGun.ModeTheta = 3; // 1 = fixed, 2 = gaus , 3 = flat
SingleParticleGun.ModePhi = 3;   // 1 = fixed, 2 = gaus , 3 = flat
SingleParticleGun.E = 1.5; //1.1221203; // GeV
// enable these flags, if you choose flat distribution
SingleParticleGun.MinTheta  = 0.0014925; // rad, = 0.08551°
SingleParticleGun.MaxTheta  = 0.015865; // rad = 0.78838°
SingleParticleGun.MinPhi = 0.; // rad, = 9.4623° default=0
SingleParticleGun.MaxPhi = 6.28; // rad, = 153.435° default=2pi

SingleParticleGun.PdgCode = 22;  // 11 = electron, 22 = photon, ...  standaMC numbering scheme



#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"
BesSim.FullBeamPipe = 0;//1;

#include "$CALIBSVCROOT/share/calibConfig_sim.txt"
RealizationSvc.RunIdList = {-20448};
#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile ="/data/work/kphpbb/bgarillo/cZDD/particlegun/rtraw/particleGun_AllFlat_Theta1-4mrad15-8mrad_1500MeV_10000_noPipe.rtraw";
// OUTPUT PRINTOUT LEVEL
// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 5;
// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 10000;
