#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"
ApplicationMgr.DLLs += { "SingleParticleGun", "BesServices" , "RootCnvSvc" };
ApplicationMgr.TopAlg += { "SingleParticleGun" };
SingleParticleGun.Mode = 1;
SingleParticleGun.ModeE  = 1; 
SingleParticleGun.ModeTheta = 1; 
SingleParticleGun.ModePhi = 1;
SingleParticleGun.E = 0.1;
SingleParticleGun.Theta = 0;
SingleParticleGun.Phi = 0;
SingleParticleGun.PdgCode = 22;
#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"
BesSim.Mdc=1;
BesSim.Tof=1;
BesSim.Emc=1;
BesSim.Muc=1;
BesSim.FullBeamPipe = 1;
BesSim.PipeSCM = 1;
MagneticFieldSvc.TurnOffField = false;
MagneticFieldSvc.TurnOffISPB = false;
BesSim.ZddMaterial = "LYSO";
BesSim.ZddSensor = "SenslC";
BesSim.ZddBoxesXCenter = 2.;
BesSim.ZddBoxesYCenter = 2.;
G4Svc.BoostLab = 1;
BesSim.ImpactRecoFlag=true;
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"
RealizationSvc.RunIdList = {-20448};
#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "/data/work/kphpbb/bgarillo/cZDD/particlegun/rtraw/particleGun_0deg_100MeV_Pipe.rtraw";
MessageSvc.OutputLevel  = 5;
ApplicationMgr.EvtMax = 10000;
