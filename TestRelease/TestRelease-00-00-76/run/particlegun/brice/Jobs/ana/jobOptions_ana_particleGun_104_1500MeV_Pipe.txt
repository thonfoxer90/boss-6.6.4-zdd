#include "$ROOTIOROOT/share/jobOptions_ReadRec.txt"
#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"
#include "$MAGNETICFIELDROOT/share/MagneticField.txt"
#include "$ABSCORROOT/share/jobOptions_AbsCor.txt"
#include "$ANALYSIS_CZDDROOT/share/jobOptions_analysis_cZDD.txt"
EventCnvSvc.digiRootInputFile ={ "/data/work/kphpbb/bgarillo/cZDD/particlegun/dst/particleGun_104_1500MeV_Pipe.dst"};
analysis_cZDD.InputImpact_reco = "/data/work/kphpbb/bgarillo/cZDD/particlegun/root/Impact_reco_particleGun_104_1500MeV_Pipe.root";
analysis_cZDD.OutFileName = "/data/work/kphpbb/bgarillo/cZDD/particlegun/root/particleGun_104_1500MeV_Pipe.root";
MessageSvc.OutputLevel = 5;
ApplicationMgr.EvtMax = -1;
ApplicationMgr.HistogramPersistency = "ROOT";
