//TofRec.PrintOutInfo = true;
//input ROOT MC data
//#include "$ROOTIOROOT/share/jobOptions_ReadRoot.txt"
#include "$TESTRELEASEROOT/run/jobOptions_ReadRoot.txt"
#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"

// background mixing
//#include "$BESEVENTMIXERROOT/share/jobOptions_EventMixer_rec.txt"

#include "$CALIBSVCROOT/share/job-CalibData.txt"
//#include "$MAGNETICFIELDROOT/share/MagneticField.txt"
//#include "$ESTIMEALGROOT/share/job_EsTimeAlg.txt"

// PAT+TSF method for MDC reconstruction
//#include "$MDCXRECOROOT/share/jobOptions_MdcPatRec.txt"

//#include "$KALFITALGROOT/share/job_kalfit_numf_data.txt"
//#include "$MDCDEDXALGROOT/share/job_dedx_all.txt"
#include "$TRKEXTALGROOT/share/TrkExtAlgOption.txt"
#include "$TOFRECROOT/share/jobOptions_TofRec.txt"
#include "$TOFENERGYRECROOT/share/TofEnergyRecOptions_MC.txt"
#include "$EMCRECROOT/share/EmcRecOptions.txt"
#include "$MUCRECALGROOT/share/jobOptions_MucRec.txt"
#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"
#include "$PRIMARYVERTEXALGROOT/share/jobOptions_kalman.txt"
#include "$VEEVERTEXALGROOT/share/jobOptions_veeVertex.txt"
#include "$EVENTNAVIGATORROOT/share/EventNavigator.txt"

#include "$HLTMAKERALGROOT/share/jobOptions_HltMakerAlg.txt"

//output ROOT REC data
//#include "$ROOTIOROOT/share/jobOptions_Dst2Root.txt"
#include "$TESTRELEASEROOT/run/jobOptions_Dst2Root.txt"
/*
RootWriter.ItemList = { "/Event/EventHeader#1",
		        "/Event/Navigator#1",
                        "/Event/MC/McParticleCol#1",
			"/Event/Digi/ZddDigiCol#1",   // Zdd Digis
			"/Event/MC/ZddMcHitCol#1",    // McTruth of Zdd Hits
                        "/Event/Dst#99",
                        "/Event/EvtRec#99"
};
*/
//configure of calibration constants for MC
#include "$CALIBSVCROOT/share/calibConfig_rec_mc.txt"

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 35000;

//Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel = 5;

//ROOT input data file
EventCnvSvc.digiRootInputFile = {"test.rtraw"};

//ROOT output data file
EventCnvSvc.digiRootOutputFile ="test.dst";
 
//Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = -1; //with -1 all events are processed
