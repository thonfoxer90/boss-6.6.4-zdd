#!/bin/bash

outdir=$1

rm $outdir*

crystalList=('0deg' '104' '103' '102' '101' '114' '113' '112' '111' '124' '123' '122' '121')
thetaList=(0 0.010865 0.008037 0.005381 0.003337 0.012033 0.009557 0.007463 0.006154 0.013760 0.011657 0.010012 0.009079)
phiList=(0 0.27830 0.38051 0.58800 1.1071 0.51915 0.67474 0.92730 1.3258 0.70863 0.87606 1.1071 1.4056)

EList=(0.05 0.1 0.2 0.5 1. 1.5 2.)

for i in "${crystalList[@]}"
	do
	echo $i
	if [[ $i == "0deg" ]]
		then
		theta=0
		phi=0
		fi
	if [[ $i == "104" ]]
		then
		theta=0.010865
		phi=0.27830
		fi
	if [[ $i == "103" ]]
		then
		theta=0.008037
		phi=0.38051
		fi
	if [[ $i == "102" ]]
		then
		theta=0.005381
		phi=0.58800
		fi
	if [[ $i == "101" ]]
		then
		theta=0.003337
		phi=1.1071
		fi
	if [[ $i == "114" ]]
		then
		theta=0.012033
		phi=0.51915
		fi
	if [[ $i == "113" ]]
		then
		theta=0.009557
		phi=0.67474
		fi
	if [[ $i == "112" ]]
		then
		theta=0.007463
		phi=0.92730
		fi
	if [[ $i == "111" ]]
		then
		theta=0.006154
		phi=1.3258
		fi
	if [[ $i == "124" ]]
		then
		theta=0.013760
		phi=0.70863
		fi
	if [[ $i == "123" ]]
		then
		theta=0.011657
		phi=0.87606
		fi
	if [[ $i == "122" ]]
		then
		theta=0.010012
		phi=1.1071
		fi
	if [[ $i == "121" ]]
		then
		theta=0.009079
		phi=1.4056
		fi	
		
	#echo $theta
	#echo $phi
		for j in "${EList[@]}"
			do
			#echo $j
			E=$(echo $j*1000 | bc)
			intE=${E%.*}
			
			jobOpt=${outdir}jobOptions_sim_particleGun_${i}_${intE}MeV_Pipe.txt
			echo '#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"' >> $jobOpt
			echo 'ApplicationMgr.DLLs += { "SingleParticleGun", "BesServices" , "RootCnvSvc" };' >> $jobOpt
			echo 'ApplicationMgr.TopAlg += { "SingleParticleGun" };' >> $jobOpt
			
			echo 'SingleParticleGun.Mode = 1;' >> $jobOpt
			echo 'SingleParticleGun.ModeE  = 1; ' >> $jobOpt
			echo 'SingleParticleGun.ModeTheta = 1; ' >> $jobOpt
			echo 'SingleParticleGun.ModePhi = 1;' >> $jobOpt
			echo 'SingleParticleGun.E = '"$j"';' >> $jobOpt
			
			echo 'SingleParticleGun.Theta = '"$theta"';' >> $jobOpt
			echo 'SingleParticleGun.Phi = '"$phi"';' >> $jobOpt
			
			echo 'SingleParticleGun.PdgCode = 22;' >> $jobOpt
			echo '#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"' >> $jobOpt
			
			#echo 'BesSim.Mdc=0;' >> $jobOpt
			echo 'BesSim.Mdc=1;' >> $jobOpt
			
			#echo 'BesSim.Tof=0;' >> $jobOpt
			echo 'BesSim.Tof=1;' >> $jobOpt
			
			#echo 'BesSim.Emc=0;' >> $jobOpt
			echo 'BesSim.Emc=1;' >> $jobOpt
			
			#echo 'BesSim.Muc=0;' >> $jobOpt
			echo 'BesSim.Muc=1;' >> $jobOpt
			
			echo 'BesSim.FullBeamPipe = 1;' >> $jobOpt
			#echo 'BesSim.FullBeamPipe = 0;' >> $jobOpt
			
			echo 'BesSim.PipeSCM = 1;' >> $jobOpt
			#echo 'BesSim.PipeSCM = 0;' >> $jobOpt
			
			#echo 'MagneticFieldSvc.TurnOffField = true;' >> $jobOpt
			echo 'MagneticFieldSvc.TurnOffField = false;' >> $jobOpt
			
			#echo 'MagneticFieldSvc.TurnOffISPB = true;' >> $jobOpt
			echo 'MagneticFieldSvc.TurnOffISPB = false;' >> $jobOpt
						
			echo 'BesSim.ZddMaterial = "LYSO";' >> $jobOpt
			echo 'BesSim.ZddSensor = "SenslC";' >> $jobOpt
			
			echo 'BesSim.ZddBoxesXCenter = 2.;' >> $jobOpt
			#echo 'BesSim.ZddBoxesXCenter = 1.;' >> $jobOpt
			
			echo 'BesSim.ZddBoxesYCenter = 2.;' >> $jobOpt
			#echo 'BesSim.ZddBoxesYCenter = 1.5;' >> $jobOpt
			
			if [[ $i == "0deg" ]]
				then
				echo 'G4Svc.BoostLab = 1;' >> $jobOpt
				else
				echo 'G4Svc.BoostLab = 0;' >> $jobOpt
				fi
			
			echo 'BesSim.ImpactRecoFlag=true;' >> $jobOpt
			#echo 'BesSim.SteppingDumpFlag=true;' >> $jobOpt
			echo '#include "$CALIBSVCROOT/share/calibConfig_sim.txt"' >> $jobOpt
			echo 'RealizationSvc.RunIdList = {-20448};' >> $jobOpt
			echo '#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"' >> $jobOpt
			echo 'RootCnvSvc.digiRootOutputFile = "/data/work/kphpbb/bgarillo/cZDD/particlegun/rtraw/'"particleGun_${i}_${intE}MeV_Pipe"'.rtraw";' >> $jobOpt
			echo 'MessageSvc.OutputLevel  = 5;' >> $jobOpt
			echo 'ApplicationMgr.EvtMax = 10000;' >> $jobOpt			
			#echo 'ApplicationMgr.EvtMax = 100;' >> $jobOpt
		done
	echo
	done