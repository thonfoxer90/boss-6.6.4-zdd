#include "$ROOTIOROOT/share/jobOptions_ReadRec.txt"
#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"
#include "$MAGNETICFIELDROOT/share/MagneticField.txt"
#include "$ABSCORROOT/share/jobOptions_AbsCor.txt"
//#include "$RHOPIALGROOT/share/jobOptions_Rhopi.txt"
//#include "$ANALYSIS_cZDD/share/jobOptions_analysis_cZdd.txt"
#include "$ANALYSIS_CZDDROOT/share/jobOptions_analysis_cZDD.txt"

// Input REC or DST file name
EventCnvSvc.digiRootInputFile = {"/data/work/kphpbb/bgarillo/cZDD/particlegun_dst_files/particleGunelectron_flat_104_1500MeV_1000_noPipe.dst"};

analysis_cZDD.OutFileName = "/data/work/kphpbb/bgarillo/cZDD/particlegun_root_files/particleGunelectron_flat_104_1500MeV_1000_noPipe.root";

// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = -1;

ApplicationMgr.HistogramPersistency = "ROOT";
NTupleSvc.Output = { "FILE1 DATAFILE=phokhara_100000_leo.root OPT=NEW TYP=ROOT"};
