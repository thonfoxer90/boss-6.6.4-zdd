#!/bin/sh

# Absolute path to this script
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in
SCRIPTPATH=$(dirname "$SCRIPT")

rm sim/*
${SCRIPTPATH}/makeJobOption_sim_particleGun.sh $PWD/sim/
rm rec/*
${SCRIPTPATH}/makeJobOption_rec_particleGun.sh $PWD/sim/ $PWD/rec/
rm ana/*
${SCRIPTPATH}/makeJobOption_ana_particleGun.sh $PWD/rec/ $PWD/ana/

rm SubScript_*
#../submitJobs_sim_particleGun.sh $PWD/sim/
#../submitJobs_rec_particleGun.sh $PWD/rec/
#../submitJobs_ana_particleGun.sh $PWD/ana/

${SCRIPTPATH}/submitJobs_sim_particleGun.sh $PWD/sim/
NJobs=$(qstat | grep bgarillo | wc -l)
while [[ $NJobs -gt 0 ]]
    do
    sleep 10m
    NJobs=$(qstat | grep bgarillo | wc -l)
    echo "Total number of jobs in queue : $NJobs"
  done
${SCRIPTPATH}/submitJobs_rec_particleGun.sh $PWD/rec/
while [[ $NJobs -gt 0 ]]
    do
    sleep 10m
    NJobs=$(qstat | grep bgarillo | wc -l)
    echo "Total number of jobs in queue : $NJobs"
  done
${SCRIPTPATH}/submitJobs_ana_particleGun.sh $PWD/ana/		