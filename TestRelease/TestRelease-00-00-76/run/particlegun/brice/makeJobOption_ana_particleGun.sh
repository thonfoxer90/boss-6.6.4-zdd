#!/bin/sh

indir=$1
outdir=$2

rm $outdir*

for Y in `ls $indir*.txt `;
	do
	#Create thejob option file into outdir directory
	jobOpt="${Y//$indir/$outdir}"
	jobOpt="${jobOpt//_rec/_ana}"
	#echo $jobOpt
	#Write the jobb option file
	echo '#include "$ROOTIOROOT/share/jobOptions_ReadRec.txt"' >> $jobOpt
	echo '#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"' >> $jobOpt
	echo '#include "$MAGNETICFIELDROOT/share/MagneticField.txt"' >> $jobOpt
	echo '#include "$ABSCORROOT/share/jobOptions_AbsCor.txt"' >> $jobOpt
	echo '#include "$ANALYSIS_CZDDROOT/share/jobOptions_analysis_cZDD.txt"' >> $jobOpt
	
	#Write the input path
	inpath=$(grep -i 'EventCnvSvc.digiRootOutputFile' $Y)
	inpath="${inpath//EventCnvSvc.digiRootOutputFile/EventCnvSvc.digiRootInputFile}"
	inpath=${inpath//;/\};}
	inpath=${inpath//=/=\{} #Replace "=" by "= {" 

	echo $inpath >> $jobOpt
	#Write the output path
	outpath=$(grep -i 'EventCnvSvc.digiRootOutputFile' $Y)
	outpath="${outpath//EventCnvSvc.digiRootOutputFile/analysis_cZDD.OutFileName}" #Remove everything from Y string before "/" character ( / included)
	outpath="${outpath//dst/root}"
	#Write the input impact_reco file
	#analysis_cZDD.InputImpact_reco
	impactreco=$outpath
	impactreco=${impactreco/analysis_cZDD.OutFileName/analysis_cZDD.InputImpact_reco}
	impactreco=${impactreco/particleGun/Impact_reco_particleGun}
	#echo $impactreco
	#echo $outpath
	echo $impactreco >> $jobOpt
	echo $outpath >> $jobOpt
	echo 'MessageSvc.OutputLevel = 5;' >> $jobOpt
	echo 'ApplicationMgr.EvtMax = -1;' >> $jobOpt
	echo 'ApplicationMgr.HistogramPersistency = "ROOT";' >> $jobOpt
	done
