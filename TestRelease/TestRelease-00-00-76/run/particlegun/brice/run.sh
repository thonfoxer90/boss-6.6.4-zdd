#!/bin/sh

pwd

rm SubScript_*

../submitJobs_sim_particleGun.sh $PWD/sim/

../makeJobOption_rec_particleGun.sh $PWD/sim/ $PWD/rec/

../submitJobs_rec_particleGun.sh $PWD/rec/

../makeJobOption_ana_particleGun.sh $PWD/sim/ $PWD/rec/

../submitJobs_ana_particleGun.sh $PWD/ana/
