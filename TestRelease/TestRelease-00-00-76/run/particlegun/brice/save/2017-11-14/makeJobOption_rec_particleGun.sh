#!/bin/sh

indir=$1
outdir=$2

rm $outdir*

for Y in `ls $indir*.txt `;
	do
	#Create thejob option file into outdir directory
	jobOpt="${Y//$indir/$outdir}"
	jobOpt="${jobOpt//_sim/_rec}"
	#echo $jobOpt
	#Write the jobb option file
	echo '#include "$TESTRELEASEROOT/run/jobOptions_ReadRoot.txt"' >> $jobOpt
	echo '#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"' >> $jobOpt
	echo '#include "$CALIBSVCROOT/share/job-CalibData.txt"' >> $jobOpt
	echo '#include "$TRKEXTALGROOT/share/TrkExtAlgOption.txt"' >> $jobOpt
	echo '#include "$TOFRECROOT/share/jobOptions_TofRec.txt"' >> $jobOpt
	echo '#include "$TOFENERGYRECROOT/share/TofEnergyRecOptions_MC.txt"' >> $jobOpt
	echo '#include "$EMCRECROOT/share/EmcRecOptions.txt"' >> $jobOpt
	echo '#include "$MUCRECALGROOT/share/jobOptions_MucRec.txt"' >> $jobOpt
	echo '#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"' >> $jobOpt
	echo '#include "$PRIMARYVERTEXALGROOT/share/jobOptions_kalman.txt"' >> $jobOpt
	echo '#include "$VEEVERTEXALGROOT/share/jobOptions_veeVertex.txt"' >> $jobOpt
	echo '#include "$EVENTNAVIGATORROOT/share/EventNavigator.txt"' >> $jobOpt
	echo '#include "$HLTMAKERALGROOT/share/jobOptions_HltMakerAlg.txt"' >> $jobOpt
	echo '#include "$TESTRELEASEROOT/run/jobOptions_Dst2Root.txt"' >> $jobOpt
	echo '#include "$CALIBSVCROOT/share/calibConfig_rec_mc.txt"' >> $jobOpt
	echo 'BesRndmGenSvc.RndmSeed = 35000;' >> $jobOpt
	echo 'MessageSvc.OutputLevel = 5;' >> $jobOpt
	#Write the input path
	inpath=$(grep -i 'RootCnvSvc.digiRootOutputFile' $Y)
	inpath="${inpath//RootCnvSvc.digiRootOutputFile/EventCnvSvc.digiRootInputFile}"
	inpath="${inpath//=\"/={\"}"
	inpath=${inpath//;/\};}
	echo $inpath >> $jobOpt
	#Write the output path
	outpath=$(grep -i 'RootCnvSvc.digiRootOutputFile' $Y)
	outpath="${outpath//RootCnvSvc.digiRootOutputFile/EventCnvSvc.digiRootOutputFile}" #Remove everything from Y string before "/" character ( / included)
	outpath="${outpath//rtraw/dst}"

	#inpath="${inpath%\";}"
	echo $outpath >> $jobOpt
	echo 'ApplicationMgr.EvtMax = -1;' >> $jobOpt
	done
