#!/bin/sh

rm sim/*
../makeJobOption_sim_particleGun.sh $PWD/sim/
rm rec/*
../makeJobOption_rec_particleGun.sh $PWD/sim/ $PWD/rec/
rm ana/*
../makeJobOption_ana_particleGun.sh $PWD/rec/ $PWD/ana/

rm SubScript_*
#../submitJobs_sim_particleGun.sh $PWD/sim/
#../submitJobs_rec_particleGun.sh $PWD/rec/
#../submitJobs_ana_particleGun.sh $PWD/ana/

../submitJobs_sim_particleGun.sh $PWD/sim/
NJobs=$(qstat | grep bgarillo | wc -l)
while [[ $NJobs -gt 0 ]]
    do
    sleep 30m
    NJobs=$(qstat | grep bgarillo | wc -l)
    echo "Total number of jobs in queue : $NJobs"
  done
../submitJobs_rec_particleGun.sh $PWD/rec/
while [[ $NJobs -gt 0 ]]
    do
    sleep 30m
    NJobs=$(qstat | grep bgarillo | wc -l)
    echo "Total number of jobs in queue : $NJobs"
  done
../submitJobs_ana_particleGun.sh $PWD/ana/		