#!/bin/sh

#Directory containing the jobOption files. 
#WARNING : 
#ABSOLUTE PATH ONLY! 
#The boss executable runs from cluster's node directory, so the full path of the directory must be given.

jobOptdir=$1 


for Y in `ls $jobOptdir*.txt `;
	do
	X="${Y##*/}" #Remove everything from Y string before "/" character ( / included)
	X="${X%.txt}" #Remove .txt suffix at the end of string
	X="${X##jobOptions_}" #Remove jobOptions_ suffix at the end of string
	#echo $X
	echo "#!/bin/bash" > SubScript_$X
  echo "#PBS -N Zdd_$X" >> SubScript_$X
  #echo "#PBS -q big" >> SubScript_$X
  #echo "#PBS -l walltime=2:00:00" >> SubScript_$X
	echo "#PBS -q long" >> SubScript_$X
  echo "#PBS -l walltime=24:00:00" >> SubScript_$X
	echo "#PBS -l mem=4096mb" >> SubScript_$X
	echo "#PBS -l vmem=4096mb" >> SubScript_$X
  echo "#PBS -j oe" >> SubScript_$X
  echo "#PBS -o /data/work/kphpbb/bgarillo/cZDD/particlegun/Logs/Zdd_$X.log" >> SubScript_$X
  echo "#PBS -V" >> SubScript_$X
  echo "#PBS -S /bin/bash" >> SubScript_$X
	echo >> SubScript_$X
	echo "cd /home/bgarillo" >> SubScript_$X
	echo "cd cmthome-6.6.4-ZDD/" >> SubScript_$X
	echo "source setup.sh" >> SubScript_$X
	echo >> SubScript_$X
  echo "export MyLocalWorkDir=/tmp/\$PBS_JOBID" >> SubScript_$X
  echo "echo \$MyLocalWorkDir" >> SubScript_$X
  echo "if [ -d \"\$MyLocalWorkDir\" ];" >> SubScript_$X
  echo "then " >> SubScript_$X
  echo "rm -rf \$MyLocalWorkDir" >> SubScript_$X
  echo "fi" >> SubScript_$X
  echo "mkdir \$MyLocalWorkDir" >> SubScript_$X
  echo >> SubScript_$X
	echo "cd \$MyLocalWorkDir" >> SubScript_$X
	echo "time boss.exe $Y" >> SubScript_$X
	Z=${X##'sim_'}
	echo $Z
	echo "cp Impact_reco.root /data/work/kphpbb/bgarillo/cZDD/particlegun/root/Impact_reco_$Z.root" >> SubScript_$X
	echo "Submit $X"
  qsub SubScript_$X
	done
