echo
echo "make jobOptions_ana_phokhara_beamPipe_leo_seed*.txt"
echo

for (( i = 35000; i <= 35000; i++))
do

echo '#include "$ROOTIOROOT/share/jobOptions_ReadRec.txt"' > jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$MAGNETICFIELDROOT/share/MagneticField.txt"' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$ABSCORROOT/share/jobOptions_AbsCor.txt"' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '//#include "$RHOPIALGROOT/share/jobOptions_Rhopi.txt"' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '//#include "$ANALYSIS_cZDD/share/jobOptions_analysis_cZdd.txt"' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$ANALYSIS_CZDDROOT/share/jobOptions_analysis_cZDD.txt"' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '// Input REC or DST file name' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo 'EventCnvSvc.digiRootInputFile = {"/graid/lkoch/cZDD_Simulations/phokhara_dst_files/phokhara_beamPipe_1000_leo_seed'$i'.dst"};' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo 'analysis_cZDD.OutFileName = "/graid/lkoch/cZDD_Simulations/phokhara_root_files/phokhara_beamPipe_1000_leo_seed'$i'.root";' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo 'MessageSvc.OutputLevel = 5;' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '// Number of events to be processed (default is 10)' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo 'ApplicationMgr.EvtMax = -1;' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo 'ApplicationMgr.HistogramPersistency = "ROOT";' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt
echo 'NTupleSvc.Output = { "FILE1 DATAFILE='phokhara_100000_leo.root' OPT='NEW' TYP='ROOT'"};' >> jobOptions_ana_phokhara_beamPipe_leo_seed$i.txt

done