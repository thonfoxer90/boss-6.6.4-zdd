echo
echo "make jobOptions_sim_phokhara_leo_seed*.txt"
echo

for (( i = 35000; i <= 35000; i++))
do

echo '#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"' > jobOptions_sim_phokhara_leo_seed$i.txt
echo 'ApplicationMgr.DLLs += { "Phokhara", "BesServices", "RootCnvSvc" };' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'ApplicationMgr.TopAlg += { "Phokhara" };' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.InitialSeed =          '$i'; //214156;// set initial random seed' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.InitialEvents =        50000;// # of events to determine the maximum of the integrand before true generation starts (?)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.NLO =                  0;     // Born(0), NLO(1)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.SoftPhotonCutoff =     0.001; // soft photon cutoff' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.Channel =              0;     // mu+mu-(0), pi+pi-(1),2pi0pi+pi-(2),2pi+2pi-(3),ppbar(4),' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '                                       // nnbar(5),' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '                                       // K+K-(6),K0K0bar(7),pi+pi-pi0(8),LambdaLambdaBar->pi-pi+ppbar(9)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.FSR =                  0;     // ISR only (0), ISR + FSR(1), ISR+INT+FSR(2)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.FSRNLO =               0;     // yes(1),no(0)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.VacuumPolarization =   0;     // yes(1),no(0)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.PionFormfactor =       0;     // KS Pionformfactor(0), GS Pionformfactor(1)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.F0model =              1;     // f0+f0(600): KK model(0), no structure (1), no f0+f0(600) (2), f0 KLOE(3)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.Ecm =                  3.770; // CMS energy (GeV), PsiPrime mass' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.CutHadInvMass =        0.25; //4.7785; //7.214; //10.150;//7.839;   //5.29; // minimal inv. mass squared of the hadrons (GeV^2)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.MaxHadInvMass =        3.70; //.152; //4.7790; //7.215; //10.152;//7.841; // maximal inv. mass squared of the hadrons (GeV^2)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.MinPhotonEnergy=       0.05; // in GeV ' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.MinPhotonAngle =       0.0855;  // 0.0855 - minimal theta angle on inner ZDD edge (point 0|+-0.5)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '                                // minimal photon angle/missing momentum angle (degree)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.MaxPhotonAngle =       0.689;//69;  // 0.689 - maximal theta angle on outer ZDD edge (origin in center of Zdd gap, no boost)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '                                //maximal photon angle/missing momentum angle (degree)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.MinHadronsAngle =      0.0; // minimal hadrons angle (degree)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'Phokhara.MaxHadronsAngle =      180.0;// maximal hardons angle (degree)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'BesSim.FullBeamPipe = 0;//1;' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '#include "$CALIBSVCROOT/share/calibConfig_sim.txt"' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'RealizationSvc.RunIdList = {-20448};' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'RootCnvSvc.digiRootOutputFile = "/graid/lkoch/cZDD_Simulations/phokhara_rtraw_files/phokhara_1000_leo_seed'$i'.rtraw";' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '// OUTPUT PRINTOUT LEVEL' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'MessageSvc.OutputLevel  = 5;' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo '// Number of events to be processed (default is 10)' >> jobOptions_sim_phokhara_leo_seed$i.txt
echo 'ApplicationMgr.EvtMax = 1000;' >> jobOptions_sim_phokhara_leo_seed$i.txt

done
