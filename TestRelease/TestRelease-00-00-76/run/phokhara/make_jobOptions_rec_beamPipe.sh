echo
echo "make jobOptions_rec_phokhara_leo_seed*.txt"
echo

for (( i = 35000; i <= 35000; i++))
do

echo '//TofRec.PrintOutInfo = true;' > jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//input ROOT MC data' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//#include "$ROOTIOROOT/share/jobOptions_ReadRoot.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$TESTRELEASEROOT/run/jobOptions_ReadRoot.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '// background mixing' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//#include "$BESEVENTMIXERROOT/share/jobOptions_EventMixer_rec.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$CALIBSVCROOT/share/job-CalibData.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//#include "$MAGNETICFIELDROOT/share/MagneticField.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//#include "$ESTIMEALGROOT/share/job_EsTimeAlg.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '// PAT+TSF method for MDC reconstruction' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//#include "$MDCXRECOROOT/share/jobOptions_MdcPatRec.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//#include "$KALFITALGROOT/share/job_kalfit_numf_data.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//#include "$MDCDEDXALGROOT/share/job_dedx_all.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$TRKEXTALGROOT/share/TrkExtAlgOption.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$TOFRECROOT/share/jobOptions_TofRec.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$TOFENERGYRECROOT/share/TofEnergyRecOptions_MC.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$EMCRECROOT/share/EmcRecOptions.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$MUCRECALGROOT/share/jobOptions_MucRec.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$PRIMARYVERTEXALGROOT/share/jobOptions_kalman.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$VEEVERTEXALGROOT/share/jobOptions_veeVertex.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$EVENTNAVIGATORROOT/share/EventNavigator.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$HLTMAKERALGROOT/share/jobOptions_HltMakerAlg.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//output ROOT REC data' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//#include "$ROOTIOROOT/share/jobOptions_Dst2Root.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$TESTRELEASEROOT/run/jobOptions_Dst2Root.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//configure of calibration constants for MC' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '#include "$CALIBSVCROOT/share/calibConfig_rec_mc.txt"' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//**************job options for random number************************' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo 'BesRndmGenSvc.RndmSeed = '$i';' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo 'MessageSvc.OutputLevel = 5;' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//ROOT input data file' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//EventCnvSvc.digiRootInputFile = {"phokhara_40000_nonresonantppbar_seed214156.rtraw"};' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//EventCnvSvc.digiRootInputFile = {"singleParticleGun_gamma_LeoTest.rtraw"};' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//EventCnvSvc.digiRootInputFile = {"phokhara_100000_leo.rtraw"};' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo 'EventCnvSvc.digiRootInputFile = {"/graid/lkoch/cZDD_Simulations/phokhara_rtraw_files/phokhara_beamPipe_1000_leo_seed'$i'.rtraw"};' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//ROOT output data file' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//EventCnvSvc.digiRootOutputFile ="phokhara_40000_nonresonantppbar_seed214156_digitizer.dst";' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//EventCnvSvc.digiRootOutputFile ="singleParticleGun_gamma_LeoTest.dst";' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//EventCnvSvc.digiRootOutputFile ="phokhara_100000_leo.dst";' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo 'EventCnvSvc.digiRootOutputFile ="/graid/lkoch/cZDD_Simulations/phokhara_dst_files/phokhara_beamPipe_1000_leo_seed'$i'.dst";' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo ' ' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo '//Number of events to be processed (default is 10)' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt
echo 'ApplicationMgr.EvtMax = -1; //with -1 all events are processed' >> jobOptions_rec_phokhara_beamPipe_leo_seed$i.txt

done