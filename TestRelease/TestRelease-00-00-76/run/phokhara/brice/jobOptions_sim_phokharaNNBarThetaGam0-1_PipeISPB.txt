#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"
ApplicationMgr.DLLs += { "Phokhara", "BesServices", "RootCnvSvc" };
ApplicationMgr.TopAlg += { "Phokhara" };
Phokhara.InitialSeed =          35000; //214156;// set initial random seed
Phokhara.InitialEvents =        50000;// # of events to determine the maximum of the integrand before true generation starts (?)
Phokhara.NLO =                  0;     // Born(0), NLO(1)
Phokhara.SoftPhotonCutoff =     0.001; // soft photon cutoff
Phokhara.Channel =              5;     // mu+mu-(0), pi+pi-(1),2pi0pi+pi-(2),2pi+2pi-(3),ppbar(4),
                                       // nnbar(5),
                                       // K+K-(6),K0K0bar(7),pi+pi-pi0(8),LambdaLambdaBar->pi-pi+ppbar(9)
Phokhara.FSR =                  0;     // ISR only (0), ISR + FSR(1), ISR+INT+FSR(2)
Phokhara.FSRNLO =               0;     // yes(1),no(0)
Phokhara.VacuumPolarization =   0;     // yes(1),no(0)
Phokhara.NarrowRes = 		0;     // none(0) jpsi(1) psip(2)
Phokhara.PionFormfactor =       0;     // KS Pionformfactor(0), GS Pionformfactor(1)
Phokhara.F0model =              1;     // f0+f0(600): KK model(0), no structure (1), no f0+f0(600) (2), f0 KLOE(3)
Phokhara.Ecm =                  3.770; // CMS energy (GeV), PsiPrime mass
Phokhara.CutHadInvMass =        0.0625; //4.7785; //7.214; //10.150;//7.839;   //5.29; // minimal inv. mass squared of the hadrons (GeV^2)
Phokhara.MaxHadInvMass =        13.69; //.152; //4.7790; //7.215; //10.152;//7.841; // maximal inv. mass squared of the hadrons (GeV^2)
Phokhara.MinPhotonEnergy=       0.05; // in GeV 
Phokhara.MinPhotonAngle =       0.;  // 0.0855 - minimal theta angle on inner ZDD edge (point 0|+-0.5)
                                // minimal photon angle/missing momentum angle (degree)
Phokhara.MaxPhotonAngle =       1.;//69;  // 0.689 - maximal theta angle on outer ZDD edge (origin in center of Zdd gap, no boost)
                                //maximal photon angle/missing momentum angle (degree)
Phokhara.MinHadronsAngle =      0.0; // minimal hadrons angle (degree)
Phokhara.MaxHadronsAngle =      180.0;// maximal hardons angle (degree)
#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"
BesSim.FullBeamPipe = 1;//1;
G4Svc.BoostLab = true;

#include "$CALIBSVCROOT/share/calibConfig_sim.txt"
RealizationSvc.RunIdList = {-20448};
#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "phokhara.rtraw";
MagneticFieldSvc.TurnOffISPB = false;
// OUTPUT PRINTOUT LEVEL
// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 5;
// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 1000;
