#!/bin/bash
##
## 26.4.2010 Björn Spruck
##

JOBDATE="$(date +%F-%H-%M-%S)"
CONFFILE="job_${JOBDATE}_$$.job"

### ./make_job_example.sh $@ > $CONFFILE
echo "#!/bin/bash" > $CONFFILE
echo "### set environment" >> $CONFFILE
env |sed 's#^#export #'|sed 's#=#="#' |sed 's#$#"#' >> $CONFFILE
echo "### set path" >> $CONFFILE
echo "cd $PWD" >> $CONFFILE
echo "### now run command" >> $CONFFILE
echo "$@" >> $CONFFILE

echo "running: qsub $PWD/$CONFFILE"
echo "which will run \"$@\" on batch node"
RESULT=$(qsub -m abe "$PWD/$CONFFILE")
echo $RESULT
JOBID=$(echo $RESULT|cut -d '.' -f 1)
echo "text output file is $PWD/$CONFFILE.o$JOBID"
echo "error output file is $PWD/$CONFFILE.e$JOBID"
