#include "$ROOTIOROOT/share/jobOptions_ReadRec.txt"
#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"
#include "$MAGNETICFIELDROOT/share/MagneticField.txt"
#include "$ABSCORROOT/share/jobOptions_AbsCor.txt"
//#include "$RHOPIALGROOT/share/jobOptions_Rhopi.txt"
//#include "$ANALYSIS_cZDD/share/jobOptions_analysis_cZdd.txt"
#include "$ANALYSIS_CZDDROOT/share/jobOptions_analysis_cZDD.txt"

// Input REC or DST file name
EventCnvSvc.digiRootInputFile = {"/graid/lkoch/cZDD_Simulations/particlegun_dst_files/particleGun_flat_newPipe_100000.dst"};

analysis_cZDD.OutFileName = "/graid/lkoch/cZDD_Simulations/particlegun_root_files/particleGun_flat_newPipe_100000.root";

// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = -1;

ApplicationMgr.HistogramPersistency = "ROOT";
NTupleSvc.Output = { "FILE1 DATAFILE=phokhara_100000_leo.root OPT=NEW TYP=ROOT"};
