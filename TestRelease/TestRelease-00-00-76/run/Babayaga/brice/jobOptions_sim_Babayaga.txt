#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"

ApplicationMgr.DLLs += {"Babayaga"};
ApplicationMgr.TopAlg += {"Babayaga"};

////////options for BABAYAGA
Babayaga.Channel=1;              // 1: e+e-->e+e-;2:e+e_->mu+mu-;3:e+e-->gamma gamma;4:e+e--->pi+pi-
Babayaga.Ebeam=1.886; 	// Ecm = 2*Ebeam [GeV]
Babayaga.Seed=62341342;    

/*           
Babayaga.MinThetaAngle=0.0573;       // minimum angle(deg.)
Babayaga.MaxThetaAngle=0.860;      //maximum angle(deg.)
*/

/*
Babayaga.MinThetaAngle=0.0573;       // minimum angle(deg.)
Babayaga.MaxThetaAngle=160;      //maximum angle(deg.)
*/

/*
Babayaga.MinThetaAngle=1;       // minimum angle(deg.)
Babayaga.MaxThetaAngle=180;      //maximum angle(deg.)
*/

Babayaga.MinThetaAngle=0.02;       // minimum angle(deg.)
Babayaga.MaxThetaAngle=10.;      //maximum angle(deg.)

Babayaga.MinimumEnergy=0.4;      //minimum energy  (GeV)
Babayaga.MaximumAcollinearity=180; //maximum acollinearity (deg.)
Babayaga.RunningAlpha=1;         //running alpha (0 = off, 1 = on)
//Babayaga.RunningAlpha=0;         //running alpha (0 = off, 1 = on)
Babayaga.HadronicResonance=0;    //hadronic resonances for ICH = 1 or 2
Babayaga.FSR_swich=0;            //FSR switch for ICH = 2 (0 = off, 1 = on)
Babayaga.MinEnerCutG=0.1;        //minimum energy for CUTG = Y (GeV)
Babayaga.MinAngCutG=5;           //minimum angle for CUTG = Y  (deg.)
Babayaga.MaxAngCutG=21;          //maximum angle for CUTG = Y  (deg.)
Babayaga.HBOOK = 1;              // close babayaga.ntuple hbook output
Babayaga.PHCUT = 0;
Babayaga.CUTG  = 0;
//Babayaga.CUTG  = 1;

////**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

////**************job options for detector simulation******************

#include "$BESSIMROOT/share/G4Svc_BesSim.txt"
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

/// run ID
RealizationSvc.RunIdList = {-20448};//, 0, -23454}; // 2011 data of psi''
//RealizationSvc.RunIdList = {-11414, 0, -13988, -14395, 0, -14604, -20448, 0, -23454}; //RUN Number for psipp data

#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"
//RootCnvSvc.digiRootOutputFile = "/data/work/kphpbb/bgarillo/cZDD/BabayagaNLO/rtraw/ee.rtraw";
RootCnvSvc.digiRootOutputFile = "ee.rtraw";

//// OUTPUT PRINTOUT LEVEL
//// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel  = 5;

#include "$REALIZATIONSVCROOT/share/jobOptions_Realization.txt"
#include "$BESSIMROOT/share/Bes_Gen.txt"

//// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 100;



//DatabaseSvc.DbType="MySql";
//#include "$CALIBSVCROOT/share/job-CalibData.txt"
//ApplicationMgr.HistogramPersistency = "ROOT";
//NTupleSvc.Output={"FILE1 FILE='genOnly.root' OPT='NEW' TYP='ROOT'"};
