   

  
  PROCESS: e+ e- ----> e+ e- 
  
  EBEAM  =  1.8860 GeV
  ANGULAR CUTS   0.0800 - 179.9200 deg.
  EMIN   =  0.4000 GeV
  ACOLL. = 180.0000 deg.
  
  FSR is switched on
  Alpha is running
  Hadronic resonances are not included
  No cuts are imposed on photons
  
  RANLUX SEED =  62341342
  OUTPUT FILE = CrossSection.txt    
  
  EVENTS STORED IN BABAYAGA.ntuple     
  
 ----------------- OUTPUTS --------------------
  
  EVENT NUMBER          100  OF          100
  
  Monte Carlo efficiency: 0.21459
   
  UNWEIGHTED CROSS SECTION
**************   +-************** (nb)
   
  WEIGHTED CROSS SECTION
**************   +-446297.5622521 (nb)
   
