#include "$KKMCROOT/share/jobOptions_KKMC.txt"

//*************job options for generator (KKMC)***************
KKMC.CMSEnergy = 3.773; //BOSS gives warning in case of inconsistency with .out file above
KKMC.BeamEnergySpread=0.0;
KKMC.GeneratePsi3770=true;
KKMC.ParticleDecayThroughEvtGen = true;
KKMC.ThresholdCut=0.0;
KKMC.RadiationCorrection = false;

#include "$MANUALGENERATORROOT/share/ManualGenerator.txt"
ManualGenerator.InputFile ="$MyLocalWorkDir/galuga.out";

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100;

//**************job options for detector simulation******************
#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"
BesSim.ZddMaterial = "LYSO";
BesSim.ZddSensor = "SenslC";
BesSim.FullBeamPipe = 1;
G4Svc.BoostLab = true;

//configure for calibration constants
#include "$CALIBSVCROOT/share/calibConfig_sim.txt"

RealizationSvc.RunIdList = {-20448};

#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "$MyLocalWorkDir/galuga.rtraw";
MagneticFieldSvc.TurnOffISPB = false;

MessageSvc.OutputLevel  = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = 10 ; //Number of events to be simulated
