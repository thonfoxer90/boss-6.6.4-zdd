#include "$ROOTIOROOT/share/jobOptions_ReadRec.txt"
#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"
#include "$MAGNETICFIELDROOT/share/MagneticField.txt"
#include "$ABSCORROOT/share/jobOptions_AbsCor.txt"
#include "$ANALYSIS_CZDDROOT/share/jobOptions_analysis_cZDD.txt"

EventCnvSvc.digiRootInputFile={"$MyLocalWorkDir/galuga.dst"};
analysis_cZDD.KKMC = 1;
analysis_cZDD.InputImpact_reco = "/data/work/kphpbb/bgarillo/cZDD/Galuga2.0/NTuples/Impact_reco_galuga_PipeISPB_100026.root";
analysis_cZDD.OutFileName = "/data/work/kphpbb/bgarillo/cZDD/Galuga2.0/NTuples/galuga_ana_PipeISPB_100026.root";
MessageSvc.OutputLevel = 5;
ApplicationMgr.EvtMax = -1;
ApplicationMgr.HistogramPersistency = "ROOT";
NTupleSvc.Output = {"FILE1 DATAFILE='$MyLocalWorkDir/ntgaluga.root' OPT='NEW' TYP='ROOT'"};
