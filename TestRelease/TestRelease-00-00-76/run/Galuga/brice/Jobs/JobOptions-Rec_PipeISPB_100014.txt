#include "$ROOTIOROOT/share/jobOptions_ReadRoot.txt"
#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"
#include "$BESEVENTMIXERROOT/share/jobOptions_EventMixer_rec.txt"
MixerAlg.ReplaceDataPath = "/data/group/bes3/bes3data/offline/data/fltrandomtrg/";
#include "$CALIBSVCROOT/share/job-CalibData.txt"
#include "$MAGNETICFIELDROOT/share/MagneticField.txt"
#include "$ESTIMEALGROOT/share/job_EsTimeAlg.txt"
#include "$MDCXRECOROOT/share/jobOptions_MdcPatTsfRec.txt"
#include "$KALFITALGROOT/share/job_kalfit_numf_data.txt"
#include "$MDCDEDXALGROOT/share/job_dedx_all.txt"
#include "$TRKEXTALGROOT/share/TrkExtAlgOption.txt"
#include "$TOFRECROOT/share/jobOptions_TofRec.txt"
#include "$TOFENERGYRECROOT/share/TofEnergyRecOptions_MC.txt"
#include "$EMCRECROOT/share/EmcRecOptions.txt"
#include "$MUCRECALGROOT/share/jobOptions_MucRec.txt"
#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"
#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"
#include "$VEEVERTEXALGROOT/share/jobOptions_veeVertex.txt"
#include "$HLTMAKERALGROOT/share/jobOptions_HltMakerAlg.txt"
#include "$EVENTNAVIGATORROOT/share/EventNavigator.txt"
#include "$ROOTIOROOT/share/jobOptions_Dst2Root.txt"
#include "$CALIBSVCROOT/share/calibConfig_rec_mc.txt"

BesRndmGenSvc.RndmSeed = 100;

MessageSvc.OutputLevel = 5;

EventCnvSvc.digiRootInputFile = {"$MyLocalWorkDir/galuga.rtraw"};

EventCnvSvc.digiRootOutputFile ="$MyLocalWorkDir/galuga.dst";

ApplicationMgr.EvtMax = 10 ;
