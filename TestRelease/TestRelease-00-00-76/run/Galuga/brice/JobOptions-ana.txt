#include "$ROOTIOROOT/share/jobOptions_ReadRec.txt"
#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"
#include "$MAGNETICFIELDROOT/share/MagneticField.txt"
#include "$ABSCORROOT/share/jobOptions_AbsCor.txt"
#include "$ANALYSIS_CZDDROOT/share/jobOptions_analysis_cZDD.txt"

EventCnvSvc.digiRootInputFile={"galuga.dst"};
analysis_cZDD.KKMC = 1;
analysis_cZDD.InputImpact_reco = "Impact_reco.root";
analysis_cZDD.OutFileName = "galuga.root";
MessageSvc.OutputLevel = 5;
ApplicationMgr.EvtMax = -1;
ApplicationMgr.HistogramPersistency = "ROOT";
NTupleSvc.Output = {"FILE1 DATAFILE='ntgaluga.root' OPT='NEW' TYP='ROOT'"};
