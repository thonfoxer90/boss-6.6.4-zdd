#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"

//*************job options for Ekhara ***************
ApplicationMgr.DLLs += { "Ekhara", "BesServices", "RootCnvSvc" };
ApplicationMgr.TopAlg += {"Ekhara"};
Ekhara.Ecm = 3.773;
Ekhara.InitialEvents = 100000; // # of events to determine the maximum 
Ekhara.FinalStateID = 3; // 1:pi+pi- 2:pi0 3:eta 4:etaprime
Ekhara.MesonProdAmplitudes = 2;
Ekhara.MesonFormfactor = 8;
Ekhara.PositronThetaMin = 0.0;
Ekhara.PositronThetaMax = 180.0;
Ekhara.PositronEnergyMin = 0.0;
Ekhara.PositronEnergyMax = 110.0;
Ekhara.ElectronThetaMin = 0.0;
Ekhara.ElectronThetaMax = 180.0;
Ekhara.ElectronEnergyMin = 0.0;
Ekhara.ElectronEnergyMax = 110.0;
Ekhara.TaggingAngle = 20.0;//!- Tagging angle (symmetric) in degrees; relevant for TaggingMode 1,2,3 
Ekhara.TaggingMode = 0; //!- 0:no tagging; 1:untagged; 2: single tagging; 3:double

//**************job options for random number************************
BesRndmGenSvc.RndmSeed = 100029;

//**************job options for detector simulation******************
ApplicationMgr.DLLs   += {"BesServices"};
#include "$REALIZATIONSVCROOT/share/jobOptions_Realization.txt"
#include "$BESSIMROOT/share/Bes_Gen.txt"
DatabaseSvc.DbType="MySql";
#include "$TESTRELEASEROOT/run/G4Svc_BesSim.txt"
BesSim.ZddMaterial = "LYSO";
BesSim.ZddSensor = "SenslC";
BesSim.FullBeamPipe = 1;
G4Svc.BoostLab = true;

#include "$CALIBSVCROOT/share/calibConfig_sim.txt"
//RealizationSvc.RunIdList = {-20448};
RealizationSvc.RunIdList = {-11414, 0, -13988, -14395, 0, -14604, -20448, 0, -23454};
#include "$TESTRELEASEROOT/run/jobOptions_Digi2Root.txt"
RootCnvSvc.digiRootOutputFile = "Ekhara.rtraw";
MagneticFieldSvc.TurnOffISPB = false;
MessageSvc.OutputLevel  = 5;
ApplicationMgr.EvtMax =  100 ;
