ApplicationMgr.DLLs += { "Bhlumi" };
ApplicationMgr.TopAlg += { "Bhlumi" };

Bhlumi.CMEnergy      = 3.097; // 2*Ebeam [GeV]
//Bhlumi.MinThetaAngle = 0.104; // [rad]
//Bhlumi.MaxThetaAngle = 0.245; // [rad]
Bhlumi.AngleMode       = 2;   //0: rad; 1: degree; 2: cos(theta);
Bhlumi.MinThetaAngle   = 0.95; 
Bhlumi.MaxThetaAngle   = -0.95; 
Bhlumi.InitializedSeed = {54217137, 0, 0}; //
Bhlumi.SoftPhotonCut = 1E-4;  // Ephoton > SoftPhotonCut*SQRT(s)/2
