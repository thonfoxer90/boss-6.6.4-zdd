//$id$
/*
 *    2005/10/31   Zhengyun You      Peking University
 *                 Zdd Geometry General for Simulation
 *                 inherit from class SubDetectorG4Geo
 */

#ifndef ZDD_G4_GEO_H
#define ZDD_G4_GEO_H

#include "SubDetectorG4Geo.h"

/**
 * Class ZddGeo contains all of the objects necessary to describe the
 * muc geometry.
 *
 * @author Zhengyun You \URL{mailto:youzy@hep.pku.cn}
 *
 */

class ZddG4Geo : public SubDetectorG4Geo
{
 public:
  
  /// Constructor.
  ZddG4Geo();

  /// Destructor.
  ~ZddG4Geo();

  /// Get a pointer to the single instance of ZddG4Geo
  static ZddG4Geo *Instance();

  /// Initialize the instance of G4Geo.
  void InitFromGdml( const char *gdmlFile, const char *setupName );
  
  /// Set default visual attributes;
  void SetDefaultVis();

 private:

  // Pointer to the instance of ZddG4Geo.
  static ZddG4Geo *m_pZddG4Geo;
};

#endif   /* ZDD_G4_GEO_H */
