#-- start of make_header -----------------

#====================================
#  Library G4GeoLib
#
<<<<<<< HEAD
#   Generated Sat May 19 14:50:04 2018  by bgarillo
=======
#   Generated Sun May 20 13:38:23 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_G4GeoLib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_G4GeoLib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_G4GeoLib

G4Geo_tag = $(tag)

#cmt_local_tagfile_G4GeoLib = $(G4Geo_tag)_G4GeoLib.make
cmt_local_tagfile_G4GeoLib = $(bin)$(G4Geo_tag)_G4GeoLib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

G4Geo_tag = $(tag)

#cmt_local_tagfile_G4GeoLib = $(G4Geo_tag).make
cmt_local_tagfile_G4GeoLib = $(bin)$(G4Geo_tag).make

endif

include $(cmt_local_tagfile_G4GeoLib)
#-include $(cmt_local_tagfile_G4GeoLib)

ifdef cmt_G4GeoLib_has_target_tag

cmt_final_setup_G4GeoLib = $(bin)setup_G4GeoLib.make
#cmt_final_setup_G4GeoLib = $(bin)G4Geo_G4GeoLibsetup.make
cmt_local_G4GeoLib_makefile = $(bin)G4GeoLib.make

else

cmt_final_setup_G4GeoLib = $(bin)setup.make
#cmt_final_setup_G4GeoLib = $(bin)G4Geosetup.make
cmt_local_G4GeoLib_makefile = $(bin)G4GeoLib.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)G4Geosetup.make

#G4GeoLib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'G4GeoLib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = G4GeoLib/
#G4GeoLib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

G4GeoLiblibname   = $(bin)$(library_prefix)G4GeoLib$(library_suffix)
G4GeoLiblib       = $(G4GeoLiblibname).a
G4GeoLibstamp     = $(bin)G4GeoLib.stamp
G4GeoLibshstamp   = $(bin)G4GeoLib.shstamp

G4GeoLib :: dirs  G4GeoLibLIB
	$(echo) "G4GeoLib ok"

#-- end of libary_header ----------------

G4GeoLibLIB :: $(G4GeoLiblib) $(G4GeoLibshstamp)
	@/bin/echo "------> G4GeoLib : library ok"

<<<<<<< HEAD
$(G4GeoLiblib) :: $(bin)EmcG4Geo.o $(bin)MucG4Geo.o $(bin)TofG4Geo.o $(bin)ZddG4Geo.o $(bin)MdcG4Geo.o $(bin)BesG4Geo.o $(bin)SubDetectorG4Geo.o
=======
$(G4GeoLiblib) :: $(bin)BesG4Geo.o $(bin)EmcG4Geo.o $(bin)MdcG4Geo.o $(bin)MucG4Geo.o $(bin)SubDetectorG4Geo.o $(bin)TofG4Geo.o $(bin)ZddG4Geo.o
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(G4GeoLiblib) $?
	$(lib_silent) $(ranlib) $(G4GeoLiblib)
	$(lib_silent) cat /dev/null >$(G4GeoLibstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(G4GeoLiblibname).$(shlibsuffix) :: $(G4GeoLiblib) $(G4GeoLibstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" G4GeoLib $(G4GeoLib_shlibflags)

$(G4GeoLibshstamp) :: $(G4GeoLiblibname).$(shlibsuffix)
	@if test -f $(G4GeoLiblibname).$(shlibsuffix) ; then cat /dev/null >$(G4GeoLibshstamp) ; fi

G4GeoLibclean ::
	$(cleanup_echo) objects
<<<<<<< HEAD
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)EmcG4Geo.o $(bin)MucG4Geo.o $(bin)TofG4Geo.o $(bin)ZddG4Geo.o $(bin)MdcG4Geo.o $(bin)BesG4Geo.o $(bin)SubDetectorG4Geo.o
=======
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)BesG4Geo.o $(bin)EmcG4Geo.o $(bin)MdcG4Geo.o $(bin)MucG4Geo.o $(bin)SubDetectorG4Geo.o $(bin)TofG4Geo.o $(bin)ZddG4Geo.o
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
G4GeoLibinstallname = $(library_prefix)G4GeoLib$(library_suffix).$(shlibsuffix)

G4GeoLib :: G4GeoLibinstall

install :: G4GeoLibinstall

G4GeoLibinstall :: $(install_dir)/$(G4GeoLibinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(G4GeoLibinstallname) :: $(bin)$(G4GeoLibinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(G4GeoLibinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(G4GeoLibinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(G4GeoLibinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(G4GeoLibinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(G4GeoLibinstallname) $(install_dir)/$(G4GeoLibinstallname); \
	      echo `pwd`/$(G4GeoLibinstallname) >$(install_dir)/$(G4GeoLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(G4GeoLibinstallname), no installation directory specified"; \
	  fi; \
	fi

G4GeoLibclean :: G4GeoLibuninstall

uninstall :: G4GeoLibuninstall

G4GeoLibuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(G4GeoLibinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(G4GeoLibinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(G4GeoLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(G4GeoLibinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),G4GeoLibclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)G4GeoLib_dependencies.make :: dirs

ifndef QUICK
<<<<<<< HEAD
$(bin)G4GeoLib_dependencies.make : $(src)EmcG4Geo.cxx $(src)MucG4Geo.cxx $(src)TofG4Geo.cxx $(src)ZddG4Geo.cxx $(src)MdcG4Geo.cxx $(src)BesG4Geo.cxx $(src)SubDetectorG4Geo.cxx $(use_requirements) $(cmt_final_setup_G4GeoLib)
	$(echo) "(G4GeoLib.make) Rebuilding $@"; \
	  $(build_dependencies) G4GeoLib -all_sources -out=$@ $(src)EmcG4Geo.cxx $(src)MucG4Geo.cxx $(src)TofG4Geo.cxx $(src)ZddG4Geo.cxx $(src)MdcG4Geo.cxx $(src)BesG4Geo.cxx $(src)SubDetectorG4Geo.cxx
=======
$(bin)G4GeoLib_dependencies.make : $(src)BesG4Geo.cxx $(src)EmcG4Geo.cxx $(src)MdcG4Geo.cxx $(src)MucG4Geo.cxx $(src)SubDetectorG4Geo.cxx $(src)TofG4Geo.cxx $(src)ZddG4Geo.cxx $(use_requirements) $(cmt_final_setup_G4GeoLib)
	$(echo) "(G4GeoLib.make) Rebuilding $@"; \
	  $(build_dependencies) G4GeoLib -all_sources -out=$@ $(src)BesG4Geo.cxx $(src)EmcG4Geo.cxx $(src)MdcG4Geo.cxx $(src)MucG4Geo.cxx $(src)SubDetectorG4Geo.cxx $(src)TofG4Geo.cxx $(src)ZddG4Geo.cxx
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
endif

#$(G4GeoLib_dependencies)

-include $(bin)G4GeoLib_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4GeoLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EmcG4Geo.d

$(bin)$(binobj)EmcG4Geo.d : $(use_requirements) $(cmt_final_setup_G4GeoLib)

$(bin)$(binobj)EmcG4Geo.d : $(src)EmcG4Geo.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/EmcG4Geo.dep $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(EmcG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(EmcG4Geo_cppflags) $(EmcG4Geo_cxx_cppflags)  $(src)EmcG4Geo.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/EmcG4Geo.o $(src)EmcG4Geo.cxx $(@D)/EmcG4Geo.dep
endif
endif

$(bin)$(binobj)EmcG4Geo.o : $(src)EmcG4Geo.cxx
else
$(bin)G4GeoLib_dependencies.make : $(EmcG4Geo_cxx_dependencies)

$(bin)$(binobj)EmcG4Geo.o : $(EmcG4Geo_cxx_dependencies)
endif
	$(cpp_echo) $(src)EmcG4Geo.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(EmcG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(EmcG4Geo_cppflags) $(EmcG4Geo_cxx_cppflags)  $(src)EmcG4Geo.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4GeoLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MucG4Geo.d

$(bin)$(binobj)MucG4Geo.d : $(use_requirements) $(cmt_final_setup_G4GeoLib)

$(bin)$(binobj)MucG4Geo.d : $(src)MucG4Geo.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MucG4Geo.dep $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(MucG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(MucG4Geo_cppflags) $(MucG4Geo_cxx_cppflags)  $(src)MucG4Geo.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MucG4Geo.o $(src)MucG4Geo.cxx $(@D)/MucG4Geo.dep
endif
endif

$(bin)$(binobj)MucG4Geo.o : $(src)MucG4Geo.cxx
else
$(bin)G4GeoLib_dependencies.make : $(MucG4Geo_cxx_dependencies)

$(bin)$(binobj)MucG4Geo.o : $(MucG4Geo_cxx_dependencies)
endif
	$(cpp_echo) $(src)MucG4Geo.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(MucG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(MucG4Geo_cppflags) $(MucG4Geo_cxx_cppflags)  $(src)MucG4Geo.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4GeoLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TofG4Geo.d

$(bin)$(binobj)TofG4Geo.d : $(use_requirements) $(cmt_final_setup_G4GeoLib)

$(bin)$(binobj)TofG4Geo.d : $(src)TofG4Geo.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/TofG4Geo.dep $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(TofG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(TofG4Geo_cppflags) $(TofG4Geo_cxx_cppflags)  $(src)TofG4Geo.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/TofG4Geo.o $(src)TofG4Geo.cxx $(@D)/TofG4Geo.dep
endif
endif

$(bin)$(binobj)TofG4Geo.o : $(src)TofG4Geo.cxx
else
$(bin)G4GeoLib_dependencies.make : $(TofG4Geo_cxx_dependencies)

$(bin)$(binobj)TofG4Geo.o : $(TofG4Geo_cxx_dependencies)
endif
	$(cpp_echo) $(src)TofG4Geo.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(TofG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(TofG4Geo_cppflags) $(TofG4Geo_cxx_cppflags)  $(src)TofG4Geo.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4GeoLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
<<<<<<< HEAD
-include $(bin)$(binobj)ZddG4Geo.d
=======
-include $(bin)$(binobj)MucG4Geo.d

$(bin)$(binobj)MucG4Geo.d : $(use_requirements) $(cmt_final_setup_G4GeoLib)

$(bin)$(binobj)MucG4Geo.d : $(src)MucG4Geo.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MucG4Geo.dep $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(MucG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(MucG4Geo_cppflags) $(MucG4Geo_cxx_cppflags)  $(src)MucG4Geo.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MucG4Geo.o $(src)MucG4Geo.cxx $(@D)/MucG4Geo.dep
endif
endif

$(bin)$(binobj)MucG4Geo.o : $(src)MucG4Geo.cxx
else
$(bin)G4GeoLib_dependencies.make : $(MucG4Geo_cxx_dependencies)

$(bin)$(binobj)MucG4Geo.o : $(MucG4Geo_cxx_dependencies)
endif
	$(cpp_echo) $(src)MucG4Geo.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(MucG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(MucG4Geo_cppflags) $(MucG4Geo_cxx_cppflags)  $(src)MucG4Geo.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4GeoLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)SubDetectorG4Geo.d
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19

$(bin)$(binobj)ZddG4Geo.d : $(use_requirements) $(cmt_final_setup_G4GeoLib)

$(bin)$(binobj)ZddG4Geo.d : $(src)ZddG4Geo.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ZddG4Geo.dep $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(ZddG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(ZddG4Geo_cppflags) $(ZddG4Geo_cxx_cppflags)  $(src)ZddG4Geo.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ZddG4Geo.o $(src)ZddG4Geo.cxx $(@D)/ZddG4Geo.dep
endif
endif

$(bin)$(binobj)ZddG4Geo.o : $(src)ZddG4Geo.cxx
else
$(bin)G4GeoLib_dependencies.make : $(ZddG4Geo_cxx_dependencies)

$(bin)$(binobj)ZddG4Geo.o : $(ZddG4Geo_cxx_dependencies)
endif
	$(cpp_echo) $(src)ZddG4Geo.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(ZddG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(ZddG4Geo_cppflags) $(ZddG4Geo_cxx_cppflags)  $(src)ZddG4Geo.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4GeoLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MdcG4Geo.d

$(bin)$(binobj)MdcG4Geo.d : $(use_requirements) $(cmt_final_setup_G4GeoLib)

$(bin)$(binobj)MdcG4Geo.d : $(src)MdcG4Geo.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MdcG4Geo.dep $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(MdcG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(MdcG4Geo_cppflags) $(MdcG4Geo_cxx_cppflags)  $(src)MdcG4Geo.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MdcG4Geo.o $(src)MdcG4Geo.cxx $(@D)/MdcG4Geo.dep
endif
endif

$(bin)$(binobj)MdcG4Geo.o : $(src)MdcG4Geo.cxx
else
$(bin)G4GeoLib_dependencies.make : $(MdcG4Geo_cxx_dependencies)

$(bin)$(binobj)MdcG4Geo.o : $(MdcG4Geo_cxx_dependencies)
endif
	$(cpp_echo) $(src)MdcG4Geo.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(MdcG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(MdcG4Geo_cppflags) $(MdcG4Geo_cxx_cppflags)  $(src)MdcG4Geo.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4GeoLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
<<<<<<< HEAD
-include $(bin)$(binobj)BesG4Geo.d

$(bin)$(binobj)BesG4Geo.d : $(use_requirements) $(cmt_final_setup_G4GeoLib)

$(bin)$(binobj)BesG4Geo.d : $(src)BesG4Geo.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/BesG4Geo.dep $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(BesG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(BesG4Geo_cppflags) $(BesG4Geo_cxx_cppflags)  $(src)BesG4Geo.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/BesG4Geo.o $(src)BesG4Geo.cxx $(@D)/BesG4Geo.dep
endif
endif

$(bin)$(binobj)BesG4Geo.o : $(src)BesG4Geo.cxx
else
$(bin)G4GeoLib_dependencies.make : $(BesG4Geo_cxx_dependencies)

$(bin)$(binobj)BesG4Geo.o : $(BesG4Geo_cxx_dependencies)
endif
	$(cpp_echo) $(src)BesG4Geo.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(BesG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(BesG4Geo_cppflags) $(BesG4Geo_cxx_cppflags)  $(src)BesG4Geo.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),G4GeoLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)SubDetectorG4Geo.d
=======
-include $(bin)$(binobj)ZddG4Geo.d
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19

$(bin)$(binobj)SubDetectorG4Geo.d : $(use_requirements) $(cmt_final_setup_G4GeoLib)

$(bin)$(binobj)SubDetectorG4Geo.d : $(src)SubDetectorG4Geo.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/SubDetectorG4Geo.dep $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(SubDetectorG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(SubDetectorG4Geo_cppflags) $(SubDetectorG4Geo_cxx_cppflags)  $(src)SubDetectorG4Geo.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/SubDetectorG4Geo.o $(src)SubDetectorG4Geo.cxx $(@D)/SubDetectorG4Geo.dep
endif
endif

$(bin)$(binobj)SubDetectorG4Geo.o : $(src)SubDetectorG4Geo.cxx
else
$(bin)G4GeoLib_dependencies.make : $(SubDetectorG4Geo_cxx_dependencies)

$(bin)$(binobj)SubDetectorG4Geo.o : $(SubDetectorG4Geo_cxx_dependencies)
endif
	$(cpp_echo) $(src)SubDetectorG4Geo.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(G4GeoLib_pp_cppflags) $(lib_G4GeoLib_pp_cppflags) $(SubDetectorG4Geo_pp_cppflags) $(use_cppflags) $(G4GeoLib_cppflags) $(lib_G4GeoLib_cppflags) $(SubDetectorG4Geo_cppflags) $(SubDetectorG4Geo_cxx_cppflags)  $(src)SubDetectorG4Geo.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: G4GeoLibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(G4GeoLib.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(G4GeoLib.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(G4GeoLib.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(G4GeoLib.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_G4GeoLib)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(G4GeoLib.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(G4GeoLib.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(G4GeoLib.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

G4GeoLibclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library G4GeoLib
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)G4GeoLib$(library_suffix).a $(library_prefix)G4GeoLib$(library_suffix).s? G4GeoLib.stamp G4GeoLib.shstamp
#-- end of cleanup_library ---------------
