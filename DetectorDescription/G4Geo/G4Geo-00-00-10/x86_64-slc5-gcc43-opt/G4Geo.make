#-- start of make_header -----------------

#====================================
#  Library G4Geo
#
<<<<<<< HEAD
#   Generated Sat May 19 14:50:10 2018  by bgarillo
=======
#   Generated Sun May 20 13:38:33 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_G4Geo_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_G4Geo_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_G4Geo

G4Geo_tag = $(tag)

#cmt_local_tagfile_G4Geo = $(G4Geo_tag)_G4Geo.make
cmt_local_tagfile_G4Geo = $(bin)$(G4Geo_tag)_G4Geo.make

else

tags      = $(tag),$(CMTEXTRATAGS)

G4Geo_tag = $(tag)

#cmt_local_tagfile_G4Geo = $(G4Geo_tag).make
cmt_local_tagfile_G4Geo = $(bin)$(G4Geo_tag).make

endif

include $(cmt_local_tagfile_G4Geo)
#-include $(cmt_local_tagfile_G4Geo)

ifdef cmt_G4Geo_has_target_tag

cmt_final_setup_G4Geo = $(bin)setup_G4Geo.make
#cmt_final_setup_G4Geo = $(bin)G4Geo_G4Geosetup.make
cmt_local_G4Geo_makefile = $(bin)G4Geo.make

else

cmt_final_setup_G4Geo = $(bin)setup.make
#cmt_final_setup_G4Geo = $(bin)G4Geosetup.make
cmt_local_G4Geo_makefile = $(bin)G4Geo.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)G4Geosetup.make

#G4Geo :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'G4Geo'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = G4Geo/
#G4Geo::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

G4Geolibname   = $(bin)$(library_prefix)G4Geo$(library_suffix)
G4Geolib       = $(G4Geolibname).a
G4Geostamp     = $(bin)G4Geo.stamp
G4Geoshstamp   = $(bin)G4Geo.shstamp

G4Geo :: dirs  G4GeoLIB
	$(echo) "G4Geo ok"

#-- end of libary_header ----------------
#-- start of cleanup_header --------------

clean :: G4Geoclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(G4Geo.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(G4Geo.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(G4Geo.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(G4Geo.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_G4Geo)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(G4Geo.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(G4Geo.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(G4Geo.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

G4Geoclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library G4Geo
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)G4Geo$(library_suffix).a $(library_prefix)G4Geo$(library_suffix).s? G4Geo.stamp G4Geo.shstamp
#-- end of cleanup_library ---------------
