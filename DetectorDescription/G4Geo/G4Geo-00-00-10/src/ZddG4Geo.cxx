//$id$
/*
 *    2005/10/30   Zhengyun You      Peking University
 *                 Zdd Geometry General for Simulation
 *                 inherit from class SubDetectorG4Geo
 */

using namespace std;

#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <iomanip>

#include "Identifier/ZddID.h"
#include "G4Geo/ZddG4Geo.h"


ZddG4Geo*
ZddG4Geo::m_pZddG4Geo = 0L;


ZddG4Geo::ZddG4Geo()
{ 
  string GdmlManagementPath = getenv("GDMLMANAGEMENTROOT");
  if (GdmlManagementPath == "") cout << "ZddG4Geo::GdmlManagementPath not set" << endl;
  string GdmlFile = GdmlManagementPath + string("/dat/Zdd.gdml");
  cout << "Construct Zdd from GdmlFile " << GdmlFile << endl;
  InitFromGdml( GdmlFile.c_str(), "Zdd" );
}

ZddG4Geo::~ZddG4Geo()
{ }

void
ZddG4Geo::InitFromGdml( const char *gdmlFile, const char *setupName )
{
  ReadGdml(gdmlFile, setupName);
  m_TopVolume = GetLogicalVolume("logicalZdd");
  if (!m_TopVolume) cout << "ZddG4Geo::InitFromGdml, m_TopVolume not found" << endl;
  else cout << "Zdd TopVolume name " << m_TopVolume->GetName() << endl;
  SetDefaultVis();

  m_G4GeoInit = 1;  
}

void 
ZddG4Geo::SetDefaultVis()
{
  //std::cout << "begin of set defaultvis" << std::endl;
  
  //std::cout << "end of set defaultvis" << std::endl;
}

ZddG4Geo*
ZddG4Geo::Instance()
{
  //Get a pointer to the single instance of ZddG4Geo
  if(!m_pZddG4Geo){
    m_pZddG4Geo = new ZddG4Geo;
    //cout<<"in ZddG4Geo:: no ZddG4Geo now."<<m_pZddG4Geo<<endl;

  }
  //cout<<"in ZddG4Geo:: get ZddG4Geo successfully."<<endl;
  return m_pZddG4Geo;
}
