if test "${CMTROOT}" = ""; then
  CMTROOT=/home/bes/boss_6.6.4_p01/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt cleanup -sh -pack=GdmlManagement -version=GdmlManagement-00-00-30 -path=/home/lkoch/wa/DetectorDescription $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

