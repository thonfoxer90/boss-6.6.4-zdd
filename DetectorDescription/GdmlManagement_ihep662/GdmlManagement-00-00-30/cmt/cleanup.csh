if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /home/bes/boss_6.6.4_p01/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh
set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt cleanup -csh -pack=GdmlManagement -version=GdmlManagement-00-00-30 -path=/home/lkoch/wa/DetectorDescription $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

