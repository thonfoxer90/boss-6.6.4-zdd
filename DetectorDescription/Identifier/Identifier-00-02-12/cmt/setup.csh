# echo "Setting Identifier Identifier-00-02-12 in /home/bgarillo/boss-6.6.4-ZDD/DetectorDescription"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cluster/bes3/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=Identifier -version=Identifier-00-02-12 -path=/home/bgarillo/boss-6.6.4-ZDD/DetectorDescription  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

