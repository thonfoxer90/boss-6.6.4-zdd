#ifndef __TofID_h__
#define __TofID_h__

#include "Identifier/BesDetectorID.h"
#include <string>
#include <assert.h>

class TofID : public BesDetectorID
{
public:
  typedef Identifier::size_type size_type; 
  typedef Identifier::value_type value_type; 

  /// constructor
  TofID();

  /// destructor 
  ~TofID();
 
  /// For a single crystal
 static Identifier cell_id ( int barrel_ec,
                             int layer, 
                             int phi_module,   
			     int end
			      );
 static value_type getIntID ( int barrel_ec,
                             int layer,
                             int phi_module,
                             int end
                              );
 static value_type getLAYER_BARREL_MAX();
 static value_type getLAYER_ENDCAP_MAX();
 static value_type getPHI_BARREL_MAX();
 static value_type getPHI_ENDCAP_MAX();
 static value_type getBARREL_EC_MAX();
 static value_type getBARREL_EC_MIN();
 
  static bool values_ok ( const unsigned int barrel_ec,  
                          const unsigned int layer, 
                          const unsigned int phi_module,   
			  const unsigned int 
		        ) ;

  /// Test for barrel
  static bool is_barrel (const Identifier& id) ;  
  static bool is_barrel (const unsigned int barrel_ec) ;  

  /// Values of different levels (failure returns 0)
  static int barrel_ec       (const Identifier& id) ;
  static int layer           (const Identifier& id) ; 
  static int phi_module      (const Identifier& id) ; 
  static int end             (const Identifier& id) ; 
 
  /// Max/Min values for each field (error returns -999)
  int phi_module_max    (const Identifier& id) ;
  int layer_max (const Identifier& id);

private:

  typedef std::vector<Identifier>     id_vec;
  typedef id_vec::const_iterator      id_vec_it;

  static const unsigned int BARREL_EC_INDEX    = 14;
  static const unsigned int BARREL_EC_MASK     = 0x0000C000;


  static const unsigned int LAYER_INDEX        = 8;
  static const unsigned int LAYER_MASK         = 0x00000100;
 
  static const unsigned int PHI_INDEX          = 1;
  static const unsigned int PHI_MASK           = 0x000000FE;

  static const unsigned int END_INDEX          = 0;
  static const unsigned int END_MASK           = 0x00000001;

  static const unsigned int LAYER_BARREL_MAX     = 1; 
  static const unsigned int LAYER_ENDCAP_MAX     = 0;   
  
  static const unsigned int PHI_BARREL_MAX     = 87; 
  static const unsigned int PHI_ENDCAP_MAX     = 47;

  static const unsigned int BARREL_EC_MAX      = 2;
  static const unsigned int BARREL_EC_MIN      = 0;  
  
};

#endif
