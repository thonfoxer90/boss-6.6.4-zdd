#include "Identifier/ZddID.h"
#include <assert.h>
#include <iostream>

ZddID::ZddID(void) {
}

ZddID::~ZddID(void) {
}

//----------------------------------------------------------------------------

bool ZddID::values_ok ( 
		        const unsigned int partID,
			const unsigned int detectorID,
			const unsigned int crystalNo
		      )
{
  // check values  

  //  std::cout << "partID = " << partID << "\t" <<
  //    << "detectorID = " << detectorID << "\t" <<
  //    << "crystalNo = " << crystalNo << std::endl;


  if (partID > PART_ID_MAX || partID < PART_ID_MIN) return false;
  else
    {
      if (detectorID > DETECTOR_ID_MAX || detectorID < DETECTOR_ID_MIN) return false;
      else
       {
       if (crystalNo > CRYSTAL_NO_MAX || crystalNo < CRYSTAL_NO_MIN) return false;
       else{ return true; }
       }
    }
}


Identifier ZddID::cell_id ( int partID, int detectorID, int crystalNo )
{
  assert ( values_ok( partID, detectorID, crystalNo ) );
  unsigned int value = (BesDetectorID::ZDD_ID << BesDetectorID::ZDD_INDEX) |
                       (partID << PART_ID_INDEX) |
                       (detectorID << DETECTOR_ID_INDEX) |
                       (crystalNo << CRYSTAL_NO_INDEX);

  return Identifier(value);  
}

unsigned int ZddID::getIntID (
			       int partID,
			       int detectorID,
			       int crystalNo
                              )
{
  unsigned int value = (BesDetectorID::ZDD_ID << BesDetectorID::ZDD_INDEX) |
    (partID << PART_ID_INDEX) |
    (detectorID << DETECTOR_ID_INDEX) |
    (crystalNo << CRYSTAL_NO_INDEX);
  return value;
}

bool ZddID::is_left_zdd( const Identifier& id )
{
  unsigned int part = (id.get_value() & ZddID::PART_ID_MASK) >> ZddID::PART_ID_INDEX; // get 32 bit id, get values of PART_ID through & PART_ID_MASK , shift bits PART_IT_INDEX right 
  return ( part == 1 )? true : false; // if condition right, return true
} 

bool ZddID::is_upper_zdd( const Identifier& id ) 
{
  unsigned int detector = (id.get_value() & DETECTOR_ID_MASK) >> DETECTOR_ID_INDEX;
  return ( detector == 1 )? true : false;
}

int ZddID::partID( const Identifier& id )
{
  return ( (id.get_value() & PART_ID_MASK) >> PART_ID_INDEX) ; // get 32 bit id, get values of PART_ID through & PART_ID_MASK , shift bits PART_IT_INDEX right         
}

int ZddID::detectorID( const Identifier& id )
{
  return ( (id.get_value() & DETECTOR_ID_MASK) >> DETECTOR_ID_INDEX) ;
}

int ZddID::crystalNo( const Identifier& id )
{
  return ( (id.get_value() & CRYSTAL_NO_MASK) >> CRYSTAL_NO_INDEX) ;
} 


