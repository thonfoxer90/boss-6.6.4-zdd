#ifndef __ZddID_h__
#define __ZddID_h__

#include "Identifier/BesDetectorID.h"
#include <string>
#include <assert.h>

class ZddID : public BesDetectorID
{
public:
  typedef Identifier::size_type size_type; 
  typedef Identifier::value_type value_type; 

  /// constructor
  ZddID();

  /// destructor 
  ~ZddID();
 
  /// For a single crystal
 static Identifier cell_id (
			       int partID,
			       int detectorID,
			       int crystalNo
			   );

 static value_type getIntID ( 
			       int partID,
			       int detectorID,
			       int crystalNo
                            ); 

 //  static value_type getIntID ();
static bool values_ok (
		const unsigned int partID,
		const unsigned int detectorID,
          	const unsigned int crystalNo  
		); 

 // Test for part, return true if the left zdd is hit (viewpoint besiii control room)
  static bool is_left_zdd (const Identifier& id) ;
  static bool is_upper_zdd (const Identifier& id) ;


 // values of different levels (failure return -999)
  static int partID     (const Identifier& id);
  static int detectorID (const Identifier& id);
  static int crystalNo  (const Identifier& id);

  /*  
  static unsigned int getCRYSTAL_NO_MAX()   ( return CRYSTAL_NO_MAX;);
  static unsigned int getCRYSTAL_NO_MIN()   ( return CRYSTAL_NO_MIN;);
  static unsigned int getDETECTOR_ID_MAX()  ( return DETECTOR_ID_MAX;);
  static unsigned int getDETECTOR_ID_MIN()  ( return DETECTOR_ID_MIN;);
  static unsigned int getPART_ID_MAX()      ( return PART_ID_MAX;);
  static unsigned int getPART_ID_MIN()      ( return PART_ID_MIN;);
  */

 private:

  typedef std::vector<Identifier>     id_vec;
  typedef id_vec::const_iterator      id_vec_it;

  // left or right
  static const unsigned int PART_ID_INDEX = 10;       // when translated into binary numbers, the bits will be shifted left by this value
  static const unsigned int PART_ID_MASK = 0xC00;     // bits: 00000000000000000000110000000000

  // up or down
  static const unsigned int DETECTOR_ID_INDEX = 8;    // when translated into binary numbers, the bits will be shifted left by this value
  static const unsigned int DETECTOR_ID_MASK = 0x300; // bits: 00000000000000000000001100000000

  // crystal no
  static const unsigned int CRYSTAL_NO_INDEX = 0;     // when translated into binary numbers, the bits will be shifted left by this value
  static const unsigned int CRYSTAL_NO_MASK = 0xFF;   // bits: 00000000000000000000000011111111
                                                      // Highest crystal number is 224, which needs 8bits to be represented in binary code. This mask takes the last 8 bits of the 32bit identifier. 
                                                      // With logical AND conjunction this decrypts the crystal signal
  static const unsigned int PART_ID_MAX = 2;
  static const unsigned int PART_ID_MIN = 1;

  static const unsigned int DETECTOR_ID_MAX = 2;
  static const unsigned int DETECTOR_ID_MIN = 1;

  static const unsigned int CRYSTAL_NO_MAX = 224;
  static const unsigned int CRYSTAL_NO_MIN = 101;

};

#endif
