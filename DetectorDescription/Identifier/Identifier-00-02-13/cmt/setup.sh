# echo "Setting Identifier Identifier-00-02-13 in /home/bgarillo/boss-6.6.4-ZDD/DetectorDescription"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cluster/him/bes3/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh

tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=Identifier -version=Identifier-00-02-13 -path=/home/bgarillo/boss-6.6.4-ZDD/DetectorDescription  -no_cleanup $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

