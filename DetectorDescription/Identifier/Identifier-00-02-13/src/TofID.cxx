#include "Identifier/TofID.h"
#include <assert.h>
#include <iostream>


TofID::TofID(void) {
}

TofID::~TofID(void) {
}

//----------------------------------------------------------------------------
bool TofID::values_ok ( const unsigned int barrel_ec,  
                         const unsigned int layer, 
			 const unsigned int phi_module,
                         const unsigned int 
		       ) {
  // Check values

  //  std::cout << " barrel_ec = "<< barrel_ec
  //          << " layer = "<< layer
  //          << " phi_module = "<< phi_module <<std::endl;
  
  if ( barrel_ec  > BARREL_EC_MAX)       return false;
  if ( BARREL_EC_MIN == barrel_ec || BARREL_EC_MIN == barrel_ec) {
    // if ( phi_module    > PHI_ENDCAP_MAX )     return false;                                                                                                                                                  
    if ( layer         > LAYER_ENDCAP_MAX   ) return false;
  }
  else {
    //if ( phi_module    > PHI_BARREL_MAX  )    return false; //#Matthias : Switched of as the simulation will not run if switched on!                                                                          
    if ( layer         >  LAYER_BARREL_MAX  ) return false;
  }

  return true;
}



bool TofID::values_ok_mrpc(const unsigned int partID, const unsigned int scinNum)
{
  if(partID<3 || partID>6) return false;
  if(scinNum<26 || scinNum>474) return false;
  if(scinNum%25==0) return false;

  return true;
}


bool TofID::values_ok_mrpc_mc(const unsigned int partID, const unsigned int scinNum)
{
  if(partID<3 || partID>6) return false;
  if(scinNum<25 || scinNum>474) return false; 

  return true;
}


bool TofID::is_mymrpc(const Identifier& id)
{
  //if( (id & TofID::MRPC_MASK)== MRPC_MASK){ return true;}
  if( (id & (BesDetectorID::MRPC_ID << MRPC_INDEX) )== (BesDetectorID::MRPC_ID << MRPC_INDEX) )
  { 
    //std::cout << "MATTHIAS       my mrpc is true " <<std::endl;
    //std::cout << "               (id & (BesDetectorID::MRPC_ID << MRPC_INDEX))  "<< (id & (BesDetectorID::MRPC_ID << MRPC_INDEX) ) <<std::endl;
    //std::cout << "               (BesDetectorID::MRPC_ID << MRPC_INDEX)     " <<  (BesDetectorID::MRPC_ID << MRPC_INDEX)<<std::endl;
    return true;
  }
  else
    {
      //std::cout << "MATTHIAS       my mrpc is false " <<std::endl;
 return false;
    }
}



//----------------------------------------------------------------------------
bool  TofID::is_barrel (const Identifier& id) {

  bool is_mrpc=TofID::is_mymrpc(id);

  if(is_mrpc==true){return false;}
  else
    {  
  unsigned int pos = (id.get_value() & TofID::BARREL_EC_MASK) >>  TofID::BARREL_EC_INDEX;
  return (pos == BARREL_EC_MAX || pos == BARREL_EC_MIN) ? false : true;
    }
}


bool  TofID::is_barrel (const unsigned int barrel_ec) {
  return (barrel_ec == BARRELCODE) ? true : false;
}
//----------------------------------------------------------------------------
int TofID::barrel_ec (const Identifier& id ) {

  bool is_mrpc=TofID::is_mymrpc(id);
  //std::cout << "TofID       is_mrpc()?  " <<  is_mrpc << std::endl;
  //if(is_mrpc==true){    std::cout << "TofID     barrel_ec(id) - mrpc  true! -> return  " <<  ((id.get_value() & TofID::PARTID_MASK) >> TofID::PARTID_INDEX)  << std::endl;}
  //else {    std::cout << "TofID       barrel_ec(id) - mrpc  false! -> return  " <<  ((id.get_value() & TofID::BARREL_EC_MASK) >>  TofID::BARREL_EC_INDEX) << std::endl;}
  if(is_mrpc==true){ return ((id.get_value() & TofID::PARTID_MASK) >> TofID::PARTID_INDEX); }   
  else  { return ((id.get_value() & TofID::BARREL_EC_MASK) >>  TofID::BARREL_EC_INDEX);}

  

}

//----------------------------------------------------------------------------
int TofID::layer (const Identifier& id) {

  bool is_mrpc=TofID::is_mymrpc(id);
  if(is_mrpc==true){ return 0;}
  else {return ((id.get_value() & TofID::LAYER_MASK) >>  TofID::LAYER_INDEX);}
}

//----------------------------------------------------------------------------
int TofID::phi_module (const Identifier& id) {

  bool is_mrpc=TofID::is_mymrpc(id);
  //std::cout << "TofID       Identifier:  " << id << std::endl;
  //std::cout << "TofID       is_mrpc()?  " <<  is_mrpc << std::endl;   
  //if(is_mrpc==true){    std::cout << "TofID       phimodule(id) - mrpc true! -> return  " <<  ((id.get_value() & TofID::SCINNUM_MASK))  << std::endl;}                                                       
  //else {    std::cout << "TofID       phimodule(id) - mrpc false! -> return  " <<  ((id.get_value() & TofID::PHI_MASK) >>  TofID::PHI_INDEX) << std::endl;}     
  if(is_mrpc==true){return ((id.get_value() & TofID::SCINNUM_MASK));}
  else {return ((id.get_value() & TofID::PHI_MASK) >>  TofID::PHI_INDEX); }
}

//----------------------------------------------------------------------------
int TofID::end(const Identifier& id) {
  bool is_mrpc=TofID::is_mymrpc(id);
  if( is_mrpc==true){return 0;}
  else { return ((id.get_value() & TofID::END_MASK) >> TofID::END_INDEX);}
}



//----------------------------------------------------------------------------
int TofID::layer_max  (const Identifier& id) {
   if (is_barrel(id)) {
    return LAYER_BARREL_MAX;
  } else {
    return LAYER_ENDCAP_MAX;
  }  
}

//----------------------------------------------------------------------------
int TofID::phi_module_max    (const Identifier& id) {
  if (is_barrel(id)) {
    return PHI_BARREL_MAX;
  } else {
    return PHI_ENDCAP_MAX;
  } 
}

//----------------------------------------------------------------------------
Identifier TofID::cell_id ( int barrel_ec,
                             int layer,   
			     int phi_module,
                             int end 
			   ) {
  assert ( values_ok(barrel_ec, layer, phi_module, end) ); 
  unsigned int value = (BesDetectorID::TOF_ID << TOF_INDEX) | 
                     (barrel_ec << BARREL_EC_INDEX)|
                     (layer << LAYER_INDEX) | (phi_module << PHI_INDEX) | 
                     (end << END_INDEX);
  return Identifier(value); 
}


Identifier TofID::cell_id(int value)
{
  return Identifier(value);
}



Identifier TofID::cell_id_mrpc ( int partID, int scinNum)
{
  assert( values_ok_mrpc(partID, scinNum));
  unsigned int value = (BesDetectorID::MRPC_ID << MRPC_INDEX) |
                        //(3 << MRPC_INDEX) |   //This two digits will be my identifier for a MRPC signal, MRPC_INDEX = 27  
                       (partID << PARTID_INDEX) |  // Which partID, PARTID_INDEX = 12
                       (scinNum);

  //std::cout << "Matthias     cell_id_mrpc    " << value << std::endl;
  return Identifier(value);  //returns a secured 32bit variable (hex)
}

Identifier TofID::cell_id_mrpc_mc ( int partID, int scinNum)
{
  assert( values_ok_mrpc_mc(partID, scinNum));
  unsigned int value = (BesDetectorID::MRPC_ID << MRPC_INDEX) |
    //(3 << MRPC_INDEX) |   //This two digits will be my identifier for a MRPC signal, MRPC_INDEX = 27                                                                                         
    (partID << PARTID_INDEX) |  // Which partID, PARTID_INDEX = 12                                                                                                                           
    (scinNum);
  return Identifier(value);  //returns a secured 32bit variable (hex)                                                                                                                                           
}


unsigned int TofID::getIntID ( int barrel_ec,
                             int layer,
                             int phi_module,
                             int end
                              )
{
     unsigned int value = (BesDetectorID::TOF_ID << TOF_INDEX) |
                     (barrel_ec << BARREL_EC_INDEX)|
                     (layer << LAYER_INDEX) | (phi_module << PHI_INDEX) |
                     (end << END_INDEX);
     return value;
}




unsigned int TofID::getIntID_mrpc ( int partID, int scinNum  )
{
  unsigned int value = ((BesDetectorID::MRPC_ID << MRPC_INDEX) |
                        //(3 << MRPC_INDEX) |   //This two digits will be my identifier for a MRPC signal, MRPC_INDEX = 27
                        (partID << PARTID_INDEX) |  // Which partID, PARTID_INDEX = 12
			(scinNum));
   return value;
}



unsigned int TofID::getLAYER_BARREL_MAX()
{
     return LAYER_BARREL_MAX;
}

unsigned int TofID::getLAYER_ENDCAP_MAX()
{
     return LAYER_ENDCAP_MAX;
}

unsigned int TofID::getPHI_BARREL_MAX()
{
     return PHI_BARREL_MAX;
}

unsigned int TofID::getPHI_ENDCAP_MAX()
{
     return PHI_ENDCAP_MAX;
}
unsigned int TofID::getBARREL_EC_MAX()
{
     return BARREL_EC_MAX;
}

unsigned int TofID::getBARREL_EC_MIN()
{
     return BARREL_EC_MIN;
}
 

