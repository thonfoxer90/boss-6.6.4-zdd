//$id$
/*
	2012/11/09   Marcel Werner, JLU Giessen, GERMANY
 */

using namespace std;

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <iomanip>

#include <TGeoManager.h>

#include "ROOTGeo/ZddROOTGeo.h"


ZddROOTGeo::ZddROOTGeo()
{ 

  string GdmlManagementPath = getenv("GDMLMANAGEMENTROOT");
  //cout << "ZddGeoGeneral::InitFromXML(), GdmlManagementPath not found" << endl;
  string GdmlFile = GdmlManagementPath + string("/dat/Zdd.gdml");
  //string GdmlFile = string("Zdd.gdml");
  cout << "GdmlFile " << GdmlFile << endl;
  InitFromGdml( GdmlFile.c_str(), "Zdd" );
}

ZddROOTGeo::~ZddROOTGeo()
{ }

void
ZddROOTGeo::InitFromGdml( const char *gdmlFile, const char *setupName )
{
  ReadGdml(gdmlFile, setupName);
  //SetDefaultVis();

  m_ROOTGeoInit = 1;  
}

void 
ZddROOTGeo::SetDefaultVis()
{
  //std::cout << "end of set defaultvis" << std::endl;
}

void
ZddROOTGeo::SetAllVisible()
{

}

TGeoVolume*
ZddROOTGeo::GetVolumeCrystal( int partID, int detectorID, int crystalNo )
{
  std::stringstream osname;
  osname << "l" << "Zdd" << "ParID" << partID << "DetID" << detectorID << "CryNo" << crystalNo;
  
  return GetLogicalVolume( osname.str() );
}
  
