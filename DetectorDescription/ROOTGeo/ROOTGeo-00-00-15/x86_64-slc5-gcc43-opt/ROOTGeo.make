#-- start of make_header -----------------

#====================================
#  Library ROOTGeo
#
<<<<<<< HEAD
#   Generated Sat May 19 14:50:17 2018  by bgarillo
=======
#   Generated Sun May 20 13:38:51 2018  by bgarillo
>>>>>>> 0f0f6374e0d78f9332793f0e2835efad54545d19
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_ROOTGeo_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_ROOTGeo_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_ROOTGeo

ROOTGeo_tag = $(tag)

#cmt_local_tagfile_ROOTGeo = $(ROOTGeo_tag)_ROOTGeo.make
cmt_local_tagfile_ROOTGeo = $(bin)$(ROOTGeo_tag)_ROOTGeo.make

else

tags      = $(tag),$(CMTEXTRATAGS)

ROOTGeo_tag = $(tag)

#cmt_local_tagfile_ROOTGeo = $(ROOTGeo_tag).make
cmt_local_tagfile_ROOTGeo = $(bin)$(ROOTGeo_tag).make

endif

include $(cmt_local_tagfile_ROOTGeo)
#-include $(cmt_local_tagfile_ROOTGeo)

ifdef cmt_ROOTGeo_has_target_tag

cmt_final_setup_ROOTGeo = $(bin)setup_ROOTGeo.make
#cmt_final_setup_ROOTGeo = $(bin)ROOTGeo_ROOTGeosetup.make
cmt_local_ROOTGeo_makefile = $(bin)ROOTGeo.make

else

cmt_final_setup_ROOTGeo = $(bin)setup.make
#cmt_final_setup_ROOTGeo = $(bin)ROOTGeosetup.make
cmt_local_ROOTGeo_makefile = $(bin)ROOTGeo.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)ROOTGeosetup.make

#ROOTGeo :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'ROOTGeo'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = ROOTGeo/
#ROOTGeo::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

ROOTGeolibname   = $(bin)$(library_prefix)ROOTGeo$(library_suffix)
ROOTGeolib       = $(ROOTGeolibname).a
ROOTGeostamp     = $(bin)ROOTGeo.stamp
ROOTGeoshstamp   = $(bin)ROOTGeo.shstamp

ROOTGeo :: dirs  ROOTGeoLIB
	$(echo) "ROOTGeo ok"

#-- end of libary_header ----------------
#-- start of cleanup_header --------------

clean :: ROOTGeoclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(ROOTGeo.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ROOTGeo.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(ROOTGeo.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(ROOTGeo.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_ROOTGeo)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ROOTGeo.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ROOTGeo.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(ROOTGeo.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

ROOTGeoclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library ROOTGeo
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)ROOTGeo$(library_suffix).a $(library_prefix)ROOTGeo$(library_suffix).s? ROOTGeo.stamp ROOTGeo.shstamp
#-- end of cleanup_library ---------------
