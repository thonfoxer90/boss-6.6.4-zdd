//$id$
/*
 *    2004/9/20    Zhengyun You      Peking University
 *                 Zdd Geometry General for EventDisplay
 *
 *    2004/12/11   Zhengyun You      Peking University
 *                 named from ZddGeo to ZddROOTGeo
 *                 inherit from class SubDetectorROOTGeo
 */

#ifndef ZDD_ROOT_GEO_H
#define ZDD_ROOT_GEO_H

#include <TGeoVolume.h>
#include <TGeoNode.h>
#include <TGeoPhysicalNode.h>

#include "SubDetectorROOTGeo.h"
/**
 * Class ZddGeo contains all of the objects necessary to describe the
 * zdd geometry.
 *
 * @author Zhengyun You \URL{mailto:youzy@hep.pku.cn}
 *
 */

class ZddROOTGeo : public SubDetectorROOTGeo
{
 public:
  
  /// Constructor.
  ZddROOTGeo();

  /// Destructor.
  ~ZddROOTGeo();

  /// Initialize the instance of ROOTGeo.
  void InitFromGdml( const char *gdmlFile, const char *setupName );

  /// Set default visual attributes;
  void SetDefaultVis();

  /// Set all visible;
  void SetAllVisible();
  
  /// Get ID of part;
  int GetPartID(){ return m_partID; };

  /// Get ID of detectort;
  int GetDetectorID(){ return m_detectorID;};
  
  /// Get crystal number;
  int GetCrystalNo(){ return m_crystalNo;};
  
  /// Get Zdd volume;
  TGeoVolume *GetVolumeZdd() { return m_Zdd; }
  
  TGeoVolume *GetVolumeCrystal( int partID, int detectorID, int crystalNo);

 private:

  static const int m_partID;
  static const int m_detectorID;
  static const int m_crystalNo;

  TGeoVolume       *m_Zdd;
 };

#endif   /* ZDD_ROOT_GEO_H */
