#include "$RAWEVENTREADERROOT/share/jobOptions_RawEventReader.txt"
#include "$ROOTIOROOT/share/jobOptions_ReadRec.txt"
#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"
#include "$MAGNETICFIELDROOT/share/MagneticField.txt"
#include "$ABSCORROOT/share/jobOptions_AbsCor.txt"
#include "$RHOPIALGROOT/share/jobOptions_Rhopi.txt"

RawEventReader.InputFiles = {
"/besfs2/offline/data/raw/round02/090612/run_0009947_All_file001_SFO-1.raw",
"/besfs2/offline/data/raw/round02/090612/run_0009947_All_file001_SFO-2.raw"
};


// Input REC or DST file name
EventCnvSvc.digiRootInputFile = {
"/besfs2/offline/data/664-1/jpsi/dst/090612/run_0009947_All_file001_SFO-1.dst",
"/besfs2/offline/data/664-1/jpsi/dst/090612/run_0009947_All_file001_SFO-2.dst"
};

// Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
MessageSvc.OutputLevel = 5;

// Number of events to be processed (default is 10)
ApplicationMgr.EvtMax = -1;

ApplicationMgr.HistogramPersistency = "ROOT";
NTupleSvc.Output = { "FILE1 DATAFILE='rhopi_ana.root' OPT='NEW' TYP='ROOT'"};
