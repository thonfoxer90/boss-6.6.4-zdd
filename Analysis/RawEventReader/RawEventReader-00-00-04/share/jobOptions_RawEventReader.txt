ApplicationMgr.DLLs += {"RawDataCnv","RawEventReader"};
ApplicationMgr.TopAlg += { "RawEventReader"};
Builder.RawDataCnvConfFile = "$RAWDATACNVROOT/share/RawDataCnvConf.conf";
MucBuilder.Fec2IdMap = "$RAWDATACNVROOT/share/MucFec2Id.map";
