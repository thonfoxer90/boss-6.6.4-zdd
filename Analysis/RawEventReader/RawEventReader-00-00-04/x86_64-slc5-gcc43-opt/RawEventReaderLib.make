#-- start of make_header -----------------

#====================================
#  Library RawEventReaderLib
#
#   Generated Fri Jan 13 11:18:20 2017  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_RawEventReaderLib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_RawEventReaderLib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_RawEventReaderLib

RawEventReader_tag = $(tag)

#cmt_local_tagfile_RawEventReaderLib = $(RawEventReader_tag)_RawEventReaderLib.make
cmt_local_tagfile_RawEventReaderLib = $(bin)$(RawEventReader_tag)_RawEventReaderLib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

RawEventReader_tag = $(tag)

#cmt_local_tagfile_RawEventReaderLib = $(RawEventReader_tag).make
cmt_local_tagfile_RawEventReaderLib = $(bin)$(RawEventReader_tag).make

endif

include $(cmt_local_tagfile_RawEventReaderLib)
#-include $(cmt_local_tagfile_RawEventReaderLib)

ifdef cmt_RawEventReaderLib_has_target_tag

cmt_final_setup_RawEventReaderLib = $(bin)setup_RawEventReaderLib.make
#cmt_final_setup_RawEventReaderLib = $(bin)RawEventReader_RawEventReaderLibsetup.make
cmt_local_RawEventReaderLib_makefile = $(bin)RawEventReaderLib.make

else

cmt_final_setup_RawEventReaderLib = $(bin)setup.make
#cmt_final_setup_RawEventReaderLib = $(bin)RawEventReadersetup.make
cmt_local_RawEventReaderLib_makefile = $(bin)RawEventReaderLib.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)RawEventReadersetup.make

#RawEventReaderLib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'RawEventReaderLib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = RawEventReaderLib/
#RawEventReaderLib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

RawEventReaderLiblibname   = $(bin)$(library_prefix)RawEventReaderLib$(library_suffix)
RawEventReaderLiblib       = $(RawEventReaderLiblibname).a
RawEventReaderLibstamp     = $(bin)RawEventReaderLib.stamp
RawEventReaderLibshstamp   = $(bin)RawEventReaderLib.shstamp

RawEventReaderLib :: dirs  RawEventReaderLibLIB
	$(echo) "RawEventReaderLib ok"

#-- end of libary_header ----------------

RawEventReaderLibLIB :: $(RawEventReaderLiblib) $(RawEventReaderLibshstamp)
	@/bin/echo "------> RawEventReaderLib : library ok"

$(RawEventReaderLiblib) :: $(bin)RawEventReader.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(RawEventReaderLiblib) $?
	$(lib_silent) $(ranlib) $(RawEventReaderLiblib)
	$(lib_silent) cat /dev/null >$(RawEventReaderLibstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(RawEventReaderLiblibname).$(shlibsuffix) :: $(RawEventReaderLiblib) $(RawEventReaderLibstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" RawEventReaderLib $(RawEventReaderLib_shlibflags)

$(RawEventReaderLibshstamp) :: $(RawEventReaderLiblibname).$(shlibsuffix)
	@if test -f $(RawEventReaderLiblibname).$(shlibsuffix) ; then cat /dev/null >$(RawEventReaderLibshstamp) ; fi

RawEventReaderLibclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)RawEventReader.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
RawEventReaderLibinstallname = $(library_prefix)RawEventReaderLib$(library_suffix).$(shlibsuffix)

RawEventReaderLib :: RawEventReaderLibinstall

install :: RawEventReaderLibinstall

RawEventReaderLibinstall :: $(install_dir)/$(RawEventReaderLibinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(RawEventReaderLibinstallname) :: $(bin)$(RawEventReaderLibinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(RawEventReaderLibinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(RawEventReaderLibinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(RawEventReaderLibinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(RawEventReaderLibinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(RawEventReaderLibinstallname) $(install_dir)/$(RawEventReaderLibinstallname); \
	      echo `pwd`/$(RawEventReaderLibinstallname) >$(install_dir)/$(RawEventReaderLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(RawEventReaderLibinstallname), no installation directory specified"; \
	  fi; \
	fi

RawEventReaderLibclean :: RawEventReaderLibuninstall

uninstall :: RawEventReaderLibuninstall

RawEventReaderLibuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(RawEventReaderLibinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(RawEventReaderLibinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(RawEventReaderLibinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(RawEventReaderLibinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),RawEventReaderLibclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)RawEventReaderLib_dependencies.make :: dirs

ifndef QUICK
$(bin)RawEventReaderLib_dependencies.make : $(src)RawEventReader.cxx $(use_requirements) $(cmt_final_setup_RawEventReaderLib)
	$(echo) "(RawEventReaderLib.make) Rebuilding $@"; \
	  $(build_dependencies) RawEventReaderLib -all_sources -out=$@ $(src)RawEventReader.cxx
endif

#$(RawEventReaderLib_dependencies)

-include $(bin)RawEventReaderLib_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),RawEventReaderLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)RawEventReader.d

$(bin)$(binobj)RawEventReader.d : $(use_requirements) $(cmt_final_setup_RawEventReaderLib)

$(bin)$(binobj)RawEventReader.d : $(src)RawEventReader.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/RawEventReader.dep $(use_pp_cppflags) $(RawEventReaderLib_pp_cppflags) $(lib_RawEventReaderLib_pp_cppflags) $(RawEventReader_pp_cppflags) $(use_cppflags) $(RawEventReaderLib_cppflags) $(lib_RawEventReaderLib_cppflags) $(RawEventReader_cppflags) $(RawEventReader_cxx_cppflags)  $(src)RawEventReader.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/RawEventReader.o $(src)RawEventReader.cxx $(@D)/RawEventReader.dep
endif
endif

$(bin)$(binobj)RawEventReader.o : $(src)RawEventReader.cxx
else
$(bin)RawEventReaderLib_dependencies.make : $(RawEventReader_cxx_dependencies)

$(bin)$(binobj)RawEventReader.o : $(RawEventReader_cxx_dependencies)
endif
	$(cpp_echo) $(src)RawEventReader.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(RawEventReaderLib_pp_cppflags) $(lib_RawEventReaderLib_pp_cppflags) $(RawEventReader_pp_cppflags) $(use_cppflags) $(RawEventReaderLib_cppflags) $(lib_RawEventReaderLib_cppflags) $(RawEventReader_cppflags) $(RawEventReader_cxx_cppflags)  $(src)RawEventReader.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: RawEventReaderLibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(RawEventReaderLib.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(RawEventReaderLib.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(RawEventReaderLib.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(RawEventReaderLib.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_RawEventReaderLib)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawEventReaderLib.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawEventReaderLib.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawEventReaderLib.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

RawEventReaderLibclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library RawEventReaderLib
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)RawEventReaderLib$(library_suffix).a $(library_prefix)RawEventReaderLib$(library_suffix).s? RawEventReaderLib.stamp RawEventReaderLib.shstamp
#-- end of cleanup_library ---------------
