#include "GaudiKernel/DeclareFactoryEntries.h"
#include "RawEventReader/RawEventReader.h"

DECLARE_ALGORITHM_FACTORY( RawEventReader)

DECLARE_FACTORY_ENTRIES( RawEventReader ) {
  DECLARE_ALGORITHM(RawEventReader);
}
