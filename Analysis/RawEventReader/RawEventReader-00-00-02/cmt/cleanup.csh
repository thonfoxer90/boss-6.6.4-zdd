if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cluster/bes3/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh
set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt cleanup -csh -pack=RawEventReader -version=RawEventReader-00-00-02 -path=/home/bgarillo/boss-6.6.4-ZDD/Analysis $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

