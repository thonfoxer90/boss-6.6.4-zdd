
#include "GaudiKernel/Algorithm.h"
#include <string>

#include "GaudiKernel/IDataProviderSvc.h"
#include "RawDataCnv/EventManagement/RAWEVENT.h"

#include "McTruth/McParticle.h"
//#include "ParticleID/ParticleID.h"

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
//#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/PropertyMgr.h"
//#include "GaudiKernel/DataObject.h"
//#include "GaudiKernel/GenericAddress.h"
//#include "GaudiKernel/ClassID.h"

#include "RawDataCnv/EventManagement/RAWEVENT.h"
#include "RawDataCnv/Util/MdcConverter.h"
#include "RawDataCnv/Util/TofConverter.h"
#include "RawDataCnv/Util/EmcConverter.h"
#include "RawDataCnv/Util/MucConverter.h"
#include "RawDataCnv/Util/ZddConverter.h"
#include "Identifier/Identifier.h"
#include "Identifier/MdcID.h"
#include "Identifier/TofID.h"
#include "Identifier/EmcID.h"
#include "Identifier/MucID.h"
#include "IRawFile/RawFileExceptions.h"

#include "RawFile/RawFileReader.h"
#include "RawEvent/DigiEvent.h"

#include "EventModel/EventModel.h"
#include "EventModel/EventHeader.h"

//Digi
#include "MdcRawEvent/MdcDigi.h"
#include "EmcRawEvent/EmcDigi.h"
#include "TofRawEvent/TofDigi.h"
#include "MucRawEvent/MucDigi.h"
#include "ZddRawEvent/ZddDigi.h"

#include "RootEventData/TMdcDigi.h"
#include "RootEventData/TTofDigi.h"
#include "RootEventData/TEmcDigi.h"
#include "RootEventData/TMucDigi.h"
#include "RootEventData/TZddDigi.h"

//MC Hit collection
#include "McTruth/McEvent.h"
#include "RootEventData/TZddMc.h"

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TObjArray.h"


class MdcConverter;
class TofConverter;
class EmcConverter;
class MucConverter;
class ZddConverter;
class TTree;

class RawEventReader:public Algorithm {
public:
  RawEventReader(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~RawEventReader();
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();
  

private:

  void readEventData();
  void readEventMC();
  void readMdc();
  void readTof();
  void readEmc();
  void readMuc();
	void readZdd();
	void readZddMCHitCol();

  IDataProviderSvc* m_evtSvc;

  MdcConverter* m_mdcCnv;
  TofConverter* m_tofCnv;
  EmcConverter* m_emcCnv;
  MucConverter* m_mucCnv;
	ZddConverter* m_zddCnv;

  bool m_mdcdigi;
  bool m_tofdigi;
  bool m_emcdigi;
  bool m_mucdigi;
	bool m_zdddigi;

	bool m_zddMcHit;
	
  TTree* m_pTree;
  int    m_iEntry;
  int    m_iFileNumber;

  RAWEVENT* m_raw_event;
  std::vector<std::string> m_inputFiles;
	
	TTree* m_outtree;
	TFile* m_outfile;
	
	int m_event;
	int m_chargechannel;
	int m_TrackIndex;

};

