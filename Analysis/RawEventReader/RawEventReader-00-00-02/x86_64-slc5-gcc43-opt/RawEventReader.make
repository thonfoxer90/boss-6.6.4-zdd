#-- start of make_header -----------------

#====================================
#  Library RawEventReader
#
#   Generated Thu Jan 12 18:59:51 2017  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_RawEventReader_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_RawEventReader_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_RawEventReader

RawEventReader_tag = $(tag)

#cmt_local_tagfile_RawEventReader = $(RawEventReader_tag)_RawEventReader.make
cmt_local_tagfile_RawEventReader = $(bin)$(RawEventReader_tag)_RawEventReader.make

else

tags      = $(tag),$(CMTEXTRATAGS)

RawEventReader_tag = $(tag)

#cmt_local_tagfile_RawEventReader = $(RawEventReader_tag).make
cmt_local_tagfile_RawEventReader = $(bin)$(RawEventReader_tag).make

endif

include $(cmt_local_tagfile_RawEventReader)
#-include $(cmt_local_tagfile_RawEventReader)

ifdef cmt_RawEventReader_has_target_tag

cmt_final_setup_RawEventReader = $(bin)setup_RawEventReader.make
#cmt_final_setup_RawEventReader = $(bin)RawEventReader_RawEventReadersetup.make
cmt_local_RawEventReader_makefile = $(bin)RawEventReader.make

else

cmt_final_setup_RawEventReader = $(bin)setup.make
#cmt_final_setup_RawEventReader = $(bin)RawEventReadersetup.make
cmt_local_RawEventReader_makefile = $(bin)RawEventReader.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)RawEventReadersetup.make

#RawEventReader :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'RawEventReader'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = RawEventReader/
#RawEventReader::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

RawEventReaderlibname   = $(bin)$(library_prefix)RawEventReader$(library_suffix)
RawEventReaderlib       = $(RawEventReaderlibname).a
RawEventReaderstamp     = $(bin)RawEventReader.stamp
RawEventReadershstamp   = $(bin)RawEventReader.shstamp

RawEventReader :: dirs  RawEventReaderLIB
	$(echo) "RawEventReader ok"

#-- end of libary_header ----------------

RawEventReaderLIB :: $(RawEventReaderlib) $(RawEventReadershstamp)
	@/bin/echo "------> RawEventReader : library ok"

$(RawEventReaderlib) :: $(bin)RawEventReader_load.o $(bin)RawEventReader_entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(RawEventReaderlib) $?
	$(lib_silent) $(ranlib) $(RawEventReaderlib)
	$(lib_silent) cat /dev/null >$(RawEventReaderstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(RawEventReaderlibname).$(shlibsuffix) :: $(RawEventReaderlib) $(RawEventReaderstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" RawEventReader $(RawEventReader_shlibflags)

$(RawEventReadershstamp) :: $(RawEventReaderlibname).$(shlibsuffix)
	@if test -f $(RawEventReaderlibname).$(shlibsuffix) ; then cat /dev/null >$(RawEventReadershstamp) ; fi

RawEventReaderclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)RawEventReader_load.o $(bin)RawEventReader_entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
RawEventReaderinstallname = $(library_prefix)RawEventReader$(library_suffix).$(shlibsuffix)

RawEventReader :: RawEventReaderinstall

install :: RawEventReaderinstall

RawEventReaderinstall :: $(install_dir)/$(RawEventReaderinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(RawEventReaderinstallname) :: $(bin)$(RawEventReaderinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(RawEventReaderinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(RawEventReaderinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(RawEventReaderinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(RawEventReaderinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(RawEventReaderinstallname) $(install_dir)/$(RawEventReaderinstallname); \
	      echo `pwd`/$(RawEventReaderinstallname) >$(install_dir)/$(RawEventReaderinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(RawEventReaderinstallname), no installation directory specified"; \
	  fi; \
	fi

RawEventReaderclean :: RawEventReaderuninstall

uninstall :: RawEventReaderuninstall

RawEventReaderuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(RawEventReaderinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(RawEventReaderinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(RawEventReaderinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(RawEventReaderinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),RawEventReaderclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)RawEventReader_dependencies.make :: dirs

ifndef QUICK
$(bin)RawEventReader_dependencies.make : $(src)components/RawEventReader_load.cxx $(src)components/RawEventReader_entries.cxx $(use_requirements) $(cmt_final_setup_RawEventReader)
	$(echo) "(RawEventReader.make) Rebuilding $@"; \
	  $(build_dependencies) RawEventReader -all_sources -out=$@ $(src)components/RawEventReader_load.cxx $(src)components/RawEventReader_entries.cxx
endif

#$(RawEventReader_dependencies)

-include $(bin)RawEventReader_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),RawEventReaderclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)RawEventReader_load.d

$(bin)$(binobj)RawEventReader_load.d : $(use_requirements) $(cmt_final_setup_RawEventReader)

$(bin)$(binobj)RawEventReader_load.d : $(src)components/RawEventReader_load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/RawEventReader_load.dep $(use_pp_cppflags) $(RawEventReader_pp_cppflags) $(lib_RawEventReader_pp_cppflags) $(RawEventReader_load_pp_cppflags) $(use_cppflags) $(RawEventReader_cppflags) $(lib_RawEventReader_cppflags) $(RawEventReader_load_cppflags) $(RawEventReader_load_cxx_cppflags) -I../src/components $(src)components/RawEventReader_load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/RawEventReader_load.o $(src)components/RawEventReader_load.cxx $(@D)/RawEventReader_load.dep
endif
endif

$(bin)$(binobj)RawEventReader_load.o : $(src)components/RawEventReader_load.cxx
else
$(bin)RawEventReader_dependencies.make : $(RawEventReader_load_cxx_dependencies)

$(bin)$(binobj)RawEventReader_load.o : $(RawEventReader_load_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/RawEventReader_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(RawEventReader_pp_cppflags) $(lib_RawEventReader_pp_cppflags) $(RawEventReader_load_pp_cppflags) $(use_cppflags) $(RawEventReader_cppflags) $(lib_RawEventReader_cppflags) $(RawEventReader_load_cppflags) $(RawEventReader_load_cxx_cppflags) -I../src/components $(src)components/RawEventReader_load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),RawEventReaderclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)RawEventReader_entries.d

$(bin)$(binobj)RawEventReader_entries.d : $(use_requirements) $(cmt_final_setup_RawEventReader)

$(bin)$(binobj)RawEventReader_entries.d : $(src)components/RawEventReader_entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/RawEventReader_entries.dep $(use_pp_cppflags) $(RawEventReader_pp_cppflags) $(lib_RawEventReader_pp_cppflags) $(RawEventReader_entries_pp_cppflags) $(use_cppflags) $(RawEventReader_cppflags) $(lib_RawEventReader_cppflags) $(RawEventReader_entries_cppflags) $(RawEventReader_entries_cxx_cppflags) -I../src/components $(src)components/RawEventReader_entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/RawEventReader_entries.o $(src)components/RawEventReader_entries.cxx $(@D)/RawEventReader_entries.dep
endif
endif

$(bin)$(binobj)RawEventReader_entries.o : $(src)components/RawEventReader_entries.cxx
else
$(bin)RawEventReader_dependencies.make : $(RawEventReader_entries_cxx_dependencies)

$(bin)$(binobj)RawEventReader_entries.o : $(RawEventReader_entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/RawEventReader_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(RawEventReader_pp_cppflags) $(lib_RawEventReader_pp_cppflags) $(RawEventReader_entries_pp_cppflags) $(use_cppflags) $(RawEventReader_cppflags) $(lib_RawEventReader_cppflags) $(RawEventReader_entries_cppflags) $(RawEventReader_entries_cxx_cppflags) -I../src/components $(src)components/RawEventReader_entries.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: RawEventReaderclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(RawEventReader.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(RawEventReader.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(RawEventReader.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(RawEventReader.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_RawEventReader)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawEventReader.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawEventReader.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(RawEventReader.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

RawEventReaderclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library RawEventReader
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)RawEventReader$(library_suffix).a $(library_prefix)RawEventReader$(library_suffix).s? RawEventReader.stamp RawEventReader.shstamp
#-- end of cleanup_library ---------------
