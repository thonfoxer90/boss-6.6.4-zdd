

#include "RawEventReader/RawEventReader.h"

using namespace Event;


/////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
RawEventReader::RawEventReader(const std::string& name, ISvcLocator* pSvcLocator) :
  Algorithm(name, pSvcLocator)
//-----------------------------------------------------------------------------
{
    m_mdcCnv = MdcConverter::instance();
    m_tofCnv = TofConverter::instance();
    m_emcCnv = EmcConverter::instance();
    m_mucCnv = MucConverter::instance();

    declareProperty("MdcDigi",m_mdcdigi=true);
    declareProperty("TofDigi",m_tofdigi=true);
    declareProperty("EmcDigi",m_emcdigi=true);
    declareProperty("MucDigi",m_mucdigi=true);
    declareProperty("InputFiles",m_inputFiles);

    m_iEntry      = -1;
    m_pTree       = 0;
    m_iFileNumber = 0;

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
RawEventReader::~RawEventReader()
//-----------------------------------------------------------------------------
{
    MdcConverter::destroy();
    TofConverter::destroy();
    EmcConverter::destroy();
    MucConverter::destroy();
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
StatusCode RawEventReader::initialize()
//-----------------------------------------------------------------------------
{
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in initialize()" << endreq;

    m_mdcCnv->init(0, 999999);
    ISvcLocator* svcLocator = Gaudi::svcLocator();
    StatusCode sc=svcLocator->service("EventDataSvc", m_evtSvc);
    if (sc.isFailure()) {std::cout<<"Could not accesss EventDataSvc!"<<std::endl;}

    return StatusCode::SUCCESS;

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
StatusCode RawEventReader::execute()
//-----------------------------------------------------------------------------
{

    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;

    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");

    DigiEvent* aDigiEvent = new DigiEvent;
    StatusCode sc = m_evtSvc->registerObject("/Event/Digi",aDigiEvent);
    if(sc!=StatusCode::SUCCESS) {
        std::cout<< "Could not register DigiEvent" <<std::endl;
    }

    if(!mcParticles) {
        //cout<<"loading raw data"<<endl;
        readEventData();
    }
    else {
        //cout<<"loading raw MC"<<endl;
        readEventMC();
    }

    readMdc();
    readTof();
    readEmc();
    readMuc();

    // clear the Memory Buffer, so that m_raw_event can be used directly during next event loop
    if(!mcParticles) {
        m_raw_event->reset();
    }

    return StatusCode::SUCCESS;
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readEventMC()
//-----------------------------------------------------------------------------
{
   // at end of file
    if (m_pTree != 0 && m_iEntry > 0 && m_iEntry == m_pTree->GetEntries()) {
        m_pTree = 0;
        m_iFileNumber ++;
        m_iEntry = 0;
    }
    // in middle of file
    else if (m_pTree != 0) {m_iEntry ++;}
    // emergency
    else {m_pTree = 0; m_iEntry = 0;}

    // load next file
    if (m_pTree == 0) {
        TFile* input = TFile::Open(m_inputFiles[m_iFileNumber].c_str(), "READ");
        std::cout<<"input file: "<<m_inputFiles[m_iFileNumber].c_str()<<std::endl;
        m_pTree = (TTree*)input->Get("Event");
        m_pTree->SetMakeClass(1);
    }
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readEventData()
//-----------------------------------------------------------------------------
{
    static RawFileReader* fileReader = new RawFileReader(m_inputFiles);
    m_raw_event = new RAWEVENT;
    const uint32_t* fragment;

    try {
        fragment = fileReader->nextEvent();
    }
    catch (RawFileException& ex) {
            ex.print();
    }
    catch (...) {
            std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
    }

    RawEvent f(fragment);
    uint32_t *robs[64];
    int nrobs = eformat::get_robs(fragment, (const uint32_t **)robs, 64);
    for (int robi = 0; robi < nrobs; robi++) {
        eformat::ROBFragment<uint32_t*> rob(robs[robi]);
        if ((rob.rod_detev_type() & 0x2) != 0) continue;  //bad data caogf add
        uint32_t* dataptr = NULL;
        rob.rod_data(dataptr);
        uint32_t source_id_number = rob.rod_source_id();
        source_id_number <<= 8;
        source_id_number >>= 24;
        switch(source_id_number) {
          case 161:
            m_raw_event->addReMdcDigi(dataptr, rob.rod_ndata());
            break;
          case 163:
            m_raw_event->addReEmcDigi(dataptr, rob.rod_ndata());
            break;
          case 162:
            m_raw_event->addReTofDigi(dataptr, rob.rod_ndata());
            break;
          case 164:
            m_raw_event->addReMucDigi(dataptr, rob.rod_ndata());
            break;
        }
    }

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
StatusCode RawEventReader::finalize()
//-----------------------------------------------------------------------------
{
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in finalize()" << endreq;

    return StatusCode::SUCCESS;
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readMdc()
//-----------------------------------------------------------------------------
{
    if(!m_mdcdigi) {return;}
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    MdcDigiCol* mdcDigiTdsCol = new MdcDigiCol;

    // data
    if(!mcParticles) {
        const BufferHolder& mdcBuf = m_raw_event->getMdcBuf();
        m_mdcCnv->convert(mdcBuf, mdcDigiTdsCol);
    }
    // MC
    else {
        TObjArray* mdcDigiCol;
        TBranch *branch = m_pTree->GetBranch("m_mdcDigiCol");
        branch->SetAddress(&mdcDigiCol);
        mdcDigiCol=0; // ???
        branch->GetEntry(m_iEntry);
        TIter mdcDigiIter(mdcDigiCol);
        TMdcDigi* mdcDigiRoot = 0;

        while ((mdcDigiRoot = (TMdcDigi*)mdcDigiIter.Next())) {
            MdcDigi *mdcDigiTds = new MdcDigi(mdcDigiRoot->getIntId());
            mdcDigiTds->setTimeChannel  (mdcDigiRoot->getTimeChannel());
            mdcDigiTds->setChargeChannel(mdcDigiRoot->getChargeChannel());
            mdcDigiTds->setOverflow     (mdcDigiRoot->getOverflow());
            mdcDigiTds->setTrackIndex   (mdcDigiRoot->getTrackIndex());
            mdcDigiTdsCol->push_back(mdcDigiTds);
        }
    }

    //register MdcDigiCol to TDS...
    StatusCode scMdc = m_evtSvc->registerObject("/Event/Digi/MdcDigiCol", mdcDigiTdsCol);
    if( scMdc!=StatusCode::SUCCESS ) {std::cout<< "Could not register MDC digi collection" <<std::endl;}

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readTof()
//-----------------------------------------------------------------------------
{
    if(!m_mdcdigi) {return;}
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    TofDigiCol* tofDigiTdsCol = new TofDigiCol;

    // data
    if(!mcParticles) {
        const BufferHolder& tofBuf = m_raw_event->getTofBuf();
        m_tofCnv->convert(tofBuf, tofDigiTdsCol);
    }
    // MC
    else {
        TBranch *branch = m_pTree->GetBranch("m_tofDigiCol");
        TObjArray* tofDigiCol;
        branch->SetAddress(&tofDigiCol);
        tofDigiCol=0;
        branch->GetEntry(m_iEntry);
        TIter tofDigiIter(tofDigiCol);
        TTofDigi *tofDigiRoot = 0;

        while ((tofDigiRoot = (TTofDigi*)tofDigiIter.Next())) {
            TofDigi *tofDigiTds = new TofDigi(tofDigiRoot->getIntId());
            tofDigiTds->setTimeChannel(tofDigiRoot->getTimeChannel());
            tofDigiTds->setChargeChannel(tofDigiRoot->getChargeChannel());
            tofDigiTds->setOverflow(tofDigiRoot->getOverflow());
            tofDigiTds->setTrackIndex(tofDigiRoot->getTrackIndex());
            tofDigiTdsCol->push_back(tofDigiTds);
        }
    }

  //register TOF digits collection to TDS
  StatusCode scTof = m_evtSvc->registerObject("/Event/Digi/TofDigiCol", tofDigiTdsCol);
  if( scTof!=StatusCode::SUCCESS ) {std::cout<< "Could not register TOF digi collection" <<std::endl;}

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readEmc()
//-----------------------------------------------------------------------------
{
    if(!m_mdcdigi) {return;}
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    EmcDigiCol* emcDigiTdsCol = new EmcDigiCol;

    // data
    if(!mcParticles) {
        const BufferHolder& emcBuf = m_raw_event->getEmcBuf();
        m_emcCnv->convert(emcBuf, emcDigiTdsCol);
    }
    // MC
    else {
        TBranch *branch = m_pTree->GetBranch("m_emcDigiCol");
        TObjArray* emcDigiCol;
        branch->SetAddress(&emcDigiCol);
        emcDigiCol=0;
        branch->GetEntry(m_iEntry);
        TIter emcDigiIter(emcDigiCol);
        TEmcDigi *emcDigiRoot = 0;

        while ((emcDigiRoot = (TEmcDigi*)emcDigiIter.Next())) {
            EmcDigi *emcDigiTds = new EmcDigi(emcDigiRoot->getIntId());
            emcDigiTds->setTimeChannel  (emcDigiRoot->getTimeChannel());
            emcDigiTds->setChargeChannel(emcDigiRoot->getChargeChannel());
            emcDigiTds->setMeasure      (emcDigiRoot->getMeasure());
            emcDigiTds->setTrackIndex   (emcDigiRoot->getTrackIndex());
            emcDigiTdsCol->push_back(emcDigiTds);
        }
    }

  //register EMC digits collection to TDS
  StatusCode scEmc = m_evtSvc->registerObject("/Event/Digi/EmcDigiCol", emcDigiTdsCol);
  if( scEmc!=StatusCode::SUCCESS ) {std::cout<< "Could not register EMC digi collection" <<std::endl;}

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readMuc()
//-----------------------------------------------------------------------------
{
    if(!m_mdcdigi) {return;}
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    MucDigiCol* mucDigiTdsCol = new MucDigiCol;

    // data
    if(!mcParticles) {
        const BufferHolder& mucBuf = m_raw_event->getMucBuf();
        m_mucCnv->convert(mucBuf, mucDigiTdsCol);
    }
    // MC
    else {
        TBranch *branch = m_pTree->GetBranch("m_mucDigiCol");
        TObjArray* mucDigiCol;
        branch->SetAddress(&mucDigiCol);
        mucDigiCol=0;
        branch->GetEntry(m_iEntry);
        TIter mucDigiIter(mucDigiCol);
        TMucDigi *mucDigiRoot = 0;

        while ((mucDigiRoot = (TMucDigi*)mucDigiIter.Next())) {
            MucDigi* mucDigiTds = new MucDigi(mucDigiRoot->getIntId());
            mucDigiTds->setTimeChannel(mucDigiRoot->getTimeChannel());
            mucDigiTds->setChargeChannel(mucDigiRoot->getChargeChannel());
            mucDigiTds->setTrackIndex(mucDigiRoot->getTrackIndex());
            mucDigiTdsCol->push_back(mucDigiTds);
        }
    }

  //register MUC digits collection to TDS
  StatusCode scMuc = m_evtSvc->registerObject("/Event/Digi/MucDigiCol", mucDigiTdsCol);
  if( scMuc!=StatusCode::SUCCESS ) {std::cout<< "Could not register MUC digi collection" <<std::endl;}

}
//-----------------------------------------------------------------------------

