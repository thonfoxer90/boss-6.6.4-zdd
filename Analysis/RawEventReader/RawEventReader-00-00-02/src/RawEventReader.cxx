

#include "RawEventReader/RawEventReader.h"
#include "McTruth/ZddMcHit.h"

using namespace Event;


/////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
RawEventReader::RawEventReader(const std::string& name, ISvcLocator* pSvcLocator) :
  Algorithm(name, pSvcLocator)
//-----------------------------------------------------------------------------
{
    m_mdcCnv = MdcConverter::instance();
    m_tofCnv = TofConverter::instance();
    m_emcCnv = EmcConverter::instance();
    m_mucCnv = MucConverter::instance();
		m_zddCnv = ZddConverter::instance();

    declareProperty("MdcDigi",m_mdcdigi=true);
    declareProperty("TofDigi",m_tofdigi=true);
    declareProperty("EmcDigi",m_emcdigi=true);
    declareProperty("MucDigi",m_mucdigi=true);
		declareProperty("ZddDigi",m_zdddigi=true);
		declareProperty("ZddMcHit",m_zddMcHit=true);
    declareProperty("InputFiles",m_inputFiles);

    m_iEntry      = -1;
    m_pTree       = 0;
    m_iFileNumber = 0;

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
RawEventReader::~RawEventReader()
//-----------------------------------------------------------------------------
{
		m_outfile->cd();
      m_outtree->Write();
      m_outfile->Write();
      m_outfile->Close();
      m_outtree = NULL;
      m_outfile = NULL;
			
    MdcConverter::destroy();
    TofConverter::destroy();
    EmcConverter::destroy();
    MucConverter::destroy();
		ZddConverter::destroy();
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
StatusCode RawEventReader::initialize()
//-----------------------------------------------------------------------------
{
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in initialize()" << endreq;

    m_mdcCnv->init(0, 999999);
    ISvcLocator* svcLocator = Gaudi::svcLocator();
    StatusCode sc=svcLocator->service("EventDataSvc", m_evtSvc);
    if (sc.isFailure()) {std::cout<<"Could not accesss EventDataSvc!"<<std::endl;}

		m_event=0;
			
		m_outfile = new TFile("RawEventReader.root","recreate");
		m_outtree = new TTree("RawEventReader","");
		m_outtree->Branch("m_event",&m_event,"m_event/i");
    m_outtree->Branch("m_chargechannel",&m_chargechannel,"m_chargechannel/i");
		m_outtree->Branch("m_TrackIndex",&m_TrackIndex,"m_TrackIndex/i");
		
    return StatusCode::SUCCESS;

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
StatusCode RawEventReader::execute()
//-----------------------------------------------------------------------------
{

    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;
		
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");

    DigiEvent* aDigiEvent = new DigiEvent;
    StatusCode sc = m_evtSvc->registerObject("/Event/Digi",aDigiEvent);
    if(sc!=StatusCode::SUCCESS) {
        std::cout<< "Could not register DigiEvent" <<std::endl;
    }

    if(!mcParticles) {
        //cout<<"loading raw data"<<endl;
        readEventData();
    }
    else {
        //cout<<"loading raw MC"<<endl;
        readEventMC();
    }

    readMdc();
    readTof();
    readEmc();
    readMuc();
		readZdd();
		readZddMCHitCol();
		
		// MUC information from raw data
    SmartDataPtr<MucDigiCol> mucDigiCol(eventSvc(),"/Event/Digi/MucDigiCol");
    for (MucDigiCol::iterator iterMuc = mucDigiCol->begin();iterMuc != mucDigiCol->end(); iterMuc++) {
         Identifier 	 identy = (*iterMuc)->identify();
        // MucID::barrel_ec(mucID);
        // MucID::segment(mucID);
        // MucID::layer(mucID);
        // MucID::channel(mucID);

    }

    unsigned int overflow;
    SmartDataPtr<TofDigiCol> tofDigiCol(eventSvc(),"/Event/Digi/TofDigiCol");
    for(TofDigiCol::iterator iterTof = tofDigiCol->begin(); iterTof!=tofDigiCol->end(); iterTof++ ) {
        overflow = (*iterTof)->getOverflow();
        if ( ( overflow & 0xfe000000 ) == 0xfe000000 ) {continue;}
        Identifier 	identy = (*iterTof)->identify();
        // TofID::barrel_ec(identy);
        // TofID::layer(identy);
        // TofID::phi_module(identy);
        // TofID::end(identy);
        //  adc = (*iterTof)->getChargeChannel();
        //  tdc = (*iterTof)->getTimeChannel();
		} 
		
		// ZDD information from raw data
    SmartDataPtr<ZddDigiCol> zddDigiCol(eventSvc(),"/Event/Digi/ZddDigiCol");
    for (ZddDigiCol::iterator iterZdd = zddDigiCol->begin();iterZdd != zddDigiCol->end(); iterZdd++) {
         Identifier 	 identy = (*iterZdd)->identify();
				long int chargeChannel = (*iterZdd)->getChargeChannel();
				printf("RawEventReader::ZddDigiCol::chargeChannel = %ld \n",chargeChannel);
    }
		
		// ZDD information from MC Hit collection
		SmartDataPtr<ZddMcHitCol> zddMcHitCol(eventSvc(), "/Event/MC/ZddMcHitCol");       
	                     
	 for (ZddMcHitCol::iterator iterZDD = zddMcHitCol->begin();iterZDD != zddMcHitCol->end(); iterZDD++) {
		printf("RawEventReader::ZddMcHitCol::getEDep() = %f \n", (*iterZDD)->getEDep());
		printf("RawEventReader::ZddMcHitCol::getPDGCode() = %d \n", (*iterZDD)->getPDGCode());
		printf("RawEventReader::ZddMcHitCol::getTrackID() = %d \n", (*iterZDD)->getTrackID());
		printf("\n");
		}
		
    // clear the Memory Buffer, so that m_raw_event can be used directly during next event loop
    if(!mcParticles) {
        m_raw_event->reset();
    }
		
		m_event++;
		
    return StatusCode::SUCCESS;
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readEventMC()
//-----------------------------------------------------------------------------
{
   // at end of file
    if (m_pTree != 0 && m_iEntry > 0 && m_iEntry == m_pTree->GetEntries()) {
        m_pTree = 0;
        m_iFileNumber ++;
        m_iEntry = 0;
    }
    // in middle of file
    else if (m_pTree != 0) {m_iEntry ++;}
    // emergency
    else {m_pTree = 0; m_iEntry = 0;}

    // load next file
    if (m_pTree == 0) {
        TFile* input = TFile::Open(m_inputFiles[m_iFileNumber].c_str(), "READ");
        std::cout<<"input file: "<<m_inputFiles[m_iFileNumber].c_str()<<std::endl;
        m_pTree = (TTree*)input->Get("Event");
        m_pTree->SetMakeClass(1);
    }
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readEventData()
//-----------------------------------------------------------------------------
{
    static RawFileReader* fileReader = new RawFileReader(m_inputFiles);
    m_raw_event = new RAWEVENT;
    const uint32_t* fragment;

    try {
        fragment = fileReader->nextEvent();
    }
    catch (RawFileException& ex) {
            ex.print();
    }
    catch (...) {
            std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
    }

    RawEvent f(fragment);
    uint32_t *robs[64];
    int nrobs = eformat::get_robs(fragment, (const uint32_t **)robs, 64);
    for (int robi = 0; robi < nrobs; robi++) {
        eformat::ROBFragment<uint32_t*> rob(robs[robi]);
        if ((rob.rod_detev_type() & 0x2) != 0) continue;  //bad data caogf add
        uint32_t* dataptr = NULL;
        rob.rod_data(dataptr);
        uint32_t source_id_number = rob.rod_source_id();
        source_id_number <<= 8;
        source_id_number >>= 24;
        switch(source_id_number) {
          case 161:
            m_raw_event->addReMdcDigi(dataptr, rob.rod_ndata());
            break;
          case 163:
            m_raw_event->addReEmcDigi(dataptr, rob.rod_ndata());
            break;
          case 162:
            m_raw_event->addReTofDigi(dataptr, rob.rod_ndata());
            break;
          case 164:
            m_raw_event->addReMucDigi(dataptr, rob.rod_ndata());
            break;
        }
    }

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
StatusCode RawEventReader::finalize()
//-----------------------------------------------------------------------------
{
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in finalize()" << endreq;

    return StatusCode::SUCCESS;
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readMdc()
//-----------------------------------------------------------------------------
{
    if(!m_mdcdigi) {return;}
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    MdcDigiCol* mdcDigiTdsCol = new MdcDigiCol;

    // data
    if(!mcParticles) {
        const BufferHolder& mdcBuf = m_raw_event->getMdcBuf();
        m_mdcCnv->convert(mdcBuf, mdcDigiTdsCol);
    }
    // MC
    else {
        TObjArray* mdcDigiCol;
        TBranch *branch = m_pTree->GetBranch("m_mdcDigiCol");
        branch->SetAddress(&mdcDigiCol);
        mdcDigiCol=0; // ???
        branch->GetEntry(m_iEntry);
        TIter mdcDigiIter(mdcDigiCol);
        TMdcDigi* mdcDigiRoot = 0;

        while ((mdcDigiRoot = (TMdcDigi*)mdcDigiIter.Next())) {
            MdcDigi *mdcDigiTds = new MdcDigi(mdcDigiRoot->getIntId());
            mdcDigiTds->setTimeChannel  (mdcDigiRoot->getTimeChannel());
            mdcDigiTds->setChargeChannel(mdcDigiRoot->getChargeChannel());
            mdcDigiTds->setOverflow     (mdcDigiRoot->getOverflow());
            mdcDigiTds->setTrackIndex   (mdcDigiRoot->getTrackIndex());
            mdcDigiTdsCol->push_back(mdcDigiTds);
        }
    }

    //register MdcDigiCol to TDS...
    StatusCode scMdc = m_evtSvc->registerObject("/Event/Digi/MdcDigiCol", mdcDigiTdsCol);
    if( scMdc!=StatusCode::SUCCESS ) {std::cout<< "Could not register MDC digi collection" <<std::endl;}

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readTof()
//-----------------------------------------------------------------------------
{
    if(!m_mdcdigi) {return;}
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    TofDigiCol* tofDigiTdsCol = new TofDigiCol;

    // data
    if(!mcParticles) {
        const BufferHolder& tofBuf = m_raw_event->getTofBuf();
        m_tofCnv->convert(tofBuf, tofDigiTdsCol);
    }
    // MC
    else {
        TBranch *branch = m_pTree->GetBranch("m_tofDigiCol");
        TObjArray* tofDigiCol;
        branch->SetAddress(&tofDigiCol);
        tofDigiCol=0;
        branch->GetEntry(m_iEntry);
        TIter tofDigiIter(tofDigiCol);
        TTofDigi *tofDigiRoot = 0;

        while ((tofDigiRoot = (TTofDigi*)tofDigiIter.Next())) {
            TofDigi *tofDigiTds = new TofDigi(tofDigiRoot->getIntId());
            tofDigiTds->setTimeChannel(tofDigiRoot->getTimeChannel());
            tofDigiTds->setChargeChannel(tofDigiRoot->getChargeChannel());
            tofDigiTds->setOverflow(tofDigiRoot->getOverflow());
            tofDigiTds->setTrackIndex(tofDigiRoot->getTrackIndex());
            tofDigiTdsCol->push_back(tofDigiTds);
        }
    }

  //register TOF digits collection to TDS
  StatusCode scTof = m_evtSvc->registerObject("/Event/Digi/TofDigiCol", tofDigiTdsCol);
  if( scTof!=StatusCode::SUCCESS ) {std::cout<< "Could not register TOF digi collection" <<std::endl;}

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readEmc()
//-----------------------------------------------------------------------------
{
    if(!m_mdcdigi) {return;}
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    EmcDigiCol* emcDigiTdsCol = new EmcDigiCol;

    // data
    if(!mcParticles) {
        const BufferHolder& emcBuf = m_raw_event->getEmcBuf();
        m_emcCnv->convert(emcBuf, emcDigiTdsCol);
    }
    // MC
    else {
        TBranch *branch = m_pTree->GetBranch("m_emcDigiCol");
        TObjArray* emcDigiCol;
        branch->SetAddress(&emcDigiCol);
        emcDigiCol=0;
        branch->GetEntry(m_iEntry);
        TIter emcDigiIter(emcDigiCol);
        TEmcDigi *emcDigiRoot = 0;

        while ((emcDigiRoot = (TEmcDigi*)emcDigiIter.Next())) {
            EmcDigi *emcDigiTds = new EmcDigi(emcDigiRoot->getIntId());
            emcDigiTds->setTimeChannel  (emcDigiRoot->getTimeChannel());
            emcDigiTds->setChargeChannel(emcDigiRoot->getChargeChannel());
            emcDigiTds->setMeasure      (emcDigiRoot->getMeasure());
            emcDigiTds->setTrackIndex   (emcDigiRoot->getTrackIndex());
            emcDigiTdsCol->push_back(emcDigiTds);
						
        }
    }

  //register EMC digits collection to TDS
  StatusCode scEmc = m_evtSvc->registerObject("/Event/Digi/EmcDigiCol", emcDigiTdsCol);
  if( scEmc!=StatusCode::SUCCESS ) {std::cout<< "Could not register EMC digi collection" <<std::endl;}

}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void RawEventReader::readMuc()
//-----------------------------------------------------------------------------
{
    if(!m_mdcdigi) {return;}
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    MucDigiCol* mucDigiTdsCol = new MucDigiCol;

    // data
    if(!mcParticles) {
        const BufferHolder& mucBuf = m_raw_event->getMucBuf();
        m_mucCnv->convert(mucBuf, mucDigiTdsCol);
    }
    // MC
    else {
        TBranch *branch = m_pTree->GetBranch("m_mucDigiCol");
        TObjArray* mucDigiCol;
        branch->SetAddress(&mucDigiCol);
        mucDigiCol=0;
        branch->GetEntry(m_iEntry);
        TIter mucDigiIter(mucDigiCol);
        TMucDigi *mucDigiRoot = 0;

        while ((mucDigiRoot = (TMucDigi*)mucDigiIter.Next())) {
            MucDigi* mucDigiTds = new MucDigi(mucDigiRoot->getIntId());
            mucDigiTds->setTimeChannel(mucDigiRoot->getTimeChannel());
            mucDigiTds->setChargeChannel(mucDigiRoot->getChargeChannel());
            mucDigiTds->setTrackIndex(mucDigiRoot->getTrackIndex());
            mucDigiTdsCol->push_back(mucDigiTds);
        }
    }

  //register MUC digits collection to TDS
  StatusCode scMuc = m_evtSvc->registerObject("/Event/Digi/MucDigiCol", mucDigiTdsCol);
  if( scMuc!=StatusCode::SUCCESS ) {std::cout<< "Could not register MUC digi collection" <<std::endl;}

}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void RawEventReader::readZdd()
//-----------------------------------------------------------------------------
{

if(!m_zdddigi) {return;}
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    ZddDigiCol *ZddTdsdigiCol = new ZddDigiCol;

// data
    if(!mcParticles) {
    }
    // MC
    else {
        TBranch *branch = m_pTree->GetBranch("m_zddDigiCol");
        TObjArray* zddDigiCol;
        branch->SetAddress(&zddDigiCol);
        zddDigiCol=0;
        branch->GetEntry(m_iEntry);
        TIter zddDigiIter(zddDigiCol);
        TZddDigi *zddDigiRoot = 0;	
				
        while ((zddDigiRoot = (TZddDigi*)zddDigiIter.Next())) {
            ZddDigi *ZddDigiTds = new ZddDigi(zddDigiRoot->getIntId());
            ZddDigiTds->setTimeChannel  (zddDigiRoot->getTimeChannel());
            ZddDigiTds->setChargeChannel(zddDigiRoot->getChargeChannel());
            ZddDigiTds->setMeasure      (zddDigiRoot->getMeasure());
            ZddDigiTds->setTrackIndex   (zddDigiRoot->getTrackIndex());
            ZddTdsdigiCol->push_back(ZddDigiTds);
						
						m_chargechannel=zddDigiRoot->getChargeChannel();
						m_TrackIndex=zddDigiRoot->getTrackIndex();
						m_outtree->Fill();
						
        }
				
    }

 StatusCode scZdd = m_evtSvc->registerObject("/Event/Digi/ZddDigiCol", ZddTdsdigiCol);
  if( scZdd!=StatusCode::SUCCESS ) {std::cout<< "Could not register ZDD digi collection" <<std::endl;}
	
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void RawEventReader::readZddMCHitCol()
//-----------------------------------------------------------------------------
{

if(!m_zdddigi) {return;}
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    ZddMcHitCol* ZddTdsMcHitCol = new ZddMcHitCol;

// data
    if(!mcParticles) {
    }
    // MC
    else {
        TBranch *branch = m_pTree->GetBranch("m_zddMcHitCol");
        TObjArray* zddMCHitCol;
        branch->SetAddress(&zddMCHitCol);
        zddMCHitCol=0;
        branch->GetEntry(m_iEntry);
        TIter zddMCIter(zddMCHitCol);
        TZddMc *zddMCRoot = 0;	
				
        while ((zddMCRoot = (TZddMc*)zddMCIter.Next())) {
            ZddMcHit *ZddMcHitTds = new ZddMcHit();
						ZddMcHitTds->setEDep(zddMCRoot->getEDep());
						ZddMcHitTds->setPDGCode(zddMCRoot->getPDGCode());
						cout << "zddMCRoot->getPDGCode()" << zddMCRoot->getPDGCode() <<endl;
						ZddMcHitTds->setTrackID(zddMCRoot->getTrackID());
						ZddTdsMcHitCol->push_back(ZddMcHitTds);
        }
				
    }

 StatusCode scZdd = m_evtSvc->registerObject("/Event/MC/ZddMcHitCol", ZddTdsMcHitCol);
 if( scZdd!=StatusCode::SUCCESS ) {std::cout<< "Could not register ZDD MC Hit collection" <<std::endl;}
	
}
//-----------------------------------------------------------------------------
