
#include "GaudiKernel/Algorithm.h"
#include <string>

#include "GaudiKernel/IDataProviderSvc.h"
#include "RawDataCnv/EventManagement/RAWEVENT.h"

#include "McTruth/McParticle.h"
//#include "ParticleID/ParticleID.h"

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
//#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/PropertyMgr.h"
//#include "GaudiKernel/DataObject.h"
//#include "GaudiKernel/GenericAddress.h"
//#include "GaudiKernel/ClassID.h"

#include "RawDataCnv/EventManagement/RAWEVENT.h"
#include "RawDataCnv/Util/MdcConverter.h"
#include "RawDataCnv/Util/TofConverter.h"
#include "RawDataCnv/Util/EmcConverter.h"
#include "RawDataCnv/Util/MucConverter.h"
#include "Identifier/Identifier.h"
#include "Identifier/MdcID.h"
#include "Identifier/TofID.h"
#include "Identifier/EmcID.h"
#include "Identifier/MucID.h"
#include "IRawFile/RawFileExceptions.h"

#include "RawFile/RawFileReader.h"
#include "RawEvent/DigiEvent.h"

#include "EventModel/EventModel.h"
#include "EventModel/EventHeader.h"

#include "EvtRecEvent/EvtRecEvent.h"
#include "TrigEvent/TrigEvent.h"
#include "TrigEvent/TrigData.h"

//Digi
#include "MdcRawEvent/MdcDigi.h"
#include "EmcRawEvent/EmcDigi.h"
#include "TofRawEvent/TofDigi.h"
#include "MucRawEvent/MucDigi.h"

#include "RootEventData/TMdcDigi.h"
#include "RootEventData/TTofDigi.h"
#include "RootEventData/TEmcDigi.h"
#include "RootEventData/TMucDigi.h"

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TObjArray.h"


class MdcConverter;
class TofConverter;
class EmcConverter;
class MucConverter;
class TTree;

class RawEventReader:public Algorithm {
public:
  RawEventReader(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~RawEventReader();
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();
  

private:

  void readEventData();
  void readEventMC();
  void readMdc();
  void readTof();
  void readEmc();
  void readMuc();

  IDataProviderSvc* m_evtSvc;

  MdcConverter* m_mdcCnv;
  TofConverter* m_tofCnv;
  EmcConverter* m_emcCnv;
  MucConverter* m_mucCnv;

  bool m_bOpenOnlyPureNeutral;
  bool m_mdcdigi;
  bool m_tofdigi;
  bool m_emcdigi;
  bool m_mucdigi;

  TTree* m_pTree;
  int    m_iEntry;
  int    m_iFileNumber;

  RAWEVENT* m_raw_event;
  std::vector<std::string> m_inputFiles;

};

