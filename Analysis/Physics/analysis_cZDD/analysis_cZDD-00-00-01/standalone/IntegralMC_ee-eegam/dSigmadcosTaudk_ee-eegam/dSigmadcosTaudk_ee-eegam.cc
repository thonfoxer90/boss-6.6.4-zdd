#include <stdlib.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_miser.h>
#include <gsl/gsl_monte_vegas.h>
#include <stdio.h>
#include <string.h>

double const alphaEM=1/137.; //Fine structure constant
double const elecClassRadius=2.8179403267e-15; //Classical electron radius (m)
double const massElec=511e-6; //Electron energy (GeV)
double const meter2ToBarn=1e28;
double const electronvoltToJoule=1.602e-19;

double ECMS;
double BetaElecCMS;

struct my_f_params { double a; double b;};

double rho2(double k)
{
double rho2;
rho2=ECMS*ECMS-2*ECMS*k;

return rho2;
}

double func(double *x, size_t dim, void *params)
{
//(void)(dim); /* avoid unused parameter warnings */
 struct my_f_params * fp = (struct my_f_params *)params;
double f;
double a=(1/BetaElecCMS)*(pow(1-BetaElecCMS*(fp->a),-1)-pow(1-BetaElecCMS*(fp->b),-1))
				*pow((ECMS*ECMS+rho2(x[0]))/(ECMS*ECMS),2)
		-2*(pow(ECMS,4)+pow(rho2(x[0]),2))/pow(ECMS,4)*log((rho2(x[0])/(2*massElec*x[0]))*(ECMS/massElec))
				;

double b=(1/(2*BetaElecCMS))*(pow(1-BetaElecCMS*(fp->a),-2)-pow(1-BetaElecCMS*(fp->b),-2))
				*((log((rho2(x[1])/(2*massElec*x[1]))*(ECMS/massElec)))-2)
				*16*massElec*massElec*rho2(x[1])/(pow(ECMS,4))
				;

double c =(1/(3*BetaElecCMS))*(pow(1-BetaElecCMS*(fp->a),-3)-pow(1-BetaElecCMS*(fp->b),-3))
				 	*((log((rho2(x[1])/(2*massElec*x[1]))*(ECMS/massElec)))-2)
					*(32*pow(massElec,4)*rho2(x[1])/(pow(ECMS,6)))
					;

f=-(1/(2*M_PI))*4*alphaEM*elecClassRadius*elecClassRadius*(massElec*massElec/(ECMS*ECMS))/x[0]
	*(
	a+b+c
	//1.
	)
	;

return f;
}

/*
double
g (double *k, size_t dim, void *params)
{
  (void)(dim); 
  (void)(params);
  double A = 1.0 / (M_PI * M_PI * M_PI);
  return A / (1.0 - cos (k[0]) * cos (k[1]) * cos (k[2]));
}
*/

void
display_results (char *title, double result, double error)
{
	/*
  printf ("%s ==================\n", title);
  printf ("result = % .6e\n", result);
  printf ("sigma  = % .6e\n", error);
  printf ("exact  = % .6e\n", exact);
  printf ("error  = % .6e = %.2g sigma\n", result - exact,
          fabs (result - exact) / error);
	*/

	printf ("%s ==================\n", title);
  //printf ("result = % .6f mb\n", result*meter2ToBarn*1e3);
  //printf ("sigma  = % .6f mb\n", error*meter2ToBarn*1e3);
	printf ("result = % .6e \n", result);
}

//int main (void)
int main(int argc, char** argv)
{
	double E = atof(argv[1]);
	double cosTauMin = atof(argv[2]);
	double cosTauMax;
	cosTauMax=atof(argv[3]);
  printf("%f \n", cosTauMax);
	double EGamMin = atof(argv[4]);
	double EGamMax = atof(argv[5]);
	double PhiMin = atof(argv[6]);
	double PhiMax;
	if(strcmp(argv[7],"M_PI")==0) PhiMax=M_PI;
 	else if (strcmp(argv[7],"2*M_PI")==0) PhiMax=2*M_PI;
	else PhiMax = atof(argv[7]);

	ECMS=E;
	double EelecCMS=ECMS/2;
	double GammaElecCMS=EelecCMS/massElec;
	BetaElecCMS=sqrt(1-(1/(pow(GammaElecCMS,2))));
	printf("GammaElecCMS= %.8e Beta = %.8e \n", GammaElecCMS,BetaElecCMS);

  double res, err;

	double xl[2] = { EGamMin, PhiMin };
  double xu[2] = { EGamMax, PhiMax };
	printf("EGamMin = %f EGamMax= %f \n",EGamMin,EGamMax);
	printf("PhiMin = %f PhiMax= %f \n",PhiMin,PhiMax);
	struct my_f_params params = { cosTauMin, cosTauMax};

  const gsl_rng_type *T;
  gsl_rng *r;

  //gsl_monte_function G = { &func, 3, 0 };
	//gsl_monte_function G = { &func, 2, &params };
	
gsl_monte_function G;
G.f = &func;
G.dim = 2;
G.params = &params;


  size_t calls = 500000;

  gsl_rng_env_setup ();

  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  {
    gsl_monte_plain_state *s = gsl_monte_plain_alloc (2);
    gsl_monte_plain_integrate (&G, xl, xu, 2, calls, r, s, 
                               &res, &err);
    gsl_monte_plain_free (s);

    display_results ("plain", res, err);
		
  }

  {
    gsl_monte_miser_state *s = gsl_monte_miser_alloc (2);
    gsl_monte_miser_integrate (&G, xl, xu, 2, calls, r, s,
                               &res, &err);
    gsl_monte_miser_free (s);

    display_results ("miser", res, err);
  }

  {	/*
    gsl_monte_vegas_state *s = gsl_monte_vegas_alloc (2);

    gsl_monte_vegas_integrate (&G, xl, xu, 2, 10000, r, s,
                               &res, &err);
    display_results ("vegas warm-up", res, err);

    printf ("converging...\n");

    do
      {
        gsl_monte_vegas_integrate (&G, xl, xu, 2, calls/5, r, s,
                                   &res, &err);
        //printf ("result = % .6f sigma = % .6f "
        //        "chisq/dof = %.1f\n", res, err, gsl_monte_vegas_chisq (s));
      }
    while (fabs (gsl_monte_vegas_chisq (s) - 1.0) > 0.5);

    display_results ("vegas final", res, err);

    gsl_monte_vegas_free (s);
		*/
  }

  gsl_rng_free (r);

  return 0;
}
