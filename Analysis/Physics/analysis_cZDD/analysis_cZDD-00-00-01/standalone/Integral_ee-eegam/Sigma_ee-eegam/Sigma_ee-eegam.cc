#include <stdlib.h>
#include <gsl/gsl_math.h>
#include <stdio.h>
#include <string.h>

double const alphaEM=1/137.; //Fine structure constant
double const elecClassRadius=2.8179403267e-15; //Classical electron radius (m)
double const massElec=511e-6; //Electron energy (GeV)
double const meter2ToBarn=1e28;
double const electronvoltToJoule=1.602e-19;

//=============================================================================
// Calculates integrated cross section (in mbarn) for e+e-->e+e-gamma process,
// using formula (32) from "Single Photon Emission in High Energy e+e- collision"
// (G.Altarelli and F.Buccella, Il Nuovo Cimento, Vol.XXXIV, N.5, 1964)
// Author : B.Garillon, University of Mainz
//=============================================================================


int main(int argc, char** argv)
{
double ECM=atof(argv[1]);
double eps=atof(argv[2]);

double XSec;
XSec=4*alphaEM*elecClassRadius*elecClassRadius
		 	*(
			((8./3.)*log(ECM/(2*eps))-(5./3.))*(log(ECM*ECM/(massElec*massElec)))
			+(4./3.)*pow((log(ECM/(2*eps))),2)
			-1.
			-4*pow(M_PI,2)/9
			)
		;
XSec=XSec*meter2ToBarn*1e3;

printf("sigma(eps=%f GeV) = %f mb \n", eps, XSec);
return 0;
}
