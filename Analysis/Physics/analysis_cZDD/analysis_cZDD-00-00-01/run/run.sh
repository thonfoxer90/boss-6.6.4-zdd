#!/bin/bash

option=$1

srcPath=/home/bgarillo/boss-6.6.4-ZDD/Analysis/Physics/analysis_cZDD/analysis_cZDD-00-00-01
dataPath=/data/work/kphpbb/bgarillo/cZDD

currentDir=$PWD

if [[ $option == *"--Init "* ]]
then
##BBBREM
cd ${dataPath}/BBBREM/Logs
rm *.txt
ls -1 MCAna_ISPBPipeAbsorber_RKO0-013_1* > fileList_electrongamma.txt
sed -i 's@MCAna@'"$PWD"'/MCAna@g' fileList_electrongamma.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList_electrongamma.txt

ls -1 MCAna_ISPBPipeAbsorber_RKO0-013_positrongamma* > fileList_positrongamma.txt
sed -i 's@MCAna@'"$PWD"'/MCAna@g' fileList_positrongamma.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList_positrongamma.txt

cd ${dataPath}/BBBREM/NTuples
find BBBREM_ana_ISPBPipeAbsorber_RKO0-013* -size -400k -delete
rm BBBREM_ana_ISPBPipeAbsorber_RKO0-013_electrongamma.root BBBREM_ana_ISPBPipeAbsorber_RKO0-013_positrongamma.root
hadd -f BBBREM_ana_ISPBPipeAbsorber_RKO0-013_electrongamma.root BBBREM_ana_ISPBPipeAbsorber_RKO0-013_1*
hadd -f BBBREM_ana_ISPBPipeAbsorber_RKO0-013_positrongamma.root BBBREM_ana_ISPBPipeAbsorber_RKO0-013_positrongamma_2*
##hadd -f BBBREM_ana_ISPBPipeAbsorber_RKO0-013.root BBBREM_ana_ISPBPipeAbsorber_RKO0-013_electrongamma.root BBBREM_ana_ISPBPipeAbsorber_RKO0-013_positrongamma.root

##Babayaga
cd ${dataPath}/Babayaga/Logs
rm *.txt
ls -1 ZDD_Babayaga_ISPBPipeAbsorber_NGam1_20ltThetalt160* > fileList.txt
sed -i 's@ZDD_Babayaga@'"$PWD"'/ZDD_Babayaga@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt

cd ${dataPath}/Babayaga/NTuples
rm Babayaga_ana_ISPBPipeAbsorber.root
hadd -f Babayaga_ana_ISPBPipeAbsorber.root Babayaga_ana_ISPBPipeAbsorber_NGam1_20ltThetalt160*

##BabayagaNLO
cd ${dataPath}/BabayagaNLO/Logs
rm *.txt
ls -1 ZDD_BabayagaNLO_ISPBPipeAbsorber_NGam1_10ltThetalt170* > fileList.txt
sed -i 's@ZDD_Babayaga@'"$PWD"'/ZDD_Babayaga@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt

cd ${dataPath}/BabayagaNLO/NTuples
rm BabayagaNLO_ana_ISPBPipeAbsorber.root
hadd -f BabayagaNLO_ana_ISPBPipeAbsorber.root BabayagaNLO_ana_ISPBPipeAbsorber_NGam1_10ltThetalt170*

##Phokhara
#NNbar
cd ${dataPath}/Phokhara/NNbar/Logs
rm *.txt
ls -1 Phokhara_NNbar_ISPBPipeAbsorber_1* > fileList.txt
sed -i 's@Phokhara_NNbar@'"$PWD"'/Phokhara_NNbar@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt

cd ${dataPath}/Phokhara/NNbar/NTuples
rm Phokhara_ana_NNbar_ISPBPipeAbsorber.root
hadd -f Phokhara_ana_NNbar_ISPBPipeAbsorber.root Phokhara_ana_NNbar_ISPBPipeAbsorber_1*

#2pi+2pi-
cd ${dataPath}/Phokhara/2pi+2pi-/Logs
rm *.txt
ls -1 Phokhara_2pi+2pi-_ISPBPipeAbsorber_1* > fileList.txt
sed -i 's@Phokhara_2pi+2pi-@'"$PWD"'/Phokhara_2pi+2pi-@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt

cd ${dataPath}/Phokhara/2pi+2pi-/NTuples
rm Phokhara_ana_2pi+2pi-_ISPBPipeAbsorber.root
hadd -f Phokhara_ana_2pi+2pi-_ISPBPipeAbsorber_new.root Phokhara_ana_2pi+2pi-_ISPBPipeAbsorber_1*

##BesEvtGen
##4piC1Pi0
cd ${dataPath}/BesEvtGen/4piC1pi0/Logs
rm *.txt
ls -1 4piC1pi0_ISPBPipeAbsorber_1000* > fileList.txt
sed -i 's@4piC1pi0@'"$PWD"'/4piC1pi0@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt

cd ${dataPath}/BesEvtGen/4piC1pi0/NTuples
rm -rf 4piC1pi0_ana_ISPBPipeAbsorber.root
#hadd -f 4piC1pi0_ana_ISPBPipeAbsorber-1.root 4piC1pi0_ana_ISPBPipeAbsorber_1000*
#hadd -f 4piC1pi0_ana_ISPBPipeAbsorber-2.root 4piC1pi0_ana_ISPBPipeAbsorber_1001*
#hadd -f 4piC1pi0_ana_ISPBPipeAbsorber-3.root 4piC1pi0_ana_ISPBPipeAbsorber_1002*
#hadd -f 4piC1pi0_ana_ISPBPipeAbsorber.root 4piC1pi0_ana_ISPBPipeAbsorber-1.root 4piC1pi0_ana_ISPBPipeAbsorber-2.root 4piC1pi0_ana_ISPBPipeAbsorber-3.root
#rm 4piC1pi0_ana_ISPBPipeAbsorber-*

hadd -f 4piC1pi0_ana_ISPBPipeAbsorber.root 4piC1pi0_ana_ISPBPipeAbsorber_1000*

##4piC1pi0gamPHOTOS
cd ${dataPath}/BesEvtGen/ConExC/4piC1pi0gam/Logs
rm *.txt
ls -1 4piC1pi0gam_ISPBPipeAbsorber_1000* > fileList.txt
sed -i 's@4piC1pi0gam@'"$PWD"'/4piC1pi0gam@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt

cd ${dataPath}/BesEvtGen/ConExC/4piC1pi0gam/NTuples
rm -rf 4piC1pi0gam_ana_ISPBPipeAbsorber.root
#hadd -f 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber-1.root 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber_1000*
#hadd -f 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber-2.root 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber_1001*
#hadd -f 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber-3.root 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber_1002*
#hadd -f 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber.root 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber-1.root 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber-2.root 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber-3.root
#rm 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber-*
hadd -f 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber-1.root 4piC1pi0gamPHOTOS_ana_ISPBPipeAbsorber_1000*


fi

if [[ $option == *"--ThetaCM_BhabhaISR "* ]]
then
cd /data/work/kphpbb/bgarillo/cZDD
root -l -b -q '~/boss-6.6.4-ZDD/Analysis/Physics/analysis_cZDD/analysis_cZDD-00-00-01/src/DrawThetaCM_BhabhaISR.c+("/data/work/kphpbb/bgarillo/cZDD/BBBREM/NTuples/BBBREM_ana_ISPBNoPipe_RKO0-013_100000-100099.root","/data/work/kphpbb/bgarillo/cZDD/Phokhara/NTuples/Phokhara_ana_2pi+2pi-_ISPBPipeAbsorber.root","ThetaCMGam_BBREMvs4PiGamISRPhok")'
fi

if [[ $option == *"--ZddXYTruth "* ]]
then
root -l -b -q '~/boss-6.6.4-ZDD/Analysis/Physics/analysis_cZDD/analysis_cZDD-00-00-01/src/DrawZddMCTruthXY.c+("Phokhara/2pi+2pi-/Phokhara_ana_2pi+2pi-_ISPBPipeAbsorber.root","/data/work/kphpbb/bgarillo/cZDD/Phokhara/2pi+2pi-/ZddMCTruthXY/XY","MCTruthISR")'
fi

if [[ $option == *"--PhotonAngleCoverage "* ]]
then
cd /data/work/kphpbb/bgarillo/cZDD
root -l -b -q '~/boss-6.6.4-ZDD/Analysis/Physics/analysis_cZDD/analysis_cZDD-00-00-01/src/DrawThetaCM_BhabhaISR.c+("/data/work/kphpbb/bgarillo/cZDD/BBBREM/NTuples/BBBREM_ana_ISPBNoPipe_RKO0-013_100000-100099.root","/data/work/kphpbb/bgarillo/cZDD/Phokhara/NTuples/Phokhara_ana_2pi+2pi-_ISPBPipeAbsorber.root","ThetaCMGam_BBREMvs4PiGamISRPhok")'
fi

if [[ $option == *"--CompareTagMissGamISR "* ]]
then
cd /data/work/kphpbb/bgarillo/cZDD/Phokhara
root -l
#.L ~/boss-6.6.4-ZDD/Analysis/Physics/analysis_cZDD/analysis_cZDD-00-00-01/src/CompareTagMissGamISR.c+
#SystStudy("NTuples/Phokhara_ana_2pi+2pi-_ISPBPipeAbsorber.root","CompareTagMissGamISR/CompareTagMissGamISR")
fi


if [[ $option == *"--Stage1OvrStage0"* ]]
then
root -l -b -q '~/boss-6.6.4-ZDD/Analysis/Physics/analysis_cZDD/analysis_cZDD-00-00-01/src/getNEZddCutvsEDepZdd.c+("/data/work/kphpbb/bgarillo/cZDD/Phokhara/2pi+2pi-/NTuples/Phokhara_ana_2pi+2pi-_ISPBPipeAbsorber.root","","/data/work/kphpbb/bgarillo/cZDD/Phokhara/2pi+2pi-/NEDepZDDvsEDepZDD/NEDepZDDvsEDepZDD")'
fi

if [[ $option == *"--taggingEfficiency"* ]]
then
root -l -b -q '~/boss-6.6.4-ZDD/Analysis/Physics/analysis_cZDD/analysis_cZDD-00-00-01/src/taggingEfficiency.c+("Phokhara/2pi+2pi-/NTuples/Phokhara_ana_2pi+2pi-_ISPBPipeAbsorber.root","","Phokhara/2pi+2pi-/taggingEfficiency/taggingEfficiency")'
fi

if [[ $option == *"--CompareETruthEZdd "* ]]
then
cd /data/work/kphpbb/bgarillo/cZDD/Phokhara
fi

if [[ $option == *"--YieldTable "* ]]
then
	root -l -b -q '/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/src/makeYieldTable.c+("makeYieldTable.in","NNbarISRTagged","YieldTable/NNbarISRTagged")'
fi


if [[ $option == *"--Normalize "* ]]
then
echo "Normalize"

root -l -b -q '/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/src/normalizeToLum.c+("normalizeToLum.in")'
root -l -b -q '/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/src/makeYieldTable.c+("makeYieldTable_Scaled.in","NNbarISRTagged","YieldTable/scaled/NNbarISRTagged")'
root -l -b -q '/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/src/makeYieldTable.c+("makeYieldTable_Scaled.in","4PiGamISRTagged","YieldTable/scaled/4PiGamISRTagged")'
echo 'gROOT->LoadMacro("/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/src/compDataMCScaled.c+");MultiCompDataMCScaled("compDataMCScaled.in","Histo=NNbarISRTagged isLogScale=1 hasData=0","compDataMCScaled");' | root -l -b
echo 'gROOT->LoadMacro("/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/src/compDataMCScaled.c+");MultiCompDataMCScaled("compDataMCScaled.in","Histo=4PiGamISRTagged isLogScale=1 hasData=0","compDataMCScaled");' | root -l -b
fi