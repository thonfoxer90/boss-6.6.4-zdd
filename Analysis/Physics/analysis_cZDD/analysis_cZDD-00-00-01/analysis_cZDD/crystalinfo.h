#ifndef Crystalinfo_H
#define Crystalinfo_H 

class Crystaldata {

public:
  Crystaldata();
  Crystaldata(int partID, int crystalNo, int chargeChannel, int timeChannel, double energy, double time);
  ~Crystaldata();

  void SetPartID(int partID)               {m_partID = partID;};
  void SetCrystalNo(int crystalNo)         {m_crystalNo = crystalNo;};
  void SetChargeChannel(int chargeChannel) {m_chargeChannel = chargeChannel;};
  void SetTimeChannel(int timeChannel)     {m_timeChannel = timeChannel;};
  void SetEnergy(double energy)            {m_energy = energy;};
  void SetTime(double time)                {m_time = time;};

  int GetPartID()                   {return m_partID;};
  int GetCrystalNo()                {return m_crystalNo;};
  int GetChargeChannel()            {return m_chargeChannel;};
  int GetTimeChannel()              {return m_timeChannel;};
  double GetEnergy()                {return m_energy;};
  double GetTime()                  {return m_time;};

private:
  int m_partID;
  int m_crystalNo;
  int m_chargeChannel;
  int m_timeChannel;
  double m_energy;
  double m_time;
};

#endif 

