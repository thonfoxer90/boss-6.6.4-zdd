#ifndef Physics_Analysis_analysis_cZDD_H
#define Physics_Analysis_analysis_cZDD_H 

#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/NTuple.h"
#include "crystalinfo.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TLorentzVector.h"
//#include "VertexFit/ReadBeamParFromDb.h"
//#include "AIDA/IHistogram1D.h"
//#include "AIDA/IHistogram2D.h"

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/PropertyMgr.h"
#include "VertexFit/IVertexDbSvc.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"

#include "EventModel/EventModel.h"
#include "EventModel/Event.h"

#include "EvtRecEvent/EvtRecEvent.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "EvtRecEvent/EvtRecVeeVertex.h"
#include "DstEvent/TofHitStatus.h"
#include "EventModel/EventHeader.h"



#include "TMath.h"
#include "GaudiKernel/INTupleSvc.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IHistogramSvc.h"

//#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/LorentzVector.h"
//#include "CLHEP/Vector/TwoVector.h"
//using CLHEP::Hep3Vector;
//using CLHEP::Hep2Vector;
using CLHEP::HepLorentzVector;
#include "CLHEP/Geometry/Point3D.h"
#ifndef ENABLE_BACKWARDS_COMPATIBILITY
   typedef HepGeom::Point3D<double> HepPoint3D;
#endif

#include "RawEvent/RawDataUtil.h"
#include "RawEvent/DigiEvent.h"
#include "ZddRawEvent/ZddDigi.h"
#include "Identifier/ZddID.h"
#include "McTruth/McParticle.h"
#include "McTruth/ZddMcHit.h"
//#include "McTruth/McEvent.h"
//#include "McTruth/McPrimaryParticle.h"

//#include "VertexFit/KinematicFit.h"
#include "VertexFit/KalmanKinematicFit.h"
#include "VertexFit/VertexFit.h"
#include "VertexFit/Helix.h"
#include "ParticleID/ParticleID.h"

#include <vector>

#include "analysis_cZDD/RootParticleContainer.h"

#include <iostream>
#include <fstream>

class analysis_cZDD : public Algorithm {

public:
  analysis_cZDD(const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();		
	void AnalysisParticleGun();
	void AnalysisBhabha();
	void Analysis4PiGamISRTagged();
	void DefineZDDCrystalsEdges();
	//double GetCrystalXCenter(int crystalnb, int partID);
	//double GetCrystalYCenter(int crystalnb, int partID);
	std::vector<double> GetCrystalCenter(int crystalnb, int partID);
	//double GetCrystalLowerX(int crystalnb, int partID); 
	//double GetCrystalUpperX(int crystalnb, int partID);
	//double GetCrystalLowerY(int crystalnb, int partID);
	//double GetCrystalUpperY(int crystalnb, int partID);
	double beam_energy(int runNo);
	bool IsFromInteractionPoint (EvtRecTrack* pCur);
	double IPdist (EvtRecTrack* pCur);
	vector<double> GetZddPhotonRecHit(std::map<std::vector<int>,double> mapcrystalenergyZDD, string mode);
	double CalculateZddSumOfCrystalEnergy(std::map<std::vector<int>,double> mapcrystalenergyZDD);
	TLorentzVector HepLorentzVectortoTLorentzVector(HepLorentzVector& heplv);
	HepLorentzVector TLorentzVectortoHepLorentzVector(TLorentzVector& lv);
	
private:
	//ZDD crystals coordinates
	//std::vector<double> vecZDDRightPosX;
	//std::vector<double> vecZDDRightPosY;
	
	
	Double_t m_XEdgesZDDRight[5];
	Double_t m_YEdgesZDDRight[8];
	
	std::map<std::vector<int>,double> m_mapcrystalenergyZDD;
	
	int m_KKMC;
	int m_BBBREM;
	
	double m_dMasselec;	
	double m_dMassPiC;
	double m_dCMSEnergy;
	double m_dCrossingAngle;
	
	TLorentzVector		m_lvBeamElectron;
	TLorentzVector 		m_lvBeamPositron;
  TLorentzVector    m_lvBoost; //Sum of beam e+ and beam e- 4 vectors (approximate)
	
	TVector3 m_vectboost;
	
	double              m_dCylRad;
  double              m_dCylHeight;
  IVertexDbSvc*	      m_vtxSvc;			
	HepPoint3D          m_hp3IP;
	
  //ReadBeamParFromDb m_reader;
  
	double m_dEoPcut;
	
	// Declare r0, z0 cut for charged tracks
  double m_vr0cut;
  double m_vz0cut;

  //Declare energy, dphi, dthe cuts for fake gamma's
  double m_energyThreshold;
  double m_gammaPhiCut;
  double m_gammaThetaCut;
  double m_gammaAngleCut;

  double isr_energy;
  double isr_x;
  double isr_y;
  double isr_z;
  double isr_px;
  double isr_py;
  double isr_pz;
  double hit_x;
  double hit_y;
  double dist_center;
 
  std::string m_outFileName;
	std::string m_ImpactRecoFileName;
	std::string m_WeightFileName;
 	std::ifstream m_ReadWeightFile;
	int m_UseMCTruthEDep;
 
  int evt;
  int run;
	int trackidx;
  int partProp;
	int mother;
  bool primPart;
  bool leafPart;
  double iniPosX;
  double iniPosY;
  double iniPosZ;
  double iniPosT;
  double finPosX;
  double finPosY;
  double finPosZ;
  double finPosT;
  double iniMomX;
  double iniMomY;
  double iniMomZ;
	double iniMomRho;
  double iniMomTheta;
  double iniMomPhi;
  double iniE;
  
	//Zdd informations
	int zdd_partID;
  double zdd_energy;
  double zdd_time;
  double chargeChannel;
	double zddRight_energy;
	double zddLeft_energy;

	//MC Truth informations
	//const int m_MaxTrack=50;
	int 	 MCntracks;
	int 	 MCTrackIndex[50];
	int 	 MCpartProp[50];
	int 	 MCMother[50];
	bool MCprimPart[50];
	double MCiniPosX[50];
  double MCiniPosY[50];
  double MCiniPosZ[50];
	double MCfinPosX[50];
  double MCfinPosY[50];
  double MCfinPosZ[50];
	double MCXCalcatZDD[50];
  double MCYCalcatZDD[50];
	double MCiniMomX[50];
  double MCiniMomY[50];
  double MCiniMomZ[50];
	double MCiniMomRho[50];
  double MCiniMomTheta[50];
  double MCiniMomPhi[50];
  double MCiniE[50];
	
	double MCiniMomXCM[50];
  double MCiniMomYCM[50];
  double MCiniMomZCM[50];
	double MCiniMomRhoCM[50];
  double MCiniMomThetaCM[50];
  double MCiniMomPhiCM[50];
  double MCiniECM[50];
	
	double MCzdd_energy;
	double MCzddRight_energy;
	double MCzddLeft_energy;
	
		//From BBBREM
	double weight;
	
		//From impact_reco
	bool	m_IsImpactReco;
	double xFirstZDDHit;
	double yFirstZDDHit;
	double xFirstElecZDDHit;
	double yFirstElecZDDHit;
	double xFirstPositronZDDHit;
	double yFirstPositronZDDHit;
	double PxImpact;
	double PyImpact;
	double PzImpact;
		
	//Particle gun information
	//particlegun
	bool m_pGunMCfound;
	int MCpGunTrackID;
	int MCpGunpartProp;
	double MCpGuniniPosX;
	double MCpGuniniPosY;
	double MCpGuniniPosZ;
	double MCpGunfinPosX;
	double MCpGunfinPosY;
	double MCpGunfinPosZ;
	double MCpGunMomX;
	double MCpGunMomY;
	double MCpGunMomZ;
	double MCpGunMomRho;
	double MCpGunMomTheta;
	double MCpGunMomPhi;
	double MCpGunE;
	double MCpGunXCalcatZDD;
	double MCpGunYCalcatZDD;
	TH1D* m_hPGunMCiniMomX;
	TH1D* m_hPGunMCiniMomY;
	TH1D* m_hPGunMCiniMomZ;
	TH1D* m_hPGunMCiniE;
	TH1D* m_hPGunMCiniCosTheta;
	TH1D* m_hPGunMCiniTheta;
	TH1D* m_hPGunMCiniPhi;
	TH2D* m_hPGunMCExtrapolatedZDDHitXY;
	TH1D* m_hPGunMCfinPosX;
	TH1D* m_hPGunMCfinPosY;
	TH1D* m_hPGunMCfinPosZ;
	TH2D* m_hPGunMCfinPosXY;
	TH2D* m_hPGunMCfinPosZX;
	TH2D* m_hPGunMCfinPosZY;
	TH2D* m_hPGunZDDFirstHitXY;
	TH1D* m_hPGunZDDDeltaHitXExtrapolX;
	TH1D* m_hPGunZDDDeltaHitYExtrapolY;
	TH1D* m_hPGunZDDEreco;
	TH2D* m_hPGunZDDErecovsETruth;
	TH2D* m_hPGunClustersZDDRight;
	
	//Baba scattering informations
	int MCBhabhaElecTrackID;
	bool m_MCIsBhabha;
	bool m_MCHasPrimElectron;
	bool m_MCHasPrimPositron;
	int  m_MCNPrimElectron;
	int  m_MCNPrimPositron;
	int	 m_MCNPrimGammas;	
	double MCBhabhaEleciniPosX;
	double MCBhabhaEleciniPosY;
	double MCBhabhaEleciniPosZ;
	double MCBhabhaElecfinPosX;
	double MCBhabhaElecfinPosY;
	double MCBhabhaElecfinPosZ;
	double MCBhabhaElecMomRho;
	double MCBhabhaElecMomTheta;
	double MCBhabhaElecMomPhi;
	double MCBhabhaElecE;
	double MCBhabhaElecXCalcatZDD;
	double MCBhabhaElecYCalcatZDD;
	int MCBhabhaPositronTrackID;
	double MCBhabhaPositroniniPosX;
	double MCBhabhaPositroniniPosY;
	double MCBhabhaPositroniniPosZ;
	double MCBhabhaPositronfinPosX;
	double MCBhabhaPositronfinPosY;
	double MCBhabhaPositronfinPosZ;
	double MCBhabhaPositronMomRho;
	double MCBhabhaPositronMomTheta;
	double MCBhabhaPositronMomPhi;
	double MCBhabhaPositronE;
	double MCBhabhaPositronXCalcatZDD;
	double MCBhabhaPositronYCalcatZDD;
	int 	 MCBhabhanGammas;
	int MCBhabhaGamTrackID[50];
	double MCBhabhaGaminiPosX[50];
	double MCBhabhaGaminiPosY[50];
	double MCBhabhaGaminiPosZ[50];
	double MCBhabhaGamfinPosX[50];
	double MCBhabhaGamfinPosY[50];
	double MCBhabhaGamfinPosZ[50];
	double MCBhabhaGamMomRho[50];
	double MCBhabhaGamMomTheta[50];
	double MCBhabhaGamMomPhi[50];
	double MCBhabhaGamE[50];
	double MCBhabhaGamXCalcatZDD[50];
	double MCBhabhaGamYCalcatZDD[50];
	double MCBhabhaMomSum3VElectronPositron;
	double MCBhabhaMissMomX;
	double MCBhabhaMissMomY;
	double MCBhabhaMissMomZ;
	
	
	TH1D* m_hMCBhabhaEleciniE;
	TH1D* m_hMCBhabhaEleciniCosTheta;
	TH1D* m_hMCBhabhaEleciniMomRho;
	TH1D* m_hMCBhabhaEleciniMomTheta;
	TH1D* m_hMCBhabhaEleciniMomPhi;
	TH1D* m_hMCBhabhaElecfinPosX;
	TH1D* m_hMCBhabhaElecfinPosY;
	TH1D* m_hMCBhabhaElecfinPosZ;
	TH2D* m_hMCBhabhaElecExtrapolatedZDDHitXY;
	TH2D* m_hMCBhabhaElecfinPosXY;
	TH2D* m_hMCBhabhaElecfinPosZX;
	TH2D* m_hMCBhabhaElecfinPosZY;
	
	TH1D* m_hMCBhabhaPositroniniE;
	TH1D* m_hMCBhabhaPositroniniCosTheta;
	TH1D* m_hMCBhabhaPositroniniMomRho;
	TH1D* m_hMCBhabhaPositroniniMomTheta;
	TH1D* m_hMCBhabhaPositroniniMomPhi;
	TH1D* m_hMCBhabhaPositronfinPosX;
	TH1D* m_hMCBhabhaPositronfinPosY;
	TH1D* m_hMCBhabhaPositronfinPosZ;
	TH2D* m_hMCBhabhaPositronExtrapolatedZDDHitXY;
	TH2D* m_hMCBhabhaPositronfinPosXY;
	TH2D* m_hMCBhabhaPositronfinPosZX;
	TH2D* m_hMCBhabhaPositronfinPosZY;
	
	TH2D* m_hZDDBhabhaFirstHitXY;
	TH1D* m_hZDDBhabhaDeltaHitXExtrapolX;
	TH1D* m_hZDDBhabhaDeltaHitYExtrapolY;
	TH1D* m_hZDDBhabhaEreco;
	TH2D* m_hZDDBhabhaErecovsETruth;
	TH2D* m_hZDDBhabhaBhabhaClustersZDDRight;
	TH1D* m_hZDDBhabhaErecoLeft;
	TH1D* m_hZDDBhabhaErecoRight;
	TH2D* m_hZDDBhabhaErecoLeftvsRight;
		
  // 
  int m_test4C;
  int m_test5C;

  // 
  int m_checkDedx;
  int m_checkTof;

	//Flags
	bool m_IsZDDhit;
	
	//4PiGammaISR
	RootParticleContainer m_T4PiGamISRPip1;
	RootParticleContainer m_T4PiGamISRPip2;
	RootParticleContainer m_T4PiGamISRPim1;
	RootParticleContainer m_T4PiGamISRPim2;
	RootParticleContainer m_T4PiGamISRMiss4PiCX;
	RootParticleContainer	m_T4PIGamISRGamISRZddTag;
	RootParticleContainer m_T4PiGamISRGamISRMCTruth;
	

	double m_XZddRecHit;
	double m_YZddRecHit;
	double m_XZddHitMiss4PiCX;
	double m_YZddHitMiss4PiCX;
	double m_XZddGammaISRMCTruth;
	double m_YZddGammaISRMCTruth;
	
	double m_MissM4PiCX;
		
	int HitColtrackID;
	int HitColPID;
  TTree* m_myTree;
  TTree* m_leoTree;
	TTree* m_AllMCTruthTree;
	TTree* m_particlegunTree;
	TTree* m_BhabhaTree;
	TTree* m_ZDDMcHitColTree;
	TTree* m_ImpactRecoTree;
	TTree* m_T4PiGamISR;
  TFile* m_myFile;
	TFile* m_ImpactRecoFile;

  double eDep[24];
	double eDepSide[24][2];
	
	double MCeDep[24];
	double MCeDepSide[24][2];
	
	
  // define Ntuples here
  /*
  NTuple::Tuple*  m_tuple1;      // charged track vertex
  NTuple::Item<double>  m_vx0;
  NTuple::Item<double>  m_vy0;
  NTuple::Item<double>  m_vz0;
  NTuple::Item<double>  m_vr0;
  NTuple::Item<double>  m_rvxy0;
  NTuple::Item<double>  m_rvz0;
  NTuple::Item<double>  m_rvphi0;

  NTuple::Tuple*  m_tuple2;      // fake photon
  NTuple::Item<double>  m_dthe;
  NTuple::Item<double>  m_dphi;
  NTuple::Item<double>  m_dang;
  NTuple::Item<double>  m_eraw;

  NTuple::Tuple*  m_tuple3;     // rhopi: raw mgg, etot
  NTuple::Item<double>  m_m2gg;
  NTuple::Item<double>  m_etot;

  NTuple::Tuple*  m_tuple4;     // rhopi 4C
  NTuple::Item<double>  m_chi1;
  NTuple::Item<double>  m_mpi0;

  NTuple::Tuple*  m_tuple5;     // rhopi 5C
  NTuple::Item<double>  m_chi2;
  NTuple::Item<double>  m_mrh0;
  NTuple::Item<double>  m_mrhp;
  NTuple::Item<double>  m_mrhm;

  NTuple::Tuple*  m_tuple6;    // photons
  NTuple::Item<double>  m_fcos;
  NTuple::Item<double>  m_elow;

  NTuple::Tuple* m_tuple7;    // dE/dx
  NTuple::Item<double> m_ptrk;
  NTuple::Item<double> m_chie;
  NTuple::Item<double> m_chimu;
  NTuple::Item<double> m_chipi;
  NTuple::Item<double> m_chik;
  NTuple::Item<double> m_chip;
  NTuple::Item<double> m_probPH;
  NTuple::Item<double> m_normPH;
  NTuple::Item<double> m_ghit;
  NTuple::Item<double> m_thit;

  NTuple::Tuple* m_tuple8;   // endcap tof
  NTuple::Item<double> m_ptot_etof;
  NTuple::Item<double> m_cntr_etof;
  NTuple::Item<double> m_te_etof;
  NTuple::Item<double> m_tmu_etof;
  NTuple::Item<double> m_tpi_etof;
  NTuple::Item<double> m_tk_etof;
  NTuple::Item<double> m_tp_etof;
  NTuple::Item<double> m_ph_etof;
  NTuple::Item<double> m_rhit_etof;
  NTuple::Item<double> m_qual_etof;

  NTuple::Tuple* m_tuple9;  // barrel inner tof
  NTuple::Item<double> m_ptot_btof1;
  NTuple::Item<double> m_cntr_btof1;
  NTuple::Item<double> m_te_btof1;
  NTuple::Item<double> m_tmu_btof1;
  NTuple::Item<double> m_tpi_btof1;
  NTuple::Item<double> m_tk_btof1;
  NTuple::Item<double> m_tp_btof1;
  NTuple::Item<double> m_ph_btof1;
  NTuple::Item<double> m_zhit_btof1;
  NTuple::Item<double> m_qual_btof1;

  NTuple::Tuple* m_tuple10;  // barrel outer tof
  NTuple::Item<double> m_ptot_btof2;
  NTuple::Item<double> m_cntr_btof2;
  NTuple::Item<double> m_te_btof2;
  NTuple::Item<double> m_tmu_btof2;
  NTuple::Item<double> m_tpi_btof2;
  NTuple::Item<double> m_tk_btof2;
  NTuple::Item<double> m_tp_btof2;
  NTuple::Item<double> m_ph_btof2;
  NTuple::Item<double> m_zhit_btof2;
  NTuple::Item<double> m_qual_btof2;
  
  NTuple::Tuple* m_tuple11;  // Particle ID info.
  NTuple::Item<double> m_ptrk_pid;
  NTuple::Item<double> m_cost_pid;
  NTuple::Item<double> m_dedx_pid;
  NTuple::Item<double> m_tof1_pid;
  NTuple::Item<double> m_tof2_pid;
  NTuple::Item<double> m_prob_pid;
*/
};

#endif 
