#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IJobOptionsSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/NTuple.h"

#include "EventModel/EventHeader.h"
#include "EventModel/Event.h"
#include "EvtRecEvent/EvtRecEvent.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "EvtRecEvent/EvtRecVeeVertex.h"

//#include "CLHEP/Matrix/Vector.h"
//#include "CLHEP/Vector/LorentzVector.h"
//#include "CLHEP/Vector/ThreeVector.h"

#include "TTree.h"
#include "TLorentzVector.h"
#include "TVector3.h"

class RootParticleContainer {
	
	public:
	RootParticleContainer();
  RootParticleContainer(const std::string& name,bool bIsPhoton=false);
	void AttachToNtuple(TTree* pTuple,const std::string& name,bool bIsPhoton);
	void Fill(const TLorentzVector* lv);
	TLorentzVector GetLV();
	void Clear();
	
	private:
	std::string            m_sName;
  bool                   m_bIsPhoton;
  double  m_dEnergy;
  double  m_dMomentum;
  double   m_dTheta;
  double   m_dPhi;
};