#-- start of make_header -----------------

#====================================
#  Document analysis_cZDD_check_install_runtime
#
#   Generated Thu May 24 15:39:08 2018  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_analysis_cZDD_check_install_runtime_has_target_tag = 1

#--------------------------------------------------------

ifdef cmt_analysis_cZDD_check_install_runtime_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_analysis_cZDD_check_install_runtime

analysis_cZDD_tag = $(tag)

#cmt_local_tagfile_analysis_cZDD_check_install_runtime = $(analysis_cZDD_tag)_analysis_cZDD_check_install_runtime.make
cmt_local_tagfile_analysis_cZDD_check_install_runtime = $(bin)$(analysis_cZDD_tag)_analysis_cZDD_check_install_runtime.make

else

tags      = $(tag),$(CMTEXTRATAGS)

analysis_cZDD_tag = $(tag)

#cmt_local_tagfile_analysis_cZDD_check_install_runtime = $(analysis_cZDD_tag).make
cmt_local_tagfile_analysis_cZDD_check_install_runtime = $(bin)$(analysis_cZDD_tag).make

endif

include $(cmt_local_tagfile_analysis_cZDD_check_install_runtime)
#-include $(cmt_local_tagfile_analysis_cZDD_check_install_runtime)

ifdef cmt_analysis_cZDD_check_install_runtime_has_target_tag

cmt_final_setup_analysis_cZDD_check_install_runtime = $(bin)setup_analysis_cZDD_check_install_runtime.make
#cmt_final_setup_analysis_cZDD_check_install_runtime = $(bin)analysis_cZDD_analysis_cZDD_check_install_runtimesetup.make
cmt_local_analysis_cZDD_check_install_runtime_makefile = $(bin)analysis_cZDD_check_install_runtime.make

else

cmt_final_setup_analysis_cZDD_check_install_runtime = $(bin)setup.make
#cmt_final_setup_analysis_cZDD_check_install_runtime = $(bin)analysis_cZDDsetup.make
cmt_local_analysis_cZDD_check_install_runtime_makefile = $(bin)analysis_cZDD_check_install_runtime.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)analysis_cZDDsetup.make

#analysis_cZDD_check_install_runtime :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'analysis_cZDD_check_install_runtime'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = analysis_cZDD_check_install_runtime/
#analysis_cZDD_check_install_runtime::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of cmt_action_runner_header ---------------

ifdef ONCE
analysis_cZDD_check_install_runtime_once = 1
endif

ifdef analysis_cZDD_check_install_runtime_once

analysis_cZDD_check_install_runtimeactionstamp = $(bin)analysis_cZDD_check_install_runtime.actionstamp
#analysis_cZDD_check_install_runtimeactionstamp = analysis_cZDD_check_install_runtime.actionstamp

analysis_cZDD_check_install_runtime :: $(analysis_cZDD_check_install_runtimeactionstamp)
	$(echo) "analysis_cZDD_check_install_runtime ok"
#	@echo analysis_cZDD_check_install_runtime ok

$(analysis_cZDD_check_install_runtimeactionstamp) :: $(analysis_cZDD_check_install_runtime_dependencies)
	$(silent) /cluster/him/bes3/dist/6.6.4/BesPolicy/BesPolicy-01-05-03/cmt/bes_check_installations.sh -files= -s=../share *.txt   -installdir=/home/bgarillo/boss-6.6.4-ZDD/InstallArea/share
	$(silent) cat /dev/null > $(analysis_cZDD_check_install_runtimeactionstamp)
#	@echo ok > $(analysis_cZDD_check_install_runtimeactionstamp)

analysis_cZDD_check_install_runtimeclean ::
	$(cleanup_silent) /bin/rm -f $(analysis_cZDD_check_install_runtimeactionstamp)

else

analysis_cZDD_check_install_runtime :: $(analysis_cZDD_check_install_runtime_dependencies)
	$(silent) /cluster/him/bes3/dist/6.6.4/BesPolicy/BesPolicy-01-05-03/cmt/bes_check_installations.sh -files= -s=../share *.txt   -installdir=/home/bgarillo/boss-6.6.4-ZDD/InstallArea/share

endif

install ::
uninstall ::

#-- end of cmt_action_runner_header -----------------
#-- start of cleanup_header --------------

clean :: analysis_cZDD_check_install_runtimeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(analysis_cZDD_check_install_runtime.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(analysis_cZDD_check_install_runtime.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(analysis_cZDD_check_install_runtime.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(analysis_cZDD_check_install_runtime.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_analysis_cZDD_check_install_runtime)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(analysis_cZDD_check_install_runtime.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(analysis_cZDD_check_install_runtime.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(analysis_cZDD_check_install_runtime.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

analysis_cZDD_check_install_runtimeclean ::
#-- end of cleanup_header ---------------
