#-- start of make_header -----------------

#====================================
#  Library analysis_cZDD
#
#   Generated Thu May 24 15:39:07 2018  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_analysis_cZDD_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_analysis_cZDD_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_analysis_cZDD

analysis_cZDD_tag = $(tag)

#cmt_local_tagfile_analysis_cZDD = $(analysis_cZDD_tag)_analysis_cZDD.make
cmt_local_tagfile_analysis_cZDD = $(bin)$(analysis_cZDD_tag)_analysis_cZDD.make

else

tags      = $(tag),$(CMTEXTRATAGS)

analysis_cZDD_tag = $(tag)

#cmt_local_tagfile_analysis_cZDD = $(analysis_cZDD_tag).make
cmt_local_tagfile_analysis_cZDD = $(bin)$(analysis_cZDD_tag).make

endif

include $(cmt_local_tagfile_analysis_cZDD)
#-include $(cmt_local_tagfile_analysis_cZDD)

ifdef cmt_analysis_cZDD_has_target_tag

cmt_final_setup_analysis_cZDD = $(bin)setup_analysis_cZDD.make
#cmt_final_setup_analysis_cZDD = $(bin)analysis_cZDD_analysis_cZDDsetup.make
cmt_local_analysis_cZDD_makefile = $(bin)analysis_cZDD.make

else

cmt_final_setup_analysis_cZDD = $(bin)setup.make
#cmt_final_setup_analysis_cZDD = $(bin)analysis_cZDDsetup.make
cmt_local_analysis_cZDD_makefile = $(bin)analysis_cZDD.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)analysis_cZDDsetup.make

#analysis_cZDD :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'analysis_cZDD'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = analysis_cZDD/
#analysis_cZDD::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

analysis_cZDDlibname   = $(bin)$(library_prefix)analysis_cZDD$(library_suffix)
analysis_cZDDlib       = $(analysis_cZDDlibname).a
analysis_cZDDstamp     = $(bin)analysis_cZDD.stamp
analysis_cZDDshstamp   = $(bin)analysis_cZDD.shstamp

analysis_cZDD :: dirs  analysis_cZDDLIB
	$(echo) "analysis_cZDD ok"

#-- end of libary_header ----------------

analysis_cZDDLIB :: $(analysis_cZDDlib) $(analysis_cZDDshstamp)
	@/bin/echo "------> analysis_cZDD : library ok"

$(analysis_cZDDlib) :: $(bin)crystalinfo.o $(bin)Pi0recHelper.o $(bin)analysis_cZDD.o $(bin)RootParticleContainer.o $(bin)analysis_cZDD_load.o $(bin)analysis_cZDD_entries.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(analysis_cZDDlib) $?
	$(lib_silent) $(ranlib) $(analysis_cZDDlib)
	$(lib_silent) cat /dev/null >$(analysis_cZDDstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(analysis_cZDDlibname).$(shlibsuffix) :: $(analysis_cZDDlib) $(analysis_cZDDstamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" analysis_cZDD $(analysis_cZDD_shlibflags)

$(analysis_cZDDshstamp) :: $(analysis_cZDDlibname).$(shlibsuffix)
	@if test -f $(analysis_cZDDlibname).$(shlibsuffix) ; then cat /dev/null >$(analysis_cZDDshstamp) ; fi

analysis_cZDDclean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)crystalinfo.o $(bin)Pi0recHelper.o $(bin)analysis_cZDD.o $(bin)RootParticleContainer.o $(bin)analysis_cZDD_load.o $(bin)analysis_cZDD_entries.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
analysis_cZDDinstallname = $(library_prefix)analysis_cZDD$(library_suffix).$(shlibsuffix)

analysis_cZDD :: analysis_cZDDinstall

install :: analysis_cZDDinstall

analysis_cZDDinstall :: $(install_dir)/$(analysis_cZDDinstallname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(analysis_cZDDinstallname) :: $(bin)$(analysis_cZDDinstallname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(analysis_cZDDinstallname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(analysis_cZDDinstallname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(analysis_cZDDinstallname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(analysis_cZDDinstallname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(analysis_cZDDinstallname) $(install_dir)/$(analysis_cZDDinstallname); \
	      echo `pwd`/$(analysis_cZDDinstallname) >$(install_dir)/$(analysis_cZDDinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(analysis_cZDDinstallname), no installation directory specified"; \
	  fi; \
	fi

analysis_cZDDclean :: analysis_cZDDuninstall

uninstall :: analysis_cZDDuninstall

analysis_cZDDuninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(analysis_cZDDinstallname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(analysis_cZDDinstallname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(analysis_cZDDinstallname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(analysis_cZDDinstallname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),analysis_cZDDclean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)analysis_cZDD_dependencies.make :: dirs

ifndef QUICK
$(bin)analysis_cZDD_dependencies.make : $(src)crystalinfo.cxx $(src)Pi0recHelper.cxx $(src)analysis_cZDD.cxx $(src)RootParticleContainer.cxx $(src)components/analysis_cZDD_load.cxx $(src)components/analysis_cZDD_entries.cxx $(use_requirements) $(cmt_final_setup_analysis_cZDD)
	$(echo) "(analysis_cZDD.make) Rebuilding $@"; \
	  $(build_dependencies) analysis_cZDD -all_sources -out=$@ $(src)crystalinfo.cxx $(src)Pi0recHelper.cxx $(src)analysis_cZDD.cxx $(src)RootParticleContainer.cxx $(src)components/analysis_cZDD_load.cxx $(src)components/analysis_cZDD_entries.cxx
endif

#$(analysis_cZDD_dependencies)

-include $(bin)analysis_cZDD_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),analysis_cZDDclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)crystalinfo.d

$(bin)$(binobj)crystalinfo.d : $(use_requirements) $(cmt_final_setup_analysis_cZDD)

$(bin)$(binobj)crystalinfo.d : $(src)crystalinfo.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/crystalinfo.dep $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(crystalinfo_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(crystalinfo_cppflags) $(crystalinfo_cxx_cppflags)  $(src)crystalinfo.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/crystalinfo.o $(src)crystalinfo.cxx $(@D)/crystalinfo.dep
endif
endif

$(bin)$(binobj)crystalinfo.o : $(src)crystalinfo.cxx
else
$(bin)analysis_cZDD_dependencies.make : $(crystalinfo_cxx_dependencies)

$(bin)$(binobj)crystalinfo.o : $(crystalinfo_cxx_dependencies)
endif
	$(cpp_echo) $(src)crystalinfo.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(crystalinfo_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(crystalinfo_cppflags) $(crystalinfo_cxx_cppflags)  $(src)crystalinfo.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),analysis_cZDDclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Pi0recHelper.d

$(bin)$(binobj)Pi0recHelper.d : $(use_requirements) $(cmt_final_setup_analysis_cZDD)

$(bin)$(binobj)Pi0recHelper.d : $(src)Pi0recHelper.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Pi0recHelper.dep $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(Pi0recHelper_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(Pi0recHelper_cppflags) $(Pi0recHelper_cxx_cppflags)  $(src)Pi0recHelper.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Pi0recHelper.o $(src)Pi0recHelper.cxx $(@D)/Pi0recHelper.dep
endif
endif

$(bin)$(binobj)Pi0recHelper.o : $(src)Pi0recHelper.cxx
else
$(bin)analysis_cZDD_dependencies.make : $(Pi0recHelper_cxx_dependencies)

$(bin)$(binobj)Pi0recHelper.o : $(Pi0recHelper_cxx_dependencies)
endif
	$(cpp_echo) $(src)Pi0recHelper.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(Pi0recHelper_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(Pi0recHelper_cppflags) $(Pi0recHelper_cxx_cppflags)  $(src)Pi0recHelper.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),analysis_cZDDclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)analysis_cZDD.d

$(bin)$(binobj)analysis_cZDD.d : $(use_requirements) $(cmt_final_setup_analysis_cZDD)

$(bin)$(binobj)analysis_cZDD.d : $(src)analysis_cZDD.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/analysis_cZDD.dep $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(analysis_cZDD_cppflags) $(analysis_cZDD_cxx_cppflags)  $(src)analysis_cZDD.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/analysis_cZDD.o $(src)analysis_cZDD.cxx $(@D)/analysis_cZDD.dep
endif
endif

$(bin)$(binobj)analysis_cZDD.o : $(src)analysis_cZDD.cxx
else
$(bin)analysis_cZDD_dependencies.make : $(analysis_cZDD_cxx_dependencies)

$(bin)$(binobj)analysis_cZDD.o : $(analysis_cZDD_cxx_dependencies)
endif
	$(cpp_echo) $(src)analysis_cZDD.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(analysis_cZDD_cppflags) $(analysis_cZDD_cxx_cppflags)  $(src)analysis_cZDD.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),analysis_cZDDclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)RootParticleContainer.d

$(bin)$(binobj)RootParticleContainer.d : $(use_requirements) $(cmt_final_setup_analysis_cZDD)

$(bin)$(binobj)RootParticleContainer.d : $(src)RootParticleContainer.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/RootParticleContainer.dep $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(RootParticleContainer_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(RootParticleContainer_cppflags) $(RootParticleContainer_cxx_cppflags)  $(src)RootParticleContainer.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/RootParticleContainer.o $(src)RootParticleContainer.cxx $(@D)/RootParticleContainer.dep
endif
endif

$(bin)$(binobj)RootParticleContainer.o : $(src)RootParticleContainer.cxx
else
$(bin)analysis_cZDD_dependencies.make : $(RootParticleContainer_cxx_dependencies)

$(bin)$(binobj)RootParticleContainer.o : $(RootParticleContainer_cxx_dependencies)
endif
	$(cpp_echo) $(src)RootParticleContainer.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(RootParticleContainer_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(RootParticleContainer_cppflags) $(RootParticleContainer_cxx_cppflags)  $(src)RootParticleContainer.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),analysis_cZDDclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)analysis_cZDD_load.d

$(bin)$(binobj)analysis_cZDD_load.d : $(use_requirements) $(cmt_final_setup_analysis_cZDD)

$(bin)$(binobj)analysis_cZDD_load.d : $(src)components/analysis_cZDD_load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/analysis_cZDD_load.dep $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(analysis_cZDD_load_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(analysis_cZDD_load_cppflags) $(analysis_cZDD_load_cxx_cppflags) -I../src/components $(src)components/analysis_cZDD_load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/analysis_cZDD_load.o $(src)components/analysis_cZDD_load.cxx $(@D)/analysis_cZDD_load.dep
endif
endif

$(bin)$(binobj)analysis_cZDD_load.o : $(src)components/analysis_cZDD_load.cxx
else
$(bin)analysis_cZDD_dependencies.make : $(analysis_cZDD_load_cxx_dependencies)

$(bin)$(binobj)analysis_cZDD_load.o : $(analysis_cZDD_load_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/analysis_cZDD_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(analysis_cZDD_load_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(analysis_cZDD_load_cppflags) $(analysis_cZDD_load_cxx_cppflags) -I../src/components $(src)components/analysis_cZDD_load.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),analysis_cZDDclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)analysis_cZDD_entries.d

$(bin)$(binobj)analysis_cZDD_entries.d : $(use_requirements) $(cmt_final_setup_analysis_cZDD)

$(bin)$(binobj)analysis_cZDD_entries.d : $(src)components/analysis_cZDD_entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/analysis_cZDD_entries.dep $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(analysis_cZDD_entries_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(analysis_cZDD_entries_cppflags) $(analysis_cZDD_entries_cxx_cppflags) -I../src/components $(src)components/analysis_cZDD_entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/analysis_cZDD_entries.o $(src)components/analysis_cZDD_entries.cxx $(@D)/analysis_cZDD_entries.dep
endif
endif

$(bin)$(binobj)analysis_cZDD_entries.o : $(src)components/analysis_cZDD_entries.cxx
else
$(bin)analysis_cZDD_dependencies.make : $(analysis_cZDD_entries_cxx_dependencies)

$(bin)$(binobj)analysis_cZDD_entries.o : $(analysis_cZDD_entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/analysis_cZDD_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(analysis_cZDD_pp_cppflags) $(lib_analysis_cZDD_pp_cppflags) $(analysis_cZDD_entries_pp_cppflags) $(use_cppflags) $(analysis_cZDD_cppflags) $(lib_analysis_cZDD_cppflags) $(analysis_cZDD_entries_cppflags) $(analysis_cZDD_entries_cxx_cppflags) -I../src/components $(src)components/analysis_cZDD_entries.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: analysis_cZDDclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(analysis_cZDD.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(analysis_cZDD.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(analysis_cZDD.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(analysis_cZDD.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_analysis_cZDD)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(analysis_cZDD.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(analysis_cZDD.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(analysis_cZDD.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

analysis_cZDDclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library analysis_cZDD
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)analysis_cZDD$(library_suffix).a $(library_prefix)analysis_cZDD$(library_suffix).s? analysis_cZDD.stamp analysis_cZDD.shstamp
#-- end of cleanup_library ---------------
