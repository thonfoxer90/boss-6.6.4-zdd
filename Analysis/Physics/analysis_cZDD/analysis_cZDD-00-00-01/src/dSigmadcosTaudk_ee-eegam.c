#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TPave.h>
#include <stdio.h>
#include <iostream>
#include <TStyle.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TMath.h>
#include "TF2.h"
#include "Math/WrappedMultiTF1.h"
#include "Math/AdaptiveIntegratorMultiDim.h"

double const alphaEM=1/137.; //Fine structure constant
double const elecClassRadius=2.8179403267e-15; //Classical electron radius (m)
double const massElec=511e-6; //Electron energy (GeV)
double const meter2ToBarn=1e28;
double const electronvoltToJoule=1.602e-19;

double ECMS;
double BetaElecCMS;

//=============================================================================
// Calculates differential cross section (in mbarn) for e+e-->e+e-gamma process,
// using formula (30) from "Single Photon Emission in High Energy e+e- collision"
// (G.Altarelli and F.Buccella, Il Nuovo Cimento, Vol.XXXIV, N.5, 1964)
// Author : B.Garillon, University of Mainz
//=============================================================================

Double_t rho2(double k)
{
double rho2;
rho2=ECMS*ECMS-2*ECMS*k;

return rho2;
}

Double_t func(double x, double y)
{
double f;

f=-TMath::Sin(x)*4*alphaEM*elecClassRadius*elecClassRadius/y
	*(
	(pow(1/(1-BetaElecCMS*TMath::Cos(x)),2))
		*(
		pow((ECMS*ECMS+rho2(y))/(ECMS*ECMS),2)
		-2*(pow(ECMS,4)+pow(rho2(y),2))/pow(ECMS,4)*TMath::Log((rho2(y)/(2*massElec*y))*(ECMS/massElec))
		+ 
			((TMath::Log((rho2(y)/(2*massElec*y))*(ECMS/massElec)))-2)
			*(16*massElec*massElec*rho2(y)/(pow(ECMS,4)*(1/(1-BetaElecCMS*TMath::Cos(x))))
				-(32*pow(massElec,4)*rho2(y)/(pow(ECMS,6)*(pow(1/(1-BetaElecCMS*TMath::Cos(x)),2))))
				)
		)

	)
	;

return f;
}
int dSigmadcosTaudk_eeToeegam(double E,double TauMin, double TauMax,double EGamMin,double EGamMax)
{
ECMS=E;
double EelecCMS=ECMS/2;
double GammaElecCMS=EelecCMS/massElec;
BetaElecCMS=sqrt(1-(1/(pow(GammaElecCMS,2))));
printf("GammaElecCMS= %.8e Beta = %.8e \n", GammaElecCMS,BetaElecCMS);

// Create the function and wrap it
   TF2 f("Sin Function", "func(x,y)", TauMin, TauMax, EGamMin, EGamMax);
   ROOT::Math::WrappedMultiTF1 wf1(f);
// Create the Integrator
   ROOT::Math::AdaptiveIntegratorMultiDim ig;
   // Set parameters of the integration
   ig.SetFunction(wf1);
   ig.SetRelTolerance(0.001);
   double xmin[] = {TauMin,TauMax};
   double xmax[] = {EGamMin, EGamMin};
   cout << ig.Integral(xmin, xmax) << endl;

double XSec=ig.Integral(xmin, xmax);
XSec=XSec*meter2ToBarn*1e3;
printf("sigma= %f mb \n", XSec);	 	 
/*
// Create the function and wrap it
   TF2 f("Sin Function", "sin(x) + y", 0, TMath::Pi(), 0, 1);
   ROOT::Math::WrappedMultiTF1 wf1(f);
   // Create the Integrator
   ROOT::Math::AdaptiveIntegratorMultiDim ig;
   // Set parameters of the integration
   ig.SetFunction(wf1);
   ig.SetRelTolerance(0.001);
   double xmin[] = {0,0};
   double xmax[] = {TMath::PiOver2(), 3};
   cout << ig.Integral(xmin, xmax) << endl;
*/

return 0;

}