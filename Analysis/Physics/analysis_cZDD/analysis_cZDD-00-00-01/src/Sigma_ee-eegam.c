#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TPave.h>
#include <stdio.h>
#include <iostream>
#include <TStyle.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TMath.h>


double const alphaEM=1/137.; //Fine structure constant
double const elecClassRadius=2.8179403267e-15; //Classical electron radius (m)
double const massElec=511e-6; //Electron energy (GeV)
double const meter2ToBarn=1e28;
double const electronvoltToJoule=1.602e-19;

//=============================================================================
// Calculates integrated cross section (in mbarn) for e+e-->e+e-gamma process,
// using formula (32) from "Single Photon Emission in High Energy e+e- collision"
// (G.Altarelli and F.Buccella, Il Nuovo Cimento, Vol.XXXIV, N.5, 1964)
// Author : B.Garillon, University of Mainz
//=============================================================================

double Sigma_eeToeegam(double ECM, double eps)
{
double XSec;
XSec=4*alphaEM*elecClassRadius*elecClassRadius
		 	*(
			((8./3.)*TMath::Log(ECM/(2*eps))-(5./3.))*(TMath::Log(ECM*ECM/(massElec*massElec)))
			+(4./3.)*pow((TMath::Log(ECM/(2*eps))),2)
			-1.
			-4*pow(TMath::Pi(),2)/9
			)
		;
XSec=XSec*meter2ToBarn*1e3;

printf("sigma(eps=%f GeV) = %f mb \n", eps, XSec);
return XSec;
}