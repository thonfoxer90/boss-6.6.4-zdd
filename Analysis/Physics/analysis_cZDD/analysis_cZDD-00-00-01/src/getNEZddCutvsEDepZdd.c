#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TMath.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

using namespace std;

void getNEZddCutvsEDepZdd(string inputfile, string option, string outputname){

	TFile* file = TFile::Open(inputfile.c_str());
	file->cd("4PiGamISRTagged/Stage0");
	TH1D* hEZDD=(TH1D*)gROOT->FindObject("EZDD");
	hEZDD->Draw();
	gPad->SetLogy();
	
	map<double, double> NEZddCut;
	map<double, double> errNEZddCut;
	map<double, double> NEZddCutOverNTotal;
	map<double, double> errNEZddCutOverNTotal;
	
	NEZddCut[0]=0; NEZddCut[10]=0; NEZddCut[50]=0;
	NEZddCut[100]=0; NEZddCut[200]=0; NEZddCut[400]=0;
	NEZddCut[600]=0; NEZddCut[800]=0; NEZddCut[1000]=0;
	NEZddCut[1200]=0;
	for(std::map<double,double>::iterator it=NEZddCut.begin(); it!=NEZddCut.end();++it){
		double key=it->first;
		Int_t binMin=(hEZDD->GetXaxis()->FindBin(key));
		Int_t binMax=hEZDD->GetXaxis()->GetNbins();
		NEZddCut[key]=hEZDD->Integral(binMin,binMax
		);
		errNEZddCut[key]=sqrt(NEZddCut[key]);
		NEZddCutOverNTotal[key]=NEZddCut[key]/hEZDD->Integral();
		//errNEZddCutOverNTotal[key]=(1/pow((double)hEZDD->Integral(),2))*NEZddCutOverNTotal[key]
		//+pow(NEZddCutOverNTotal[key],2)/((double)hEZDD->Integral());
		errNEZddCutOverNTotal[key]=(1/pow((double)hEZDD->Integral(),2))*NEZddCutOverNTotal[key];
		cout << NEZddCutOverNTotal[key] << " " <<errNEZddCutOverNTotal[key] << endl;
	}
	
	//Write log file
	string outTable = outputname+".txt";
	ofstream writeTable(outTable.c_str());
	writeTable << "\t N^{Stage 1} (N^{Stage 1}/N^{Stage 0})" << endl;
	for(std::map<double,double>::iterator it=NEZddCut.begin(); it!=NEZddCut.end();++it){
		double key=it->first;
		writeTable << "E^{ZDD}>=" << key <<" MeV"<< "\t"
		<< NEZddCut[key] 
		<< "("<<NEZddCutOverNTotal[key]*100
		//<< "+/-"<< errNEZddCutOverNTotal[key]*100 
		<<"\%)"
		<< endl;
	}
	
	vector<double> vecX;
	vector<double> vecY;
	vector<double> vecErrX;
	vector<double> vecErrY;
	//Make graph
	for(std::map<double,double>::iterator it=NEZddCut.begin(); it!=NEZddCut.end();++it){
		double key = it->first;
		vecX.push_back(key);
		vecErrX.push_back(0);
		vecY.push_back(NEZddCutOverNTotal[key]*100);
		vecErrY.push_back(errNEZddCutOverNTotal[key]*100);
	}
	TGraphErrors* graph = new TGraphErrors(vecX.size(),&vecX[0],&vecY[0],&vecErrX[0],&vecErrY[0]);
	graph->SetMarkerStyle(8);
	graph->Draw("ALP");
	graph->GetXaxis()->SetTitle("Min(E^{ZDD}_{Dep}) (MeV)");
	graph->GetYaxis()->SetTitle("Stage1/Stage0 (\%)");
	graph->SetTitle("");
	
	string outpdf = outputname+".pdf";
	gPad->SaveAs(outpdf.c_str());
}