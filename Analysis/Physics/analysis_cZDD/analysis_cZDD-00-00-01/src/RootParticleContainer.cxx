#include "analysis_cZDD/RootParticleContainer.h"

using namespace Event;

//----------------------------------------------------------------------------------------------------------
RootParticleContainer::RootParticleContainer()
//----------------------------------------------------------------------------------------------------------
{
  m_sName      = "FourVect";
  m_bIsPhoton  = false;
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
RootParticleContainer::RootParticleContainer(const std::string& name,bool bIsPhoton)
//----------------------------------------------------------------------------------------------------------
{
  m_sName      = name;
  m_bIsPhoton  = bIsPhoton;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
void RootParticleContainer::AttachToNtuple(TTree* pTuple,const std::string& name,bool bIsPhoton=false)
//----------------------------------------------------------------------------------------------------------
{
  m_sName      = name;
  m_bIsPhoton  = bIsPhoton;
  //if (!m_bIsPhoton) {pTuple->Branch((m_sName+"P").c_str(),m_dMomentum);}
	pTuple->Branch((m_sName+"P").c_str(),&m_dMomentum);
  pTuple->Branch((m_sName+"E").c_str()     ,&m_dEnergy);
  pTuple->Branch((m_sName+"Theta").c_str() ,&m_dTheta);
  pTuple->Branch((m_sName+"Phi").c_str()   ,&m_dPhi);
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
void RootParticleContainer::Fill(const TLorentzVector* lv)
//----------------------------------------------------------------------------------------------------------
{
  m_dEnergy   = lv->E();
  //if (!m_bIsPhoton) {m_dMomentum = lv->Rho();}
	if(lv->Rho()<0) cout << "RootParticleContainer::Fill NEGATIVE RHO " << endl;
	m_dMomentum = lv->Rho();
  m_dTheta    = lv->Theta();
  m_dPhi      = lv->Phi();
//printf("RootParticleContainer::Fill %f %f %f %f \n",m_dEnergy,m_dMomentum, m_dTheta, m_dPhi);
	if(m_dEnergy==-1) cout << "RootParticleContainer::Fill lv unchanged" << endl;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
TLorentzVector RootParticleContainer::GetLV()
//----------------------------------------------------------------------------------------------------------
{
	/*
	TLorentzVector lv(0.,0.,0.,0.);	
	lv.SetRho(m_dMomentum);
	lv.SetTheta(m_dTheta);
	lv.SetPhi(m_dPhi);
	lv.SetE(m_dEnergy);
	*/
	
	TLorentzVector lv(0.,0.,0.,0.);	
	TVector3 vec3(0.,0.,0.);
	//vec3.SetMag(m_dMomentum);
	//vec3.SetTheta(m_dTheta);
	//vec3.SetPhi(m_dPhi);
	vec3.SetMagThetaPhi(m_dMomentum,m_dTheta,m_dPhi);
	lv.SetPxPyPzE(vec3.Px(),vec3.Py(),vec3.Pz(),m_dEnergy);
  return lv;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
void RootParticleContainer::Clear()
//----------------------------------------------------------------------------------------------------------
{
  m_dEnergy   = -1;
  //if (!m_bIsPhoton) {m_dMomentum = -1;}
	m_dMomentum = -1;
  m_dTheta    = -4;
  m_dPhi      = -4;
}
//----------------------------------------------------------------------------------------------------------


