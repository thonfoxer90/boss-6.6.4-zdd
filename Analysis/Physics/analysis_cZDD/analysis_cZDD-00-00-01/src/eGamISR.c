#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TMath.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

using namespace std;

const int NMAX=256;
int MCntracks;
int 	 MCpartProp[NMAX];
bool	 MCprimPart[NMAX];
double MCXCalcatZDD[NMAX];
double MCYCalcatZDD[NMAX];
double MCiniMomTheta[NMAX];
double MCiniE[NMAX];


void eGamISR(string inputfile, string option, string outputname){
	TFile* file = TFile::Open(inputfile.c_str());
	TTree* treeMCTruth = (TTree*)file->Get("AllMCTruthTree");
	treeMCTruth->SetBranchAddress("MCntracks", &MCntracks);
	treeMCTruth->SetBranchAddress("MCpartProp", MCpartProp);
	treeMCTruth->SetBranchAddress("MCprimPart", MCprimPart);
	treeMCTruth->SetBranchAddress("MCiniE", MCiniE);
	treeMCTruth->SetBranchAddress("MCXCalcatZDD", MCXCalcatZDD);
	treeMCTruth->SetBranchAddress("MCYCalcatZDD", MCYCalcatZDD);
	
	TH1D* hEGam = new TH1D("hEGam","",256,0.,2.);
	TH1D* hSumEGam = new TH1D("hSumEGam","",256,0.,2.);
	TH1D* hNGam = new TH1D("hNGam","",10,0.,10.);
	
	for(int entry=0;entry<treeMCTruth->GetEntries();entry++){
		treeMCTruth->GetEntry(entry);
		double Esum=0;
		int NGam=0;
		for(int k=0;k<MCntracks;k++){
			if(MCpartProp[k]==22 && MCprimPart[k]==1){NGam++;Esum=MCiniE[k];hEGam->Fill(MCiniE[k]);}
			
		}
		hNGam->Fill(NGam);
		hSumEGam->Fill(Esum);
	}
	TCanvas* c0 = new TCanvas();
	c0->Divide(2,2);
	c0->cd(1);
	hNGam->Draw();
	c0->cd(2);
	hEGam->Draw();
	hSumEGam->SetLineColor(2);
	hSumEGam->Draw("same");
	
}