#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TArrow.h>
#include <TText.h>
#include <TMath.h>
#include <TLorentzVector.h>
#include <TVector3.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace std;

void DrawThetaCM_BhabhaISR(string fnameBhabha,string fnameISR, string outpath="ThetaCM_BhabhavsISR", string option="e-Bhabha")
{
gStyle->SetOptTitle(0);
gStyle->SetOptStat(0);
TGaxis::SetMaxDigits(2);
gStyle->SetPadTickX(0);
gStyle->SetPadTickY(0);

TFile* fBhabha = TFile::Open(fnameBhabha.c_str());
TFile* fISR = TFile::Open(fnameISR.c_str());

TTree* treeBhabha = (TTree*) fBhabha->Get("AllMCTruthTree");
TTree* treeISR = (TTree*) fISR->Get("AllMCTruthTree");

string outps = outpath+".ps";
string outpdf = outpath+".pdf";

int NBins=256;
double thetamin=0.;
//double thetamax=4.;
double thetamax=8.;
char hname[32];
char drawcmd[256];
char selection[256];
char htitle[256];

sprintf(hname,"hBhabha");
sprintf(selection,"MCpartProp==22");
double yunit=(thetamax-thetamin)/NBins;
sprintf(htitle,";#theta_{CM}^{#gamma} (mrad);Probability / %.2f mrad", yunit);

if(option=="e-Bhabha") sprintf(drawcmd,"(TMath::Pi()-MCiniMomThetaCM)*1000.>>%s(%d,%f,%f)",hname,NBins,thetamin,thetamax);
else sprintf(drawcmd,"(TMath::Pi()-MCiniMomThetaCM)*1000.>>%s(%d,%f,%f)",hname,NBins,thetamin,thetamax);
treeBhabha->Draw(drawcmd,selection,"goff");
TH1D* hBhabha = (TH1D*) gROOT->FindObject(hname);
hBhabha->SetTitle(htitle);
hBhabha->SetLineColor(1);


sprintf(hname,"hISR");
if(option=="e-Bhabha") sprintf(drawcmd,"(TMath::Pi()-MCiniMomThetaCM)*1000.>>%s(%d,%f,%f)",hname,NBins,thetamin,thetamax);
else sprintf(drawcmd,"(TMath::Pi()-MCiniMomThetaCM)*1000.>>%s(%d,%f,%f)",hname,NBins,thetamin,thetamax);
treeISR->Draw(drawcmd,selection,"goff");
TH1D* hISR = (TH1D*) gROOT->FindObject(hname);
hISR->SetTitle(htitle);
hISR->SetLineColor(2);

hBhabha->DrawNormalized();
hISR->DrawNormalized("same");

double ZZDD=335.; //cm
//double ZZDD=350.; //cm
//double ZZDD=400.; //value used in Fig.1, NIM A 718 (2013) 118-120?
double yZDDmin=tan(thetamin*1e-3)*ZZDD;
double yZDDmax=tan(thetamax*1e-3)*ZZDD;
double thetagap = 1e3*atan(0.5/ZZDD);

cout << hBhabha->GetMaximum()/hBhabha->GetEntries() << endl;
TGaxis* ax1 = new TGaxis(thetamin, 1.054*hBhabha->GetMaximum()/hBhabha->GetEntries(),
                    thetamax, 1.054*hBhabha->GetMaximum()/hBhabha->GetEntries(),
                    yZDDmin*10, yZDDmax*10, 510, "-");
ax1->SetTitle("y (mm)");										
ax1->Draw();										

TLegend* leg = new TLegend(0.6,0.5,0.85,0.7,"","NDC");
leg->SetBorderSize(0);
leg->SetFillColor(0);
leg->SetFillStyle(3003);
TLegendEntry* entryBhabha = leg->AddEntry(hBhabha,"e^{+}e^{-} #rightarrow e^{+}e^{-}#gamma","L");
TLegendEntry* entryISR = leg->AddEntry(hISR,"e^{+}e^{-} #rightarrow #pi^{+}#pi^{-}#pi^{+}#pi^{-}#gamma_{ISR}","-L");
leg->Draw();

TLine* lgap = new TLine(thetagap,0.,thetagap,hBhabha->GetMaximum()/hBhabha->GetEntries());
lgap->SetLineStyle(2);
lgap->Draw();	 

TArrow* arrowgap = new TArrow(thetamin,hBhabha->GetMaximum()/hBhabha->GetEntries(),thetagap,hBhabha->GetMaximum()/hBhabha->GetEntries(),0.03,"<>");
arrowgap->Draw();

TArrow* arrowZDD = new TArrow(thetagap,hBhabha->GetMaximum()/hBhabha->GetEntries(),thetamax,hBhabha->GetMaximum()/hBhabha->GetEntries(),0.03,"<");
arrowZDD->Draw();

TLatex* ltxgap = new TLatex(thetagap/2,0.90*hBhabha->GetMaximum()/hBhabha->GetEntries(),"gap");
ltxgap->SetTextAlign(21);
ltxgap->Draw();

TLatex* ltxZDD = new TLatex((thetagap+thetamax)/2,0.90*hBhabha->GetMaximum()/hBhabha->GetEntries(),"ZDD");
ltxZDD->SetTextAlign(21);
ltxZDD->Draw();

gPad->Print(Form("%s",outps.c_str()));		

//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
								
return;
}