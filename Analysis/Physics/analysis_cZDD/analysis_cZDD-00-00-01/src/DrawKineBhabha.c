#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <stdio.h>
#include <iostream>
#include <TKey.h>
#include <TList.h>

using namespace std;

void DrawKineBhabha(string inputname, string option, string outputname)
{

TFile* file = TFile::Open(inputname.c_str());
string outps=outputname+".ps";
string outpdf=outputname+".pdf";

TTree* tree;
TH1D* hPElecX = new TH1D("hPElecX","Electron;P_{X} (GeV/c);",100.,0.,0.);
TH1D* hPElecY = new TH1D("hPElecY","Electron;P_{Y} (GeV/c);",100.,0.,0.); 
TH1D* hPElecZ = new TH1D("hPElecZ","Electron;P_{Z} (GeV/c);",100.,0.,0.); 
TH1D* hPElecE = new TH1D("hPElecE","Electron;E (GeV);",100.,0.,0.);  

TH1D* hPPositronX = new TH1D("hPPositronX","Positron;P_{X} (GeV/c);",100.,0.,0.);
TH1D* hPPositronY = new TH1D("hPPositronY","Positron;P_{Y} (GeV/c);",100.,0.,0.); 
TH1D* hPPositronZ = new TH1D("hPPositronZ","Positron;P_{Z} (GeV/c);",100.,0.,0.); 
TH1D* hPPositronE = new TH1D("hPPositronE","Positron;E (GeV);",100.,0.,0.);

if(option=="ZDDBhabha")
	{
tree = (TTree*) file->Get("BhabhaTree");


double MCBhabhaElecMomRho;
double MCBhabhaElecMomTheta;
double MCBhabhaElecMomPhi;
double MCBhabhaElecE;

double MCBhabhaPositronMomRho;
	double MCBhabhaPositronMomTheta;
	double MCBhabhaPositronMomPhi;
	double MCBhabhaPositronE;

tree->SetBranchAddress("MCBhabhaElecMomRho",&MCBhabhaElecMomRho);	
tree->SetBranchAddress("MCBhabhaElecMomTheta",&MCBhabhaElecMomTheta);
tree->SetBranchAddress("MCBhabhaElecMomPhi",&MCBhabhaElecMomPhi);
tree->SetBranchAddress("MCBhabhaElecE",&MCBhabhaElecE);

tree->SetBranchAddress("MCBhabhaPositronMomRho",&MCBhabhaPositronMomRho);	
tree->SetBranchAddress("MCBhabhaPositronMomTheta",&MCBhabhaPositronMomTheta);
tree->SetBranchAddress("MCBhabhaPositronMomPhi",&MCBhabhaPositronMomPhi);
tree->SetBranchAddress("MCBhabhaPositronE",&MCBhabhaPositronE);




for(int entry=0;entry<tree->GetEntries();entry++)
	{
	tree->GetEntry(entry);
	
	TVector3 v3Elec;
	v3Elec.SetMagThetaPhi (MCBhabhaElecMomRho, MCBhabhaElecMomTheta, MCBhabhaElecMomPhi);
	//printf("%f %f %f %f \n",MCBhabhaElecMomRho,MCBhabhaElecMomTheta,MCBhabhaElecMomPhi,MCBhabhaElecE);
	
	TLorentzVector lvElec(v3Elec,MCBhabhaElecE);

	hPElecX->Fill(lvElec.Px());
	hPElecY->Fill(lvElec.Py());
	hPElecZ->Fill(lvElec.Pz());
	hPElecE->Fill(lvElec.E());
	
	TVector3 v3Positron;
	v3Positron.SetMagThetaPhi (MCBhabhaPositronMomRho, MCBhabhaPositronMomTheta, MCBhabhaPositronMomPhi);

	TLorentzVector lvPositron(v3Positron,MCBhabhaPositronE);

	hPPositronX->Fill(lvPositron.Px());
	hPPositronY->Fill(lvPositron.Py());
	hPPositronZ->Fill(lvPositron.Pz());
	hPPositronE->Fill(lvPositron.E());
	
	}
}	

if(option=="AllMCBhabhaold")
	{
	tree = (TTree*) file->Get("AllMCTruthTree");
	
	
	int MCpartProp;
	double MCiniMomRho[50];
	double MCiniMomTheta[50];
	double MCiniMomPhi[50];
	double MCiniE[50];
	
	tree->SetBranchAddress("MCiniMomRho",&MCiniMomRho);
	tree->SetBranchAddress("MCiniMomTheta",&MCiniMomTheta);
	tree->SetBranchAddress("MCiniMomPhi",&MCiniMomPhi);
	tree->SetBranchAddress("MCiniE",&MCiniE);
	
	for(int entry=0;entry<tree->GetEntries();entry++)
		{
		tree->GetEntry(entry);
	
		TVector3 v3Elec;
		v3Elec.SetMagThetaPhi (MCiniMomRho[0], MCiniMomTheta[0], MCiniMomPhi[0]);
		TLorentzVector lvElec(v3Elec,MCiniE[0]);
		
		hPElecX->Fill(lvElec.Px());
		hPElecY->Fill(lvElec.Py());
		hPElecZ->Fill(lvElec.Pz());
		hPElecE->Fill(lvElec.E());
	
		TVector3 v3Positron;
	v3Positron.SetMagThetaPhi (MCiniMomRho[1], MCiniMomTheta[1], MCiniMomPhi[1]);

	TLorentzVector lvPositron(v3Positron,MCiniE[1]);

	hPPositronX->Fill(lvPositron.Px());
	hPPositronY->Fill(lvPositron.Py());
	hPPositronZ->Fill(lvPositron.Pz());
	hPPositronE->Fill(lvPositron.E());
		}
	
TCanvas *c = new TCanvas();
c->Divide(4,2);

c->cd(1);
hPElecX->Draw();

c->cd(2);
hPElecY->Draw();

c->cd(3);
hPElecZ->Draw();

c->cd(4);
hPElecE->Draw();

c->cd(5);
hPPositronX->Draw();

c->cd(6);
hPPositronY->Draw();

c->cd(7);
hPPositronZ->Draw();

c->cd(8);
hPPositronE->Draw();

	}

if(option=="AllMCBhabha")
	{
	tree = (TTree*) file->Get("AllMCTruthTree");
char title[256];

	TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str())); 

	for(int i=0;i<3;i++)
		{
		if(i==0)
			{
			tree->Draw("MCpartProp","");
			}
		if(i==1)
			{
			c->Divide(4,2);

			c->cd(1);
			tree->Draw("MCiniMomX>>hMCiniMomXElec","MCpartProp==11");

			TH1D* histo;
			histo=(TH1D*) gPad->GetPrimitive("hMCiniMomXElec");
			histo->SetTitle("; Px_{e-}(GeV/c)");

			c->cd(2);
			tree->Draw("MCiniMomY>>hMCiniMomYElec","MCpartProp==11");
			histo=(TH1D*) gPad->GetPrimitive("hMCiniMomYElec");
			histo->SetTitle("; Py_{e-}(GeV/c)");

			c->cd(3);
			tree->Draw("MCiniMomZ>>hMCiniMomZElec","MCpartProp==11");
			histo=(TH1D*) gPad->GetPrimitive("hMCiniMomZElec");
			histo->SetTitle("; Pz_{e-}(GeV/c)");

			c->cd(4);
			tree->Draw("MCiniE>>hMCiniEElec","MCpartProp==11");
			histo=(TH1D*) gPad->GetPrimitive("hMCiniEElec");
			histo->SetTitle("; E_{e-}(GeV)");

			c->cd(5);
			tree->Draw("MCiniMomX>>hMCiniMomXPositron","MCpartProp==-11");
			histo=(TH1D*) gPad->GetPrimitive("hMCiniMomXPositron");
			histo->SetTitle("; Px_{e+} (GeV/c)");
			
			c->cd(6);
			tree->Draw("MCiniMomY>>hMCiniMomYPositron","MCpartProp==-11");
			histo=(TH1D*) gPad->GetPrimitive("hMCiniMomYPositron");
			histo->SetTitle("; Py_{e+} (GeV/c)");
			
			c->cd(7);
			tree->Draw("MCiniMomZ>>hMCiniMomZPositron","MCpartProp==-11");
			histo=(TH1D*) gPad->GetPrimitive("hMCiniMomZPositron");
			histo->SetTitle("; Pz_{e+} (GeV/c)");
			
			c->cd(8);
			tree->Draw("MCiniE>>hMCiniEPositron","MCpartProp==-11");
			histo=(TH1D*) gPad->GetPrimitive("hMCiniEPositron");
			histo->SetTitle("; E_{e+}(GeV)");
		}
		if(i==2)
			{
			c->Divide(3,2);

			c->cd(1);
			tree->Draw("MCfinPosX>>hMCfinPosXElec","MCpartProp==11");
			TH1D* histo;
			histo=(TH1D*) gPad->GetPrimitive("hMCfinPosXElec");
			histo->SetTitle("; X^{Final}_{e-}(cm)");

			c->cd(2);
			tree->Draw("MCfinPosY>>hMCfinPosYElec","MCpartProp==11");
			histo=(TH1D*) gPad->GetPrimitive("hMCfinPosYElec");
			histo->SetTitle("; Y^{Final}_{e-}(cm)");
			
			c->cd(3);
			tree->Draw("MCfinPosZ>>hMCfinPosZElec","MCpartProp==11");
			histo=(TH1D*) gPad->GetPrimitive("hMCfinPosZElec");
			histo->SetTitle("; Z^{Final}_{e-}(cm)");
			
			c->cd(4);
			tree->Draw("MCfinPosX>>hMCfinPosXPositron","MCpartProp==-11");
			histo=(TH1D*) gPad->GetPrimitive("hMCfinPosXPositron");
			histo->SetTitle("; X^{Final}_{e+}(cm)");

			c->cd(5);
			tree->Draw("MCfinPosY>>hMCfinPosYPositron","MCpartProp==-11");
			histo=(TH1D*) gPad->GetPrimitive("hMCfinPosYPositron");
			histo->SetTitle("; Y^{Final}_{e+}(cm)");
			
			c->cd(6);
			tree->Draw("MCfinPosZ>>hMCfinPosZPositron","MCpartProp==-11");
			histo=(TH1D*) gPad->GetPrimitive("hMCfinPosZPositron");
			histo->SetTitle("; Z^{Final}_{e+}(cm)");

		}
	c->Print(Form("%s",outps.c_str()));
	c->Clear();
	}
	
	c->Print(Form("%s]",outps.c_str()));
	}
	
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
}