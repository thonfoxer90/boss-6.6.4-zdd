#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TPave.h>
#include <stdio.h>
#include <iostream>
#include <TStyle.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>

using namespace std;

void DrawcZDDcrystals()
{

double crystalDX=1;
double crystalDY=1;
int NCrystalsX=4;
int NCrystalsY=3;
double XEdgeOrigTop=0;
double YEdgeOrigTop=0.5;
double XEdgeOrigBottom=0;XEdgeOrigBottom=XEdgeOrigTop;
double YEdgeOrigBottom;YEdgeOrigBottom=-YEdgeOrigTop;
//Top cZDD
vector<TLine*> linesY;
vector<TLine*> linesX;

double x0,y0;
double x1,y1;
x0=0;y0=0;
x1=0;y1=0;
for(int k=0;k<5;k++)
	{
	x0=k*crystalDX+XEdgeOrigTop;
	y0=0.5;
	x1=x0;
	y1=NCrystalsY*crystalDX+YEdgeOrigTop;
	linesY.push_back(new TLine(x0,y0,x1,y1));
	linesY.back()->SetLineColor(1);
	linesY.back()->SetLineWidth(2);
	linesY.back()->Draw();
	}

for(int k=0;k<4;k++)
	{
	x0=XEdgeOrigTop;
	y0=k*crystalDX+YEdgeOrigTop;
	x1=NCrystalsX*crystalDX+XEdgeOrigTop;
	y1=y0;
	linesX.push_back(new TLine(x0,y0,x1,y1));
	linesX.back()->SetLineColor(1);
	linesX.back()->SetLineWidth(2);
	linesX.back()->Draw();
	}
	
//Bottom cZDD
vector<TLine*> linesYBottom;
vector<TLine*> linesXBottom;

for(int k=0;k<5;k++)
	{
	x0=k*crystalDX+XEdgeOrigBottom;
	y0=YEdgeOrigBottom;
	x1=x0;
	y1=-NCrystalsY*crystalDX+YEdgeOrigBottom;
	linesYBottom.push_back(new TLine(x0,y0,x1,y1));
	linesYBottom.back()->SetLineColor(1);
	linesYBottom.back()->SetLineWidth(2);
	linesYBottom.back()->Draw();
	}

for(int k=0;k<4;k++)
	{
	x0=XEdgeOrigBottom;
	y0=-k*crystalDX+YEdgeOrigBottom;
	x1=NCrystalsX*crystalDX+XEdgeOrigBottom;
	y1=y0;
	linesXBottom.push_back(new TLine(x0,y0,x1,y1));
	linesXBottom.back()->SetLineColor(1);
	linesXBottom.back()->SetLineWidth(2);
	linesXBottom.back()->Draw();
	}
	
}


void DrawAnalysiscZDD(string inputname, string option, string outputname)
{

TFile* file = TFile::Open(inputname.c_str());
string outps=outputname+".ps";
string outpdf=outputname+".pdf";

if(option=="Impact_reco"){
	TTree* tree = (TTree*) file->Get(option.c_str());
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));
	char drawcmd[512];
	char selection[512];
	char drawopt[16];
	for(int i=0;i<2;i++){
		if(i==0) {
			sprintf(drawcmd,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>>h%d",i);	
			sprintf(selection,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>0");
			sprintf(drawopt,"");
			}
		if(i==1) {
		
			sprintf(drawcmd,"yImpact:xImpact>>h%d(128,-1.,5.,128,-5.,5.)",i);	
			sprintf(selection,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>0");
			sprintf(drawopt,"colz");
			}
		tree->Draw(drawcmd,selection,drawopt);
		if(i==1) DrawcZDDcrystals();
		c->Print(Form("%s",outps.c_str()));
		}
	c->Print(Form("%s]",outps.c_str()));
	}
		
else if(option=="myTree_events"){
	TTree* tree = (TTree*) file->Get(option.c_str());
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));
	char drawcmd[512];
	char selection[512];
	char drawopt[16];
	for(int i=0;i<2;i++){
		if(i==0) {
			sprintf(drawcmd,"zdd_energy>>h%d",i);	
			//sprintf(selection,"zdd_energy>0");
			sprintf(selection,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>0");
			sprintf(drawopt,"");
			}
		if(i==1) {
		
			sprintf(drawcmd,"hit_y:hit_x>>h%d(128,-1.,5.,128,-5.,5.)",i);	
			//sprintf(selection,"zdd_energy>0");
			sprintf(selection,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>0");
			sprintf(drawopt,"colz");
			}
		tree->Draw(drawcmd,selection,drawopt);
		if(i==1) DrawcZDDcrystals();
		c->Print(Form("%s",outps.c_str()));
		}
	c->Print(Form("%s]",outps.c_str()));
	}
	
else if(option=="particlegunTree"){
	TTree* tree = (TTree*) file->Get("particlegunTree");
	TTree* treeAllMC = (TTree*) file->Get("AllMCTruthTree");
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));
	char drawcmd[512];
	char selection[512];
	char drawopt[16];
	char title[256];
	char Xtitle[256];
	char Ytitle[256];
	double xmin,xmax,ymin,ymax;
	for(int i=0;i<11;i++){
	cout << i << endl;
		/*
		if(i==0) {			
			//sprintf(drawcmd,"MCpGunfinPosZ");
			sprintf(drawcmd,"MCpGunfinPosZ");
			sprintf(selection,"zdd_energy>0");
			sprintf(drawopt,"");
				
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h= (TH1D*)gPad->GetPrimitive("htemp");
			sprintf(title,";Z^{Final Hit} (cm)");
			h->SetTitle(title);
			}
		*/
		if(i==0) {			
			//sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h");
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h(64,-1.,5.,64,-5.,5.)");
			sprintf(selection,"");
			sprintf(drawopt,"colz");
				
			treeAllMC->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
			TH2D* h= (TH2D*)gPad->GetPrimitive("h");
			sprintf(title,"All MC;X^{Calc} (cm);Y^{Calc} (cm)");
			h->SetTitle(title);
			
		}
			
		if(i==1) {	
				/*
	sprintf(drawcmd,"MCpGunYCalcatZDD:MCpGunXCalcatZDD>>h(128,-1.,5.,128,-5.,5.)");
			sprintf(selection,"zdd_energy>0");
		
			sprintf(drawopt,"colz");			
			tree->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
			
			TH1D* h= (TH1D*)gPad->GetPrimitive("h");
			sprintf(title,"E^{Total}_{ZDD}>0;X^{Calc} (cm);Y^{Calc} (cm)");
			h->SetTitle(title);
			*/
			
			
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h(64,-1.,5.,64,-5.,5.)");
			sprintf(selection,"zdd_energy>0");
			sprintf(drawopt,"colz");
				
			treeAllMC->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
			TH2D* h= (TH2D*)gPad->GetPrimitive("h");
			sprintf(title,"E^{Total}_{ZDD}>0;X^{Calc} (cm);Y^{Calc} (cm)");
			h->SetTitle(title);
			
			}
		
		if(i==2) {			
			sprintf(drawcmd,"MCpGunfinPosY:MCpGunfinPosX>>h(64,-1.,5.,64,-5.,5.)");	
			sprintf(selection,"zdd_energy>0");
		
			//sprintf(selection,"MCpartProp==11");
			sprintf(drawopt,"colz");			
			tree->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
			}
		
		if(i==3) {			
			sprintf(drawcmd,"yFirstZDDHit:xFirstZDDHit>>h(64,-1.,5.,64,-5.,5.)");	
			sprintf(selection,"zdd_energy>0");
		
			//sprintf(selection,"MCpartProp==11");
			sprintf(drawopt,"colz");			
			tree->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
			
			TH1D* h= (TH1D*)gPad->GetPrimitive("h");
			sprintf(title,";X^{ZDD Impact} (cm);Y^{ZDD Impact} (cm)");
			h->SetTitle(title);
			}
		
		if(i==4) {			
			sprintf(drawcmd,"MCpGunfinPosX:MCpGunfinPosZ");	
			sprintf(selection,"zdd_energy>0");
		
			sprintf(drawopt,"colz");			
			tree->Draw(drawcmd,selection,drawopt);
			}
			
		if(i==5) {			
			sprintf(drawcmd,"MCpGunfinPosY:MCpGunfinPosZ");	
			sprintf(selection,"zdd_energy>0");
		
			sprintf(drawopt,"colz");			
			tree->Draw(drawcmd,selection,drawopt);
			}	
		
		if(i==6) {			
			sprintf(drawcmd,"yFirstZDDHit:MCpGunfinPosZ");	
			sprintf(selection,"zdd_energy>0");
		
			sprintf(drawopt,"colz");			
			tree->Draw(drawcmd,selection,drawopt);
						
			}
		
		if(i==7) {			
			sprintf(drawcmd,"xFirstZDDHit:MCpGunfinPosZ");	
			sprintf(selection,"zdd_energy>0");
		
			sprintf(drawopt,"colz");			
			tree->Draw(drawcmd,selection,drawopt);
			}
		
		if(i==8) {			
			sprintf(drawcmd,"zdd_energy");	
			sprintf(selection,"zdd_energy>0");
		
			sprintf(drawopt,"");			
			tree->Draw(drawcmd,selection,drawopt);
			
			TH1D* h= (TH1D*)gPad->GetPrimitive("htemp");
			sprintf(title,";E^{Total}_{ZDD} (MeV)");
			h->SetTitle(title);
			}
		
		if(i==9) {			
			sprintf(drawcmd,"MCiniMomTheta>>h");	
			sprintf(selection,"");
			sprintf(drawopt,"");
						
			treeAllMC->Draw(drawcmd,selection,drawopt);
			
			TH1D* h= (TH1D*)gPad->GetPrimitive("h");
			sprintf(title,"All MC;#theta (rad)");
			h->SetTitle(title);
			
			sprintf(drawcmd,"MCiniMomTheta>>h2");	
			sprintf(selection,"zdd_energy>0");
			sprintf(drawopt,"same");
						
			treeAllMC->Draw(drawcmd,selection,drawopt);
			
			TH1D* h2= (TH1D*)gPad->GetPrimitive("h2");
			sprintf(title,"E^{Total}_{ZDD}>0;#theta (rad)");
			h2->SetTitle(title);
			h2->SetLineColor(2);
			
			TLegend* leg= c->BuildLegend();
			leg->SetFillStyle(0);
			c->SetLogy();
			}
		c->SetLogy(0);
		if(i==10) {			
			sprintf(drawcmd,"MCfinPosZ");	
			sprintf(selection,"");
			sprintf(drawopt,"");
						
			treeAllMC->Draw(drawcmd,selection,drawopt);
			
			TH1D* h= (TH1D*)gPad->GetPrimitive("htemp");
			sprintf(title,"All MC;Z^{Final Hit} (cm)");
			h->SetTitle(title);
			
			}
										
		c->Print(Form("%s",outps.c_str()));
		}
	c->Print(Form("%s]",outps.c_str()));
	}

else if(option=="particlegun"){
//file->cd("particlegun");
//TIter nextkey(file->GetListOfKeys());

TDirectory* directory= file->GetDirectory("particlegun");
TIter nextkey(directory->GetListOfKeys());
TKey *key;

TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str())); 	

   while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
				
       if (obj->InheritsFrom("TH1") || obj->InheritsFrom("TH2")) {
       if(obj->InheritsFrom("TH2")) obj->Draw("colz");
			 else obj->Draw();
			 c->Print(Form("%s",outps.c_str()));
       }
			 
   } 
c->Print(Form("%s]",outps.c_str()));	 
}

else if(option=="Bhabha"){
TDirectory* directory= file->GetDirectory("Bhabha");
TIter nextkey(directory->GetListOfKeys());
TKey *key;

TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str())); 	

   while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
				
       if (obj->InheritsFrom("TH1") || obj->InheritsFrom("TH2")) {
       if(obj->InheritsFrom("TH2")) obj->Draw("colz");
			 else obj->Draw();
			 //c->Print(Form("%s",outps.c_str()));
			 c->Print(Form("%s",outps.c_str()),"Title:truc");
       }
			 
   } 
c->Print(Form("%s]",outps.c_str()));
}

else if(option.find("AllMCBhabhaNTuple")!= string::npos){
TTree* tree = (TTree*) file->Get("AllMCTruthTree");
TTree* treeZDD=(TTree*) file->Get("BhabhaTree");


TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str())); 	

   char drawcmd[512];
	char selection[512];
	char drawopt[16];
	char title[256];
	char Xtitle[256];
	char Ytitle[256];
	double xmin,xmax,ymin,ymax;
	for(int i=0;i<19;i++){
		if(i==0){
		if(option.find("e-toz+") != string::npos) {sprintf(selection,"MCpartProp==-11");}
		else {sprintf(selection,"MCpartProp==11");}
		gStyle->SetOptStat(0);
		c->Divide(2,2);
		c->cd(1);
		//sprintf(drawcmd,"MCfinPosZ");
		sprintf(drawcmd,"MCfinPosZ>>h1(128,-400.,0.)");			
		sprintf(drawopt,"");
		tree->Draw(drawcmd,selection,drawopt);
		
		TObject *obj;
		TIter next(gPad->GetListOfPrimitives());
   	while ((obj=next())) {
     cout << "Reading: " << obj->GetName() << endl;
       if (obj->InheritsFrom("TH1")) {
	   cout << "histo: " << obj->GetName() << endl;
		 		TH1D* h= (TH1D*)gPad->GetPrimitive(obj->GetName());
				if(option.find("e-toz+") != string::npos) {sprintf(title,"All MC Truth; Z^{Final Hit}_{e+} (cm)");}
				else {sprintf(title,"All MC Truth; Z^{Final Hit}_{e-}(cm)");}
				h->SetTitle(title);
       }
  	}
	
		c->cd(2);
		//sprintf(drawcmd,"MCfinPosX:MCfinPosZ");
		sprintf(drawcmd,"MCfinPosX:MCfinPosZ>>h2(128,-400.,0.,128,-10.,10.)");			
		sprintf(drawopt,"colz");
		tree->Draw(drawcmd,selection,drawopt);
		
		TIter next2(gPad->GetListOfPrimitives());
   	while ((obj=next2())) {
     cout << "Reading: " << obj->GetName() << endl;
       if (obj->InheritsFrom("TH2")) {
	   cout << "histo: " << obj->GetName() << endl;
		 		TH2D* h= (TH2D*)gPad->GetPrimitive(obj->GetName());
				if(option.find("e-toz+") != string::npos) {sprintf(title,"All MC Truth; Z^{Final Hit}_{e+}(cm);X^{Final Hit}_{e+}(cm)");}
				else {sprintf(title,"All MC Truth; Z^{Final Hit}_{e-}(cm);X^{Final Hit}_{e-}(cm)");}
				h->SetTitle(title);
       }
  	}
		
		c->cd(3);
		//sprintf(drawcmd,"MCfinPosY:MCfinPosZ");	
		sprintf(drawcmd,"MCfinPosY:MCfinPosZ>>h3(128,-400.,0.,128,-10.,10.)");			
		sprintf(drawopt,"colz");
		tree->Draw(drawcmd,selection,drawopt);
		
		TIter next3(gPad->GetListOfPrimitives());
   	while ((obj=next3())) {
     cout << "Reading: " << obj->GetName() << endl;
       if (obj->InheritsFrom("TH2")) {
	   cout << "histo: " << obj->GetName() << endl;
		 		TH2D* h= (TH2D*)gPad->GetPrimitive(obj->GetName());
				if(option.find("e-toz+") != string::npos) {sprintf(title,"All MC Truth; Z^{Final Hit}_{e+}(cm);Y^{Final Hit}_{e+}(cm)");}
				else {sprintf(title,"All MC Truth; Z^{Final Hit}_{e-}(cm);Y^{Final Hit}_{e-}(cm)");}
				h->SetTitle(title);
       }
  	}
		
		}
		
		if(i==1) {
		/*
			sprintf(drawcmd,"MCYCalcatZDD[0]:MCXCalcatZDD[0]>>h(128,-1.,6.,128,-5.,5.)");	
			sprintf(selection,"zddLeft_energy==0");
		*/	
			/*
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h(128,-1.,6.,128,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"zddLeft_energy==0 && MCpartProp==11");
			else sprintf(selection,"zddLeft_energy==0 && MCpartProp==-11");
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
			*/
			gStyle->SetOptStat(1);
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h1(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) 
			{sprintf(selection,"MCpartProp==-11");
			sprintf(title,"All MC Truth ; X_{e+} (cm); Y_{e+} (cm)");
			}
			else 
			{sprintf(selection,"MCpartProp==11");
			sprintf(title,"All MC Truth ; X_{e-} (cm); Y_{e-} (cm)");
			}
			//sprintf(drawopt,"box");
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h1 = (TH1D*) gROOT->FindObject("h1");
			h1->SetTitle(title);
			//h1->SetFillColor(1);
			DrawcZDDcrystals();
			/*
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h2(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==-11 && zddLeft_energy==0");
			else sprintf(selection,"MCpartProp==11 && zddLeft_energy==0");
			sprintf(drawopt,"box same");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h2 = (TH1D*) gROOT->FindObject("h2");
			h2->SetFillColor(2);
			
			DrawcZDDcrystals();
			*/
			}
			if(i==2) {
		
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h1(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) {sprintf(selection,"MCpartProp==-11 && zdd_energy>0");}
			else {sprintf(selection,"MCpartProp==11 && zdd_energy>0");}
			sprintf(drawopt,"box");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h1 = (TH1D*) gROOT->FindObject("h1");
			h1->SetFillColor(1);
			DrawcZDDcrystals();
			}
			if(i==3) {
		
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h1(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddLeft_energy==0");
			else sprintf(selection,"MCpartProp==11 && zdd_energy>0 && zddLeft_energy==0");
			sprintf(drawopt,"box");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h1 = (TH1D*) gROOT->FindObject("h1");
			h1->SetFillColor(1);
			DrawcZDDcrystals();
			}
			
			if(i==4){
			/*		
			sprintf(drawcmd,"MCfinPosZ");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==11");
			else sprintf(selection,"MCpartProp==-11");
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
			*/
		gStyle->SetOptStat(0);	
		if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==11");
		else sprintf(selection,"MCpartProp==-11");
		
		c->Divide(2,2);
		c->cd(1);
		//sprintf(drawcmd,"MCfinPosZ");
		sprintf(drawcmd,"MCfinPosZ>>h1(128,0.,400.)");			
		sprintf(drawopt,"");
		tree->Draw(drawcmd,selection,drawopt);
		
		TObject *obj;
		TIter next(gPad->GetListOfPrimitives());
   	while ((obj=next())) {
     cout << "Reading: " << obj->GetName() << endl;
       if (obj->InheritsFrom("TH1")) {
	   cout << "histo: " << obj->GetName() << endl;
		 		TH1D* h= (TH1D*)gPad->GetPrimitive(obj->GetName());
				if(option.find("e-toz+") != string::npos) {sprintf(title,"All MC Truth; Z^{Final Hit}_{e-} (cm)");}
				else {sprintf(title,"All MC Truth; Z^{Final Hit}_{e+}(cm)");}
				h->SetTitle(title);
       }
  	}
	
		c->cd(2);
		//sprintf(drawcmd,"MCfinPosX:MCfinPosZ");
		sprintf(drawcmd,"MCfinPosX:MCfinPosZ>>h2(128,0.,400.,128,-10.,10.)");			
		sprintf(drawopt,"colz");
		tree->Draw(drawcmd,selection,drawopt);
		
		TIter next2(gPad->GetListOfPrimitives());
   	while ((obj=next2())) {
     cout << "Reading: " << obj->GetName() << endl;
       if (obj->InheritsFrom("TH2")) {
	   cout << "histo: " << obj->GetName() << endl;
		 		TH2D* h= (TH2D*)gPad->GetPrimitive(obj->GetName());
				if(option.find("e-toz+") != string::npos) {sprintf(title,"All MC Truth; Z^{Final Hit}_{e-}(cm);X^{Final Hit}_{e-}(cm)");}
				else {sprintf(title,"All MC Truth; Z^{Final Hit}_{e+}(cm);X^{Final Hit}_{e+}(cm)");}
				h->SetTitle(title);
       }
			
			
			}
			
			c->cd(3);
		//sprintf(drawcmd,"MCfinPosY:MCfinPosZ");	
		sprintf(drawcmd,"MCfinPosY:MCfinPosZ>>h3(128,0.,400.,128,-10.,10.)");			
		sprintf(drawopt,"colz");
		tree->Draw(drawcmd,selection,drawopt);
		
		TIter next3(gPad->GetListOfPrimitives());
   	while ((obj=next3())) {
     cout << "Reading: " << obj->GetName() << endl;
       if (obj->InheritsFrom("TH2")) {
	   cout << "histo: " << obj->GetName() << endl;
		 		TH2D* h= (TH2D*)gPad->GetPrimitive(obj->GetName());
				if(option.find("e-toz+") != string::npos) {sprintf(title,"All MC Truth; Z^{Final Hit}_{e-}(cm);Y^{Final Hit}_{e-}(cm)");}
				else {sprintf(title,"All MC Truth; Z^{Final Hit}_{e+}(cm);Y^{Final Hit}_{e+}(cm)");}
				h->SetTitle(title);
       }
			 
			}
			
			}
			
			if(i==5) {
			gStyle->SetOptStat(1);
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h1(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) 
			{sprintf(selection,"MCpartProp==11");
			sprintf(title,"All MC Truth ; X_{e-} (cm); Y_{e-} (cm)");
			}
			else 
			{sprintf(selection,"MCpartProp==-11");
			sprintf(title,"All MC Truth ; X_{e+} (cm); Y_{e+} (cm)");
			}
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h1 = (TH1D*) gROOT->FindObject("h1");
			h1->SetTitle(title);
			DrawcZDDcrystals();		
			}
			
			if(i==6) {
			/*
			gStyle->SetOptStat(1);	
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h1(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==11 && zdd_energy>0");
			else sprintf(selection,"MCpartProp==-11 && zdd_energy>0");
			sprintf(drawopt,"box");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h1 = (TH1D*) gROOT->FindObject("h1");
			h1->SetFillColor(1);
			DrawcZDDcrystals();
			*/
			
			TLegend* leg = new TLegend(0.15, 0.85, 0.85,1.);
			//TLegend* leg = new TLegend(0.5, 0.67, 0.88,0.88);
				
			gStyle->SetOptStat(1);	
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h1(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==11");
			else sprintf(selection,"MCpartProp==-11");
			sprintf(drawopt,"box");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h1 = (TH1D*) gROOT->FindObject("h1");
			h1->SetTitle("");
			h1->SetLineColor(1);
			leg->AddEntry(h1,selection);
			
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h2(64,-1.,6.,64,-5.,5.)");	
			//if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==11 && zddRight_energy>0");
			//else sprintf(selection,"MCpartProp==-11 && zdd_energy>0");
			if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==11 && zddRight_energy>0");
			else sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddRight_energy>0");
			sprintf(drawopt,"box same");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h2 = (TH1D*) gROOT->FindObject("h2");
			h2->SetTitle("");
			h2->SetLineColor(2);
			leg->AddEntry(h2,selection);
						
			DrawcZDDcrystals();
						
			leg->Draw();			
			}
			/*
			if(i==6) {
		
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h1(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==11 && zdd_energy>0  && zddRight_energy==0");
			else sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddRight_energy==0");
			sprintf(drawopt,"box");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h1 = (TH1D*) gROOT->FindObject("h1");
			h1->SetFillColor(1);
			DrawcZDDcrystals();
			}
			*/
			if(i==7) {
		
			sprintf(drawcmd,"zddLeft_energy:zddRight_energy>>h1(64,0.,2000.,64,0.,2000.)");	
			sprintf(selection,"zdd_energy>0");
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			
			TH2D* h = (TH2D*)gPad->GetPrimitive("h1");
			sprintf(title,"E^{Total}_{ZDD} > 0 ; E^{Right}_{ZDD} (MeV) ; E^{Left}_{ZDD} (MeV)");
			h->SetTitle(title);
			}
			
			if(i==8) {
		
			sprintf(drawcmd,"zdd_energy>>h1(128,0.,4000.)");	
			sprintf(selection,"zdd_energy>0");
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
			
			TH2D* h = (TH2D*)gPad->GetPrimitive("h1");
			sprintf(title,"E^{Total}_{ZDD} > 0 ; E^{Total}_{ZDD} (MeV)");
			h->SetTitle(title);
			
			}
			
			
			if(i==9) {
			c->Divide(3,1);	
			c->cd(1);
			sprintf(drawcmd,"MCiniMomX>>hPx(64,0.,0.)");
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0");				
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
			
			sprintf(drawcmd,"MCiniMomX>>hPx2(64,0.,0.)");
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddLeft_energy>0 && zddRight_energy>0");			
			sprintf(drawopt,"same");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* hPx2 = (TH1D*) gROOT->FindObject("hPx2");
			hPx2->SetLineColor(2);
			c->cd(2);
			sprintf(drawcmd,"MCiniMomY>>hPy(64,0.,0.)");	
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0");
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
			
			sprintf(drawcmd,"MCiniMomY>>hPy2(64,0.,0.)");
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddLeft_energy>0 && zddRight_energy>0");			
			sprintf(drawopt,"same");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* hPy2 = (TH1D*) gROOT->FindObject("hPy2");
			hPy2->SetLineColor(2);
			c->cd(3);
			sprintf(drawcmd,"MCiniMomZ>>hPz(64,0.,0.)");	
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0");
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
			
			sprintf(drawcmd,"MCiniMomZ>>hPz2(64,0.,0.)");
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddLeft_energy>0 && zddRight_energy>0");			
			sprintf(drawopt,"same");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* hPz2 = (TH1D*) gROOT->FindObject("hPz2");
			hPz2->SetLineColor(2);
			}
			
			if(i==10) {
			c->Divide(3,1);	
			c->cd(1);
			sprintf(drawcmd,"MCiniMomRho>>hPRho(64,0.,0.)");
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0");				
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
			
			sprintf(drawcmd,"MCiniMomRho>>hPRho2(64,0.,0.)");
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddLeft_energy>0 && zddRight_energy>0");			
			sprintf(drawopt,"same");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* hPRho2 = (TH1D*) gROOT->FindObject("hPRho2");
			hPRho2->SetLineColor(2);
			c->cd(2);
			sprintf(drawcmd,"MCiniMomTheta>>hPTheta(64,0.,0.)");	
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0");
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
			
			sprintf(drawcmd,"MCiniMomTheta>>hPTheta2(64,0.,0.)");
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddLeft_energy>0 && zddRight_energy>0");			
			sprintf(drawopt,"same");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* hPTheta2 = (TH1D*) gROOT->FindObject("hPTheta2");
			hPTheta2->SetLineColor(2);
			c->cd(3);
			sprintf(drawcmd,"MCiniMomPhi>>hPPhi(64,0.,0.)");	
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0");
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
			
			sprintf(drawcmd,"MCiniMomPhi>>hPPhi2(64,0.,0.)");
			sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddLeft_energy>0 && zddRight_energy>0");			
			sprintf(drawopt,"same");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* hPPhi2 = (TH1D*) gROOT->FindObject("hPPhi2");
			hPPhi2->SetLineColor(2);
			}
		if(i==11) {
			c->Divide(3,1);	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddLeft_energy==0");
			else sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddRight_energy==0");
			
			c->cd(1);	
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h1(64,-1.,6.,64,-5.,5.)");					
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
			
			c->cd(2);					
			sprintf(drawcmd,"MCfinPosY:MCfinPosX>>h2(64,-1.,6.,64,-5.,5.)");					
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
					
			c->cd(3);
			sprintf(drawcmd,"MCfinPosZ");					
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
		}
		
		if(i==12) {
			c->Divide(3,1);	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddLeft_energy>0 && zddRight_energy>0");
			else sprintf(selection,"MCpartProp==-11 && zdd_energy>0 && zddLeft_energy>0 && zddRight_energy>0");
			
			c->cd(1);	
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h1(64,-1.,6.,64,-5.,5.)");					
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
			
			c->cd(2);					
			sprintf(drawcmd,"MCfinPosY:MCfinPosX>>h2(64,-1.,6.,64,-5.,5.)");					
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
					
			c->cd(3);
			sprintf(drawcmd,"MCfinPosZ");					
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
		}
		
		if(i==13) {
			sprintf(drawcmd,"MCXCalcatZDD[1]:MCXCalcatZDD[0]>>h1(64,-1.,6.,64,-1.,6.)");	
			sprintf(selection,"zdd_energy>0 && MCpartProp[0]==11 && MCpartProp[1]==-11");
			sprintf(drawopt,"colz");
			sprintf(title,"E^{Total}_{ZDD} > 0 ; X_{e-} (cm);X_{e+} (cm)");
			
			tree->Draw(drawcmd,selection,drawopt);
			TH2D* h = (TH2D*) gROOT->FindObject("h1");
			h->SetTitle(title);
			h->SetFillColor(1);
			
			TPave* paveZddXRegion = new TPave(0.,0.,4.,4.);			
			paveZddXRegion->SetFillStyle(0);
			paveZddXRegion->SetBorderSize(1);
			paveZddXRegion->SetLineColor(2);
			paveZddXRegion->SetLineWidth(2);
			paveZddXRegion->Draw();
			/*
			TLine* lxmin = new TLine(0.,h->GetYaxis()->GetXmin(),0.,h->GetYaxis()->GetXmax());
			lxmin->SetLineColor(2);
			lxmin->Draw();
			TLine* lxmax = new TLine(4.,h->GetYaxis()->GetXmin(),4.,h->GetYaxis()->GetXmax());
			lxmax->SetLineColor(2);
			lxmax->Draw();
			
			TLine* lymin = new TLine(h->GetXaxis()->GetXmin(),0.,h->GetXaxis()->GetXmax(),0.);
			lymin->SetLineColor(2);
			lymin->Draw();
			
			TLine* lymax = new TLine(h->GetXaxis()->GetXmin(),4.,h->GetXaxis()->GetXmax(),4.);
			lymax->SetLineColor(2);
			lymax->Draw();
			*/
		}
		
		if(i==14) {
			sprintf(drawcmd,"MCYCalcatZDD[1]:MCYCalcatZDD[0]>>h1(64,-5.,5.,64,-5.,5.)");	
			sprintf(selection,"zdd_energy>0 && MCpartProp[0]==11 && MCpartProp[1]==-11");
			sprintf(drawopt,"colz");
			sprintf(title,"E^{Total}_{ZDD} > 0 ; Y_{e-} (cm);Y_{e+} (cm)");
			
			tree->Draw(drawcmd,selection,drawopt);
			TH2D* h = (TH2D*) gROOT->FindObject("h1");
			h->SetTitle(title);
			h->SetFillColor(1);
			
			TPave* paveZddYRegions[4];
			
			paveZddYRegions[0] = new TPave(-4.,0.5,-0.5,4);			
			paveZddYRegions[0]->SetFillStyle(0);
			paveZddYRegions[0]->SetBorderSize(1);
			paveZddYRegions[0]->SetLineColor(2);
			paveZddYRegions[0]->SetLineWidth(2);
			paveZddYRegions[0]->Draw();
			
			paveZddYRegions[1] = new TPave(0.5,0.5,4.,4.);			
			paveZddYRegions[1]->SetFillStyle(0);
			paveZddYRegions[1]->SetBorderSize(1);
			paveZddYRegions[1]->SetLineColor(2);
			paveZddYRegions[1]->SetLineWidth(2);
			paveZddYRegions[1]->Draw();
					
			paveZddYRegions[2] = new TPave(-4.,-4.,-0.5,-0.5);			
			paveZddYRegions[2]->SetFillStyle(0);
			paveZddYRegions[2]->SetBorderSize(1);
			paveZddYRegions[2]->SetLineColor(2);
			paveZddYRegions[2]->SetLineWidth(2);
			paveZddYRegions[2]->Draw();
			
			paveZddYRegions[3] = new TPave(0.5,-4.,4.,-0.5);			
			paveZddYRegions[3]->SetFillStyle(0);
			paveZddYRegions[3]->SetBorderSize(1);
			paveZddYRegions[3]->SetLineColor(2);
			paveZddYRegions[3]->SetLineWidth(2);
			paveZddYRegions[3]->Draw();
			
			/*
			TLine* lxmin = new TLine(-4.,h->GetYaxis()->GetXmin(),-4.,h->GetYaxis()->GetXmax());
			lxmin->SetLineColor(2);
			lxmin->Draw();
			
			TLine* lxmax = new TLine(4.,h->GetYaxis()->GetXmin(),4.,h->GetYaxis()->GetXmax());
			lxmax->SetLineColor(2);
			lxmax->Draw();
			
			TLine* lymin = new TLine(h->GetXaxis()->GetXmin(),-4.,h->GetXaxis()->GetXmax(),-4.);
			lymin->SetLineColor(2);
			lymin->Draw();
			
			TLine* lymax = new TLine(h->GetXaxis()->GetXmin(),4.,h->GetXaxis()->GetXmax(),4.);
			lymax->SetLineColor(2);
			lymax->Draw();
			*/
		}
		
		if(i==15) {
			sprintf(drawcmd,"MCfinPosX[1]:MCfinPosX[0]>>h1(64,-1.,6.,64,-1.,6.)");
			//sprintf(drawcmd,"MCfinPosX:MCfinPosX>>h1(64,-1.,6.,64,-1.,6.)");	
			sprintf(selection,"zdd_energy>0 && MCpartProp[0]==11 && MCpartProp[1]==-11");
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			TH2D* h = (TH2D*) gROOT->FindObject("h1");
			h->SetTitle(";X_{e-} (cm);X_{e+} (cm)");
			h->SetFillColor(1);
			
			TPave* paveZddXRegion = new TPave(0.,0.,4.,4.);			
			paveZddXRegion->SetFillStyle(0);
			paveZddXRegion->SetBorderSize(1);
			paveZddXRegion->SetLineColor(2);
			paveZddXRegion->SetLineWidth(2);
			paveZddXRegion->Draw();
		}
		
		if(i==16) {
		
			sprintf(drawcmd,"yFirstPositronZDDHit:xFirstPositronZDDHit>>h1(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"zddLeft_energy>0");
			else sprintf(selection,"zddRight_energy>0");
			sprintf(drawopt,"colz");
			treeZDD->Draw(drawcmd,selection,drawopt);
			TH1D* h1 = (TH1D*) gROOT->FindObject("h1");
			h1->SetFillColor(1);
			DrawcZDDcrystals();
			}
		
		if(i==17) {
		
			sprintf(drawcmd,"yFirstElecZDDHit:xFirstElecZDDHit>>h1(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"zddRight_energy>0");
			else sprintf(selection,"zddLeft_energy>0");
			sprintf(drawopt,"colz");
			treeZDD->Draw(drawcmd,selection,drawopt);
			TH1D* h1 = (TH1D*) gROOT->FindObject("h1");
			h1->SetFillColor(1);
			DrawcZDDcrystals();
			}
		/*	
		if(i==18) {
		
			sprintf(drawcmd,"MCfinPosX:MCfinPosZ");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"MCpartProp==-11");
			else sprintf(selection,"MCpartProp==11");
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			}
			
		if(i==19) {
		
			sprintf(drawcmd,"yFirstElecZDDHit:xFirstElecZDDHit>>h1(64,-1.,6.,64,-5.,5.)");	
			if(option.find("e-toz+") != string::npos) sprintf(selection,"zddRight_energy>0");
			else sprintf(selection,"zddLeft_energy>0");
			sprintf(drawopt,"colz");
			treeZDD->Draw(drawcmd,selection,drawopt);
			TH1D* h1 = (TH1D*) gROOT->FindObject("h1");
			h1->SetFillColor(1);
			DrawcZDDcrystals();
			}
			*/				
		c->Print(Form("%s",outps.c_str()));
		c->Clear();
		}
c->Print(Form("%s]",outps.c_str()));
}

else if(option.find("Phokhara")!= string::npos){
TTree* tree = (TTree*) file->Get("AllMCTruthTree");


TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str())); 	

   char drawcmd[512];
	char selection[512];
	char drawopt[16];
	char title[256];
	char Xtitle[256];
	char Ytitle[256];
	double xmin,xmax,ymin,ymax;
	for(int i=0;i<4;i++){
		if(i==0){
			sprintf(drawcmd,"MCfinPosZ>>h");	
			sprintf(selection,"MCpartProp==22");
			sprintf(title," ; Z^{Final}_{#gamma ISR};");
			sprintf(drawopt,"");
			tree->Draw(drawcmd,selection,drawopt);
			TH1D* h = (TH1D*) gROOT->FindObject("h");
			h->SetTitle(title);
		}
		if(i==1){
		sprintf(drawcmd,"MCiniE");	
		sprintf(selection,"MCpartProp==22");
		sprintf(title," ; E_{#gamma ISR} (GeV);");
		sprintf(drawopt,"");
		tree->Draw(drawcmd,selection,drawopt);
		TH1D* h = (TH1D*) gROOT->FindObject("htemp");
		h->SetTitle(title);
		}
		if(i==2){
		sprintf(drawcmd,"zdd_energy");	
		sprintf(selection,"zdd_energy>0");
		sprintf(title," E^{Total}_{ZDD}>0 ; E^{Total}_{ZDD} (GeV);");
		sprintf(drawopt,"");
		tree->Draw(drawcmd,selection,drawopt);
		TH1D* h = (TH1D*) gROOT->FindObject("htemp");
		h->SetTitle(title);
		}

		if(i==3){
		gStyle->SetOptStat(0);
		sprintf(drawcmd,"zdd_energy/1000:MCiniE");	
		sprintf(selection,"MCpartProp==22 && zdd_energy>0");
		sprintf(title," E^{Total}_{ZDD}>0 ; E_{#gamma ISR} (GeV); E^{Total}_{ZDD} (GeV)");
		sprintf(drawopt,"colz");
		tree->Draw(drawcmd,selection,drawopt);
		TH2D* h = (TH2D*) gROOT->FindObject("htemp");
		h->SetTitle(title);
		}

	c->Print(Form("%s",outps.c_str()));
	c->Clear();
	}
c->Print(Form("%s]",outps.c_str()));
}	

//else if(option=="Impact_reco&particlegunTree"){
else {
	/*
	TTree* particlegunTree = (TTree*) file->Get("particlegunTree");
	TTree* ImpactrecoTree = (TTree*) file->Get("Impact_reco");
	//Impact_reco variables
	double xImpact, yImpact;
	ImpactrecoTree->SetBranchAddress("xImpact",&xImpact);
	ImpactrecoTree->SetBranchAddress("xImpact",&yImpact);
	
	//Initialize histograms
	vector <TH2D*> histos2D;
	//Brice tree variables
	for(int entry=0;entry<particlegunTree->GetEntries();entry++)
		{
		particlegunTree->GetEntry(entry);
		ImpactrecoTree->GetEntry(entry);
		}
	*/
	
	
	TTree* tree = (TTree*) file->Get("particlegunTree");
	tree->AddFriend("Impact_reco",inputname.c_str());
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));
	char drawcmd[512];
	char selection[512];
	char drawopt[16];
	for(int i=0;i<1;i++){
		if(i==0) {
			sprintf(drawcmd,"xImpact-MCXCalcatZDD");	
			sprintf(selection,"");
			sprintf(drawopt,"");
			}
		tree->Draw(drawcmd,selection,drawopt);
		c->Print(Form("%s",outps.c_str()));
		}
	c->Print(Form("%s]",outps.c_str()));
	
	
	}
	
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
	
}



