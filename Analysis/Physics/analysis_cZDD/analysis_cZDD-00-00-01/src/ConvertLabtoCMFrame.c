#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TPave.h>
#include <stdio.h>
#include <iostream>
#include <TStyle.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TLorentzVector.h>
#include <TVector3.h>

using namespace std;

const double m_dCrossingAngle		 = 0.011;
const double m_dMasselec					 = 511e-6;

//void ConvertLabToCMFrame(string filename, double m_dCMSEnergy, string outputname, string option)
void ConvertLabToCMFrame(string filename, double m_dCMSEnergy, string outputname)
{

TFile* file = TFile::Open(filename.c_str());
TTree* tree = (TTree*) file->Get("AllMCTruthTree");

//bool rotY=false;
//if(option.find("rotY") != string::npos) rotY=true;

const int NMax=256;
int MCntracks;
int MCpartProp[NMax];
double MCiniMomRho[NMax], MCiniMomTheta[NMax], MCiniMomPhi[NMax], MCiniE[NMax],
			 MCiniMomX[NMax], MCiniMomY[NMax], MCiniMomZ[NMax]

			;
double zdd_energy;

tree->SetBranchAddress("MCntracks", &MCntracks);
tree->SetBranchAddress("MCpartProp",MCpartProp);
tree->SetBranchAddress("MCiniMomRho",MCiniMomRho);
tree->SetBranchAddress("MCiniMomTheta",MCiniMomTheta);
tree->SetBranchAddress("MCiniMomPhi",MCiniMomPhi);

tree->SetBranchAddress("MCiniMomX",MCiniMomX);
tree->SetBranchAddress("MCiniMomY",MCiniMomY);
tree->SetBranchAddress("MCiniMomZ",MCiniMomZ);
tree->SetBranchAddress("MCiniE",MCiniE);

tree->SetBranchAddress("zdd_energy",&zdd_energy);



int MCntracksCM;
int MCpartPropCM[NMax];
double MCiniMomRhoCM[NMax], MCiniMomThetaCM[NMax], MCiniMomPhiCM[NMax], MCiniECM[NMax],
			 MCiniMomXCM[NMax], MCiniMomYCM[NMax], MCiniMomZCM[NMax]

			;
double zdd_energyCM;

string outroot=outputname+".root";
TFile* fileCM = new TFile(outroot.c_str(),"RECREATE");
TTree* treeCM = new TTree("AllMCTruthTreeCM","");

treeCM->Branch("MCntracks", &MCntracksCM,"MCntracks/I");
treeCM->Branch("MCpartProp", MCpartPropCM,"MCpartProp[MCntracks]/I");
treeCM->Branch("MCiniMomX", MCiniMomXCM,"MCiniMomX[MCntracks]/D");
treeCM->Branch("MCiniMomY", MCiniMomYCM, "MCiniMomY[MCntracks]/D");
treeCM->Branch("MCiniMomZ", MCiniMomZCM, "MCiniMomZ[MCntracks]/D");
treeCM->Branch("MCiniMomRho", MCiniMomRhoCM, "MCiniMomRho[MCntracks]/D");
treeCM->Branch("MCiniMomTheta", MCiniMomThetaCM,"MCiniMomTheta[MCntracks]/D");
treeCM->Branch("MCiniMomPhi", MCiniMomPhiCM, "MCiniMomPhi[MCntracks]/D");
treeCM->Branch("MCiniE", MCiniECM,"MCiniE[MCntracks]/D");
treeCM->Branch("zdd_energy",&zdd_energyCM);

for(int entry=0;entry<tree->GetEntries();entry++)
	{
	tree->GetEntry(entry);
	/*
	double TotEngBeam = m_dCMSEnergy/2 + m_dMasselec;
	double Abs3MomBeam = sqrt(TotEngBeam*TotEngBeam - m_dMasselec*m_dMasselec);
		
	TLorentzVector m_lvBeamElectron,m_lvBeamPositron;
	m_lvBeamElectron.SetPx(0);
	m_lvBeamElectron.SetPy(0);
	m_lvBeamElectron.SetPz(-Abs3MomBeam);
	m_lvBeamElectron.SetE(TotEngBeam);
  
	m_lvBeamPositron.SetPx(Abs3MomBeam*sin(m_dCrossingAngle));
	m_lvBeamPositron.SetPy(0);
	m_lvBeamPositron.SetPz(Abs3MomBeam*cos(m_dCrossingAngle));
	m_lvBeamPositron.SetE(TotEngBeam);
	
	TLorentzVector m_lvBoost        = m_lvBeamElectron+m_lvBeamPositron;
	*/
/*	
double TotEngBeam = m_dCMSEnergy/2 + m_dMasselec;
	double Abs3MomBeam = sqrt(TotEngBeam*TotEngBeam - m_dMasselec*m_dMasselec);
		
	TLorentzVector m_lvBeamElectron,m_lvBeamPositron;
	m_lvBeamPositron.SetPx(0);
	m_lvBeamPositron.SetPy(0);
	m_lvBeamPositron.SetPz(-Abs3MomBeam);
	m_lvBeamPositron.SetE(TotEngBeam);
  
	m_lvBeamElectron.SetPx(Abs3MomBeam*sin(m_dCrossingAngle));
	m_lvBeamElectron.SetPy(0);
	m_lvBeamElectron.SetPz(Abs3MomBeam*cos(m_dCrossingAngle));
	m_lvBeamElectron.SetE(TotEngBeam);
	
	TLorentzVector m_lvBoost        = m_lvBeamElectron+m_lvBeamPositron;
*/

//TLorentzVector m_lvBoost(m_dCMSEnergy*sin(0.011),0,0,m_dCMSEnergy); //OK

double TotEngBeam = m_dCMSEnergy/2 + m_dMasselec;
	double Abs3MomBeam = sqrt(TotEngBeam*TotEngBeam - m_dMasselec*m_dMasselec);
		
	TLorentzVector m_lvBeamElectron,m_lvBeamPositron;
	m_lvBeamElectron.SetPx(Abs3MomBeam*sin(m_dCrossingAngle));
	m_lvBeamElectron.SetPy(0);
	m_lvBeamElectron.SetPz(-Abs3MomBeam*cos(m_dCrossingAngle));
	m_lvBeamElectron.SetE(TotEngBeam);
  
	m_lvBeamPositron.SetPx(Abs3MomBeam*sin(m_dCrossingAngle));
	m_lvBeamPositron.SetPy(0);
	m_lvBeamPositron.SetPz(Abs3MomBeam*cos(m_dCrossingAngle));
	m_lvBeamPositron.SetE(TotEngBeam);
	
	TLorentzVector m_lvBoost        = m_lvBeamElectron+m_lvBeamPositron;

	TVector3 m_vectboost=m_lvBoost.BoostVector();	
	
	zdd_energyCM=zdd_energy;
	MCntracksCM=MCntracks;
	for(int k=0;k<MCntracks;k++)
		{
		TLorentzVector lv;
		lv.SetPxPyPzE(MCiniMomX[k],MCiniMomY[k],MCiniMomZ[k],MCiniE[k]);
		TLorentzVector lvCM=lv;
		lvCM.Boost(-m_vectboost);
		
		//Assign CM NTuple variable

		MCpartPropCM[k]=MCpartProp[k];
		//cout << MCpartProp[k] << " " << MCpartPropCM[k] << endl;
		MCiniECM[k]=	lvCM.E();
		MCiniMomXCM[k]=	lvCM.Px();
		MCiniMomXCM[k]=	lvCM.Px();
		MCiniMomYCM[k]=	lvCM.Py();
		MCiniMomZCM[k]=	lvCM.Pz();
		
		MCiniMomRhoCM[k]=	lvCM.Rho();
		MCiniMomThetaCM[k]=	lvCM.Theta();
		MCiniMomPhiCM[k]=	lvCM.Phi();
		}	
	treeCM->Fill();	
	}
	
	treeCM->Write();
}
