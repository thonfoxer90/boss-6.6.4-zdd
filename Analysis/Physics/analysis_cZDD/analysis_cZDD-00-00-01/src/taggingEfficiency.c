#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TMath.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TString.h>
#include <TObjArray.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#include "BesZddCrystals.cxx"

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()
						
using namespace std;

double zdd_energy;
double zddLeft_energy;
double zddRight_energy;
double XZddGammaISRMCTruth;
double YZddGammaISRMCTruth;
double GamISRMCTruthTheta;
double GamISRMCTruthE;


const int NMAX=256;
int MCntracks;
int 	 MCpartProp[NMAX];
int 	 MCprimPart[NMAX];
double MCXCalcatZDD[NMAX];
double MCYCalcatZDD[NMAX];
double MCiniMomTheta[NMAX];
double MCiniE[NMAX];
double MCiniMomThetaCM[NMAX];
double MCiniECM[NMAX];

BesZddCrystals* Zddcrystals;
vector<double> stlVLimBinsX;
vector<double> stlVLimBinsY;
vector<string> vCrystalNames;
vector<string> vXYLabels;
vector<string> vLabels;

vector <string> tokenize(string str, char delim){
	vector <string> strsplit;

	stringstream ss;
	string strtmp;

	ss.str(str);				
		while (getline(ss, strtmp, delim)) {
 		strsplit.push_back(strtmp);
		}	

	return strsplit;		
}

TGraphErrors* makeTaggingEfficiencyGraph(map<string, double> &mapval,map<string, double> &maperr,string pattern){
	TGraphErrors* gr;
	vector<double> vecX;
	vector<double> vecErrX;
	vector<double> vecY;
	vector<double> vecErrY;
	
	for(std::map<string,double>::iterator it = mapval.begin();it!=mapval.end();++it){
		string key=it->first;
		double val=it->second;
		if(key.find(pattern)!=string::npos){
		vector<string> keySplit = tokenize(key, ';');
			for(int k=0;k<keySplit.size();k++){
				if(keySplit[k].find("EMCTrue")!=string::npos){
					string strX=(tokenize(keySplit[k],'>'))[1];
					cout << strX <<endl;
					vecX.push_back(atof(strX.c_str()));
					vecErrX.push_back(0);
					vecY.push_back(val);
					vecErrY.push_back(maperr[key]);
				}
			}
		}
	}
	gr = new TGraphErrors(vecX.size(),&vecX[0],&vecY[0],&vecErrX[0],&vecErrY[0]);
	return gr;
}

map<string,double> convertMapStringInt(map<string,int> &oldmap){
	map<string,double> newmap;
	for(std::map<string,int>::iterator it = oldmap.begin();it!=oldmap.end();++it){
		string key=it->first;
		int val=it->second;
		newmap[key]=(double)val;
	}
	return newmap;
}

void initialize(vector<string> &keys, map<string, int> &map){
	for(int i=0;i<keys.size();i++){
		string key = keys[i];
		map[key]=0;
	}
}

void initialize(vector<string> &keys, map<string,double> &map){
	for(int i=0;i<keys.size();i++){
		string key = keys[i];
		map[key]=0.;
	}
}

vector<string> initalizeXYLocationLabels(vector<string> &vCrystalNames){
	vector<string> vLabels;
	vLabels.push_back("inZDD");
	for(int i=0;i<vCrystalNames.size();i++){
		vLabels.push_back(vCrystalNames[i]);
	}
	return vLabels;
}

vector<string> initalizeVecLabels(vector<string> &vXYLabels){
	vector<string> vLabels;
	vector<string> vZDDLocation;
	//vZDDLocation.push_back("all"); vZDDLocation.push_back("East");vZDDLocation.push_back("West");
	vZDDLocation.push_back("all"); 
	for(int i=0;i<vZDDLocation.size();i++){
		for(int j=0;j<vXYLabels.size();j++){
			string label;
			label=vZDDLocation[i]+";"+vXYLabels[j];
			vLabels.push_back(label);
		}
	}
	
	for(int k=0;k<vLabels.size();k++) cout << vLabels[k]<<endl;
	cout << endl;
	sleep(2);
	return vLabels;
}

void initialize(){
	Zddcrystals= new BesZddCrystals(4,3,0,1);
	stlVLimBinsX = Zddcrystals->getVLimBinX();
	stlVLimBinsY = Zddcrystals->getVLimBinY();
	Zddcrystals->printLimBin(stlVLimBinsX);
	Zddcrystals->printLimBin(stlVLimBinsY);
	vCrystalNames = Zddcrystals->getVecCrystalNames();
	vXYLabels = initalizeXYLocationLabels(vCrystalNames);  
	vLabels=initalizeVecLabels(vXYLabels);
	
}
	
void taggingEfficiency(string inputfile,string option, string outputname){
	gROOT->SetBatch();
	initialize();
	
	TFile* file = TFile::Open(inputfile.c_str());
	TTree* treeMCTruth = (TTree*)file->Get("AllMCTruthTree");
	treeMCTruth->SetBranchAddress("MCntracks", &MCntracks);
	treeMCTruth->SetBranchAddress("MCpartProp", MCpartProp);
	treeMCTruth->SetBranchAddress("MCprimPart", MCprimPart);
	treeMCTruth->SetBranchAddress("MCXCalcatZDD", MCXCalcatZDD);
	treeMCTruth->SetBranchAddress("MCYCalcatZDD", MCYCalcatZDD);
	treeMCTruth->SetBranchAddress("zdd_energy", &zdd_energy);
	treeMCTruth->SetBranchAddress("MCiniE", MCiniE);
	treeMCTruth->SetBranchAddress("MCiniECM", MCiniECM);
	treeMCTruth->SetBranchAddress("MCiniMomThetaCM", MCiniMomThetaCM);
	
	TH2D* hThetaCMvsECM = new TH2D("hThetaCMvsECM",";E_{CM}(GeV);#theta_{CM} (rad)",256,0.,2.,256,0.,TMath::Pi());
	
	TH2D* hThetaCMvsECMTruthToZDD = new TH2D("hThetaCMvsECMTruthToZDD",";E_{CM}(GeV);#theta_{CM} (mrad)",64,0.,2.,64,0.,20.);
	
	
	vector<double> limbin_ETruth;
	int nBins=10;
	limbin_ETruth.resize(nBins+1);
	for(int k=0;k<limbin_ETruth.size();k++){
		limbin_ETruth[k]= (0+k*2/(double)(nBins));
		cout << k << endl;
		cout << limbin_ETruth[k] << endl;
	}
	
	map<string, int> nGamTruthToZDD;
	initialize(vLabels,nGamTruthToZDD);
	map<string, int> nGamTruthOutZDD;
	initialize(vLabels,nGamTruthOutZDD);
	for(int entry=0;entry<treeMCTruth->GetEntries();entry++){
		//if(entry%10000==0) cout << "Event# "<< entry << endl;
		cout << "Event# "<< entry << endl;
		treeMCTruth->GetEntry(entry);
		int nPrimPhotons=0;	
		vector<int> idxPrimPhotons;
		bool ISREvent=false;
		for(int k=0;k<MCntracks;k++){
			//if(MCpartProp[k]==22 && MCprimPart[k]==1)
			if(MCpartProp[k]==22) {nPrimPhotons++;idxPrimPhotons.push_back(k);}
		}
		
		if(nPrimPhotons==1) ISREvent=true;
		
		if(!ISREvent) continue;
		int idxISR=idxPrimPhotons[0];
		double EGam=MCiniE[idxISR];
		double ThetaGam=MCiniMomThetaCM[idxISR];

		hThetaCMvsECM->Fill(MCiniECM[idxISR],MCiniMomThetaCM[idxISR]);
		for(int i=0;i<vLabels.size();i++){
		
			vector<string> splitLabels=tokenize(vLabels[i],';');
			string labelXY=splitLabels[1];
			
			//bool InZDD = MCXCalcatZDD[idxISR]>=0. && MCXCalcatZDD[idxISR]<=4. && fabs(MCYCalcatZDD[idxISR])>=0.5 && fabs(MCYCalcatZDD[idxISR])<=3.5; 
			//bool InZDD = MCXCalcatZDD[idxISR]>=3. && MCXCalcatZDD[idxISR]<=4. && MCYCalcatZDD[idxISR]>=0.5 && MCYCalcatZDD[idxISR]<=1.5; //Crystal 104
			
			bool condition=Zddcrystals->checkHitCrystal(labelXY,MCXCalcatZDD[idxISR],MCYCalcatZDD[idxISR]);			
			
			if(condition){
				nGamTruthToZDD[vLabels[i]]++;
				for(int k=0;k<limbin_ETruth.size();k++){
					string key= vLabels[i]+";EMCTrue>"+SSTR( limbin_ETruth[k]);
					if(EGam>limbin_ETruth[k])nGamTruthToZDD[key]++;

					hThetaCMvsECMTruthToZDD->Fill(MCiniECM[idxISR],MCiniMomThetaCM[idxISR]*1e3);
				}
			}
			else {
				nGamTruthOutZDD[vLabels[i]]++;
				for(int k=0;k<limbin_ETruth.size();k++){
					string key= vLabels[i]+";EMCTrue>"+SSTR( limbin_ETruth[k]);
					if(EGam>limbin_ETruth[k])nGamTruthOutZDD[key]++;
				}
			}
		}
	}
	/*
	for(std::map<string,int>::iterator it = nGamTruthToZDD.begin();it!=nGamTruthToZDD.end();++it){
		string key =it->first;
		cout << key << " " << it->second << endl;
	}
	cout <<endl;
	for(std::map<string,int>::iterator it = nGamTruthOutZDD.begin();it!=nGamTruthOutZDD.end();++it){
		string key =it->first;
		cout << key << " " << it->second << endl;
	}
	sleep(2);
	*/
	
	cout << "nGamTruthToZDD[\"all;inZDD\"]" << nGamTruthToZDD["all;inZDD"] << endl;
	cout << "nGamTruthOutZDD[\"all;inZDD\"]" << nGamTruthOutZDD["all;inZDD"] << endl;
	string treename;
	if(option=="") treename="4PiGamISRTaggedTree";
	else treename=option;
	TTree* tree = (TTree*) file->Get(treename.c_str());		
	tree->SetBranchAddress("zdd_energy", &zdd_energy);
	tree->SetBranchAddress("zddLeft_energy", &zddLeft_energy);
	tree->SetBranchAddress("zddRight_energy", &zddRight_energy);
	
	tree->SetBranchAddress("XZddGammaISRMCTruth", &XZddGammaISRMCTruth);
	tree->SetBranchAddress("YZddGammaISRMCTruth", &YZddGammaISRMCTruth);
	tree->SetBranchAddress("GamISRMCTruthCMTheta", &GamISRMCTruthTheta);
	tree->SetBranchAddress("GamISRMCTruthCME", &GamISRMCTruthE);
	
	TH2D* hThetaCMvsECMTagDirect = new TH2D("hThetaCMvsECMTagDirect",";E_{CM}(GeV);#theta_{CM} (mrad)",64,0.,2.,64,0.,20.);
	
	map<string, int> nTag;
	initialize(vLabels,nTag);
	
	map<string, int> nTagDirect;
	initialize(vLabels,nTag);
	
	map<string, int> nTagIndirect;
	initialize(vLabels,nTag);
	
	
	
	for(int entry=0;entry<tree->GetEntries();entry++){
		tree->GetEntry(entry);
		double EGam=GamISRMCTruthE;
		for(int i=0;i<vLabels.size();i++){
			vector<string> splitLabels=tokenize(vLabels[i],';');
			string labelXY=splitLabels[1];
			
			if(zdd_energy>0) {nTag[vLabels[i]]++;
				for(int k=0;k<limbin_ETruth.size();k++){
						string key= vLabels[i]+";EMCTrue>"+SSTR( limbin_ETruth[k]);
						if(EGam>limbin_ETruth[k])nTag[key]++;
				}

				
				bool condition=Zddcrystals->checkHitCrystal(labelXY,XZddGammaISRMCTruth,YZddGammaISRMCTruth);
				if(condition){
					nTagDirect[vLabels[i]]++;
					for(int k=0;k<limbin_ETruth.size();k++){
						string key= vLabels[i]+";EMCTrue>"+SSTR( limbin_ETruth[k]);
						if(EGam>limbin_ETruth[k])nTagDirect[key]++;
					}
					hThetaCMvsECMTagDirect->Fill(GamISRMCTruthE,GamISRMCTruthTheta*1e3);
				}
				else {nTagIndirect[vLabels[i]]++;
					for(int k=0;k<limbin_ETruth.size();k++){
						string key= vLabels[i]+";EMCTrue>"+SSTR( limbin_ETruth[k]);
						if(EGam>limbin_ETruth[k])nTagIndirect[key]++;
					}
				}
			}
		}
	}
	
	for(std::map<string,int>::iterator it = nTagDirect.begin();it!=nTagDirect.end();++it){
		string key =it->first;
		cout << key << " " << it->second << endl;
	}
	cout <<endl;
	
	map<string, double> nTagOvernGamTruth;
	map<string, double> nTagOvernGamTruthErr;
	map<string, double> nTagOvernGamTruthToZDD;
	map<string, double> nTagOvernGamTruthToZDDErr;
	map<string, double> nTagOvernGamTruthOutZDD;
	map<string, double> nTagOvernGamTruthOutZDDErr;	
		
	map<string, double> fractiongTagIndirect;
	map<string, double> fractiongTagIndirectErr;
	map<string, double> fractiongTagDirect;
	map<string, double> efficiencyTagging;	
	map<string, double> efficiencyTaggingErr;	
	
	for(std::map<string,int>::iterator it = nTag.begin();it!=nTag.end();++it){
		string key=it->first;
		
		nTagOvernGamTruth[key]=(double)nTag[key]/(double)treeMCTruth->GetEntries();
		nTagOvernGamTruthErr[key]=sqrt((double)nTag[key]/(pow((double)treeMCTruth->GetEntries(),2))
		+pow((double)nTag[key],2)/(pow((double)treeMCTruth->GetEntries(),3))
		);
		nTagOvernGamTruthToZDD[key]=(double)nTag[key]/(double)nGamTruthToZDD[key];
		nTagOvernGamTruthOutZDD[key]=(double)nTag[key]/(double)nGamTruthOutZDD[key];
	
		fractiongTagIndirect[key]=(double)nTagIndirect[key]/(double)nTag[key];
		fractiongTagIndirectErr[key]=(double)nTagIndirect[key]/pow((double)nTag[key],2)
		+pow((double)nTagIndirect[key],2)/pow((double)nTag[key],3)
		;
		fractiongTagDirect[key]=(double)nTagDirect[key]/(double)nTag[key];
		efficiencyTagging[key] =  (double)nTag[key]*(1-fractiongTagIndirect[key])/(double)nGamTruthToZDD[key];
		//efficiencyTaggingErr[key]=0;
		efficiencyTaggingErr[key]=sqrt(
		pow((1-fractiongTagIndirect[key]),2)*(double)nTag[key]/pow((double)nTagDirect[key],2)
		+pow((double)nTag[key]*fractiongTagIndirectErr[key],2)/pow((double)nTagDirect[key],2)
		+pow((double)nTag[key]*(1-fractiongTagIndirect[key]),2)/pow((double)nTagDirect[key],3)
		)
		;
		
		
		cout << key << endl;
		cout << " Nselected = " << nTag[key] 
		     << ", NMCTruth = " << treeMCTruth->GetEntries()
		     << ", NMCTruthToZDD = " << nGamTruthToZDD[key]
		     << ", nGamTruthOutZDD = " << nGamTruthOutZDD[key]
		     << endl;
		cout << " Nselected/NMCTruth = " << nTagOvernGamTruth[key] 
				 << ", Nselected/NMCTruthToZDD = " << nTagOvernGamTruthToZDD[key]
				 << ", Nselected/NMCTruthOutZDD = " << nTagOvernGamTruthOutZDD[key]
				 << ", fractiongTagIndirect="<< fractiongTagIndirect[key] 
				 << ", Tagging Efficiency="<<efficiencyTagging[key] << "+/-" << efficiencyTaggingErr[key]
				 << endl;
	}
	cout<<endl;
	
	TH1D* hECMTruthToZDD = hThetaCMvsECMTruthToZDD->ProjectionX();
	TH1D* hECMTagDirect = hThetaCMvsECMTagDirect->ProjectionX();
	TH1D* hTaggingEfficiencyECM = (TH1D*)hECMTagDirect->Clone("hTaggingEfficiencyECM");
	hTaggingEfficiencyECM->Divide(hECMTruthToZDD);
	
	TH2D* hTaggingEfficiency = (TH2D*)hThetaCMvsECMTagDirect->Clone("hTaggingEfficiency");
	hTaggingEfficiency->Divide(hThetaCMvsECMTruthToZDD);	
		
	string outlog=outputname+".log";
	ofstream writeLog(outlog.c_str());
	writeLog << "nTag[\"all;inZDD\"] = " << nTag["all;inZDD"]<< endl;
	writeLog << "nTag/nGamTruth = " << nTagOvernGamTruth["all;inZDD"]<< "+/-"<< nTagOvernGamTruthErr["all;inZDD"] << endl;
	writeLog << "nTag/nGamTruthToZDD[\"all;inZDD\"] = " << nTagOvernGamTruthToZDD["all;inZDD"]<< "+/-"<< nTagOvernGamTruthToZDDErr["all;inZDD"] << endl;
	writeLog << "nTag/nGamTruthOutZDD[\"all;inZDD\"] = " << nTagOvernGamTruthOutZDD["all;inZDD"]<< "+/-"<< nTagOvernGamTruthOutZDDErr["all;inZDD"]<< endl;
	writeLog << "Fraction of detected direct gamma = " << fractiongTagDirect["all;inZDD"] << endl;
	writeLog << "Fraction of detected indirect gamma = " << fractiongTagIndirect["all;inZDD"]<< "+/-"<< fractiongTagIndirectErr["all;inZDD"] << endl;
	writeLog << "Tagging efficiency " << efficiencyTagging["all;inZDD"] << "+/-"<< efficiencyTaggingErr["all;inZDD"]<< endl;
	
	string outROOT=outputname+".root";
	TFile* outfile = new TFile(outROOT.c_str(),"RECREATE");
	hThetaCMvsECM->Write();
	hThetaCMvsECMTruthToZDD->Write();
	hThetaCMvsECMTagDirect->Write();
	hTaggingEfficiencyECM->Write();
	hTaggingEfficiency->Write();
	hECMTruthToZDD->Write();
	hECMTagDirect->Write();
	TGraphErrors* grEffvsEMC = makeTaggingEfficiencyGraph(efficiencyTagging,efficiencyTaggingErr,"EMCTrue");
	grEffvsEMC->SetName("grEffvsEMC");
	grEffvsEMC->SetTitle(";E^{MC Truth}_{min}(GeV) ;#varepsilon^{Tag}(E^{MC Truth}_{min})");
	grEffvsEMC->SetMarkerStyle(8);	
	grEffvsEMC->Write();
	
	TGraphErrors* grTagFractionvsEMC = makeTaggingEfficiencyGraph(nTagOvernGamTruth,nTagOvernGamTruthErr,"EMCTrue");
	grTagFractionvsEMC->SetName("grTagFractionvsEMC");
	grTagFractionvsEMC->SetTitle(";E^{MC Truth}_{min}(GeV) ;N^{EZdd>0}/N^{All MC Truth}(E^{MC Truth}_{min})");
	grTagFractionvsEMC->SetMarkerStyle(8);
	grTagFractionvsEMC->Write();
	
	map<string,double> dnGamTruthToZDD= convertMapStringInt(nGamTruthToZDD);
	map<string,double> dnGamTruthToZDDErr;
	for(std::map<string,double>::iterator it = dnGamTruthToZDD.begin();it!=dnGamTruthToZDD.end();++it){
		string key =it->first;
		dnGamTruthToZDDErr[key]=0;
	}
	TGraphErrors* grnGamTruthToZDD=makeTaggingEfficiencyGraph(dnGamTruthToZDD,dnGamTruthToZDDErr,"all;inZDD");;
	grnGamTruthToZDD->SetName("grnGamTruthToZDD");
	grnGamTruthToZDD->Write();
	
	map<string,double> dnTagDirect= convertMapStringInt(nTagDirect);
	map<string,double> dnTagDirectErr;
	for(std::map<string,double>::iterator it = dnTagDirect.begin();it!=dnTagDirect.end();++it){
		string key =it->first;
		dnTagDirectErr[key]=0;
	}
	TGraphErrors* grnTagDirect=makeTaggingEfficiencyGraph(dnTagDirect,dnTagDirectErr,"all;inZDD");
	grnTagDirect->SetName("grnTagDirect");
	grnTagDirect->Write();
	
	string outpdf = outputname+".pdf";
	TCanvas *c = new TCanvas();
	c->SaveAs(Form("%s[",outpdf.c_str()));
	gPad->SetLogz();	
	hThetaCMvsECM->Draw("colz");
	c->SaveAs(Form("%s",outpdf.c_str()));
	grEffvsEMC->Draw("ALP");
	c->SaveAs(Form("%s",outpdf.c_str()));
	grTagFractionvsEMC->Draw("ALP");
	c->SaveAs(Form("%s",outpdf.c_str()));
	c->SaveAs(Form("%s]",outpdf.c_str()));
	
}

void multiTaggingEfficiencyParticleGun(string inpath){
	vector<string> crystalNames;
	crystalNames.resize(13);
	crystalNames[0]="0deg";
	crystalNames[1]="104";crystalNames[2]="114";crystalNames[3]="124";
	crystalNames[4]="103";crystalNames[5]="113";crystalNames[6]="123";
	crystalNames[7]="102";crystalNames[8]="112";crystalNames[9]="122";
	crystalNames[10]="101";crystalNames[11]="111";crystalNames[12]="121";

	vector<string> energies;
	energies.resize(6);
	energies[0]="50MeV";energies[1]="100MeV";energies[2]="200MeV";
	energies[3]="500MeV";energies[4]="1500MeV";energies[5]="2000MeV";
	
	for(int i=0;i<crystalNames.size();i++){
		for(int j=0;j<energies.size();j++){
			string infile=inpath+"particleGun_"+crystalNames[i]+"_"+energies[j]+"_Pipe.root";
			string outpath="taggingEfficiency/particleGun_"+crystalNames[i]+"_"+energies[j]+"_Pipe";
			taggingEfficiency(infile,"particlegunTree",outpath);
		}
	}
}


vector<double> getTaggingEfficiency(string logfile){
	vector<double> result; //Value and error
	result.resize(2);
	ifstream readFile(logfile.c_str());
	if(readFile.is_open()){
		while(readFile.good()){
			string str;
			getline(readFile,str);
			TString tstr(str);
			if(str.find("Tagging efficiency")!=string::npos){
				TObjArray* strTok = tstr.Tokenize(" ");
				//for(int k=0;k<strTok->GetEntries();k++) cout <<  ((TObjString *)(strTok->At(k)))->String() << endl;
				//cout << endl;
				TObjArray* strTok2 = ((TObjString *)(strTok->Last()))->String().Tokenize("+/-");
				//for(int k=0;k<strTok2->GetEntries();k++) cout <<  ((TObjString *)(strTok2->At(k)))->String() << endl;
				result[0]=((TObjString *)(strTok2->First()))->String().Atof();
				result[1]=((TObjString *)(strTok2->Last()))->String().Atof();
			}
		}
	}
	
	cout << result[0] << " " << result[1] << endl;
	return result;
}

void makeTaggingEfficiencyVsCrystal(string inpath){
	gStyle->SetOptStat(0);
	initialize();
	
	vector<string> crystalNames;
	crystalNames.resize(13);
	crystalNames[0]="0deg";
	crystalNames[1]="104";crystalNames[2]="114";crystalNames[3]="124";
	crystalNames[4]="103";crystalNames[5]="113";crystalNames[6]="123";
	crystalNames[7]="102";crystalNames[8]="112";crystalNames[9]="122";
	crystalNames[10]="101";crystalNames[11]="111";crystalNames[12]="121";

	vector<string> energies;
	energies.resize(6);
	energies[0]="50MeV";energies[1]="100MeV";energies[2]="200MeV";
	energies[3]="500MeV";energies[4]="1500MeV";energies[5]="2000MeV";
	
	map<string, TGraphErrors*> vGraphEffvsEnergy;/*
	for(int k=0;k<crystalNames.size();k++){
	 vGraphEffvsEnergy[crystalNames[k]]=new TGraphErrors();
	 string grname="gr"+crystalNames[k];
	 vGraphEffvsEnergy[crystalNames[k]]->SetName(grname.c_str());
	}
	*/
	map<string,vector<double> > mapEnergy;
	map<string,vector<double> > mapEnergyErr;
	map<string,vector<double> > mapTaggingEfficiency;
	map<string,vector<double> > mapTaggingEfficiencyErr;
	
	
	string outROOTfile = inpath+"taggingEfficiencyVsCrystals.root";
	TFile* outROOT = new TFile(outROOTfile.c_str(),"RECREATE");
	
	string outpdf=inpath+"taggingEfficiencyVsCrystals.pdf";
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outpdf.c_str())); 
	for(int i=0;i<energies.size();i++){
		double energy;
		
		vector<string> strTokenized = tokenize(energies[i],'M');
		energy=atof((strTokenized[0]).c_str());
		//cout << "ENERGY= "<< energy << endl; sleep(1);
		string outTableName=inpath+"taggingEfficiency_"+energies[i]+"_Pipe.dat";
		ofstream writeTable(outTableName.c_str());
		writeTable << "#crystal	" << "TaggingEfficiency	" << "#delta" << endl;
		string hname = "htaggingEfficiencyXY_"+energies[i];
		string htitle = "E_{#gamma} = "+ energies[i]+";X(cm);Y(cm)";
		TH2D* histo = new TH2D(hname.c_str(),htitle.c_str(),stlVLimBinsX.size()-1,&(stlVLimBinsX[0]), stlVLimBinsY.size()-1, &(stlVLimBinsY[0]));
		histo->SetMinimum(0);
		histo->SetMaximum(1);
		TH2D* hFrame = new TH2D("hframe",htitle.c_str(),1,-1.,5.,1,-5.,5.);
		for(int j=0;j<crystalNames.size();j++){
			string infile=inpath+"particleGun_"+crystalNames[j]+"_"+energies[i]+"_Pipe.log";
			cout << infile <<endl;
			vector<double> vals = getTaggingEfficiency(infile);
			if(vals[0]!=vals[0]) continue;
			writeTable << crystalNames[j] << "	"<< vals[0] << "	"<<vals[1] <<endl;
			Zddcrystals->fillTH2(histo,crystalNames[j],vals[0],vals[1]);
			/*
			//vGraphEffvsEnergy[crystalNames[j]]->Set();
			vGraphEffvsEnergy[crystalNames[j]]->SetPoint(vGraphEffvsEnergy[crystalNames[j]]->GetN()+1,vals[0],i);
			*/
			/*
			vGraphEffvsEnergy[crystalNames[j]]->Set(vGraphEffvsEnergy[crystalNames[j]]->GetN()+1);
			vGraphEffvsEnergy[crystalNames[j]]->SetPoint(vGraphEffvsEnergy[crystalNames[j]]->GetN(),i,vals[0]);
			*/
			mapEnergy[crystalNames[j]].push_back(energy);
			mapEnergyErr[crystalNames[j]].push_back(0);
			mapTaggingEfficiency[crystalNames[j]].push_back(vals[0]);
			mapTaggingEfficiencyErr[crystalNames[j]].push_back(vals[1]);
		}
		histo->Write();
		
		hFrame->Draw();
		histo->Draw("same colz text E");
		c->Print(Form("%s",outpdf.c_str()));
		c->Clear();
	}
	
	
	
	for(std::map<string, vector<double> >::iterator it = mapEnergy.begin();it!=mapEnergy.end();++it){
		string key=it->first;
		vector<double> vx= mapEnergy[key];
		vector<double> vy=mapTaggingEfficiency[key];
		vector<double> errvx=mapEnergyErr[key];
		vector<double> errvy=mapTaggingEfficiencyErr[key];
		vGraphEffvsEnergy[key] = new TGraphErrors(vx.size(),&vx[0],&vy[0],&errvx[0],&errvy[0]);
		string grname="gr"+key;
		vGraphEffvsEnergy[key]->SetName(grname.c_str());
		string grtitle="Crystal #"+key+";E_{#gamma}(MeV);#varepsilon^{Eff tag}";
		vGraphEffvsEnergy[key]->SetTitle(grtitle.c_str());
		//vGraphEffvsEnergy[key]->SetMarkerStyle(8);
		vGraphEffvsEnergy[key]->SetMinimum(0);
		vGraphEffvsEnergy[key]->SetMaximum(1.5);
		vGraphEffvsEnergy[key]->Write();
		
		
		vGraphEffvsEnergy[key]->Draw("ALP");
		TLine* lineRatioeq1 = new TLine(vx.front(),1.,vx.back(),1.);
		lineRatioeq1->SetLineStyle(2);
		lineRatioeq1->Draw();
		c->Print(Form("%s",outpdf.c_str()));
		c->Clear();
	}
	
	c->Print(Form("%s]",outpdf.c_str()));
	
	outROOT->Close();
}