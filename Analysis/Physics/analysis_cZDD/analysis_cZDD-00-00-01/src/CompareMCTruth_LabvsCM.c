#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TPave.h>
#include <stdio.h>
#include <iostream>
#include <TStyle.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TLorentzVector.h>
#include <TVector3.h>

using namespace std;

void CompareMCTruth_LabvsCM(string filenameLab, string filenameCM, string outputname)
{
gStyle->SetOptTitle(0);
gStyle->SetOptStat(0);

TFile* fileLab = TFile::Open(filenameLab.c_str());
TTree* treeLab = (TTree*) fileLab->Get("AllMCTruthTree");
TFile* fileCM = TFile::Open(filenameCM.c_str());
TTree* treeCM;
//treeCM = (TTree*) fileCM->Get("Bhabha");
	string outps=outputname+".ps";
	string outpdf=outputname+".pdf";

if((treeCM = (TTree*) fileCM->Get("Bhabha"))!=NULL)
	{
	treeCM->AddFriend(treeLab);

	TCanvas* c =new TCanvas();
	c->Print(Form("%s[",outps.c_str())); 	
	//treeCM->Draw("Bhabha.MCpartProp-AllMCTruthTree.MCpartProp","");
	treeCM->Draw("Bhabha.MCiniMomTheta*180./3.14159>>h(256,0.,2.)","Bhabha.MCpartProp==-11");
	TH1D* h=(TH1D*)gROOT->FindObject("h");
	h->SetTitle("All ; #theta^{CM}_{e+} (#circ)");
	treeCM->Draw("Bhabha.MCiniMomTheta*180./3.14159>>h2(256,0.,2.)","Bhabha.MCpartProp==-11 && AllMCTruthTree.zdd_energy>0","same");
	TH1D* h2=(TH1D*)gROOT->FindObject("h2");
	h2->SetTitle("E^{Total}_{ZDD}>0 ; #theta_{e+} (#circ)");
	h2->SetLineColor(2);

	treeCM->Draw("Bhabha.MCiniMomTheta*180./3.14159>>h3(256,0.,2.)","Bhabha.MCpartProp==-11 && Bhabha.MCntracks==2 && AllMCTruthTree.zdd_energy>0","same");
	TH1D* h3=(TH1D*)gROOT->FindObject("h3");
	h3->SetTitle("E^{Total}_{ZDD}>0, e^{+}e^{-} #rightarrow e^{+}e^{-} only ; #theta_{e+} (#circ)");
	h3->SetLineColor(3);

	c->SetLogy();
	c->BuildLegend();

	c->Print(Form("%s",outps.c_str()));
	c->Clear();

	treeCM->Draw("Bhabha.MCiniE>>hEGam(256,0.,0.)","Bhabha.MCpartProp==22");
	TH1D* hEGam=(TH1D*)gROOT->FindObject("hEGam");
	hEGam->SetTitle("All ; E_{#gamma} (GeV)");
	treeCM->Draw("Bhabha.MCiniE>>hEGam2(256,0.,0.)","Bhabha.MCpartProp==22 && Bhabha.MCntracks>2 && AllMCTruthTree.zdd_energy>0","same");
	TH1D* hEGam2=(TH1D*)gROOT->FindObject("hEGam2");
	hEGam2->SetTitle("E^{Total}_{ZDD}>0 ; E_{#gamma} (GeV)");
	hEGam2->SetLineColor(2);

	c->SetLogy();
	c->BuildLegend();

	c->Print(Form("%s",outps.c_str()));
	c->Clear();

	c->SetLogy(0);
	TH1D* hratioEGam=(TH1D*) hEGam2->Clone();
	hratioEGam->Divide(hEGam);
	hratioEGam->Draw();
	c->Print(Form("%s",outps.c_str()));
	c->Clear();

	c->Print(Form("%s]",outps.c_str()));
	}

else if ((treeCM = (TTree*) fileCM->Get("AllMCTruthTreeCM"))!=NULL)
	{
	treeCM->AddFriend(treeLab);

	TCanvas* c =new TCanvas();
	c->Print(Form("%s[",outps.c_str())); 	
	treeCM->Draw("AllMCTruthTreeCM.MCiniMomTheta*180./3.14159>>h(256,0.,2.)","AllMCTruthTreeCM.MCpartProp==-11");
	TH1D* h=(TH1D*)gROOT->FindObject("h");
	h->SetTitle("All ; #theta^{CM}_{e+} (#circ)");
	treeCM->Draw("AllMCTruthTreeCM.MCiniMomTheta*180./3.14159>>h2(256,0.,2.)","AllMCTruthTreeCM.MCpartProp==-11 && AllMCTruthTree.zdd_energy>0","same");
	TH1D* h2=(TH1D*)gROOT->FindObject("h2");
	h2->SetTitle("E^{Total}_{ZDD}>0 ; #theta_{e+} (#circ)");
	h2->SetLineColor(2);

	treeCM->Draw("AllMCTruthTreeCM.MCiniMomTheta*180./3.14159>>h3(256,0.,2.)","AllMCTruthTreeCM.MCpartProp==-11 && AllMCTruthTreeCM.MCntracks==2 && AllMCTruthTree.zdd_energy>0","same");
	TH1D* h3=(TH1D*)gROOT->FindObject("h3");
	h3->SetTitle("E^{Total}_{ZDD}>0, e^{+}e^{-} #rightarrow e^{+}e^{-} only ; #theta_{e+} (#circ)");
	h3->SetLineColor(3);

	c->SetLogy();
	c->BuildLegend();

	c->Print(Form("%s",outps.c_str()));
	c->Clear();

	treeCM->Draw("AllMCTruthTreeCM.MCiniE>>hEGam(256,0.,0.)","AllMCTruthTreeCM.MCpartProp==22");
	TH1D* hEGam=(TH1D*)gROOT->FindObject("hEGam");
	hEGam->SetTitle("All ; E_{#gamma} (GeV)");
	treeCM->Draw("AllMCTruthTreeCM.MCiniE>>hEGam2(256,0.,0.)","AllMCTruthTreeCM.MCpartProp==22 && AllMCTruthTreeCM.MCntracks>2 && AllMCTruthTree.zdd_energy>0","same");
	TH1D* hEGam2=(TH1D*)gROOT->FindObject("hEGam2");
	hEGam2->SetTitle("E^{Total}_{ZDD}>0 ; E_{#gamma} (GeV)");
	hEGam2->SetLineColor(2);

	c->SetLogy();
	c->BuildLegend();

	c->Print(Form("%s",outps.c_str()));
	c->Clear();

	c->SetLogy(0);
	TH1D* hratioEGam=(TH1D*) hEGam2->Clone();
	hratioEGam->Divide(hEGam);
	hratioEGam->Draw();
	c->Print(Form("%s",outps.c_str()));
	c->Clear();

	c->Print(Form("%s]",outps.c_str()));
	}
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

return;
}
