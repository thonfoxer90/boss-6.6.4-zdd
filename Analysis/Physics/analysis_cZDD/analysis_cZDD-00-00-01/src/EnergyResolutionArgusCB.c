#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooConstVar.h"
#include "RooAddPdf.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooArgusBG.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TMath.h>
#include <TLatex.h>
#include <TText.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()
    
#include "TRandom.h"

using namespace std;
using namespace RooFit ;

void EnergyResolution(string inputfile, double Etot, string option, string outputname)
{

gStyle->SetOptFit(1);

TFile* file = TFile::Open(inputfile.c_str());
TTree* tree = (TTree*) file->Get("AllMCTruthTree");

string outps=outputname+".ps";
string outpdf=outputname+".pdf";
string outroot = outputname+".root";

/*
char drawcmd[256];
sprintf(drawcmd,"(zdd_energy/%f)>>h(256,0.,1.2)",Etot);
TCanvas *c = new TCanvas();
tree->Draw(drawcmd,"zdd_energy>0");
delete c;
TH1D* histo = (TH1D*)gROOT->FindObject("h");
histo->SetTitle(";E_{ZDD}/E_{Total}");
*/

double zdd_energy;
tree->SetBranchAddress("zdd_energy",&zdd_energy);

TH1D* histo = new TH1D("histo",";E_{ZDD}/E_{Total}",256,0.,1.2);

for(int entry=0;entry<tree->GetEntries();entry++)
	{
	tree->GetEntry(entry);
	if(zdd_energy==0) continue;
	histo->Fill(zdd_energy/Etot);
	}

double intg = histo->Integral();

 // Declare observables
 /*
 RooRealVar x("x","x",0.,1.2) ;
 
 RooRealVar argcutoff("argcutoff","argus cutoff",1.,0.05,1.2) ; 
 RooRealVar argshape("argshape","argus shape parameter",-20.0,-100.,-1.) ; 
 
 
 RooRealVar cbmean("cbmean", "cbmean" , 0.6, 0.5, 1.) ;
	RooRealVar cbsigma("cbsigma", "cbsigma" , 0.02, 0., 0.5) ;
	RooRealVar cbsig("cbsig", "cbsignal", intg/2, 0, intg);
	RooRealVar n("n","", 2.,0.,5.);
	RooRealVar alpha("alpha","", 1.,0.,4.);
 
 // Create a binned dataset that imports contents of TH1 and associates its contents to observable 'x'
  RooDataHist dh("dh","dh",x,Import(*histo)) ;
	
	//Create fit functions
	RooCBShape cball("cball", "crystal ball", x, cbmean, cbsigma, alpha, n);
	//RooArgusBG argus("argus","Argus PDF",x,RooConst(1.),argshape) ;
	RooArgusBG argus("argus","Argus PDF",x,argcutoff,argshape) ;
	
	// --- Construct signal+background PDF --- 
	RooRealVar nsig("nsig","#signal events",intg/2,0.,intg) ; 
	RooRealVar nbkg("nbkg","#background events",intg/2,0.,intg) ; 
	RooAddPdf sum("sum","g+a",RooArgList(cball,argus),RooArgList(nsig,nbkg)) ; 
	
	//Fit
	//cball.fitTo(dh);
	sum.fitTo(dh,Range(0.05,1.2));
	
	// Make plot of binned dataset showing Poisson error bars (RooFit default)
  RooPlot* frame = x.frame() ;
  dh.plotOn(frame) ; 
	//cball.plotOn(frame, LineColor(kMagenta));
	//sum.plotOn(frame, LineColor(kRed));
	cball.plotOn(frame, LineColor(kBlue));
	//argus.plotOn(frame, LineColor(kMagenta));
	
	frame->Draw();

	printf("sigE = %f mu=%f sigE/mu = %f \n", cbsigma.getVal(),cbmean.getVal(),cbsigma.getVal()/cbmean.getVal());
	*/

RooRealVar x("x","x",0.,1.2) ;
 
 RooRealVar cbmean("cbmean", "cbmean" , 0.6, 0.5, 1.) ;
	RooRealVar cbsigma("cbsigma", "cbsigma" , 0.02, 0., 0.5) ;
	RooRealVar cbsig("cbsig", "cbsignal", intg/2, 0, intg);
	RooRealVar n("n","", 2.,0.,5.);
	RooRealVar alpha("alpha","", 1.,0.,4.);
 
 // Create a binned dataset that imports contents of TH1 and associates its contents to observable 'x'
  RooDataHist dh("dh","dh",x,Import(*histo)) ;
	
	//Create fit functions
	RooCBShape cball("cball", "crystal ball", x, cbmean, cbsigma, alpha, n);
	
	//Fit
	cball.fitTo(dh,Range(0.05,1.2));
	
	// Make plot of binned dataset showing Poisson error bars (RooFit default)
  RooPlot* frame = x.frame() ;
  dh.plotOn(frame) ; 
	cball.plotOn(frame, LineColor(kBlue));	
	frame->Draw();

	printf("sigE = %f mu=%f sigE/mu = %f \n", cbsigma.getVal(),cbmean.getVal(),cbsigma.getVal()/cbmean.getVal());
		
return;
}