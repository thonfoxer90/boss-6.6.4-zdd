#include <TEveManager.h>
#include <TEveEventManager.h>
#include <TEveVSD.h>
#include <TEveVSDStructs.h>

#include <TEveTrack.h>
#include <TEveLine.h>
#include <TEveTrackPropagator.h>
#include <TEveGeoShape.h>

#include <TGTab.h>
#include <TGButton.h>


#include <TFile.h>
#include <TDirectory.h>
#include <TMath.h>
#include <TDatabasePDG.h>
#include <TParticlePDG.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TSpline.h>
#include <TPolyLine3D.h>
#include <TVector3.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TView.h>
#include <TAxis3D.h>
#include <TPolyMarker3D.h>
#include <TImage.h>
#include <TASImage.h>
#include <TSystem.h>
#include <TPRegexp.h>
#include <TRandom.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

#ifdef __MAKECINT__
#pragma link C++ class vector<string>+;
#endif

using namespace std;

// Include componets -- compile time link :)

#include "MultiView.c"
MultiView* gMultiView = 0;

class TVSDReader
{
public:
   // ----------------------------------------------------------
   // File / Event Data
   // ----------------------------------------------------------

   TFile      *m_file;
	 TTree* m_tree;
   TDirectory *m_directory;

   TObjArray  *m_evDirKeys;

   TEveVSD    *m_VSD;

   Int_t       m_MaxEv, m_CurEv;

		int m_evtID;

		double m_zdd_energy;
		double m_zddLeft_energy;
		double m_zddRight_energy;

		std::vector<int> *m_vectrackIDStep = 0;
		TBranch *m_bvectrackIDStep=0;

		std::vector<int> *m_vecparentIDStep = 0;
		TBranch *m_bvecparentIDStep=0;

		std::vector<int> *m_vecPDGCodeStep = 0;
		TBranch *m_bvecPDGCodeStep=0;

		std::vector<double> *m_vecXPreStep = 0;
		TBranch *m_bvecXPreStep=0;

		std::vector<double> *m_vecYPreStep = 0;
		TBranch *m_bvecYPreStep=0;

		std::vector<double> *m_vecZPreStep = 0;
		TBranch *m_bvecZPreStep=0;

		std::vector<double> *m_vecXPostStep = 0;
		TBranch *m_bvecXPostStep=0;

		std::vector<double> *m_vecYPostStep = 0;
		TBranch *m_bvecYPostStep=0;

		std::vector<double> *m_vecZPostStep = 0;
		TBranch *m_bvecZPostStep=0;

		std::vector<double> *m_vecPxPreStep = 0;
		TBranch *m_bvecPxPreStep=0;

		std::vector<double> *m_vecPyPreStep = 0;
		TBranch *m_bvecPyPreStep=0;

		std::vector<double> *m_vecPzPreStep = 0;
		TBranch *m_bvecPzPreStep=0;

		std::vector<double> *m_vecPxPostStep = 0;
		TBranch *m_bvecPxPostStep=0;

		std::vector<double> *m_vecPyPostStep = 0;
		TBranch *m_bvecPyPostStep=0;

		std::vector<double> *m_vecPzPostStep = 0;
		TBranch *m_bvecPzPostStep=0;

		std::vector< std::string > *m_vecVolPreStep = 0;
		TBranch *m_bvecVolPreStep=0;

		std::vector< std::string > *m_vecVolPostStep = 0;
		TBranch *m_bvecVolPostStep=0;

		std::vector< double > *m_vecETotPreStep = 0;
		TBranch *m_bvecETotPreStep=0;

		std::vector< double > *m_vecETotPostStep = 0;
		TBranch *m_bvecETotPostStep=0;

		std::vector< double > *m_vecEDepStep = 0;
		TBranch *m_bvecEDepStep=0;

		std::map<int,std::vector<TVector3> > m_mapTrackPoints;
		std::map<int,std::vector<TVector3> > m_mapTrackMomentum;
		std::map<int,int> m_mapTrackPDG;
		std::map<int,int> m_mapTrackPrimary;

		std::vector<TPolyLine3D> m_vTrajectories;
		std::vector<int> m_vTrajPDGs;
		std::vector<int> m_vTrajPrimary;
		std::map<int,int> m_mapColor;

		std::string m_outpath;

		std::string m_option;

		TDatabasePDG* m_pdg;

		const double m_WorldXMin=-500.;
		const double m_WorldXMax=500.;

		const double m_WorldYMin=-100.;
		const double m_WorldYMax=100.;

		const double m_WorldZMin=-5000.;
		const double m_WorldZMax=5000.;

   // ----------------------------------------------------------
   // Event visualization structures
   // ----------------------------------------------------------

   TEveTrackList *fTrackList;

public:
   TVSDReader(string inputname, string option, string outpath, int NEntries=-1) :
      m_file(0), m_directory(0), m_evDirKeys(0),
      m_VSD(0),

      m_MaxEv(-1), m_CurEv(-1),

      fTrackList(0)
   {
	 		/*
      m_file = TFile::Open(inputname.c_str());
      if (!m_file)
      {
         Error("VSD_Reader", "Can not open file '%s' ... terminating.",
               inputname.c_str());
         gSystem->Exit(1);
      }

      m_evDirKeys = new TObjArray;
      TPMERegexp name_re("Event\\d+");
      TObjLink* lnk = m_file->GetListOfKeys()->FirstLink();
      while (lnk)
      {
         if (name_re.Match(lnk->GetObject()->GetName()))
         {
            m_evDirKeys->Add(lnk->GetObject());
         }
         lnk = lnk->Next();
      }

      m_MaxEv = m_evDirKeys->GetEntriesFast();
      if (m_MaxEv == 0)
      {
         Error("VSD_Reader", "No events to show ... terminating.");
         gSystem->Exit(1);
      }

      m_VSD = new TEveVSD;
			*/
			
			
			gStyle->SetLabelSize(0.030, "x");
			gStyle->SetLabelSize(0.030, "y");
			gStyle->SetLabelSize(0.030, "z");

			gStyle->SetTitleSize(0.045, "x");
			gStyle->SetTitleSize(0.045, "y");
			gStyle->SetTitleSize(0.045, "z");

			m_option=option;

			m_pdg = new TDatabasePDG();
			
			m_mapColor[-11] = 2;
			m_mapColor[11] = 4;
			m_mapColor[22] = 3;

			if(inputname.find(".root") != string::npos){
			TFile* file = TFile::Open(inputname.c_str());
			m_tree= (TTree*) file->Get("Impact_reco");
			}
			else if(inputname.find(".txt") != string::npos){
			TChain* chain = new TChain("Impact_reco");
			ifstream readList(inputname.c_str());
			if(readList.is_open()){
				while(readList.good()){
				string strline;
				getline(readList,strline);
				chain->Add(strline.c_str());
				}
			}
			m_tree = (TTree*) chain;
			}
			
			m_MaxEv=m_tree->GetEntries();
			cout << "Number of events " << m_MaxEv << endl;
			
			m_tree->SetBranchAddress("evtID",&m_evtID);
			m_tree->SetBranchAddress("zdd_energy",&m_zdd_energy);
			m_tree->SetBranchAddress("zddLeft_energy",&m_zddLeft_energy);
			m_tree->SetBranchAddress("zddRight_energy",&m_zddRight_energy);

			m_tree->SetBranchAddress("trackIDStep",&m_vectrackIDStep,&m_bvectrackIDStep );
			m_tree->SetBranchAddress("parentIDStep",&m_vecparentIDStep,&m_bvecparentIDStep );
			m_tree->SetBranchAddress("PDGCodeStep",&m_vecPDGCodeStep,&m_bvecPDGCodeStep );

			m_tree->SetBranchAddress("xPreStep",&m_vecXPreStep,&m_bvecXPreStep);
			m_tree->SetBranchAddress("yPreStep",&m_vecYPreStep,&m_bvecYPreStep);
			m_tree->SetBranchAddress("zPreStep",&m_vecZPreStep,&m_bvecZPreStep);

			m_tree->SetBranchAddress("xPostStep",&m_vecXPostStep,&m_bvecXPostStep);
			m_tree->SetBranchAddress("yPosttep",&m_vecYPostStep,&m_bvecYPostStep);
			m_tree->SetBranchAddress("zPostStep",&m_vecZPostStep,&m_bvecZPostStep);

			m_tree->SetBranchAddress("PxPreStep",&m_vecPxPreStep,&m_bvecPxPreStep);
			m_tree->SetBranchAddress("PyPreStep",&m_vecPyPreStep,&m_bvecPyPreStep);
			m_tree->SetBranchAddress("PzPreStep",&m_vecPzPreStep,&m_bvecPzPreStep);

			m_tree->SetBranchAddress("PxPostStep",&m_vecPxPostStep,&m_bvecPxPostStep);
			m_tree->SetBranchAddress("PyPostStep",&m_vecPyPostStep,&m_bvecPyPostStep);
			m_tree->SetBranchAddress("PzPostStep",&m_vecPzPostStep,&m_bvecPzPostStep);

			m_tree->SetBranchAddress("VolumePreStep",&m_vecVolPreStep,&m_bvecVolPreStep);
			m_tree->SetBranchAddress("VolumePostStep",&m_vecVolPostStep,&m_bvecVolPostStep);

			m_tree->SetBranchAddress("ETotPreStep",&m_vecETotPreStep,&m_bvecETotPreStep);
			m_tree->SetBranchAddress("ETotPostStep",&m_vecETotPostStep,&m_bvecETotPostStep);

			m_tree->SetBranchAddress("eDepStep",&m_vecEDepStep,&m_bvecEDepStep);
   }

	 void Initialize()
	{
		
		m_mapTrackPoints.clear();
		m_mapTrackMomentum.clear();
		m_mapTrackPDG.clear();

		m_vTrajectories.clear();
		m_vTrajPDGs.clear();
		m_vTrajPrimary.clear();
	}

   virtual ~TVSDReader()
   {
      // Destructor.

      DropEvent();

      delete m_VSD;
      delete m_evDirKeys;

      m_file->Close();
      delete m_file;
   }

   void AttachEvent()
   {
      // Attach event data from current directory.

      m_VSD->LoadTrees();
      m_VSD->SetBranchAddresses();
   }

   void DropEvent()
   {
      // Drup currently held event data, release current directory.

      // Drop old visualization structures.

      gEve->GetViewers()->DeleteAnnotations();
      gEve->GetCurrentEvent()->DestroyElements();

      // Drop old event-data.

      m_VSD->DeleteTrees();
      delete m_directory;
      m_directory = 0;
   }

   //---------------------------------------------------------------------------
   // Event navigation
   //---------------------------------------------------------------------------

   void NextEvent()
   {
      GotoEvent(m_CurEv + 1);
   }

   void PrevEvent()
   {
      GotoEvent(m_CurEv - 1);
   }

   Bool_t GotoEvent(Int_t ev)
   {
	 		
      if (ev < 0 || ev >= m_MaxEv)
      {
         Warning("GotoEvent", "Invalid event id %d.", ev);
         return kFALSE;
      }
			
			
			/*
      DropEvent();
			
      // Connect to new event-data.

      m_CurEv = ev;
      m_directory = (TDirectory*) ((TKey*) m_evDirKeys->At(m_CurEv))->ReadObj();
      m_VSD->SetDirectory(m_directory);
			
      AttachEvent();
			
      // Load event data into visualization structures.
			
			//Currently Segmentation violation after exiting alive_vsd() if LoadClusters() left uncommented
			//Error : "Error in <TGLLockable::TakeLock>: 'TGLSceneBase Geometry scene' unable to take ModifyLock, already DrawLock"
			//Possible solutions :
			//-fDLCache = kFALSE; in the constructor of TPointSet3DGL.h
			*/
			
			
			m_CurEv = ev;
			cout << "Event #" << m_CurEv << endl;
			gEve->GetViewers()->DeleteAnnotations();
      gEve->GetCurrentEvent()->DestroyElements();
			
			m_tree->GetEntry(m_CurEv);
			Initialize();
			printf("evtID = %d \n",m_evtID);
			printf("evtID = %d m_vecXPreStep->size() = %d m_zdd_energy = %f \n",m_evtID, m_vecXPreStep->size(),m_zdd_energy);
			
      //LoadEsdTracks();
			//MakeRandSphereTracks();
			LoadTracks();
			
      // Fill projected views.

      TEveElement* top = gEve->GetCurrentEvent();

      //gMultiView->DestroyEventRPhi();
      //gMultiView->ImportEventRPhi(top);
			
      //gMultiView->DestroyEventRhoZ();
      //gMultiView->ImportEventRhoZ(top);
			
			gEve->FullRedraw3D(kTRUE);
			
      return kTRUE;
   }


   Bool_t trackIsOn(TEveTrack* t, Int_t mask)
   {
      // Check is track-flag specified by mask are set.

      return (t->GetStatus() & mask) > 0;
   }

	 void MakeRandSphereTracks(){
      
	 	for (Int_t n = 0; n < 2; ++n)
      {		
				 TPolyLine3D l = makeTPolyLine3D(65539+m_CurEv+n);
				 TEveLine* eveLine = makeTEveLine(l);
				 int colorCode=2+n;
				 eveLine->SetLineColor(colorCode);
				 gEve->AddElement(eveLine);
      }
			
	 }
	 
	 TPolyLine3D makeTPolyLine3D(UInt_t seed=65539){
		const Int_t n = 500;
		TRandom* r = new TRandom(seed);
		Double_t x, y, z;
		TPolyLine3D l(n);
		for (Int_t i=0;i<n;i++) {
  		r->Sphere(x, y, z, 1);
  		l.SetPoint(i,x+1,y+1,z+1);
		}
		return l;
	}

	 TPolyLine3D MakeTPolyLine3D(std::vector<TVector3> traj){
			TPolyLine3D poly3D(traj.size());
			for(int k=0;k<traj.size();k++)
			{
			poly3D.SetPoint(k,traj[k].X(),traj[k].Y(),traj[k].Z());
			}
			return poly3D;	
	 }
	 
	 TEveLine* makeTEveLine(const TPolyLine3D& poly3D){
		int n = poly3D.GetN();
		Float_t* vecP = poly3D.GetP();

		TEveLine *l = new TEveLine();
		for (Int_t i=0;i<n;i++) {
  		int iX = i*3;
			int iY = i*3+1;
			int iZ = i*3+2;
  		l->SetNextPoint(vecP[iX],vecP[iY],vecP[iZ]);
		}
		return l;
		}

	 void LoadTracks(){

		if(m_pdg==NULL) cout << "m_pdg NULL" << endl;
	 	for(int step=0;step<m_vecXPreStep->size();step++){
		
			bool changeTrack= step>0 && m_vectrackIDStep->at(step-1)!=m_vectrackIDStep->at(step);			
			int trackID = m_vectrackIDStep->at(step);
			int parentID = m_vecparentIDStep->at(step);
			int PDG = m_vecPDGCodeStep->at(step);
			if(m_pdg->GetParticle(PDG)==NULL) continue; //PDG=1000822050 (Pb205) and 1000070150 (N15) appears in NTuple and is not included in TDatabasePDG
			
			double mass = 	(m_pdg->GetParticle(PDG)->Mass())*1e3;	
			
			TVector3 vPreStep(m_vecXPreStep->at(step),m_vecYPreStep->at(step),m_vecZPreStep->at(step));	
			TVector3 vPostStep(m_vecXPostStep->at(step),m_vecYPostStep->at(step),m_vecZPostStep->at(step));
			TVector3 vPreStepMom(m_vecPxPreStep->at(step),m_vecPyPreStep->at(step),m_vecPzPreStep->at(step));
			TLorentzVector lvPreStep;
			lvPreStep.SetVectM(vPreStepMom,mass);
			string volPreStep = m_vecVolPreStep->at(step);
			string volPostStep = m_vecVolPostStep->at(step);

			double ETotPreStep = m_vecETotPreStep->at(step);
			double ETotPostStep = m_vecETotPostStep->at(step);

			double EDepStep = m_vecEDepStep->at(step);

			bool isPrimary = parentID==0;
		bool fromIP=vPreStep.x()==0. && vPreStep.y()==0. && vPreStep.z()==0.;
			
			//Complete the previous trajectory with its last point at postStep
		if(changeTrack){
			int prevTrackID=m_vectrackIDStep->at(step-1);
			TVector3 vPrevPostStep(m_vecXPostStep->at(step-1),m_vecYPostStep->at(step-1),m_vecZPostStep->at(step-1));
			TVector3 vPrevPostStepMom(m_vecPxPostStep->at(step-1),m_vecPyPostStep->at(step-1),m_vecPzPostStep->at(step-1));
			
			bool inWorld=	(vPrevPostStep.X()>=m_WorldXMin && vPrevPostStep.X()<=m_WorldXMax)
			&& (vPrevPostStep.Y()>=m_WorldYMin && vPrevPostStep.Y()<=m_WorldYMax)
			&& (vPrevPostStep.Z()>=m_WorldZMin && vPrevPostStep.Z()<=m_WorldZMax)
			; 
		
			if(inWorld){
				m_mapTrackPoints[prevTrackID].push_back(vPrevPostStep);
				m_mapTrackMomentum[prevTrackID].push_back(vPrevPostStepMom);
			}
		}
		
		bool inWorld=	(vPreStep.X()>=m_WorldXMin && vPreStep.X()<=m_WorldXMax)
		&& (vPreStep.Y()>=m_WorldYMin && vPreStep.Y()<=m_WorldYMax)
		&& (vPreStep.Z()>=m_WorldZMin && vPreStep.Z()<=m_WorldZMax)
		; 

		if(inWorld)
			{
			//AddPointToTrajectory(trackID,vPreStep);
			m_mapTrackPoints[trackID].push_back(vPreStep);
			m_mapTrackMomentum[trackID].push_back(lvPreStep.Vect());
			m_mapTrackPDG[trackID]=PDG;
			m_mapTrackPrimary[trackID]=isPrimary;
			}
		}
	 
	 for (std::map<int,std::vector<TVector3> > ::iterator it=m_mapTrackPoints.begin(); it!=m_mapTrackPoints.end(); ++it)
		{
			int trackIndex = it->first;
			std::vector<TVector3> traj=it->second;
			
			if(m_option.find("HorizontalZ ") != std::string::npos) for(int k=0;k<traj.size();k++) traj[k].RotateX(-TMath::Pi()/2);
			
			TPolyLine3D poly3D = MakeTPolyLine3D(traj);
						
			m_vTrajectories.push_back(poly3D);		
			m_vTrajPDGs.push_back(m_mapTrackPDG[trackIndex]);
			m_vTrajPrimary.push_back(m_mapTrackPrimary[trackIndex]);	
			
			TEveLine* eveLine = makeTEveLine(poly3D);
			int colorCode=m_mapColor[m_vTrajPDGs.back()];
			eveLine->SetLineColor(colorCode);
			gEve->AddElement(eveLine);	
		}
		
	 }
   ClassDef(TVSDReader, 0);
};

TVSDReader* gVSDReader = 0;


// Forward declaration.
void make_gui();

//______________________________________________________________________________
void BesStepVis(string inputname, string option, string outpath, int NEntries=-1)
{
   // Main function, initializes the application.
   //
   // 1. Load the auto-generated library holding ESD classes and
   //    ESD dictionaries.
   // 2. Open ESD data-files.
   // 3. Load cartoon geometry.
   // 4. Spawn simple GUI.
   // 5. Load first event.

   TFile::SetCacheFileDir(".");

   TEveVSD::DisableTObjectStreamersForVSDStruct();

   gVSDReader = new TVSDReader(inputname,"","",-1);

   TEveManager::Create();

		
   // Standard multi-view
   //=====================

   gMultiView = new MultiView;
	 
   //gMultiView->f3DView->GetGLViewer()->SetStyle(TGLRnrCtx::kOutline);

   //gMultiView->SetDepth(-10);
   //gMultiView->SetDepth(0);

	
   // Final stuff
   //=============
   //gEve->GetDefaultGLViewer()->SetStyle(TGLRnrCtx::kOutline);

   gEve->GetBrowser()->GetTabRight()->SetTab(1);
	
   make_gui();
	
   gEve->AddEvent(new TEveEventManager("Event", "BESIII VSD Event"));
	
   gVSDReader->GotoEvent(0);

	 gEve->FullRedraw3D();
}


//______________________________________________________________________________
void make_gui()
{
   // Create minimal GUI for event navigation.

   TEveBrowser* browser = gEve->GetBrowser();
   browser->StartEmbedding(TRootBrowser::kLeft);

   TGMainFrame* frmMain = new TGMainFrame(gClient->GetRoot(), 1000, 600);
   frmMain->SetWindowName("XX GUI");
   frmMain->SetCleanup(kDeepCleanup);

   TGHorizontalFrame* hf = new TGHorizontalFrame(frmMain);
   {
      TString icondir(TString::Format("%s/icons/", gSystem->Getenv("ROOTSYS")));
      TGPictureButton* b = 0;

      b = new TGPictureButton(hf, gClient->GetPicture(icondir+"GoBack.gif"));
      hf->AddFrame(b);
      b->Connect("Clicked()", "TVSDReader", gVSDReader, "PrevEvent()");

      b = new TGPictureButton(hf, gClient->GetPicture(icondir+"GoForward.gif"));
      hf->AddFrame(b);
      b->Connect("Clicked()", "TVSDReader", gVSDReader, "NextEvent()");
   }
   frmMain->AddFrame(hf);

   frmMain->MapSubwindows();
   frmMain->Resize();
   frmMain->MapWindow();

   browser->StopEmbedding();
   browser->SetTabTitle("Event Control", 0);
}

//______________________________________________________________________________
void SaveViews(const char* out="BesStepVis")
{
TEveViewerList *viewerList = gEve->GetViewers();
	 TEveElement::List_t listchildren = viewerList->RefChildren();	 
	 int counter = 0 ;
	 for (TEveElement::List_i i=listchildren.begin(); i!=listchildren.end(); ++i){
			TGLViewer* glv = ((TEveViewer*)*i)->GetGLViewer();
			//cout << glv->GetName() << endl;
			glv->SetClearColor(1);
			char fname[256];
			sprintf(fname,"png/%s-%d.png",out,counter);
			//glv->SavePicture(fname);
			glv->SavePictureUsingFBO(fname,800,600);
			counter++;
	 }

std::cout << out << endl;	 
char cmd[512];

sprintf(cmd,"convert png/%s-3.png -rotate 90 png/%s-3_rotated.png",out,out);
system(cmd); 
sprintf(cmd,"mv png/%s-3_rotated.png png/%s-3.png",out,out);
system(cmd);		
	
sprintf(cmd,"convert \\( png/%s-0.png png/%s-1.png -append \\) \\\( png/%s-2.png png/%s-3.png -append \\) +append png/%s.png",out,out,out,out,out);
cout << cmd << endl;
system(cmd);   
sprintf(cmd,"rm png/%s-*",out);
system(cmd);
}

//______________________________________________________________________________
void SaveAsMovie(const char* out="BesStepVis",int NEntries=-1 )
{
if(NEntries==-1) NEntries=gVSDReader->m_MaxEv;

for(int i=0;i<NEntries;i++){
	gVSDReader->GotoEvent(i);
	//gEve->FullRedraw3D(kTRUE);
	char prefix[256];
	sprintf(prefix,"%s-%d",out,i);
	SaveViews(prefix);	
}

char cmd[256];
sprintf(cmd,"convert -delay 10 -loop 0 png/%s-*.png %s.gif",out,out);
system(cmd);
sprintf(cmd,"rm png/%s-*",out);
system(cmd);
}