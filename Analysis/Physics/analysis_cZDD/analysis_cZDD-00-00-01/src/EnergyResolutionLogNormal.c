#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TMath.h>
#include <TLatex.h>
#include <TText.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()
    
using namespace std;
/*
Double_t func(Double_t *x, Double_t *par)
{
Double_t sigma0 = (2/2.35)*log(par[1]*(2.35/2)+sqrt(1+pow(par[1]*(2.35/2),2)));
Double_t f=0;
Double_t g=(log(1-(par[1]*(x[0]-par[2])/par[3])))/sigma0;
f=par[0]*(par[1]/(sqrt(2*TMath::Pi())*par[3]*sigma0))
	*exp((-1/2)*(pow(g,2)+pow(sigma0,2)))+par[4];
return f;
}
*/

Double_t func(Double_t *x, Double_t *par)
{
Double_t sigma0 = (2/2.35)*log(par[1]*(2.35/2)+sqrt(1+pow(par[1]*(2.35/2),2)));
Double_t f=0;
Double_t fa=(par[1]/(sqrt(2*TMath::Pi())*par[3]*sigma0));
Double_t fb=1-(par[1]*(x[0]-par[2]))/par[3];
Double_t fc=log(fb);
f=par[0]*fa*exp((-0.5)*((pow(fc,2)/pow(sigma0,2))+pow(sigma0,2)))+par[4];
return f;
}


Double_t func2(Double_t *x, Double_t *par)
{
Double_t f=0;
f=(1/(x[0]*par[1]*sqrt(2*TMath::Pi())))*exp(-0.5*pow((log(x[0]-par[0]))/par[1],2));
return f;
}

double GetFWHM(TH1D* h1)
{
int bin1 = h1->FindFirstBinAbove(h1->GetMaximum()/2);
int bin2 = h1->FindLastBinAbove(h1->GetMaximum()/2);
double fwhm = h1->GetBinCenter(bin2) - h1->GetBinCenter(bin1);
return fwhm;
}

double GetMedian(TH1D* h1)
{
// find median of histogram 
   double prob[] = {0.5}; 
   double q[1]; 
   h1->GetQuantiles(1,q,prob);
   double median = q[0];
	 return median;
}

int EnergyResolution(string inputfile, double Etot, string option, string outputname)
{
gStyle->SetOptFit(1);

TFile* file = TFile::Open(inputfile.c_str());
TTree* tree = (TTree*) file->Get("AllMCTruthTree");

string outps=outputname+".ps";
string outpdf=outputname+".pdf";
string outroot = outputname+".root";

char drawcmd[256];
sprintf(drawcmd,"(zdd_energy/%f)>>h(256,0.,1.2)",Etot);

tree->Draw(drawcmd,"zdd_energy>0");

TH1D* histo = (TH1D*)gROOT->FindObject("h");
histo->SetTitle(";E_{ZDD}/E_{Total}");

double A,eta,mu,sigE,b;
double Emin,Emax;

/*
A = histo->GetEntries()*histo->GetXaxis()->GetBinWidth(1); 
//eta = 0.8;
eta = 1.;
//mu=0.6;
histo->GetXaxis()->SetRangeUser(0.2,1.2);
mu=histo->GetXaxis()->GetBinCenter(histo->GetMaximumBin());
histo->GetXaxis()->SetRangeUser(0.0,1.2);
sigE = GetFWHM(histo)/2.35;
b=0;
//histo->Fit("pol0","","",0.05,0.3);
//TF1* fcst=histo->GetFunction("pol0");
//b=fcst->GetParameter(0);
*/

Emin=0.05;
//Emax=(sigE/eta)+mu;
Emax = histo->GetXaxis()->GetBinCenter(histo->FindLastBinAbove(5.,2));
printf("Emax = %f \n",Emax);

histo->GetXaxis()->SetRangeUser(Emin,1.2);
A = histo->GetEntries()*histo->GetXaxis()->GetBinWidth(1); 
//eta = 0.8;
//eta = 1.;
//mu=0.6;
mu=histo->GetXaxis()->GetBinCenter(histo->GetMaximumBin());
sigE = GetFWHM(histo)/2.35;
eta=sigE/(Emax-mu);
b=0;
/*
histo->Fit("pol0","","",0.05,0.3);
TF1* fcst=histo->GetFunction("pol0");
b=fcst->GetParameter(0);
histo->GetXaxis()->SetRangeUser(0.0,1.2);
Emax = histo->GetXaxis()->GetBinCenter(histo->FindLastBinAbove(b,2));
*/

printf("A=%f eta=%f mu=%f sigE=%f b=%f \n",A,eta,mu,sigE,b);


//TF1 *f1 = new TF1("logNormal","ROOT::Math::lognormal_pdf(x,[0],[1])",0.,1400.);
//TF1 *f1 = new TF1("logNormal",func,0,Emax,5);
TF1 *f1 = new TF1("logNormal",func,Emin,Emax,5);
//TF1 *f1 = new TF1("logNormal",func,0.2,Emax,5);
//TF1 *f1 = new TF1("logNormal",func,0,0.88,5);

f1->SetParName(0,"A");
f1->SetParName(1,"#eta");
f1->SetParName(2,"#mu");
f1->SetParName(3,"#sigma_{E}");
f1->SetParName(4,"b");


f1->SetParameter(0,A);
f1->SetParameter(1,eta);
f1->SetParameter(2,mu);
f1->SetParameter(3,sigE);
f1->SetParameter(4,b);

//f1->SetParLimits(A,0,A*4);
//f1->SetParLimits(sigE,sigE,sigE/4);

histo->Fit(f1,"R N S");

double chi2 = f1->GetChisquare();
int ndf = f1->GetNDF();
A=f1->GetParameter(0);
eta=f1->GetParameter(1);
mu=f1->GetParameter(2);
sigE=f1->GetParameter(3);
b=f1->GetParameter(4);
Emax=(sigE/eta)+mu;
f1->SetRange(Emin,Emax);

f1->Draw("same");

printf("Energy resolution #sigma_{E}/#mu = %.1f \n",sigE/mu);

TLatex* ltx = new TLatex();
ltx->SetNDC();
char strltx[256];
sprintf(strltx,"#chi^{2}/ndf= %.2f",chi2/ndf);
ltx->DrawLatex(0.6,0.75,strltx);
sprintf(strltx,"A= %.2f",A);
ltx->DrawLatex(0.6,0.7,strltx);
sprintf(strltx,"#eta = %.2f",eta);
ltx->DrawLatex(0.6,0.66,strltx);
sprintf(strltx,"#mu = %.2f",mu);
ltx->DrawLatex(0.6,0.62,strltx);
sprintf(strltx,"#sigma_{E}=%.2f",sigE);
ltx->DrawLatex(0.6,0.58,strltx);
sprintf(strltx,"b=%.2f",b);
ltx->DrawLatex(0.6,0.53,strltx);
sprintf(strltx,"#sigma_{E}/#mu=%.2f",sigE/mu);
ltx->DrawLatex(0.6,0.48,strltx);
/*
TF1 *f1 = new TF1("logNormal",func,0,0.84,5);
f1->SetParameter(0,3.63448e+00);
f1->SetParameter(1,5.23001e-01);
f1->SetParameter(2,7.59047e-01);
f1->SetParameter(3,4.82746e-02);
f1->SetParameter(4,4.74540e+00);
histo->Draw();
f1->Draw("same");
*/


/*
TF1 *f1 = new TF1("logNormal",func,0.2,0.89,5);
f1->SetParameter(0,36.25);
f1->SetParameter(1,0.87);
f1->SetParameter(2,0.82);
f1->SetParameter(3,0.068);
f1->SetParameter(4,3.072);

f1->Draw();
//histo->Fit(f1,"R");
*/

/*
// set initial parameters 
   double p[3]; 
   p[0] = histo->GetEntries()*histo->GetXaxis()->GetBinWidth(1);   // area of the histogram 
   
   // find median of histogram 
   double prob[] = {0.5}; 
   double q[1]; 
   histo->GetQuantiles(1,q,prob);
   double median = q[0];
   // find mode of histogram 
   double  mode = histo->GetBinCenter( histo->GetMaximumBin());

   std::cout << "histogram mode is " << mode  << " median is " << median << std::endl;

TF1 *f1 = new TF1("logNormal",func,0.2,0.89,5);
f1->SetParameter(0,p[0]);
f1->SetParameter(1,0.87);
f1->SetParameter(2,p[1]);
f1->SetParameter(3,p[2]);
f1->SetParameter(4,3.072);

histo->Fit(f1,"M R");
*/

/*
TF1 * f1 = new TF1("f1","[0]*ROOT::Math::lognormal_pdf(x,[1],[2] ) " ); 
// set initial parameters 
   double p[3]; 
   p[0] = histo->GetEntries()*histo->GetXaxis()->GetBinWidth(1);   // area of the histogram 
   
   // find median of histogram 
   double prob[] = {0.5}; 
   double q[1]; 
   histo->GetQuantiles(1,q,prob);
   double median = q[0];
   // find mode of histogram 
   double  mode = histo->GetBinCenter( histo->GetMaximumBin());

   std::cout << "histogram mode is " << mode  << " median is " << median << std::endl;

   if (median < 0) { 
      Error("lognormal","not valid histogram median");
      return -1;
   }

   // m is log(median)
   p[1] = log(median); 

   // s2 is  log(median) - log(mode) 

   p[2] = sqrt( log(median/mode) ); 

   f1->SetParameters(p); 
   f1->SetParName(0,"A");
   f1->SetParName(1,"m");
   f1->SetParName(2,"s");
   
   f1->Print();

   histo->Draw();
   std::cout << f1->GetNpar() << std::endl;


   //histo->Fit(f1,"V","",0.2,0.9);	 
	 histo->Fit(f1,"","",0.2,0.9);
*/
	 
/*
TF1 * f1 = new TF1("f1","TMath::LogNormal(x,[0],[1],[2] ) " );
// set initial parameters 
   double p[3]; 
	 double m = histo->GetMean();
	 double v = histo->GetRMS();
	 printf("m=%f v =%f \n", m,v);
	  
   p[0] = sqrt(log(1+(v/(m*m)))); 

   p[1] = log(m/(sqrt(1+(v/(m*m))))); 

		p[2] = histo->GetEntries()*histo->GetXaxis()->GetBinWidth(1);   // area of the histogram 
		
		for(int k=0;k<3;k++) printf("p[%d] = %f \n", k,p[k]);
		
   f1->SetParameters(p); 
   f1->SetParName(0,"#sigma");
   f1->SetParName(1,"#mu");
   f1->SetParName(2,"A");
   

   histo->Draw();

   histo->Fit(f1,"","",0.0,0.9);	

*/
/*
TF1 * f1 = new TF1("f1","[0]*TMath::LogNormal(x,[1],[2],[3] ) " );
// set initial parameters 
   double p[4]; 
	 double m = histo->GetMean();
	 double v = histo->GetRMS();
	 printf("m=%f v =%f \n", m,v);
	  
		p[0] = histo->GetEntries()*histo->GetXaxis()->GetBinWidth(1);   // area of the histogram 
		
   p[1] = sqrt(log(1+(v/(m*m)))); 
	 //p[1] = v; 

   p[2] = log(m/(sqrt(1+(v/(m*m))))); 

		//p[3] = m;	
	p[3] = 0.8;		
		//f1->SetParLimits(3, 0., 1.);
		for(int k=0;k<4;k++) printf("p[%d] = %f \n", k,p[k]);
		
   f1->SetParameters(p); 
   f1->SetParName(0,"A");
   f1->SetParName(1,"#sigma");
   f1->SetParName(2,"#theta");
	 f1->SetParName(3,"m");
   

   histo->Draw();

   histo->Fit(f1,"","",0.0,0.9);
	 
*/
	 	 
/*
TF1 *f1 = new TF1("logNormal",func,0.2,0.89,5);
f1->SetParameter(0,36.25);
f1->SetParameter(1,0.87);
f1->SetParameter(2,0.82);
f1->SetParameter(3,0.068);
f1->SetParameter(4,3.072);

f1->Draw();
*/

/*
TF1 *f1 = new TF1("logNormal",func,0.2,0.89,5);
f1->SetParameter(0,36.25);
f1->SetParameter(1,0.87);
f1->SetParameter(2,0.82);
f1->SetParameter(3,0.068);
f1->SetParameter(4,3.072);
f1->Draw();
histo->Draw("same");
*/

/*
TF1 *f2 = new TF1("logNormal",func2,0.9,2.,2);
f2->SetParameter(0,0.8);
f2->SetParameter(1,0.068);

f2->Draw();
*/
return 0;
}