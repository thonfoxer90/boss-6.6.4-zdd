#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TText.h>
#include <TMath.h>
#include <TLorentzVector.h>
#include <TVector3.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace std;

vector<TFile*> m_files;
vector<TTree*> m_trees;
vector<double> m_scales;
map<string,TH1D*> m_maphistos;

void drawEZdd(string inputcard, string treename, string option, string outpath){

	ifstream readList(inputcard.c_str());
	if(readList.is_open()){
		while(readList.good()){
			string str;
			getline(readList,str);
			cout << str << endl;
			//m_files.push_back(TFile::Open(str.c_str()));
			TFile* file = TFile::Open(str.c_str());
			m_files.push_back(file);
			cout << "TA GUEULE" << endl; 
		}
	}
	/*
	m_trees.resize(m_files.size());
	
	for(int k=0;k<m_trees.size();k++){
		
		m_trees[k] =(TTree*)m_files[k]->Get(treename.c_str());
		string treeHeadername;
		TTree* treeHeader;
		char hname[256];
		sprintf(hname,"h%d",k);
		char drawcmd[256];
		sprintf(drawcmd,"zdd_energy>>h%d(256,0,1500)",hname);
		if(k==0) m_trees[k]->Draw(drawcmd);
		else m_trees[k]->Draw(drawcmd,"","same");
		string key = SSTR(k);
		m_maphistos[key] = (TH1D*) gROOT->FindObject(hname);
		m_maphistos[key]->SetLineColor(k+1);
	}
	*/
	//TCanvas *c = new TCanvas();
}