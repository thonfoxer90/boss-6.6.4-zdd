#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TMath.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

//#pragma link C++ class map<string,TH1D*> +;

const int NMAX=256;
int MCntracks;
int 	 MCpartProp[NMAX];
double MCiniMomRho[NMAX];
double MCiniMomTheta[NMAX];
double MCiniMomPhi[NMAX];
double MCiniE[NMAX];
double MCiniMomRhoCM[NMAX];
double MCiniMomThetaCM[NMAX];
double MCiniMomPhiCM[NMAX];
double MCiniECM[NMAX];
double MCXCalcatZDD[NMAX];
double MCYCalcatZDD[NMAX];
    
using namespace std;

void SetFilledBintoUnity(TH2D* histo){
for(int binx=1 ; binx<=histo->GetNbinsX();binx++){
	for(int biny=1 ;biny<=histo->GetNbinsY();biny++){
	int bincontent = histo->GetBinContent(binx,biny);
	if(bincontent>0) histo->SetBinContent(binx,biny,1);	
	}
}
}

int GetNFilledBins(TH2D* histo){
int nFilledBins=0;
for(int binx=1 ; binx<=histo->GetNbinsX();binx++){
	for(int biny=1 ;biny<=histo->GetNbinsY();biny++){
	int bincontent = histo->GetBinContent(binx,biny);
	if(bincontent>0) nFilledBins++;
	}
}

return nFilledBins;
}

void PhotonAngleCoverage(string inputfile, string option, string outputname)
{
gStyle->SetOptStat(0);
string outps = outputname+".ps";
string outpdf = outputname+".pdf";
string outlog = outputname+".log";

TFile* file = TFile::Open(inputfile.c_str());
TTree* tree = (TTree*) file->Get("AllMCTruthTree");

tree->SetBranchAddress("MCntracks", &MCntracks);
tree->SetBranchAddress("MCpartProp", MCpartProp);
tree->SetBranchAddress("MCiniMomRho", MCiniMomRho);
tree->SetBranchAddress("MCiniMomTheta", MCiniMomTheta);
tree->SetBranchAddress("MCiniMomPhi", MCiniMomPhi);
tree->SetBranchAddress("MCiniE", MCiniE);

tree->SetBranchAddress("MCiniMomRhoCM", MCiniMomRhoCM);
tree->SetBranchAddress("MCiniMomThetaCM", MCiniMomThetaCM);
tree->SetBranchAddress("MCiniMomPhiCM", MCiniMomPhiCM);
tree->SetBranchAddress("MCiniECM", MCiniECM);

tree->SetBranchAddress("MCXCalcatZDD", MCXCalcatZDD);
tree->SetBranchAddress("MCYCalcatZDD", MCYCalcatZDD);

int NBinsPhi=128;
double PhiMin,PhiMax;
PhiMin=-(TMath::Pi());
PhiMax=TMath::Pi();

int NBinsTheta=128;
double ThetaMin,ThetaMax;
ThetaMin=0.;
//ThetaMax=3159.;
ThetaMax=15.;

map<string,TH2D*> hThetavsPhiCM;
hThetavsPhiCM["all West"] = new TH2D("hThetavsPhiCMWest","West;#Phi^{#gamma ISR}_{CM} (rad);#Theta^{#gamma ISR}_{CM} (mrad)",NBinsPhi,PhiMin,PhiMax,NBinsTheta,ThetaMin,ThetaMax);

hThetavsPhiCM["InZDD West"] = new TH2D("hThetavsPhiCMWestZDD","Full ZDD solid angle;#Phi^{#gamma ISR}_{CM} (rad);#Theta^{#gamma ISR}_{CM} (mrad)",NBinsPhi,PhiMin,PhiMax,NBinsTheta,ThetaMin,ThetaMax);
hThetavsPhiCM["InZDD West"]->SetLineColor(3);
hThetavsPhiCM["InZDD West"]->SetMarkerColor(3);

hThetavsPhiCM["validCrystals West"] = new TH2D("hThetavsPhiCMWest104","Valid Crystals;#Phi^{#gamma ISR}_{CM} (rad);#Theta^{#gamma ISR}_{CM} (mrad)",NBinsPhi,PhiMin,PhiMax,NBinsTheta,ThetaMin,ThetaMax);
hThetavsPhiCM["validCrystals West"]->SetLineColor(2);
hThetavsPhiCM["validCrystals West"]->SetMarkerColor(2);

hThetavsPhiCM["all East"] = new TH2D("hThetavsPhiCMEast","East;#Phi^{#gamma ISR}_{CM} (rad);#Theta^{#gamma ISR}_{CM} (mrad)",NBinsPhi,PhiMin,PhiMax,NBinsTheta,TMath::Pi()*1e3-15.,TMath::Pi()*1e3);

hThetavsPhiCM["InZDD East"] = new TH2D("hThetavsPhiCMEastZDD","Full ZDD solid angle;#Phi^{#gamma ISR}_{CM} (rad);#Theta^{#gamma ISR}_{CM} (mrad)",NBinsPhi,PhiMin,PhiMax,NBinsTheta,TMath::Pi()*1e3-15.,TMath::Pi()*1e3);
hThetavsPhiCM["InZDD East"]->SetLineColor(3);
hThetavsPhiCM["InZDD East"]->SetMarkerColor(3);

hThetavsPhiCM["validCrystals East"] = new TH2D("hThetavsPhiCMEast104","Valid Crystals;#Phi^{#gamma ISR}_{CM} (rad);#Theta^{#gamma ISR}_{CM} (mrad)",NBinsPhi,PhiMin,PhiMax,NBinsTheta,TMath::Pi()*1e3-15.,TMath::Pi()*1e3);
hThetavsPhiCM["validCrystals East"]->SetLineColor(2);
hThetavsPhiCM["validCrystals East"]->SetMarkerColor(2);

map<string,TH2D*> hSinThetavsPhiCM;
hSinThetavsPhiCM["InZDD"] = new TH2D("hSinThetavsPhiCMZDD","Full ZDD solid angle;#Phi^{#gamma ISR}_{CM} (rad);sin #Theta^{#gamma ISR}_{CM} (mrad)",NBinsPhi,PhiMin,PhiMax,NBinsTheta,0,1);
hSinThetavsPhiCM["InZDD"]->SetLineColor(3);
hSinThetavsPhiCM["InZDD"]->SetMarkerColor(3);

hSinThetavsPhiCM["validCrystals"] = new TH2D("hSinThetavsPhiCM104","Valid Crystals;#Phi^{#gamma ISR}_{CM} (rad);sin #Theta^{#gamma ISR}_{CM} (mrad)",NBinsPhi,PhiMin,PhiMax,NBinsTheta,0,1);
hSinThetavsPhiCM["validCrystals"]->SetLineColor(2);
hSinThetavsPhiCM["validCrystals"]->SetMarkerColor(2);

map<string, int> N;
N["InZDD"]=0; N["InZDD West"]=0; N["InZDD East"]=0;
N["validCrystals"]=0; N["validCrystals West"]=0; N["validCrystals East"]=0;

for(int entry=0; entry<tree->GetEntries();entry++){
	tree->GetEntry(entry);

	for(int i=0;i<MCntracks;i++){
		if(MCpartProp[i]==22){
			bool InZDD = MCXCalcatZDD[i]>=0. && MCXCalcatZDD[i]<=4. && fabs(MCYCalcatZDD[i])>=0.5 && fabs(MCYCalcatZDD[i])<=3.5; 
			
			bool In104 = MCXCalcatZDD[i]>=3 && MCXCalcatZDD[i]<=4 && MCYCalcatZDD[i]>=0.5 && MCYCalcatZDD[i]<=1.5;
			bool In114 = MCXCalcatZDD[i]>=3 && MCXCalcatZDD[i]<=4 && MCYCalcatZDD[i]>=1.5 && MCYCalcatZDD[i]<=2.5;
			bool In102 = MCXCalcatZDD[i]>=1 && MCXCalcatZDD[i]<=2 && MCYCalcatZDD[i]>=0.5 && MCYCalcatZDD[i]<=1.5;
			bool In112 = MCXCalcatZDD[i]>=1 && MCXCalcatZDD[i]<=2 && MCYCalcatZDD[i]>=1.5 && MCYCalcatZDD[i]<=2.5;
			bool In122 = MCXCalcatZDD[i]>=1 && MCXCalcatZDD[i]<=2 && MCYCalcatZDD[i]>=2.5 && MCYCalcatZDD[i]<=3.5;
			
			bool In204 = MCXCalcatZDD[i]>=3 && MCXCalcatZDD[i]<=4 && MCYCalcatZDD[i]>=-1.5 && MCYCalcatZDD[i]<=-0.5;
			bool In214 = MCXCalcatZDD[i]>=3 && MCXCalcatZDD[i]<=4 && MCYCalcatZDD[i]>=-2.5 && MCYCalcatZDD[i]<=-1.5;
			bool In202 = MCXCalcatZDD[i]>=1 && MCXCalcatZDD[i]<=2 && MCYCalcatZDD[i]>=-1.5 && MCYCalcatZDD[i]<=-0.5;
			bool In212 = MCXCalcatZDD[i]>=1 && MCXCalcatZDD[i]<=2 && MCYCalcatZDD[i]>=-2.5 && MCYCalcatZDD[i]<=-1.5;
			bool In222 = MCXCalcatZDD[i]>=1 && MCXCalcatZDD[i]<=2 && MCYCalcatZDD[i]>=-3.5 && MCYCalcatZDD[i]<=-2.5;
			
			bool InValidCrystals = In104 || In114 || In204 || In214;
			//bool InValidCrystals = In104 || In114 || In204 || In214 || In102 || In112 || In122 || In202 || In212 || In222;
			
			hThetavsPhiCM["all West"]->Fill(MCiniMomPhiCM[i],MCiniMomThetaCM[i]*1e3);
			hThetavsPhiCM["all East"]->Fill(MCiniMomPhiCM[i],MCiniMomThetaCM[i]*1e3);
			
			if(InZDD) {
				hThetavsPhiCM["InZDD West"]->Fill(MCiniMomPhiCM[i],MCiniMomThetaCM[i]*1e3);
				hThetavsPhiCM["InZDD East"]->Fill(MCiniMomPhiCM[i],MCiniMomThetaCM[i]*1e3);
				hSinThetavsPhiCM["InZDD"]->Fill(MCiniMomPhiCM[i],sin(MCiniMomThetaCM[i]));
				N["InZDD"]++;				
			}
			
			if(InValidCrystals) 
				{hThetavsPhiCM["validCrystals West"]->Fill(MCiniMomPhiCM[i],MCiniMomThetaCM[i]*1e3);
				hThetavsPhiCM["validCrystals East"]->Fill(MCiniMomPhiCM[i],MCiniMomThetaCM[i]*1e3);
				hSinThetavsPhiCM["validCrystals"]->Fill(MCiniMomPhiCM[i],sin(MCiniMomThetaCM[i]));
				N["validCrystals"]++;
			}
		}
	}
}

//Calculate solid angle (dOmega=sintheta*dtheta*dphi)
map<string, double> solidAngle;
solidAngle["all"]=4*TMath::Pi();
double solidAngleInZDD=0;
double solidAngleInValidCrystals=0;
map<string,int> nBinsFilled;
nBinsFilled["all"] = NBinsPhi*NBinsTheta;

SetFilledBintoUnity(hSinThetavsPhiCM["InZDD"]);
nBinsFilled["ZDD"]=GetNFilledBins(hSinThetavsPhiCM["InZDD"]);
solidAngle["ZDD"]=nBinsFilled["ZDD"]*solidAngle["all"]/nBinsFilled["all"];

SetFilledBintoUnity(hSinThetavsPhiCM["validCrystals"]);
nBinsFilled["validCrystals"]=GetNFilledBins(hSinThetavsPhiCM["validCrystals"]);
solidAngle["validCrystals"]=nBinsFilled["validCrystals"]*solidAngle["all"]/nBinsFilled["all"];

TCanvas* c = new TCanvas();
c->Print(Form("%s[",outps.c_str())); 	
c->Divide(2,1);
/*
c->cd(1);
hThetavsPhiCMWest["all"]->Draw("");	
hThetavsPhiCMWest["InZDD"]->Draw("same");	
hThetavsPhiCMWest["validCrystals"]->Draw("same");	

c->cd(2);
hThetavsPhiCMEast["all"]->Draw("");	
hThetavsPhiCMEast["InZDD"]->Draw("same");	
hThetavsPhiCMEast["validCrystals"]->Draw("same");
*/

c->cd(1);
hThetavsPhiCM["all West"]->Draw("box");	
hThetavsPhiCM["InZDD West"]->Draw("box same");	
hThetavsPhiCM["validCrystals West"]->Draw("box same");	

c->cd(2);
hThetavsPhiCM["all East"]->Draw("box");	
hThetavsPhiCM["InZDD East"]->Draw("box same");	
hThetavsPhiCM["validCrystals East"]->Draw("box same");

c->Print(Form("%s",outps.c_str()));

c->Clear();
hSinThetavsPhiCM["InZDD"]->Draw("box");
hSinThetavsPhiCM["validCrystals"]->Draw("box same");

c->Print(Form("%s",outps.c_str()));

c->Print(Form("%s]",outps.c_str()));
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

ofstream writeLog(outlog.c_str());

writeLog << "Total number of events = " << tree->GetEntries() <<endl;

writeLog << "ZDD solid angle (East and West side) = " << solidAngle["ZDD"] << " sr ( " << solidAngle["ZDD"]*100/solidAngle["all"] << " \% )" <<endl;
writeLog << "Number of events in ZDD solid angle (East and West side) = " << N["InZDD"] << " (" << (double) N["InZDD"]*100/(double)tree->GetEntries() << "\% )" <<endl;
writeLog << "ZDD valid crystals solid angle (East and West side) = " << solidAngle["validCrystals"] << " sr ( " << solidAngle["validCrystals"]*100/solidAngle["all"] << " \% )" <<endl;
writeLog << "Number of events in valid crystals (East and West side) = " << N["validCrystals"] << " (" << (double) N["validCrystals"]*100/(double)tree->GetEntries() << "\% )" <<endl; 

writeLog << endl;

writeLog << "ZDD solid angle (East/West side) = " << solidAngle["ZDD"] << " sr ( " << solidAngle["ZDD"]*100/solidAngle["all"]/2 << " \% )" <<endl;
writeLog << "Number of events in ZDD solid angle (East/West side) = " << N["InZDD"] << " (" << (double) N["InZDD"]*100/(double)tree->GetEntries()/2 << "\% )" <<endl;
writeLog << "ZDD valid crystals solid angle (East/West side) = " << solidAngle["validCrystals"] << " sr ( " << solidAngle["validCrystals"]*100/solidAngle["all"]/2 << " \% )" <<endl;
writeLog << "Number of events in valid crystals (East/West side) = " << N["validCrystals"] << " (" << (double) N["validCrystals"]*100/(double)tree->GetEntries()/2 << "\% )" <<endl;

}