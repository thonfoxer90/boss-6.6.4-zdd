#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TText.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

//Define crystal center positions
double crystalDX=1;
double crystalDY=1;
int NCrystalsX=4;
int NCrystalsY=3;
double XEdgeOrigTop=0;
double gap=1;
double minEDep=0.;

void DrawZDDcrystals(vector<double> vecX, vector<double> vecY)
{
vector<TLine*> linesY;
vector<TLine*> linesX;
//Y=cst Lines
for(int i=0;i<vecY.size();i++)
	{
	double x0=vecX[0];
	double y0=vecY[i];
	double x1=vecX.back();
	double y1=vecY[i];
	linesY.push_back(new TLine(x0,y0,x1,y1));
	linesY.back()->Draw();
	}
	
//X=cst Lines
for(int i=0;i<vecX.size();i++)
	{
	double x0,y0,x1,y1;
	x0=vecX[i];
	y0=vecY[0];
	x1=vecX[i];
	y1=vecY[NCrystalsY];
	linesX.push_back(new TLine(x0,y0,x1,y1));
	linesX.back()->Draw();
	
	y0=vecY[NCrystalsY+1];
	y1=vecY.back();
	linesX.push_back(new TLine(x0,y0,x1,y1));
	linesX.back()->Draw();
	}
	
}

vector< double>  GetConfidenceInterval(TH1D* histo, double fracintg)
{
vector<double> CI;
CI.resize(3);
//double center = histo->GetMean();
double center = histo->GetXaxis()->GetBinCenter(histo->GetMaximumBin());
int bincenter = histo->FindBin(center);


double intgTot=histo->Integral();
double intg=0;

for(int bin=1;bin<=(histo->GetNbinsX()/2);bin++ )
	{
	intg = histo->Integral(bincenter-bin,bincenter+bin);
	if(intg/intgTot>=fracintg) 
		{
		CI[0]=histo->GetXaxis()->GetBinLowEdge(bincenter-bin);
		CI[1]=histo->GetXaxis()->GetBinUpEdge(bincenter+bin);
		break;
		}
	}

CI[2]=center;
printf("C[0] = %f C[1] = %f \n",CI[0],CI[1]);
return CI;
}
						
int ZddHitRec(string inputfile, string option, string outputname)
{
TFile* file = TFile::Open(inputfile.c_str());
TTree* tree = (TTree*) file->Get("AllMCTruthTree");

if(tree==NULL) {cout << "Tree not found" <<endl; return 1;}
else cout << "Number of entries in tree : " << tree->GetEntries() << endl;

string outps=outputname+".ps";
string outpdf=outputname+".pdf";
string outroot = outputname+".root";

TFile* outfile = new TFile(outroot.c_str(),"RECREATE");

vector<double> stlVLimBinsX;
vector<double> stlVLimBinsY;

//X coordinates
for(int k=0;k<NCrystalsX+1;k++)
	{
	double x=XEdgeOrigTop+k*crystalDX;
	stlVLimBinsX.push_back(x);
	}

//Y coordinates
	//Lower block
	for(int k=0;k<NCrystalsY+1;k++)
	{
	double y=0;
	y=k*crystalDY-(NCrystalsY*crystalDY+gap/2);
	stlVLimBinsY.push_back(y);
	}	
	//gap
	stlVLimBinsY.push_back(gap/2);
	//Upper block
	for(int k=1;k<NCrystalsY+1;k++)
	{
	double y=0;
	y=k*crystalDY+gap/2;
	stlVLimBinsY.push_back(y);

	}

vector<double> vecCrystalXCenter;
vector<double> vecCrystalYCenter;

for(int i=0;i<stlVLimBinsX.size()-1;i++)
	{
	double xcenter = (stlVLimBinsX[i]+stlVLimBinsX[i+1])/2;
	vecCrystalXCenter.push_back(xcenter);
	printf("xcenter = %f \n", xcenter);
		for(int j=0;j<stlVLimBinsY.size()-1;j++)
			{
			if(j==3) continue;
			double ycenter=(stlVLimBinsY[j]+stlVLimBinsY[j+1])/2;
			vecCrystalYCenter.push_back(ycenter);
			printf("ycenter = %f \n", ycenter);
			}
		printf("\n");	
	}
	
	
//Define the list of branch names from NTuple to be read	
vector<string> VarNameList;
for(int k=0;k<(tree->GetListOfBranches())->GetEntries();k++)
		{
		TBranch* br = (TBranch*) (tree->GetListOfBranches())->At(k);
		TLeaf* lf = br->GetLeaf(br->GetName());
		// Leaf name = branch name only if branch has only one leaf.		
		string lfname(lf->GetName());
		string lftypename(lf->GetTypeName());
		Int_t lfLen = lf->GetLen(); // 0 : Array of elements 1 : One element per event
		//printf("lf->GetName() %s lf->GetTypeName() %s lf->GetLen() %d  lf->GetLenStatic() %d \n",lf->GetName(), lf->GetTypeName(),lf->GetLen(),lf->GetLenStatic());
		
		if(lfLen==1){//Only consider leafs with one entry per event
			if(lftypename=="Double_t") VarNameList.push_back(lfname);
		}
		}
	
//Assign for each branch an internal variable where its value will be stored.
	//The internal variable is labeled using the corresponding branch name.
	//Its value can be retrieved as the following ;
	// double variable = ZDDVarList[Name of variable in NTuple]
	map<string, double> VarList;
	for(int k=0;k<VarNameList.size();k++)
		{
		string varname = VarNameList[k];
		VarList[varname]=0;
				
		double& var=(VarList[varname]);
		tree->SetBranchAddress(varname.c_str(),&var);
		
		}


//Initialize variables describing rate for each ZDD crystal
map<string,double > mapEnergy; //Associate a sum of weight variable to string key (key is crystal number)

for(int k=0;k<VarNameList.size();k++)
		{
		string key = VarNameList[k];
		//cout << key << endl;
		//if(key.find("eDep") != string::npos){
		if(key.find("eDep") != string::npos && key.find("Right") != string::npos){
			//If variable name contains "eDep"
			mapEnergy[key]=0.;
			}								
		}	

TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str()));
TH1D* hZDDEnergy = new TH1D("hZDDEnergy",";E_{ZDD} (MeV);",256,0.,2000.);
TH2D* hHitXY = new TH2D("hHitXY",";X(cm);Y(cm)",256,-1.,5.,256,-5.,5.);
//TH2D* hHitRecXY = new TH2D("hHitRecXY ",";X_{rec}(cm);Y_{rec}(cm)",256,-1.,5.,256,-5.,5.);
TH2D* hHitRecXY = new TH2D("hHitRecXY ",";X_{rec}(cm);Y_{rec}(cm)",64,-1.,5.,64,-5.,5.);
TH1D* hDelta_X_RecX = new TH1D("hDelta_X_RecX",";X-X_{rec}(cm)",512,-10.,10.);
TH1D* hDelta_Y_RecY = new TH1D("hDelta_Y_RecY",";Y-Y_{rec}(cm)",512,-10.,10.);
TH1D* hDelta_R_RecR = new TH1D("hDelta_R_RecR",";R-R_{rec}(cm)",1024,-10.,10.);

for(int entry=0;entry<tree->GetEntries();entry++)
	{
	
	tree->GetEntry(entry);
	
	if(VarList["zdd_energy"]==0) continue;
	double hitRecX=0.;
	double hitRecY=0.;
	double hitRecR=0.;
	double ZDDenergy=0;
	
	//	for(std::map<string,double>::iterator it = VarList.begin();it!=VarList.end();++it){
	//	string key = it->first;
	//	printf("%s %f \n", key.c_str(),VarList[key]);
	//	
	//	}
	//printf("\n");	
	
	double sumweight=0;
	double xGravityCenter=0;
	double yGravityCenter=0;
	
	for(std::map<string,double>::iterator it = mapEnergy.begin();it!=mapEnergy.end();++it){
		string key = it->first;
		mapEnergy[key]=VarList[key];
		//printf("%s %f \n", key.c_str(),mapEnergy[key]);
		double weight;
		//if(mapEnergy[key]<=50) continue;
		if(mapEnergy[key]<=10) continue;
		if(option.find("LogWeightAlg")!=std::string::npos)
			{
			double W0=3.9;
			double eT=0;
			double crystalEnergy=mapEnergy[key];
			for(std::map<string,double>::iterator it2 = mapEnergy.begin();it2!=mapEnergy.end();++it2)
				{
				string key2 = it2->first;
				double energy=VarList[key2];
				eT=eT+energy;
				}
			//printf("crystalEnergy %f eT %f \n",crystalEnergy,eT);
			if((W0+log(crystalEnergy/eT))<=0) weight = 0;
			else weight = W0+log(crystalEnergy/eT);
			}
		else weight = mapEnergy[key];
		
		//if(weight>100) continue;
		int i,j;
		if(key=="eDep221Right") {i=0;j=0;}
		if(key=="eDep222Right") {i=1;j=0;}
		
		if(key=="eDep223Right") {i=2;j=0;}
		if(key=="eDep224Right") {i=3;j=0;}
		if(key=="eDep211Right") {i=0;j=1;}
		if(key=="eDep212Right") {i=1;j=1;}
		if(key=="eDep213Right") {i=2;j=1;}
		if(key=="eDep214Right") {i=3;j=1;}
		if(key=="eDep201Right") {i=0;j=2;}
		if(key=="eDep202Right") {i=1;j=2;}
		if(key=="eDep203Right") {i=2;j=2;}
		if(key=="eDep204Right") {i=3;j=2;}
		if(key=="eDep101Right") {i=0;j=3;}
		if(key=="eDep102Right") {i=1;j=3;}
		if(key=="eDep103Right") {i=2;j=3;}
		if(key=="eDep104Right") {i=3;j=3;}
		if(key=="eDep111Right") {i=0;j=4;}
		if(key=="eDep112Right") {i=1;j=4;}
		if(key=="eDep113Right") {i=2;j=4;}
		if(key=="eDep114Right") {i=3;j=4;}
		if(key=="eDep121Right") {i=0;j=5;}
		if(key=="eDep122Right") {i=1;j=5;}
		if(key=="eDep123Right") {i=2;j=5;}
		if(key=="eDep124Right") {i=3;j=5;}
		
		//printf("%d %d \n",vecCrystalXCenter.size(),vecCrystalYCenter.size());
		double xcenter = vecCrystalXCenter[i];
		double ycenter = vecCrystalYCenter[j];
		
		xGravityCenter=xGravityCenter+weight*xcenter;
		yGravityCenter=yGravityCenter+weight*ycenter;
		
		sumweight = sumweight+weight;
		
		ZDDenergy=ZDDenergy+mapEnergy[key];
		}
	hitRecX=xGravityCenter/sumweight;	
	hitRecY=yGravityCenter/sumweight;
	hitRecR=sqrt(hitRecX*hitRecX+hitRecY*hitRecY);	
	
	hZDDEnergy->Fill(ZDDenergy);
	hHitRecXY->Fill(hitRecX,hitRecY);
		
	//printf("\n");	
	
	//Original impact point
	double xImpact,yImpact,rImpact;
	xImpact=VarList["xImpact"];
	yImpact=VarList["yImpact"];
	rImpact=sqrt(xImpact*xImpact+yImpact*yImpact);
	
	hHitXY->Fill(xImpact,yImpact);
	
	//Reconstruction resolution
	hDelta_X_RecX->Fill(hitRecX-xImpact);
	hDelta_Y_RecY->Fill(hitRecY-yImpact);
	hDelta_R_RecR->Fill(hitRecR-rImpact);
	
	}

hZDDEnergy->Draw();
c->Print(Form("%s",outps.c_str()));	

gStyle->SetNumberContours(20);
//hHitRecXY->Draw("colz");
hHitRecXY->Draw("contz");
gPad->SetLogz();
DrawZDDcrystals(stlVLimBinsX, stlVLimBinsY);	
c->Print(Form("%s",outps.c_str()));		

c->Clear();
hHitXY->Draw("colz");
DrawZDDcrystals(stlVLimBinsX, stlVLimBinsY);
c->Print(Form("%s",outps.c_str()));

c->Clear();
hDelta_X_RecX->Draw("");
c->Print(Form("%s",outps.c_str()));

c->Clear();
hDelta_Y_RecY->Draw("");
c->Print(Form("%s",outps.c_str()));

c->Clear();
hDelta_R_RecR->Draw("");
vector<double> CI = GetConfidenceInterval(hDelta_R_RecR,0.90);
TLine* lCILow = new TLine(CI[0],hDelta_R_RecR->GetMinimum(),CI[0],hDelta_R_RecR->GetMaximum());
lCILow->SetLineColor(2);
TLine* lCIUp = new TLine(CI[1],hDelta_R_RecR->GetMinimum(),CI[1],hDelta_R_RecR->GetMaximum());
lCIUp->SetLineColor(2);
TLine* lCICenter = new TLine(CI[2],hDelta_R_RecR->GetMinimum(),CI[2],hDelta_R_RecR->GetMaximum());
lCICenter->SetLineColor(2);
lCICenter->SetLineStyle(2);
lCILow->Draw();
lCIUp->Draw();
lCICenter->Draw();
TLatex* ltx = new TLatex();
ltx->SetNDC();
char strltx[256];
sprintf(strltx,"CI@90% = %.2f #pm %.2f\n",CI[2],(CI[1]-CI[0])/2);
ltx->DrawLatex(0.15,0.75,strltx);
c->Print(Form("%s",outps.c_str()));

c->Print(Form("%s]",outps.c_str()));		
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

outfile->Write();
			
return 0;
}						