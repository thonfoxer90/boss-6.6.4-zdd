#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TText.h>
#include <TMath.h>
#include <TLorentzVector.h>
#include <TVector3.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace std;

vector<TFile*> m_files;
vector<TH1D*> m_histos;
vector<int> m_colors;
vector<string> m_descriptions;

double GetRadius(double x, double y){
	double r=0;
	r=sqrt(x*x+y*y);
	return r;
}

void drawR(string inputcard, string treename,string selection,string outpath){
	ifstream readList(inputcard.c_str());
	if(readList.is_open()){
		while(readList.good()){
			string str;
			getline(readList,str);
			m_files.push_back(TFile::Open(str.c_str()));
			getline(readList,str);
			int color =atoi(str.c_str());
			m_colors.push_back(color);
			getline(readList,str);
			m_descriptions.push_back(str);
		}
	}
	
	for(int i=0;i<m_files.size();i++){
		

		TTree* tree = (TTree*)m_files[i]->Get(treename.c_str());
		double XZdd;
		double YZdd;
		
		tree->SetBranchAddress("XZddRecHit", &XZdd);
		tree->SetBranchAddress("YZddRecHit", &YZdd);

		//tree->SetBranchAddress("XZddHitMiss4PiCX",&XZdd);
		//tree->SetBranchAddress("YZddHitMiss4PiCX",&YZdd);
		
		char hname[256];
		sprintf(hname,"histo-%d",i);
		TH1D* histo = new TH1D(hname,";R (cm);",128,0.,10.);
		histo->SetLineColor(m_colors[i]);
		
		for(int entry=0;entry<tree->GetEntries();entry++){
			tree->GetEntry(entry);
			double rXY=GetRadius(XZdd,YZdd);			
			histo->Fill(rXY);
		}		
		m_histos.push_back(histo);
	}
TCanvas *c = new TCanvas();
	for(int k=0;k<m_histos.size();k++){
		if(k==0)m_histos[k]->DrawNormalized();
		else m_histos[k]->DrawNormalized("SAME");
	}	
}