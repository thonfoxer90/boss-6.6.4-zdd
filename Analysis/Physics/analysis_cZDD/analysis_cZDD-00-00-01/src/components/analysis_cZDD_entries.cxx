#include "GaudiKernel/DeclareFactoryEntries.h"
#include "analysis_cZDD/analysis_cZDD.h"

DECLARE_ALGORITHM_FACTORY( analysis_cZDD )

DECLARE_FACTORY_ENTRIES( analysis_cZDD ) {
  DECLARE_ALGORITHM(analysis_cZDD);
}

