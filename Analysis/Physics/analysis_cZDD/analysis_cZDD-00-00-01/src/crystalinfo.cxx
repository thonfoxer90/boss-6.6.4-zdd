#include "analysis_cZDD/crystalinfo.h"

Crystaldata :: Crystaldata()
{
  m_partID = 0;
  m_crystalNo = 0;
  m_chargeChannel = 0;
  m_timeChannel = 0;
  m_energy = 0.;
  m_time = 0.;
}

Crystaldata :: Crystaldata(int partID, int crystalNo, int chargeChannel, int timeChannel, double energy, double time)
{
  m_partID = partID;
  m_crystalNo = crystalNo;
  m_chargeChannel = chargeChannel;
  m_timeChannel = timeChannel;
  m_energy = energy;
  m_time = time;
}


