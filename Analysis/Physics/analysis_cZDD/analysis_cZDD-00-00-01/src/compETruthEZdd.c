#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TText.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

double XZddRecHit;
double YZddRecHit;

double zdd_energy;
double zddLeft_energy;
double zddRight_energy;

double GamISRMCTruthE;

const int NMAX=256;
int MCntracks;
int 	 MCpartProp[NMAX];
double MCiniE[NMAX];
double MCXCalcatZDD[NMAX];
double MCYCalcatZDD[NMAX];

map<string,TH1D*> m_maphETruth;
map<string,TH1D*> m_maphETruthZdd;
map<string,TH1D*> m_maphEZdd;
map<string,TH1D*> m_maphEZddOverETruth;
map<string,TH1D*> m_maphDeltaETruthEZdd;
map<string,TH2D*> m_maphEZddvsETruth;
map<string,TH2D*> m_maphEZddOverETruthvsETruth;
						
void compETruthEZdd(string inputname, string treename, string option, string outpath){
	TFile* infile = TFile::Open(inputname.c_str());
	TTree* tree = (TTree*) infile->Get(treename.c_str());

	string outpdf = outpath+".pdf";
	string outROOT = outpath+".root";
	
	TFile* outfile = new TFile(Form("%s",outROOT.c_str()),"RECREATE");
	
	tree->SetBranchAddress("zdd_energy",&zdd_energy);
	tree->SetBranchAddress("zddRight_energy",&zddRight_energy);
	tree->SetBranchAddress("zdd_energy",&zdd_energy);

	if(treename=="AllMCTruthTree"){
		tree->SetBranchAddress("MCntracks", &MCntracks);
		tree->SetBranchAddress("MCpartProp", MCpartProp);
		tree->SetBranchAddress("MCXCalcatZDD", MCXCalcatZDD);
		tree->SetBranchAddress("MCYCalcatZDD", MCYCalcatZDD);
		tree->SetBranchAddress("MCiniE", MCiniE);
	}
	vector<string> keys;
	keys.resize(25);
	keys[0]="all";

	keys[1]="121";keys[2]="122";keys[3]="123";keys[4]="124";
	keys[5]="111";keys[6]="112";keys[7]="113";keys[8]="114";
	keys[9]="101";keys[10]="102";keys[11]="103";keys[12]="104";

	keys[13]="201";keys[14]="202";keys[15]="203";keys[16]="204";
	keys[17]="211";keys[18]="212";keys[19]="213";keys[20]="214";
	keys[21]="221";keys[22]="222";keys[23]="223";keys[24]="224";
	
	for(int k=0;k<keys.size();k++){
		string hname,htitle;
		hname="hETruth"+keys[k];
		htitle=";E^{True Lab}_{ISR} [GeV];";
		m_maphETruth[keys[k]] = new TH1D(hname.c_str(),htitle.c_str(),256,0.,2.);
		hname="hETruthZdd"+keys[k];
		htitle=";E^{True Lab}_{ISR} [GeV];";
		m_maphETruthZdd[keys[k]] = new TH1D(hname.c_str(),htitle.c_str(),256,0.,2.);
		hname="hEZdd"+keys[k];
		htitle=";E^{ZDD}_{Deposited} [GeV];";
		m_maphEZdd[keys[k]] = new TH1D(hname.c_str(),htitle.c_str(),256,0.,2.);
		hname="hEZddOverETruth"+keys[k];
		htitle=";E^{ZDD}_{Deposited}/E^{True Lab};";
		m_maphEZddOverETruth[keys[k]] = new TH1D(hname.c_str(),htitle.c_str(),256,0.,1.2);
		hname="hDeltaETruthEZdd"+keys[k];
		htitle=";E^{True Lab}_{ISR}-E^{ZDD}_{Deposited}/E^{True Lab};";
		m_maphDeltaETruthEZdd[keys[k]] = new TH1D(hname.c_str(),htitle.c_str(),256,-0.1,2.);
		hname="hEZddvsETruth"+keys[k];
		htitle=";E^{True Lab}[GeV];E^{Zdd} [GeV]";
		m_maphEZddvsETruth[keys[k]] = new TH2D(hname.c_str(),htitle.c_str(),256,0.,2.,256,0.,2.);
		hname="hEZddOverETruthvsETruth"+keys[k];
		htitle=";E^{True Lab}[GeV];E^{Zdd}/E^{True Lab}";
		m_maphEZddOverETruthvsETruth[keys[k]] = new TH2D(hname.c_str(),htitle.c_str(),256,0.,2.,256,0.,1.2);
		
	}
	for(int entry=0;entry<tree->GetEntries();entry++){
			tree->GetEntry(entry);
			if(treename=="AllMCTruthTree"){
				double ETruth=0;
				double XZddISR=0;
				double YZddISR=0;
								
				for(int i=0;i<MCntracks;i++) {
					if(MCpartProp[i]==22) {
						ETruth=MCiniE[i];
						XZddISR=MCXCalcatZDD[i];
						YZddISR=MCYCalcatZDD[i];
					}
				}
				
				m_maphETruth["all"]->Fill(ETruth);
				
				//if(zdd_energy==0) continue;
				if(!(XZddISR>=0 && XZddISR<=4 && fabs(YZddISR)>=0.5)) continue;
				
				m_maphETruthZdd["all"]->Fill(ETruth);
				m_maphEZdd["all"]->Fill(zdd_energy*1e-3);			
				m_maphEZddOverETruth["all"]->Fill(zdd_energy*1e-3/ETruth);
				m_maphDeltaETruthEZdd["all"]->Fill((ETruth-zdd_energy*1e-3)/ETruth);
				m_maphEZddvsETruth["all"]->Fill(ETruth,zdd_energy*1e-3);
				m_maphEZddOverETruthvsETruth["all"]->Fill(ETruth,zdd_energy*1e-3/ETruth);
			}
	}
	
	gStyle->SetOptStat(0);
	TCanvas *c  =new TCanvas();
	c->Print(Form("%s[",outpdf.c_str()));	
	m_maphETruth["all"]->Draw();
	m_maphETruthZdd["all"]->SetLineColor(2);
	m_maphETruthZdd["all"]->Draw("same");
	c->Print(Form("%s",outpdf.c_str()));
	c->Clear();
	c->SetLogy(1);
	m_maphEZdd["all"]->Draw();
	c->Print(Form("%s",outpdf.c_str()));
	c->Clear();
	m_maphEZddOverETruth["all"]->Draw();
	c->Print(Form("%s",outpdf.c_str()));
	c->Clear();
	m_maphDeltaETruthEZdd["all"]->Draw();
	c->Print(Form("%s",outpdf.c_str()));
	c->Clear();
	c->SetLogy(0);
	c->SetLogz(1);
	m_maphEZddvsETruth["all"]->Draw("colz");
	c->Print(Form("%s",outpdf.c_str()));
	c->Clear();
	m_maphEZddOverETruthvsETruth["all"]->Draw("colz");
	c->Print(Form("%s",outpdf.c_str()));
	c->Print(Form("%s]",outpdf.c_str()));
	
	outfile->Write();
	outfile->Close();
}