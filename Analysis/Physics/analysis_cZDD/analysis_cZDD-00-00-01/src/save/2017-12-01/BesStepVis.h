#include <TEveManager.h>
#include <TEveEventManager.h>
#include <TEveVSD.h>
#include <TEveVSDStructs.h>

#include <TEveTrack.h>
#include <TEveLine.h>
#include <TEveTrackPropagator.h>
#include <TEveGeoShape.h>

#include <TGTab.h>
#include <TGButton.h>

#include <TFile.h>
#include <TKey.h>
#include <TSystem.h>
#include <TPRegexp.h>
#include <TPolyLine3D.h>
#include <TRandom.h>

#include <ostream>
#include <iostream> 
#include <vector>
#include <map> 
using namespace std;

// Include componets -- compile time link :)

#include "MultiView.C"
MultiView* gMultiView = 0;

class BesStepVis