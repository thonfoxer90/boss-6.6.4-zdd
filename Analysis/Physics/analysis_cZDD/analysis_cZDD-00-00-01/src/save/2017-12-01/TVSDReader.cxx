#include "TVSDReader.h"

ClassImp(TVSDReader)

MultiView* gMultiView = 0;

TVSDReader::TVSDReader(const char* file_name) :
      fFile(0), fDirectory(0), fEvDirKeys(0),
      fVSD(0),

      fMaxEv(-1), fCurEv(-1),

      fTrackList(0)
   {
      fFile = TFile::Open(file_name);
      if (!fFile)
      {
         Error("VSD_Reader", "Can not open file '%s' ... terminating.",
               file_name);
         gSystem->Exit(1);
      }

      fEvDirKeys = new TObjArray;
      TPMERegexp name_re("Event\\d+");
      TObjLink* lnk = fFile->GetListOfKeys()->FirstLink();
      while (lnk)
      {
         if (name_re.Match(lnk->GetObject()->GetName()))
         {
            fEvDirKeys->Add(lnk->GetObject());
         }
         lnk = lnk->Next();
      }

      fMaxEv = fEvDirKeys->GetEntriesFast();
      if (fMaxEv == 0)
      {
         Error("VSD_Reader", "No events to show ... terminating.");
         gSystem->Exit(1);
      }

      fVSD = new TEveVSD;
   }

   TVSDReader::~TVSDReader()
   {
      // Destructor.

      DropEvent();

      delete fVSD;
      delete fEvDirKeys;

      fFile->Close();
      delete fFile;
   }

   void TVSDReader::AttachEvent()
   {
      // Attach event data from current directory.

      fVSD->LoadTrees();
      fVSD->SetBranchAddresses();
   }

   void TVSDReader::DropEvent()
   {
      // Drup currently held event data, release current directory.

      // Drop old visualization structures.

      gEve->GetViewers()->DeleteAnnotations();
      gEve->GetCurrentEvent()->DestroyElements();

      // Drop old event-data.

      fVSD->DeleteTrees();
      delete fDirectory;
      fDirectory = 0;
   }

   //---------------------------------------------------------------------------
   // Event navigation
   //---------------------------------------------------------------------------

   void TVSDReader::NextEvent()
   {
      GotoEvent(fCurEv + 1);
   }

   void TVSDReader::PrevEvent()
   {
      GotoEvent(fCurEv - 1);
   }

   Bool_t TVSDReader::GotoEvent(Int_t ev)
   {
	 		
      if (ev < 0 || ev >= fMaxEv)
      {
         Warning("GotoEvent", "Invalid event id %d.", ev);
         return kFALSE;
      }

      DropEvent();
			
      // Connect to new event-data.

      fCurEv = ev;
      fDirectory = (TDirectory*) ((TKey*) fEvDirKeys->At(fCurEv))->ReadObj();
      fVSD->SetDirectory(fDirectory);
			
      AttachEvent();
			
      // Load event data into visualization structures.
			
			//Currently Segmentation violation after exiting alive_vsd() if LoadClusters() left uncommented
			//Error : "Error in <TGLLockable::TakeLock>: 'TGLSceneBase Geometry scene' unable to take ModifyLock, already DrawLock"
			//Possible solutions :
			//-fDLCache = kFALSE; in the constructor of TPointSet3DGL.h
			
      //LoadEsdTracks();
			MakeRandSphereTracks();
			
      // Fill projected views.

      TEveElement* top = gEve->GetCurrentEvent();

      //gMultiView->DestroyEventRPhi();
      //gMultiView->ImportEventRPhi(top);
			
      //gMultiView->DestroyEventRhoZ();
      //gMultiView->ImportEventRhoZ(top);
			
			gEve->FullRedraw3D(kTRUE);
			
      return kTRUE;
   }


   //---------------------------------------------------------------------------
   // Track loading
   //---------------------------------------------------------------------------

   enum ESDTrackFlags
   {
      kITSin=0x0001,kITSout=0x0002,kITSrefit=0x0004,kITSpid=0x0008,
      kTPCin=0x0010,kTPCout=0x0020,kTPCrefit=0x0040,kTPCpid=0x0080,
      kTRDin=0x0100,kTRDout=0x0200,kTRDrefit=0x0400,kTRDpid=0x0800,
      kTOFin=0x1000,kTOFout=0x2000,kTOFrefit=0x4000,kTOFpid=0x8000,
      kHMPIDpid=0x20000,
      kEMCALmatch=0x40000,
      kTRDbackup=0x80000,
      kTRDStop=0x20000000,
      kESDpid=0x40000000,
      kTIME=0x80000000
   };

   Bool_t trackIsOn(TEveTrack* t, Int_t mask)
   {
      // Check is track-flag specified by mask are set.

      return (t->GetStatus() & mask) > 0;
   }

   void TVSDReader::LoadEsdTracks()
   {
      // Read reconstructed tracks from current event.

      if (fTrackList == 0)
      {
         fTrackList = new TEveTrackList("ESD Tracks"); 
         fTrackList->SetMainColor(6);
         fTrackList->SetMarkerColor(kYellow);
         fTrackList->SetMarkerStyle(4);
         fTrackList->SetMarkerSize(0.5);

         fTrackList->IncDenyDestroy();
      }
      else
      {
         fTrackList->DestroyElements();
      }

      TEveTrackPropagator* trkProp = fTrackList->GetPropagator();
      // !!!! Need to store field on file !!!!
      // Can store TEveMagField ?
      trkProp->SetMagField(0.5);
      trkProp->SetStepper(TEveTrackPropagator::kRungeKutta);

      Int_t nTracks = fVSD->fTreeR->GetEntries();
      for (Int_t n = 0; n < nTracks; ++n)
      {
         fVSD->fTreeR->GetEntry(n);

         TEveTrack* track = new TEveTrack(&fVSD->fR, trkProp);
         track->SetName(Form("ESD Track %d", fVSD->fR.fIndex));
         track->SetStdTitle();
         track->SetAttLineAttMarker(fTrackList);
         fTrackList->AddElement(track);
      }

      fTrackList->MakeTracks();

      gEve->AddElement(fTrackList);
   }

	 void TVSDReader::MakeRandSphereTracks(){
	 if (fTrackList == 0)
      
	 	for (Int_t n = 0; n < 2; ++n)
      {		
				 TPolyLine3D *l = makeTPolyLine3D(65539+fCurEv+n);
				 TEveLine* eveLine = makeTEveLine(l);
				 int colorCode=2+n;
				 eveLine->SetLineColor(colorCode);
				 gEve->AddElement(eveLine);
      }
			
	 }
	 
	 TPolyLine3D* TVSDReader::makeTPolyLine3D(UInt_t seed){
const Int_t n = 500;
   TRandom* r = new TRandom(seed);
   Double_t x, y, z;
   TPolyLine3D *l = new TPolyLine3D(n);
   for (Int_t i=0;i<n;i++) {
      r->Sphere(x, y, z, 1);
      l->SetPoint(i,x+1,y+1,z+1);
   }
return l;
}

TEveLine* TVSDReader::makeTEveLine(TPolyLine3D* poly3D){
int n = poly3D->GetN();
Float_t* vecP = poly3D->GetP();

TEveLine *l = new TEveLine();
for (Int_t i=0;i<n;i++) {
  int iX = i*3;
	int iY = i*3+1;
	int iZ = i*3+2;
	//cout << vecP[iX] << " " << vecP[iY] << " " << vecP[iZ] << endl;
  l->SetNextPoint(vecP[iX],vecP[iY],vecP[iZ]);
}
return l;
}