#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/PropertyMgr.h"
#include "VertexFit/IVertexDbSvc.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"

#include "EventModel/EventModel.h"
#include "EventModel/Event.h"

#include "EvtRecEvent/EvtRecEvent.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "DstEvent/TofHitStatus.h"
#include "EventModel/EventHeader.h"



#include "TMath.h"
#include "GaudiKernel/INTupleSvc.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IHistogramSvc.h"

//#include "CLHEP/Vector/ThreeVector.h"
//#include "CLHEP/Vector/LorentzVector.h"
//#include "CLHEP/Vector/TwoVector.h"
//using CLHEP::Hep3Vector;
//using CLHEP::Hep2Vector;
//using CLHEP::HepLorentzVector;
#include "CLHEP/Geometry/Point3D.h"
#ifndef ENABLE_BACKWARDS_COMPATIBILITY
   typedef HepGeom::Point3D<double> HepPoint3D;
#endif
#include "analysis_cZDD/analysis_cZDD.h"
#include "analysis_cZDD/crystalinfo.h"

#include "RawEvent/RawDataUtil.h"
#include "RawEvent/DigiEvent.h"
#include "ZddRawEvent/ZddDigi.h"
#include "Identifier/ZddID.h"
#include "McTruth/McParticle.h"
#include "McTruth/ZddMcHit.h"
//#include "McTruth/McEvent.h"
//#include "McTruth/McPrimaryParticle.h"

//#include "VertexFit/KinematicFit.h"
#include "VertexFit/KalmanKinematicFit.h"
#include "VertexFit/VertexFit.h"
#include "VertexFit/Helix.h"
#include "ParticleID/ParticleID.h"

#include <vector>


//const double twopi = 6.2831853;
//const double pi = 3.1415927;
const double mpi = 0.13957;
const double xmass[5] = {0.000511, 0.105658, 0.139570,0.493677, 0.938272};
//const double velc = 29.9792458;  tof_path unit in cm.
const double velc = 299.792458;   // tof path unit in mm
typedef std::vector<int> Vint;
typedef std::vector<HepLorentzVector> Vp4;

int Ncut0,Ncut1,Ncut2,Ncut3,Ncut4,Ncut5,Ncut6;

/*
double charge2energy(long int charge)
{
  double energy = 0;
  //fitparameters for chargeChannel -> energy conversion
  //x <=72000: polynomial 4th order, else linear function with f(x) = f(72000) + f'(72000)*(x-72000.) 
  double p[5] = {0.2258, 0.005359, 2.665e-8, 3.184e-12,-3.888e-17};

  
  if(charge <=72000) energy = p[0] + p[1]*charge + p[2] * TMath::Power(charge,2.) + p[3] * TMath::Power(charge,3.)
    + p[4]*TMath::Power(charge,4.);
  else
    energy = p[0] + p[1]*72000. + p[2]*TMath::Power(72000.,2.) + p[3]*TMath::Power(72000.,3.) + p[4]*TMath::Power(72000.,4.) + (p[1] + 2.*p[2]*charge + 3.*p[3]*charge*charge + 4.*p[4]*TMath::Power(charge,3.)) * (charge - 72000.);

  return energy;
}
*/


double charge2energy(long int charge)
{
  double energy = 0;
  // double limit = 91000.; // 4SiPMs
  //    double limit = 1000000.; // 9SiPMs
  //fitparameters for chargeChannel -> energy conversion
  //x <=limit: polynomial 5th order, else linear function with f(x) = f(limit) + f'(limit)*(x-limit)                   
      //  double p[5] = {0.1919, 0.005379, 4.983e-8, 2.791e-13, -7.012e-18}; // 4SiPMs
      //       double p[5] = {0.02611, 0.001179, 5.228e-10, 4.926e-16, 6.46e-22}; // 9SiPMs no Attenuation
      //      double p[5] = {1.106, 0.04695, -1.583e-6, 8.748e-11,-1.946e-15};//2SiPMs
  double p[6] = {0.389193,6.04002e-2,-3.36707e-6,5.30877e-10,-3.26692e-14,7.02335e-19};//2SiPMs
 energy = p[0] + p[1]*charge + p[2] * TMath::Power(charge,2.) + p[3] * TMath::Power(charge,3.)
   + p[4]*TMath::Power(charge,4.) + p[5]*TMath::Power(charge,5.) ;
  return energy;
}


/////////////////////////////////////////////////////////////////////////////

analysis_cZDD::analysis_cZDD(const std::string& name, ISvcLocator* pSvcLocator) :
  Algorithm(name, pSvcLocator) {
  
  //Declare the properties  
	declareProperty("KKMC",  m_KKMC=0);
  declareProperty("Vr0cut", m_vr0cut=1.0);
  declareProperty("Vz0cut", m_vz0cut=5.0);
  declareProperty("EnergyThreshold", m_energyThreshold=0.04);
  declareProperty("GammaPhiCut", m_gammaPhiCut=20.0);
  declareProperty("GammaThetaCut", m_gammaThetaCut=20.0);
  declareProperty("GammaAngleCut", m_gammaAngleCut=20.0);
  declareProperty("Test4C", m_test4C = 1);
  declareProperty("Test5C", m_test5C = 1);
  declareProperty("CheckDedx", m_checkDedx = 1);
  declareProperty("CheckTof",  m_checkTof = 1);
  
  declareProperty("OutFileName",  m_outFileName = "Leo_test_cZDD.root");
	declareProperty("InputImpact_reco", m_ImpactRecoFileName = "Impact_reco.root");
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
StatusCode analysis_cZDD::initialize(){
  MsgStream log(msgSvc(), name());

  log << MSG::INFO << "in initialize()" << endmsg;
  
  StatusCode status;
  myFile = new TFile(m_outFileName.c_str(),"RECREATE","TreeFile");
//  myFile = new TFile(m_outFileName,"RECREATE","TreeFile");
  myTree = new TTree("myTree_events", "");   //Create Tree    
  // variables to save tree data                                                                  
	DefineZDDCrystalsEdges();
	                                                                          
  // HepLorentzVector hepvec_test;                                                                                                                                         
  G4double vertex_x0, vertex_y0, vertex_z0, vertex_r0;
  // protons/antiprotons                                                                                                                                                 

    // charged track vertex                                                                                                                                                   
  /*
  myTree->Branch("vertex_x0",&vertex_x0);
  // p pbar properties                                                                                                                                                      
  myTree->Branch("p_px",&p_px);
  myTree->Branch("p_py",&p_py);
  myTree->Branch("p_pz",&p_pz);
  myTree->Branch("p_probID",&p_probID);
  myTree->Branch("pbar_px",&pbar_px);
  myTree->Branch("pbar_py",&pbar_py);
  myTree->Branch("pbar_pz",&pbar_pz);
  myTree->Branch("pbar_probID",&pbar_probID);
  myTree->Branch("ppbar_invmass",&ppbar_invmass);
  */
  // mc truth of isr photons                                                                                                                                              
  myTree->Branch("isr_energy",&isr_energy);
  myTree->Branch("isr_x",&isr_x);
  myTree->Branch("isr_y",&isr_y);
  myTree->Branch("isr_z",&isr_z);
  myTree->Branch("isr_px",&isr_px);
  myTree->Branch("isr_py",&isr_py);
  myTree->Branch("isr_pz",&isr_pz);
  myTree->Branch("hit_x",&hit_x);
  myTree->Branch("hit_y",&hit_y);
  myTree->Branch("dist_center",&dist_center);

  // tot energy in zdd, average time                                                                                                                                        
  myTree->Branch("zdd_energy",&zdd_energy);
  myTree->Branch("zdd_time",&zdd_time);
  //  myTree->Branch("chargeChannel", &zdd_chargeChannel);
  
  myTree->Branch("eDep101",&eDep[0]);
  myTree->Branch("eDep102",&eDep[1]);
  myTree->Branch("eDep103",&eDep[2]);
  myTree->Branch("eDep104",&eDep[3]);
  myTree->Branch("eDep111",&eDep[4]);
  myTree->Branch("eDep112",&eDep[5]);
  myTree->Branch("eDep113",&eDep[6]);
  myTree->Branch("eDep114",&eDep[7]);
  myTree->Branch("eDep121",&eDep[8]);
  myTree->Branch("eDep122",&eDep[9]);
  myTree->Branch("eDep123",&eDep[10]);
  myTree->Branch("eDep124",&eDep[11]);
  myTree->Branch("eDep201",&eDep[12]);
  myTree->Branch("eDep202",&eDep[13]);
  myTree->Branch("eDep203",&eDep[14]);
  myTree->Branch("eDep204",&eDep[15]);
  myTree->Branch("eDep211",&eDep[16]);
  myTree->Branch("eDep212",&eDep[17]);
  myTree->Branch("eDep213",&eDep[18]);
  myTree->Branch("eDep214",&eDep[19]);
  myTree->Branch("eDep221",&eDep[20]);
  myTree->Branch("eDep222",&eDep[21]);
  myTree->Branch("eDep223",&eDep[22]);
  myTree->Branch("eDep224",&eDep[23]);
  myTree->Branch("event", &evt);
  myTree->Branch("run", &run);
  
  
  
  leoTree = new TTree("leoTree", "leoTree");
  leoTree->Branch("event", &evt);
  leoTree->Branch("run", &run);
  leoTree->Branch("partProp", &partProp);
  leoTree->Branch("primPart", &primPart);
  leoTree->Branch("leafPart", &leafPart);
  leoTree->Branch("iniPosX", &iniPosX);
  leoTree->Branch("iniPosY", &iniPosY);
  leoTree->Branch("iniPosZ", &iniPosZ);
  leoTree->Branch("iniPosT", &iniPosT);
  leoTree->Branch("finPosX", &finPosX);
  leoTree->Branch("finPosY", &finPosY);
  leoTree->Branch("finPosZ", &finPosZ);
  leoTree->Branch("finPosT", &finPosT);
  leoTree->Branch("iniMomX", &iniMomX);
  leoTree->Branch("iniMomY", &iniMomY);
  leoTree->Branch("iniMomZ", &iniMomZ);
  leoTree->Branch("iniMomE", &iniMomE);
  
	AllMCTruthTree = new TTree("AllMCTruthTree", "AllMCTruthTree");
	AllMCTruthTree->Branch("event", &evt);
	AllMCTruthTree->Branch("run", &run);
	AllMCTruthTree->Branch("MCntracks", &MCntracks,"MCntracks/I");
	AllMCTruthTree->Branch("MCTrackIndex", MCTrackIndex,"MCTrackIndex[MCntracks]/I");
	AllMCTruthTree->Branch("MCpartProp", MCpartProp,"MCpartProp[MCntracks]/I");
  AllMCTruthTree->Branch("MCprimPart", MCprimPart,"MCprimPart[MCntracks]/O");
	AllMCTruthTree->Branch("MCMother", MCMother,"MCMother[MCntracks]/I");
  AllMCTruthTree->Branch("MCiniPosX", MCiniPosX,"MCiniPosX[MCntracks]/D");
  AllMCTruthTree->Branch("MCiniPosY", MCiniPosY,"MCiniPosY[MCntracks]/D");
  AllMCTruthTree->Branch("MCiniPosZ", MCiniPosZ,"MCiniPosZ[MCntracks]/D");
	AllMCTruthTree->Branch("MCfinPosX", MCfinPosX,"MCfinPosX[MCntracks]/D");
  AllMCTruthTree->Branch("MCfinPosY", MCfinPosY,"MCfinPosY[MCntracks]/D");
  AllMCTruthTree->Branch("MCfinPosZ", MCfinPosZ,"MCfinPosZ[MCntracks]/D");
	AllMCTruthTree->Branch("MCXCalcatZDD", MCXCalcatZDD,"MCXCalcatZDD[MCntracks]/D");
  AllMCTruthTree->Branch("MCYCalcatZDD", MCYCalcatZDD,"MCYCalcatZDD[MCntracks]/D");
  AllMCTruthTree->Branch("MCiniMomX", MCiniMomX,"MCiniMomX[MCntracks]/D");
  AllMCTruthTree->Branch("MCiniMomY", MCiniMomY, "MCiniMomY[MCntracks]/D");
  AllMCTruthTree->Branch("MCiniMomZ", MCiniMomZ, "MCiniMomZ[MCntracks]/D");
	AllMCTruthTree->Branch("MCiniMomRho", MCiniMomRho, "MCiniMomRho[MCntracks]/D");
  AllMCTruthTree->Branch("MCiniMomTheta", MCiniMomTheta,"MCiniMomTheta[MCntracks]/D");
  AllMCTruthTree->Branch("MCiniMomPhi", MCiniMomPhi, "MCiniMomPhi[MCntracks]/D");
  AllMCTruthTree->Branch("MCiniE", MCiniE,"MCiniE[MCntracks]/D");
	AllMCTruthTree->Branch("zdd_energy",&zdd_energy);
	AllMCTruthTree->Branch("zddLeft_energy",&zddLeft_energy);
	AllMCTruthTree->Branch("zddRight_energy",&zddRight_energy);
	
	particlegunTree = new TTree("particlegunTree", "particlegunTree");
	/*
	particlegunTree->Branch("MCntracks", &MCntracks,"MCntracks/I");
	particlegunTree->Branch("MCTrackIndex", MCTrackIndex,"MCTrackIndex[MCntracks]/I");
	particlegunTree->Branch("MCpartProp", MCpartProp,"MCpartProp[MCntracks]/I");
  particlegunTree->Branch("MCprimPart", MCprimPart,"MCprimPart[MCntracks]/O");
  particlegunTree->Branch("MCiniPosX", MCiniPosX,"MCiniPosX[MCntracks]/D");
  particlegunTree->Branch("MCiniPosY", MCiniPosY,"MCiniPosY[MCntracks]/D");
  particlegunTree->Branch("MCiniPosZ", MCiniPosZ,"MCiniPosZ[MCntracks]/D");
	particlegunTree->Branch("MCfinPosX", MCfinPosX,"MCfinPosX[MCntracks]/D");
  particlegunTree->Branch("MCfinPosY", MCfinPosY,"MCfinPosY[MCntracks]/D");
  particlegunTree->Branch("MCfinPosZ", MCfinPosZ,"MCfinPosZ[MCntracks]/D");
	particlegunTree->Branch("MCXCalcatZDD", MCXCalcatZDD,"MCXCalcatZDD[MCntracks]/D");
  particlegunTree->Branch("MCYCalcatZDD", MCYCalcatZDD,"MCYCalcatZDD[MCntracks]/D");
  particlegunTree->Branch("MCiniMomX", MCiniMomX,"MCiniMomX[MCntracks]/D");
  particlegunTree->Branch("MCiniMomY", MCiniMomY, "MCiniMomY[MCntracks]/D");
  particlegunTree->Branch("MCiniMomZ", MCiniMomZ, "MCiniMomZ[MCntracks]/D");
	particlegunTree->Branch("MCiniMomRho", MCiniMomRho, "MCiniMomRho[MCntracks]/D");
  particlegunTree->Branch("MCiniMomTheta", MCiniMomTheta,"MCiniMomTheta[MCntracks]/D");
  particlegunTree->Branch("MCiniMomPhi", MCiniMomPhi, "MCiniMomPhi[MCntracks]/D");
  particlegunTree->Branch("MCiniE", MCiniE,"MCiniE[MCntracks]/D");
	*/
	particlegunTree->Branch("run", &run);
	particlegunTree->Branch("event", &evt);
	particlegunTree->Branch("MCpGunTrackID", &MCpGunTrackID);
	particlegunTree->Branch("MCpGunpartProp", &MCpGunpartProp);
	particlegunTree->Branch("MCpGuniniPosX", &MCpGuniniPosX);
	particlegunTree->Branch("MCpGuniniPosY", &MCpGuniniPosY);
	particlegunTree->Branch("MCpGuniniPosZ", &MCpGuniniPosZ);
	particlegunTree->Branch("MCpGunfinPosX", &MCpGunfinPosX);
	particlegunTree->Branch("MCpGunfinPosY", &MCpGunfinPosY);
	particlegunTree->Branch("MCpGunfinPosZ", &MCpGunfinPosZ);
	particlegunTree->Branch("MCpGunMomX", &MCpGunMomX);
	particlegunTree->Branch("MCpGunMomY", &MCpGunMomY);
	particlegunTree->Branch("MCpGunMomZ", &MCpGunMomZ);
	particlegunTree->Branch("MCpGunMomRho", &MCpGunMomRho);
	particlegunTree->Branch("MCpGunMomTheta", &MCpGunMomTheta);
	particlegunTree->Branch("MCpGunMomPhi", &MCpGunMomPhi);
	particlegunTree->Branch("MCpGunE", &MCpGunE);
	particlegunTree->Branch("MCpGunXCalcatZDD", &MCpGunXCalcatZDD);
	particlegunTree->Branch("MCpGunYCalcatZDD", &MCpGunYCalcatZDD);
	particlegunTree->Branch("zdd_partID",&zdd_partID);
  particlegunTree->Branch("zdd_energy",&zdd_energy);
  particlegunTree->Branch("zdd_time",&zdd_time);
	particlegunTree->Branch("eDep101",&eDep[0]);
  particlegunTree->Branch("eDep102",&eDep[1]);
  particlegunTree->Branch("eDep103",&eDep[2]);
  particlegunTree->Branch("eDep104",&eDep[3]);
  particlegunTree->Branch("eDep111",&eDep[4]);
  particlegunTree->Branch("eDep112",&eDep[5]);
  particlegunTree->Branch("eDep113",&eDep[6]);
  particlegunTree->Branch("eDep114",&eDep[7]);
  particlegunTree->Branch("eDep121",&eDep[8]);
  particlegunTree->Branch("eDep122",&eDep[9]);
  particlegunTree->Branch("eDep123",&eDep[10]);
  particlegunTree->Branch("eDep124",&eDep[11]);
  particlegunTree->Branch("eDep201",&eDep[12]);
  particlegunTree->Branch("eDep202",&eDep[13]);
  particlegunTree->Branch("eDep203",&eDep[14]);
  particlegunTree->Branch("eDep204",&eDep[15]);
  particlegunTree->Branch("eDep211",&eDep[16]);
  particlegunTree->Branch("eDep212",&eDep[17]);
  particlegunTree->Branch("eDep213",&eDep[18]);
  particlegunTree->Branch("eDep214",&eDep[19]);
  particlegunTree->Branch("eDep221",&eDep[20]);
  particlegunTree->Branch("eDep222",&eDep[21]);
  particlegunTree->Branch("eDep223",&eDep[22]);
  particlegunTree->Branch("eDep224",&eDep[23]);
	particlegunTree->Branch("eDep101Left",&eDepSide[0][0]);
  particlegunTree->Branch("eDep102Left",&eDepSide[1][0]);
  particlegunTree->Branch("eDep103Left",&eDepSide[2][0]);
  particlegunTree->Branch("eDep104Left",&eDepSide[3][0]);
  particlegunTree->Branch("eDep111Left",&eDepSide[4][0]);
  particlegunTree->Branch("eDep112Left",&eDepSide[5][0]);
  particlegunTree->Branch("eDep113Left",&eDepSide[6][0]);
  particlegunTree->Branch("eDep114Left",&eDepSide[7][0]);
  particlegunTree->Branch("eDep121Left",&eDepSide[8][0]);
  particlegunTree->Branch("eDep122Left",&eDepSide[9][0]);
  particlegunTree->Branch("eDep123Left",&eDepSide[10][0]);
  particlegunTree->Branch("eDep124Left",&eDepSide[11][0]);
  particlegunTree->Branch("eDep201Left",&eDepSide[12][0]);
  particlegunTree->Branch("eDep202Left",&eDepSide[13][0]);
  particlegunTree->Branch("eDep203Left",&eDepSide[14][0]);
  particlegunTree->Branch("eDep204Left",&eDepSide[15][0]);
  particlegunTree->Branch("eDep211Left",&eDepSide[16][0]);
  particlegunTree->Branch("eDep212Left",&eDepSide[17][0]);
  particlegunTree->Branch("eDep213Left",&eDepSide[18][0]);
  particlegunTree->Branch("eDep214Left",&eDepSide[19][0]);
  particlegunTree->Branch("eDep221Left",&eDepSide[20][0]);
  particlegunTree->Branch("eDep222Left",&eDepSide[21][0]);
  particlegunTree->Branch("eDep223Left",&eDepSide[22][0]);
  particlegunTree->Branch("eDep224Left",&eDepSide[23][0]);
	particlegunTree->Branch("eDep101Right",&eDepSide[0][1]);
  particlegunTree->Branch("eDep102Right",&eDepSide[1][1]);
  particlegunTree->Branch("eDep103Right",&eDepSide[2][1]);
  particlegunTree->Branch("eDep104Right",&eDepSide[3][1]);
  particlegunTree->Branch("eDep111Right",&eDepSide[4][1]);
  particlegunTree->Branch("eDep112Right",&eDepSide[5][1]);
  particlegunTree->Branch("eDep113Right",&eDepSide[6][1]);
  particlegunTree->Branch("eDep114Right",&eDepSide[7][1]);
  particlegunTree->Branch("eDep121Right",&eDepSide[8][1]);
  particlegunTree->Branch("eDep122Right",&eDepSide[9][1]);
  particlegunTree->Branch("eDep123Right",&eDepSide[10][1]);
  particlegunTree->Branch("eDep124Right",&eDepSide[11][1]);
  particlegunTree->Branch("eDep201Right",&eDepSide[12][1]);
  particlegunTree->Branch("eDep202Right",&eDepSide[13][1]);
  particlegunTree->Branch("eDep203Right",&eDepSide[14][1]);
  particlegunTree->Branch("eDep204Right",&eDepSide[15][1]);
  particlegunTree->Branch("eDep211Right",&eDepSide[16][1]);
  particlegunTree->Branch("eDep212Right",&eDepSide[17][1]);
  particlegunTree->Branch("eDep213Right",&eDepSide[18][1]);
  particlegunTree->Branch("eDep214Right",&eDepSide[19][1]);
  particlegunTree->Branch("eDep221Right",&eDepSide[20][1]);
  particlegunTree->Branch("eDep222Right",&eDepSide[21][1]);
  particlegunTree->Branch("eDep223Right",&eDepSide[22][1]);
  particlegunTree->Branch("eDep224Right",&eDepSide[23][1]);
	particlegunTree->Branch("xFirstZDDHit",&xFirstZDDHit);
  particlegunTree->Branch("yFirstZDDHit",&yFirstZDDHit);

	int hnum;

	myFile->mkdir("particlegun");
	myFile->cd("particlegun");
	hnum=1;
	int NBinsX=128;
	int NBinsY=128;
	m_hPGunMCiniMomX =	new TH1D(Form("%i",hnum),"Particule gun Initial Px - All MC",NBinsX,0.,0.);
	m_hPGunMCiniMomX->GetXaxis()->SetTitle("P_{X} (GeV/c)");
	hnum++;
	m_hPGunMCiniMomY =	new TH1D(Form("%i",hnum),"Particule gun Initial Py - All MC ",NBinsX,0.,0.);
	m_hPGunMCiniMomY->GetXaxis()->SetTitle("P_{Y} (GeV/c)");
	hnum++;
	m_hPGunMCiniMomZ =	new TH1D(Form("%i",hnum),"Particule gun Initial Pz - All MC ",NBinsX,0.,0.);
	m_hPGunMCiniMomZ->GetXaxis()->SetTitle("P_{Z} (GeV/c)");
	hnum++;
	m_hPGunMCiniE =	new TH1D(Form("%i",hnum),"Particule gun Initial Energy - All MC ",NBinsX,0.,0.);
	m_hPGunMCiniE->GetXaxis()->SetTitle("E (GeV/c)");
	hnum++;
	m_hPGunMCiniTheta =	new TH1D(Form("%i",hnum),"Particule gun Initial Theta - All MC ",NBinsX,0.,0.);
	m_hPGunMCiniTheta->GetXaxis()->SetTitle("#Theta (rad)");
	hnum++;
	m_hPGunMCiniPhi =	new TH1D(Form("%i",hnum),"Particule gun Initial Phi - All MC ",NBinsX,0.,0.);
	m_hPGunMCiniPhi->GetXaxis()->SetTitle("#phi (rad)");
	hnum++;
	m_hPGunMCfinPosX =	new TH1D(Form("%i",hnum),"Particule gun Final X - All MC ",NBinsX,0.,0.);
	m_hPGunMCfinPosX->GetXaxis()->SetTitle("X_{Final} (cm)");
	hnum++;
	m_hPGunMCfinPosY =	new TH1D(Form("%i",hnum),"Particule gun Final Y - All MC ",NBinsX,0.,0.);
	m_hPGunMCfinPosY->GetXaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hPGunMCfinPosZ =	new TH1D(Form("%i",hnum),"Particule gun Final Z - All MC ",NBinsX,0.,0.);
	m_hPGunMCfinPosZ->GetXaxis()->SetTitle("Z_{Final} (cm)");
	hnum++;
	m_hPGunMCExtrapolatedZDDHitXY =	new TH2D(Form("%i",hnum),"Particule gun Extrapolated Hit XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hPGunMCExtrapolatedZDDHitXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hPGunMCExtrapolatedZDDHitXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hPGunMCfinPosXY =	new TH2D(Form("%i",hnum),"Particule gun Final XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hPGunMCfinPosXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hPGunMCfinPosXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hPGunMCfinPosZX =	new TH2D(Form("%i",hnum),"Particule gun Final ZX - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hPGunMCfinPosZX->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hPGunMCfinPosZX->GetYaxis()->SetTitle("X_{Final} (cm)");
	hnum++;
	m_hPGunMCfinPosZY =	new TH2D(Form("%i",hnum),"Particule gun Final ZY - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hPGunMCfinPosZY->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hPGunMCfinPosZY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hPGunZDDFirstHitXY =	new TH2D(Form("%i",hnum),"First hit on ZDD XY - ZDD hit ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hPGunZDDFirstHitXY->GetXaxis()->SetTitle("X_{hit} (cm)");
	m_hPGunZDDFirstHitXY->GetYaxis()->SetTitle("Y_{hit} (cm)");
	hnum++;
	m_hPGunZDDDeltaHitXExtrapolX =	new TH1D(Form("%i",hnum),"X_{hit}-X_{extrapolated from IP} (cm)",NBinsX,0,0);
	m_hPGunZDDDeltaHitXExtrapolX->GetXaxis()->SetTitle("X_{hit}-X_{extrapolated from IP} (cm)");
	hnum++;
	m_hPGunZDDDeltaHitYExtrapolY =	new TH1D(Form("%i",hnum),"Y_{hit}-Y_{extrapolated from IP} (cm)",NBinsX,0,0);
	m_hPGunZDDDeltaHitYExtrapolY->GetXaxis()->SetTitle("Y_{hit}-Y_{extrapolated from IP} (cm)");
	hnum++;
	m_hPGunZDDEreco =	new TH1D(Form("%i",hnum),"Energy reconstructed - ZDD hit ",NBinsX,0.,0.);
	m_hPGunZDDEreco->GetXaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	m_hPGunZDDErecovsETruth =	new TH2D(Form("%i",hnum),"E_{reco} vs E_{Truth} - ZDD hit ",NBinsX,0.,3.,NBinsY,0.,3.);
	m_hPGunZDDErecovsETruth->GetXaxis()->SetTitle("E_{Truth} (GeV)");
	m_hPGunZDDErecovsETruth->GetYaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	Double_t xbins[5]={0,1.,2.,3.,4.};
	Double_t ybins[8]={-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5};
	//m_hPGunClustersZDDRight = new TH2D(Form("%i",hnum),"Cluster hit - ZDD Right",4,xbins,7,ybins);
	m_hPGunClustersZDDRight = new TH2D(Form("%i",hnum),"Cluster hit - ZDD Right",4,m_XEdgesZDDRight,7,m_YEdgesZDDRight);
	m_hPGunClustersZDDRight->GetXaxis()->SetTitle("X");
	m_hPGunClustersZDDRight->GetYaxis()->SetTitle("Y");
	hnum++;
	myFile->cd();
	
	BhabhaTree = new TTree("BhabhaTree", "BhabhaTree");
	BhabhaTree->Branch("run", &run);
	BhabhaTree->Branch("event", &evt);
	BhabhaTree->Branch("MCBhabhaElecTrackID", &MCBhabhaElecTrackID);
	BhabhaTree->Branch("MCBhabhaEleciniPosX", &MCBhabhaEleciniPosX);
	BhabhaTree->Branch("MCBhabhaEleciniPosY", &MCBhabhaEleciniPosY);
	BhabhaTree->Branch("MCBhabhaEleciniPosZ", &MCBhabhaEleciniPosZ);
	BhabhaTree->Branch("MCBhabhaElecfinPosX", &MCBhabhaElecfinPosX);
	BhabhaTree->Branch("MCBhabhaElecfinPosY", &MCBhabhaElecfinPosY);
	BhabhaTree->Branch("MCBhabhaElecfinPosZ", &MCBhabhaElecfinPosZ);
	BhabhaTree->Branch("MCBhabhaElecMomRho", &MCBhabhaElecMomRho);
	BhabhaTree->Branch("MCBhabhaElecMomTheta", &MCBhabhaElecMomTheta);
	BhabhaTree->Branch("MCBhabhaElecMomPhi", &MCBhabhaElecMomPhi);
	BhabhaTree->Branch("MCBhabhaElecE", &MCBhabhaElecE);
	BhabhaTree->Branch("MCBhabhaElecXCalcatZDD", &MCBhabhaElecXCalcatZDD);
	BhabhaTree->Branch("MCBhabhaElecYCalcatZDD", &MCBhabhaElecYCalcatZDD);
	
	BhabhaTree->Branch("MCBhabhaPositronTrackID", &MCBhabhaPositronTrackID);
	BhabhaTree->Branch("MCBhabhaPositroniniPosX", &MCBhabhaPositroniniPosX);
	BhabhaTree->Branch("MCBhabhaPositroniniPosY", &MCBhabhaPositroniniPosY);
	BhabhaTree->Branch("MCBhabhaPositroniniPosZ", &MCBhabhaPositroniniPosZ);
	BhabhaTree->Branch("MCBhabhaPositronfinPosX", &MCBhabhaPositronfinPosX);
	BhabhaTree->Branch("MCBhabhaPositronfinPosY", &MCBhabhaPositronfinPosY);
	BhabhaTree->Branch("MCBhabhaPositronfinPosZ", &MCBhabhaPositronfinPosZ);
	BhabhaTree->Branch("MCBhabhaPositronMomRho", &MCBhabhaPositronMomRho);
	BhabhaTree->Branch("MCBhabhaPositronMomTheta", &MCBhabhaPositronMomTheta);
	BhabhaTree->Branch("MCBhabhaPositronMomPhi", &MCBhabhaPositronMomPhi);
	BhabhaTree->Branch("MCBhabhaPositronE", &MCBhabhaPositronE);
	BhabhaTree->Branch("MCBhabhaPositronXCalcatZDD", &MCBhabhaPositronXCalcatZDD);
	BhabhaTree->Branch("MCBhabhaPositronYCalcatZDD", &MCBhabhaPositronYCalcatZDD);
	
	
	BhabhaTree->Branch("MCBhabhanGammas", &MCBhabhanGammas,"MCBhabhanGammas/I");
	BhabhaTree->Branch("MCBhabhaGamTrackID", MCBhabhaGamTrackID,"MCBhabhaGamTrackID[MCBhabhanGammas]/I");
	
	BhabhaTree->Branch("MCBhabhaGaminiPosX", MCBhabhaGaminiPosX, "MCBhabhaGaminiPosX[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGaminiPosY", MCBhabhaGaminiPosY, "MCBhabhaGaminiPosY[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGaminiPosZ", MCBhabhaGaminiPosZ, "MCBhabhaGaminiPosZ[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGamfinPosX", MCBhabhaGamfinPosX,"MCBhabhaGamfinPosX[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGamfinPosY", MCBhabhaGamfinPosY,"MCBhabhaGamfinPosY[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGamfinPosZ", MCBhabhaGamfinPosZ,"MCBhabhaGamfinPosZ[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGamMomRho", MCBhabhaGamMomRho,"MCBhabhaGamMomRho[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGamMomTheta", MCBhabhaGamMomTheta,"MCBhabhaGamMomTheta[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGamMomPhi", MCBhabhaGamMomPhi,"MCBhabhaGamMomPhi[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGamE", MCBhabhaGamE,"MCBhabhaGamE[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGamXCalcatZDD", MCBhabhaGamXCalcatZDD,"MCBhabhaGamXCalcatZDD[MCBhabhanGammas]/D");
	BhabhaTree->Branch("MCBhabhaGamYCalcatZDD", MCBhabhaGamYCalcatZDD,"MCBhabhaGamYCalcatZDD[MCBhabhanGammas]/D");
	
	BhabhaTree->Branch("zdd_partID",&zdd_partID);
  BhabhaTree->Branch("zdd_energy",&zdd_energy);
	BhabhaTree->Branch("zddLeft_energy",&zddLeft_energy);
	BhabhaTree->Branch("zddRight_energy",&zddRight_energy);
  BhabhaTree->Branch("zdd_time",&zdd_time);
	BhabhaTree->Branch("eDep101",&eDep[0]);
  BhabhaTree->Branch("eDep102",&eDep[1]);
  BhabhaTree->Branch("eDep103",&eDep[2]);
  BhabhaTree->Branch("eDep104",&eDep[3]);
  BhabhaTree->Branch("eDep111",&eDep[4]);
  BhabhaTree->Branch("eDep112",&eDep[5]);
  BhabhaTree->Branch("eDep113",&eDep[6]);
  BhabhaTree->Branch("eDep114",&eDep[7]);
  BhabhaTree->Branch("eDep121",&eDep[8]);
  BhabhaTree->Branch("eDep122",&eDep[9]);
  BhabhaTree->Branch("eDep123",&eDep[10]);
  BhabhaTree->Branch("eDep124",&eDep[11]);
  BhabhaTree->Branch("eDep201",&eDep[12]);
  BhabhaTree->Branch("eDep202",&eDep[13]);
  BhabhaTree->Branch("eDep203",&eDep[14]);
  BhabhaTree->Branch("eDep204",&eDep[15]);
  BhabhaTree->Branch("eDep211",&eDep[16]);
  BhabhaTree->Branch("eDep212",&eDep[17]);
  BhabhaTree->Branch("eDep213",&eDep[18]);
  BhabhaTree->Branch("eDep214",&eDep[19]);
  BhabhaTree->Branch("eDep221",&eDep[20]);
  BhabhaTree->Branch("eDep222",&eDep[21]);
  BhabhaTree->Branch("eDep223",&eDep[22]);
  BhabhaTree->Branch("eDep224",&eDep[23]);
	BhabhaTree->Branch("eDep101Left",&eDepSide[0][0]);
  BhabhaTree->Branch("eDep102Left",&eDepSide[1][0]);
  BhabhaTree->Branch("eDep103Left",&eDepSide[2][0]);
  BhabhaTree->Branch("eDep104Left",&eDepSide[3][0]);
  BhabhaTree->Branch("eDep111Left",&eDepSide[4][0]);
  BhabhaTree->Branch("eDep112Left",&eDepSide[5][0]);
  BhabhaTree->Branch("eDep113Left",&eDepSide[6][0]);
  BhabhaTree->Branch("eDep114Left",&eDepSide[7][0]);
  BhabhaTree->Branch("eDep121Left",&eDepSide[8][0]);
  BhabhaTree->Branch("eDep122Left",&eDepSide[9][0]);
  BhabhaTree->Branch("eDep123Left",&eDepSide[10][0]);
  BhabhaTree->Branch("eDep124Left",&eDepSide[11][0]);
  BhabhaTree->Branch("eDep201Left",&eDepSide[12][0]);
  BhabhaTree->Branch("eDep202Left",&eDepSide[13][0]);
  BhabhaTree->Branch("eDep203Left",&eDepSide[14][0]);
  BhabhaTree->Branch("eDep204Left",&eDepSide[15][0]);
  BhabhaTree->Branch("eDep211Left",&eDepSide[16][0]);
  BhabhaTree->Branch("eDep212Left",&eDepSide[17][0]);
  BhabhaTree->Branch("eDep213Left",&eDepSide[18][0]);
  BhabhaTree->Branch("eDep214Left",&eDepSide[19][0]);
  BhabhaTree->Branch("eDep221Left",&eDepSide[20][0]);
  BhabhaTree->Branch("eDep222Left",&eDepSide[21][0]);
  BhabhaTree->Branch("eDep223Left",&eDepSide[22][0]);
  BhabhaTree->Branch("eDep224Left",&eDepSide[23][0]);
	BhabhaTree->Branch("eDep101Right",&eDepSide[0][1]);
  BhabhaTree->Branch("eDep102Right",&eDepSide[1][1]);
  BhabhaTree->Branch("eDep103Right",&eDepSide[2][1]);
  BhabhaTree->Branch("eDep104Right",&eDepSide[3][1]);
  BhabhaTree->Branch("eDep111Right",&eDepSide[4][1]);
  BhabhaTree->Branch("eDep112Right",&eDepSide[5][1]);
  BhabhaTree->Branch("eDep113Right",&eDepSide[6][1]);
  BhabhaTree->Branch("eDep114Right",&eDepSide[7][1]);
  BhabhaTree->Branch("eDep121Right",&eDepSide[8][1]);
  BhabhaTree->Branch("eDep122Right",&eDepSide[9][1]);
  BhabhaTree->Branch("eDep123Right",&eDepSide[10][1]);
  BhabhaTree->Branch("eDep124Right",&eDepSide[11][1]);
  BhabhaTree->Branch("eDep201Right",&eDepSide[12][1]);
  BhabhaTree->Branch("eDep202Right",&eDepSide[13][1]);
  BhabhaTree->Branch("eDep203Right",&eDepSide[14][1]);
  BhabhaTree->Branch("eDep204Right",&eDepSide[15][1]);
  BhabhaTree->Branch("eDep211Right",&eDepSide[16][1]);
  BhabhaTree->Branch("eDep212Right",&eDepSide[17][1]);
  BhabhaTree->Branch("eDep213Right",&eDepSide[18][1]);
  BhabhaTree->Branch("eDep214Right",&eDepSide[19][1]);
  BhabhaTree->Branch("eDep221Right",&eDepSide[20][1]);
  BhabhaTree->Branch("eDep222Right",&eDepSide[21][1]);
  BhabhaTree->Branch("eDep223Right",&eDepSide[22][1]);
  BhabhaTree->Branch("eDep224Right",&eDepSide[23][1]);
	BhabhaTree->Branch("xFirstZDDHit",&xFirstZDDHit);
  BhabhaTree->Branch("yFirstZDDHit",&yFirstZDDHit);
	BhabhaTree->Branch("xFirstElecZDDHit",&xFirstElecZDDHit);
  BhabhaTree->Branch("yFirstElecZDDHit",&yFirstElecZDDHit);
	BhabhaTree->Branch("xFirstPositronZDDHit",&xFirstPositronZDDHit);
  BhabhaTree->Branch("yFirstPositronZDDHit",&yFirstPositronZDDHit);	BhabhaTree->Branch("MCBhabhaMomSum3VElectronPositron",&MCBhabhaMomSum3VElectronPositron);
	BhabhaTree->Branch("MCBhabhaMissMomX",&MCBhabhaMissMomX);
  BhabhaTree->Branch("MCBhabhaMissMomY",&MCBhabhaMissMomY);
	BhabhaTree->Branch("MCBhabhaMissMomZ",&MCBhabhaMissMomZ);
	
	
	myFile->mkdir("Bhabha");
	myFile->cd("Bhabha");	
	hnum=1;
	
	m_hMCBhabhaEleciniE =	new TH1D(Form("%i",hnum),"Bhabha e^{-} Initial Energy - All MC",NBinsX,0.,0.);
	m_hMCBhabhaEleciniE->GetXaxis()->SetTitle("E_{e-} (GeV)");
	hnum++;
	m_hMCBhabhaEleciniMomRho =	new TH1D(Form("%i",hnum),"Bhabha e^{-} Momentum - All MC",NBinsX,0.,0.);
	m_hMCBhabhaEleciniMomRho->GetXaxis()->SetTitle("P_{e-} (GeV/c)");
	hnum++;
	m_hMCBhabhaEleciniMomTheta =	new TH1D(Form("%i",hnum),"Bhabha e^{-} #Theta - All MC",NBinsX,0.,0.);
	m_hMCBhabhaEleciniMomTheta->GetXaxis()->SetTitle("#Theta_{e-} (rad)");
	hnum++;
	m_hMCBhabhaEleciniMomPhi =	new TH1D(Form("%i",hnum),"Bhabha e^{-} #phi - All MC",NBinsX,0.,0.);
	m_hMCBhabhaEleciniMomPhi->GetXaxis()->SetTitle("#phi_{e-} (rad)");
	hnum++;
	m_hMCBhabhaElecExtrapolatedZDDHitXY =	new TH2D(Form("%i",hnum),"Bhabha e^{-} Extrapolated Hit XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hMCBhabhaElecExtrapolatedZDDHitXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hMCBhabhaElecExtrapolatedZDDHitXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hMCBhabhaElecfinPosXY =	new TH2D(Form("%i",hnum),"Bhabha e^{-} Final XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hMCBhabhaElecfinPosXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hMCBhabhaElecfinPosXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hMCBhabhaElecfinPosZX =	new TH2D(Form("%i",hnum),"Bhabha e^{-} Final ZX - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hMCBhabhaElecfinPosZX->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hMCBhabhaElecfinPosZX->GetYaxis()->SetTitle("X_{Final} (cm)");
	hnum++;
	m_hMCBhabhaElecfinPosZY =	new TH2D(Form("%i",hnum),"Bhabha e^{-} Final ZY - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hMCBhabhaElecfinPosZY->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hMCBhabhaElecfinPosZY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	
	m_hMCBhabhaPositroniniE =	new TH1D(Form("%i",hnum),"Bhabha e^{+} Initial Energy - All MC",NBinsX,0.,0.);
	m_hMCBhabhaPositroniniE->GetXaxis()->SetTitle("E_{e+} (GeV)");
	hnum++;
	m_hMCBhabhaPositroniniMomRho =	new TH1D(Form("%i",hnum),"Bhabha e^{+} Momentum - All MC",NBinsX,0.,0.);
	m_hMCBhabhaPositroniniMomRho->GetXaxis()->SetTitle("P_{e+} (GeV/c)");
	hnum++;
	m_hMCBhabhaPositroniniMomTheta =	new TH1D(Form("%i",hnum),"Bhabha e^{+} #Theta - All MC",NBinsX,0.,0.);
	m_hMCBhabhaPositroniniMomTheta->GetXaxis()->SetTitle("#Theta_{e+} (rad)");
	hnum++;
	m_hMCBhabhaPositroniniMomPhi =	new TH1D(Form("%i",hnum),"Bhabha e^{+} #phi - All MC",NBinsX,0.,0.);
	m_hMCBhabhaPositroniniMomPhi->GetXaxis()->SetTitle("#phi_{e+} (rad)");
	hnum++;
	m_hMCBhabhaPositronExtrapolatedZDDHitXY =	new TH2D(Form("%i",hnum),"Bhabha e^{+} Extrapolated Hit XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hMCBhabhaPositronExtrapolatedZDDHitXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hMCBhabhaPositronExtrapolatedZDDHitXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hMCBhabhaPositronfinPosXY =	new TH2D(Form("%i",hnum),"Bhabha e^{+} Final XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hMCBhabhaPositronfinPosXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hMCBhabhaPositronfinPosXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hMCBhabhaPositronfinPosZX =	new TH2D(Form("%i",hnum),"Bhabha e^{+} Final ZX - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hMCBhabhaPositronfinPosZX->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hMCBhabhaPositronfinPosZX->GetYaxis()->SetTitle("X_{Final} (cm)");
	hnum++;
	m_hMCBhabhaPositronfinPosZY =	new TH2D(Form("%i",hnum),"Bhabha e^{+} Final ZY - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hMCBhabhaPositronfinPosZY->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hMCBhabhaPositronfinPosZY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hZDDBhabhaFirstHitXY =	new TH2D(Form("%i",hnum),"First hit on ZDD XY - ZDD hit ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hZDDBhabhaFirstHitXY->GetXaxis()->SetTitle("X_{hit} (cm)");
	m_hZDDBhabhaFirstHitXY->GetYaxis()->SetTitle("Y_{hit} (cm)");
	hnum++;
	m_hZDDBhabhaEreco =	new TH1D(Form("%i",hnum),"Energy reconstructed - ZDD hit ",NBinsX,0.,0.);
	m_hZDDBhabhaEreco->GetXaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	m_hZDDBhabhaErecovsETruth =	new TH2D(Form("%i",hnum),"E_{reco} vs E_{Truth} - ZDD hit ",NBinsX,0.,5.,NBinsY,0.,5.);
	m_hZDDBhabhaErecovsETruth->GetXaxis()->SetTitle("E_{Truth} (GeV)");
	m_hZDDBhabhaErecovsETruth->GetYaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	m_hZDDBhabhaErecoLeft =	new TH1D(Form("%i",hnum),"E_{reco} - ZDD Left ",NBinsX,0.,0.);
	m_hZDDBhabhaErecoLeft->GetXaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	m_hZDDBhabhaErecoRight =	new TH1D(Form("%i",hnum),"E_{reco} - ZDD Right ",NBinsX,0.,0.);
	m_hZDDBhabhaErecoRight->GetXaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	m_hZDDBhabhaErecoLeftvsRight =	new TH2D(Form("%i",hnum),"E_{reco Left} vs E_{reco Right} - ZDD hit ",NBinsX,0.,2000.,NBinsY,0.,2000.);
	m_hZDDBhabhaErecoLeftvsRight->GetXaxis()->SetTitle("E_{reco Right} (GeV)");
	m_hZDDBhabhaErecoLeftvsRight->GetYaxis()->SetTitle("E_{reco Left} (GeV)");
	hnum++;
	myFile->cd();
	/*
	ZDDMcHitColTree = new TTree("ZDDMcHitColTree", "ZDDMcHitColTree");
	ZDDMcHitColTree->Branch("HitColtrackID",&HitColtrackID);
  ZDDMcHitColTree->Branch("HitColPID",&HitColPID);
	*/
	
	//Load Impact_reco file (if any)
	m_IsImpactReco=false;
	ImpactRecoFile=TFile::Open(m_ImpactRecoFileName.c_str());
	if(ImpactRecoFile!=NULL)
		{m_IsImpactReco=true;
		ImpactRecoTree=(TTree*) ImpactRecoFile->Get("Impact_reco");
		ImpactRecoTree->SetBranchAddress("xImpact",&xFirstZDDHit);
		ImpactRecoTree->SetBranchAddress("yImpact",&yFirstZDDHit);
		
		ImpactRecoTree->SetBranchAddress("xImpactElec",&xFirstElecZDDHit);
		ImpactRecoTree->SetBranchAddress("yImpactElec",&yFirstElecZDDHit);
		
		ImpactRecoTree->SetBranchAddress("xImpactPositron",&xFirstPositronZDDHit);
		ImpactRecoTree->SetBranchAddress("yImpactPositron",&yFirstPositronZDDHit);
		}
	else
		{
		xFirstZDDHit=0;
		yFirstZDDHit=0;
		
		xFirstElecZDDHit=0;
		yFirstElecZDDHit=0;
		
		xFirstPositronZDDHit=0;
		yFirstPositronZDDHit=0;
		}
	
  log << MSG::INFO << "successfully return from initialize()" <<endmsg;
  return StatusCode::SUCCESS;

}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
StatusCode analysis_cZDD::execute() {
  
  std::cout << "execute()" << std::endl;

  G4bool debug = false; // couts on ?


  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in execute()" << endreq;

	//Initialisation of flags
	m_IsZDDhit=false;
	//Particle gun
	m_pGunMCfound=false;
	//Bhabha
	m_MCIsBhabha=false;
	m_MCHasPrimElectron=false;
	m_MCHasPrimPositron=false;
	m_MCNPrimElectron=0;
	m_MCNPrimPositron=0;
	m_MCNPrimGammas=0;
	
  SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
  int runNo=eventHeader->runNumber();
  int event=eventHeader->eventNumber();
  log << MSG::DEBUG <<"run, evtnum = "
      << runNo << " , "
      << event <<endreq;
  cout<<"event "<<event<<endl;
  Ncut0++;

  SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), EventModel::EvtRec::EvtRecEvent);
  //  log << MSG::INFO << "get event tag OK" << endreq;
    log << MSG::DEBUG <<"ncharg, nneu, tottks = " 
      << evtRecEvent->totalCharged() << " , "
      << evtRecEvent->totalNeutral() << " , "
      << evtRecEvent->totalTracks() <<endreq;

  SmartDataPtr<EvtRecTrackCol> evtRecTrkCol(eventSvc(),  EventModel::EvtRec::EvtRecTrackCol);
  //
  // check x0, y0, z0, r0
  // suggest cut: |z0|<5 && r0<1
  //
  
  // collect zdd digi information -> if energy is deposited, look under which angle the isr_photon was emitted
  //default values for unfired crystals                                                                                                                                  

  SmartDataPtr<ZddDigiCol> zddDigiCol(eventSvc(), EventModel::Digi::ZddDigiCol);                                                                                         
  if(!zddDigiCol)
    {
      G4cout<<"Could not retrieve Zdd digi collection"<<G4endl;                                                                                                                  return StatusCode::FAILURE;  
    }
  double zddenergy = 0.;  
  double zddtime = 0.; 
  double zddenergyLeft =0.;
	double zddenergyRight = 0.;
  int nhits = 0;                                                                                                                                                          
  ZddDigiCol::iterator iDigiCol;                                                                                                                                          

  for(int w =0; w <=23; w++) {eDep[w] = 0.;eDepSide[w][0]=0;eDepSide[w][1]=0;}
  for(iDigiCol=zddDigiCol->begin(); iDigiCol!=zddDigiCol->end(); iDigiCol++)                                                                                              
    {
      long int chargeChannel = (*iDigiCol)->getChargeChannel();
      if(debug) cout << "analysis_cZDD.cxx : chargeChannel : " << chargeChannel << endl;
      long int timeChannel = (*iDigiCol)->getTimeChannel();
      if(debug) cout << "analysis_cZDD.cxx : timeChannel : " << timeChannel << endl;
      long int zddTime = timeChannel/1000000;
      zddtime += zddTime;
      double crystalEnergy = charge2energy((*iDigiCol)->getChargeChannel());
      if(debug) cout << "analysis_cZDD.cxx : crystalEnergy : " << crystalEnergy << endl;
      zddenergy += crystalEnergy;
      Identifier zddID = (*iDigiCol)->identify();
      int crystalNo =ZddID::crystalNo(zddID);
      if(debug) cout << "analysis_cZDD.cxx : crystalNo : " << crystalNo << endl;
      int partID = ZddID::partID(zddID); //1 : East Detector 2: West Detector
			zdd_partID= partID;
			if(partID==1)zddenergyLeft+= crystalEnergy;
			if(partID==2)zddenergyRight+= crystalEnergy;
      if(debug) cout << "analysis_cZDD.cxx : partID : " << partID << endl;
			vector<int> vecCrystalNoPartID;
			vecCrystalNoPartID.resize(2);
			vecCrystalNoPartID[0]=crystalNo;
			vecCrystalNoPartID[1]=partID;
			m_mapcrystalenergyZDD[vecCrystalNoPartID]=crystalEnergy;
			
      switch( abs(crystalNo) )
	{
	case 101:
	  eDep[0] = crystalEnergy;
	  break;
        case 102:
          eDep[1] = crystalEnergy;       
          break;
        case 103:
	  eDep[2] = crystalEnergy;
          break;
        case 104:
	  eDep[3] = crystalEnergy;
          break;
	case 111:
	  eDep[4] = crystalEnergy;
          break;
        case 112:
	  eDep[5] = crystalEnergy;
          break;
        case 113:
	  eDep[6] = crystalEnergy;
          break;
        case 114:
	  eDep[7] = crystalEnergy;
          break;
        case 121:
	  eDep[8] = crystalEnergy;
          break;
        case 122:
	  eDep[9] = crystalEnergy;
          break;
        case 123:
	  eDep[10] = crystalEnergy;
	  break;
        case 124:
	  eDep[11] = crystalEnergy;
          break;
        case 201:
	  eDep[12] = crystalEnergy;
          break;
        case 202:
          eDep[13]= crystalEnergy;
          break;
        case 203:
          eDep[14] = crystalEnergy;
          break;
        case 204:
          eDep[15] = crystalEnergy;
          break;
        case 211:
          eDep[16] = crystalEnergy;
          break;
        case 212:
          eDep[17] = crystalEnergy;
          break;
        case 213:
          eDep[18] = crystalEnergy;
          break;
        case 214:
          eDep[19] = crystalEnergy;
          break;
        case 221:
          eDep[20] = crystalEnergy;
          break;
        case 222:
          eDep[21] = crystalEnergy;
          break;
        case 223:
          eDep[22] = crystalEnergy;
          break;
        case 224:
          eDep[23] = crystalEnergy;
          break;
	}//end switch(abs(crystalNo))
   
	 switch(partID){ 
	 case 1:
	 switch( crystalNo )
	{
	case 101:
	  eDepSide[0][0] = crystalEnergy;
	  break;
        case 102:
          eDepSide[1][0] = crystalEnergy;       
          break;
        case 103:
	  eDepSide[2][0] = crystalEnergy;
          break;
        case 104:
	  eDepSide[3][0] = crystalEnergy;
          break;
	case 111:
	  eDepSide[4][0] = crystalEnergy;
          break;
        case 112:
	  eDepSide[5][0] = crystalEnergy;
          break;
        case 113:
	  eDepSide[6][0] = crystalEnergy;
          break;
        case 114:
	  eDepSide[7][0] = crystalEnergy;
          break;
        case 121:
	  eDepSide[8][0] = crystalEnergy;
          break;
        case 122:
	  eDepSide[9][0] = crystalEnergy;
          break;
        case 123:
	  eDepSide[10][0] = crystalEnergy;
	  break;
        case 124:
	  eDepSide[11][0] = crystalEnergy;
          break;
        case 201:
	  eDepSide[12][0] = crystalEnergy;
          break;
        case 202:
          eDepSide[13][0]= crystalEnergy;
          break;
        case 203:
          eDepSide[14][0] = crystalEnergy;
          break;
        case 204:
          eDepSide[15][0] = crystalEnergy;
          break;
        case 211:
          eDepSide[16][0] = crystalEnergy;
          break;
        case 212:
          eDepSide[17][0] = crystalEnergy;
          break;
        case 213:
          eDepSide[18][0] = crystalEnergy;
          break;
        case 214:
          eDepSide[19][0] = crystalEnergy;
          break;
        case 221:
          eDepSide[20][0] = crystalEnergy;
          break;
        case 222:
          eDepSide[21][0] = crystalEnergy;
          break;
        case 223:
          eDepSide[22][0] = crystalEnergy;
          break;
        case 224:
          eDepSide[23][0] = crystalEnergy;
          break;			
		}
	break;
	 case 2:  
			switch( crystalNo )
	{
	case 101:
	  eDepSide[0][1] = crystalEnergy;
	  break;
        case 102:
          eDepSide[1][1] = crystalEnergy;       
          break;
        case 103:
	  eDepSide[2][1] = crystalEnergy;
          break;
        case 104:
	  eDepSide[3][1] = crystalEnergy;
          break;
	case 111:
	  eDepSide[4][1] = crystalEnergy;
          break;
        case 112:
	  eDepSide[5][1] = crystalEnergy;
          break;
        case 113:
	  eDepSide[6][1] = crystalEnergy;
          break;
        case 114:
	  eDepSide[7][1] = crystalEnergy;
          break;
        case 121:
	  eDepSide[8][1] = crystalEnergy;
          break;
        case 122:
	  eDepSide[9][1] = crystalEnergy;
          break;
        case 123:
	  eDepSide[10][1] = crystalEnergy;
	  break;
        case 124:
	  eDepSide[11][1] = crystalEnergy;
          break;
        case 201:
	  eDepSide[12][1] = crystalEnergy;
          break;
        case 202:
          eDepSide[13][1]= crystalEnergy;
          break;
        case 203:
          eDepSide[14][1] = crystalEnergy;
          break;
        case 204:
          eDepSide[15][1] = crystalEnergy;
          break;
        case 211:
          eDepSide[16][1] = crystalEnergy;
          break;
        case 212:
          eDepSide[17][1] = crystalEnergy;
          break;
        case 213:
          eDepSide[18][1] = crystalEnergy;
          break;
        case 214:
          eDepSide[19][1] = crystalEnergy;
          break;
        case 221:
          eDepSide[20][1] = crystalEnergy;
          break;
        case 222:
          eDepSide[21][1] = crystalEnergy;
          break;
        case 223:
          eDepSide[22][1] = crystalEnergy;
          break;
        case 224:
          eDepSide[23][1] = crystalEnergy;
          break;			
		}
	break;
	}
      zddtime += (*iDigiCol)->getTimeChannel()/1000000.;
      nhits++;

}// end for(iDigiCol)
  cout << "nhits           : " << nhits << endl;
  cout << "zddenergy  : " << zddenergy << endl;
  cout << "zddtime [ns]    : " << (zddtime/nhits)/ns << endl << "============================" << endl;
  /*
  if(debug && zddenergy > 0)
    {
      for(int i=0; i<=23; i++)
	{
	  cout << "analysis_cZDD: crystaldata[" << i << "] partID       :  " << crystaldata[i]->GetPartID() << endl;
	  cout << "analysis_cZDD: crystaldata[" << i << "] crystalNo    :  " << crystaldata[i]->GetCrystalNo() << endl;
          cout << "analysis_cZDD: crystaldata[" << i << "] chargeChannel:  " << crystaldata[i]->GetChargeChannel() << endl;
          cout << "analysis_cZDD: crystaldata[" << i << "] timeChannel  :  " << crystaldata[i]->GetTimeChannel() << endl;
          cout << "analysis_cZDD: crystaldata[" << i << "] energy       :  " << crystaldata[i]->GetEnergy() << endl;
          cout << "analysis_cZDD: crystaldata[" << i << "] time         :  " << crystaldata[i]->GetTime() << endl;
	  cout << "========================================================================" << endl;

	}//end for(int i)
    }//if(debug)
*/

zdd_energy = zddenergy;
zdd_time = zddtime/nhits;

zddLeft_energy=zddenergyLeft;
zddRight_energy=zddenergyRight;

if(zdd_energy>0) m_IsZDDhit=true;

  SmartDataPtr<Event::McParticleCol> mcParticleCol(eventSvc(), "/Event/MC/McParticleCol");
  if(!mcParticleCol)
    { 
      G4cout<<"Could not retrieve mcParticleCol"<<G4endl;
      return StatusCode::FAILURE;
    }
  else
    {
      Event::McParticleCol::iterator iter_mc = mcParticleCol->begin();
			MCntracks=mcParticleCol->size();
      for (; iter_mc != mcParticleCol->end(); iter_mc++)
      {
			int index = std::distance(mcParticleCol->begin(),iter_mc);
// 	runNo
// 	event
	evt = event;
	run = runNo;
	partProp = (*iter_mc)->particleProperty();
	primPart = (*iter_mc)->primaryParticle();
	leafPart = (*iter_mc)->leafParticle();
	iniPosX = (*iter_mc)->initialPosition().x();
	iniPosY = (*iter_mc)->initialPosition().y();
	iniPosZ = (*iter_mc)->initialPosition().z();
	iniPosT = (*iter_mc)->initialPosition().t();
	finPosX = (*iter_mc)->finalPosition().x();
	finPosY = (*iter_mc)->finalPosition().y();
	finPosZ = (*iter_mc)->finalPosition().z();
	finPosT = (*iter_mc)->finalPosition().t();
	iniMomX = (*iter_mc)->initialFourMomentum().x();
	iniMomY = (*iter_mc)->initialFourMomentum().y();
	iniMomZ = (*iter_mc)->initialFourMomentum().z();
	iniMomE = (*iter_mc)->initialFourMomentum().t();
	
	leoTree->Fill();
	
	MCTrackIndex[index] = (*iter_mc)->trackIndex();
	MCpartProp[index]= (*iter_mc)->particleProperty();
	MCprimPart[index]= (*iter_mc)->primaryParticle();
	MCMother[index]= ((*iter_mc)->mother()).particleProperty();
	MCiniPosX[index] = (*iter_mc)->initialPosition().x();
	MCiniPosY[index] = (*iter_mc)->initialPosition().y();
	MCiniPosZ[index] = (*iter_mc)->initialPosition().z();
	MCfinPosX[index] = (*iter_mc)->finalPosition().x();
	MCfinPosY[index] = (*iter_mc)->finalPosition().y();
	MCfinPosZ[index] = (*iter_mc)->finalPosition().z();
	MCiniMomX[index] = (*iter_mc)->initialFourMomentum().x();
	MCiniMomY[index] = (*iter_mc)->initialFourMomentum().y();
	MCiniMomZ[index] = (*iter_mc)->initialFourMomentum().z();
	MCiniMomRho[index] = (*iter_mc)->initialFourMomentum().rho();
	MCiniMomTheta[index] = (*iter_mc)->initialFourMomentum().theta();
	MCiniMomPhi[index] = (*iter_mc)->initialFourMomentum().phi();
	MCiniE[index] = (*iter_mc)->initialFourMomentum().t();
	
	/*
	double distance_to_zaxis_at_zdd = tan(MCiniMomTheta[index]) * 335.;
	MCXCalcatZDD[index] = cos(MCiniMomPhi[index]) * distance_to_zaxis_at_zdd; // x coordinate of photon at z = +-333cm (zdd position)                                              
  MCYCalcatZDD[index] = sin(MCiniMomPhi[index]) * distance_to_zaxis_at_zdd; // y coordinate of photon at z = +-333cm (zdd position) 
	*/
						
	Hep3Vector V3Mom;
	V3Mom.setRThetaPhi(MCiniMomRho[index],MCiniMomTheta[index],MCiniMomPhi[index]);
	//Stretch V3Mom to ZDD
	Hep3Vector V3atZDD;
	double zZDD;
	double RhoV3atZDD;	
	if(V3Mom.z()<0)
		{zZDD=-335;
		RhoV3atZDD=zZDD/cos(MCiniMomTheta[index]);
		V3Mom.setRThetaPhi(RhoV3atZDD,MCiniMomTheta[index],MCiniMomPhi[index]);
		MCXCalcatZDD[index]=V3Mom.x();
		MCYCalcatZDD[index]=V3Mom.y();
		}
	else if(V3Mom.z()>0) 
		{zZDD=335;
		RhoV3atZDD=zZDD/cos(MCiniMomTheta[index]);
		V3Mom.setRThetaPhi(RhoV3atZDD,MCiniMomTheta[index],MCiniMomPhi[index]);
		MCXCalcatZDD[index]=V3Mom.x();
		MCYCalcatZDD[index]=V3Mom.y();
		}
	else {MCXCalcatZDD[index]=-99;MCYCalcatZDD[index]=99;}
	
	printf("******* %f %f ******* \n",V3Mom.x(),V3Mom.y());
	
	if( (*iter_mc)->particleProperty() == 22 && zdd_energy > 0.)
	  {
	    double x = (*iter_mc)->initialFourMomentum().x(); // x at vertex 
	    double y = (*iter_mc)->initialFourMomentum().y(); // y at vertex
	    double z = (*iter_mc)->initialFourMomentum().z(); // z at vertex	  
	    double t = (*iter_mc)->initialFourMomentum().t(); // t at vertex
	    
	    double px = (*iter_mc)->initialFourMomentum().px(); // px at vertex
	    double py = (*iter_mc)->initialFourMomentum().py(); // py at vertex
	    double pz = (*iter_mc)->initialFourMomentum().pz(); // pz at vertex
	    double e = (*iter_mc)->initialFourMomentum().e(); // e at vertex
	    double pges = sqrt(px*px + py*py + pz*pz); // in GeV
	    
	    //	      double xangle = acos(px/pges); // angle between photon trajectory and x-axis
	    //	      double yangle = acos(py/pges); // angle between photon trajectory and y-axis
	    double phi = atan2(py,px); // tan(phi) = px/py
	    double theta = acos(pz/pges);// angle between photon trajectory and z-axis
            cout << "theta [deg]: " << theta * 180./TMath::Pi() << endl;
            cout << "phi [deg]  : " << phi * 180./TMath::Pi() << endl;
	    
	    //	    double distance_to_zaxis_at_zdd = tan(theta) * (335.-z) + sqrt( x*x + y*y );
	    double distance_to_zaxis_at_zdd = tan(theta) * 335.;
	    //	    double x_at_zdd = cos(phi) * distance_to_zaxis_at_zdd + x; // x coordinate of photon at z = +-333cm (zdd position)
	    //	    double y_at_zdd = sin(phi) * distance_to_zaxis_at_zdd + y; // y coordinate of photon at z = +-333cm (zdd position)
	    double x_at_zdd = cos(phi) * distance_to_zaxis_at_zdd; // x coordinate of photon at z = +-333cm (zdd position)                                              
            double y_at_zdd = sin(phi) * distance_to_zaxis_at_zdd; // y coordinate of photon at z = +-333cm (zdd position) 
	    double x_center = 0.;
	    double y_center_up = 2.;
	    double y_center_down = -2.;

	    double distance_from_center = 0.;
	    if(y_at_zdd >= 0) distance_from_center = sqrt(pow(y_at_zdd - y_center_up,2.)+pow(x_at_zdd,2.) );
	    else
	      {
		distance_from_center = sqrt(pow(y_at_zdd - y_center_down,2.)+pow(x_at_zdd,2.) );
	      }
	    cout << "x at zdd : " << x_at_zdd << endl;
	    cout << "y at zdd : " << y_at_zdd << endl;
	    
	    isr_energy = e; 
	    isr_x = x;
	    isr_y = y;
	    isr_z = z;
	    isr_px =px;
	    isr_py =py;
	    isr_pz =pz; 
	    hit_x = x_at_zdd;
	    hit_y = y_at_zdd;
	    dist_center = distance_from_center;

	    if(debug)
	      {
		cout << "analysis_cZDD.cxx : " << endl <<
		  "isr_energy = " << isr_energy << endl <<
		  "isr_x      = " << isr_x << endl <<
		  "isr_y      = " << isr_y << endl <<
		  "isr_z      = " << isr_z << endl <<
		  "isr_px     = " << isr_px << endl <<
		  "isr_py     = " << isr_py << endl <<
		  "isr_pz     = " << isr_pz << endl;
	      }
	   
		 
	  }// end if(*iter_mc)
		
		//For particle gun   
	   if(m_pGunMCfound==false){
		 	if((*iter_mc)->primaryParticle()==1) {		
				MCpGunTrackID=index;
				m_pGunMCfound=true;
				}
		 	}
		 
		//For Bhabha
		/*
		 if(m_MCIsBhabha==false){
		 			if(m_MCHasPrimElectron==false && (*iter_mc)->primaryParticle()==1 && (*iter_mc)->particleProperty()==11){
					m_MCHasPrimElectron=true;
					MCBhabhaElecTrackID=index;
					}
					if(m_MCHasPrimPositron=false && (*iter_mc)->primaryParticle()==1 && (*iter_mc)->particleProperty()==-11){
					m_MCHasPrimPositron=true;
					MCBhabhaPositronTrackID=index;
					}
					if(m_MCHasPrimElectron==true && m_MCHasPrimPositron==true) m_MCIsBhabha=true;
		 		}
			*/
			if((*iter_mc)->primaryParticle()==1 && (*iter_mc)->particleProperty()==11){					
					MCBhabhaElecTrackID=index;
					m_MCNPrimElectron++;
					}
			if((*iter_mc)->primaryParticle()==1 && (*iter_mc)->particleProperty()==-11){
					MCBhabhaPositronTrackID=index;
					m_MCNPrimPositron++;
					}
			if((*iter_mc)->primaryParticle()==1 && (*iter_mc)->particleProperty()==22){										
					MCBhabhaGamTrackID[m_MCNPrimGammas]=index;
					m_MCNPrimGammas++;
					}		
				
      }// end for(*iter_mc)
    }//else monte carlo collection  

  /*if(zdd_energy > 0)*/ myTree->Fill();
  //  else{myTree->Fill();}
  Vint iGood, ipip, ipim;
  iGood.clear();
  ipip.clear();
  ipim.clear();
  Vp4 ppip, ppim;
  ppip.clear();
  ppim.clear();
	
	//Add xImpact and yImpact
	if(m_IsImpactReco)ImpactRecoTree->GetEntry(event);
	
	AllMCTruthTree->Fill();
	
	AnalysisParticleGun();

	AnalysisBhabha();
	/*
	SmartDataPtr<Event::ZddMcHitCol> zddMcHitCol(eventSvc(), "/Event/MC/ZddMcHitCol"); 
  if(!zddMcHitCol)
    { 
      G4cout<<"Could not retrieve zddMcHitCol"<<G4endl;
      return StatusCode::FAILURE;
    }	
  else
    {
      Event::ZddMcHitCol::iterator iMcHitCol = zddMcHitCol->begin();
      for (; iMcHitCol != zddMcHitCol->end(); iMcHitCol++)
      {
			int index = std::distance(zddMcHitCol->begin(),iMcHitCol);
			int trackID = (*iMcHitCol)->getTrackID();
			int pid = (*iMcHitCol)->getPDGCode();
			double preposX=(*iMcHitCol)->getPrePositionX();
			printf("%d %d %d %d %f \n", event, index, trackID,pid,preposX);
			
			HitColtrackID=trackID;
			HitColPID=pid;
			//ZDDMcHitColTree->Fill();
			}
		}
		*/
		
//   int nCharge = 0;
// 
//   Hep3Vector xorigin(0,0,0);
//   
//   //if (m_reader.isRunNumberValid(runNo)) {
//    IVertexDbSvc*  vtxsvc;
//   Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);
//   if(vtxsvc->isVertexValid()){
//     cout << "analysis_cZDD.cxx: VertexValid" << endl;
//   double* dbv = vtxsvc->PrimaryVertex(); 
//   double*  vv = vtxsvc->SigmaPrimaryVertex();  
// //    HepVector dbv = m_reader.PrimaryVertex(runNo);
// //    HepVector vv = m_reader.SigmaPrimaryVertex(runNo);
//     xorigin.setX(dbv[0]);
//     xorigin.setY(dbv[1]);
//     xorigin.setZ(dbv[2]);
//   }
// 
//   for(int i = 0; i < evtRecEvent->totalCharged(); i++){
//     EvtRecTrackIterator itTrk=evtRecTrkCol->begin() + i;
//     if(!(*itTrk)->isMdcTrackValid()) continue;
//     RecMdcTrack *mdcTrk = (*itTrk)->mdcTrack();
//     double pch=mdcTrk->p();
//     double x0=mdcTrk->x();
//     double y0=mdcTrk->y();
//     double z0=mdcTrk->z();
//     double phi0=mdcTrk->helix(1);
//     double xv=xorigin.x();
//     double yv=xorigin.y();
//     double Rxy=(x0-xv)*cos(phi0)+(y0-yv)*sin(phi0);
//     //    m_vx0 = x0;
//     //    m_vy0 = y0;
//     //    m_vz0 = z0;
//     //    m_vr0 = Rxy;
// 
//     HepVector a = mdcTrk->helix();
//     HepSymMatrix Ea = mdcTrk->err();
//     HepPoint3D point0(0.,0.,0.);   // the initial point for MDC recosntruction
//     HepPoint3D IP(xorigin[0],xorigin[1],xorigin[2]); 
//     VFHelix helixip(point0,a,Ea); 
//     helixip.pivot(IP);
//     HepVector vecipa = helixip.a();
//     double  Rvxy0=fabs(vecipa[0]);  //the nearest distance to IP in xy plane
//     double  Rvz0=vecipa[3];         //the nearest distance to IP in z direction
//     double  Rvphi0=vecipa[1];
//     //    m_rvxy0=Rvxy0;
//     //    m_rvz0=Rvz0;
//     //    m_rvphi0=Rvphi0;
// 
//     //    m_tuple1->write();
//     cout << "analysis_cZDD.cxx: Fill m_tuple1: vxyz" << endl;
// //    if(fabs(z0) >= m_vz0cut) continue;
// //    if(fabs(Rxy) >= m_vr0cut) continue;
//     
//     if(fabs(Rvz0) >= 10.0) continue;
//     if(fabs(Rvxy0) >= 1.0) continue;
//     
//     iGood.push_back(i);
//     nCharge += mdcTrk->charge();
//   }
//   
//   //
//   // Finish Good Charged Track Selection
//   //
//   int nGood = iGood.size();
//   log << MSG::DEBUG << "ngood, totcharge = " << nGood << " , " << nCharge << endreq;
//   if((nGood != 2)||(nCharge!=0)){
//     return StatusCode::SUCCESS;
//   }
//   Ncut1++;
// 
//   Vint iGam;
//   iGam.clear();
//   for(int i = evtRecEvent->totalCharged(); i< evtRecEvent->totalTracks(); i++) {
//     cout << "analysis_cZDD.cxx:Neutral Particle loop" << endl;
// 
//     EvtRecTrackIterator itTrk=evtRecTrkCol->begin() + i;
//     if(!(*itTrk)->isEmcShowerValid()) continue;
//     RecEmcShower *emcTrk = (*itTrk)->emcShower();
//     Hep3Vector emcpos(emcTrk->x(), emcTrk->y(), emcTrk->z());
//     // find the nearest charged track
//     double dthe = 200.;
//     double dphi = 200.;
//     double dang = 200.; 
//     for(int j = 0; j < evtRecEvent->totalCharged(); j++) {
//       EvtRecTrackIterator jtTrk = evtRecTrkCol->begin() + j;
//       if(!(*jtTrk)->isExtTrackValid()) continue;
//       RecExtTrack *extTrk = (*jtTrk)->extTrack();
//       if(extTrk->emcVolumeNumber() == -1) continue;
//       Hep3Vector extpos = extTrk->emcPosition();
//       //      double ctht = extpos.cosTheta(emcpos);
//       double angd = extpos.angle(emcpos);
//       double thed = extpos.theta() - emcpos.theta();
//       double phid = extpos.deltaPhi(emcpos);
//       thed = fmod(thed+CLHEP::twopi+CLHEP::twopi+pi, CLHEP::twopi) - CLHEP::pi;
//       phid = fmod(phid+CLHEP::twopi+CLHEP::twopi+pi, CLHEP::twopi) - CLHEP::pi;
//       if(angd < dang){
//         dang = angd;
//         dthe = thed;
//         dphi = phid;
//       }
//     }
//     if(dang>=200) continue;
//     double eraw = emcTrk->energy();
//     dthe = dthe * 180 / (CLHEP::pi);
//     dphi = dphi * 180 / (CLHEP::pi);
//     dang = dang * 180 / (CLHEP::pi);
//     //    m_dthe = dthe;
//     //    m_dphi = dphi;
//     //    m_dang = dang;
//     //    m_eraw = eraw;
//     //    m_tuple2->write();
//     cout << "analysis_cZDD.cxx: Fill m_tuple2: photon" << endl;
// 
//     if(eraw < m_energyThreshold) continue;
// //    if((fabs(dthe) < m_gammaThetaCut) && (fabs(dphi)<m_gammaPhiCut) ) continue;
//     if(fabs(dang) < m_gammaAngleCut) continue;
//     //
//     // good photon cut will be set here
//     //
//     iGam.push_back(i);
//   }
//   
//   //
//   // Finish Good Photon Selection
//   //
//   int nGam = iGam.size();
// 
//   log << MSG::DEBUG << "num Good Photon " << nGam  << " , " <<evtRecEvent->totalNeutral()<<endreq;
//   if(nGam<2){
//     return StatusCode::SUCCESS;
//   }
//   Ncut2++;
// 
// 
// 
//   //
//   //
//   // check dedx infomation
//   //
//   //
//   
//   if(m_checkDedx == 1) {
//     for(int i = 0; i < nGood; i++) {
//       cout << "analysis_cZDD.cxx: Check dE/dx Information" << endl;
// 
//       EvtRecTrackIterator  itTrk = evtRecTrkCol->begin() + iGood[i];
//       if(!(*itTrk)->isMdcTrackValid()) continue;
//       if(!(*itTrk)->isMdcDedxValid())continue;
//       RecMdcTrack* mdcTrk = (*itTrk)->mdcTrack();
//       RecMdcDedx* dedxTrk = (*itTrk)->mdcDedx();
//       //      m_ptrk = mdcTrk->p();
// 
//       //      m_chie = dedxTrk->chiE();
//       //      m_chimu = dedxTrk->chiMu();
//       //      m_chipi = dedxTrk->chiPi();
//       // m_chik = dedxTrk->chiK();
//       // m_chip = dedxTrk->chiP();
//       // m_ghit = dedxTrk->numGoodHits();
//       // m_thit = dedxTrk->numTotalHits();
//       // m_probPH = dedxTrk->probPH();
//       // m_normPH = dedxTrk->normPH();
//       //      m_tuple7->write();
//       cout << "analysis_cZDD.cxx: Fill m_tuple7: dedx" << endl;
// 
//     }
//   }
// 
//   //
//   // check TOF infomation
//   //
// 
// 
//   if(m_checkTof == 1) {
//     for(int i = 0; i < nGood; i++) {
//       EvtRecTrackIterator  itTrk = evtRecTrkCol->begin() + iGood[i];
//       if(!(*itTrk)->isMdcTrackValid()) continue;
//       if(!(*itTrk)->isTofTrackValid()) continue;
// 
//       RecMdcTrack * mdcTrk = (*itTrk)->mdcTrack();
//       SmartRefVector<RecTofTrack> tofTrkCol = (*itTrk)->tofTrack();
// 
//       double ptrk = mdcTrk->p();
// 
//       SmartRefVector<RecTofTrack>::iterator iter_tof = tofTrkCol.begin();
//       for(;iter_tof != tofTrkCol.end(); iter_tof++ ) { 
// 	cout << "analysis_cZDD.cxx: TofTrackCol" << endl;
// 
//         TofHitStatus *status = new TofHitStatus; 
//         status->setStatus((*iter_tof)->status());
//         if(!(status->is_barrel())){//endcap
//           if( !(status->is_counter()) ) continue; // ? 
//           if( status->layer()!=0 ) continue;//layer1
//           double path=(*iter_tof)->path(); // ? 
//           double tof  = (*iter_tof)->tof();
//           double ph   = (*iter_tof)->ph();
//           double rhit = (*iter_tof)->zrhit();
//           double qual = 0.0 + (*iter_tof)->quality();
//           double cntr = 0.0 + (*iter_tof)->tofID();
//           double texp[5];
//           for(int j = 0; j < 5; j++) {
//             double gb = ptrk/xmass[j];
//             double beta = gb/sqrt(1+gb*gb);
//             texp[j] = 10 * path /beta/velc;
//           }
//           // m_cntr_etof  = cntr;
//           // m_ptot_etof = ptrk;
//           // m_ph_etof   = ph;
//           // m_rhit_etof  = rhit;
//           // m_qual_etof  = qual;
//           // m_te_etof    = tof - texp[0];
//           // m_tmu_etof   = tof - texp[1];
//           // m_tpi_etof   = tof - texp[2];
//           // m_tk_etof    = tof - texp[3];
//           // m_tp_etof    = tof - texp[4];
// 	  //  m_tuple8->write();
// 	  cout << "analysis_cZDD.cxx: Fill m_tuple8: tofe" << endl;
// 
//         }
//         else {//barrel
//           if( !(status->is_counter()) ) continue; // ? 
//           if(status->layer()==1){ //layer1
//             double path=(*iter_tof)->path(); // ? 
//             double tof  = (*iter_tof)->tof();
//             double ph   = (*iter_tof)->ph();
//             double rhit = (*iter_tof)->zrhit();
//             double qual = 0.0 + (*iter_tof)->quality();
//             double cntr = 0.0 + (*iter_tof)->tofID();
//             double texp[5];
//             for(int j = 0; j < 5; j++) {
//               double gb = ptrk/xmass[j];
//               double beta = gb/sqrt(1+gb*gb);
//               texp[j] = 10 * path /beta/velc;
//             }
//  
//             // m_cntr_btof1  = cntr;
//             // m_ptot_btof1 = ptrk;
//             // m_ph_btof1   = ph;
//             // m_zhit_btof1  = rhit;
//             // m_qual_btof1  = qual;
//             // m_te_btof1    = tof - texp[0];
//             // m_tmu_btof1   = tof - texp[1];
//             // m_tpi_btof1   = tof - texp[2];
//             // m_tk_btof1    = tof - texp[3];
//             // m_tp_btof1    = tof - texp[4];
// 	    //            m_tuple9->write();
// 	    cout << "analysis_cZDD.cxx: Fill m_tuple9: tof1" << endl;
// 
//           }
// 
//           if(status->layer()==2){//layer2
//             double path=(*iter_tof)->path(); // ? 
//             double tof  = (*iter_tof)->tof();
//             double ph   = (*iter_tof)->ph();
//             double rhit = (*iter_tof)->zrhit();
//             double qual = 0.0 + (*iter_tof)->quality();
//             double cntr = 0.0 + (*iter_tof)->tofID();
//             double texp[5];
//             for(int j = 0; j < 5; j++) {
//               double gb = ptrk/xmass[j];
//               double beta = gb/sqrt(1+gb*gb);
//               texp[j] = 10 * path /beta/velc;
//             }
//  
//             // m_cntr_btof2  = cntr;
//             // m_ptot_btof2 = ptrk;
//             // m_ph_btof2   = ph;
//             // m_zhit_btof2  = rhit;
//             // m_qual_btof2  = qual;
//             // m_te_btof2    = tof - texp[0];
//             // m_tmu_btof2   = tof - texp[1];
//             // m_tpi_btof2   = tof - texp[2];
//             // m_tk_btof2    = tof - texp[3];
//             // m_tp_btof2    = tof - texp[4];
// 	    //            m_tuple10->write();
// 	    cout << "analysis_cZDD.cxx: Fill m_tuple10: tof2" << endl;
// 
//           } 
//         }
// 
//         delete status; 
//       } 
//     } // loop all charged track
//   }  // check tof
// 
// 
//   //
//   // Assign 4-momentum to each photon
//   // 
// 
//   Vp4 pGam;
//   pGam.clear();
//   for(int i = 0; i < nGam; i++) {
//     EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + iGam[i]; 
//     RecEmcShower* emcTrk = (*itTrk)->emcShower();
//     double eraw = emcTrk->energy();
//     double phi = emcTrk->phi();
//     double the = emcTrk->theta();
//     HepLorentzVector ptrk;
//     ptrk.setPx(eraw*sin(the)*cos(phi));
//     ptrk.setPy(eraw*sin(the)*sin(phi));
//     ptrk.setPz(eraw*cos(the));
//     ptrk.setE(eraw);
// 
// //    ptrk = ptrk.boost(-0.011,0,0);// boost to cms
// 
//     pGam.push_back(ptrk);
//   }
//   cout<<"before pid"<<endl;
//   //
//   // Assign 4-momentum to each charged track
//   //
//   ParticleID *pid = ParticleID::instance();
//   for(int i = 0; i < nGood; i++) {
//     EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + iGood[i]; 
//     //    if(pid) delete pid;
//     pid->init();
//     pid->setMethod(pid->methodProbability());
// //    pid->setMethod(pid->methodLikelihood());  //for Likelihood Method  
// 
//     pid->setChiMinCut(4);
//     pid->setRecTrack(*itTrk);
//     pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTofE()); // use PID sub-system
//     pid->identify(pid->onlyPion() | pid->onlyKaon());    // seperater Pion/Kaon
//     //    pid->identify(pid->onlyPion());
//     //  pid->identify(pid->onlyKaon());
//     pid->calculate();
//     if(!(pid->IsPidInfoValid())) continue;
//     RecMdcTrack* mdcTrk = (*itTrk)->mdcTrack();
//     // m_ptrk_pid = mdcTrk->p();
//     // m_cost_pid = cos(mdcTrk->theta());
//     // m_dedx_pid = pid->chiDedx(2);
//     // m_tof1_pid = pid->chiTof1(2);
//     // m_tof2_pid = pid->chiTof2(2);
//     // m_prob_pid = pid->probPion();
//     //    // m_tuple11->write();
//     cout << "analysis_cZDD.cxx: Fill m_tuple11: pid" << endl;
// 
// 
// //  if(pid->probPion() < 0.001 || (pid->probPion() < pid->probKaon())) continue;
//     if(pid->probPion() < 0.001) continue;
// //    if(pid->pdf(2)<pid->pdf(3)) continue; //  for Likelihood Method(0=electron 1=muon 2=pion 3=kaon 4=proton) 
// 
//     RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();//After ParticleID, use RecMdcKalTrack substitute RecMdcTrack
//     RecMdcKalTrack::setPidType  (RecMdcKalTrack::pion);//PID can set to electron, muon, pion, kaon and proton;The default setting is pion
// 
//     if(mdcKalTrk->charge() >0) {
//       ipip.push_back(iGood[i]);
//       HepLorentzVector ptrk;
//       ptrk.setPx(mdcKalTrk->px());
//       ptrk.setPy(mdcKalTrk->py());
//       ptrk.setPz(mdcKalTrk->pz());
//       double p3 = ptrk.mag();
//       ptrk.setE(sqrt(p3*p3+mpi*mpi));
// 
// //      ptrk = ptrk.boost(-0.011,0,0);//boost to cms
// 
//       ppip.push_back(ptrk);
//     } else {
//       ipim.push_back(iGood[i]);
//       HepLorentzVector ptrk;
//       ptrk.setPx(mdcKalTrk->px());
//       ptrk.setPy(mdcKalTrk->py());
//       ptrk.setPz(mdcKalTrk->pz());
//       double p3 = ptrk.mag();
//       ptrk.setE(sqrt(p3*p3+mpi*mpi));
// 
// //      ptrk = ptrk.boost(-0.011,0,0);//boost to cms
// 
//       ppim.push_back(ptrk);
//     }
//   }
// 
// /*
//   for(int i = 0; i < nGood; i++) {//for rhopi without PID
//     EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + iGood[i];
//     RecMdcTrack* mdcTrk = (*itTrk)->mdcTrack(); 
//     if(mdcTrk->charge() >0) {
//       ipip.push_back(iGood[i]);
//       HepLorentzVector ptrk;
//       ptrk.setPx(mdcTrk->px());
//       ptrk.setPy(mdcTrk->py());
//       ptrk.setPz(mdcTrk->pz());
//       double p3 = ptrk.mag();
//       ptrk.setE(sqrt(p3*p3+mpi*mpi));
//       ppip.push_back(ptrk);
//     } else {
//       ipim.push_back(iGood[i]);
//       HepLorentzVector ptrk;
//       ptrk.setPx(mdcTrk->px());
//       ptrk.setPy(mdcTrk->py());
//       ptrk.setPz(mdcTrk->pz());
//       double p3 = ptrk.mag();
//       ptrk.setE(sqrt(p3*p3+mpi*mpi));
//       ppim.push_back(ptrk);
//     }
//   }// without PID
// */
// 
//   int npip = ipip.size();
//   int npim = ipim.size();
//   if(npip*npim != 1) return SUCCESS;
// 
//   Ncut3++;
// 
// 
//   //
//   // Loop each gamma pair, check ppi0 and pTot 
//   //
// 
//   HepLorentzVector pTot;
//   for(int i = 0; i < nGam - 1; i++){
//     for(int j = i+1; j < nGam; j++) {
//       HepLorentzVector p2g = pGam[i] + pGam[j];
//       pTot = ppip[0] + ppim[0];
//       pTot += p2g;
//       // m_m2gg = p2g.m();
//       // m_etot = pTot.e();
//       //      m_tuple3 -> write();
//       cout << "analysis_cZDD.cxx: Fill m_tuple3: etot" << endl;
// 
//     }
//   }
//   
//   
//   RecMdcKalTrack *pipTrk = (*(evtRecTrkCol->begin()+ipip[0]))->mdcKalTrack();
//   RecMdcKalTrack *pimTrk = (*(evtRecTrkCol->begin()+ipim[0]))->mdcKalTrack();
// 
//   WTrackParameter wvpipTrk, wvpimTrk;
//   wvpipTrk = WTrackParameter(mpi, pipTrk->getZHelix(), pipTrk->getZError());
//   wvpimTrk = WTrackParameter(mpi, pimTrk->getZHelix(), pimTrk->getZError());
// 
// /* Default is pion, for other particles:
//   wvppTrk = WTrackParameter(mp, pipTrk->getZHelixP(), pipTrk->getZErrorP());//proton
//   wvmupTrk = WTrackParameter(mmu, pipTrk->getZHelixMu(), pipTrk->getZErrorMu());//muon
//   wvepTrk = WTrackParameter(me, pipTrk->getZHelixE(), pipTrk->getZErrorE());//electron
//   wvkpTrk = WTrackParameter(mk, pipTrk->getZHelixK(), pipTrk->getZErrorK());//kaon
// */
//   //
//   //    Test vertex fit
//   //
// 
//   HepPoint3D vx(0., 0., 0.);
//   HepSymMatrix Evx(3, 0);
//   double bx = 1E+6;
//   double by = 1E+6;
//   double bz = 1E+6;
//   Evx[0][0] = bx*bx;
//   Evx[1][1] = by*by;
//   Evx[2][2] = bz*bz;
// 
//   VertexParameter vxpar;
//   vxpar.setVx(vx);
//   vxpar.setEvx(Evx);
//   
//   VertexFit* vtxfit = VertexFit::instance();
//   vtxfit->init();
//   vtxfit->AddTrack(0,  wvpipTrk);
//   vtxfit->AddTrack(1,  wvpimTrk);
//   vtxfit->AddVertex(0, vxpar,0, 1);
//   if(!vtxfit->Fit(0)) return SUCCESS;
//   vtxfit->Swim(0);
//   
//   WTrackParameter wpip = vtxfit->wtrk(0);
//   WTrackParameter wpim = vtxfit->wtrk(1);
// 
//   //KinematicFit * kmfit = KinematicFit::instance();
//   KalmanKinematicFit * kmfit = KalmanKinematicFit::instance();
// 
//   //
//   //  Apply Kinematic 4C fit
//   // 
//   cout<<"before 4c"<<endl;
//   if(m_test4C==1) {
// //    double ecms = 3.097;
//     HepLorentzVector ecms(0.034,0,0,3.097);
// 
//     double chisq = 9999.;
//     int ig1 = -1;
//     int ig2 = -1;
//     for(int i = 0; i < nGam-1; i++) {
//       RecEmcShower *g1Trk = (*(evtRecTrkCol->begin()+iGam[i]))->emcShower();
//       for(int j = i+1; j < nGam; j++) {
// 	RecEmcShower *g2Trk = (*(evtRecTrkCol->begin()+iGam[j]))->emcShower();
// 	kmfit->init();
// 	kmfit->AddTrack(0, wpip);
// 	kmfit->AddTrack(1, wpim);
// 	kmfit->AddTrack(2, 0.0, g1Trk);
// 	kmfit->AddTrack(3, 0.0, g2Trk);
// 	kmfit->AddFourMomentum(0, ecms);
// 	bool oksq = kmfit->Fit();
// 	if(oksq) {
// 	  double chi2 = kmfit->chisq();
// 	  if(chi2 < chisq) {
// 	    chisq = chi2;
// 	    ig1 = iGam[i];
// 	    ig2 = iGam[j];
// 	  }
// 	}
//       }
//     }
//     
//     if(chisq < 200) { 
// 
//       RecEmcShower *g1Trk = (*(evtRecTrkCol->begin()+ig1))->emcShower();
//       RecEmcShower *g2Trk = (*(evtRecTrkCol->begin()+ig2))->emcShower();
//       kmfit->init();
//       kmfit->AddTrack(0, wpip);
//       kmfit->AddTrack(1, wpim);
//       kmfit->AddTrack(2, 0.0, g1Trk);
//       kmfit->AddTrack(3, 0.0, g2Trk);
//       kmfit->AddFourMomentum(0, ecms);
//       bool oksq = kmfit->Fit();
//       if(oksq) {
// 	HepLorentzVector ppi0 = kmfit->pfit(2) + kmfit->pfit(3);
// 	// m_mpi0 = ppi0.m();
// 	// m_chi1 = kmfit->chisq();
// 	//	m_tuple4->write();
//         Ncut4++;
//       }
//     }
//   }
//   
//   //
//   //  Apply Kinematic 5C Fit
//   //
// 
//   // find the best combination over all possible pi+ pi- gamma gamma pair
//   if(m_test5C==1) {
// //    double ecms = 3.097;
//     HepLorentzVector ecms(0.034,0,0,3.097);
//     double chisq = 9999.;
//     int ig1 = -1;
//     int ig2 = -1;
//     for(int i = 0; i < nGam-1; i++) {
//       RecEmcShower *g1Trk = (*(evtRecTrkCol->begin()+iGam[i]))->emcShower();
//       for(int j = i+1; j < nGam; j++) {
// 	RecEmcShower *g2Trk = (*(evtRecTrkCol->begin()+iGam[j]))->emcShower();
// 	kmfit->init();
// 	kmfit->AddTrack(0, wpip);
// 	kmfit->AddTrack(1, wpim);
// 	kmfit->AddTrack(2, 0.0, g1Trk);
// 	kmfit->AddTrack(3, 0.0, g2Trk);
// 	kmfit->AddResonance(0, 0.135, 2, 3);
// 	kmfit->AddFourMomentum(1, ecms);
// 	if(!kmfit->Fit(0)) continue;
// 	if(!kmfit->Fit(1)) continue;
// 	bool oksq = kmfit->Fit();
// 	if(oksq) {
// 	  double chi2 = kmfit->chisq();
// 	  if(chi2 < chisq) {
// 	    chisq = chi2;
// 	    ig1 = iGam[i];
// 	    ig2 = iGam[j];
// 	  }
// 	}
//       }
//     }
//   
// 
//     log << MSG::INFO << " chisq = " << chisq <<endreq;
// 
//     if(chisq < 200) {
//       RecEmcShower* g1Trk = (*(evtRecTrkCol->begin()+ig1))->emcShower();
//       RecEmcShower* g2Trk = (*(evtRecTrkCol->begin()+ig2))->emcShower();
// 
//       kmfit->init();
//       kmfit->AddTrack(0, wpip);
//       kmfit->AddTrack(1, wpim);
//       kmfit->AddTrack(2, 0.0, g1Trk);
//       kmfit->AddTrack(3, 0.0, g2Trk);
//       kmfit->AddResonance(0, 0.135, 2, 3);
//       kmfit->AddFourMomentum(1, ecms);
//       bool oksq = kmfit->Fit();
//       if(oksq){
// 	HepLorentzVector ppi0 = kmfit->pfit(2) + kmfit->pfit(3);
// 	HepLorentzVector prho0 = kmfit->pfit(0) + kmfit->pfit(1);
// 	HepLorentzVector prhop = ppi0 + kmfit->pfit(0);
// 	HepLorentzVector prhom = ppi0 + kmfit->pfit(1);
// 	
// 	// m_chi2  = kmfit->chisq();
// 	// m_mrh0 = prho0.m();
// 	// m_mrhp = prhop.m();
// 	// m_mrhm = prhom.m();
// 	double eg1 = (kmfit->pfit(2)).e();
// 	double eg2 = (kmfit->pfit(3)).e();
// 	double fcos = abs(eg1-eg2)/ppi0.rho();
// 	//	m_tuple5->write();
//         Ncut5++;
// 	// 
// 	//  Measure the photon detection efficiences via
// 	//          J/psi -> rho0 pi0
// 	//
// 	if(fabs(prho0.m()-0.770)<0.150) {  
// 	  if(fabs(fcos)<0.99) {
// 	    // m_fcos = (eg1-eg2)/ppi0.rho();
// 	    // m_elow =  (eg1 < eg2) ? eg1 : eg2;
// 	    //  m_tuple6->write();
//             Ncut6++;
// 	  }
// 	} // rho0 cut
//       }  //oksq
//     } 
//   }
  return StatusCode::SUCCESS;
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
StatusCode analysis_cZDD::finalize() {
	/*
  myFile->cd();
  myTree->Write();
  leoTree->Write();
	AllMCTruthTree->Write();
	particlegunTree->Write();
	//ZDDMcHitColTree->Write();
	myFile->Write();
  myFile->Close();
  //  delete myTree;
  //  delete myFile;
	*/
	
	myFile->cd();
	myFile->Write();
  myFile->Close();
	
  cout<<"total number:         "<<Ncut0<<endl;
  cout<<"nGood==2, nCharge==0: "<<Ncut1<<endl;
  cout<<"nGam>=2:              "<<Ncut2<<endl;
  cout<<"Pass Pid:             "<<Ncut3<<endl;
  cout<<"Pass 4C:              "<<Ncut4<<endl;
  cout<<"Pass 5C:              "<<Ncut5<<endl;
  cout<<"J/psi->rho0 pi0:      "<<Ncut6<<endl;
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in finalize()" << endmsg;
  return StatusCode::SUCCESS;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
void analysis_cZDD::DefineZDDCrystalsEdges()
{
m_XEdgesZDDRight[0]=0.;m_XEdgesZDDRight[1]=1.;m_XEdgesZDDRight[2]=2.;m_XEdgesZDDRight[3]=3.;
m_XEdgesZDDRight[4]=4.;

m_YEdgesZDDRight[0]=-3.5;m_YEdgesZDDRight[1]=-2.5;m_YEdgesZDDRight[2]=-1.5;m_YEdgesZDDRight[3]=-0.5;
m_YEdgesZDDRight[4]=0.5;m_YEdgesZDDRight[5]=1.5;m_YEdgesZDDRight[6]=2.5;m_YEdgesZDDRight[7]=3.5;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
vector<double> analysis_cZDD::GetCrystalCenter(int crystalNo, int partID)
{
vector<double> XYCenter;
XYCenter.resize(2);

switch(partID){ 
	 case 2:
	 switch( crystalNo )
	{
			case 101:
	  		XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
				XYCenter[1]=(m_YEdgesZDDRight[4]+m_YEdgesZDDRight[5])/2;
	  		break;
        case 102:
          XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[4]+m_YEdgesZDDRight[5])/2;       
          break;
        case 103:
	  			XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[4]+m_YEdgesZDDRight[5])/2; 
          break;
        case 104:
	  			XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[4]+m_YEdgesZDDRight[5])/2; 
          break;
			case 111:
	  			XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
					XYCenter[1]=(m_YEdgesZDDRight[5]+m_YEdgesZDDRight[6])/2;
          break;
        case 112:
	  			XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[5]+m_YEdgesZDDRight[6])/2;
          break;
        case 113:
	  			XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[5]+m_YEdgesZDDRight[6])/2;
          break;
        case 114:
	  			XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[5]+m_YEdgesZDDRight[6])/2;
          break;
        case 121:
	  			XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
					XYCenter[1]=(m_YEdgesZDDRight[6]+m_YEdgesZDDRight[7])/2;
          break;
        case 122:
	  			XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[6]+m_YEdgesZDDRight[7])/2;
          break;
        case 123:
	  			XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[6]+m_YEdgesZDDRight[7])/2;
	  			break;
        case 124:
	  			XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[6]+m_YEdgesZDDRight[7])/2;
          break;
        case 201:
	  			XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
					XYCenter[1]=(m_YEdgesZDDRight[2]+m_YEdgesZDDRight[3])/2;
          break;
        case 202:
	  			XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[2]+m_YEdgesZDDRight[3])/2;
          break;
        case 203:
	  			XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[2]+m_YEdgesZDDRight[3])/2;
          break;
        case 204:
	  			XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[2]+m_YEdgesZDDRight[3])/2;
          break;
        case 211:
	  			XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
					XYCenter[1]=(m_YEdgesZDDRight[1]+m_YEdgesZDDRight[2])/2;
          break;
        case 212:
	  			XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[1]+m_YEdgesZDDRight[2])/2;
          break;
        case 213:
	  			XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[1]+m_YEdgesZDDRight[2])/2;
          break;
        case 214:
	  			XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[1]+m_YEdgesZDDRight[2])/2;
          break;
        case 221:
          XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
					XYCenter[1]=(m_YEdgesZDDRight[0]+m_YEdgesZDDRight[1])/2;
          break;
        case 222:
          XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[0]+m_YEdgesZDDRight[1])/2;
          break;
        case 223:
          XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[0]+m_YEdgesZDDRight[1])/2;
          break;
        case 224:
          XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[0]+m_YEdgesZDDRight[1])/2;
          break;			
		}
	break;
	}
	
//printf("%d %d %f %f \n",crystalNo, partID, XYCenter[0],XYCenter[1]);
return XYCenter;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
void analysis_cZDD::AnalysisParticleGun()
{
/*
//All MC Truth
//Find particle from particle gun
for(int i=0;i<MCntracks;i++){
	if(MCprimPart[i]==1) {		
		MCpGunTrackID=i;
		break;
		}
	}
*/

MCpGunpartProp=MCpartProp[MCpGunTrackID];
MCpGuniniPosX=MCiniPosX[MCpGunTrackID];
MCpGuniniPosY=MCiniPosY[MCpGunTrackID];
MCpGuniniPosZ=MCiniPosZ[MCpGunTrackID];
MCpGunfinPosX=MCfinPosX[MCpGunTrackID];
MCpGunfinPosY=MCfinPosY[MCpGunTrackID];
MCpGunfinPosZ=MCfinPosZ[MCpGunTrackID];
MCpGunMomX=MCiniMomX[MCpGunTrackID];
MCpGunMomY=MCiniMomY[MCpGunTrackID];
MCpGunMomZ=MCiniMomZ[MCpGunTrackID];
MCpGunMomRho=MCiniMomRho[MCpGunTrackID];
MCpGunMomTheta=MCiniMomTheta[MCpGunTrackID];
MCpGunMomPhi=MCiniMomPhi[MCpGunTrackID];
MCpGunE=MCiniE[MCpGunTrackID];
MCpGunXCalcatZDD=MCXCalcatZDD[MCpGunTrackID];
MCpGunYCalcatZDD=MCYCalcatZDD[MCpGunTrackID];

m_hPGunMCiniMomX->Fill(MCpGunMomX);
m_hPGunMCiniMomY->Fill(MCpGunMomY);
m_hPGunMCiniMomZ->Fill(MCpGunMomZ);
m_hPGunMCiniE->Fill(MCpGunE);
m_hPGunMCiniTheta->Fill(MCpGunMomTheta);
m_hPGunMCiniPhi->Fill(MCpGunMomPhi);
m_hPGunMCfinPosX->Fill(MCpGunfinPosX);
m_hPGunMCfinPosY->Fill(MCpGunfinPosY);
m_hPGunMCfinPosZ->Fill(MCpGunfinPosZ);
m_hPGunMCExtrapolatedZDDHitXY->Fill(MCpGunXCalcatZDD,MCpGunYCalcatZDD);
m_hPGunMCfinPosXY->Fill(MCpGunfinPosX,MCpGunfinPosY);
m_hPGunMCfinPosZX->Fill(MCpGunfinPosZ,MCpGunfinPosX);
m_hPGunMCfinPosZY->Fill(MCpGunfinPosZ,MCpGunfinPosY);

if(!m_IsZDDhit) return;

m_hPGunZDDFirstHitXY->Fill(xFirstZDDHit,yFirstZDDHit);
m_hPGunZDDDeltaHitXExtrapolX->Fill(xFirstZDDHit-MCpGunXCalcatZDD);
m_hPGunZDDDeltaHitYExtrapolY->Fill(yFirstZDDHit-MCpGunYCalcatZDD);
m_hPGunZDDEreco->Fill(zdd_energy/1000);
m_hPGunZDDErecovsETruth->Fill(MCpGunE,zdd_energy/1000);
for (std::map<std::vector<int>,double>::iterator it=m_mapcrystalenergyZDD.begin(); it!=m_mapcrystalenergyZDD.end(); ++it) {
    vector <int> coord = it->first;
		int crystalNo=coord[0];
		int partID=coord[1];
		vector<double> crystalcenter=GetCrystalCenter(crystalNo,partID);
		
		double energy=it->second;
		if(energy>0 && partID==2) m_hPGunClustersZDDRight->Fill(crystalcenter[0],crystalcenter[1]);
		}
//Fill NTuple	
particlegunTree->Fill();
	
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
void analysis_cZDD::AnalysisBhabha()
{
bool IsBhabha= m_MCNPrimElectron==1 && m_MCNPrimPositron==1;
if(!IsBhabha) return;

MCBhabhanGammas=m_MCNPrimGammas;

MCBhabhaEleciniPosX=MCiniPosX[MCBhabhaElecTrackID];
MCBhabhaEleciniPosX=MCiniPosX[MCBhabhaElecTrackID];
MCBhabhaEleciniPosY=MCiniPosY[MCBhabhaElecTrackID];
MCBhabhaEleciniPosZ=MCiniPosZ[MCBhabhaElecTrackID];
MCBhabhaElecfinPosX=MCfinPosX[MCBhabhaElecTrackID];
MCBhabhaElecfinPosY=MCfinPosY[MCBhabhaElecTrackID];
MCBhabhaElecfinPosZ=MCfinPosZ[MCBhabhaElecTrackID];
MCBhabhaElecMomRho=MCiniMomRho[MCBhabhaElecTrackID];
MCBhabhaElecMomTheta=MCiniMomTheta[MCBhabhaElecTrackID];
MCBhabhaElecMomPhi=MCiniMomPhi[MCBhabhaElecTrackID];
MCBhabhaElecE=MCiniE[MCBhabhaElecTrackID];
MCBhabhaElecXCalcatZDD=MCXCalcatZDD[MCBhabhaElecTrackID];
MCBhabhaElecYCalcatZDD=MCYCalcatZDD[MCBhabhaElecTrackID];

MCBhabhaPositroniniPosX=MCiniPosX[MCBhabhaPositronTrackID];
MCBhabhaPositroniniPosX=MCiniPosX[MCBhabhaPositronTrackID];
MCBhabhaPositroniniPosY=MCiniPosY[MCBhabhaPositronTrackID];
MCBhabhaPositroniniPosZ=MCiniPosZ[MCBhabhaPositronTrackID];
MCBhabhaPositronfinPosX=MCfinPosX[MCBhabhaPositronTrackID];
MCBhabhaPositronfinPosY=MCfinPosY[MCBhabhaPositronTrackID];
MCBhabhaPositronfinPosZ=MCfinPosZ[MCBhabhaPositronTrackID];
MCBhabhaPositronMomRho=MCiniMomRho[MCBhabhaPositronTrackID];
MCBhabhaPositronMomTheta=MCiniMomTheta[MCBhabhaPositronTrackID];
MCBhabhaPositronMomPhi=MCiniMomPhi[MCBhabhaPositronTrackID];
MCBhabhaPositronE=MCiniE[MCBhabhaPositronTrackID];
MCBhabhaPositronXCalcatZDD=MCXCalcatZDD[MCBhabhaPositronTrackID];
MCBhabhaPositronYCalcatZDD=MCYCalcatZDD[MCBhabhaPositronTrackID];

Hep3Vector V3Electron;
V3Electron.setRThetaPhi (MCBhabhaElecMomRho, MCBhabhaElecMomTheta, MCBhabhaElecMomPhi);
Hep3Vector V3Positron;
V3Positron.setRThetaPhi (MCBhabhaPositronMomRho, MCBhabhaPositronMomTheta, MCBhabhaPositronMomPhi);
MCBhabhaMomSum3VElectronPositron=(V3Electron+V3Positron).r();

printf("****** pz(e-) = %f pz(e+) = %f *******",V3Electron.z(),V3Positron.z());

Hep3Vector V3MissMom;
V3MissMom=V3Electron+V3Positron;


for(int k=0;k<MCBhabhanGammas;k++)
	{
	MCBhabhaGaminiPosX[k]=MCiniPosX[MCBhabhaGamTrackID[k]];
	MCBhabhaGaminiPosX[k]=MCiniPosX[MCBhabhaGamTrackID[k]];
	MCBhabhaGaminiPosY[k]=MCiniPosY[MCBhabhaGamTrackID[k]];
	MCBhabhaGaminiPosZ[k]=MCiniPosZ[MCBhabhaGamTrackID[k]];
	MCBhabhaGamfinPosX[k]=MCfinPosX[MCBhabhaGamTrackID[k]];
	MCBhabhaGamfinPosY[k]=MCfinPosY[MCBhabhaGamTrackID[k]];
	MCBhabhaGamfinPosZ[k]=MCfinPosZ[MCBhabhaGamTrackID[k]];
	MCBhabhaGamMomRho[k]=MCiniMomRho[MCBhabhaGamTrackID[k]];
	MCBhabhaGamMomTheta[k]=MCiniMomTheta[MCBhabhaGamTrackID[k]];
	MCBhabhaGamMomPhi[k]=MCiniMomPhi[MCBhabhaGamTrackID[k]];
	MCBhabhaGamE[k]=MCiniE[MCBhabhaGamTrackID[k]];
	MCBhabhaGamXCalcatZDD[k]=MCXCalcatZDD[MCBhabhaGamTrackID[k]];
	MCBhabhaGamYCalcatZDD[k]=MCYCalcatZDD[MCBhabhaGamTrackID[k]];
	
	Hep3Vector V3Gam;
	V3Gam.setRThetaPhi (MCBhabhaGamMomRho[k], MCBhabhaGamMomTheta[k], MCBhabhaGamMomPhi[k]);
	V3MissMom=V3MissMom+V3Gam;
	}

MCBhabhaMissMomX=V3MissMom.x();
MCBhabhaMissMomY=V3MissMom.y();
MCBhabhaMissMomZ=V3MissMom.z();

double Etot=MCBhabhaElecE+MCBhabhaPositronE;

m_hMCBhabhaEleciniE->Fill(MCBhabhaElecE);
m_hMCBhabhaEleciniMomRho->Fill(MCBhabhaElecMomRho);
m_hMCBhabhaEleciniMomTheta->Fill(MCBhabhaElecMomTheta);
m_hMCBhabhaEleciniMomPhi->Fill(MCBhabhaElecMomPhi);
m_hMCBhabhaElecExtrapolatedZDDHitXY->Fill(MCBhabhaElecXCalcatZDD,MCBhabhaElecYCalcatZDD);
m_hMCBhabhaElecfinPosXY->Fill(MCBhabhaElecfinPosX,MCBhabhaElecfinPosY);
m_hMCBhabhaElecfinPosZX->Fill(MCBhabhaElecfinPosZ,MCBhabhaElecfinPosX);
m_hMCBhabhaElecfinPosZY->Fill(MCBhabhaElecfinPosZ,MCBhabhaElecfinPosY);

m_hMCBhabhaPositroniniE->Fill(MCBhabhaPositronE);
m_hMCBhabhaPositroniniMomRho->Fill(MCBhabhaPositronMomRho);
m_hMCBhabhaPositroniniMomTheta->Fill(MCBhabhaPositronMomTheta);
m_hMCBhabhaPositroniniMomPhi->Fill(MCBhabhaPositronMomPhi);
m_hMCBhabhaPositronExtrapolatedZDDHitXY->Fill(MCBhabhaPositronXCalcatZDD,MCBhabhaPositronYCalcatZDD);
m_hMCBhabhaPositronfinPosXY->Fill(MCBhabhaPositronfinPosX,MCBhabhaPositronfinPosY);
m_hMCBhabhaPositronfinPosZX->Fill(MCBhabhaPositronfinPosZ,MCBhabhaPositronfinPosX);
m_hMCBhabhaPositronfinPosZY->Fill(MCBhabhaPositronfinPosZ,MCBhabhaPositronfinPosY);
	
if(!m_IsZDDhit) return;

m_hZDDBhabhaFirstHitXY->Fill(xFirstZDDHit,yFirstZDDHit);
m_hZDDBhabhaEreco->Fill(zdd_energy/1000);
m_hZDDBhabhaErecovsETruth->Fill(Etot,zdd_energy/1000);
m_hZDDBhabhaErecoLeft->Fill(zddLeft_energy);
m_hZDDBhabhaErecoRight->Fill(zddRight_energy);
m_hZDDBhabhaErecoLeftvsRight->Fill(zddRight_energy,zddLeft_energy);
//printf("****** Etot = %f zdd_energy/1000= %f ****** \n",Etot,zdd_energy/1000);

BhabhaTree->Fill();	
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
