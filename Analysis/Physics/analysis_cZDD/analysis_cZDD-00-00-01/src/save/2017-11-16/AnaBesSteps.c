#include <TFile.h>
#include <TDirectory.h>
#include <TMath.h>
#include <TDatabasePDG.h>
#include <TParticlePDG.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TPolyLine3D.h>
#include <TVector3.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TView.h>
#include <TAxis3D.h>
#include <TPolyMarker3D.h>
#include <TImage.h>
#include <TASImage.h>

#include "Math/GenVector/Plane3D.h"

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

//#ifdef __CINT__
//#pragma link C++ class std::vector<double>;
//#endif

#ifdef __MAKECINT__
//#pragma link C++ class vector<double>+;
#pragma link C++ class vector<string>+;
#endif
    
using namespace std;

TFile* m_file;
TTree* m_tree;

int m_evtID;

double m_zdd_energy;
double m_zddLeft_energy;
double m_zddRight_energy;

std::vector<int> *m_vectrackIDStep = 0;
TBranch *m_bvectrackIDStep=0;

std::vector<int> *m_vecparentIDStep = 0;
TBranch *m_bvecparentIDStep=0;

std::vector<int> *m_vecPDGCodeStep = 0;
TBranch *m_bvecPDGCodeStep=0;

std::vector<double> *m_vecXPreStep = 0;
TBranch *m_bvecXPreStep=0;

std::vector<double> *m_vecYPreStep = 0;
TBranch *m_bvecYPreStep=0;

std::vector<double> *m_vecZPreStep = 0;
TBranch *m_bvecZPreStep=0;

std::vector<double> *m_vecXPostStep = 0;
TBranch *m_bvecXPostStep=0;

std::vector<double> *m_vecYPostStep = 0;
TBranch *m_bvecYPostStep=0;

std::vector<double> *m_vecZPostStep = 0;
TBranch *m_bvecZPostStep=0;

std::vector<double> *m_vecPxPreStep = 0;
TBranch *m_bvecPxPreStep=0;

std::vector<double> *m_vecPyPreStep = 0;
TBranch *m_bvecPyPreStep=0;

std::vector<double> *m_vecPzPreStep = 0;
TBranch *m_bvecPzPreStep=0;

std::vector< std::string > *m_vecVolPreStep = 0;
TBranch *m_bvecVolPreStep=0;

std::vector< std::string > *m_vecVolPostStep = 0;
TBranch *m_bvecVolPostStep=0;

std::vector< double > *m_vecETotPreStep = 0;
TBranch *m_bvecETotPreStep=0;

std::vector< double > *m_vecETotPostStep = 0;
TBranch *m_bvecETotPostStep=0;

std::vector< double > *m_vecEDepStep = 0;
TBranch *m_bvecEDepStep=0;

std::map<int,std::vector<TVector3> > m_mapTrackPoints;
std::map<int,std::vector<TVector3> > m_mapTrackMomentum;
std::map<int,int> m_mapTrackPDG;

std::vector<TPolyLine3D> m_Trajectories;


std::string m_outpath;

std::string m_option;

TDatabasePDG* pdg;

double eTotPrimaries;
double eTotExitPipe;
double eTotEntersZDD;
double eGammasEntersZDD;
double eElectronsEntersZDD;
double ePositronsEntersZDD;
double eElectronsPositronsEntersZDD;

double eDepBeforeZDD;

int nHitsZDD;
int nHitsGammasZDD;
int nHitsElectronsZDD;
int nHitsPositronsZDD;
int nHitsZDDEast;
int nHitsZDDWest;

int nHitsPrimaryZDD;
int nHitsPrimaryZDDEast;
int nHitsPrimaryZDDWest;

int nExitWindow;
	

TH1D* hNHitsZDD;
TH1D* hNHitsGammasZDD;
TH1D* hNHitsElectronsZDD;
TH1D* hNHitsPositronsZDD;
TH1D* hNHitsZDDEast;
TH1D* hNHitsZDDWest;

TH1D* hNHitsPrimaryZDD;
TH1D* hNHitsPrimaryZDDEast;
TH1D* hNHitsPrimaryZDDWest;

TH1D* hPDGHitsZDD;
TH1D* hPDGHitsZDDEast;
TH1D* hPDGHitsZDDWest;

TH1D* hPDGHitsPrimaryZDD;
TH1D* hPDGHitsPrimaryZDDEast;
TH1D* hPDGHitsPrimaryZDDWest;

TH1D* hETotPrimaries;
TH1D* hETotEntersZDD;
TH1D* hEGammasEntersZDDOverETotEntersZDD;
TH1D* hEElectronsEntersZDDOverETotEntersZDD;
TH1D* hEPositronsEntersZDDOverETotEntersZDD;
TH1D* hETotExitPipe;
TH1D* hEDepZDD;

TH1D* hETotEntersZDDOverETotPrimaries;
TH1D* hEGammasEntersZDDOverETotPrimaries;
TH1D* hEElectronsEntersZDDOverETotPrimaries;
TH1D* hEPositronsEntersZDDOverETotPrimaries;
TH1D* hEElectronsPositronsEntersZDDOverETotPrimaries;
TH1D* hEDepZDDOverETotPrimaries;
TH1D* hEDepZDDOverETotEntersZDD;
//TH1D* hETotEntersZDDOverETotExitPipe;
TH1D* hETotEntersZDDOverETotExitPipe;

map<string,TH2D*> m_maph2DPlaneHits;

map<string,ROOT::Math::Plane3D*> m_mapPlanes;

const double m_WorldXMin=-100.;
const double m_WorldXMax=100.;

const double m_WorldYMin=-100.;
const double m_WorldYMax=100.;

//const double m_WorldZMin=-3500.;
//const double m_WorldZMax=3500;

const double m_WorldZMin=-5000.;
const double m_WorldZMax=5000.;

vector <string> m_crystalList;
vector <string> m_EnergyList;

void Initialize()
{
m_mapTrackPoints.clear();
m_mapTrackMomentum.clear();
m_mapTrackPDG.clear();
m_Trajectories.clear();

}

void AddPointToTrajectory(int trackID,TVector3 vec)
{
m_mapTrackPoints[trackID].push_back(vec);	
}

TPolyLine3D MakeTPolyLine3D(std::vector<TVector3> traj)
{
TPolyLine3D poly3D(traj.size());
for(int k=0;k<traj.size();k++)
	{
	//printf("MakeTPolyLine3D traj[k].Z()=%f \n",traj[k].Z());
	//sleep(1);
	poly3D.SetPoint(k,traj[k].X(),traj[k].Y(),traj[k].Z());
	}
return poly3D;	
}

TVector3 getIntersectLinePlane(ROOT::Math::Plane3D* plane3D,TVector3 a, TVector3 b) {
		ROOT::Math::Plane3D::Scalar d = plane3D->HesseDistance();
		ROOT::Math::Plane3D::Vector n = plane3D->Normal();
    TVector3 ba = b-a;
    float nDotA = n.Dot(a);
    float nDotBA = n.Dot(ba);
		float t = ((d - nDotA)/nDotBA);
		
		if(nDotBA==0 || t>1){
			//Check if AB parallel to the plane 
			//Or if the plane is within A and B (t<1)
			TVector3 vExcept(-1e4,-1e4,-1e4);
			return vExcept;
		}
    else return a + (t * ba);
}

vector<TVector3> getIntersectPolyLinePlane(ROOT::Math::Plane3D* plane3D,TPolyLine3D* poly3D){
	vector<TVector3> intersectPoints;
	Float_t* vecP = poly3D->GetP();
	Int_t npoints = poly3D->GetN();
		for(int i=0;i<npoints-1;i++){
		int iXA = i*3;
		int iYA = i*3+1;
		int iZA = i*3+2;
		TVector3 a(vecP[iXA],vecP[iYA],vecP[iZA]);
		
		int iXB = (i+1)*3;
		int iYB = (i+1)*3+1;
		int iZB = (i+1)*3+2;
		TVector3 b(vecP[iXB],vecP[iYB],vecP[iZB]);
		
		getIntersectLinePlane(plane3D,a,b);
		}
	return intersectPoints;
}


void TrackPointsToTrajectories(){

for (std::map<int,std::vector<TVector3> > ::iterator it=m_mapTrackPoints.begin(); it!=m_mapTrackPoints.end(); ++it)
		{
		int trackIndex = it->first;
		std::vector<TVector3> traj=it->second;
		
		if(m_option.find("HorizontalZ ") != std::string::npos) for(int k=0;k<traj.size();k++) traj[k].RotateX(-TMath::Pi()/2);
		TPolyLine3D poly3D = MakeTPolyLine3D(traj);
		m_Trajectories.push_back(poly3D);
		
		//TrajPDGs.push_back(m_mapTrackPDG[trackIndex]);
		}
}

void DrawTrajectories()
{
//std::string outname=m_outpath+".gif";
std::string outname=m_outpath+".gif+10";

std::vector<TPolyLine3D> Trajectories;
std::vector<int> TrajPDGs;

int counter=0;
for (std::map<int,std::vector<TVector3> > ::iterator it=m_mapTrackPoints.begin(); it!=m_mapTrackPoints.end(); ++it)
		{
		int trackIndex = it->first;
		std::vector<TVector3> traj=it->second;
		//if(counter==0)
		//	{
		//	for(int k=0;k<traj.size();k++) printf("%f %f %f \n",traj[k].X(),traj[k].Y(),traj[k].Z());
		//	}
		
		//for(int k=0;k<traj.size();k++) printf("%d %f %f %f \n",counter, traj[k].X(),traj[k].Y(),traj[k].Z());
		//printf("\n");
		
		if(m_option.find("HorizontalZ ") != std::string::npos) for(int k=0;k<traj.size();k++) traj[k].RotateX(-TMath::Pi()/2);
		TPolyLine3D poly3D = MakeTPolyLine3D(traj);
		Trajectories.push_back(poly3D);
		
		TrajPDGs.push_back(m_mapTrackPDG[trackIndex]);
		
		counter++;
		}

//TCanvas *c1 = new TCanvas("c1","",1280,720);
//TCanvas *c1 = new TCanvas("c1","",1000.,2500.);
TCanvas *c1 = new TCanvas("c1","",800.,1800.);
c1->Divide(2,4);
vector< vector< TPolyLine3D > > vtraj;
vtraj.resize(4);
for(int padnb=1;padnb<=8;padnb++)
	{
	c1->cd(padnb);
	if(padnb<=4)
		{
TView *view = TView::CreateView(1);

if(m_option.find("HorizontalZ ") != std::string::npos) view->SetRange(m_WorldXMin,m_WorldZMin,m_WorldYMin,m_WorldXMax,m_WorldZMax,m_WorldYMax);
else view->SetRange(m_WorldXMin,m_WorldYMin,m_WorldZMin,m_WorldXMax,m_WorldYMax,m_WorldZMax);
//printf("Trajectories.size() = %d \n",Trajectories.size());

//TAxis3D *axis = TAxis3D::GetPadAxis(gPad);

//view->RotateView(TMath::Pi()/2,0.);
//view->RotateView(TMath::Pi()/2,3*TMath::Pi()/2);
if(m_option.find("ViewPipeZDDRegionWest ") != std::string::npos) view->SetRange(m_WorldXMin,m_WorldYMin,2900.,m_WorldXMax,m_WorldYMax,3600.,1);
if(m_option.find("ViewPipeZDDRegionEast ") != std::string::npos) view->SetRange(m_WorldXMin,m_WorldYMin,-3600.,m_WorldXMax,m_WorldYMax,-2900.,1);
for(int k=0;k<Trajectories.size();k++)
	{
	Trajectories[k].Draw();
	if(TrajPDGs[k]==22) Trajectories[k].SetLineColor(3);
	if(TrajPDGs[k]==11) Trajectories[k].SetLineColor(4);
	if(TrajPDGs[k]==-11) Trajectories[k].SetLineColor(2);
	}

view->ShowAxis();
//view->RotateView(TMath::Pi(),0.);
if(padnb==1) 
	{
	/*
	TLatex* ltx = new TLatex();
	ltx->SetNDC();
	char strltx[256];
	sprintf(strltx,"event #%d",m_evtID);
	ltx->SetTextAngle(90.);
	ltx->DrawLatex(0.3,0.75,strltx);
	*/
	
	
	view->Front();
	}
if(padnb==2) view->Side();
if(padnb==3) {view->Top();}
//if(padnb==4) {view->RotateView(TMath::Pi()/2,0.);}
	}
if(padnb==5)	{hETotExitPipe->Draw("hbar");}	
if(padnb==6)	{hETotEntersZDD->Draw("hbar");}	
if(padnb==7)	{hEDepZDD->Draw("hbar");}
if(padnb==8)	
		{
		char strltx[256];
	vector<TLatex*> vltx;
	
	double Xltx=0.25;
	double Xstep=0.05;
	double Yltx=0.1;
	
	vltx.push_back(new TLatex());
	vltx.back()->SetNDC();
	//sprintf(strltx,"event #%d",m_evtID);
	vltx.back()->SetTextAngle(90.);
	vltx.back()->DrawLatex(Xltx,Yltx,strltx);
	
	vltx.push_back(new TLatex());
	vltx.back()->SetNDC();
	sprintf(strltx,"E_{primaries} ZDD = %f",eTotPrimaries);
	vltx.back()->SetTextAngle(90.);
	Xltx=Xltx+Xstep;
	vltx.back()->DrawLatex(Xltx,Yltx,strltx);
	
	vltx.push_back(new TLatex());
	vltx.back()->SetNDC();
	sprintf(strltx,"N exit pipe = %d E_{Tot} exit pipe = %f",nExitWindow,eTotExitPipe);
	vltx.back()->SetTextAngle(90.);
	Xltx=Xltx+Xstep;
	vltx.back()->DrawLatex(Xltx,Yltx,strltx);
	
	vltx.push_back(new TLatex());
	vltx.back()->SetNDC();
	sprintf(strltx,"N enters ZDD = %d E_{Tot} enters ZDD = %f",nHitsZDD,eTotEntersZDD);
	vltx.back()->SetTextAngle(90.);
	Xltx=Xltx+Xstep;
	vltx.back()->DrawLatex(Xltx,Yltx,strltx);
	
	vltx.push_back(new TLatex());
	vltx.back()->SetNDC();
	sprintf(strltx,"E_{Dep} ZDD = %f",m_zdd_energy);
	vltx.back()->SetTextAngle(90.);
	Xltx=Xltx+Xstep;
	vltx.back()->DrawLatex(Xltx,Yltx,strltx);
		}
	}

c1->Print(outname.c_str());

/*
TImage *img = TImage::Create();
TCanvas *c2 = new TCanvas("c2","",1000.,2000.);
img->FromPad(c1);
img->Flip(90);
img->Draw("");
c2->Print(outname.c_str());
*/
		
}

void AnaBesSteps(string inputname, string option, string outpath, int NEntries=-1)
{

gStyle->SetLabelSize(0.030, "x");
gStyle->SetLabelSize(0.030, "y");
gStyle->SetLabelSize(0.030, "z");

gStyle->SetTitleSize(0.045, "x");
gStyle->SetTitleSize(0.045, "y");
gStyle->SetTitleSize(0.045, "z");
	 	 
m_option=option;

TDatabasePDG *pdg = new TDatabasePDG();

gROOT->SetBatch();
m_file = TFile::Open(inputname.c_str());
m_tree = (TTree*) m_file->Get("Impact_reco");

m_tree->SetBranchAddress("evtID",&m_evtID);
m_tree->SetBranchAddress("zdd_energy",&m_zdd_energy);
m_tree->SetBranchAddress("zddLeft_energy",&m_zddLeft_energy);
m_tree->SetBranchAddress("zddRight_energy",&m_zddRight_energy);

m_tree->SetBranchAddress("trackIDStep",&m_vectrackIDStep,&m_bvectrackIDStep );
m_tree->SetBranchAddress("parentIDStep",&m_vecparentIDStep,&m_bvecparentIDStep );
m_tree->SetBranchAddress("PDGCodeStep",&m_vecPDGCodeStep,&m_bvecPDGCodeStep );

m_tree->SetBranchAddress("xPreStep",&m_vecXPreStep,&m_bvecXPreStep);
m_tree->SetBranchAddress("yPreStep",&m_vecYPreStep,&m_bvecYPreStep);
m_tree->SetBranchAddress("zPreStep",&m_vecZPreStep,&m_bvecZPreStep);

m_tree->SetBranchAddress("xPostStep",&m_vecXPostStep,&m_bvecXPostStep);
m_tree->SetBranchAddress("yPosttep",&m_vecYPostStep,&m_bvecYPostStep);
m_tree->SetBranchAddress("zPostStep",&m_vecZPostStep,&m_bvecZPostStep);

m_tree->SetBranchAddress("PxPreStep",&m_vecPxPreStep,&m_bvecPxPreStep);
m_tree->SetBranchAddress("PyPreStep",&m_vecPyPreStep,&m_bvecPyPreStep);
m_tree->SetBranchAddress("PzPreStep",&m_vecPzPreStep,&m_bvecPzPreStep);

m_tree->SetBranchAddress("VolumePreStep",&m_vecVolPreStep,&m_bvecVolPreStep);
m_tree->SetBranchAddress("VolumePostStep",&m_vecVolPostStep,&m_bvecVolPostStep);

m_tree->SetBranchAddress("ETotPreStep",&m_vecETotPreStep,&m_bvecETotPreStep);
m_tree->SetBranchAddress("ETotPostStep",&m_vecETotPostStep,&m_bvecETotPostStep);

m_tree->SetBranchAddress("eDepStep",&m_vecEDepStep,&m_bvecEDepStep);

m_outpath=outpath;
string outname=outpath+".root";
TFile* outfile = new TFile(outname.c_str(),"RECREATE");

char cmd[256];
sprintf(cmd,"rm %s.gif",outpath.c_str());
system(cmd);

hNHitsZDD = new TH1D("hNHitsZDD",";Number of hits on ZDD;",100,0.,100.);
hNHitsGammasZDD = new TH1D("hNHitsGammasZDD",";Number of #gamma hits on ZDD;",100,0.,100.);
hNHitsElectronsZDD = new TH1D("hNHitsElectronsZDD",";Number of e^{-} hits on ZDD;",100,0.,100.);
hNHitsPositronsZDD = new TH1D("hNHitsPositronsZDD",";Number of e^{+} hits on ZDD;",100,0.,100.);
hNHitsZDDEast = new TH1D("hNHitsZDDEast",";Number of hits on ZDD East;",100,0.,100.);
hNHitsZDDWest = new TH1D("hNHitsZDDWest",";Number of hits on ZDD West;",100,0.,100.);

hNHitsPrimaryZDD = new TH1D("hNHitsPrimaryZDD","",100,0.,100.);
hNHitsPrimaryZDDEast = new TH1D("hNHitsPrimaryZDDEast","",100,0.,100.);
hNHitsPrimaryZDDWest = new TH1D("hNHitsPrimaryZDDWest","",100,0.,100.);

hPDGHitsZDD = new TH1D("hPDGHitsZDD",";PDG Code;",200.,-100.,100.);
hPDGHitsZDDEast = new TH1D("hPDGHitsZDDEast",";PDG Code;",200.,-100.,100.);
hPDGHitsZDDWest = new TH1D("hPDGHitsZDDWest",";PDG Code;",200.,-100.,100.);

hPDGHitsPrimaryZDD = new TH1D("hPDGHitsPrimaryZDD",";PDG Code;",200.,-100.,100.);
hPDGHitsPrimaryZDDEast = new TH1D("hPDGHitsPrimaryZDDEast",";PDG Code;",200.,-100.,100.);
hPDGHitsPrimaryZDDWest = new TH1D("hPDGHitsPrimaryZDDWest",";PDG Code;",200.,-100.,100.);

hETotPrimaries = new TH1D("hETotPrimaries",";E^{IP}_{Primary particles}(MeV);",256,0.,2000.);
hETotEntersZDD = new TH1D("hETotEntersZDD",";E^{ZDD front}_{Tot}(MeV);",256,0.,2000.);
hETotEntersZDD->SetFillColor(1);
hEGammasEntersZDDOverETotEntersZDD = new TH1D("hEGammasEntersZDDOverETotEntersZDD",";E^{ZDD front}_{#gamma}(MeV);",256,0.,2.);
hEElectronsEntersZDDOverETotEntersZDD = new TH1D("hEElectronsEntersZDDOverETotEntersZDD",";E^{ZDD front}_{e-}(MeV);",256,0.,2.);
hEPositronsEntersZDDOverETotEntersZDD = new TH1D("hEPositronsEntersZDDOverETotEntersZDD",";E^{ZDD front}_{e+}(MeV);",256,0.,2.);
hETotExitPipe = new TH1D("hETotExitPipe",";E^{Pipe exit}_{Tot};",256,0.,2000.);
hETotExitPipe->SetFillColor(1);
hEDepZDD = new TH1D("hEDepZDD",";E^{ZDD}_{Dep};",256,0.,2000.);
hEDepZDD->SetFillColor(1);

hETotEntersZDDOverETotPrimaries = new TH1D("hETotEntersZDDOverETotPrimaries",";E^{ZDD front}_{Tot}/E^{IP}_{Primary particles};",256,0.,2.);
TH1D* hEGammasEntersZDDOverETotPrimaries = new TH1D("hEGammasEntersZDDOverETotPrimaries",";E^{ZDD front}_{#gamma}/E^{IP}_{Primary particles};",256,0.,2.);
TH1D* hEElectronsEntersZDDOverETotPrimaries = new TH1D("hEElectronsEntersZDDOverETotPrimaries",";E^{ZDD front}_{e-}/E^{IP}_{Primary particles};",256,0.,2.);
TH1D* hEPositronsEntersZDDOverETotPrimaries= new TH1D("hEPositronsEntersZDDOverETotPrimaries",";E^{ZDD front}_{e+}/E^{IP}_{Primary particles};",256,0.,2.);
TH1D* hEElectronsPositronsEntersZDDOverETotPrimaries = new TH1D("hEElectronsPositronsEntersZDDOverETotPrimaries",";E^{ZDD front}_{e+ e-}/E^{IP}_{Primary particles};",256,0.,2.);

hEDepZDDOverETotPrimaries = new TH1D("hEDepZDDOverETotPrimaries",";E^{ZDD}_{Dep}/E^{IP}_{Primary particles};",256,0.,2.);

hEDepZDDOverETotEntersZDD = new TH1D("hEDepZDDOverETotEntersZDD",";E^{ZDD}_{Dep}/E^{ZDD front}_{Tot};",256,0.,2.);
//hETotEntersZDDOverETotExitPipe = new TH1D("hETotEntersZDDOverETotExitPipe",";;",256,0.,100.);
hETotEntersZDDOverETotExitPipe = new TH1D("hETotEntersZDDOverETotExitPipe",";E^{ZDD front}_{Tot}/E^{Pipe exit}_{Tot};",256,0.,2.);

m_maph2DPlaneHits["X=10cm"] = new TH2D("hX=10cm","",1000,-3500.,3500.,1000,-100,100);

ROOT::Math::Plane3D::Vector n(1,0,0);
ROOT::Math::Plane3D::Point p(10,0,0);
m_mapPlanes["X=10cm"] = new ROOT::Math::Plane3D(n,p);

if(m_option.find("ParticleGun ") != std::string::npos)
	{
	TH1D* hEPGunIP = new TH1D("hEPGunIP","",1000,0.,2000.);
	}

if(NEntries==-1) NEntries=m_tree->GetEntries();

for(int entry=0;entry<NEntries;entry++)
	{
	m_tree->GetEntry(entry);
	
	Initialize();
	
	eTotPrimaries=0;
	eTotExitPipe=0;
	eTotEntersZDD=0;
	eGammasEntersZDD=0;
	eElectronsEntersZDD=0.;
	ePositronsEntersZDD=0.;
	eElectronsPositronsEntersZDD=0;
	
	eDepBeforeZDD=0;
	
	nHitsZDD=0;
	nHitsGammasZDD=0;
	nHitsElectronsZDD=0;
	nHitsPositronsZDD=0;
	nHitsZDDEast=0;
	nHitsZDDWest=0;
	
	nHitsPrimaryZDD=0;
	nHitsPrimaryZDDEast=0;
	nHitsPrimaryZDDWest=0;
	
	nExitWindow=0;
	
	//printf("evtID = %d \n",m_evtID);
	//printf("evtID = %d m_vecXPreStep->size() = %d m_zdd_energy = %f \n",m_evtID, m_vecXPreStep->size(),m_zdd_energy);
		for(int step=0;step<m_vecXPreStep->size();step++)
			{
			//printf("m_vecXPreStep->at(step) = %f (m_vecVolPostStep->at(step)).c_str() = %s \n",m_vecXPreStep->at(step),(m_vecVolPostStep->at(step)).c_str());
			int trackID = m_vectrackIDStep->at(step);
			int parentID = m_vecparentIDStep->at(step);
			int PDG = m_vecPDGCodeStep->at(step);	
			if(pdg->GetParticle(PDG)==NULL) continue; //PDG=1000822050 (Pb205) and 1000070150 (N15) appears in NTuple and is not included in TDatabasePDG
			double mass = 	pdg->GetParticle(PDG)->Mass()*1e3;	
			
			TVector3 vPreStep(m_vecXPreStep->at(step),m_vecYPreStep->at(step),m_vecZPreStep->at(step));	
			TVector3 vPostStep(m_vecXPostStep->at(step),m_vecYPostStep->at(step),m_vecZPostStep->at(step));
			TVector3 vPreStepMom(m_vecPxPreStep->at(step),m_vecPyPreStep->at(step),m_vecPzPreStep->at(step));
			TLorentzVector lvPreStep;
			lvPreStep.SetVectM(vPreStepMom,mass);
			string volPreStep = m_vecVolPreStep->at(step);
			string volPostStep = m_vecVolPostStep->at(step);
			
			double ETotPreStep = m_vecETotPreStep->at(step);
			double ETotPostStep = m_vecETotPostStep->at(step);
			
			double EDepStep = m_vecEDepStep->at(step);
			
			bool isPrimary = parentID==0;
			bool fromIP=vPreStep.x()==0. && vPreStep.y()==0. && vPreStep.z()==0.;
			
			if(isPrimary && fromIP)
				{
				//eTotPrimaries=eTotPrimaries+lvPreStep.E();
				if(m_option.find("BBBREM ") != std::string::npos)
					{
					if(PDG==22 || PDG==11) eTotPrimaries=eTotPrimaries+ETotPreStep;					
					}				
				else eTotPrimaries=eTotPrimaries+ETotPreStep ;
				}
			
			/*
			bool preStepInTwoBeamPipe = volPreStep=="physicalTwoBeamPipe" || volPreStep=="physicalTwoBeamPipe1"
			|| volPreStep=="phycical_endplate_twoholes_square1" || volPreStep=="phycical_endplate_twoholes_square2"
			|| volPreStep=="phycical_endplate_twoholes_arc1" || volPreStep=="phycical_endplate_twoholes_arc2" || volPreStep=="phycical_endplate_twoholes_arc3" || volPreStep=="phycical_endplate_twoholes_arc4"
			||volPreStep=="phycical_endplate_twoholes_tshape1" || volPreStep=="phycical_endplate_twoholes_tshape2"
			;
			*/
			
			//bool preStepInBeamWindow = volPreStep=="physical_window";
			//bool preStepInBeamWindow = volPreStep=="physical_window" || volPreStep=="physicalTwoBeamPipe" || volPreStep=="physicalTwoBeamPipe1";  
			//bool preStepInBeamWindow = volPreStep=="physical_window" || volPreStep=="physical_outgoing_top" || volPreStep=="physical_outgoing_bottom";
			bool preStepInBeamWindow = volPreStep=="physical_window" || volPreStep=="physical_outgoing_top" || volPreStep=="physical_outgoing_bottom" || volPreStep=="physical_prewindow" || volPreStep=="physical_afterwindow" || volPreStep=="physical_ispb";
			
			bool exitWindow = preStepInBeamWindow && volPreStep!=volPostStep;
			if(exitWindow)
			//if(volPreStep!=volPostStep && (volPreStep=="physical_window" || volPreStep=="physicalTwoBeamPipe" || volPreStep=="physicalTwoBeamPipe1")&& fabs(vPreStep.Z())>3000.)
				{
				nExitWindow++;
				eTotExitPipe=eTotExitPipe+lvPreStep.E();
				}
			
			if(vPostStep.Z()<3349.95) eDepBeforeZDD=eDepBeforeZDD+EDepStep;
			
			bool postStepInZDD=vPostStep.Z()>=3350. && vPostStep.Z()<=3490. && vPostStep.X()>=0 && vPostStep.X()<=40. && fabs(vPostStep.Y())>=5 && fabs(vPostStep.Y())<=35;
			
			//bool entersZDD = volPreStep=="physicWorld" && (volPreStep!=volPostStep);
			//bool entersZDD = volPreStep=="physicWorld" && volPostStep.find("crystal_") != std::string::npos;
			//bool entersZDD = volPreStep=="physicWorld" && m_zdd_energy>0;
			//bool entersZDD = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && volPostStep.find("physicZdd") == std::string::npos && m_zdd_energy>0;
			//bool entersZDD = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && vPostStep.Z()<3350.0005 && m_zdd_energy>0;
			//bool entersZDD = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && fabs(vPostStep.Z())<3350.0005 && m_zdd_energy>0;			
			//bool entersZDD = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && fabs(vPreStep.Z())<3349.995 && m_zdd_energy>0;
			
			bool entersZDD = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && fabs(vPreStep.Z())<3350.000 && m_zdd_energy>0;
			
			//bool entersZDD = volPreStep!=volPostStep && (volPostStep.find("crystal_") || volPostStep.find("physicZdd") ) && fabs(vPreStep.Z())<3350. && m_zdd_energy>0;			
			//bool entersZDD = volPreStep!=volPostStep && (volPostStep.find("crystal_")) &&  postStepInZDD && m_zdd_energy>0;			
			//bool entersZDD = vPostStep.Z()>=3300. && vPostStep.Z()<3490. && vPostStep.X()>=0 && vPostStep.X()<=40. && fabs(vPostStep.Y())>=5 && fabs(vPostStep.Y())<=35;
			
			if(entersZDD) 
				{nHitsZDD++;
				//eTotEntersZDD=eTotEntersZDD+lvPreStep.E();
				eTotEntersZDD=eTotEntersZDD+ETotPreStep;				
				hPDGHitsZDD->Fill(PDG);
				
				if(PDG==22) {nHitsGammasZDD++;eGammasEntersZDD=eGammasEntersZDD+lvPreStep.E();}
				if(PDG==11) {nHitsElectronsZDD++;eElectronsEntersZDD=eElectronsEntersZDD+lvPreStep.E();}
				if(PDG==-11) {nHitsPositronsZDD++;ePositronsEntersZDD=ePositronsEntersZDD+lvPreStep.E();}
				if(PDG==11 || PDG==-11){eElectronsPositronsEntersZDD=eElectronsPositronsEntersZDD+lvPreStep.E();}
				//eElectronsPositronsEntersZDD=eElectronsPositronsEntersZDD+(eElectronsEntersZDD+ePositronsEntersZDD);
				
				if(isPrimary) 
					{nHitsPrimaryZDD++;
					hPDGHitsPrimaryZDD->Fill(PDG);
					}
				}
			
			//bool entersZDDEast = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && vPostStep.Z()>-3350.0005 && m_zddLeft_energy>0;
			bool entersZDDEast= entersZDD && vPreStep.Z()>-3350.000 && m_zddLeft_energy>0;
			
			if(entersZDDEast)
				{
				nHitsZDDEast++;
					
				hPDGHitsZDDEast->Fill(PDG);	
				if(isPrimary) 
					{nHitsPrimaryZDDEast++;
					hPDGHitsPrimaryZDDEast->Fill(PDG);
					}
				}
			
			//bool entersZDDWest = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && vPostStep.Z()<3350.0005 && m_zddRight_energy>0;
			bool entersZDDWest = entersZDD && vPreStep.Z()<3350.000 && m_zddRight_energy>0;
			
			if(entersZDDWest)
				{
				nHitsZDDWest++;
					
				hPDGHitsZDDWest->Fill(PDG);	
				if(isPrimary) 
					{nHitsPrimaryZDDWest++;
					hPDGHitsPrimaryZDDWest->Fill(PDG);
					}
				}
				
			bool inWorld=	(vPreStep.X()>=m_WorldXMin && vPreStep.X()<=m_WorldXMax)
			&& (vPreStep.Y()>=m_WorldYMin && vPreStep.Y()<=m_WorldYMax)
			&& (vPreStep.Z()>=m_WorldZMin && vPreStep.Z()<=m_WorldZMax)
			; 
			
			if(inWorld)
				{
				//AddPointToTrajectory(trackID,vPreStep);
				m_mapTrackPoints[trackID].push_back(vPreStep);
				m_mapTrackMomentum[trackID].push_back(lvPreStep.Vect());
				m_mapTrackPDG[trackID]=PDG;
				}
			}
		
		hETotPrimaries->Fill(eTotPrimaries);
		
		hNHitsZDD->Fill(nHitsZDD);
		hNHitsGammasZDD->Fill(nHitsGammasZDD);
		hNHitsElectronsZDD->Fill(nHitsElectronsZDD);
		hNHitsPositronsZDD->Fill(nHitsPositronsZDD);
		hNHitsZDDEast->Fill(nHitsZDDEast);	
		hNHitsZDDWest->Fill(nHitsZDDWest);
		
		hNHitsPrimaryZDD->Fill(nHitsPrimaryZDD);
		hNHitsPrimaryZDDEast->Fill(nHitsPrimaryZDDEast);
		hNHitsPrimaryZDDWest->Fill(nHitsPrimaryZDDWest);
		
		hETotExitPipe->Fill(eTotExitPipe);
		
		hETotEntersZDD->Fill(eTotEntersZDD);
		if(eTotEntersZDD>0)hEGammasEntersZDDOverETotEntersZDD->Fill(eGammasEntersZDD/eTotEntersZDD);
		if(eTotEntersZDD>0)hEElectronsEntersZDDOverETotEntersZDD->Fill(eElectronsEntersZDD/eTotEntersZDD);
		if(eTotEntersZDD>0)hEPositronsEntersZDDOverETotEntersZDD->Fill(ePositronsEntersZDD/eTotEntersZDD);
		hETotEntersZDDOverETotPrimaries->Fill(eTotEntersZDD/eTotPrimaries);
		
		hEGammasEntersZDDOverETotPrimaries->Fill(eGammasEntersZDD/eTotPrimaries);
		hEElectronsEntersZDDOverETotPrimaries->Fill(eElectronsEntersZDD/eTotPrimaries);
		hEPositronsEntersZDDOverETotPrimaries->Fill(ePositronsEntersZDD/eTotPrimaries);
		hEElectronsPositronsEntersZDDOverETotPrimaries->Fill(eElectronsPositronsEntersZDD/eTotPrimaries);
		
		if(eTotExitPipe>0) hETotEntersZDDOverETotExitPipe->Fill(eTotEntersZDD/eTotExitPipe);
		
		hEDepZDD->Fill(m_zdd_energy);
		hEDepZDDOverETotPrimaries->Fill(m_zdd_energy/eTotPrimaries);
		
		if(eTotEntersZDD>0) hEDepZDDOverETotEntersZDD ->Fill(m_zdd_energy/eTotEntersZDD);
		/*
		for (std::map<int,std::vector<TVector3> > ::iterator it=m_mapTrackPoints.begin(); it!=m_mapTrackPoints.end(); ++it)
		{
		std::vector<TVector3> traj=it->second;
		TPolyLine3D poly3D = MakeTPolyLine3D(traj);
		m_Trajectories.push_back(poly3D);
		}
		*/
	if(m_option.find("DrawTraj ") != std::string::npos)	
		{
		//if(entry==1) DrawTrajectories();
		DrawTrajectories();
		}
	//cout << "Energy Deposited before ZDD = " << eDepBeforeZDD << endl;
	//cout << "Energy entering ZDD = "<<eTotEntersZDD << endl;
	//cout << "Energy deposited in ZDD = " << m_zdd_energy << endl;
	}

hETotPrimaries->Scale(100*1/((double)hETotPrimaries->GetEntries()));
hETotPrimaries->GetYaxis()->SetTitle("\%");

hPDGHitsZDD->Scale(100*1/((double)hPDGHitsZDD->GetEntries()));
hPDGHitsZDD->GetYaxis()->SetTitle("\%");

hNHitsZDD->Scale(100*1/((double)hNHitsZDD->GetEntries()));
hNHitsZDD->GetYaxis()->SetTitle("\%");

hNHitsGammasZDD->Scale(100*1/((double)hNHitsGammasZDD->GetEntries()));
hNHitsGammasZDD->GetYaxis()->SetTitle("\%");

hNHitsElectronsZDD->Scale(100*1/((double)hNHitsElectronsZDD->GetEntries()));
hNHitsElectronsZDD->GetYaxis()->SetTitle("\%");

hNHitsPositronsZDD->Scale(100*1/((double)hNHitsPositronsZDD->GetEntries()));
hNHitsPositronsZDD->GetYaxis()->SetTitle("\%");

hPDGHitsZDDEast->Scale(100*1/((double)hPDGHitsZDDEast->GetEntries()));
hPDGHitsZDDEast->GetYaxis()->SetTitle("\%");

hPDGHitsZDDWest->Scale(100*1/((double)hPDGHitsZDDWest->GetEntries()));
hPDGHitsZDDWest->GetYaxis()->SetTitle("\%");

	
hPDGHitsPrimaryZDD->Scale(100*1/((double)hPDGHitsPrimaryZDD->GetEntries()));
hPDGHitsPrimaryZDD->GetYaxis()->SetTitle("\%");

hPDGHitsPrimaryZDDEast->Scale(100*1/((double)hPDGHitsPrimaryZDDEast->GetEntries()));
hPDGHitsPrimaryZDDEast->GetYaxis()->SetTitle("\%");

hPDGHitsPrimaryZDDWest->Scale(100*1/((double)hPDGHitsPrimaryZDDWest->GetEntries()));
hPDGHitsPrimaryZDDWest->GetYaxis()->SetTitle("\%");

hETotExitPipe->Scale(100*1/((double)hETotExitPipe->GetEntries()));
hETotExitPipe->GetYaxis()->SetTitle("\%");

hETotEntersZDD->Scale(100*1/((double)hETotEntersZDD->GetEntries()));
hETotEntersZDD->GetYaxis()->SetTitle("\%");


hEGammasEntersZDDOverETotEntersZDD->Scale(100*1/((double)hEGammasEntersZDDOverETotEntersZDD->GetEntries()));
hEGammasEntersZDDOverETotEntersZDD->GetYaxis()->SetTitle("\%");

hEElectronsEntersZDDOverETotEntersZDD->Scale(100*1/((double)hEElectronsEntersZDDOverETotEntersZDD->GetEntries()));
hEElectronsEntersZDDOverETotEntersZDD->GetYaxis()->SetTitle("\%");

hEPositronsEntersZDDOverETotEntersZDD->Scale(100*1/((double)hEPositronsEntersZDDOverETotEntersZDD->GetEntries()));
hEPositronsEntersZDDOverETotEntersZDD->GetYaxis()->SetTitle("\%");

hETotEntersZDDOverETotPrimaries->Scale(100*1/((double)hETotEntersZDDOverETotPrimaries->GetEntries()));
hETotEntersZDDOverETotPrimaries->GetYaxis()->SetTitle("\%");

hEGammasEntersZDDOverETotPrimaries->Scale(100*1/((double)hEGammasEntersZDDOverETotPrimaries->GetEntries()));
hEElectronsEntersZDDOverETotPrimaries->Scale(100*1/((double)hEElectronsEntersZDDOverETotPrimaries->GetEntries()));
hEElectronsEntersZDDOverETotPrimaries->SetTitle("\%");
hEPositronsEntersZDDOverETotPrimaries->Scale(100*1/((double)hEPositronsEntersZDDOverETotPrimaries->GetEntries()));
hEPositronsEntersZDDOverETotPrimaries->SetTitle("\%");			hEElectronsPositronsEntersZDDOverETotPrimaries->Scale(100*1/((double)hEElectronsPositronsEntersZDDOverETotPrimaries->GetEntries()));
hEElectronsPositronsEntersZDDOverETotPrimaries->SetTitle("\%");      

hETotEntersZDDOverETotExitPipe->Scale(100*1/((double)hETotEntersZDDOverETotExitPipe->GetEntries()));
hETotEntersZDDOverETotExitPipe->GetYaxis()->SetTitle("\%");
		
hEDepZDD->Scale(100*1/((double)hEDepZDD->GetEntries()));
hEDepZDD->GetYaxis()->SetTitle("\%");		

hEDepZDDOverETotPrimaries->Scale(100*1/((double)hEDepZDDOverETotPrimaries->GetEntries()));
hEDepZDDOverETotPrimaries->GetYaxis()->SetTitle("\%");

	
outfile->Write();

//Modify the .gif to make it loops
if(m_option.find("DrawTraj ") != std::string::npos)	{
//sprintf(cmd,"convert -loop 0 %s.gif %s.gif",outpath.c_str(),outpath.c_str());
sprintf(cmd,"convert -rotate 90 -loop 0 %s.gif %s.gif",outpath.c_str(),outpath.c_str());
//sprintf(cmd,"convert -rotate 90 %s.gif %s.gif",outpath.c_str(),outpath.c_str());
cout<< cmd << endl;
system(cmd);
}

}


void InitSystVect()
{

m_crystalList.resize(12);
m_crystalList[0]="104";
m_crystalList[1]="103";
m_crystalList[2]="102";
m_crystalList[3]="101";
m_crystalList[4]="114";
m_crystalList[5]="113";
m_crystalList[6]="112";
m_crystalList[7]="111";
m_crystalList[8]="124";
m_crystalList[9]="123";
m_crystalList[10]="122";
m_crystalList[11]="121";


/*
m_crystalList.resize(13);
m_crystalList[0]="0deg";
m_crystalList[1]="104";
m_crystalList[2]="103";
m_crystalList[3]="102";
m_crystalList[4]="101";
m_crystalList[5]="114";
m_crystalList[6]="113";
m_crystalList[7]="112";
m_crystalList[8]="111";
m_crystalList[9]="124";
m_crystalList[10]="123";
m_crystalList[11]="122";
m_crystalList[12]="121";
*/
m_EnergyList.resize(7);
m_EnergyList[0]="50";
m_EnergyList[1]="100";
m_EnergyList[2]="200";
m_EnergyList[3]="500";
m_EnergyList[4]="1000";
m_EnergyList[5]="1500";
m_EnergyList[6]="2000";

}

void MultiAnaBesSteps(string inputpath, string outpath)
{

InitSystVect();


for(int i=0;i<m_EnergyList.size();i++)
	{
	
	for(int j=0;j<m_crystalList.size();j++)
		{

		string infilename="Impact_reco_particleGun_"+m_crystalList[j]+"_"+m_EnergyList[i]+"MeV_Pipe.root";
		string infilepath=inputpath+"/"+infilename;
		cout<< infilepath << endl;
		
		string outfilename="AnaBesSteps_particleGun_"+m_crystalList[j]+"_"+m_EnergyList[i]+"MeV_Pipe";
		string outfilepath=outpath+"/"+outfilename;
		cout << outfilepath << endl;
		cout << endl;
		//AnaBesSteps(infilepath, "", outfilepath, 10);
		AnaBesSteps(infilepath, "", outfilepath);
		}
	}

}

TH2D* MakeTH2FromTH1Slices(std::vector<TH1D*> histos)
{
TH2D* h2D;
h2D = new TH2D((histos[0]->GetName()),"",histos.size(),0.,4.,histos[0]->GetNbinsX(),histos[0]->GetXaxis()->GetXmin(),histos[0]->GetXaxis()->GetXmax());

for(int k=0;k<histos.size();k++)
	{
	for(int biny=1;biny<=histos[k]->GetNbinsX();biny++)
		{
		//h2D->SetBinContent(k,biny,histos[k]->GetBinContent(biny));
		h2D->SetBinContent(k+1,biny,histos[k]->GetBinContent(biny));
		}
	h2D->GetXaxis()->SetBinLabel(k+1,histos[k]->GetTitle());
	}

return h2D;
}

void MergeAnaBesSteps(string inputpath, string outpath)
{

InitSystVect();

//std::map<std::vector<string>,std::vector<TH1D> > m_mapHistos;
string outname=outpath+".root";
TFile* outfile = new TFile(outname.c_str(),"RECREATE");
/*
for(int i=0;i<m_EnergyList.size();i++)
	{
	std::vector<TH1D*> m_mapHistos;
	for(int j=0;j<m_crystalList.size();j++)
		{
		string infilename=inputpath+"/AnaBesSteps_particleGun_"+m_crystalList[j]+"_"+m_EnergyList[i]+"MeV_Pipe.root";
		cout << infilename << endl;
		TFile* file = TFile::Open(infilename.c_str());
		if(file==NULL) continue;
		TH1D* histo=(TH1D*) file->Get("hEDepZDDOverETotEntersZDD");
		if(histo==NULL) continue;
		histo->SetTitle(m_crystalList[j].c_str());
		m_mapHistos.push_back(histo);	
		
		}
	outfile->mkdir(m_EnergyList[i].c_str());
	outfile->cd(m_EnergyList[i].c_str());	
	TH2D* h2D = MakeTH2FromTH1Slices(m_mapHistos);
	char hname[256];
	char htitle[256];
	//h2D->SetTitle(m_EnergyList[i].c_str());
	//outfile->cd();
	h2D->GetXaxis()->SetTitle("ZDD crystal #");
	h2D->GetYaxis()->SetTitle(m_mapHistos[0]->GetXaxis()->GetTitle());
	h2D->Write();
	outfile->cd();
	}
outfile->Close();
*/

int counter=0;
vector<const char*> vechname;
for(int i=0;i<m_EnergyList.size();i++)
	{
	vector< vector<TH1D*> > m_mapHistos;
	for(int j=0;j<m_crystalList.size();j++)
		{
		string infilename=inputpath+"/AnaBesSteps_particleGun_"+m_crystalList[j]+"_"+m_EnergyList[i]+"MeV_Pipe.root";
		cout << infilename << endl;
		TFile* file = TFile::Open(infilename.c_str());
		if(file==NULL) continue;
		
		vector<TH1D*> vechistos;
			TIter nextkey(file->GetListOfKeys());
			TKey *key;
			while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      //cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;				
			if (obj->InheritsFrom("TH1D")) {
			//TH1D* histo= (TH1D*)key->ReadObj();
			TH1D* histo= (TH1D*)file->Get(obj->GetName());
			histo->SetTitle(m_crystalList[j].c_str());
			//if(strcmp(obj->GetName(),"hEDepZDD")==0)vechistos.push_back(histo);
			vechistos.push_back(histo);
			}	
		}
		m_mapHistos.push_back(vechistos);			
	}
	outfile->mkdir(m_EnergyList[i].c_str());
	outfile->cd(m_EnergyList[i].c_str());	
	
	for(int iHist=0;iHist<m_mapHistos[0].size();iHist++)
		{
		vector <TH1D*> vechistos;
			for(int iCrystal=0;iCrystal<m_mapHistos.size();iCrystal++)
			{
			vechistos.push_back(m_mapHistos[iCrystal][iHist]);
			cout << (vechistos.back())->GetName() << endl;
			}
		TH2D* h2D = MakeTH2FromTH1Slices(vechistos);
		h2D->GetXaxis()->SetTitle("ZDD crystal #");
	  h2D->GetYaxis()->SetTitle(vechistos[0]->GetXaxis()->GetTitle());
		h2D->Write();	
		}
	outfile->cd();		
	}
outfile->Close();
		
				
		
}

void DrawProjection1DvsEnergy(string inputpath, string hname, string crystal, string outpath)
{
TFile* file = TFile::Open(inputpath.c_str());

InitSystVect();

TCanvas *c = new TCanvas();
for(int k=0;k<m_EnergyList.size();k++)
{
string path=m_EnergyList[k]+"/"+hname;
//file->cd(path.c_str());
TH2D* h2D = (TH2D*)file->Get(path.c_str());
string hprojname="hproj_crystal"+crystal+"_E"+m_EnergyList[k];
int binnb=h2D->GetXaxis()->FindBin(crystal.c_str());

TH1D* histo = (TH1D*) h2D->ProjectionY(hprojname.c_str(),binnb,binnb);
histo->SetTitle(m_EnergyList[k].c_str());

histo->SetLineColor(kBlue+k);
if(k==0)histo->Draw();
else histo->Draw("same");

}
c->BuildLegend();


}