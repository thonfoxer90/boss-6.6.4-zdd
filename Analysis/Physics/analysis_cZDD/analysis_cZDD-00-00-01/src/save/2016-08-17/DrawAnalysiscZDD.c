#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <stdio.h>
#include <iostream>
#include <TKey.h>
#include <TList.h>

using namespace std;

void DrawcZDDcrystals()
{

double crystalDX=1;
double crystalDY=1;
int NCrystalsX=4;
int NCrystalsY=3;
double XEdgeOrigTop=0;
double YEdgeOrigTop=0.5;
double XEdgeOrigBottom=0;XEdgeOrigBottom=XEdgeOrigTop;
double YEdgeOrigBottom;YEdgeOrigBottom=-YEdgeOrigTop;
//Top cZDD
vector<TLine*> linesY;
vector<TLine*> linesX;

double x0,y0;
double x1,y1;
x0=0;y0=0;
x1=0;y1=0;
for(int k=0;k<5;k++)
	{
	x0=k*crystalDX+XEdgeOrigTop;
	y0=0.5;
	x1=x0;
	y1=NCrystalsY*crystalDX+YEdgeOrigTop;
	linesY.push_back(new TLine(x0,y0,x1,y1));
	linesY.back()->SetLineColor(1);
	linesY.back()->SetLineWidth(2);
	linesY.back()->Draw();
	}

for(int k=0;k<4;k++)
	{
	x0=XEdgeOrigTop;
	y0=k*crystalDX+YEdgeOrigTop;
	x1=NCrystalsX*crystalDX+XEdgeOrigTop;
	y1=y0;
	linesX.push_back(new TLine(x0,y0,x1,y1));
	linesX.back()->SetLineColor(1);
	linesX.back()->SetLineWidth(2);
	linesX.back()->Draw();
	}
	
//Bottom cZDD
vector<TLine*> linesYBottom;
vector<TLine*> linesXBottom;

for(int k=0;k<5;k++)
	{
	x0=k*crystalDX+XEdgeOrigBottom;
	y0=YEdgeOrigBottom;
	x1=x0;
	y1=-NCrystalsY*crystalDX+YEdgeOrigBottom;
	linesYBottom.push_back(new TLine(x0,y0,x1,y1));
	linesYBottom.back()->SetLineColor(1);
	linesYBottom.back()->SetLineWidth(2);
	linesYBottom.back()->Draw();
	}

for(int k=0;k<4;k++)
	{
	x0=XEdgeOrigBottom;
	y0=-k*crystalDX+YEdgeOrigBottom;
	x1=NCrystalsX*crystalDX+XEdgeOrigBottom;
	y1=y0;
	linesXBottom.push_back(new TLine(x0,y0,x1,y1));
	linesXBottom.back()->SetLineColor(1);
	linesXBottom.back()->SetLineWidth(2);
	linesXBottom.back()->Draw();
	}
	
}


void DrawAnalysiscZDD(string inputname, string option, string outputname)
{

TFile* file = TFile::Open(inputname.c_str());
string outps=outputname+".ps";
string outpdf=outputname+".pdf";

if(option=="Impact_reco"){
	TTree* tree = (TTree*) file->Get(option.c_str());
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));
	char drawcmd[512];
	char selection[512];
	char drawopt[16];
	for(int i=0;i<2;i++){
		if(i==0) {
			sprintf(drawcmd,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>>h%d",i);	
			sprintf(selection,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>0");
			sprintf(drawopt,"");
			}
		if(i==1) {
		
			sprintf(drawcmd,"yImpact:xImpact>>h%d(128,-1.,5.,128,-5.,5.)",i);	
			sprintf(selection,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>0");
			sprintf(drawopt,"colz");
			}
		tree->Draw(drawcmd,selection,drawopt);
		if(i==1) DrawcZDDcrystals();
		c->Print(Form("%s",outps.c_str()));
		}
	c->Print(Form("%s]",outps.c_str()));
	}
		
else if(option=="myTree_events"){
	TTree* tree = (TTree*) file->Get(option.c_str());
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));
	char drawcmd[512];
	char selection[512];
	char drawopt[16];
	for(int i=0;i<2;i++){
		if(i==0) {
			sprintf(drawcmd,"zdd_energy>>h%d",i);	
			//sprintf(selection,"zdd_energy>0");
			sprintf(selection,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>0");
			sprintf(drawopt,"");
			}
		if(i==1) {
		
			sprintf(drawcmd,"hit_y:hit_x>>h%d(128,-1.,5.,128,-5.,5.)",i);	
			//sprintf(selection,"zdd_energy>0");
			sprintf(selection,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>0");
			sprintf(drawopt,"colz");
			}
		tree->Draw(drawcmd,selection,drawopt);
		if(i==1) DrawcZDDcrystals();
		c->Print(Form("%s",outps.c_str()));
		}
	c->Print(Form("%s]",outps.c_str()));
	}
	
else if(option=="particlegunTree"){
	TTree* tree = (TTree*) file->Get(option.c_str());
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));
	char drawcmd[512];
	char selection[512];
	char drawopt[16];
	for(int i=0;i<2;i++){
		if(i==0) {
			sprintf(drawcmd,"");	
			sprintf(selection,"");
			sprintf(drawopt,"");
			}
		
		if(i==1) {			
			//sprintf(drawcmd,"MCfinPosY:MCfinPosX>>h%d(128,-1.,5.,128,-5.,5.)",i);	
			sprintf(drawcmd,"MCpGunYCalcatZDD:MCpGunXCalcatZDD>>h%d(128,-1.,5.,128,-5.,5.)",i);
			//sprintf(selection,"zdd_energy>0");
		
			//sprintf(selection,"eDep121+eDep122+eDep123+eDep124+eDep111+eDep112+eDep113+eDep114+eDep101+eDep102+eDep103+eDep104+eDep201+eDep202+eDep203+eDep204+eDep211+eDep212+eDep213+eDep214+eDep221+eDep222+eDep223+eDep224>0 && MCpartProp==11");
			sprintf(selection,"MCpartProp==11");
			sprintf(drawopt,"colz");			
			
			}
		tree->Draw(drawcmd,selection,drawopt);
		if(i==1) DrawcZDDcrystals();
		c->Print(Form("%s",outps.c_str()));
		}
	c->Print(Form("%s]",outps.c_str()));
	}

else if(option=="particlegun"){
//file->cd("particlegun");
//TIter nextkey(file->GetListOfKeys());

TDirectory* directory= file->GetDirectory("particlegun");
TIter nextkey(directory->GetListOfKeys());
TKey *key;

TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str())); 	

   while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
				
       if (obj->InheritsFrom("TH1") || obj->InheritsFrom("TH2")) {
       if(obj->InheritsFrom("TH2")) obj->Draw("colz");
			 else obj->Draw();
			 c->Print(Form("%s",outps.c_str()));
       }
			 
   } 
c->Print(Form("%s]",outps.c_str()));	 
}

else if(option=="Bhabha"){
TDirectory* directory= file->GetDirectory("Bhabha");
TIter nextkey(directory->GetListOfKeys());
TKey *key;

TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str())); 	

   while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
				
       if (obj->InheritsFrom("TH1") || obj->InheritsFrom("TH2")) {
       if(obj->InheritsFrom("TH2")) obj->Draw("colz");
			 else obj->Draw();
			 //c->Print(Form("%s",outps.c_str()));
			 c->Print(Form("%s",outps.c_str()),"Title:truc");
       }
			 
   } 
c->Print(Form("%s]",outps.c_str()));
}

else if(option=="AllMCBhabhaNtuple"){
TTree* tree = (TTree*) file->Get("AllMCTruthTree");

TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str())); 	

   char drawcmd[512];
	char selection[512];
	char drawopt[16];
	for(int i=0;i<1;i++){
		if(i==0) {
		/*
			sprintf(drawcmd,"MCYCalcatZDD[0]:MCXCalcatZDD[0]>>h(128,-1.,6.,128,-5.,5.)");	
			sprintf(selection,"zddLeft_energy==0");
		*/	
			//if(option.find("Histo") != string::npos)
			sprintf(drawcmd,"MCYCalcatZDD:MCXCalcatZDD>>h(128,-1.,6.,128,-5.,5.)");	
			sprintf(selection,"zddLeft_energy==0");
			sprintf(drawopt,"colz");
			tree->Draw(drawcmd,selection,drawopt);
			DrawcZDDcrystals();
			}
		
		c->Print(Form("%s",outps.c_str()));
		}
c->Print(Form("%s]",outps.c_str()));
}

//else if(option=="Impact_reco&particlegunTree"){
else {
	/*
	TTree* particlegunTree = (TTree*) file->Get("particlegunTree");
	TTree* ImpactrecoTree = (TTree*) file->Get("Impact_reco");
	//Impact_reco variables
	double xImpact, yImpact;
	ImpactrecoTree->SetBranchAddress("xImpact",&xImpact);
	ImpactrecoTree->SetBranchAddress("xImpact",&yImpact);
	
	//Initialize histograms
	vector <TH2D*> histos2D;
	//Brice tree variables
	for(int entry=0;entry<particlegunTree->GetEntries();entry++)
		{
		particlegunTree->GetEntry(entry);
		ImpactrecoTree->GetEntry(entry);
		}
	*/
	
	
	TTree* tree = (TTree*) file->Get("particlegunTree");
	tree->AddFriend("Impact_reco",inputname.c_str());
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));
	char drawcmd[512];
	char selection[512];
	char drawopt[16];
	for(int i=0;i<1;i++){
		if(i==0) {
			sprintf(drawcmd,"xImpact-MCXCalcatZDD");	
			sprintf(selection,"");
			sprintf(drawopt,"");
			}
		tree->Draw(drawcmd,selection,drawopt);
		c->Print(Form("%s",outps.c_str()));
		}
	c->Print(Form("%s]",outps.c_str()));
	
	
	}

		
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
	
}



