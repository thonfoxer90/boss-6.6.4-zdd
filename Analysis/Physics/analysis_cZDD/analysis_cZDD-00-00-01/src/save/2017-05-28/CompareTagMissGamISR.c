#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TText.h>
#include <TMath.h>
#include <TLorentzVector.h>
#include <TVector3.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace std;

const int NMAX=256;
int MCntracks;
int 	 MCpartProp[NMAX];
double MCiniMomRho[NMAX];
double MCiniMomTheta[NMAX];
double MCiniMomPhi[NMAX];
double MCiniE[NMAX];
double MCiniMomRhoCM[NMAX];
double MCiniMomThetaCM[NMAX];
double MCiniMomPhiCM[NMAX];
double MCiniECM[NMAX];
double MCXCalcatZDD[NMAX];
double MCYCalcatZDD[NMAX];
double MCiniMomX[NMAX];
double MCiniMomY[NMAX];
double MCiniMomZ[NMAX];

double XZddRecHit;
double YZddRecHit;
double XZddHitMiss4PiCX;
double YZddHitMiss4PiCX;
double XZddGammaISRMCTruth;
double YZddGammaISRMCTruth;

double zdd_energy;
double zddLeft_energy;
double zddRight_energy;
double GamISRMCTruthE;
double GamISRMCTruthTheta;
double GamISRMCTruthPhi;
double Miss4PiCXE;
double Miss4PiCXP;
double Miss4PiCXTheta;
double Miss4PiCXPhi;

vector<string> split(const string& str, const string& delim)
{
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == string::npos) pos = str.length();
        string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}

double GetRadius(double x, double y){
double r=0;
r=sqrt(x*x+y*y);
return r;
}

void CompareTagMissGamISR(string inputfile, string outpath, string option,bool newfile=true){
gROOT->SetBatch();


vector<string> optionsplit=split(option," ");
vector<string> optiontag;
vector<string> optionval;
for(int k=0;k<optionsplit.size();k++){
	string  strtmp=optionsplit[k];
	vector<string> strtmpsplit = split(strtmp,"=");
	if(strtmpsplit.size()==2){
		optiontag.push_back(strtmpsplit[0]);
		optionval.push_back(strtmpsplit[1]);
	}
}

double EZddMin=0;
int tagCrystals=0;

for(int k=0;k<optiontag.size();k++){
	cout << optiontag[k] << " " << optionval[k] <<endl;
	if(optiontag[k]=="EZddMin"){EZddMin=atof(optionval[k].c_str());}
	if(optiontag[k]=="TagCrystals"){tagCrystals=atoi(optionval[k].c_str());}
}

TFile* file = TFile::Open(inputfile.c_str());
if(file==NULL) return;
TTree* tree = (TTree*) file->Get("4PiGamISRTagged");
if(tree==NULL) return;

tree->SetBranchAddress("XZddRecHit", &XZddRecHit);
tree->SetBranchAddress("YZddRecHit", &YZddRecHit);

tree->SetBranchAddress("XZddHitMiss4PiCX",&XZddHitMiss4PiCX);
tree->SetBranchAddress("YZddHitMiss4PiCX",&YZddHitMiss4PiCX);

tree->SetBranchAddress("XZddGammaISRMCTruth",&XZddGammaISRMCTruth);
tree->SetBranchAddress("YZddGammaISRMCTruth",&YZddGammaISRMCTruth);

tree->SetBranchAddress("zdd_energy",&zdd_energy);
tree->SetBranchAddress("zddLeft_energy",&zddLeft_energy);
tree->SetBranchAddress("zddRight_energy",&zddRight_energy);

tree->SetBranchAddress("GamISRMCTruthE",&GamISRMCTruthE);
tree->SetBranchAddress("GamISRMCTruthTheta",&GamISRMCTruthTheta);
tree->SetBranchAddress("GamISRMCTruthPhi",&GamISRMCTruthPhi);

tree->SetBranchAddress("Miss4PiCXE",&Miss4PiCXE);
tree->SetBranchAddress("Miss4PiCXP",&Miss4PiCXP);
tree->SetBranchAddress("Miss4PiCXTheta",&Miss4PiCXTheta);
tree->SetBranchAddress("Miss4PiCXPhi",&Miss4PiCXPhi);

string outps = outpath+".ps";
string outpdf = outpath+".pdf";
string outroot = outpath+".root";

TFile* outRoot;
if(newfile==true)outRoot = new TFile(outroot.c_str(),"RECREATE");
else outRoot = new TFile(outroot.c_str(),"UPDATE");

int nBinsX;
double xmin,xmax;
nBinsX=256;
//xmin=30.;
xmin=-30.;
xmax=30.;


TH1D* hDeltaRTagRMiss = new TH1D("hDeltaRTagRMiss",";R^{Tagged}_{#gamma ISR}-R^{Miss}_{4#pi X} (cm);", nBinsX,xmin,xmax);
TH1D* hDeltaRTagRTrue = new TH1D("hDeltaRTagRTrue",";R^{Tagged}_{#gamma ISR}-R^{MC Truth}_{#gamma ISR} (cm);", nBinsX,xmin,xmax);
TH1D* hDeltaRMissRTrue = new TH1D("hDeltaRMissRTrue",";R^{Miss}_{4#pi X}-R^{MC Truth}_{#gamma ISR} (cm);", nBinsX,xmin,xmax);
TH2D* hDeltaRMissRTruevsDeltaRTagRTrue = new TH2D("hDeltaRMissRTruevsDeltaRTagRTrue",";R^{Tagged}_{#gamma ISR}-R^{MC Truth}_{#gamma ISR} (cm);R^{Miss}_{4#pi X}-R^{MC Truth}_{#gamma ISR} (cm)",nBinsX,xmin,xmax,nBinsX,xmin,xmax);

xmin=0.;
xmax=0.;

TH1D* hDeltaETagEMiss = new TH1D("hDeltaETagEMiss",";E^{Tagged}_{#gamma ISR}-E^{Miss}_{4#pi X} (MeV);", nBinsX,xmin,xmax);
TH1D* hDeltaETagETrue = new TH1D("hDeltaETagETrue",";E^{Tagged}_{#gamma ISR}-E^{MC Truth}_{#gamma ISR} (MeV);", nBinsX,xmin,xmax);
TH1D* hDeltaEMissETrue = new TH1D("hDeltaEMissETrue",";E^{Miss}_{4#pi X}-E^{MC Truth}_{#gamma ISR} (MeV);", nBinsX,xmin,xmax);

//xmin=-2*TMath::Pi();
//xmax=2*TMath::Pi();
nBinsX=512;
xmin=-TMath::Pi();
xmax=TMath::Pi();

TH1D* hDeltaThetaTagThetaMiss = new TH1D("hDeltaThetaTagThetaMiss",";#theta^{Tagged}_{#gamma ISR}-#theta^{Miss}_{4#pi X} (MeV);", nBinsX,xmin,xmax);
TH1D* hDeltaThetaTagThetaTrue = new TH1D("hDeltaThetaTagThetaTrue",";#theta^{Tagged}_{#gamma ISR}-#theta^{MC Truth}_{#gamma ISR} (rad);", nBinsX,xmin,xmax);
TH1D* hDeltaThetaMissThetaTrue = new TH1D("hDeltaThetaMissThetaTrue",";#theta^{Miss}_{4#pi X}-#theta^{MC Truth}_{#gamma ISR} (rad);", nBinsX,xmin,xmax);

TH1D* hDeltaPhiTagPhiMiss = new TH1D("hDeltaPhiTagPhiMiss",";#Phi^{Tagged}_{#gamma ISR}-#Phi^{Miss}_{4#pi X} (MeV);", nBinsX,xmin,xmax);
TH1D* hDeltaPhiTagPhiTrue = new TH1D("hDeltaPhiTagPhiTrue",";#Phi^{Tagged}_{#gamma ISR}-#Phi^{MC Truth}_{#gamma ISR} (rad);", nBinsX,xmin,xmax);
TH1D* hDeltaPhiMissPhiTrue = new TH1D("hDeltaPhiMissPhiTrue",";#Phi^{Miss}_{4#pi X}-#Phi^{MC Truth}_{#gamma ISR} (rad);", nBinsX,xmin,xmax);


xmin=0.;
xmax=0.;
TH1D* hMissMass = new TH1D("hMissMass",";Mm[4#pi^{C}X];",nBinsX,xmin,xmax);
TH1D* hMissMassTagThetaPhi = new TH1D("hMissMassTagThetaPhi",";Mm[4#pi^{C}X]_{tagged #theta,#phi};",nBinsX,xmin,xmax);

int nRMissClosesttoRTrue=0;
int nRTagClosesttoRTrue=0;

for(int entry=0;entry<tree->GetEntries();entry++){
	tree->GetEntry(entry);
	if(zdd_energy<EZddMin) continue;
	if(tagCrystals==1){
		if(!((XZddRecHit>=3. && XZddRecHit<=4.) &&(fabs(YZddRecHit)>=0.5 && fabs(YZddRecHit)<=1.5))) continue;	
	}
	double rTagged=GetRadius(XZddRecHit,YZddRecHit);
	double rMiss=GetRadius(XZddHitMiss4PiCX,YZddHitMiss4PiCX);
	double rTrue=0;	
	rTrue=GetRadius(XZddGammaISRMCTruth,YZddGammaISRMCTruth);

	double ETagged = zdd_energy;
	double EMiss = Miss4PiCXE*1000;
	double ETruth = GamISRMCTruthE*1000;
	
	TVector3 vecTag(0,0,0);
	double zZDD=335;
	if(zddRight_energy>0 && zddLeft_energy==0) vecTag.SetXYZ(XZddRecHit,YZddRecHit,zZDD);
	else if (zddLeft_energy>0 && zddRight_energy==0) vecTag.SetXYZ(XZddRecHit,YZddRecHit,-zZDD);
	
	double ThetaTag = vecTag.Theta();
	double ThetaMiss = Miss4PiCXTheta;
	double ThetaTruth = GamISRMCTruthTheta;
	
	double PhiTag = vecTag.Phi();
	double PhiMiss = Miss4PiCXPhi;
	double PhiTruth = GamISRMCTruthPhi;
	
	TLorentzVector lvMiss(0,0,0,0);
	TVector3 vecMiss(0,0,0);
	vecMiss.SetMagThetaPhi(Miss4PiCXP,Miss4PiCXTheta,Miss4PiCXPhi);
	lvMiss.SetPxPyPzE(vecMiss.Px(),vecMiss.Py(),vecMiss.Pz(),Miss4PiCXE);
	double MissMass=lvMiss.M();
	TLorentzVector lvMissTagThetaPhi(0,0,0,0);
	TVector3 vecMissTagThetaPhi(0,0,0);
	vecMissTagThetaPhi.SetMagThetaPhi(Miss4PiCXP,vecTag.Theta(),vecTag.Phi());
	lvMissTagThetaPhi.SetPxPyPzE(vecMissTagThetaPhi.Px(),vecMissTagThetaPhi.Py(),vecMissTagThetaPhi.Pz(),Miss4PiCXE);
	double MissMassTagThetaPhi = lvMissTagThetaPhi.M();
	
	hDeltaRTagRMiss->Fill(rTagged-rMiss);
	hDeltaRTagRTrue->Fill(rTagged-rTrue);
	hDeltaRMissRTrue->Fill(rMiss-rTrue);

	hDeltaRMissRTruevsDeltaRTagRTrue->Fill(rTagged-rTrue,rMiss-rTrue);
	
	hDeltaETagEMiss->Fill(ETagged-EMiss);
	hDeltaETagETrue->Fill(ETagged-ETruth);
	hDeltaEMissETrue->Fill(EMiss-ETruth);
	
	hDeltaThetaTagThetaMiss->Fill(ThetaTag-ThetaMiss);
	hDeltaThetaTagThetaTrue->Fill(ThetaTag-ThetaTruth);	
	hDeltaThetaMissThetaTrue->Fill(ThetaMiss-ThetaTruth);
	
	hDeltaPhiTagPhiMiss->Fill(PhiTag-PhiMiss);
	hDeltaPhiTagPhiTrue->Fill(PhiTag-PhiTruth);	
	hDeltaPhiMissPhiTrue->Fill(PhiMiss-PhiTruth);
	
	hMissMass->Fill(MissMass);
	hMissMassTagThetaPhi->Fill(MissMassTagThetaPhi);
	
	if(fabs(rTagged-rTrue)<= fabs(rMiss-rTrue)) nRTagClosesttoRTrue++;
	else  nRMissClosesttoRTrue++;
}

cout << "nRTagClosesttoRTrue = " << nRTagClosesttoRTrue <<endl;
cout << "nRMissClosesttoRTrue = " << nRMissClosesttoRTrue << endl;

TCanvas* c = new TCanvas();
//gPad->SetLogy();
c->Print(Form("%s[",outps.c_str())); 	
hDeltaRTagRMiss->Draw();
c->Print(Form("%s",outps.c_str()));
c->Clear();
hDeltaRTagRTrue->Draw();
c->Print(Form("%s",outps.c_str()));
c->Clear();
hDeltaRMissRTrue->Draw();
c->Print(Form("%s",outps.c_str()));
c->Clear();
hDeltaRMissRTruevsDeltaRTagRTrue->Draw("box");
c->Print(Form("%s",outps.c_str()));
c->Clear();
c->Print(Form("%s]",outps.c_str()));

delete c;

//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

outRoot->Write();
outRoot->Close();


/*
map<string,TH1D*> hDeltaRTagRMiss;
map<string,TH1D*> hDeltaRTagRTrue;
map<string,TH1D*> hDeltaRMissRTrue;

vector<string> 
*/

}

void SystStudy(string inputfile, string outpath)
{
vector<double> vecEMin;
vecEMin.push_back(0.);vecEMin.push_back(10.);
vecEMin.push_back(20.); vecEMin.push_back(50.);
vecEMin.push_back(70.); vecEMin.push_back(100.);
//vecEMin.push_back(150.);vecEMin.push_back(200.);

vector<int> vecTagCrystals;
vecTagCrystals.push_back(0);vecTagCrystals.push_back(1);

for(int j=0;j<vecTagCrystals.size();j++){
	for(int k=0;k<vecEMin.size();k++)
		{
		string outname=outpath+".TagCrystals="+SSTR(vecTagCrystals[j])+"EZddMin="+SSTR(vecEMin[k]);
		string option="TagCrystals="+SSTR(vecTagCrystals[j])+" "+"EZddMin="+SSTR(vecEMin[k]);
		CompareTagMissGamISR(inputfile, outname, option);
		}
}


return;
}