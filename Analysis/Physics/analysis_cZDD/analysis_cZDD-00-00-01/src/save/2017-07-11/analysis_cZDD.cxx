
#include "analysis_cZDD/analysis_cZDD.h"
#include "analysis_cZDD/crystalinfo.h"

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()
						
using Event::ZddMcHit;
using Event::ZddMcHitCol;

//const double twopi = 6.2831853;
//const double pi = 3.1415927;
const double mpi = 0.13957;
const double xmass[5] = {0.000511, 0.105658, 0.139570,0.493677, 0.938272};
//const double velc = 29.9792458;  tof_path unit in cm.
const double velc = 299.792458;   // tof path unit in mm
typedef std::vector<int> Vint;
typedef std::vector<HepLorentzVector> Vp4;

int Ncut0,Ncut1,Ncut2,Ncut3,Ncut4,Ncut5,Ncut6;

/*
double charge2energy(long int charge)
{
  double energy = 0;
  //fitparameters for chargeChannel -> energy conversion
  //x <=72000: polynomial 4th order, else linear function with f(x) = f(72000) + f'(72000)*(x-72000.) 
  double p[5] = {0.2258, 0.005359, 2.665e-8, 3.184e-12,-3.888e-17};

  
  if(charge <=72000) energy = p[0] + p[1]*charge + p[2] * TMath::Power(charge,2.) + p[3] * TMath::Power(charge,3.)
    + p[4]*TMath::Power(charge,4.);
  else
    energy = p[0] + p[1]*72000. + p[2]*TMath::Power(72000.,2.) + p[3]*TMath::Power(72000.,3.) + p[4]*TMath::Power(72000.,4.) + (p[1] + 2.*p[2]*charge + 3.*p[3]*charge*charge + 4.*p[4]*TMath::Power(charge,3.)) * (charge - 72000.);

  return energy;
}
*/


double charge2energy(long int charge)
{
  double energy = 0;
  // double limit = 91000.; // 4SiPMs
  //    double limit = 1000000.; // 9SiPMs
  //fitparameters for chargeChannel -> energy conversion
  //x <=limit: polynomial 5th order, else linear function with f(x) = f(limit) + f'(limit)*(x-limit)                   
      //  double p[5] = {0.1919, 0.005379, 4.983e-8, 2.791e-13, -7.012e-18}; // 4SiPMs
      //       double p[5] = {0.02611, 0.001179, 5.228e-10, 4.926e-16, 6.46e-22}; // 9SiPMs no Attenuation
      //      double p[5] = {1.106, 0.04695, -1.583e-6, 8.748e-11,-1.946e-15};//2SiPMs
  double p[6] = {0.389193,6.04002e-2,-3.36707e-6,5.30877e-10,-3.26692e-14,7.02335e-19};//2SiPMs
 energy = p[0] + p[1]*charge + p[2] * TMath::Power(charge,2.) + p[3] * TMath::Power(charge,3.)
   + p[4]*TMath::Power(charge,4.) + p[5]*TMath::Power(charge,5.) ;
  return energy;
}


/////////////////////////////////////////////////////////////////////////////

analysis_cZDD::analysis_cZDD(const std::string& name, ISvcLocator* pSvcLocator) :
  Algorithm(name, pSvcLocator) {
  
  //Declare the properties  
	declareProperty("KKMC",  m_KKMC=0);
	declareProperty("BBBREM", m_BBBREM=0);
  declareProperty("Vr0cut", m_vr0cut=1.0);
  declareProperty("Vz0cut", m_vz0cut=5.0);
  declareProperty("EnergyThreshold", m_energyThreshold=0.04);
  declareProperty("GammaPhiCut", m_gammaPhiCut=20.0);
  declareProperty("GammaThetaCut", m_gammaThetaCut=20.0);
  declareProperty("GammaAngleCut", m_gammaAngleCut=20.0);
  declareProperty("Test4C", m_test4C = 1);
  declareProperty("Test5C", m_test5C = 1);
  declareProperty("CheckDedx", m_checkDedx = 1);
  declareProperty("CheckTof",  m_checkTof = 1);
  
  declareProperty("OutFileName",  m_outFileName = "Leo_test_cZDD.root");
	declareProperty("InputImpact_reco", m_ImpactRecoFileName = "Impact_reco.root");
	declareProperty("WeightFileName",m_WeightFileName = "WEIGHT.out");
	declareProperty("UseMCTruthEDep",m_UseMCTruthEDep = 1);
	
	//Default values
	m_dMasselec	= 511e-6;
	m_dMassPiC = 0.13957;
	m_dCrossingAngle=0.011;
	
	m_dEoPcut            = 0.8;
	m_dCylRad            = 1;
  m_dCylHeight         = 10;
}

//---------------------------------------------------------------------------------------------------------- 
StatusCode analysis_cZDD::initialize(){
  MsgStream log(msgSvc(), name());

  log << MSG::INFO << "in initialize()" << endmsg;
  
  StatusCode status;
	
	if(m_BBBREM==1){m_ReadWeightFile.open(m_WeightFileName.c_str());
	if(m_ReadWeightFile.is_open())
		{
		cout << "Weight file opened." << endl;
		}
	}
	
  m_myFile = new TFile(m_outFileName.c_str(),"RECREATE","TreeFile");
//  m_myFile = new TFile(m_outFileName,"RECREATE","TreeFile");
  m_myTree = new TTree("myTree_events", "");   //Create Tree    
  // variables to save tree data                                                                  
	DefineZDDCrystalsEdges();
	                                                                          
  // HepLorentzVector hepvec_test;                                                                                                                                         
  G4double vertex_x0, vertex_y0, vertex_z0, vertex_r0;
  // protons/antiprotons                                                                                                                                                 

    // charged track vertex                                                                                                                                                   
  /*
  m_myTree->Branch("vertex_x0",&vertex_x0);
  // p pbar properties                                                                                                                                                      
  m_myTree->Branch("p_px",&p_px);
  m_myTree->Branch("p_py",&p_py);
  m_myTree->Branch("p_pz",&p_pz);
  m_myTree->Branch("p_probID",&p_probID);
  m_myTree->Branch("pbar_px",&pbar_px);
  m_myTree->Branch("pbar_py",&pbar_py);
  m_myTree->Branch("pbar_pz",&pbar_pz);
  m_myTree->Branch("pbar_probID",&pbar_probID);
  m_myTree->Branch("ppbar_invmass",&ppbar_invmass);
  */
  // mc truth of isr photons                                                                                                                                              
  m_myTree->Branch("isr_energy",&isr_energy);
  m_myTree->Branch("isr_x",&isr_x);
  m_myTree->Branch("isr_y",&isr_y);
  m_myTree->Branch("isr_z",&isr_z);
  m_myTree->Branch("isr_px",&isr_px);
  m_myTree->Branch("isr_py",&isr_py);
  m_myTree->Branch("isr_pz",&isr_pz);
  m_myTree->Branch("hit_x",&hit_x);
  m_myTree->Branch("hit_y",&hit_y);
  m_myTree->Branch("dist_center",&dist_center);

  // tot energy in zdd, average time                                                                                                                                        
  m_myTree->Branch("zdd_energy",&zdd_energy);
  m_myTree->Branch("zdd_time",&zdd_time);
  //  m_myTree->Branch("chargeChannel", &zdd_chargeChannel);
  
  m_myTree->Branch("eDep101",&eDep[0]);
  m_myTree->Branch("eDep102",&eDep[1]);
  m_myTree->Branch("eDep103",&eDep[2]);
  m_myTree->Branch("eDep104",&eDep[3]);
  m_myTree->Branch("eDep111",&eDep[4]);
  m_myTree->Branch("eDep112",&eDep[5]);
  m_myTree->Branch("eDep113",&eDep[6]);
  m_myTree->Branch("eDep114",&eDep[7]);
  m_myTree->Branch("eDep121",&eDep[8]);
  m_myTree->Branch("eDep122",&eDep[9]);
  m_myTree->Branch("eDep123",&eDep[10]);
  m_myTree->Branch("eDep124",&eDep[11]);
  m_myTree->Branch("eDep201",&eDep[12]);
  m_myTree->Branch("eDep202",&eDep[13]);
  m_myTree->Branch("eDep203",&eDep[14]);
  m_myTree->Branch("eDep204",&eDep[15]);
  m_myTree->Branch("eDep211",&eDep[16]);
  m_myTree->Branch("eDep212",&eDep[17]);
  m_myTree->Branch("eDep213",&eDep[18]);
  m_myTree->Branch("eDep214",&eDep[19]);
  m_myTree->Branch("eDep221",&eDep[20]);
  m_myTree->Branch("eDep222",&eDep[21]);
  m_myTree->Branch("eDep223",&eDep[22]);
  m_myTree->Branch("eDep224",&eDep[23]);
  m_myTree->Branch("event", &evt);
  m_myTree->Branch("run", &run);
  
  
  
  m_leoTree = new TTree("leoTree", "leoTree");
  m_leoTree->Branch("event", &evt);
  m_leoTree->Branch("run", &run);
  m_leoTree->Branch("partProp", &partProp);
  m_leoTree->Branch("primPart", &primPart);
  m_leoTree->Branch("leafPart", &leafPart);
  m_leoTree->Branch("iniPosX", &iniPosX);
  m_leoTree->Branch("iniPosY", &iniPosY);
  m_leoTree->Branch("iniPosZ", &iniPosZ);
  m_leoTree->Branch("iniPosT", &iniPosT);
  m_leoTree->Branch("finPosX", &finPosX);
  m_leoTree->Branch("finPosY", &finPosY);
  m_leoTree->Branch("finPosZ", &finPosZ);
  m_leoTree->Branch("finPosT", &finPosT);
  m_leoTree->Branch("iniMomX", &iniMomX);
  m_leoTree->Branch("iniMomY", &iniMomY);
  m_leoTree->Branch("iniMomZ", &iniMomZ);
  m_leoTree->Branch("iniMomE", &iniE);
  
	m_AllMCTruthTree = new TTree("AllMCTruthTree", "AllMCTruthTree");
	m_AllMCTruthTree->Branch("event", &evt);
	m_AllMCTruthTree->Branch("run", &run);
	m_AllMCTruthTree->Branch("MCntracks", &MCntracks,"MCntracks/I");
	m_AllMCTruthTree->Branch("MCTrackIndex", MCTrackIndex,"MCTrackIndex[MCntracks]/I");
	m_AllMCTruthTree->Branch("MCpartProp", MCpartProp,"MCpartProp[MCntracks]/I");
  m_AllMCTruthTree->Branch("MCprimPart", MCprimPart,"MCprimPart[MCntracks]/O");
	m_AllMCTruthTree->Branch("MCMother", MCMother,"MCMother[MCntracks]/I");
  m_AllMCTruthTree->Branch("MCiniPosX", MCiniPosX,"MCiniPosX[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniPosY", MCiniPosY,"MCiniPosY[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniPosZ", MCiniPosZ,"MCiniPosZ[MCntracks]/D");
	m_AllMCTruthTree->Branch("MCfinPosX", MCfinPosX,"MCfinPosX[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCfinPosY", MCfinPosY,"MCfinPosY[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCfinPosZ", MCfinPosZ,"MCfinPosZ[MCntracks]/D");
	m_AllMCTruthTree->Branch("MCXCalcatZDD", MCXCalcatZDD,"MCXCalcatZDD[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCYCalcatZDD", MCYCalcatZDD,"MCYCalcatZDD[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniMomX", MCiniMomX,"MCiniMomX[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniMomY", MCiniMomY, "MCiniMomY[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniMomZ", MCiniMomZ, "MCiniMomZ[MCntracks]/D");
	m_AllMCTruthTree->Branch("MCiniMomRho", MCiniMomRho, "MCiniMomRho[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniMomTheta", MCiniMomTheta,"MCiniMomTheta[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniMomPhi", MCiniMomPhi, "MCiniMomPhi[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniE", MCiniE,"MCiniE[MCntracks]/D");
		
	m_AllMCTruthTree->Branch("MCiniMomXCM", MCiniMomXCM,"MCiniMomXCM[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniMomYCM", MCiniMomYCM, "MCiniMomYCM[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniMomZCM", MCiniMomZCM, "MCiniMomZCM[MCntracks]/D");
	m_AllMCTruthTree->Branch("MCiniMomRhoCM", MCiniMomRhoCM, "MCiniMomRhoCM[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniMomThetaCM", MCiniMomThetaCM,"MCiniMomThetaCM[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniMomPhiCM", MCiniMomPhiCM, "MCiniMomPhiCM[MCntracks]/D");
  m_AllMCTruthTree->Branch("MCiniECM", MCiniECM,"MCiniECM[MCntracks]/D");
	
	m_AllMCTruthTree->Branch("zdd_energy",&MCzdd_energy);
	m_AllMCTruthTree->Branch("zddLeft_energy",&MCzddLeft_energy);
	m_AllMCTruthTree->Branch("zddRight_energy",&MCzddRight_energy);
	m_AllMCTruthTree->Branch("xImpact",&xFirstZDDHit);
	m_AllMCTruthTree->Branch("yImpact",&yFirstZDDHit);
	
	for(int i=0;i<24;i++)
			{
			char crystalID[256];
			if(i==0) sprintf(crystalID,"101");
			if(i==1) sprintf(crystalID,"102");
			if(i==2) sprintf(crystalID,"103");
			if(i==3) sprintf(crystalID,"104");
			if(i==4) sprintf(crystalID,"111");
			if(i==5) sprintf(crystalID,"112");
			if(i==6) sprintf(crystalID,"113");
			if(i==7) sprintf(crystalID,"114");
			if(i==8) sprintf(crystalID,"121");
			if(i==9) sprintf(crystalID,"122");
			if(i==10) sprintf(crystalID,"123");
			if(i==11) sprintf(crystalID,"124");
			if(i==12) sprintf(crystalID,"201");
			if(i==13) sprintf(crystalID,"202");
			if(i==14) sprintf(crystalID,"203");
			if(i==15) sprintf(crystalID,"204");
			if(i==16) sprintf(crystalID,"211");
			if(i==17) sprintf(crystalID,"212");
			if(i==18) sprintf(crystalID,"213");
			if(i==19) sprintf(crystalID,"214");			
			if(i==20) sprintf(crystalID,"221");
			if(i==21) sprintf(crystalID,"222");
			if(i==22) sprintf(crystalID,"223");
			if(i==23) sprintf(crystalID,"224");
				for(int j=0;j<3;j++)
				{
				char varName[256];
				if(j==0) 
					{sprintf(varName,"eDep%sLeft",crystalID);
					m_AllMCTruthTree->Branch(varName,&MCeDepSide[i][j]);
					}
				if(j==1) 
					{sprintf(varName,"eDep%sRight",crystalID);
					m_AllMCTruthTree->Branch(varName,&MCeDepSide[i][j]);
					}
				if(j==3) 
					{sprintf(varName,"eDep%s",crystalID);
					m_AllMCTruthTree->Branch(varName,&MCeDep[i]);
					}
				}
			}
	if(m_BBBREM==1) m_AllMCTruthTree->Branch("weight",&weight);
	
	m_particlegunTree = new TTree("particlegunTree", "particlegunTree");
	
	m_particlegunTree->Branch("run", &run);
	m_particlegunTree->Branch("event", &evt);
	m_particlegunTree->Branch("MCpGunTrackID", &MCpGunTrackID);
	m_particlegunTree->Branch("MCpGunpartProp", &MCpGunpartProp);
	m_particlegunTree->Branch("MCpGuniniPosX", &MCpGuniniPosX);
	m_particlegunTree->Branch("MCpGuniniPosY", &MCpGuniniPosY);
	m_particlegunTree->Branch("MCpGuniniPosZ", &MCpGuniniPosZ);
	m_particlegunTree->Branch("MCpGunfinPosX", &MCpGunfinPosX);
	m_particlegunTree->Branch("MCpGunfinPosY", &MCpGunfinPosY);
	m_particlegunTree->Branch("MCpGunfinPosZ", &MCpGunfinPosZ);
	m_particlegunTree->Branch("MCpGunMomX", &MCpGunMomX);
	m_particlegunTree->Branch("MCpGunMomY", &MCpGunMomY);
	m_particlegunTree->Branch("MCpGunMomZ", &MCpGunMomZ);
	m_particlegunTree->Branch("MCpGunMomRho", &MCpGunMomRho);
	m_particlegunTree->Branch("MCpGunMomTheta", &MCpGunMomTheta);
	m_particlegunTree->Branch("MCpGunMomPhi", &MCpGunMomPhi);
	m_particlegunTree->Branch("MCpGunE", &MCpGunE);
	m_particlegunTree->Branch("MCpGunXCalcatZDD", &MCpGunXCalcatZDD);
	m_particlegunTree->Branch("MCpGunYCalcatZDD", &MCpGunYCalcatZDD);
	m_particlegunTree->Branch("zdd_partID",&zdd_partID);
  m_particlegunTree->Branch("zdd_energy",&zdd_energy);
  m_particlegunTree->Branch("zdd_time",&zdd_time);
	m_particlegunTree->Branch("eDep101",&eDep[0]);
  m_particlegunTree->Branch("eDep102",&eDep[1]);
  m_particlegunTree->Branch("eDep103",&eDep[2]);
  m_particlegunTree->Branch("eDep104",&eDep[3]);
  m_particlegunTree->Branch("eDep111",&eDep[4]);
  m_particlegunTree->Branch("eDep112",&eDep[5]);
  m_particlegunTree->Branch("eDep113",&eDep[6]);
  m_particlegunTree->Branch("eDep114",&eDep[7]);
  m_particlegunTree->Branch("eDep121",&eDep[8]);
  m_particlegunTree->Branch("eDep122",&eDep[9]);
  m_particlegunTree->Branch("eDep123",&eDep[10]);
  m_particlegunTree->Branch("eDep124",&eDep[11]);
  m_particlegunTree->Branch("eDep201",&eDep[12]);
  m_particlegunTree->Branch("eDep202",&eDep[13]);
  m_particlegunTree->Branch("eDep203",&eDep[14]);
  m_particlegunTree->Branch("eDep204",&eDep[15]);
  m_particlegunTree->Branch("eDep211",&eDep[16]);
  m_particlegunTree->Branch("eDep212",&eDep[17]);
  m_particlegunTree->Branch("eDep213",&eDep[18]);
  m_particlegunTree->Branch("eDep214",&eDep[19]);
  m_particlegunTree->Branch("eDep221",&eDep[20]);
  m_particlegunTree->Branch("eDep222",&eDep[21]);
  m_particlegunTree->Branch("eDep223",&eDep[22]);
  m_particlegunTree->Branch("eDep224",&eDep[23]);
	m_particlegunTree->Branch("eDep101Left",&eDepSide[0][0]);
  m_particlegunTree->Branch("eDep102Left",&eDepSide[1][0]);
  m_particlegunTree->Branch("eDep103Left",&eDepSide[2][0]);
  m_particlegunTree->Branch("eDep104Left",&eDepSide[3][0]);
  m_particlegunTree->Branch("eDep111Left",&eDepSide[4][0]);
  m_particlegunTree->Branch("eDep112Left",&eDepSide[5][0]);
  m_particlegunTree->Branch("eDep113Left",&eDepSide[6][0]);
  m_particlegunTree->Branch("eDep114Left",&eDepSide[7][0]);
  m_particlegunTree->Branch("eDep121Left",&eDepSide[8][0]);
  m_particlegunTree->Branch("eDep122Left",&eDepSide[9][0]);
  m_particlegunTree->Branch("eDep123Left",&eDepSide[10][0]);
  m_particlegunTree->Branch("eDep124Left",&eDepSide[11][0]);
  m_particlegunTree->Branch("eDep201Left",&eDepSide[12][0]);
  m_particlegunTree->Branch("eDep202Left",&eDepSide[13][0]);
  m_particlegunTree->Branch("eDep203Left",&eDepSide[14][0]);
  m_particlegunTree->Branch("eDep204Left",&eDepSide[15][0]);
  m_particlegunTree->Branch("eDep211Left",&eDepSide[16][0]);
  m_particlegunTree->Branch("eDep212Left",&eDepSide[17][0]);
  m_particlegunTree->Branch("eDep213Left",&eDepSide[18][0]);
  m_particlegunTree->Branch("eDep214Left",&eDepSide[19][0]);
  m_particlegunTree->Branch("eDep221Left",&eDepSide[20][0]);
  m_particlegunTree->Branch("eDep222Left",&eDepSide[21][0]);
  m_particlegunTree->Branch("eDep223Left",&eDepSide[22][0]);
  m_particlegunTree->Branch("eDep224Left",&eDepSide[23][0]);
	m_particlegunTree->Branch("eDep101Right",&eDepSide[0][1]);
  m_particlegunTree->Branch("eDep102Right",&eDepSide[1][1]);
  m_particlegunTree->Branch("eDep103Right",&eDepSide[2][1]);
  m_particlegunTree->Branch("eDep104Right",&eDepSide[3][1]);
  m_particlegunTree->Branch("eDep111Right",&eDepSide[4][1]);
  m_particlegunTree->Branch("eDep112Right",&eDepSide[5][1]);
  m_particlegunTree->Branch("eDep113Right",&eDepSide[6][1]);
  m_particlegunTree->Branch("eDep114Right",&eDepSide[7][1]);
  m_particlegunTree->Branch("eDep121Right",&eDepSide[8][1]);
  m_particlegunTree->Branch("eDep122Right",&eDepSide[9][1]);
  m_particlegunTree->Branch("eDep123Right",&eDepSide[10][1]);
  m_particlegunTree->Branch("eDep124Right",&eDepSide[11][1]);
  m_particlegunTree->Branch("eDep201Right",&eDepSide[12][1]);
  m_particlegunTree->Branch("eDep202Right",&eDepSide[13][1]);
  m_particlegunTree->Branch("eDep203Right",&eDepSide[14][1]);
  m_particlegunTree->Branch("eDep204Right",&eDepSide[15][1]);
  m_particlegunTree->Branch("eDep211Right",&eDepSide[16][1]);
  m_particlegunTree->Branch("eDep212Right",&eDepSide[17][1]);
  m_particlegunTree->Branch("eDep213Right",&eDepSide[18][1]);
  m_particlegunTree->Branch("eDep214Right",&eDepSide[19][1]);
  m_particlegunTree->Branch("eDep221Right",&eDepSide[20][1]);
  m_particlegunTree->Branch("eDep222Right",&eDepSide[21][1]);
  m_particlegunTree->Branch("eDep223Right",&eDepSide[22][1]);
  m_particlegunTree->Branch("eDep224Right",&eDepSide[23][1]);
	m_particlegunTree->Branch("xFirstZDDHit",&xFirstZDDHit);
  m_particlegunTree->Branch("yFirstZDDHit",&yFirstZDDHit);

	int hnum;

	m_myFile->mkdir("particlegun");
	m_myFile->cd("particlegun");
	hnum=1;
	int NBinsX=128;
	int NBinsY=128;
	m_hPGunMCiniMomX =	new TH1D(Form("%i",hnum),"Particule gun Initial Px - All MC",NBinsX,0.,0.);
	m_hPGunMCiniMomX->GetXaxis()->SetTitle("P_{X} (GeV/c)");
	hnum++;
	m_hPGunMCiniMomY =	new TH1D(Form("%i",hnum),"Particule gun Initial Py - All MC ",NBinsX,0.,0.);
	m_hPGunMCiniMomY->GetXaxis()->SetTitle("P_{Y} (GeV/c)");
	hnum++;
	m_hPGunMCiniMomZ =	new TH1D(Form("%i",hnum),"Particule gun Initial Pz - All MC ",NBinsX,0.,0.);
	m_hPGunMCiniMomZ->GetXaxis()->SetTitle("P_{Z} (GeV/c)");
	hnum++;
	m_hPGunMCiniE =	new TH1D(Form("%i",hnum),"Particule gun Initial Energy - All MC ",NBinsX,0.,0.);
	m_hPGunMCiniE->GetXaxis()->SetTitle("E (GeV/c)");
	hnum++;
	m_hPGunMCiniTheta =	new TH1D(Form("%i",hnum),"Particule gun Initial Theta - All MC ",NBinsX,0.,0.);
	m_hPGunMCiniTheta->GetXaxis()->SetTitle("#Theta (rad)");
	hnum++;
	m_hPGunMCiniPhi =	new TH1D(Form("%i",hnum),"Particule gun Initial Phi - All MC ",NBinsX,0.,0.);
	m_hPGunMCiniPhi->GetXaxis()->SetTitle("#phi (rad)");
	hnum++;
	m_hPGunMCfinPosX =	new TH1D(Form("%i",hnum),"Particule gun Final X - All MC ",NBinsX,0.,0.);
	m_hPGunMCfinPosX->GetXaxis()->SetTitle("X_{Final} (cm)");
	hnum++;
	m_hPGunMCfinPosY =	new TH1D(Form("%i",hnum),"Particule gun Final Y - All MC ",NBinsX,0.,0.);
	m_hPGunMCfinPosY->GetXaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hPGunMCfinPosZ =	new TH1D(Form("%i",hnum),"Particule gun Final Z - All MC ",NBinsX,0.,0.);
	m_hPGunMCfinPosZ->GetXaxis()->SetTitle("Z_{Final} (cm)");
	hnum++;
	m_hPGunMCExtrapolatedZDDHitXY =	new TH2D(Form("%i",hnum),"Particule gun Extrapolated Hit XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hPGunMCExtrapolatedZDDHitXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hPGunMCExtrapolatedZDDHitXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hPGunMCfinPosXY =	new TH2D(Form("%i",hnum),"Particule gun Final XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hPGunMCfinPosXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hPGunMCfinPosXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hPGunMCfinPosZX =	new TH2D(Form("%i",hnum),"Particule gun Final ZX - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hPGunMCfinPosZX->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hPGunMCfinPosZX->GetYaxis()->SetTitle("X_{Final} (cm)");
	hnum++;
	m_hPGunMCfinPosZY =	new TH2D(Form("%i",hnum),"Particule gun Final ZY - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hPGunMCfinPosZY->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hPGunMCfinPosZY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hPGunZDDFirstHitXY =	new TH2D(Form("%i",hnum),"First hit on ZDD XY - ZDD hit ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hPGunZDDFirstHitXY->GetXaxis()->SetTitle("X_{hit} (cm)");
	m_hPGunZDDFirstHitXY->GetYaxis()->SetTitle("Y_{hit} (cm)");
	hnum++;
	m_hPGunZDDDeltaHitXExtrapolX =	new TH1D(Form("%i",hnum),"X_{hit}-X_{extrapolated from IP} (cm)",NBinsX,0,0);
	m_hPGunZDDDeltaHitXExtrapolX->GetXaxis()->SetTitle("X_{hit}-X_{extrapolated from IP} (cm)");
	hnum++;
	m_hPGunZDDDeltaHitYExtrapolY =	new TH1D(Form("%i",hnum),"Y_{hit}-Y_{extrapolated from IP} (cm)",NBinsX,0,0);
	m_hPGunZDDDeltaHitYExtrapolY->GetXaxis()->SetTitle("Y_{hit}-Y_{extrapolated from IP} (cm)");
	hnum++;
	m_hPGunZDDEreco =	new TH1D(Form("%i",hnum),"Energy reconstructed - ZDD hit ",NBinsX,0.,0.);
	m_hPGunZDDEreco->GetXaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	m_hPGunZDDErecovsETruth =	new TH2D(Form("%i",hnum),"E_{reco} vs E_{Truth} - ZDD hit ",NBinsX,0.,3.,NBinsY,0.,3.);
	m_hPGunZDDErecovsETruth->GetXaxis()->SetTitle("E_{Truth} (GeV)");
	m_hPGunZDDErecovsETruth->GetYaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	Double_t xbins[5]={0,1.,2.,3.,4.};
	Double_t ybins[8]={-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5};
	//m_hPGunClustersZDDRight = new TH2D(Form("%i",hnum),"Cluster hit - ZDD Right",4,xbins,7,ybins);
	m_hPGunClustersZDDRight = new TH2D(Form("%i",hnum),"Cluster hit - ZDD Right",4,m_XEdgesZDDRight,7,m_YEdgesZDDRight);
	m_hPGunClustersZDDRight->GetXaxis()->SetTitle("X");
	m_hPGunClustersZDDRight->GetYaxis()->SetTitle("Y");
	hnum++;
	m_myFile->cd();
	
	m_BhabhaTree = new TTree("BhabhaTree", "BhabhaTree");
	m_BhabhaTree->Branch("run", &run);
	m_BhabhaTree->Branch("event", &evt);
	m_BhabhaTree->Branch("MCBhabhaElecTrackID", &MCBhabhaElecTrackID);
	m_BhabhaTree->Branch("MCBhabhaEleciniPosX", &MCBhabhaEleciniPosX);
	m_BhabhaTree->Branch("MCBhabhaEleciniPosY", &MCBhabhaEleciniPosY);
	m_BhabhaTree->Branch("MCBhabhaEleciniPosZ", &MCBhabhaEleciniPosZ);
	m_BhabhaTree->Branch("MCBhabhaElecfinPosX", &MCBhabhaElecfinPosX);
	m_BhabhaTree->Branch("MCBhabhaElecfinPosY", &MCBhabhaElecfinPosY);
	m_BhabhaTree->Branch("MCBhabhaElecfinPosZ", &MCBhabhaElecfinPosZ);
	m_BhabhaTree->Branch("MCBhabhaElecMomRho", &MCBhabhaElecMomRho);
	m_BhabhaTree->Branch("MCBhabhaElecMomTheta", &MCBhabhaElecMomTheta);
	m_BhabhaTree->Branch("MCBhabhaElecMomPhi", &MCBhabhaElecMomPhi);
	m_BhabhaTree->Branch("MCBhabhaElecE", &MCBhabhaElecE);
	m_BhabhaTree->Branch("MCBhabhaElecXCalcatZDD", &MCBhabhaElecXCalcatZDD);
	m_BhabhaTree->Branch("MCBhabhaElecYCalcatZDD", &MCBhabhaElecYCalcatZDD);
	
	m_BhabhaTree->Branch("MCBhabhaPositronTrackID", &MCBhabhaPositronTrackID);
	m_BhabhaTree->Branch("MCBhabhaPositroniniPosX", &MCBhabhaPositroniniPosX);
	m_BhabhaTree->Branch("MCBhabhaPositroniniPosY", &MCBhabhaPositroniniPosY);
	m_BhabhaTree->Branch("MCBhabhaPositroniniPosZ", &MCBhabhaPositroniniPosZ);
	m_BhabhaTree->Branch("MCBhabhaPositronfinPosX", &MCBhabhaPositronfinPosX);
	m_BhabhaTree->Branch("MCBhabhaPositronfinPosY", &MCBhabhaPositronfinPosY);
	m_BhabhaTree->Branch("MCBhabhaPositronfinPosZ", &MCBhabhaPositronfinPosZ);
	m_BhabhaTree->Branch("MCBhabhaPositronMomRho", &MCBhabhaPositronMomRho);
	m_BhabhaTree->Branch("MCBhabhaPositronMomTheta", &MCBhabhaPositronMomTheta);
	m_BhabhaTree->Branch("MCBhabhaPositronMomPhi", &MCBhabhaPositronMomPhi);
	m_BhabhaTree->Branch("MCBhabhaPositronE", &MCBhabhaPositronE);
	m_BhabhaTree->Branch("MCBhabhaPositronXCalcatZDD", &MCBhabhaPositronXCalcatZDD);
	m_BhabhaTree->Branch("MCBhabhaPositronYCalcatZDD", &MCBhabhaPositronYCalcatZDD);
	
	
	m_BhabhaTree->Branch("MCBhabhanGammas", &MCBhabhanGammas,"MCBhabhanGammas/I");
	m_BhabhaTree->Branch("MCBhabhaGamTrackID", MCBhabhaGamTrackID,"MCBhabhaGamTrackID[MCBhabhanGammas]/I");
	
	m_BhabhaTree->Branch("MCBhabhaGaminiPosX", MCBhabhaGaminiPosX, "MCBhabhaGaminiPosX[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGaminiPosY", MCBhabhaGaminiPosY, "MCBhabhaGaminiPosY[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGaminiPosZ", MCBhabhaGaminiPosZ, "MCBhabhaGaminiPosZ[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGamfinPosX", MCBhabhaGamfinPosX,"MCBhabhaGamfinPosX[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGamfinPosY", MCBhabhaGamfinPosY,"MCBhabhaGamfinPosY[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGamfinPosZ", MCBhabhaGamfinPosZ,"MCBhabhaGamfinPosZ[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGamMomRho", MCBhabhaGamMomRho,"MCBhabhaGamMomRho[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGamMomTheta", MCBhabhaGamMomTheta,"MCBhabhaGamMomTheta[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGamMomPhi", MCBhabhaGamMomPhi,"MCBhabhaGamMomPhi[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGamE", MCBhabhaGamE,"MCBhabhaGamE[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGamXCalcatZDD", MCBhabhaGamXCalcatZDD,"MCBhabhaGamXCalcatZDD[MCBhabhanGammas]/D");
	m_BhabhaTree->Branch("MCBhabhaGamYCalcatZDD", MCBhabhaGamYCalcatZDD,"MCBhabhaGamYCalcatZDD[MCBhabhanGammas]/D");
	
	m_BhabhaTree->Branch("zdd_partID",&zdd_partID);
  m_BhabhaTree->Branch("zdd_energy",&zdd_energy);
	m_BhabhaTree->Branch("zddLeft_energy",&zddLeft_energy);
	m_BhabhaTree->Branch("zddRight_energy",&zddRight_energy);
  m_BhabhaTree->Branch("zdd_time",&zdd_time);
	m_BhabhaTree->Branch("eDep101",&eDep[0]);
  m_BhabhaTree->Branch("eDep102",&eDep[1]);
  m_BhabhaTree->Branch("eDep103",&eDep[2]);
  m_BhabhaTree->Branch("eDep104",&eDep[3]);
  m_BhabhaTree->Branch("eDep111",&eDep[4]);
  m_BhabhaTree->Branch("eDep112",&eDep[5]);
  m_BhabhaTree->Branch("eDep113",&eDep[6]);
  m_BhabhaTree->Branch("eDep114",&eDep[7]);
  m_BhabhaTree->Branch("eDep121",&eDep[8]);
  m_BhabhaTree->Branch("eDep122",&eDep[9]);
  m_BhabhaTree->Branch("eDep123",&eDep[10]);
  m_BhabhaTree->Branch("eDep124",&eDep[11]);
  m_BhabhaTree->Branch("eDep201",&eDep[12]);
  m_BhabhaTree->Branch("eDep202",&eDep[13]);
  m_BhabhaTree->Branch("eDep203",&eDep[14]);
  m_BhabhaTree->Branch("eDep204",&eDep[15]);
  m_BhabhaTree->Branch("eDep211",&eDep[16]);
  m_BhabhaTree->Branch("eDep212",&eDep[17]);
  m_BhabhaTree->Branch("eDep213",&eDep[18]);
  m_BhabhaTree->Branch("eDep214",&eDep[19]);
  m_BhabhaTree->Branch("eDep221",&eDep[20]);
  m_BhabhaTree->Branch("eDep222",&eDep[21]);
  m_BhabhaTree->Branch("eDep223",&eDep[22]);
  m_BhabhaTree->Branch("eDep224",&eDep[23]);
	m_BhabhaTree->Branch("eDep101Left",&eDepSide[0][0]);
  m_BhabhaTree->Branch("eDep102Left",&eDepSide[1][0]);
  m_BhabhaTree->Branch("eDep103Left",&eDepSide[2][0]);
  m_BhabhaTree->Branch("eDep104Left",&eDepSide[3][0]);
  m_BhabhaTree->Branch("eDep111Left",&eDepSide[4][0]);
  m_BhabhaTree->Branch("eDep112Left",&eDepSide[5][0]);
  m_BhabhaTree->Branch("eDep113Left",&eDepSide[6][0]);
  m_BhabhaTree->Branch("eDep114Left",&eDepSide[7][0]);
  m_BhabhaTree->Branch("eDep121Left",&eDepSide[8][0]);
  m_BhabhaTree->Branch("eDep122Left",&eDepSide[9][0]);
  m_BhabhaTree->Branch("eDep123Left",&eDepSide[10][0]);
  m_BhabhaTree->Branch("eDep124Left",&eDepSide[11][0]);
  m_BhabhaTree->Branch("eDep201Left",&eDepSide[12][0]);
  m_BhabhaTree->Branch("eDep202Left",&eDepSide[13][0]);
  m_BhabhaTree->Branch("eDep203Left",&eDepSide[14][0]);
  m_BhabhaTree->Branch("eDep204Left",&eDepSide[15][0]);
  m_BhabhaTree->Branch("eDep211Left",&eDepSide[16][0]);
  m_BhabhaTree->Branch("eDep212Left",&eDepSide[17][0]);
  m_BhabhaTree->Branch("eDep213Left",&eDepSide[18][0]);
  m_BhabhaTree->Branch("eDep214Left",&eDepSide[19][0]);
  m_BhabhaTree->Branch("eDep221Left",&eDepSide[20][0]);
  m_BhabhaTree->Branch("eDep222Left",&eDepSide[21][0]);
  m_BhabhaTree->Branch("eDep223Left",&eDepSide[22][0]);
  m_BhabhaTree->Branch("eDep224Left",&eDepSide[23][0]);
	m_BhabhaTree->Branch("eDep101Right",&eDepSide[0][1]);
  m_BhabhaTree->Branch("eDep102Right",&eDepSide[1][1]);
  m_BhabhaTree->Branch("eDep103Right",&eDepSide[2][1]);
  m_BhabhaTree->Branch("eDep104Right",&eDepSide[3][1]);
  m_BhabhaTree->Branch("eDep111Right",&eDepSide[4][1]);
  m_BhabhaTree->Branch("eDep112Right",&eDepSide[5][1]);
  m_BhabhaTree->Branch("eDep113Right",&eDepSide[6][1]);
  m_BhabhaTree->Branch("eDep114Right",&eDepSide[7][1]);
  m_BhabhaTree->Branch("eDep121Right",&eDepSide[8][1]);
  m_BhabhaTree->Branch("eDep122Right",&eDepSide[9][1]);
  m_BhabhaTree->Branch("eDep123Right",&eDepSide[10][1]);
  m_BhabhaTree->Branch("eDep124Right",&eDepSide[11][1]);
  m_BhabhaTree->Branch("eDep201Right",&eDepSide[12][1]);
  m_BhabhaTree->Branch("eDep202Right",&eDepSide[13][1]);
  m_BhabhaTree->Branch("eDep203Right",&eDepSide[14][1]);
  m_BhabhaTree->Branch("eDep204Right",&eDepSide[15][1]);
  m_BhabhaTree->Branch("eDep211Right",&eDepSide[16][1]);
  m_BhabhaTree->Branch("eDep212Right",&eDepSide[17][1]);
  m_BhabhaTree->Branch("eDep213Right",&eDepSide[18][1]);
  m_BhabhaTree->Branch("eDep214Right",&eDepSide[19][1]);
  m_BhabhaTree->Branch("eDep221Right",&eDepSide[20][1]);
  m_BhabhaTree->Branch("eDep222Right",&eDepSide[21][1]);
  m_BhabhaTree->Branch("eDep223Right",&eDepSide[22][1]);
  m_BhabhaTree->Branch("eDep224Right",&eDepSide[23][1]);
	m_BhabhaTree->Branch("xFirstZDDHit",&xFirstZDDHit);
  m_BhabhaTree->Branch("yFirstZDDHit",&yFirstZDDHit);
	m_BhabhaTree->Branch("xFirstElecZDDHit",&xFirstElecZDDHit);
  m_BhabhaTree->Branch("yFirstElecZDDHit",&yFirstElecZDDHit);
	m_BhabhaTree->Branch("xFirstPositronZDDHit",&xFirstPositronZDDHit);
  m_BhabhaTree->Branch("yFirstPositronZDDHit",&yFirstPositronZDDHit);	m_BhabhaTree->Branch("MCBhabhaMomSum3VElectronPositron",&MCBhabhaMomSum3VElectronPositron);
	m_BhabhaTree->Branch("MCBhabhaMissMomX",&MCBhabhaMissMomX);
  m_BhabhaTree->Branch("MCBhabhaMissMomY",&MCBhabhaMissMomY);
	m_BhabhaTree->Branch("MCBhabhaMissMomZ",&MCBhabhaMissMomZ);
	if(m_BBBREM==1) m_BhabhaTree->Branch("weight",&weight);
	
	m_myFile->mkdir("Bhabha");
	m_myFile->cd("Bhabha");	
	hnum=1;
	
	m_hMCBhabhaEleciniE =	new TH1D(Form("%i",hnum),"Bhabha e^{-} Initial Energy - All MC",NBinsX,0.,0.);
	m_hMCBhabhaEleciniE->GetXaxis()->SetTitle("E_{e-} (GeV)");
	hnum++;
	m_hMCBhabhaEleciniMomRho =	new TH1D(Form("%i",hnum),"Bhabha e^{-} Momentum - All MC",NBinsX,0.,0.);
	m_hMCBhabhaEleciniMomRho->GetXaxis()->SetTitle("P_{e-} (GeV/c)");
	hnum++;
	m_hMCBhabhaEleciniMomTheta =	new TH1D(Form("%i",hnum),"Bhabha e^{-} #Theta - All MC",NBinsX,0.,0.);
	m_hMCBhabhaEleciniMomTheta->GetXaxis()->SetTitle("#Theta_{e-} (rad)");
	hnum++;
	m_hMCBhabhaEleciniMomPhi =	new TH1D(Form("%i",hnum),"Bhabha e^{-} #phi - All MC",NBinsX,0.,0.);
	m_hMCBhabhaEleciniMomPhi->GetXaxis()->SetTitle("#phi_{e-} (rad)");
	hnum++;
	m_hMCBhabhaElecExtrapolatedZDDHitXY =	new TH2D(Form("%i",hnum),"Bhabha e^{-} Extrapolated Hit XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hMCBhabhaElecExtrapolatedZDDHitXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hMCBhabhaElecExtrapolatedZDDHitXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hMCBhabhaElecfinPosXY =	new TH2D(Form("%i",hnum),"Bhabha e^{-} Final XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hMCBhabhaElecfinPosXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hMCBhabhaElecfinPosXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hMCBhabhaElecfinPosZX =	new TH2D(Form("%i",hnum),"Bhabha e^{-} Final ZX - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hMCBhabhaElecfinPosZX->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hMCBhabhaElecfinPosZX->GetYaxis()->SetTitle("X_{Final} (cm)");
	hnum++;
	m_hMCBhabhaElecfinPosZY =	new TH2D(Form("%i",hnum),"Bhabha e^{-} Final ZY - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hMCBhabhaElecfinPosZY->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hMCBhabhaElecfinPosZY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	
	m_hMCBhabhaPositroniniE =	new TH1D(Form("%i",hnum),"Bhabha e^{+} Initial Energy - All MC",NBinsX,0.,0.);
	m_hMCBhabhaPositroniniE->GetXaxis()->SetTitle("E_{e+} (GeV)");
	hnum++;
	m_hMCBhabhaPositroniniMomRho =	new TH1D(Form("%i",hnum),"Bhabha e^{+} Momentum - All MC",NBinsX,0.,0.);
	m_hMCBhabhaPositroniniMomRho->GetXaxis()->SetTitle("P_{e+} (GeV/c)");
	hnum++;
	m_hMCBhabhaPositroniniMomTheta =	new TH1D(Form("%i",hnum),"Bhabha e^{+} #Theta - All MC",NBinsX,0.,0.);
	m_hMCBhabhaPositroniniMomTheta->GetXaxis()->SetTitle("#Theta_{e+} (rad)");
	hnum++;
	m_hMCBhabhaPositroniniMomPhi =	new TH1D(Form("%i",hnum),"Bhabha e^{+} #phi - All MC",NBinsX,0.,0.);
	m_hMCBhabhaPositroniniMomPhi->GetXaxis()->SetTitle("#phi_{e+} (rad)");
	hnum++;
	m_hMCBhabhaPositronExtrapolatedZDDHitXY =	new TH2D(Form("%i",hnum),"Bhabha e^{+} Extrapolated Hit XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hMCBhabhaPositronExtrapolatedZDDHitXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hMCBhabhaPositronExtrapolatedZDDHitXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hMCBhabhaPositronfinPosXY =	new TH2D(Form("%i",hnum),"Bhabha e^{+} Final XY - All MC ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hMCBhabhaPositronfinPosXY->GetXaxis()->SetTitle("X_{Final} (cm)");
	m_hMCBhabhaPositronfinPosXY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hMCBhabhaPositronfinPosZX =	new TH2D(Form("%i",hnum),"Bhabha e^{+} Final ZX - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hMCBhabhaPositronfinPosZX->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hMCBhabhaPositronfinPosZX->GetYaxis()->SetTitle("X_{Final} (cm)");
	hnum++;
	m_hMCBhabhaPositronfinPosZY =	new TH2D(Form("%i",hnum),"Bhabha e^{+} Final ZY - All MC ",NBinsX,-400.,400.,NBinsY,-5.,5.);
	m_hMCBhabhaPositronfinPosZY->GetXaxis()->SetTitle("Z_{Final} (cm)");
	m_hMCBhabhaPositronfinPosZY->GetYaxis()->SetTitle("Y_{Final} (cm)");
	hnum++;
	m_hZDDBhabhaFirstHitXY =	new TH2D(Form("%i",hnum),"First hit on ZDD XY - ZDD hit ",NBinsX,-5.,5.,NBinsY,-5.,5.);
	m_hZDDBhabhaFirstHitXY->GetXaxis()->SetTitle("X_{hit} (cm)");
	m_hZDDBhabhaFirstHitXY->GetYaxis()->SetTitle("Y_{hit} (cm)");
	hnum++;
	m_hZDDBhabhaEreco =	new TH1D(Form("%i",hnum),"Energy reconstructed - ZDD hit ",NBinsX,0.,0.);
	m_hZDDBhabhaEreco->GetXaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	m_hZDDBhabhaErecovsETruth =	new TH2D(Form("%i",hnum),"E_{reco} vs E_{Truth} - ZDD hit ",NBinsX,0.,5.,NBinsY,0.,5.);
	m_hZDDBhabhaErecovsETruth->GetXaxis()->SetTitle("E_{Truth} (GeV)");
	m_hZDDBhabhaErecovsETruth->GetYaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	m_hZDDBhabhaErecoLeft =	new TH1D(Form("%i",hnum),"E_{reco} - ZDD Left ",NBinsX,0.,0.);
	m_hZDDBhabhaErecoLeft->GetXaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	m_hZDDBhabhaErecoRight =	new TH1D(Form("%i",hnum),"E_{reco} - ZDD Right ",NBinsX,0.,0.);
	m_hZDDBhabhaErecoRight->GetXaxis()->SetTitle("E_{reco} (GeV)");
	hnum++;
	m_hZDDBhabhaErecoLeftvsRight =	new TH2D(Form("%i",hnum),"E_{reco Left} vs E_{reco Right} - ZDD hit ",NBinsX,0.,2000.,NBinsY,0.,2000.);
	m_hZDDBhabhaErecoLeftvsRight->GetXaxis()->SetTitle("E_{reco Right} (GeV)");
	m_hZDDBhabhaErecoLeftvsRight->GetYaxis()->SetTitle("E_{reco Left} (GeV)");
	hnum++;
	m_myFile->cd();
	/*
	m_ZDDMcHitColTree = new TTree("m_ZDDMcHitColTree", "m_ZDDMcHitColTree");
	m_ZDDMcHitColTree->Branch("HitColtrackID",&HitColtrackID);
  m_ZDDMcHitColTree->Branch("HitColPID",&HitColPID);
	*/
	
	m_T4PiGamISR = new TTree("4PiGamISRTagged", "4PiGamISRTagged");
	m_T4PiGamISR->Branch("run", &run);
	m_T4PiGamISR->Branch("event", &evt);
	m_T4PiGamISRPip1.AttachToNtuple(m_T4PiGamISR,"Pip1",false);
	m_T4PiGamISRPip2.AttachToNtuple(m_T4PiGamISR,"Pip2",false);
	m_T4PiGamISRPim1.AttachToNtuple(m_T4PiGamISR,"Pim1",false);
	m_T4PiGamISRPim2.AttachToNtuple(m_T4PiGamISR,"Pim2",false);
	m_T4PiGamISRMiss4PiCX.AttachToNtuple(m_T4PiGamISR,"Miss4PiCX",false);	
	m_T4PIGamISRGamISRZddTag.AttachToNtuple(m_T4PiGamISR,"GamISRZddTag",false);	
	m_T4PiGamISRGamISRMCTruth.AttachToNtuple(m_T4PiGamISR,"GamISRMCTruth",false);
	
	m_T4PiGamISR->Branch("MissM4PiCX", &m_MissM4PiCX);

	m_T4PiGamISR->Branch("XZddRecHit", &m_XZddRecHit);
	m_T4PiGamISR->Branch("YZddRecHit", &m_YZddRecHit);
	m_T4PiGamISR->Branch("XZddHitMiss4PiCX", &m_XZddHitMiss4PiCX);
	m_T4PiGamISR->Branch("YZddHitMiss4PiCX", &m_YZddHitMiss4PiCX);
	
	m_T4PiGamISR->Branch("XZddGammaISRMCTruth", &m_XZddGammaISRMCTruth);
	m_T4PiGamISR->Branch("YZddGammaISRMCTruth", &m_YZddGammaISRMCTruth);
	
	m_T4PiGamISR->Branch("zdd_energy",&zdd_energy);
	m_T4PiGamISR->Branch("zddLeft_energy",&zddLeft_energy);
	m_T4PiGamISR->Branch("zddRight_energy",&zddRight_energy);
		
	m_T4PiGamISR->Branch("MCiniMomXCM", MCiniMomXCM,"MCiniMomXCM[MCntracks]/D");
  m_T4PiGamISR->Branch("MCiniMomYCM", MCiniMomYCM, "MCiniMomYCM[MCntracks]/D");
  m_T4PiGamISR->Branch("MCiniMomZCM", MCiniMomZCM, "MCiniMomZCM[MCntracks]/D");
	m_T4PiGamISR->Branch("MCiniMomRhoCM", MCiniMomRhoCM, "MCiniMomRhoCM[MCntracks]/D");
  m_T4PiGamISR->Branch("MCiniMomThetaCM", MCiniMomThetaCM,"MCiniMomThetaCM[MCntracks]/D");
  m_T4PiGamISR->Branch("MCiniMomPhiCM", MCiniMomPhiCM, "MCiniMomPhiCM[MCntracks]/D");
  m_T4PiGamISR->Branch("MCiniECM", MCiniECM,"MCiniECM[MCntracks]/D");
	
	//Load Impact_reco file (if any)
	m_IsImpactReco=false;
	m_ImpactRecoFile=TFile::Open(m_ImpactRecoFileName.c_str());
	if(m_ImpactRecoFile!=NULL)
		{m_IsImpactReco=true;
		m_ImpactRecoTree=(TTree*) m_ImpactRecoFile->Get("Impact_reco");
		m_ImpactRecoTree->SetBranchAddress("xImpact",&xFirstZDDHit);
		m_ImpactRecoTree->SetBranchAddress("yImpact",&yFirstZDDHit);
		
		m_ImpactRecoTree->SetBranchAddress("xImpactElec",&xFirstElecZDDHit);
		m_ImpactRecoTree->SetBranchAddress("yImpactElec",&yFirstElecZDDHit);
		
		m_ImpactRecoTree->SetBranchAddress("xImpactPositron",&xFirstPositronZDDHit);
		m_ImpactRecoTree->SetBranchAddress("yImpactPositron",&yFirstPositronZDDHit);
		
		m_ImpactRecoTree->SetBranchAddress("zdd_energy",&MCzdd_energy);
		m_ImpactRecoTree->SetBranchAddress("zddRight_energy",&MCzddRight_energy);
		m_ImpactRecoTree->SetBranchAddress("zddLeft_energy",&MCzddLeft_energy);
		
		for(int i=0;i<24;i++)
			{
			char crystalID[256];
			if(i==0) sprintf(crystalID,"101");
			if(i==1) sprintf(crystalID,"102");
			if(i==2) sprintf(crystalID,"103");
			if(i==3) sprintf(crystalID,"104");
			if(i==4) sprintf(crystalID,"111");
			if(i==5) sprintf(crystalID,"112");
			if(i==6) sprintf(crystalID,"113");
			if(i==7) sprintf(crystalID,"114");
			if(i==8) sprintf(crystalID,"121");
			if(i==9) sprintf(crystalID,"122");
			if(i==10) sprintf(crystalID,"123");
			if(i==11) sprintf(crystalID,"124");
			if(i==12) sprintf(crystalID,"201");
			if(i==13) sprintf(crystalID,"202");
			if(i==14) sprintf(crystalID,"203");
			if(i==15) sprintf(crystalID,"204");
			if(i==16) sprintf(crystalID,"211");
			if(i==17) sprintf(crystalID,"212");
			if(i==18) sprintf(crystalID,"213");
			if(i==19) sprintf(crystalID,"214");			
			if(i==20) sprintf(crystalID,"221");
			if(i==21) sprintf(crystalID,"222");
			if(i==22) sprintf(crystalID,"223");
			if(i==23) sprintf(crystalID,"224");
				for(int j=0;j<3;j++)
				{
				char varName[256];
				if(j==0) 
					{sprintf(varName,"eDep%sLeft",crystalID);
					m_ImpactRecoTree->SetBranchAddress(varName,&MCeDepSide[i][j]);
					}
				if(j==1) 
					{sprintf(varName,"eDep%sRight",crystalID);
					m_ImpactRecoTree->SetBranchAddress(varName,&MCeDepSide[i][j]);
					}
				if(j==3) 
					{sprintf(varName,"eDep%s",crystalID);
					m_ImpactRecoTree->SetBranchAddress(varName,&MCeDep[i]);
					}
				}
			}
		}
	else
		{
		xFirstZDDHit=0;
		yFirstZDDHit=0;
		
		xFirstElecZDDHit=0;
		yFirstElecZDDHit=0;
		
		xFirstPositronZDDHit=0;
		yFirstPositronZDDHit=0;
		
		MCzdd_energy=0.;
		MCzddRight_energy=0.;
		MCzddLeft_energy=0.;
		}
	
	StatusCode sc = Gaudi::svcLocator()->service( "VertexDbSvc", m_vtxSvc,true);
   if(sc.isFailure())
     {
     //assert(false); //terminate program
     log << MSG::ERROR << "Cannot book VertexDbSvc!!" <<endmsg;
     return StatusCode::FAILURE;
   }
	 
  log << MSG::INFO << "successfully return from initialize()" <<endmsg;
  return StatusCode::SUCCESS;

}

//---------------------------------------------------------------------------------------------------------- 
StatusCode analysis_cZDD::execute() {
  
  std::cout << "execute()" << std::endl;

  G4bool debug = false; // couts on ?


  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in execute()" << endreq;

	//Initialisation of flags
	m_IsZDDhit=false;
	//Particle gun
	m_pGunMCfound=false;
	//Bhabha
	m_MCIsBhabha=false;
	m_MCHasPrimElectron=false;
	m_MCHasPrimPositron=false;
	m_MCNPrimElectron=0;
	m_MCNPrimPositron=0;
	m_MCNPrimGammas=0;
	
	m_mapcrystalenergyZDD.clear();
	
  SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
  int runNo=eventHeader->runNumber();
  int event=eventHeader->eventNumber();
	if(m_IsImpactReco)m_ImpactRecoTree->GetEntry(event);
	m_dCMSEnergy=beam_energy(abs(eventHeader->runNumber()));
  if (m_dCMSEnergy == -1) {log<<MSG::FATAL<<"unknown runNr, can not determine CMS Energy!"<<endreq; return StatusCode::FAILURE;}  
	
	double TotEngBeam = m_dCMSEnergy/2 + m_dMasselec;
	double Abs3MomBeam = sqrt(TotEngBeam*TotEngBeam - m_dMasselec*m_dMasselec);
		
	m_lvBeamElectron.SetPx(Abs3MomBeam*sin(m_dCrossingAngle));
	m_lvBeamElectron.SetPy(0);
	m_lvBeamElectron.SetPz(-Abs3MomBeam*cos(m_dCrossingAngle));
	m_lvBeamElectron.SetE(TotEngBeam);
  
	m_lvBeamPositron.SetPx(Abs3MomBeam*sin(m_dCrossingAngle));
	m_lvBeamPositron.SetPy(0);
	m_lvBeamPositron.SetPz(Abs3MomBeam*cos(m_dCrossingAngle));
	m_lvBeamPositron.SetE(TotEngBeam);
	
	m_lvBoost        = m_lvBeamElectron+m_lvBeamPositron;

	m_vectboost=m_lvBoost.BoostVector();		
		
  log << MSG::DEBUG <<"run, evtnum = "
      << runNo << " , "
      << event <<endreq;
  cout<<"event "<<event<<endl;
	
	//------ initialize virtual box ------//
    m_hp3IP.setX(0);
    m_hp3IP.setY(0);
    m_hp3IP.setZ(0);
    if (m_vtxSvc->isVertexValid()) {
      double* dbv =  m_vtxSvc->PrimaryVertex();
      m_hp3IP.setX(dbv[0]);
      m_hp3IP.setY(dbv[1]);
      m_hp3IP.setZ(dbv[2]);
    }
    else{std::cout<<"no IP"<<std::endl;}
		
	if(m_BBBREM==1){
	string a,b;
	if(!m_ReadWeightFile.good()) return StatusCode::SUCCESS;
	m_ReadWeightFile >> a >> b;
	printf("WEIGHT.out : %f %f \n",a,b);
	weight = atof(b.c_str());
	}
	
  Ncut0++;

  SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), EventModel::EvtRec::EvtRecEvent);
  //  log << MSG::INFO << "get event tag OK" << endreq;
    log << MSG::DEBUG <<"ncharg, nneu, tottks = " 
      << evtRecEvent->totalCharged() << " , "
      << evtRecEvent->totalNeutral() << " , "
      << evtRecEvent->totalTracks() <<endreq;

  SmartDataPtr<EvtRecTrackCol> evtRecTrkCol(eventSvc(),  EventModel::EvtRec::EvtRecTrackCol);
  //
  // check x0, y0, z0, r0
  // suggest cut: |z0|<5 && r0<1
  //
  
  // collect zdd digi information -> if energy is deposited, look under which angle the isr_photon was emitted
  //default values for unfired crystals 
	
	SmartDataPtr<ZddMcHitCol> zddMcHitCol(eventSvc(), "/Event/MC/ZddMcHitCol");                  if(!zddMcHitCol)
    {
      G4cout<<"Could not retrieve ZDD MC Hit collection"<<G4endl;                                                                                                                  return StatusCode::FAILURE;  
    }
	                     
	 for (ZddMcHitCol::iterator iterZDD = zddMcHitCol->begin();iterZDD != zddMcHitCol->end(); iterZDD++) {
		double edep = (*iterZDD)->getEDep();
		printf("(*iterZDD)->getEDep() = %f \n", edep);
		printf("(*iterZDD)->getPDGCode() = %d \n", (*iterZDD)->getPDGCode());
		printf("(*iterZDD)->getTrackID() = %d \n", (*iterZDD)->getTrackID());
    }
	
  SmartDataPtr<ZddDigiCol> zddDigiCol(eventSvc(), EventModel::Digi::ZddDigiCol);                                                                                         
  if(!zddDigiCol)
    {
      G4cout<<"Could not retrieve Zdd digi collection"<<G4endl;                                                                                                                  return StatusCode::FAILURE;  
    }
  double zddenergy = 0.;  
  double zddtime = 0.; 
  double zddenergyLeft =0.;
	double zddenergyRight = 0.;
  int nhits = 0;                                                                                                                                                          
  ZddDigiCol::iterator iDigiCol;                                                                                                                                          

  for(int w =0; w <=23; w++) {eDep[w] = 0.;eDepSide[w][0]=0;eDepSide[w][1]=0;}
  for(iDigiCol=zddDigiCol->begin(); iDigiCol!=zddDigiCol->end(); iDigiCol++){
			Identifier zddID = (*iDigiCol)->identify();
      int crystalNo =ZddID::crystalNo(zddID);
      if(debug) cout << "analysis_cZDD.cxx : crystalNo : " << crystalNo << endl;
      int partID = ZddID::partID(zddID); //1 : East Detector 2: West Detector
			zdd_partID= partID;
      long int chargeChannel = (*iDigiCol)->getChargeChannel();
      if(debug) cout << "analysis_cZDD.cxx : chargeChannel : " << chargeChannel << endl;
      long int timeChannel = (*iDigiCol)->getTimeChannel();
      if(debug) cout << "analysis_cZDD.cxx : timeChannel : " << timeChannel << endl;
      long int zddTime = timeChannel/1000000;
      zddtime += zddTime;
			double crystalEnergy=0;
			
			if(m_UseMCTruthEDep==1) {
			int idxCrystal, idxPart;
			
			if(partID==1) idxPart=0;
			if(partID==2) idxPart=1;
			
			if(abs(crystalNo) == 101) idxCrystal=0;
			if(abs(crystalNo) == 102) idxCrystal=1;
			if(abs(crystalNo) == 103) idxCrystal=2;
			if(abs(crystalNo) == 104) idxCrystal=3;
			if(abs(crystalNo) == 111) idxCrystal=4;
			if(abs(crystalNo) == 112) idxCrystal=5;
			if(abs(crystalNo) == 113) idxCrystal=6;
			if(abs(crystalNo) == 114) idxCrystal=7;
			if(abs(crystalNo) == 121) idxCrystal=8;
			if(abs(crystalNo) == 122) idxCrystal=9;
			if(abs(crystalNo) == 123) idxCrystal=10;
			if(abs(crystalNo) == 124) idxCrystal=11;
			if(abs(crystalNo) == 201) idxCrystal=12;
			if(abs(crystalNo) == 202) idxCrystal=13;
			if(abs(crystalNo) == 203) idxCrystal=14;
			if(abs(crystalNo) == 204) idxCrystal=15;
			if(abs(crystalNo) == 211) idxCrystal=16;
			if(abs(crystalNo) == 212) idxCrystal=17;
			if(abs(crystalNo) == 213) idxCrystal=18;
			if(abs(crystalNo) == 214) idxCrystal=19;
			if(abs(crystalNo) == 221) idxCrystal=20;
			if(abs(crystalNo) == 222) idxCrystal=21;
			if(abs(crystalNo) == 223) idxCrystal=22;
			if(abs(crystalNo) == 224) idxCrystal=23;
						
			crystalEnergy=MCeDepSide[idxCrystal][idxPart];
			}
      else{ crystalEnergy = charge2energy((*iDigiCol)->getChargeChannel());
			crystalEnergy = charge2energy((*iDigiCol)->getChargeChannel());    
			}
			
			if(debug) cout << "analysis_cZDD.cxx : crystalEnergy : " << crystalEnergy << endl;
      zddenergy += crystalEnergy;  
			if(partID==1)zddenergyLeft+= crystalEnergy;
			if(partID==2)zddenergyRight+= crystalEnergy;
      if(debug) cout << "analysis_cZDD.cxx : partID : " << partID << endl;
			vector<int> vecCrystalNoPartID;
			vecCrystalNoPartID.resize(2);
			vecCrystalNoPartID[0]=crystalNo;
			vecCrystalNoPartID[1]=partID;
			m_mapcrystalenergyZDD[vecCrystalNoPartID]=crystalEnergy;
      switch( abs(crystalNo) )
	{
	case 101:
	  eDep[0] = crystalEnergy;
	  break;
        case 102:
          eDep[1] = crystalEnergy;       
          break;
        case 103:
	  eDep[2] = crystalEnergy;
          break;
        case 104:
	  eDep[3] = crystalEnergy;
          break;
	case 111:
	  eDep[4] = crystalEnergy;
          break;
        case 112:
	  eDep[5] = crystalEnergy;
          break;
        case 113:
	  eDep[6] = crystalEnergy;
          break;
        case 114:
	  eDep[7] = crystalEnergy;
          break;
        case 121:
	  eDep[8] = crystalEnergy;
          break;
        case 122:
	  eDep[9] = crystalEnergy;
          break;
        case 123:
	  eDep[10] = crystalEnergy;
	  break;
        case 124:
	  eDep[11] = crystalEnergy;
          break;
        case 201:
	  eDep[12] = crystalEnergy;
          break;
        case 202:
          eDep[13]= crystalEnergy;
          break;
        case 203:
          eDep[14] = crystalEnergy;
          break;
        case 204:
          eDep[15] = crystalEnergy;
          break;
        case 211:
          eDep[16] = crystalEnergy;
          break;
        case 212:
          eDep[17] = crystalEnergy;
          break;
        case 213:
          eDep[18] = crystalEnergy;
          break;
        case 214:
          eDep[19] = crystalEnergy;
          break;
        case 221:
          eDep[20] = crystalEnergy;
          break;
        case 222:
          eDep[21] = crystalEnergy;
          break;
        case 223:
          eDep[22] = crystalEnergy;
          break;
        case 224:
          eDep[23] = crystalEnergy;
          break;
	}//end switch(abs(crystalNo))
   
	 switch(partID){ 
	 case 1:
	 switch( crystalNo )
	{
	case 101:
	  eDepSide[0][0] = crystalEnergy;
	  break;
        case 102:
          eDepSide[1][0] = crystalEnergy;       
          break;
        case 103:
	  eDepSide[2][0] = crystalEnergy;
          break;
        case 104:
	  eDepSide[3][0] = crystalEnergy;
          break;
	case 111:
	  eDepSide[4][0] = crystalEnergy;
          break;
        case 112:
	  eDepSide[5][0] = crystalEnergy;
          break;
        case 113:
	  eDepSide[6][0] = crystalEnergy;
          break;
        case 114:
	  eDepSide[7][0] = crystalEnergy;
          break;
        case 121:
	  eDepSide[8][0] = crystalEnergy;
          break;
        case 122:
	  eDepSide[9][0] = crystalEnergy;
          break;
        case 123:
	  eDepSide[10][0] = crystalEnergy;
	  break;
        case 124:
	  eDepSide[11][0] = crystalEnergy;
          break;
        case 201:
	  eDepSide[12][0] = crystalEnergy;
          break;
        case 202:
          eDepSide[13][0]= crystalEnergy;
          break;
        case 203:
          eDepSide[14][0] = crystalEnergy;
          break;
        case 204:
          eDepSide[15][0] = crystalEnergy;
          break;
        case 211:
          eDepSide[16][0] = crystalEnergy;
          break;
        case 212:
          eDepSide[17][0] = crystalEnergy;
          break;
        case 213:
          eDepSide[18][0] = crystalEnergy;
          break;
        case 214:
          eDepSide[19][0] = crystalEnergy;
          break;
        case 221:
          eDepSide[20][0] = crystalEnergy;
          break;
        case 222:
          eDepSide[21][0] = crystalEnergy;
          break;
        case 223:
          eDepSide[22][0] = crystalEnergy;
          break;
        case 224:
          eDepSide[23][0] = crystalEnergy;
          break;			
		}
	break;
	 case 2:  
			switch( crystalNo )
	{
	case 101:
	  eDepSide[0][1] = crystalEnergy;
	  break;
        case 102:
          eDepSide[1][1] = crystalEnergy;       
          break;
        case 103:
	  eDepSide[2][1] = crystalEnergy;
          break;
        case 104:
	  eDepSide[3][1] = crystalEnergy;
          break;
	case 111:
	  eDepSide[4][1] = crystalEnergy;
          break;
        case 112:
	  eDepSide[5][1] = crystalEnergy;
          break;
        case 113:
	  eDepSide[6][1] = crystalEnergy;
          break;
        case 114:
	  eDepSide[7][1] = crystalEnergy;
          break;
        case 121:
	  eDepSide[8][1] = crystalEnergy;
          break;
        case 122:
	  eDepSide[9][1] = crystalEnergy;
          break;
        case 123:
	  eDepSide[10][1] = crystalEnergy;
	  break;
        case 124:
	  eDepSide[11][1] = crystalEnergy;
          break;
        case 201:
	  eDepSide[12][1] = crystalEnergy;
          break;
        case 202:
          eDepSide[13][1]= crystalEnergy;
          break;
        case 203:
          eDepSide[14][1] = crystalEnergy;
          break;
        case 204:
          eDepSide[15][1] = crystalEnergy;
          break;
        case 211:
          eDepSide[16][1] = crystalEnergy;
          break;
        case 212:
          eDepSide[17][1] = crystalEnergy;
          break;
        case 213:
          eDepSide[18][1] = crystalEnergy;
          break;
        case 214:
          eDepSide[19][1] = crystalEnergy;
          break;
        case 221:
          eDepSide[20][1] = crystalEnergy;
          break;
        case 222:
          eDepSide[21][1] = crystalEnergy;
          break;
        case 223:
          eDepSide[22][1] = crystalEnergy;
          break;
        case 224:
          eDepSide[23][1] = crystalEnergy;
          break;			
		}
	break;
	}
      zddtime += (*iDigiCol)->getTimeChannel()/1000000.;
      nhits++;

}// end for(iDigiCol)
/*
  cout << "nhits           : " << nhits << endl;
  cout << "zddenergy  : " << zddenergy << endl;
  cout << "zddtime [ns]    : " << (zddtime/nhits)/ns << endl << "============================" << endl;
	*/
  /*
  if(debug && zddenergy > 0)
    {
      for(int i=0; i<=23; i++)
	{
	  cout << "analysis_cZDD: crystaldata[" << i << "] partID       :  " << crystaldata[i]->GetPartID() << endl;
	  cout << "analysis_cZDD: crystaldata[" << i << "] crystalNo    :  " << crystaldata[i]->GetCrystalNo() << endl;
          cout << "analysis_cZDD: crystaldata[" << i << "] chargeChannel:  " << crystaldata[i]->GetChargeChannel() << endl;
          cout << "analysis_cZDD: crystaldata[" << i << "] timeChannel  :  " << crystaldata[i]->GetTimeChannel() << endl;
          cout << "analysis_cZDD: crystaldata[" << i << "] energy       :  " << crystaldata[i]->GetEnergy() << endl;
          cout << "analysis_cZDD: crystaldata[" << i << "] time         :  " << crystaldata[i]->GetTime() << endl;
	  cout << "========================================================================" << endl;

	}//end for(int i)
    }//if(debug)
*/

zdd_energy = zddenergy;
zdd_time = zddtime/nhits;

zddLeft_energy=zddenergyLeft;
zddRight_energy=zddenergyRight;
	
if(zdd_energy>0) m_IsZDDhit=true;

  SmartDataPtr<Event::McParticleCol> mcParticleCol(eventSvc(), "/Event/MC/McParticleCol");
  if(!mcParticleCol)
    { 
      G4cout<<"Could not retrieve mcParticleCol"<<G4endl;
      return StatusCode::FAILURE;
    }
  else
    {
      Event::McParticleCol::iterator iter_mc = mcParticleCol->begin();
			Event::McParticleCol::iterator first = mcParticleCol->begin();
			Event::McParticleCol::iterator last = mcParticleCol->end();
			//std::cout << "The distance is: " << std::distance(first,last) << '\n';
			MCntracks=0;
      for (; iter_mc != mcParticleCol->end(); iter_mc++)
      {
// 	runNo
// 	event
	evt = event;
	run = runNo;
	partProp = (*iter_mc)->particleProperty();
	primPart = (*iter_mc)->primaryParticle();
	trackidx = (*iter_mc)->trackIndex();
	mother = ((*iter_mc)->mother()).particleProperty();
	leafPart = (*iter_mc)->leafParticle();
	iniPosX = (*iter_mc)->initialPosition().x();
	iniPosY = (*iter_mc)->initialPosition().y();
	iniPosZ = (*iter_mc)->initialPosition().z();
	iniPosT = (*iter_mc)->initialPosition().t();
	finPosX = (*iter_mc)->finalPosition().x();
	finPosY = (*iter_mc)->finalPosition().y();
	finPosZ = (*iter_mc)->finalPosition().z();
	finPosT = (*iter_mc)->finalPosition().t();
	iniMomX = (*iter_mc)->initialFourMomentum().x();
	iniMomY = (*iter_mc)->initialFourMomentum().y();
	iniMomZ = (*iter_mc)->initialFourMomentum().z();
	iniMomRho = (*iter_mc)->initialFourMomentum().rho();
	iniMomTheta = (*iter_mc)->initialFourMomentum().theta();
	iniMomPhi = (*iter_mc)->initialFourMomentum().phi();
	iniE = (*iter_mc)->initialFourMomentum().t();
	if(m_KKMC==1){
		if(mother!=30443) {continue;}
		else{primPart=1;}
	}
	
	m_leoTree->Fill();
	
	MCTrackIndex[MCntracks] = trackidx;
	MCpartProp[MCntracks]= partProp;
	MCprimPart[MCntracks]= primPart;
	MCMother[MCntracks]= mother;
	MCiniPosX[MCntracks] = iniPosX;
	MCiniPosY[MCntracks] = iniPosY;
	MCiniPosZ[MCntracks] = iniPosZ;
	MCfinPosX[MCntracks] = finPosX;
	MCfinPosY[MCntracks] = finPosY;
	MCfinPosZ[MCntracks] = finPosZ;
	MCiniMomX[MCntracks] = iniMomX;
	MCiniMomY[MCntracks] = iniMomY;
	MCiniMomZ[MCntracks] = iniMomZ;
	MCiniMomRho[MCntracks] = iniMomRho;
	MCiniMomTheta[MCntracks] = iniMomTheta;
	MCiniMomPhi[MCntracks] = iniMomPhi;
	MCiniE[MCntracks] = iniE;
	
	TLorentzVector lv;
	lv.SetPxPyPzE(MCiniMomX[MCntracks],MCiniMomY[MCntracks],MCiniMomZ[MCntracks],MCiniE[MCntracks]);
	TLorentzVector lvCM=lv;
	lvCM.Boost(-m_vectboost);
		
	MCiniMomXCM[MCntracks]=	lvCM.Px();
	MCiniMomXCM[MCntracks]=	lvCM.Px();
	MCiniMomYCM[MCntracks]=	lvCM.Py();
	MCiniMomZCM[MCntracks]=	lvCM.Pz();		
	MCiniMomRhoCM[MCntracks]=	lvCM.Rho();
	MCiniMomThetaCM[MCntracks]=	lvCM.Theta();
	MCiniMomPhiCM[MCntracks]=	lvCM.Phi();
	MCiniECM[MCntracks]=	lvCM.E();	
		
	/*
	double distance_to_zaxis_at_zdd = tan(MCiniMomTheta[index]) * 335.;
	MCXCalcatZDD[index] = cos(MCiniMomPhi[index]) * distance_to_zaxis_at_zdd; // x coordinate of photon at z = +-333cm (zdd position)                                              
  MCYCalcatZDD[index] = sin(MCiniMomPhi[index]) * distance_to_zaxis_at_zdd; // y coordinate of photon at z = +-333cm (zdd position) 
	*/
						
	Hep3Vector V3Mom;
	V3Mom.setRThetaPhi(MCiniMomRho[MCntracks],MCiniMomTheta[MCntracks],MCiniMomPhi[MCntracks]);
	//Stretch V3Mom to ZDD
	Hep3Vector V3atZDD;
	double zZDD;
	double RhoV3atZDD;	
	if(V3Mom.z()<0)
		{zZDD=-335;
		RhoV3atZDD=zZDD/cos(MCiniMomTheta[MCntracks]);
		V3Mom.setRThetaPhi(RhoV3atZDD,MCiniMomTheta[MCntracks],MCiniMomPhi[MCntracks]);
		MCXCalcatZDD[MCntracks]=V3Mom.x();
		MCYCalcatZDD[MCntracks]=V3Mom.y();
		}
	else if(V3Mom.z()>0) 
		{zZDD=335;
		RhoV3atZDD=zZDD/cos(MCiniMomTheta[MCntracks]);
		V3Mom.setRThetaPhi(RhoV3atZDD,MCiniMomTheta[MCntracks],MCiniMomPhi[MCntracks]);
		MCXCalcatZDD[MCntracks]=V3Mom.x();
		MCYCalcatZDD[MCntracks]=V3Mom.y();
		}
	else {MCXCalcatZDD[MCntracks]=-99;MCYCalcatZDD[MCntracks]=99;}
	
	//printf("******* %f %f ******* \n",V3Mom.x(),V3Mom.y());
	
	if( (*iter_mc)->particleProperty() == 22 && zdd_energy > 0.)
	  {
	    double x = (*iter_mc)->initialFourMomentum().x(); // x at vertex 
	    double y = (*iter_mc)->initialFourMomentum().y(); // y at vertex
	    double z = (*iter_mc)->initialFourMomentum().z(); // z at vertex	  
	    double t = (*iter_mc)->initialFourMomentum().t(); // t at vertex
	    
	    double px = (*iter_mc)->initialFourMomentum().px(); // px at vertex
	    double py = (*iter_mc)->initialFourMomentum().py(); // py at vertex
	    double pz = (*iter_mc)->initialFourMomentum().pz(); // pz at vertex
	    double e = (*iter_mc)->initialFourMomentum().e(); // e at vertex
	    double pges = sqrt(px*px + py*py + pz*pz); // in GeV
	    
	    //	      double xangle = acos(px/pges); // angle between photon trajectory and x-axis
	    //	      double yangle = acos(py/pges); // angle between photon trajectory and y-axis
	    double phi = atan2(py,px); // tan(phi) = px/py
	    double theta = acos(pz/pges);// angle between photon trajectory and z-axis
      //      cout << "theta [deg]: " << theta * 180./TMath::Pi() << endl;
      //      cout << "phi [deg]  : " << phi * 180./TMath::Pi() << endl;
	    
	    //	    double distance_to_zaxis_at_zdd = tan(theta) * (335.-z) + sqrt( x*x + y*y );
	    double distance_to_zaxis_at_zdd = tan(theta) * 335.;
	    //	    double x_at_zdd = cos(phi) * distance_to_zaxis_at_zdd + x; // x coordinate of photon at z = +-333cm (zdd position)
	    //	    double y_at_zdd = sin(phi) * distance_to_zaxis_at_zdd + y; // y coordinate of photon at z = +-333cm (zdd position)
	    double x_at_zdd = cos(phi) * distance_to_zaxis_at_zdd; // x coordinate of photon at z = +-333cm (zdd position)                                              
            double y_at_zdd = sin(phi) * distance_to_zaxis_at_zdd; // y coordinate of photon at z = +-333cm (zdd position) 
	    double x_center = 0.;
	    double y_center_up = 2.;
	    double y_center_down = -2.;

	    double distance_from_center = 0.;
	    if(y_at_zdd >= 0) distance_from_center = sqrt(pow(y_at_zdd - y_center_up,2.)+pow(x_at_zdd,2.) );
	    else
	      {
		distance_from_center = sqrt(pow(y_at_zdd - y_center_down,2.)+pow(x_at_zdd,2.) );
	      }
	    //cout << "x at zdd : " << x_at_zdd << endl;
	    //cout << "y at zdd : " << y_at_zdd << endl;
	    
	    isr_energy = e; 
	    isr_x = x;
	    isr_y = y;
	    isr_z = z;
	    isr_px =px;
	    isr_py =py;
	    isr_pz =pz; 
	    hit_x = x_at_zdd;
	    hit_y = y_at_zdd;
	    dist_center = distance_from_center;

			m_XZddGammaISRMCTruth = MCXCalcatZDD[MCntracks];
			m_YZddGammaISRMCTruth = MCYCalcatZDD[MCntracks];

			TLorentzVector lv(0,0,0,0);
			lv.SetPxPyPzE(px,py,pz,e);			
			m_T4PiGamISRGamISRMCTruth.Fill(&lv);
			
	    if(debug)
	      {
		cout << "analysis_cZDD.cxx : " << endl <<
		  "isr_energy = " << isr_energy << endl <<
		  "isr_x      = " << isr_x << endl <<
		  "isr_y      = " << isr_y << endl <<
		  "isr_z      = " << isr_z << endl <<
		  "isr_px     = " << isr_px << endl <<
		  "isr_py     = " << isr_py << endl <<
		  "isr_pz     = " << isr_pz << endl;
	      }
	   
		 
	  }// end if(*iter_mc)
		
		//For particle gun   
	   if(m_pGunMCfound==false){
		 	if(primPart==1) {		
				MCpGunTrackID=MCntracks;
				m_pGunMCfound=true;
				}
		 	}
		 
		//For Bhabha
		/*
		 if(m_MCIsBhabha==false){
		 			if(m_MCHasPrimElectron==false && primPart==1 && (*iter_mc)->particleProperty()==11){
					m_MCHasPrimElectron=true;
					MCBhabhaElecTrackID=MCntracks;
					}
					if(m_MCHasPrimPositron=false && primPart==1 && (*iter_mc)->particleProperty()==-11){
					m_MCHasPrimPositron=true;
					MCBhabhaPositronTrackID=MCntracks;
					}
					if(m_MCHasPrimElectron==true && m_MCHasPrimPositron==true) m_MCIsBhabha=true;
		 		}
			*/
			if(primPart==1 && (*iter_mc)->particleProperty()==11){					
					MCBhabhaElecTrackID=MCntracks;
					m_MCNPrimElectron++;
					}
			if(primPart==1 && (*iter_mc)->particleProperty()==-11){
					MCBhabhaPositronTrackID=MCntracks;
					m_MCNPrimPositron++;
					}
			if(primPart==1 && (*iter_mc)->particleProperty()==22){										
					MCBhabhaGamTrackID[m_MCNPrimGammas]=MCntracks;
					m_MCNPrimGammas++;
					}		
			MCntracks++;	
      }// end for(*iter_mc)
    }//else monte carlo collection  

  /*if(zdd_energy > 0)*/ m_myTree->Fill();
  //  else{m_myTree->Fill();}
  Vint iGood, ipip, ipim;
  iGood.clear();
  ipip.clear();
  ipim.clear();
  Vp4 ppip, ppim;
  ppip.clear();
  ppim.clear();
		
	m_AllMCTruthTree->Fill();
/*	
	EvtRecTrackIterator itBegin      = evtRecTrkCol->begin();
EvtRecTrackIterator itEndCharged = itBegin + evtRecEvent->totalCharged();
EvtRecTrackIterator itEndNeutral = itBegin + evtRecEvent->totalTracks();
    
    
int	iNCharged= evtRecEvent->totalCharged();
int	iNNeutral= evtRecEvent->totalNeutral();
cout << 	iNCharged << " "  << iNNeutral << endl;
*/

	AnalysisParticleGun();

	AnalysisBhabha();
	
	Analysis4PiGamISRTagged();

	/*
	SmartDataPtr<Event::ZddMcHitCol> zddMcHitCol(eventSvc(), "/Event/MC/ZddMcHitCol"); 
  if(!zddMcHitCol)
    { 
      G4cout<<"Could not retrieve zddMcHitCol"<<G4endl;
      return StatusCode::FAILURE;
    }	
  else
    {
      Event::ZddMcHitCol::iterator iMcHitCol = zddMcHitCol->begin();
      for (; iMcHitCol != zddMcHitCol->end(); iMcHitCol++)
      {
			int MCntracks = std::distance(zddMcHitCol->begin(),iMcHitCol);
			int trackID = (*iMcHitCol)->getTrackID();
			int pid = (*iMcHitCol)->getPDGCode();
			double preposX=(*iMcHitCol)->getPrePositionX();
			printf("%d %d %d %d %f \n", event, MCntracks, trackID,pid,preposX);
			
			HitColtrackID=trackID;
			HitColPID=pid;
			//m_ZDDMcHitColTree->Fill();
			}
		}
		*/
		
//   int nCharge = 0;
// 
//   Hep3Vector xorigin(0,0,0);
//   
//   //if (m_reader.isRunNumberValid(runNo)) {
//    IVertexDbSvc*  vtxsvc;
//   Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);
//   if(vtxsvc->isVertexValid()){
//     cout << "analysis_cZDD.cxx: VertexValid" << endl;
//   double* dbv = vtxsvc->PrimaryVertex(); 
//   double*  vv = vtxsvc->SigmaPrimaryVertex();  
// //    HepVector dbv = m_reader.PrimaryVertex(runNo);
// //    HepVector vv = m_reader.SigmaPrimaryVertex(runNo);
//     xorigin.setX(dbv[0]);
//     xorigin.setY(dbv[1]);
//     xorigin.setZ(dbv[2]);
//   }
// 
//   for(int i = 0; i < evtRecEvent->totalCharged(); i++){
//     EvtRecTrackIterator itTrk=evtRecTrkCol->begin() + i;
//     if(!(*itTrk)->isMdcTrackValid()) continue;
//     RecMdcTrack *mdcTrk = (*itTrk)->mdcTrack();
//     double pch=mdcTrk->p();
//     double x0=mdcTrk->x();
//     double y0=mdcTrk->y();
//     double z0=mdcTrk->z();
//     double phi0=mdcTrk->helix(1);
//     double xv=xorigin.x();
//     double yv=xorigin.y();
//     double Rxy=(x0-xv)*cos(phi0)+(y0-yv)*sin(phi0);
//     //    m_vx0 = x0;
//     //    m_vy0 = y0;
//     //    m_vz0 = z0;
//     //    m_vr0 = Rxy;
// 
//     HepVector a = mdcTrk->helix();
//     HepSymMatrix Ea = mdcTrk->err();
//     HepPoint3D point0(0.,0.,0.);   // the initial point for MDC recosntruction
//     HepPoint3D IP(xorigin[0],xorigin[1],xorigin[2]); 
//     VFHelix helixip(point0,a,Ea); 
//     helixip.pivot(IP);
//     HepVector vecipa = helixip.a();
//     double  Rvxy0=fabs(vecipa[0]);  //the nearest distance to IP in xy plane
//     double  Rvz0=vecipa[3];         //the nearest distance to IP in z direction
//     double  Rvphi0=vecipa[1];
//     //    m_rvxy0=Rvxy0;
//     //    m_rvz0=Rvz0;
//     //    m_rvphi0=Rvphi0;
// 
//     //    m_tuple1->write();
//     cout << "analysis_cZDD.cxx: Fill m_tuple1: vxyz" << endl;
// //    if(fabs(z0) >= m_vz0cut) continue;
// //    if(fabs(Rxy) >= m_vr0cut) continue;
//     
//     if(fabs(Rvz0) >= 10.0) continue;
//     if(fabs(Rvxy0) >= 1.0) continue;
//     
//     iGood.push_back(i);
//     nCharge += mdcTrk->charge();
//   }
//   
//   //
//   // Finish Good Charged Track Selection
//   //
//   int nGood = iGood.size();
//   log << MSG::DEBUG << "ngood, totcharge = " << nGood << " , " << nCharge << endreq;
//   if((nGood != 2)||(nCharge!=0)){
//     return StatusCode::SUCCESS;
//   }
//   Ncut1++;
// 
//   Vint iGam;
//   iGam.clear();
//   for(int i = evtRecEvent->totalCharged(); i< evtRecEvent->totalTracks(); i++) {
//     cout << "analysis_cZDD.cxx:Neutral Particle loop" << endl;
// 
//     EvtRecTrackIterator itTrk=evtRecTrkCol->begin() + i;
//     if(!(*itTrk)->isEmcShowerValid()) continue;
//     RecEmcShower *emcTrk = (*itTrk)->emcShower();
//     Hep3Vector emcpos(emcTrk->x(), emcTrk->y(), emcTrk->z());
//     // find the nearest charged track
//     double dthe = 200.;
//     double dphi = 200.;
//     double dang = 200.; 
//     for(int j = 0; j < evtRecEvent->totalCharged(); j++) {
//       EvtRecTrackIterator jtTrk = evtRecTrkCol->begin() + j;
//       if(!(*jtTrk)->isExtTrackValid()) continue;
//       RecExtTrack *extTrk = (*jtTrk)->extTrack();
//       if(extTrk->emcVolumeNumber() == -1) continue;
//       Hep3Vector extpos = extTrk->emcPosition();
//       //      double ctht = extpos.cosTheta(emcpos);
//       double angd = extpos.angle(emcpos);
//       double thed = extpos.theta() - emcpos.theta();
//       double phid = extpos.deltaPhi(emcpos);
//       thed = fmod(thed+CLHEP::twopi+CLHEP::twopi+pi, CLHEP::twopi) - CLHEP::pi;
//       phid = fmod(phid+CLHEP::twopi+CLHEP::twopi+pi, CLHEP::twopi) - CLHEP::pi;
//       if(angd < dang){
//         dang = angd;
//         dthe = thed;
//         dphi = phid;
//       }
//     }
//     if(dang>=200) continue;
//     double eraw = emcTrk->energy();
//     dthe = dthe * 180 / (CLHEP::pi);
//     dphi = dphi * 180 / (CLHEP::pi);
//     dang = dang * 180 / (CLHEP::pi);
//     //    m_dthe = dthe;
//     //    m_dphi = dphi;
//     //    m_dang = dang;
//     //    m_eraw = eraw;
//     //    m_tuple2->write();
//     cout << "analysis_cZDD.cxx: Fill m_tuple2: photon" << endl;
// 
//     if(eraw < m_energyThreshold) continue;
// //    if((fabs(dthe) < m_gammaThetaCut) && (fabs(dphi)<m_gammaPhiCut) ) continue;
//     if(fabs(dang) < m_gammaAngleCut) continue;
//     //
//     // good photon cut will be set here
//     //
//     iGam.push_back(i);
//   }
//   
//   //
//   // Finish Good Photon Selection
//   //
//   int nGam = iGam.size();
// 
//   log << MSG::DEBUG << "num Good Photon " << nGam  << " , " <<evtRecEvent->totalNeutral()<<endreq;
//   if(nGam<2){
//     return StatusCode::SUCCESS;
//   }
//   Ncut2++;
// 
// 
// 
//   //
//   //
//   // check dedx infomation
//   //
//   //
//   
//   if(m_checkDedx == 1) {
//     for(int i = 0; i < nGood; i++) {
//       cout << "analysis_cZDD.cxx: Check dE/dx Information" << endl;
// 
//       EvtRecTrackIterator  itTrk = evtRecTrkCol->begin() + iGood[i];
//       if(!(*itTrk)->isMdcTrackValid()) continue;
//       if(!(*itTrk)->isMdcDedxValid())continue;
//       RecMdcTrack* mdcTrk = (*itTrk)->mdcTrack();
//       RecMdcDedx* dedxTrk = (*itTrk)->mdcDedx();
//       //      m_ptrk = mdcTrk->p();
// 
//       //      m_chie = dedxTrk->chiE();
//       //      m_chimu = dedxTrk->chiMu();
//       //      m_chipi = dedxTrk->chiPi();
//       // m_chik = dedxTrk->chiK();
//       // m_chip = dedxTrk->chiP();
//       // m_ghit = dedxTrk->numGoodHits();
//       // m_thit = dedxTrk->numTotalHits();
//       // m_probPH = dedxTrk->probPH();
//       // m_normPH = dedxTrk->normPH();
//       //      m_tuple7->write();
//       cout << "analysis_cZDD.cxx: Fill m_tuple7: dedx" << endl;
// 
//     }
//   }
// 
//   //
//   // check TOF infomation
//   //
// 
// 
//   if(m_checkTof == 1) {
//     for(int i = 0; i < nGood; i++) {
//       EvtRecTrackIterator  itTrk = evtRecTrkCol->begin() + iGood[i];
//       if(!(*itTrk)->isMdcTrackValid()) continue;
//       if(!(*itTrk)->isTofTrackValid()) continue;
// 
//       RecMdcTrack * mdcTrk = (*itTrk)->mdcTrack();
//       SmartRefVector<RecTofTrack> tofTrkCol = (*itTrk)->tofTrack();
// 
//       double ptrk = mdcTrk->p();
// 
//       SmartRefVector<RecTofTrack>::iterator iter_tof = tofTrkCol.begin();
//       for(;iter_tof != tofTrkCol.end(); iter_tof++ ) { 
// 	cout << "analysis_cZDD.cxx: TofTrackCol" << endl;
// 
//         TofHitStatus *status = new TofHitStatus; 
//         status->setStatus((*iter_tof)->status());
//         if(!(status->is_barrel())){//endcap
//           if( !(status->is_counter()) ) continue; // ? 
//           if( status->layer()!=0 ) continue;//layer1
//           double path=(*iter_tof)->path(); // ? 
//           double tof  = (*iter_tof)->tof();
//           double ph   = (*iter_tof)->ph();
//           double rhit = (*iter_tof)->zrhit();
//           double qual = 0.0 + (*iter_tof)->quality();
//           double cntr = 0.0 + (*iter_tof)->tofID();
//           double texp[5];
//           for(int j = 0; j < 5; j++) {
//             double gb = ptrk/xmass[j];
//             double beta = gb/sqrt(1+gb*gb);
//             texp[j] = 10 * path /beta/velc;
//           }
//           // m_cntr_etof  = cntr;
//           // m_ptot_etof = ptrk;
//           // m_ph_etof   = ph;
//           // m_rhit_etof  = rhit;
//           // m_qual_etof  = qual;
//           // m_te_etof    = tof - texp[0];
//           // m_tmu_etof   = tof - texp[1];
//           // m_tpi_etof   = tof - texp[2];
//           // m_tk_etof    = tof - texp[3];
//           // m_tp_etof    = tof - texp[4];
// 	  //  m_tuple8->write();
// 	  cout << "analysis_cZDD.cxx: Fill m_tuple8: tofe" << endl;
// 
//         }
//         else {//barrel
//           if( !(status->is_counter()) ) continue; // ? 
//           if(status->layer()==1){ //layer1
//             double path=(*iter_tof)->path(); // ? 
//             double tof  = (*iter_tof)->tof();
//             double ph   = (*iter_tof)->ph();
//             double rhit = (*iter_tof)->zrhit();
//             double qual = 0.0 + (*iter_tof)->quality();
//             double cntr = 0.0 + (*iter_tof)->tofID();
//             double texp[5];
//             for(int j = 0; j < 5; j++) {
//               double gb = ptrk/xmass[j];
//               double beta = gb/sqrt(1+gb*gb);
//               texp[j] = 10 * path /beta/velc;
//             }
//  
//             // m_cntr_btof1  = cntr;
//             // m_ptot_btof1 = ptrk;
//             // m_ph_btof1   = ph;
//             // m_zhit_btof1  = rhit;
//             // m_qual_btof1  = qual;
//             // m_te_btof1    = tof - texp[0];
//             // m_tmu_btof1   = tof - texp[1];
//             // m_tpi_btof1   = tof - texp[2];
//             // m_tk_btof1    = tof - texp[3];
//             // m_tp_btof1    = tof - texp[4];
// 	    //            m_tuple9->write();
// 	    cout << "analysis_cZDD.cxx: Fill m_tuple9: tof1" << endl;
// 
//           }
// 
//           if(status->layer()==2){//layer2
//             double path=(*iter_tof)->path(); // ? 
//             double tof  = (*iter_tof)->tof();
//             double ph   = (*iter_tof)->ph();
//             double rhit = (*iter_tof)->zrhit();
//             double qual = 0.0 + (*iter_tof)->quality();
//             double cntr = 0.0 + (*iter_tof)->tofID();
//             double texp[5];
//             for(int j = 0; j < 5; j++) {
//               double gb = ptrk/xmass[j];
//               double beta = gb/sqrt(1+gb*gb);
//               texp[j] = 10 * path /beta/velc;
//             }
//  
//             // m_cntr_btof2  = cntr;
//             // m_ptot_btof2 = ptrk;
//             // m_ph_btof2   = ph;
//             // m_zhit_btof2  = rhit;
//             // m_qual_btof2  = qual;
//             // m_te_btof2    = tof - texp[0];
//             // m_tmu_btof2   = tof - texp[1];
//             // m_tpi_btof2   = tof - texp[2];
//             // m_tk_btof2    = tof - texp[3];
//             // m_tp_btof2    = tof - texp[4];
// 	    //            m_tuple10->write();
// 	    cout << "analysis_cZDD.cxx: Fill m_tuple10: tof2" << endl;
// 
//           } 
//         }
// 
//         delete status; 
//       } 
//     } // loop all charged track
//   }  // check tof
// 
// 
//   //
//   // Assign 4-momentum to each photon
//   // 
// 
//   Vp4 pGam;
//   pGam.clear();
//   for(int i = 0; i < nGam; i++) {
//     EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + iGam[i]; 
//     RecEmcShower* emcTrk = (*itTrk)->emcShower();
//     double eraw = emcTrk->energy();
//     double phi = emcTrk->phi();
//     double the = emcTrk->theta();
//     HepLorentzVector ptrk;
//     ptrk.setPx(eraw*sin(the)*cos(phi));
//     ptrk.setPy(eraw*sin(the)*sin(phi));
//     ptrk.setPz(eraw*cos(the));
//     ptrk.setE(eraw);
// 
// //    ptrk = ptrk.boost(-0.011,0,0);// boost to cms
// 
//     pGam.push_back(ptrk);
//   }
//   cout<<"before pid"<<endl;
//   //
//   // Assign 4-momentum to each charged track
//   //
//   ParticleID *pid = ParticleID::instance();
//   for(int i = 0; i < nGood; i++) {
//     EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + iGood[i]; 
//     //    if(pid) delete pid;
//     pid->init();
//     pid->setMethod(pid->methodProbability());
// //    pid->setMethod(pid->methodLikelihood());  //for Likelihood Method  
// 
//     pid->setChiMinCut(4);
//     pid->setRecTrack(*itTrk);
//     pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTofE()); // use PID sub-system
//     pid->identify(pid->onlyPion() | pid->onlyKaon());    // seperater Pion/Kaon
//     //    pid->identify(pid->onlyPion());
//     //  pid->identify(pid->onlyKaon());
//     pid->calculate();
//     if(!(pid->IsPidInfoValid())) continue;
//     RecMdcTrack* mdcTrk = (*itTrk)->mdcTrack();
//     // m_ptrk_pid = mdcTrk->p();
//     // m_cost_pid = cos(mdcTrk->theta());
//     // m_dedx_pid = pid->chiDedx(2);
//     // m_tof1_pid = pid->chiTof1(2);
//     // m_tof2_pid = pid->chiTof2(2);
//     // m_prob_pid = pid->probPion();
//     //    // m_tuple11->write();
//     cout << "analysis_cZDD.cxx: Fill m_tuple11: pid" << endl;
// 
// 
// //  if(pid->probPion() < 0.001 || (pid->probPion() < pid->probKaon())) continue;
//     if(pid->probPion() < 0.001) continue;
// //    if(pid->pdf(2)<pid->pdf(3)) continue; //  for Likelihood Method(0=electron 1=muon 2=pion 3=kaon 4=proton) 
// 
//     RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();//After ParticleID, use RecMdcKalTrack substitute RecMdcTrack
//     RecMdcKalTrack::setPidType  (RecMdcKalTrack::pion);//PID can set to electron, muon, pion, kaon and proton;The default setting is pion
// 
//     if(mdcKalTrk->charge() >0) {
//       ipip.push_back(iGood[i]);
//       HepLorentzVector ptrk;
//       ptrk.setPx(mdcKalTrk->px());
//       ptrk.setPy(mdcKalTrk->py());
//       ptrk.setPz(mdcKalTrk->pz());
//       double p3 = ptrk.mag();
//       ptrk.setE(sqrt(p3*p3+mpi*mpi));
// 
// //      ptrk = ptrk.boost(-0.011,0,0);//boost to cms
// 
//       ppip.push_back(ptrk);
//     } else {
//       ipim.push_back(iGood[i]);
//       HepLorentzVector ptrk;
//       ptrk.setPx(mdcKalTrk->px());
//       ptrk.setPy(mdcKalTrk->py());
//       ptrk.setPz(mdcKalTrk->pz());
//       double p3 = ptrk.mag();
//       ptrk.setE(sqrt(p3*p3+mpi*mpi));
// 
// //      ptrk = ptrk.boost(-0.011,0,0);//boost to cms
// 
//       ppim.push_back(ptrk);
//     }
//   }
// 
// /*
//   for(int i = 0; i < nGood; i++) {//for rhopi without PID
//     EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + iGood[i];
//     RecMdcTrack* mdcTrk = (*itTrk)->mdcTrack(); 
//     if(mdcTrk->charge() >0) {
//       ipip.push_back(iGood[i]);
//       HepLorentzVector ptrk;
//       ptrk.setPx(mdcTrk->px());
//       ptrk.setPy(mdcTrk->py());
//       ptrk.setPz(mdcTrk->pz());
//       double p3 = ptrk.mag();
//       ptrk.setE(sqrt(p3*p3+mpi*mpi));
//       ppip.push_back(ptrk);
//     } else {
//       ipim.push_back(iGood[i]);
//       HepLorentzVector ptrk;
//       ptrk.setPx(mdcTrk->px());
//       ptrk.setPy(mdcTrk->py());
//       ptrk.setPz(mdcTrk->pz());
//       double p3 = ptrk.mag();
//       ptrk.setE(sqrt(p3*p3+mpi*mpi));
//       ppim.push_back(ptrk);
//     }
//   }// without PID
// */
// 
//   int npip = ipip.size();
//   int npim = ipim.size();
//   if(npip*npim != 1) return SUCCESS;
// 
//   Ncut3++;
// 
// 
//   //
//   // Loop each gamma pair, check ppi0 and pTot 
//   //
// 
//   HepLorentzVector pTot;
//   for(int i = 0; i < nGam - 1; i++){
//     for(int j = i+1; j < nGam; j++) {
//       HepLorentzVector p2g = pGam[i] + pGam[j];
//       pTot = ppip[0] + ppim[0];
//       pTot += p2g;
//       // m_m2gg = p2g.m();
//       // m_etot = pTot.e();
//       //      m_tuple3 -> write();
//       cout << "analysis_cZDD.cxx: Fill m_tuple3: etot" << endl;
// 
//     }
//   }
//   
//   
//   RecMdcKalTrack *pipTrk = (*(evtRecTrkCol->begin()+ipip[0]))->mdcKalTrack();
//   RecMdcKalTrack *pimTrk = (*(evtRecTrkCol->begin()+ipim[0]))->mdcKalTrack();
// 
//   WTrackParameter wvpipTrk, wvpimTrk;
//   wvpipTrk = WTrackParameter(mpi, pipTrk->getZHelix(), pipTrk->getZError());
//   wvpimTrk = WTrackParameter(mpi, pimTrk->getZHelix(), pimTrk->getZError());
// 
// /* Default is pion, for other particles:
//   wvppTrk = WTrackParameter(mp, pipTrk->getZHelixP(), pipTrk->getZErrorP());//proton
//   wvmupTrk = WTrackParameter(mmu, pipTrk->getZHelixMu(), pipTrk->getZErrorMu());//muon
//   wvepTrk = WTrackParameter(me, pipTrk->getZHelixE(), pipTrk->getZErrorE());//electron
//   wvkpTrk = WTrackParameter(mk, pipTrk->getZHelixK(), pipTrk->getZErrorK());//kaon
// */
//   //
//   //    Test vertex fit
//   //
// 
//   HepPoint3D vx(0., 0., 0.);
//   HepSymMatrix Evx(3, 0);
//   double bx = 1E+6;
//   double by = 1E+6;
//   double bz = 1E+6;
//   Evx[0][0] = bx*bx;
//   Evx[1][1] = by*by;
//   Evx[2][2] = bz*bz;
// 
//   VertexParameter vxpar;
//   vxpar.setVx(vx);
//   vxpar.setEvx(Evx);
//   
//   VertexFit* vtxfit = VertexFit::instance();
//   vtxfit->init();
//   vtxfit->AddTrack(0,  wvpipTrk);
//   vtxfit->AddTrack(1,  wvpimTrk);
//   vtxfit->AddVertex(0, vxpar,0, 1);
//   if(!vtxfit->Fit(0)) return SUCCESS;
//   vtxfit->Swim(0);
//   
//   WTrackParameter wpip = vtxfit->wtrk(0);
//   WTrackParameter wpim = vtxfit->wtrk(1);
// 
//   //KinematicFit * kmfit = KinematicFit::instance();
//   KalmanKinematicFit * kmfit = KalmanKinematicFit::instance();
// 
//   //
//   //  Apply Kinematic 4C fit
//   // 
//   cout<<"before 4c"<<endl;
//   if(m_test4C==1) {
// //    double ecms = 3.097;
//     HepLorentzVector ecms(0.034,0,0,3.097);
// 
//     double chisq = 9999.;
//     int ig1 = -1;
//     int ig2 = -1;
//     for(int i = 0; i < nGam-1; i++) {
//       RecEmcShower *g1Trk = (*(evtRecTrkCol->begin()+iGam[i]))->emcShower();
//       for(int j = i+1; j < nGam; j++) {
// 	RecEmcShower *g2Trk = (*(evtRecTrkCol->begin()+iGam[j]))->emcShower();
// 	kmfit->init();
// 	kmfit->AddTrack(0, wpip);
// 	kmfit->AddTrack(1, wpim);
// 	kmfit->AddTrack(2, 0.0, g1Trk);
// 	kmfit->AddTrack(3, 0.0, g2Trk);
// 	kmfit->AddFourMomentum(0, ecms);
// 	bool oksq = kmfit->Fit();
// 	if(oksq) {
// 	  double chi2 = kmfit->chisq();
// 	  if(chi2 < chisq) {
// 	    chisq = chi2;
// 	    ig1 = iGam[i];
// 	    ig2 = iGam[j];
// 	  }
// 	}
//       }
//     }
//     
//     if(chisq < 200) { 
// 
//       RecEmcShower *g1Trk = (*(evtRecTrkCol->begin()+ig1))->emcShower();
//       RecEmcShower *g2Trk = (*(evtRecTrkCol->begin()+ig2))->emcShower();
//       kmfit->init();
//       kmfit->AddTrack(0, wpip);
//       kmfit->AddTrack(1, wpim);
//       kmfit->AddTrack(2, 0.0, g1Trk);
//       kmfit->AddTrack(3, 0.0, g2Trk);
//       kmfit->AddFourMomentum(0, ecms);
//       bool oksq = kmfit->Fit();
//       if(oksq) {
// 	HepLorentzVector ppi0 = kmfit->pfit(2) + kmfit->pfit(3);
// 	// m_mpi0 = ppi0.m();
// 	// m_chi1 = kmfit->chisq();
// 	//	m_tuple4->write();
//         Ncut4++;
//       }
//     }
//   }
//   
//   //
//   //  Apply Kinematic 5C Fit
//   //
// 
//   // find the best combination over all possible pi+ pi- gamma gamma pair
//   if(m_test5C==1) {
// //    double ecms = 3.097;
//     HepLorentzVector ecms(0.034,0,0,3.097);
//     double chisq = 9999.;
//     int ig1 = -1;
//     int ig2 = -1;
//     for(int i = 0; i < nGam-1; i++) {
//       RecEmcShower *g1Trk = (*(evtRecTrkCol->begin()+iGam[i]))->emcShower();
//       for(int j = i+1; j < nGam; j++) {
// 	RecEmcShower *g2Trk = (*(evtRecTrkCol->begin()+iGam[j]))->emcShower();
// 	kmfit->init();
// 	kmfit->AddTrack(0, wpip);
// 	kmfit->AddTrack(1, wpim);
// 	kmfit->AddTrack(2, 0.0, g1Trk);
// 	kmfit->AddTrack(3, 0.0, g2Trk);
// 	kmfit->AddResonance(0, 0.135, 2, 3);
// 	kmfit->AddFourMomentum(1, ecms);
// 	if(!kmfit->Fit(0)) continue;
// 	if(!kmfit->Fit(1)) continue;
// 	bool oksq = kmfit->Fit();
// 	if(oksq) {
// 	  double chi2 = kmfit->chisq();
// 	  if(chi2 < chisq) {
// 	    chisq = chi2;
// 	    ig1 = iGam[i];
// 	    ig2 = iGam[j];
// 	  }
// 	}
//       }
//     }
//   
// 
//     log << MSG::INFO << " chisq = " << chisq <<endreq;
// 
//     if(chisq < 200) {
//       RecEmcShower* g1Trk = (*(evtRecTrkCol->begin()+ig1))->emcShower();
//       RecEmcShower* g2Trk = (*(evtRecTrkCol->begin()+ig2))->emcShower();
// 
//       kmfit->init();
//       kmfit->AddTrack(0, wpip);
//       kmfit->AddTrack(1, wpim);
//       kmfit->AddTrack(2, 0.0, g1Trk);
//       kmfit->AddTrack(3, 0.0, g2Trk);
//       kmfit->AddResonance(0, 0.135, 2, 3);
//       kmfit->AddFourMomentum(1, ecms);
//       bool oksq = kmfit->Fit();
//       if(oksq){
// 	HepLorentzVector ppi0 = kmfit->pfit(2) + kmfit->pfit(3);
// 	HepLorentzVector prho0 = kmfit->pfit(0) + kmfit->pfit(1);
// 	HepLorentzVector prhop = ppi0 + kmfit->pfit(0);
// 	HepLorentzVector prhom = ppi0 + kmfit->pfit(1);
// 	
// 	// m_chi2  = kmfit->chisq();
// 	// m_mrh0 = prho0.m();
// 	// m_mrhp = prhop.m();
// 	// m_mrhm = prhom.m();
// 	double eg1 = (kmfit->pfit(2)).e();
// 	double eg2 = (kmfit->pfit(3)).e();
// 	double fcos = abs(eg1-eg2)/ppi0.rho();
// 	//	m_tuple5->write();
//         Ncut5++;
// 	// 
// 	//  Measure the photon detection efficiences via
// 	//          J/psi -> rho0 pi0
// 	//
// 	if(fabs(prho0.m()-0.770)<0.150) {  
// 	  if(fabs(fcos)<0.99) {
// 	    // m_fcos = (eg1-eg2)/ppi0.rho();
// 	    // m_elow =  (eg1 < eg2) ? eg1 : eg2;
// 	    //  m_tuple6->write();
//             Ncut6++;
// 	  }
// 	} // rho0 cut
//       }  //oksq
//     } 
//   }
  return StatusCode::SUCCESS;
}


//---------------------------------------------------------------------------------------------------------- 
StatusCode analysis_cZDD::finalize() {
	/*
  m_myFile->cd();
  m_myTree->Write();
  m_leoTree->Write();
	m_AllMCTruthTree->Write();
	m_particlegunTree->Write();
	//m_ZDDMcHitColTree->Write();
	m_myFile->Write();
  m_myFile->Close();
  //  delete m_myTree;
  //  delete m_myFile;
	*/
	
	m_myFile->cd();
	m_myFile->Write();
  m_myFile->Close();
	
  cout<<"total number:         "<<Ncut0<<endl;
  cout<<"nGood==2, nCharge==0: "<<Ncut1<<endl;
  cout<<"nGam>=2:              "<<Ncut2<<endl;
  cout<<"Pass Pid:             "<<Ncut3<<endl;
  cout<<"Pass 4C:              "<<Ncut4<<endl;
  cout<<"Pass 5C:              "<<Ncut5<<endl;
  cout<<"J/psi->rho0 pi0:      "<<Ncut6<<endl;
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in finalize()" << endmsg;
  return StatusCode::SUCCESS;
}

//---------------------------------------------------------------------------------------------------------- 
void analysis_cZDD::DefineZDDCrystalsEdges()
{
m_XEdgesZDDRight[0]=0.;m_XEdgesZDDRight[1]=1.;m_XEdgesZDDRight[2]=2.;m_XEdgesZDDRight[3]=3.;
m_XEdgesZDDRight[4]=4.;

m_YEdgesZDDRight[0]=-3.5;m_YEdgesZDDRight[1]=-2.5;m_YEdgesZDDRight[2]=-1.5;m_YEdgesZDDRight[3]=-0.5;
m_YEdgesZDDRight[4]=0.5;m_YEdgesZDDRight[5]=1.5;m_YEdgesZDDRight[6]=2.5;m_YEdgesZDDRight[7]=3.5;
}

//---------------------------------------------------------------------------------------------------------- 
vector<double> analysis_cZDD::GetCrystalCenter(int crystalNo, int partID)
{
vector<double> XYCenter;
XYCenter.resize(2);

	 switch( crystalNo )
	{
			case 101:
	  		XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
				XYCenter[1]=(m_YEdgesZDDRight[4]+m_YEdgesZDDRight[5])/2;
	  		break;
        case 102:
          XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[4]+m_YEdgesZDDRight[5])/2;       
          break;
        case 103:
	  			XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[4]+m_YEdgesZDDRight[5])/2; 
          break;
        case 104:
	  			XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[4]+m_YEdgesZDDRight[5])/2; 
          break;
			case 111:
	  			XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
					XYCenter[1]=(m_YEdgesZDDRight[5]+m_YEdgesZDDRight[6])/2;
          break;
        case 112:
	  			XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[5]+m_YEdgesZDDRight[6])/2;
          break;
        case 113:
	  			XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[5]+m_YEdgesZDDRight[6])/2;
          break;
        case 114:
	  			XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[5]+m_YEdgesZDDRight[6])/2;
          break;
        case 121:
	  			XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
					XYCenter[1]=(m_YEdgesZDDRight[6]+m_YEdgesZDDRight[7])/2;
          break;
        case 122:
	  			XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[6]+m_YEdgesZDDRight[7])/2;
          break;
        case 123:
	  			XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[6]+m_YEdgesZDDRight[7])/2;
	  			break;
        case 124:
	  			XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[6]+m_YEdgesZDDRight[7])/2;
          break;
        case 201:
	  			XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
					XYCenter[1]=(m_YEdgesZDDRight[2]+m_YEdgesZDDRight[3])/2;
          break;
        case 202:
	  			XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[2]+m_YEdgesZDDRight[3])/2;
          break;
        case 203:
	  			XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[2]+m_YEdgesZDDRight[3])/2;
          break;
        case 204:
	  			XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[2]+m_YEdgesZDDRight[3])/2;
          break;
        case 211:
	  			XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
					XYCenter[1]=(m_YEdgesZDDRight[1]+m_YEdgesZDDRight[2])/2;
          break;
        case 212:
	  			XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[1]+m_YEdgesZDDRight[2])/2;
          break;
        case 213:
	  			XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[1]+m_YEdgesZDDRight[2])/2;
          break;
        case 214:
	  			XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[1]+m_YEdgesZDDRight[2])/2;
          break;
        case 221:
          XYCenter[0]=(m_XEdgesZDDRight[0]+m_XEdgesZDDRight[1])/2;
					XYCenter[1]=(m_YEdgesZDDRight[0]+m_YEdgesZDDRight[1])/2;
          break;
        case 222:
          XYCenter[0]=(m_XEdgesZDDRight[1]+m_XEdgesZDDRight[2])/2;
					XYCenter[1]=(m_YEdgesZDDRight[0]+m_YEdgesZDDRight[1])/2;
          break;
        case 223:
          XYCenter[0]=(m_XEdgesZDDRight[2]+m_XEdgesZDDRight[3])/2;
					XYCenter[1]=(m_YEdgesZDDRight[0]+m_YEdgesZDDRight[1])/2;
          break;
        case 224:
          XYCenter[0]=(m_XEdgesZDDRight[3]+m_XEdgesZDDRight[4])/2;
					XYCenter[1]=(m_YEdgesZDDRight[0]+m_YEdgesZDDRight[1])/2;
          break;			
		}
	
//printf("%d %d %f %f \n",crystalNo, partID, XYCenter[0],XYCenter[1]);
//if(XYCenter[0]==0 && XYCenter[1]==0) {cout << "WARNING : (X,Y) ZDD = 0" << endl;sleep(5);}
return XYCenter;
}

//---------------------------------------------------------------------------------------------------------- 
void analysis_cZDD::AnalysisParticleGun()
{
/*
//All MC Truth
//Find particle from particle gun
for(int i=0;i<MCntracks;i++){
	if(MCprimPart[i]==1) {		
		MCpGunTrackID=i;
		break;
		}
	}
*/

MCpGunpartProp=MCpartProp[MCpGunTrackID];
MCpGuniniPosX=MCiniPosX[MCpGunTrackID];
MCpGuniniPosY=MCiniPosY[MCpGunTrackID];
MCpGuniniPosZ=MCiniPosZ[MCpGunTrackID];
MCpGunfinPosX=MCfinPosX[MCpGunTrackID];
MCpGunfinPosY=MCfinPosY[MCpGunTrackID];
MCpGunfinPosZ=MCfinPosZ[MCpGunTrackID];
MCpGunMomX=MCiniMomX[MCpGunTrackID];
MCpGunMomY=MCiniMomY[MCpGunTrackID];
MCpGunMomZ=MCiniMomZ[MCpGunTrackID];
MCpGunMomRho=MCiniMomRho[MCpGunTrackID];
MCpGunMomTheta=MCiniMomTheta[MCpGunTrackID];
MCpGunMomPhi=MCiniMomPhi[MCpGunTrackID];
MCpGunE=MCiniE[MCpGunTrackID];
MCpGunXCalcatZDD=MCXCalcatZDD[MCpGunTrackID];
MCpGunYCalcatZDD=MCYCalcatZDD[MCpGunTrackID];

m_hPGunMCiniMomX->Fill(MCpGunMomX);
m_hPGunMCiniMomY->Fill(MCpGunMomY);
m_hPGunMCiniMomZ->Fill(MCpGunMomZ);
m_hPGunMCiniE->Fill(MCpGunE);
m_hPGunMCiniTheta->Fill(MCpGunMomTheta);
m_hPGunMCiniPhi->Fill(MCpGunMomPhi);
m_hPGunMCfinPosX->Fill(MCpGunfinPosX);
m_hPGunMCfinPosY->Fill(MCpGunfinPosY);
m_hPGunMCfinPosZ->Fill(MCpGunfinPosZ);
m_hPGunMCExtrapolatedZDDHitXY->Fill(MCpGunXCalcatZDD,MCpGunYCalcatZDD);
m_hPGunMCfinPosXY->Fill(MCpGunfinPosX,MCpGunfinPosY);
m_hPGunMCfinPosZX->Fill(MCpGunfinPosZ,MCpGunfinPosX);
m_hPGunMCfinPosZY->Fill(MCpGunfinPosZ,MCpGunfinPosY);

if(!m_IsZDDhit) return;

m_hPGunZDDFirstHitXY->Fill(xFirstZDDHit,yFirstZDDHit);
m_hPGunZDDDeltaHitXExtrapolX->Fill(xFirstZDDHit-MCpGunXCalcatZDD);
m_hPGunZDDDeltaHitYExtrapolY->Fill(yFirstZDDHit-MCpGunYCalcatZDD);
m_hPGunZDDEreco->Fill(zdd_energy/1000);
m_hPGunZDDErecovsETruth->Fill(MCpGunE,zdd_energy/1000);
for (std::map<std::vector<int>,double>::iterator it=m_mapcrystalenergyZDD.begin(); it!=m_mapcrystalenergyZDD.end(); ++it) {
    vector <int> coord = it->first;
		int crystalNo=coord[0];
		int partID=coord[1];
		vector<double> crystalcenter=GetCrystalCenter(crystalNo,partID);
		
		double energy=it->second;
		if(energy>0 && partID==2) m_hPGunClustersZDDRight->Fill(crystalcenter[0],crystalcenter[1]);
		}
//Fill NTuple	
m_particlegunTree->Fill();
	
}

//---------------------------------------------------------------------------------------------------------- 
void analysis_cZDD::AnalysisBhabha()
{
bool IsBhabha= m_MCNPrimElectron==1 && m_MCNPrimPositron==1;
if(!IsBhabha) return;

MCBhabhanGammas=m_MCNPrimGammas;

MCBhabhaEleciniPosX=MCiniPosX[MCBhabhaElecTrackID];
MCBhabhaEleciniPosX=MCiniPosX[MCBhabhaElecTrackID];
MCBhabhaEleciniPosY=MCiniPosY[MCBhabhaElecTrackID];
MCBhabhaEleciniPosZ=MCiniPosZ[MCBhabhaElecTrackID];
MCBhabhaElecfinPosX=MCfinPosX[MCBhabhaElecTrackID];
MCBhabhaElecfinPosY=MCfinPosY[MCBhabhaElecTrackID];
MCBhabhaElecfinPosZ=MCfinPosZ[MCBhabhaElecTrackID];
MCBhabhaElecMomRho=MCiniMomRho[MCBhabhaElecTrackID];
MCBhabhaElecMomTheta=MCiniMomTheta[MCBhabhaElecTrackID];
MCBhabhaElecMomPhi=MCiniMomPhi[MCBhabhaElecTrackID];
MCBhabhaElecE=MCiniE[MCBhabhaElecTrackID];
MCBhabhaElecXCalcatZDD=MCXCalcatZDD[MCBhabhaElecTrackID];
MCBhabhaElecYCalcatZDD=MCYCalcatZDD[MCBhabhaElecTrackID];

MCBhabhaPositroniniPosX=MCiniPosX[MCBhabhaPositronTrackID];
MCBhabhaPositroniniPosX=MCiniPosX[MCBhabhaPositronTrackID];
MCBhabhaPositroniniPosY=MCiniPosY[MCBhabhaPositronTrackID];
MCBhabhaPositroniniPosZ=MCiniPosZ[MCBhabhaPositronTrackID];
MCBhabhaPositronfinPosX=MCfinPosX[MCBhabhaPositronTrackID];
MCBhabhaPositronfinPosY=MCfinPosY[MCBhabhaPositronTrackID];
MCBhabhaPositronfinPosZ=MCfinPosZ[MCBhabhaPositronTrackID];
MCBhabhaPositronMomRho=MCiniMomRho[MCBhabhaPositronTrackID];
MCBhabhaPositronMomTheta=MCiniMomTheta[MCBhabhaPositronTrackID];
MCBhabhaPositronMomPhi=MCiniMomPhi[MCBhabhaPositronTrackID];
MCBhabhaPositronE=MCiniE[MCBhabhaPositronTrackID];
MCBhabhaPositronXCalcatZDD=MCXCalcatZDD[MCBhabhaPositronTrackID];
MCBhabhaPositronYCalcatZDD=MCYCalcatZDD[MCBhabhaPositronTrackID];

Hep3Vector V3Electron;
V3Electron.setRThetaPhi (MCBhabhaElecMomRho, MCBhabhaElecMomTheta, MCBhabhaElecMomPhi);
Hep3Vector V3Positron;
V3Positron.setRThetaPhi (MCBhabhaPositronMomRho, MCBhabhaPositronMomTheta, MCBhabhaPositronMomPhi);
MCBhabhaMomSum3VElectronPositron=(V3Electron+V3Positron).r();

printf("****** pz(e-) = %f pz(e+) = %f *******",V3Electron.z(),V3Positron.z());

Hep3Vector V3MissMom;
V3MissMom=V3Electron+V3Positron;


for(int k=0;k<MCBhabhanGammas;k++)
	{
	MCBhabhaGaminiPosX[k]=MCiniPosX[MCBhabhaGamTrackID[k]];
	MCBhabhaGaminiPosX[k]=MCiniPosX[MCBhabhaGamTrackID[k]];
	MCBhabhaGaminiPosY[k]=MCiniPosY[MCBhabhaGamTrackID[k]];
	MCBhabhaGaminiPosZ[k]=MCiniPosZ[MCBhabhaGamTrackID[k]];
	MCBhabhaGamfinPosX[k]=MCfinPosX[MCBhabhaGamTrackID[k]];
	MCBhabhaGamfinPosY[k]=MCfinPosY[MCBhabhaGamTrackID[k]];
	MCBhabhaGamfinPosZ[k]=MCfinPosZ[MCBhabhaGamTrackID[k]];
	MCBhabhaGamMomRho[k]=MCiniMomRho[MCBhabhaGamTrackID[k]];
	MCBhabhaGamMomTheta[k]=MCiniMomTheta[MCBhabhaGamTrackID[k]];
	MCBhabhaGamMomPhi[k]=MCiniMomPhi[MCBhabhaGamTrackID[k]];
	MCBhabhaGamE[k]=MCiniE[MCBhabhaGamTrackID[k]];
	MCBhabhaGamXCalcatZDD[k]=MCXCalcatZDD[MCBhabhaGamTrackID[k]];
	MCBhabhaGamYCalcatZDD[k]=MCYCalcatZDD[MCBhabhaGamTrackID[k]];
	
	Hep3Vector V3Gam;
	V3Gam.setRThetaPhi (MCBhabhaGamMomRho[k], MCBhabhaGamMomTheta[k], MCBhabhaGamMomPhi[k]);
	V3MissMom=V3MissMom+V3Gam;
	}

MCBhabhaMissMomX=V3MissMom.x();
MCBhabhaMissMomY=V3MissMom.y();
MCBhabhaMissMomZ=V3MissMom.z();

double Etot=MCBhabhaElecE+MCBhabhaPositronE;

m_hMCBhabhaEleciniE->Fill(MCBhabhaElecE);
m_hMCBhabhaEleciniMomRho->Fill(MCBhabhaElecMomRho);
m_hMCBhabhaEleciniMomTheta->Fill(MCBhabhaElecMomTheta);
m_hMCBhabhaEleciniMomPhi->Fill(MCBhabhaElecMomPhi);
m_hMCBhabhaElecExtrapolatedZDDHitXY->Fill(MCBhabhaElecXCalcatZDD,MCBhabhaElecYCalcatZDD);
m_hMCBhabhaElecfinPosXY->Fill(MCBhabhaElecfinPosX,MCBhabhaElecfinPosY);
m_hMCBhabhaElecfinPosZX->Fill(MCBhabhaElecfinPosZ,MCBhabhaElecfinPosX);
m_hMCBhabhaElecfinPosZY->Fill(MCBhabhaElecfinPosZ,MCBhabhaElecfinPosY);

m_hMCBhabhaPositroniniE->Fill(MCBhabhaPositronE);
m_hMCBhabhaPositroniniMomRho->Fill(MCBhabhaPositronMomRho);
m_hMCBhabhaPositroniniMomTheta->Fill(MCBhabhaPositronMomTheta);
m_hMCBhabhaPositroniniMomPhi->Fill(MCBhabhaPositronMomPhi);
m_hMCBhabhaPositronExtrapolatedZDDHitXY->Fill(MCBhabhaPositronXCalcatZDD,MCBhabhaPositronYCalcatZDD);
m_hMCBhabhaPositronfinPosXY->Fill(MCBhabhaPositronfinPosX,MCBhabhaPositronfinPosY);
m_hMCBhabhaPositronfinPosZX->Fill(MCBhabhaPositronfinPosZ,MCBhabhaPositronfinPosX);
m_hMCBhabhaPositronfinPosZY->Fill(MCBhabhaPositronfinPosZ,MCBhabhaPositronfinPosY);
	
if(!m_IsZDDhit) return;

m_hZDDBhabhaFirstHitXY->Fill(xFirstZDDHit,yFirstZDDHit);
m_hZDDBhabhaEreco->Fill(zdd_energy/1000);
m_hZDDBhabhaErecovsETruth->Fill(Etot,zdd_energy/1000);
m_hZDDBhabhaErecoLeft->Fill(zddLeft_energy);
m_hZDDBhabhaErecoRight->Fill(zddRight_energy);
m_hZDDBhabhaErecoLeftvsRight->Fill(zddRight_energy,zddLeft_energy);
//printf("****** Etot = %f zdd_energy/1000= %f ****** \n",Etot,zdd_energy/1000);

m_BhabhaTree->Fill();	
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
void analysis_cZDD::Analysis4PiGamISRTagged()
{

MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;
		
SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), "/Event/EvtRec/EvtRecEvent");
    if ( ! evtRecEvent ) {
        log << MSG::FATAL << "Could not find EvtRecEvent" << endreq;
        return;
    }
    SmartDataPtr<EvtRecTrackCol> evtRecTrackCol(eventSvc(), "/Event/EvtRec/EvtRecTrackCol");
    if ( ! evtRecTrackCol ) {
        log << MSG::FATAL << "Could not find EvtRecTrackCol" << endreq;
        return;
    }
    //Gaudi::svcLocator()->service("VertexDbSvc", m_vtxSvc);

EvtRecTrackIterator itBegin      = evtRecTrackCol->begin();
EvtRecTrackIterator itEndCharged = itBegin + evtRecEvent->totalCharged();
EvtRecTrackIterator itEndNeutral = itBegin + evtRecEvent->totalTracks();
    
    
int	iNCharged= evtRecEvent->totalCharged();
int	iNNeutral= evtRecEvent->totalNeutral();
cout << 	iNCharged << " "  << iNNeutral << endl;
		
EvtRecTrackIterator    itERT;

//Charged track selection
double       dDist, dMinDistP = 100, dMinDistN = 100;
double       dEoP,dEoPP,dEoPN;

vector<EvtRecTrackIterator>	vecERTGoodCharged;
vector<EvtRecTrackIterator>	vecERTGoodPip;
vector<EvtRecTrackIterator>	vecERTGoodPim;
vector<EvtRecTrackIterator>	vecERTGoodPiC;

vector<EvtRecTrackIterator> vecERTGoodPhoton;

m_T4PiGamISRPip1.Clear();
m_T4PiGamISRPip2.Clear();
m_T4PiGamISRPim1.Clear();
m_T4PiGamISRPim2.Clear();
m_T4PIGamISRGamISRZddTag.Clear();
m_T4PiGamISRMiss4PiCX.Clear();
/*
if(zdd_energy==0) return;
	
for (itERT  = itBegin;itERT != itEndCharged;itERT ++) {
if (!(*itERT)->isMdcKalTrackValid()) {continue;}
      if (!IsFromInteractionPoint(*itERT)) {continue;} // skip if track origin is not in the virtual box
			
      if ((*itERT)->isEmcShowerValid()) {
          // calculate EoP
          dEoP = (*itERT)->emcShower()->energy()/(*itERT)->mdcTrack()->p3().mag();
      }
      else {dEoP = -2;} // no valid emc track, but track will be accepted
      if (dEoP > m_dEoPcut) {continue;} //exclude e- candidates
			vecERTGoodCharged.push_back(itERT);
		
}
cout << "vecERTGoodCharged.size() " << vecERTGoodCharged.size() << endl;
if(vecERTGoodCharged.size()!=4) return;

int iPip=0;
int iPim=0;

ParticleID* PID        = ParticleID::instance();

for(int i=0;i<vecERTGoodCharged.size();i++){
	itERT=vecERTGoodCharged[i];
	double dDist = IPdist (*itERT);
	//cout << "dDist = " << dDist << endl; 
	
	PID->init();
	PID->setMethod(PID->methodProbability());
	PID->usePidSys(PID->useDedx());
  PID->usePidSys(PID->useEmc());
  PID->usePidSys(PID->useTof());
  PID->usePidSys(PID->useTof1());
  PID->usePidSys(PID->useTof2());
  PID->usePidSys(PID->useTofE());
	PID->identify(PID->onlyPion() | PID->onlyKaon() | PID->onlyProton());
  PID->setRecTrack(*itERT);
  PID->calculate();
	
	if(!(PID->IsPidInfoValid())) continue;
	RecMdcTrack* mdcTrk = (*itERT)->mdcTrack();
	double probPi = PID->probPion();
  double probK  = PID->probKaon();
  double probProton  = PID->probProton();
	
	if(probPi < 0.001) continue;
	if(!(probPi>probK && probPi>probProton)) continue;
	
	RecMdcKalTrack* mdcKalTrk = (*itERT)->mdcKalTrack();//After ParticleID, use RecMdcKalTrack substitute RecMdcTrack
	RecMdcKalTrack::setPidType  (RecMdcKalTrack::pion); //PID can set to electron, muon, pion, kaon and proton;The default setting is pion
	
	if(mdcKalTrk->charge() >0){	
	
	iPip++;
	vecERTGoodPip.push_back(itERT);
	
	WTrackParameter wtpPi (m_dMassPiC,mdcKalTrk->getZHelix(),mdcKalTrk->getZError());
   
  HepLorentzVector   heplv = wtpPi.p();
		
	TLorentzVector lv = HepLorentzVectortoTLorentzVector(heplv);

	if(iPip==1) m_T4PiGamISRPip1.Fill(&lv);
	if(iPip==2) m_T4PiGamISRPip2.Fill(&lv);	
	
	}
	else{

	iPim++;
	vecERTGoodPim.push_back(itERT);
	
	WTrackParameter wtpPi (m_dMassPiC,mdcKalTrk->getZHelix(),mdcKalTrk->getZError());
   
  HepLorentzVector   heplv = wtpPi.p();
		
	TLorentzVector lv = HepLorentzVectortoTLorentzVector(heplv);

	if(iPim==1) m_T4PiGamISRPim1.Fill(&lv);
	if(iPim==2) m_T4PiGamISRPim2.Fill(&lv);	
	
	}
	
}

if(iPip!=2 || iPim!=2) return;

//Find gamma candidates
for (itERT = itEndCharged;itERT != itEndNeutral;itERT ++) {

}

//Calculate missing 4 vector [4PiX]
TLorentzVector lvMiss4PiCX(0.,0.,0.,0.);
lvMiss4PiCX = m_lvBeamElectron+m_lvBeamPositron
-(m_T4PiGamISRPip1.GetLV()+m_T4PiGamISRPip2.GetLV()
+m_T4PiGamISRPim1.GetLV()+m_T4PiGamISRPim2.GetLV());

m_T4PiGamISRMiss4PiCX.Fill(&lvMiss4PiCX);
m_MissM4PiCX=lvMiss4PiCX.M();

m_XZddHitMiss4PiCX=-99.;
m_YZddHitMiss4PiCX=-99.;
Hep3Vector V3Mom;
	V3Mom.setRThetaPhi(lvMiss4PiCX.Rho(),lvMiss4PiCX.Theta(),lvMiss4PiCX.Phi());
	//Stretch V3Mom to ZDD
	Hep3Vector V3atZDD;
	
	double zZDD;
	double RhoV3atZDD;	
	//if(V3Mom.z()<0)
	if(lvMiss4PiCX.Pz()<0)
		{zZDD=-335;
		RhoV3atZDD=zZDD/cos(lvMiss4PiCX.Theta());
		V3Mom.setRThetaPhi(RhoV3atZDD,lvMiss4PiCX.Theta(),lvMiss4PiCX.Phi());
		m_XZddHitMiss4PiCX=V3Mom.x();
		m_YZddHitMiss4PiCX=V3Mom.y();
		}
	//else if(V3Mom.z()>0)
	else if(lvMiss4PiCX.Pz()>0) 
		{zZDD=335;
		RhoV3atZDD=zZDD/cos(MCiniMomTheta[MCntracks]);
		V3Mom.setRThetaPhi(RhoV3atZDD,lvMiss4PiCX.Theta(),lvMiss4PiCX.Phi());
		m_XZddHitMiss4PiCX=V3Mom.x();
		m_YZddHitMiss4PiCX=V3Mom.y();
		}
	else {m_XZddHitMiss4PiCX=-99;m_YZddHitMiss4PiCX=99;}
	

//ZDD Tag Photon selection
//Locate the hit position
//vector<double> ZddRecHit = GetZddPhotonRecHit(m_mapcrystalenergyZDD,"");
vector<double> ZddRecHit = GetZddPhotonRecHit(m_mapcrystalenergyZDD,"LogWeightAlg");
m_XZddRecHit=-99;
m_YZddRecHit=-99;
m_XZddRecHit=ZddRecHit[0];
m_YZddRecHit=ZddRecHit[1];

TLorentzVector lvGamISRZddTag(0.,0.,0.,0.);
TVector3 vGamISRZddTag(0.,0.,0.);
if(lvMiss4PiCX.Pz()<0){
	zZDD=-335;
	vGamISRZddTag.SetXYZ(m_XZddRecHit,m_YZddRecHit,zZDD);
	vGamISRZddTag.SetMag((lvMiss4PiCX.Vect()).Mag());
	lvGamISRZddTag.SetPxPyPzE(vGamISRZddTag.Px(),vGamISRZddTag.Py(),vGamISRZddTag.Pz(),lvMiss4PiCX.E());
}
else if(lvMiss4PiCX.Pz()>0) {
zZDD=335;
	vGamISRZddTag.SetXYZ(m_XZddRecHit,m_YZddRecHit,zZDD);
	vGamISRZddTag.SetMag((lvMiss4PiCX.Vect()).Mag());
	lvGamISRZddTag.SetPxPyPzE(vGamISRZddTag.Px(),vGamISRZddTag.Py(),vGamISRZddTag.Pz(),lvMiss4PiCX.E());
}		

m_T4PIGamISRGamISRZddTag.Fill(&lvGamISRZddTag);
*/

if(zdd_energy==0) return;
	
for (itERT  = itBegin;itERT != itEndCharged;itERT ++) {
if (!(*itERT)->isMdcKalTrackValid()) {continue;}
      if (!IsFromInteractionPoint(*itERT)) {continue;} // skip if track origin is not in the virtual box
			
      if ((*itERT)->isEmcShowerValid()) {
          // calculate EoP
          dEoP = (*itERT)->emcShower()->energy()/(*itERT)->mdcTrack()->p3().mag();
      }
      else {dEoP = -2;} // no valid emc track, but track will be accepted
      if (dEoP > m_dEoPcut) {continue;} //exclude e- candidates
			vecERTGoodCharged.push_back(itERT);
		
}
cout << "vecERTGoodCharged.size() " << vecERTGoodCharged.size() << endl;
if(vecERTGoodCharged.size()!=4) return;

int iPip=0;
int iPim=0;

ParticleID* PID        = ParticleID::instance();

for(int i=0;i<vecERTGoodCharged.size();i++){
	itERT=vecERTGoodCharged[i];
	double dDist = IPdist (*itERT);
	
	PID->init();
	PID->setMethod(PID->methodProbability());
	PID->usePidSys(PID->useDedx());
  PID->usePidSys(PID->useEmc());
  PID->usePidSys(PID->useTof());
  PID->usePidSys(PID->useTof1());
  PID->usePidSys(PID->useTof2());
  PID->usePidSys(PID->useTofE());
	PID->identify(PID->onlyPion() | PID->onlyKaon() | PID->onlyProton());
  PID->setRecTrack(*itERT);
  PID->calculate();
	
	if(!(PID->IsPidInfoValid())) continue;
	RecMdcTrack* mdcTrk = (*itERT)->mdcTrack();
	double probPi = PID->probPion();
  double probK  = PID->probKaon();
  double probProton  = PID->probProton();
	
	if(probPi < 0.001) continue;
	if(!(probPi>probK && probPi>probProton)) continue;
	
	RecMdcKalTrack* mdcKalTrk = (*itERT)->mdcKalTrack();//After ParticleID, use RecMdcKalTrack substitute RecMdcTrack
	RecMdcKalTrack::setPidType  (RecMdcKalTrack::pion); //PID can set to electron, muon, pion, kaon and proton;The default setting is pion
	
	if(mdcKalTrk->charge() == 1){		
	iPip++;
	vecERTGoodPip.push_back(itERT);	
	vecERTGoodPiC.push_back(itERT);
	}
	else if (mdcKalTrk->charge() == -1){
	iPim++;
	vecERTGoodPim.push_back(itERT);
	vecERTGoodPiC.push_back(itERT);
	}
	
}

if(vecERTGoodPip.size()!=2 && vecERTGoodPim.size()!=2) return;

//Vertex fit on charged tracks
HepPoint3D vx(0., 0., 0.);
HepSymMatrix Evx(3, 0);
double bx = 1E+6;
double by = 1E+6;
double bz = 1E+6;
Evx[0][0] = bx*bx;
Evx[1][1] = by*by;
Evx[2][2] = bz*bz;

VertexParameter vxpar;
vxpar.setVx(vx);
vxpar.setEvx(Evx);

VertexFit* vtxfit = VertexFit::instance();
vtxfit->init();

vector<HepLorentzVector> vechepKalTrklv;
HepLorentzVector heplvMiss=TLorentzVectortoHepLorentzVector(m_lvBeamElectron)+TLorentzVectortoHepLorentzVector(m_lvBeamPositron);
for(int k=0;k<vecERTGoodPiC.size();k++){	
	EvtRecTrackIterator    itERTPi=	vecERTGoodPiC[k];
	RecMdcKalTrack* mdcKalTrk = (*itERTPi)->mdcKalTrack();
	WTrackParameter wvpiTrk = WTrackParameter(mpi, mdcKalTrk->getZHelix(), mdcKalTrk->getZError());
	vtxfit->AddTrack(k,  wvpiTrk);
	
	heplvMiss=heplvMiss-wvpiTrk.p();
}
vtxfit->AddMissTrack(vecERTGoodPiC.size(),heplvMiss);
vtxfit->AddVertex(0, vxpar,0, 1);
if(!vtxfit->Fit(0)) return;
vtxfit->Swim(0);

vector<WTrackParameter> vecwpi;
vecwpi.resize(vecERTGoodPiC.size());
int nQm=0; int nQp=0;

for(int k=0;k<vecwpi.size();k++){
	vecwpi[k]=vtxfit->wtrk(k);
	cout << "vtxfit->wtrk(" << k << ")"<< endl;
	HepLorentzVector   heplv = vecwpi[k].p();		
	cout << "heplv.rho() = " << heplv.rho() << endl;
	if(heplv.rho()<0) cout << "heplv.rho()<0" << endl;
	TLorentzVector lv = HepLorentzVectortoTLorentzVector(heplv);
	cout << "lv.Rho()=" << lv.Rho() << endl;
	if(lv.Rho()<0) cout << "lv.Rho()<0" << endl;
	EvtRecTrackIterator    itERTPi=	vecERTGoodPiC[k];
	RecMdcKalTrack* mdcKalTrk = (*itERTPi)->mdcKalTrack();
	double charge = vecwpi[k].charge();	
	if(charge!=1 && charge!=-1)cout << "vecwpi[k].charge() " << vecwpi[k].charge() << " mdcKalTrk->charge() " << mdcKalTrk->charge() << endl;
	if(charge==1){
		nQp++;
		if(nQp==1) m_T4PiGamISRPip1.Fill(&lv);
		if(nQp==2) {m_T4PiGamISRPip2.Fill(&lv);
			if(lv.E()==-1) cout << "m_T4PiGamISRPip2.E()==-1" << endl;
		}
	}
	else if(charge==-1){
		nQm++;
		if(nQm==1) m_T4PiGamISRPim1.Fill(&lv);
		if(nQm==2) m_T4PiGamISRPim2.Fill(&lv);
	}
}

//Calculate missing 4 vector [4PiX]
TLorentzVector lvMiss4PiCX(0.,0.,0.,0.);
lvMiss4PiCX = m_lvBeamElectron+m_lvBeamPositron
-(m_T4PiGamISRPip1.GetLV()+m_T4PiGamISRPip2.GetLV()
+m_T4PiGamISRPim1.GetLV()+m_T4PiGamISRPim2.GetLV());



m_T4PiGamISRMiss4PiCX.Fill(&lvMiss4PiCX);
m_MissM4PiCX=lvMiss4PiCX.M();

m_XZddHitMiss4PiCX=-99.;
m_YZddHitMiss4PiCX=-99.;
Hep3Vector V3Mom;
V3Mom.setRThetaPhi(lvMiss4PiCX.Rho(),lvMiss4PiCX.Theta(),lvMiss4PiCX.Phi());
//Stretch V3Mom to ZDD
Hep3Vector V3atZDD;

double zZDD;
double RhoV3atZDD;	
//if(V3Mom.z()<0)
if(lvMiss4PiCX.Pz()<0)
	{zZDD=-335;
	RhoV3atZDD=zZDD/cos(lvMiss4PiCX.Theta()); //Warning : RhoV3atZDD could be negative
	V3Mom.setRThetaPhi(RhoV3atZDD,lvMiss4PiCX.Theta(),lvMiss4PiCX.Phi());
	m_XZddHitMiss4PiCX=V3Mom.x();
	m_YZddHitMiss4PiCX=V3Mom.y();
	}
//else if(V3Mom.z()>0)
else if(lvMiss4PiCX.Pz()>0) 
	{zZDD=335;
	RhoV3atZDD=zZDD/cos(lvMiss4PiCX.Theta());
	V3Mom.setRThetaPhi(RhoV3atZDD,lvMiss4PiCX.Theta(),lvMiss4PiCX.Phi());
	m_XZddHitMiss4PiCX=V3Mom.x();
	m_YZddHitMiss4PiCX=V3Mom.y();
	}
else {m_XZddHitMiss4PiCX=-99;m_YZddHitMiss4PiCX=99;}
	
//Find Zdd gamma candidates
for (itERT = itEndCharged;itERT != itEndNeutral;itERT ++) {
	//Exclude photon cluster from EMC?
	//
	if ((*itERT)->isMdcKalTrackValid() && (*itERT)->isEmcShowerValid()) {continue;}
	vecERTGoodPhoton.push_back(itERT);
}

//ZDD Tag Photon selection
//Locate the hit position
vector<double> ZddRecHit = GetZddPhotonRecHit(m_mapcrystalenergyZDD,"LogWeightAlg");
m_XZddRecHit=-99;
m_YZddRecHit=-99;
m_XZddRecHit=ZddRecHit[0];
m_YZddRecHit=ZddRecHit[1];

TLorentzVector lvGamISRZddTag(0.,0.,0.,0.);
TVector3 vGamISRZddTag(0.,0.,0.);
if(lvMiss4PiCX.Pz()<0){
	zZDD=-335;
	vGamISRZddTag.SetXYZ(m_XZddRecHit,m_YZddRecHit,zZDD);
	vGamISRZddTag.SetMag((lvMiss4PiCX.Vect()).Mag());
	lvGamISRZddTag.SetPxPyPzE(vGamISRZddTag.Px(),vGamISRZddTag.Py(),vGamISRZddTag.Pz(),lvMiss4PiCX.E());
}
else if(lvMiss4PiCX.Pz()>0) {
zZDD=335;
	vGamISRZddTag.SetXYZ(m_XZddRecHit,m_YZddRecHit,zZDD);
	vGamISRZddTag.SetMag((lvMiss4PiCX.Vect()).Mag());
	lvGamISRZddTag.SetPxPyPzE(vGamISRZddTag.Px(),vGamISRZddTag.Py(),vGamISRZddTag.Pz(),lvMiss4PiCX.E());
}		

m_T4PIGamISRGamISRZddTag.Fill(&lvGamISRZddTag);

//Kinematic fit on all ZDD photons candidates
//
//for(int k=0;k<vecERTGoodPhoton.size();k++){
//
//}

//Kinematic Fit
KalmanKinematicFit * kmfit = KalmanKinematicFit::instance();
kmfit->init();
for(int k=0;k<vecwpi.size();k++){
kmfit->AddTrack(k,vecwpi[k]);
}
kmfit->AddMissTrack(vecwpi.size()+1,TLorentzVectortoHepLorentzVector(lvGamISRZddTag));
TLorentzVector lvSumEpEm=m_lvBeamElectron+m_lvBeamPositron;
kmfit->AddFourMomentum(0,TLorentzVectortoHepLorentzVector(lvSumEpEm));
bool oksq = kmfit->Fit();
if(oksq){
	double chi2 = kmfit->chisq();
	
}   


//   //
//   //  Apply Kinematic 4C fit
//   // 
//   cout<<"before 4c"<<endl;
//   if(m_test4C==1) {
// //    double ecms = 3.097;
//     HepLorentzVector ecms(0.034,0,0,3.097);
// 
//     double chisq = 9999.;
//     int ig1 = -1;
//     int ig2 = -1;
//     for(int i = 0; i < nGam-1; i++) {
//       RecEmcShower *g1Trk = (*(evtRecTrkCol->begin()+iGam[i]))->emcShower();
//       for(int j = i+1; j < nGam; j++) {
// 	RecEmcShower *g2Trk = (*(evtRecTrkCol->begin()+iGam[j]))->emcShower();
// 	kmfit->init();
// 	kmfit->AddTrack(0, wpip);
// 	kmfit->AddTrack(1, wpim);
// 	kmfit->AddTrack(2, 0.0, g1Trk);
// 	kmfit->AddTrack(3, 0.0, g2Trk);
// 	kmfit->AddFourMomentum(0, ecms);
// 	bool oksq = kmfit->Fit();
// 	if(oksq) {
// 	  double chi2 = kmfit->chisq();
// 	  if(chi2 < chisq) {
// 	    chisq = chi2;
// 	    ig1 = iGam[i];
// 	    ig2 = iGam[j];
// 	  }
// 	}
//       }
//     }
//     
//     if(chisq < 200) { 
// 
//       RecEmcShower *g1Trk = (*(evtRecTrkCol->begin()+ig1))->emcShower();
//       RecEmcShower *g2Trk = (*(evtRecTrkCol->begin()+ig2))->emcShower();
//       kmfit->init();
//       kmfit->AddTrack(0, wpip);
//       kmfit->AddTrack(1, wpim);
//       kmfit->AddTrack(2, 0.0, g1Trk);
//       kmfit->AddTrack(3, 0.0, g2Trk);
//       kmfit->AddFourMomentum(0, ecms);
//       bool oksq = kmfit->Fit();
//       if(oksq) {
// 	HepLorentzVector ppi0 = kmfit->pfit(2) + kmfit->pfit(3);
// 	// m_mpi0 = ppi0.m();
// 	// m_chi1 = kmfit->chisq();
// 	//	m_tuple4->write();
//         Ncut4++;
//       }
//     }
//   }

m_T4PiGamISR->Fill();

printf("\n");
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double analysis_cZDD::beam_energy(int runNo)
{
    double ecms = -1;

    if( runNo>=23463 && runNo<=23505 ){//psi(4040) dataset-I
      ecms = 4.0104;
    }
    else if( runNo>=23509 && runNo<24141 ){//psi(4040) dataset-II
      ecms = 4.0093;
    }
    else if( runNo>=-24141 && runNo<=-23463 ){// psi(4009)
      ecms = 4.009;
    }
    else if( runNo>=24151 && runNo<=24177 ){//psi(2S) data
      ecms = 3.686;
    }
    else if( runNo>=29677 && runNo<=30367 ){//Y(4260) data-I
      ecms = 4.26;
    }
    else if( runNo>=31561 && runNo<=31981 ){//Y(4260) data-II
      ecms = 4.26;
    }
    else if( runNo>=31327 && runNo<=31390 ){//4420 data
      ecms = 4.42;
    }
    else if( runNo>=31281 && runNo<=31325 ){//4390 data
      ecms = 4.39;
    }
    else if( runNo>=30616 && runNo<=31279 ){//Y(4360) data
      ecms = 4.36;
    }
    else if( runNo>=30492 && runNo<=30557 ){// 4310 data
      ecms = 4.31;
    }
    else if( runNo>=30438 && runNo<=30491 ){// 4230 data
      ecms = 4.23;
    }
    else if( runNo>=30372 && runNo<=30437 ){// 4190 data
      ecms = 4.19;
    }
    else if( runNo>=31983 && runNo<=32045 ){// 4210 data
      ecms = 4.21;
    }
    else if( runNo>=32046 && runNo<=32140 ){// 4220 data
      ecms = 4.22;
    }
    else if( runNo>=32141 && runNo<=32226 ){// 4245 data
      ecms = 4.245;
    }
    else if( runNo>=32239 && runNo<=32850 ){// new 4230 data-I
      ecms = 4.23;
    }
    else if( runNo>=32851 && runNo<=33484 ){// new 4230 data-II
      ecms = 4.228;
    }
    else if( runNo>=33490 && runNo<=33556 ){// 3810 data
      ecms = 3.81;
    }
    else if( runNo>=33571 && runNo<=33657 ){// 3900 data
      ecms = 3.90;
    }
    else if( runNo>=33659 && runNo<=33719 ){// 4090 data
      ecms = 4.09;
    }
    else if( runNo>=11397 && runNo<=23454 ){// psi(3770) data
      ecms = 3.773;
    }

    return ecms;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
bool analysis_cZDD::IsFromInteractionPoint (EvtRecTrack* pCur)
{
		
    if (!pCur->isMdcKalTrackValid()) {return(false);}
    // Constructor with pivot, helix parameter a, and its error matrix.
    HepPoint3D   point0(0,0,0);
    VFHelix      helixipP(point0,pCur->mdcKalTrack()->helix(),pCur->mdcKalTrack()->err());
    helixipP.pivot(m_hp3IP);
    if (fabs(helixipP.a()[0]) < m_dCylRad && fabs(helixipP.a()[3]) < m_dCylHeight) {
      return (true);
    }
    return(false);
		
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double analysis_cZDD::IPdist (EvtRecTrack* pCur)
//----------------------------------------------------------------------------------------------------------
{
    // Constructor with pivot, helix parameter a, and its error matrix.
    HepPoint3D   point0(0,0,0);
    VFHelix      helixipP(point0,pCur->mdcKalTrack()->helix(),pCur->mdcKalTrack()->err());
    helixipP.pivot(m_hp3IP);
    return (sqrt(pow(fabs(helixipP.a()[0]),2)+pow(helixipP.a()[3],2)));
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
vector<double> analysis_cZDD::GetZddPhotonRecHit(std::map<std::vector<int>,double> mapcrystalenergyZDD, string mode)
//----------------------------------------------------------------------------------------------------------
{
    vector<double> RecHit;
		RecHit.resize(2);
		double sumweight=0;
		double xGravityCenter=0;
		double yGravityCenter=0;
		double ZDDenergy=0;
		for (std::map<std::vector<int>,double>::iterator it=mapcrystalenergyZDD.begin(); it!=mapcrystalenergyZDD.end(); ++it) {
    vector <int> key = it->first;
		int crystalNo=key[0];
		int partID=key[1];
		double val = it->second;
		vector<double> crystalcenter=GetCrystalCenter(crystalNo,partID);
		//printf("crystalNo %d partID %d val %f \n",key[0],key[1],val);
		double weight;
		if(mode.find("LogWeightAlg")!=std::string::npos){
			double W0=3.9;
			double crystalEnergy=mapcrystalenergyZDD[key];
			double eT=CalculateZddSumOfCrystalEnergy(mapcrystalenergyZDD);
			if((W0+log(crystalEnergy/eT))<=0) weight = 0;
			else weight = W0+log(crystalEnergy/eT);
		}
	 else weight = mapcrystalenergyZDD[key];
	 	double xcenter,ycenter;
		vector<double> crystalCenter=GetCrystalCenter(crystalNo,partID);
		xcenter = crystalCenter[0];
		ycenter = crystalCenter[1];
		
		xGravityCenter=xGravityCenter+weight*xcenter;
		yGravityCenter=yGravityCenter+weight*ycenter;
		
		sumweight = sumweight+weight;
		//printf("weight %f sumweight %f \n", weight,sumweight);
		ZDDenergy=ZDDenergy+mapcrystalenergyZDD[key]; 
	}
RecHit[0] = xGravityCenter/sumweight;
RecHit[1] = yGravityCenter/sumweight;	
return RecHit;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double analysis_cZDD::CalculateZddSumOfCrystalEnergy(std::map<std::vector<int>,double> mapcrystalenergyZDD)
//---------------------------------------------------------------------------------------------------------- 
{
double eT=0;
for(std::map<std::vector<int>,double>::iterator it2 = mapcrystalenergyZDD.begin();it2!=mapcrystalenergyZDD.end();++it2){
		vector <int> key2 = it2->first;
		double energy=mapcrystalenergyZDD[key2];
		eT=eT+energy;
		}

return eT;
}
//---------------------------------------------------------------------------------------------------------- 

//----------------------------------------------------------------------------------------------------------
TLorentzVector analysis_cZDD::HepLorentzVectortoTLorentzVector(HepLorentzVector& heplv)
//----------------------------------------------------------------------------------------------------------
{
TLorentzVector rootlv(0,0,0,0);
rootlv.SetPxPyPzE(heplv.px(),heplv.py(),heplv.pz(),heplv.e());
return rootlv;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
HepLorentzVector analysis_cZDD::TLorentzVectortoHepLorentzVector(TLorentzVector& lv)
//----------------------------------------------------------------------------------------------------------
{
HepLorentzVector heplv(0,0,0,0);
heplv.setPx(lv.Px());
heplv.setPy(lv.Py());
heplv.setPz(lv.Pz());
heplv.setE(lv.E());
return heplv;
}
//----------------------------------------------------------------------------------------------------------