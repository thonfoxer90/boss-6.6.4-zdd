#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TText.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

using namespace std;

void DrawcZDDcrystals()
{
gStyle->SetOptStat(0);
double crystalDX=1;
double crystalDY=1;
int NCrystalsX=4;
int NCrystalsY=3;
double XEdgeOrigTop=0;
double YEdgeOrigTop=0.5;
double XEdgeOrigBottom=0;XEdgeOrigBottom=XEdgeOrigTop;
double YEdgeOrigBottom;YEdgeOrigBottom=-YEdgeOrigTop;
//Top cZDD
vector<TLine*> linesY;
vector<TLine*> linesX;

double x0,y0;
double x1,y1;
x0=0;y0=0;
x1=0;y1=0;
for(int k=0;k<5;k++)
	{
	x0=k*crystalDX+XEdgeOrigTop;
	y0=0.5;
	x1=x0;
	y1=NCrystalsY*crystalDX+YEdgeOrigTop;
	linesY.push_back(new TLine(x0,y0,x1,y1));
	linesY.back()->SetLineColor(1);
	linesY.back()->SetLineWidth(2);
	linesY.back()->Draw();
	}

for(int k=0;k<4;k++)
	{
	x0=XEdgeOrigTop;
	y0=k*crystalDX+YEdgeOrigTop;
	x1=NCrystalsX*crystalDX+XEdgeOrigTop;
	y1=y0;
	linesX.push_back(new TLine(x0,y0,x1,y1));
	linesX.back()->SetLineColor(1);
	linesX.back()->SetLineWidth(2);
	linesX.back()->Draw();
	}
	
//Bottom cZDD
vector<TLine*> linesYBottom;
vector<TLine*> linesXBottom;

for(int k=0;k<5;k++)
	{
	x0=k*crystalDX+XEdgeOrigBottom;
	y0=YEdgeOrigBottom;
	x1=x0;
	y1=-NCrystalsY*crystalDX+YEdgeOrigBottom;
	linesYBottom.push_back(new TLine(x0,y0,x1,y1));
	linesYBottom.back()->SetLineColor(1);
	linesYBottom.back()->SetLineWidth(2);
	linesYBottom.back()->Draw();
	}

for(int k=0;k<4;k++)
	{
	x0=XEdgeOrigBottom;
	y0=-k*crystalDX+YEdgeOrigBottom;
	x1=NCrystalsX*crystalDX+XEdgeOrigBottom;
	y1=y0;
	linesXBottom.push_back(new TLine(x0,y0,x1,y1));
	linesXBottom.back()->SetLineColor(1);
	linesXBottom.back()->SetLineWidth(2);
	linesXBottom.back()->Draw();
	}
	
}

void DrawZddMCTruthXY(string inputfile, string outpath,string option){
if(option.find("drawcont")!= string::npos){
gStyle->SetNumberContours(8);
}

TFile* file = TFile::Open(inputfile.c_str());
if(file==NULL) return;

TH2D* hZddXYEast;
TH2D* hZddXYWest;

string outps = outpath+".ps";
string outpdf = outpath+".pdf";

if(option.find("MCTruthISR")!= string::npos){
TTree* tree = (TTree*)file->Get("AllMCTruthTree");
if(tree==NULL) return;

const int NMAX=256;
int MCntracks;
int 	 MCpartProp[NMAX];
double MCiniMomRho[NMAX];
double MCiniMomTheta[NMAX];
double MCiniMomPhi[NMAX];
double MCiniE[NMAX];
double MCiniMomRhoCM[NMAX];
double MCiniMomThetaCM[NMAX];
double MCiniMomPhiCM[NMAX];
double MCiniECM[NMAX];
double MCXCalcatZDD[NMAX];
double MCYCalcatZDD[NMAX];
double MCiniMomX[NMAX];
double MCiniMomY[NMAX];
double MCiniMomZ[NMAX];

tree->SetBranchAddress("MCntracks", &MCntracks);
tree->SetBranchAddress("MCpartProp", MCpartProp);
tree->SetBranchAddress("MCiniMomRho", MCiniMomRho);
tree->SetBranchAddress("MCiniMomTheta", MCiniMomTheta);
tree->SetBranchAddress("MCiniMomPhi", MCiniMomPhi);
tree->SetBranchAddress("MCiniE", MCiniE);

tree->SetBranchAddress("MCiniMomX", MCiniMomX);
tree->SetBranchAddress("MCiniMomY", MCiniMomY);
tree->SetBranchAddress("MCiniMomZ", MCiniMomZ);

tree->SetBranchAddress("MCXCalcatZDD", MCXCalcatZDD);
tree->SetBranchAddress("MCYCalcatZDD", MCYCalcatZDD);

if(option.find("drawcont")!= string::npos){
/*
hZddXYEast = new TH2D("hZddXYEast","ZDD East;X_{interpolated} (cm);Y_{interpolated} (cm) ",64,-1.,5., 64,-5.,5.);
hZddXYWest = new TH2D("hZddXYWest","ZDD West;X_{interpolated} (cm);Y_{interpolated} (cm) ",64,-1.,5., 64,-5.,5.);
*/

hZddXYEast = new TH2D("hZddXYEast","ZDD East;X_{interpolated} (cm);Y_{interpolated} (cm) ",256,-1.,5., 256,-5.,5.);
hZddXYWest = new TH2D("hZddXYWest","ZDD West;X_{interpolated} (cm);Y_{interpolated} (cm) ",256,-1.,5., 256,-5.,5.);

}
else {
hZddXYEast = new TH2D("hZddXYEast","ZDD East;X_{interpolated} (cm);Y_{interpolated} (cm) ",256,-1.,5., 256,-5.,5.);
hZddXYWest = new TH2D("hZddXYWest","ZDD West;X_{interpolated} (cm);Y_{interpolated} (cm) ",256,-1.,5., 256,-5.,5.);
}

for(int entry=0;entry<tree->GetEntries();entry++){
	tree->GetEntry(entry);
	for(int i=0;i<MCntracks;i++){
		if(MCpartProp[i]==22){
			if(MCiniMomZ[i]<0)	hZddXYEast->Fill(MCXCalcatZDD[i],MCYCalcatZDD[i]);
			else 	hZddXYWest->Fill(MCXCalcatZDD[i],MCYCalcatZDD[i]);
		}
	} 
} 
}

else if (option.find("4PiCGamISR")!= string::npos){
TTree* tree = (TTree*)file->Get("4PiGamISRTagged");
if(tree==NULL) return;

double XZddRecHit;
double YZddRecHit;
double XZddHitMiss4PiCX;
double YZddHitMiss4PiCX;
double XZddGammaISRMCTruth;
double YZddGammaISRMCTruth;

double zdd_energy;
double zddLeft_energy;
double zddRight_energy;

tree->SetBranchAddress("XZddRecHit", &XZddRecHit);
tree->SetBranchAddress("YZddRecHit", &YZddRecHit);

tree->SetBranchAddress("XZddHitMiss4PiCX",&XZddHitMiss4PiCX);
tree->SetBranchAddress("YZddHitMiss4PiCX",&YZddHitMiss4PiCX);

tree->SetBranchAddress("XZddGammaISRMCTruth",&XZddGammaISRMCTruth);
tree->SetBranchAddress("YZddGammaISRMCTruth",&YZddGammaISRMCTruth);

tree->SetBranchAddress("zdd_energy",&zdd_energy);
tree->SetBranchAddress("zddLeft_energy",&zddLeft_energy);
tree->SetBranchAddress("zddRight_energy",&zddRight_energy);

hZddXYEast = new TH2D("hZddXYEast","ZDD East;X_{interpolated} (cm);Y_{interpolated} (cm) ",256,-1.,5., 256,-5.,5.);
hZddXYWest = new TH2D("hZddXYWest","ZDD West;X_{interpolated} (cm);Y_{interpolated} (cm) ",256,-1.,5., 256,-5.,5.);

for(int entry=0;entry<tree->GetEntries();entry++){
	tree->GetEntry(entry);
	if(zddLeft_energy>0)	hZddXYEast->Fill(XZddRecHit,YZddRecHit);
	else if (zddRight_energy>0) hZddXYWest->Fill(XZddRecHit,YZddRecHit);
}

}

TCanvas* c = new TCanvas();
gPad->SetLogz();
c->Print(Form("%s[",outps.c_str())); 	
//if(option.find("drawcont")!= string::npos) hZddXYEast->Draw("contz");
if(option.find("drawcont")!= string::npos) hZddXYEast->Draw("colz");
else hZddXYEast->Draw("colz");
DrawcZDDcrystals();
c->Print(Form("%s",outps.c_str()));
c->Clear();
//if(option.find("drawcont")!= string::npos) hZddXYWest->Draw("contz");
if(option.find("drawcont")!= string::npos) hZddXYWest->Draw("colz");
else hZddXYWest->Draw("colz");
DrawcZDDcrystals();
c->Print(Form("%s",outps.c_str()));
c->Clear();
c->Print(Form("%s]",outps.c_str()));

//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

return;
}