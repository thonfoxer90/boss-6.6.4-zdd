#include <TFile.h>
#include <TDirectory.h>
#include <TMath.h>
#include <TDatabasePDG.h>
#include <TParticlePDG.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TSpline.h>
#include <TPolyLine3D.h>
#include <TVector3.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TView.h>
#include <TAxis3D.h>
#include <TPolyMarker3D.h>
#include <TImage.h>
#include <TASImage.h>

#include "Math/GenVector/Plane3D.h"
#include "Math/GenVector/DisplacementVector3D.h"
#include "Math/GenVector/PositionVector3D.h"

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

//#ifdef __CINT__
//#pragma link C++ class std::vector<double>;
//#endif

#ifdef __MAKECINT__
//#pragma link C++ class vector<double>+;
#pragma link C++ class vector<string>+;
#endif
    
using namespace std;

const double m_dMasselec					 = 511e-6;
const double m_dMassPi0           = 0.134976;    // PDG 2010
const double m_dMassPiCharged     = 0.139570;    // PDG 2010

const double m_dCrossingAngle		 = 0.011;
const double m_rad2Deg = 180/TMath::Pi();
	
TFile* m_file;
TTree* m_tree;

int m_evtID;

double m_zdd_energy;
double m_zddLeft_energy;
double m_zddRight_energy;

std::vector<int> *m_vectrackIDStep = 0;
TBranch *m_bvectrackIDStep=0;

std::vector<int> *m_vecparentIDStep = 0;
TBranch *m_bvecparentIDStep=0;

std::vector<int> *m_vecPDGCodeStep = 0;
TBranch *m_bvecPDGCodeStep=0;

std::vector<double> *m_vecXPreStep = 0;
TBranch *m_bvecXPreStep=0;

std::vector<double> *m_vecYPreStep = 0;
TBranch *m_bvecYPreStep=0;

std::vector<double> *m_vecZPreStep = 0;
TBranch *m_bvecZPreStep=0;

std::vector<double> *m_vecXPostStep = 0;
TBranch *m_bvecXPostStep=0;

std::vector<double> *m_vecYPostStep = 0;
TBranch *m_bvecYPostStep=0;

std::vector<double> *m_vecZPostStep = 0;
TBranch *m_bvecZPostStep=0;

std::vector<double> *m_vecPxPreStep = 0;
TBranch *m_bvecPxPreStep=0;

std::vector<double> *m_vecPyPreStep = 0;
TBranch *m_bvecPyPreStep=0;

std::vector<double> *m_vecPzPreStep = 0;
TBranch *m_bvecPzPreStep=0;

std::vector<double> *m_vecPxPostStep = 0;
TBranch *m_bvecPxPostStep=0;

std::vector<double> *m_vecPyPostStep = 0;
TBranch *m_bvecPyPostStep=0;

std::vector<double> *m_vecPzPostStep = 0;
TBranch *m_bvecPzPostStep=0;

std::vector< std::string > *m_vecVolPreStep = 0;
TBranch *m_bvecVolPreStep=0;

std::vector< std::string > *m_vecVolPostStep = 0;
TBranch *m_bvecVolPostStep=0;

std::vector< double > *m_vecETotPreStep = 0;
TBranch *m_bvecETotPreStep=0;

std::vector< double > *m_vecETotPostStep = 0;
TBranch *m_bvecETotPostStep=0;

std::vector< double > *m_vecEDepStep = 0;
TBranch *m_bvecEDepStep=0;

std::map<int,std::vector<TVector3> > m_mapTrackPoints;
std::map<int,std::vector<TVector3> > m_mapTrackMomentum;
std::map<int,int> m_mapTrackPDG;
std::map<int,int> m_mapTrackPrimary;

TLorentzVector m_lvBeamElectron;
TLorentzVector m_lvBeamPositron;


std::vector<TPolyLine3D> m_vTrajectories;
std::vector<int> m_vTrajPDGs;
std::vector<int> m_vTrajPrimary;
std::vector<TLorentzVector> m_vIniLV;
std::map<int,int> m_mapColor;

std::string m_outpath;

std::string m_option;

TDatabasePDG* pdg;

double eTotPrimaries;
double eTotExitPipe;
double eTotEntersZDD;
double eGammasEntersZDD;
double eElectronsEntersZDD;
double ePositronsEntersZDD;
double eElectronsPositronsEntersZDD;

double eDepBeforeZDD;

int nHitsZDD;
int nHitsGammasZDD;
int nHitsElectronsZDD;
int nHitsPositronsZDD;
int nHitsZDDEast;
int nHitsZDDWest;

int nHitsPrimaryZDD;
int nHitsPrimaryZDDEast;
int nHitsPrimaryZDDWest;

int nExitWindow;
	

TH1D* hNHitsZDD;
TH1D* hNHitsGammasZDD;
TH1D* hNHitsElectronsZDD;
TH1D* hNHitsPositronsZDD;
TH1D* hNHitsZDDEast;
TH1D* hNHitsZDDWest;

TH1D* hNHitsPrimaryZDD;
TH1D* hNHitsPrimaryZDDEast;
TH1D* hNHitsPrimaryZDDWest;

TH1D* hPDGHitsZDD;
TH1D* hPDGHitsZDDEast;
TH1D* hPDGHitsZDDWest;

TH1D* hPDGHitsPrimaryZDD;
TH1D* hPDGHitsPrimaryZDDEast;
TH1D* hPDGHitsPrimaryZDDWest;

TH1D* hETotPrimaries;
TH1D* hETotEntersZDD;
TH1D* hEGammasEntersZDDOverETotEntersZDD;
TH1D* hEElectronsEntersZDDOverETotEntersZDD;
TH1D* hEPositronsEntersZDDOverETotEntersZDD;
TH1D* hETotExitPipe;
TH1D* hEDepZDD;

TH1D* hETotEntersZDDOverETotPrimaries;
TH1D* hEGammasEntersZDDOverETotPrimaries;
TH1D* hEElectronsEntersZDDOverETotPrimaries;
TH1D* hEPositronsEntersZDDOverETotPrimaries;
TH1D* hEElectronsPositronsEntersZDDOverETotPrimaries;
TH1D* hEDepZDDOverETotPrimaries;
TH1D* hEDepZDDOverETotEntersZDD;
//TH1D* hETotEntersZDDOverETotExitPipe;
TH1D* hETotEntersZDDOverETotExitPipe;

map<string,ROOT::Math::Plane3D> m_mapPlanes;
map<string,TH2D*> m_maph2DPlaneHits;
map<string,TTree*> m_mapHitsNTuples;

const double m_WorldXMin=-500.;
const double m_WorldXMax=500.;

const double m_WorldYMin=-100.;
const double m_WorldYMax=100.;

//const double m_WorldZMin=-3500.;
//const double m_WorldZMax=3500;

const double m_WorldZMin=-5000.;
const double m_WorldZMax=5000.;

vector <string> m_crystalList;
vector <string> m_EnergyList;

double m_Q2;
double m_theta;
double m_phi;

int m_NIntersectPoints;
vector<double> m_xImpacts;
vector<double> m_yImpacts;
vector<double> m_zImpacts;
vector<double> m_ThetaImpacts;
vector<double> m_PhiImpacts;

string doubleToStr(double val){
std::ostringstream sstream;
sstream << val;
std::string varAsString = sstream.str();
return varAsString;
}

string intToStr(int val){
std::ostringstream sstream;
sstream << val;
std::string varAsString = sstream.str();
return varAsString;
}

double Phi0to2Pi(double phi){
double newphi;
if(phi<0.) newphi=2*TMath::Pi()+phi;
else newphi=phi;
return newphi;
}

void Print(TVector3 vec3){
cout << vec3.X() << " "<< vec3.Y() << " " << vec3.Z() << endl;
}

void Print(vector<TVector3> vec){
for(int k=0;k<vec.size();k++){
//cout << vec[k].X() << " "<< vec[k].Y() << " " << vec[k].Z() << endl;
Print(vec[k]);
}
cout << endl;
}

vector<double> makeLimBinVec(double min,double max, int NBins){
vector<double> limbin;
limbin.resize(NBins+1);
for(int i=0;i<limbin.size();i++){
	limbin[i]=min+i*(max-min)/((double)NBins);
	cout<< limbin[i] << " " ;
}
cout << endl;
return limbin;
}

void translateGraph(TGraph* gr, vector<double> &vecTransform){
for(int i=0;i<gr->GetN();i++){
double x=0;
double y=0;
gr->GetPoint(i,x,y);
gr->SetPoint(i,x+vecTransform[0],y+vecTransform[1]);
}

}

void Initialize()
{
m_mapTrackPoints.clear();
m_mapTrackMomentum.clear();
m_mapTrackPDG.clear();

m_vTrajectories.clear();
m_vTrajPDGs.clear();
m_vTrajPrimary.clear();
m_vIniLV.clear();
}

void DrawTitle(string str,double posX=0.5,double posY=0.97,double size=0.04){
TLatex* ltx = new TLatex();
ltx->SetNDC();
ltx->SetTextSize(size);
ltx->SetTextAlign(22);
ltx->DrawLatex(posX,posY,str.c_str());
}

void AddPointToTrajectory(int trackID,TVector3 vec)
{
m_mapTrackPoints[trackID].push_back(vec);	
}

TPolyLine3D MakeTPolyLine3D(std::vector<TVector3> traj)
{
TPolyLine3D poly3D(traj.size());
for(int k=0;k<traj.size();k++)
	{
	//printf("MakeTPolyLine3D traj[k].Z()=%f \n",traj[k].Z());
	//sleep(1);
	poly3D.SetPoint(k,traj[k].X(),traj[k].Y(),traj[k].Z());
	}
return poly3D;	
}

void PrintTrajectory(const TPolyLine3D& poly3D){
Float_t* vecP = poly3D.GetP();
	Int_t npoints = poly3D.GetN();
for(int i=0;i<npoints;i++){
	int iX = i*3;
	int iY = i*3+1;
	int iZ = i*3+2;
	TVector3 a(vecP[iX],vecP[iY],vecP[iZ]);
	cout << a.X() << " " << a.Y() << " " << a.Z() << endl;
}
cout << endl;
}

ROOT::Math::Plane3D::Vector makeUnitVector(const TVector3 &ROOTvec3){
TVector3 ROOTvec3Norm = ROOTvec3.Unit();
ROOT::Math::Plane3D::Vector v(ROOTvec3Norm.X(),ROOTvec3Norm.Y(),ROOTvec3Norm.Z());
return v;
}

TVector3* getIntersectLinePlane(const ROOT::Math::Plane3D &plane3D,const TVector3 &a, const TVector3 &b) {
		/*
		ROOT::Math::Plane3D::Scalar d = plane3D.HesseDistance();
		ROOT::Math::Plane3D::Vector n = plane3D.Normal();
    TVector3 ba = b-a;
    float nDotA = n.Dot(a);
    float nDotBA = n.Dot(ba);
		float t = ((d - nDotA)/nDotBA);
		cout << "d = " << d << endl;
		cout << "(b-a) = " ; Print(ba);
		cout << "n.Dot(ba) = " << nDotBA << endl;
		cout << "t = " << t << endl;
		
		//if(nDotBA==0 || fabs(t)>1){
		if(nDotBA==0 || t<-1 || t>0){
			return NULL;
		}
		
    else {
			TVector3* result = new TVector3(a + (t * ba));
			return result;		
		}
		*/
		
		
		ROOT::Math::Plane3D::Scalar d = -plane3D.HesseDistance();
		ROOT::Math::Plane3D::Vector n = plane3D.Normal();
    TVector3 ba = b-a;
    float nDotA = n.Dot(a);
    float nDotBA = n.Dot(ba);
		float t = ((d - nDotA)/nDotBA);
		//cout << "d = " << d << endl;
		//cout << "(b-a) = " ; Print(ba);
		//cout << "n.Dot(ba) = " << nDotBA << endl;
		//cout << "t = " << t << endl;
		
		//if(nDotBA==0 || fabs(t)>1){
		if(nDotBA==0 || t>1 || t<0){
			return NULL;
		}
		
    else {
			TVector3* result = new TVector3(a + (t * ba));
			return result;		
		}
		
}

vector<TVector3> getIntersectPolyLinePlane(const ROOT::Math::Plane3D &plane3D,const TPolyLine3D &poly3D){
	vector<TVector3> vintersectPoints;
	Float_t* vecP = poly3D.GetP();
	Int_t npoints = poly3D.GetN();
		for(int i=0;i<npoints-1;i++){
		int iXA = i*3;
		int iYA = i*3+1;
		int iZA = i*3+2;
		TVector3 a(vecP[iXA],vecP[iYA],vecP[iZA]);
		
		int iXB = (i+1)*3;
		int iYB = (i+1)*3+1;
		int iZB = (i+1)*3+2;
		TVector3 b(vecP[iXB],vecP[iYB],vecP[iZB]);
		
		TVector3* intersectPoint=getIntersectLinePlane(plane3D,a,b);
		if(intersectPoint==NULL) continue;
		else vintersectPoints.push_back(*intersectPoint);
		}
	return vintersectPoints;
}

void TrackPointsToTrajectories(){

for (std::map<int,std::vector<TVector3> > ::iterator it=m_mapTrackPoints.begin(); it!=m_mapTrackPoints.end(); ++it)
		{
		int trackIndex = it->first;
		std::vector<TVector3> traj=it->second;
		
		if(m_option.find("HorizontalZ ") != std::string::npos) for(int k=0;k<traj.size();k++) traj[k].RotateX(-TMath::Pi()/2);
		TPolyLine3D poly3D = MakeTPolyLine3D(traj);
		m_vTrajectories.push_back(poly3D);
		
		m_vTrajPDGs.push_back(m_mapTrackPDG[trackIndex]);
		}
}

void DrawBes(string key){

if(key.find("Z=-2900mm")!=string::npos || key.find("Z=2900mm")!=string::npos){
TGraph* grPipeOut = new TGraph("/data/work/kphpbb/bgarillo/cZDD/BeamPipeOutXY_Zeq2900mm.dat");
grPipeOut->Draw("LP same");

TGraph* grPipeIn = new TGraph("/data/work/kphpbb/bgarillo/cZDD/BeamPipeOutXY_Zeq2900mm.dat");
vector<double> vecTranslate;
vecTranslate.resize(2); vecTranslate[0]=-125.9; vecTranslate[1]=0;
translateGraph(grPipeIn, vecTranslate);
grPipeIn->Draw("LP same");
}

if(key.find("Z=-3350mm")!=string::npos || key.find("Z=3350mm")!=string::npos){
TGraph* grPipeOut = new TGraph("/data/work/kphpbb/bgarillo/cZDD/BeamPipeOutXY_Zeq3350mm.dat");
grPipeOut->Draw("LP same");

TGraph* grPipeIn = new TGraph("/data/work/kphpbb/bgarillo/cZDD/BeamPipeOutXY_Zeq3350mm.dat");
vector<double> vecTranslate;
vecTranslate.resize(2); vecTranslate[0]=-124; vecTranslate[1]=0;
translateGraph(grPipeIn, vecTranslate);
grPipeIn->Draw("LP same");
}

}

TCanvas* makeHitDistThetaPhi(string key, const vector<double> &limbinTheta,const vector<double> &limbinPhi){
TCanvas* c = new TCanvas();

c->Divide(4,4,0.02,0.02);
DrawTitle(key,0.5,0.99,0.02);
int nPad=1;
for(int i=0;i<limbinTheta.size()-1;i++){
	for(int j=0;j<limbinPhi.size()-1;j++){
		c->cd(nPad);
		char hname[256];
		char drawcmd[256];
		char selectioncmd[256];
		char title[256];
		sprintf(hname,"h%d%d",i,j);		sprintf(drawcmd,"yImpacts:xImpacts>>%s(%d,%f,%f,%d,%f,%f)",hname,(int)(m_WorldXMax-m_WorldXMin),m_WorldXMin,m_WorldXMax,(int)(m_WorldYMax-m_WorldYMin),m_WorldYMin,m_WorldYMax);
		sprintf(selectioncmd,"ThetaImpacts>%f && ThetaImpacts<%f && PhiImpacts>%f && PhiImpacts<%f",limbinTheta[i],limbinTheta[i+1],limbinPhi[j],limbinPhi[j+1]);
		sprintf(title,"%.2f<#theta<%.2f deg, %.2f<#Phi<%.2f deg",limbinTheta[i]*m_rad2Deg,limbinTheta[i+1]*m_rad2Deg,limbinPhi[j]*m_rad2Deg,limbinPhi[j+1]*m_rad2Deg);
		m_mapHitsNTuples[key]->Draw(drawcmd,selectioncmd);
		
		TH2D* histo = (TH2D*) gROOT->FindObject(hname);
			if(histo->GetEntries()>0){
			histo->Draw("");		
			histo->SetMarkerStyle(7);
			if(key.find("PDG=-11")!=string::npos)histo->SetMarkerColor(2);
			if(key.find("PDG=11")!=string::npos)histo->SetMarkerColor(4);		
			DrawTitle(string(title));
			DrawBes(key);
		}
		/*
		histo->SetMarkerStyle(8);
		//if(key.find("PDG=-11")!=string::npos)histo->SetMarkerColor(2);
		histo->SetMarkerColor(2);
		*/
		
		//histo->Draw("box");
		//histo->SetLineColor(2);
		
		nPad++;
	}
}
return c; 
}

void DrawTrajectories()
{
//std::string outname=m_outpath+".gif";
std::string outname=m_outpath+".gif+10";

//TCanvas *c1 = new TCanvas("c1","",1280,720);
//TCanvas *c1 = new TCanvas("c1","",1000.,2500.);
TCanvas *c1 = new TCanvas("c1","",800.,1800.);
c1->Divide(2,4);
vector< vector< TPolyLine3D > > vtraj;
vtraj.resize(4);
for(int padnb=1;padnb<=8;padnb++)
	{
	c1->cd(padnb);
	if(padnb<=4){
		TView *view = TView::CreateView(1);

		if(m_option.find("HorizontalZ ") != std::string::npos) view->SetRange(m_WorldXMin,m_WorldZMin,m_WorldYMin,m_WorldXMax,m_WorldZMax,m_WorldYMax);
		else view->SetRange(m_WorldXMin,m_WorldYMin,m_WorldZMin,m_WorldXMax,m_WorldYMax,m_WorldZMax);
		//printf("Trajectories.size() = %d \n",Trajectories.size());

		//TAxis3D *axis = TAxis3D::GetPadAxis(gPad);

		//view->RotateView(TMath::Pi()/2,0.);
		//view->RotateView(TMath::Pi()/2,3*TMath::Pi()/2);
		if(m_option.find("ViewPipeZDDRegionWest ") != std::string::npos) view->SetRange(m_WorldXMin,m_WorldYMin,2900.,m_WorldXMax,m_WorldYMax,3600.,1);
		if(m_option.find("ViewPipeZDDRegionEast ") != std::string::npos) view->SetRange(m_WorldXMin,m_WorldYMin,-3600.,m_WorldXMax,m_WorldYMax,-2900.,1);
		for(int k=0;k<m_vTrajectories.size();k++){
			m_vTrajectories[k].Draw();		
			m_vTrajectories[k].SetLineColor(m_mapColor[m_vTrajPDGs[k]]);
		}

		view->ShowAxis();
		//view->RotateView(TMath::Pi(),0.);
		if(padnb==1){
			/*
			TLatex* ltx = new TLatex();
			ltx->SetNDC();
			char strltx[256];
			sprintf(strltx,"event #%d",m_evtID);
			ltx->SetTextAngle(90.);
			ltx->DrawLatex(0.3,0.75,strltx);
			*/
			view->Front();
		}
		if(padnb==2) view->Side();
		if(padnb==3) {view->Top();}
		//if(padnb==4) {view->RotateView(TMath::Pi()/2,0.);}
	}
	if(m_option.find("showHitsPlanes ") != std::string::npos){
		if(padnb==5){
			m_maph2DPlaneHits["Primary, PDG=-11, Z=-2900mm"]->Draw("");
			m_maph2DPlaneHits["Primary, PDG=11, Z=-2900mm"]->Draw("same");
			m_maph2DPlaneHits["Primary, PDG=22, Z=-2900mm"]->Draw("same");
			DrawBes("Z=-2900mm");
			DrawTitle("Primary, Z=-2900mm");
		}
		if(padnb==6){
			
			m_maph2DPlaneHits["Primary, PDG=11, Z=2900mm"]->Draw("");
			m_maph2DPlaneHits["Primary, PDG=-11, Z=2900mm"]->Draw("same");
			m_maph2DPlaneHits["Primary, PDG=22, Z=2900mm"]->Draw("same");
			DrawBes("Z=-2900mm");
			DrawTitle("Primary, Z=2900mm");
		}
		if(padnb==7){
			m_maph2DPlaneHits["Primary, PDG=-11, X=100mm"]->Draw("");
			m_maph2DPlaneHits["Primary, PDG=11, X=100mm"]->Draw("same");
			m_maph2DPlaneHits["Primary, PDG=22, X=100mm"]->Draw("same");
			DrawTitle("Primary, X=100mm");
		}
		if(padnb==8){
			m_maph2DPlaneHits["Primary, PDG=-11, X=-100mm"]->Draw("");
			m_maph2DPlaneHits["Primary, PDG=11, X=-100mm"]->Draw("same");
			m_maph2DPlaneHits["Primary, PDG=22, X=-100mm"]->Draw("same");
			DrawTitle("Primary, X=-100mm");
		}
	}
	else{
		if(padnb==5)	{hETotExitPipe->Draw("hbar");}	
		if(padnb==6)	{hETotEntersZDD->Draw("hbar");}	
		if(padnb==7)	{hEDepZDD->Draw("hbar");}
		if(padnb==8)	{
			char strltx[256];
			vector<TLatex*> vltx;

			double Xltx=0.25;
			double Xstep=0.05;
			double Yltx=0.1;

			vltx.push_back(new TLatex());
			vltx.back()->SetNDC();
			//sprintf(strltx,"event #%d",m_evtID);
			vltx.back()->SetTextAngle(90.);
			vltx.back()->DrawLatex(Xltx,Yltx,strltx);

			vltx.push_back(new TLatex());
			vltx.back()->SetNDC();
			sprintf(strltx,"E_{primaries} ZDD = %f",eTotPrimaries);
			vltx.back()->SetTextAngle(90.);
			Xltx=Xltx+Xstep;
			vltx.back()->DrawLatex(Xltx,Yltx,strltx);

			vltx.push_back(new TLatex());
			vltx.back()->SetNDC();
			sprintf(strltx,"N exit pipe = %d E_{Tot} exit pipe = %f",nExitWindow,eTotExitPipe);
			vltx.back()->SetTextAngle(90.);
			Xltx=Xltx+Xstep;
			vltx.back()->DrawLatex(Xltx,Yltx,strltx);

			vltx.push_back(new TLatex());
			vltx.back()->SetNDC();
			sprintf(strltx,"N enters ZDD = %d E_{Tot} enters ZDD = %f",nHitsZDD,eTotEntersZDD);
			vltx.back()->SetTextAngle(90.);
			Xltx=Xltx+Xstep;
			vltx.back()->DrawLatex(Xltx,Yltx,strltx);

			vltx.push_back(new TLatex());
			vltx.back()->SetNDC();
			sprintf(strltx,"E_{Dep} ZDD = %f",m_zdd_energy);
			vltx.back()->SetTextAngle(90.);
			Xltx=Xltx+Xstep;
			vltx.back()->DrawLatex(Xltx,Yltx,strltx);
		}
	}
}

c1->Print(outname.c_str());

/*
TImage *img = TImage::Create();
TCanvas *c2 = new TCanvas("c2","",1000.,2000.);
img->FromPad(c1);
img->Flip(90);
img->Draw("");
c2->Print(outname.c_str());
*/
		
}

void AnaBesSteps(string inputname, string option, string outpath, int NEntries=-1)
{

gStyle->SetLabelSize(0.030, "x");
gStyle->SetLabelSize(0.030, "y");
gStyle->SetLabelSize(0.030, "z");

gStyle->SetTitleSize(0.045, "x");
gStyle->SetTitleSize(0.045, "y");
gStyle->SetTitleSize(0.045, "z");
	 	 
m_option=option;

TDatabasePDG *pdg = new TDatabasePDG();

m_mapColor[-11] = 2;
m_mapColor[11] = 4;
m_mapColor[22] = 3;

gROOT->SetBatch();

if(inputname.find(".root") != string::npos){
	TFile* file = TFile::Open(inputname.c_str());
	m_tree= (TTree*) file->Get("Impact_reco");
}
else if(inputname.find(".txt") != string::npos){
	TChain* chain = new TChain("Impact_reco");
	ifstream readList(inputname.c_str());
	if(readList.is_open()){
		while(readList.good()){
		string strline;
		getline(readList,strline);
		chain->Add(strline.c_str());
		}
	}
m_tree = (TTree*) chain;
}


/*
m_file = TFile::Open(inputname.c_str());
m_tree = (TTree*) m_file->Get("Impact_reco");
*/

m_tree->SetBranchAddress("evtID",&m_evtID);
m_tree->SetBranchAddress("zdd_energy",&m_zdd_energy);
m_tree->SetBranchAddress("zddLeft_energy",&m_zddLeft_energy);
m_tree->SetBranchAddress("zddRight_energy",&m_zddRight_energy);

m_tree->SetBranchAddress("trackIDStep",&m_vectrackIDStep,&m_bvectrackIDStep );
m_tree->SetBranchAddress("parentIDStep",&m_vecparentIDStep,&m_bvecparentIDStep );
m_tree->SetBranchAddress("PDGCodeStep",&m_vecPDGCodeStep,&m_bvecPDGCodeStep );

m_tree->SetBranchAddress("xPreStep",&m_vecXPreStep,&m_bvecXPreStep);
m_tree->SetBranchAddress("yPreStep",&m_vecYPreStep,&m_bvecYPreStep);
m_tree->SetBranchAddress("zPreStep",&m_vecZPreStep,&m_bvecZPreStep);

m_tree->SetBranchAddress("xPostStep",&m_vecXPostStep,&m_bvecXPostStep);
m_tree->SetBranchAddress("yPosttep",&m_vecYPostStep,&m_bvecYPostStep);
m_tree->SetBranchAddress("zPostStep",&m_vecZPostStep,&m_bvecZPostStep);

m_tree->SetBranchAddress("PxPreStep",&m_vecPxPreStep,&m_bvecPxPreStep);
m_tree->SetBranchAddress("PyPreStep",&m_vecPyPreStep,&m_bvecPyPreStep);
m_tree->SetBranchAddress("PzPreStep",&m_vecPzPreStep,&m_bvecPzPreStep);

m_tree->SetBranchAddress("PxPostStep",&m_vecPxPostStep,&m_bvecPxPostStep);
m_tree->SetBranchAddress("PyPostStep",&m_vecPyPostStep,&m_bvecPyPostStep);
m_tree->SetBranchAddress("PzPostStep",&m_vecPzPostStep,&m_bvecPzPostStep);

m_tree->SetBranchAddress("VolumePreStep",&m_vecVolPreStep,&m_bvecVolPreStep);
m_tree->SetBranchAddress("VolumePostStep",&m_vecVolPostStep,&m_bvecVolPostStep);

m_tree->SetBranchAddress("ETotPreStep",&m_vecETotPreStep,&m_bvecETotPreStep);
m_tree->SetBranchAddress("ETotPostStep",&m_vecETotPostStep,&m_bvecETotPostStep);

m_tree->SetBranchAddress("eDepStep",&m_vecEDepStep,&m_bvecEDepStep);

m_outpath=outpath;
string outname=outpath+".root";
TFile* outfile = new TFile(outname.c_str(),"RECREATE");

string outps = outpath+".ps";
string outpdf = outpath+".pdf";

char cmd[256];
sprintf(cmd,"rm %s.gif",outpath.c_str());
system(cmd);

hNHitsZDD = new TH1D("hNHitsZDD",";Number of hits on ZDD;",100,0.,100.);
hNHitsGammasZDD = new TH1D("hNHitsGammasZDD",";Number of #gamma hits on ZDD;",100,0.,100.);
hNHitsElectronsZDD = new TH1D("hNHitsElectronsZDD",";Number of e^{-} hits on ZDD;",100,0.,100.);
hNHitsPositronsZDD = new TH1D("hNHitsPositronsZDD",";Number of e^{+} hits on ZDD;",100,0.,100.);
hNHitsZDDEast = new TH1D("hNHitsZDDEast",";Number of hits on ZDD East;",100,0.,100.);
hNHitsZDDWest = new TH1D("hNHitsZDDWest",";Number of hits on ZDD West;",100,0.,100.);

hNHitsPrimaryZDD = new TH1D("hNHitsPrimaryZDD","",100,0.,100.);
hNHitsPrimaryZDDEast = new TH1D("hNHitsPrimaryZDDEast","",100,0.,100.);
hNHitsPrimaryZDDWest = new TH1D("hNHitsPrimaryZDDWest","",100,0.,100.);

hPDGHitsZDD = new TH1D("hPDGHitsZDD",";PDG Code;",200.,-100.,100.);
hPDGHitsZDDEast = new TH1D("hPDGHitsZDDEast",";PDG Code;",200.,-100.,100.);
hPDGHitsZDDWest = new TH1D("hPDGHitsZDDWest",";PDG Code;",200.,-100.,100.);

hPDGHitsPrimaryZDD = new TH1D("hPDGHitsPrimaryZDD",";PDG Code;",200.,-100.,100.);
hPDGHitsPrimaryZDDEast = new TH1D("hPDGHitsPrimaryZDDEast",";PDG Code;",200.,-100.,100.);
hPDGHitsPrimaryZDDWest = new TH1D("hPDGHitsPrimaryZDDWest",";PDG Code;",200.,-100.,100.);

hETotPrimaries = new TH1D("hETotPrimaries",";E^{IP}_{Primary particles}(MeV);",256,0.,2000.);
hETotEntersZDD = new TH1D("hETotEntersZDD",";E^{ZDD front}_{Tot}(MeV);",256,0.,2000.);
hETotEntersZDD->SetFillColor(1);
hEGammasEntersZDDOverETotEntersZDD = new TH1D("hEGammasEntersZDDOverETotEntersZDD",";E^{ZDD front}_{#gamma}(MeV);",256,0.,2.);
hEElectronsEntersZDDOverETotEntersZDD = new TH1D("hEElectronsEntersZDDOverETotEntersZDD",";E^{ZDD front}_{e-}(MeV);",256,0.,2.);
hEPositronsEntersZDDOverETotEntersZDD = new TH1D("hEPositronsEntersZDDOverETotEntersZDD",";E^{ZDD front}_{e+}(MeV);",256,0.,2.);
hETotExitPipe = new TH1D("hETotExitPipe",";E^{Pipe exit}_{Tot};",256,0.,2000.);
hETotExitPipe->SetFillColor(1);
hEDepZDD = new TH1D("hEDepZDD",";E^{ZDD}_{Dep};",256,0.,2000.);
hEDepZDD->SetFillColor(1);

hETotEntersZDDOverETotPrimaries = new TH1D("hETotEntersZDDOverETotPrimaries",";E^{ZDD front}_{Tot}/E^{IP}_{Primary particles};",256,0.,2.);
TH1D* hEGammasEntersZDDOverETotPrimaries = new TH1D("hEGammasEntersZDDOverETotPrimaries",";E^{ZDD front}_{#gamma}/E^{IP}_{Primary particles};",256,0.,2.);
TH1D* hEElectronsEntersZDDOverETotPrimaries = new TH1D("hEElectronsEntersZDDOverETotPrimaries",";E^{ZDD front}_{e-}/E^{IP}_{Primary particles};",256,0.,2.);
TH1D* hEPositronsEntersZDDOverETotPrimaries= new TH1D("hEPositronsEntersZDDOverETotPrimaries",";E^{ZDD front}_{e+}/E^{IP}_{Primary particles};",256,0.,2.);
TH1D* hEElectronsPositronsEntersZDDOverETotPrimaries = new TH1D("hEElectronsPositronsEntersZDDOverETotPrimaries",";E^{ZDD front}_{e+ e-}/E^{IP}_{Primary particles};",256,0.,2.);

hEDepZDDOverETotPrimaries = new TH1D("hEDepZDDOverETotPrimaries",";E^{ZDD}_{Dep}/E^{IP}_{Primary particles};",256,0.,2.);

hEDepZDDOverETotEntersZDD = new TH1D("hEDepZDDOverETotEntersZDD",";E^{ZDD}_{Dep}/E^{ZDD front}_{Tot};",256,0.,2.);
//hETotEntersZDDOverETotExitPipe = new TH1D("hETotEntersZDDOverETotExitPipe",";;",256,0.,100.);
hETotEntersZDDOverETotExitPipe = new TH1D("hETotEntersZDDOverETotExitPipe",";E^{ZDD front}_{Tot}/E^{Pipe exit}_{Tot};",256,0.,2.);

for(int i=0;i<3;i++){
	
	int PDGcode;
	if(i==0) PDGcode=-11;
	if(i==1) PDGcode=11;
	if(i==2) PDGcode=22;
	
	std::map<string,double> m_mapPosPlane;
	
	m_mapPosPlane["X=-200mm"]=-200;
	m_mapPosPlane["X=-100mm"]=-100;
	m_mapPosPlane["X=-50mm"]=-50;
	m_mapPosPlane["X=-10mm"]=-10;
	m_mapPosPlane["X=-5mm"]=-5;
	m_mapPosPlane["X=-1mm"]=-1;
	m_mapPosPlane["X=0mm"]=0;
	m_mapPosPlane["X=1mm"]=1;
	m_mapPosPlane["X=5mm"]=5;
	m_mapPosPlane["X=10mm"]=10;
	m_mapPosPlane["X=50mm"]=50;
	m_mapPosPlane["X=100mm"]=100;
	m_mapPosPlane["X=200mm"]=200;
	
	m_mapPosPlane["Z=-3490mm"]=-3490; //ZDD East end
	m_mapPosPlane["Z=-3350mm"]=-3350; //ZDD East start
	m_mapPosPlane["Z=-2900mm"]=-2900; //ISPB East end
	m_mapPosPlane["Z=-1000mm"]=-1000;
	m_mapPosPlane["Z=-100mm"]=-100;
	m_mapPosPlane["Z=0mm"]=0;
	m_mapPosPlane["Z=100mm"]=100;
	m_mapPosPlane["Z=1000mm"]=1000;
	m_mapPosPlane["Z=2900mm"]=2900; //ISPB West end
	m_mapPosPlane["Z=3350mm"]=3350; //ZDD West start
	m_mapPosPlane["Z=3490mm"]=3490; //ZDD West end
	
	for(std::map<string,double>::iterator it=m_mapPosPlane.begin(); it!=m_mapPosPlane.end(); ++it){
		string keyMapPosPlane=it->first;
		string key="Primary, PDG="+intToStr(PDGcode)+", "+keyMapPosPlane;
		string ntName = "NT"+key;
		m_mapHitsNTuples[key]= new TTree(ntName.c_str(),"");
		
		if(keyMapPosPlane.find("X=")!=string::npos){
			//TVector3 v(it->second,0,0);
			//ROOT::Math::Plane3D::Vector n=makeUnitVector(v);
			
			ROOT::Math::Plane3D::Vector n(1,0,0);
			ROOT::Math::Plane3D::Point p(it->second,0,0);
			
			m_mapPlanes[key] = ROOT::Math::Plane3D(n,p);
			
			m_maph2DPlaneHits[key] = new TH2D(key.c_str(),key.c_str(),(m_WorldZMax-m_WorldZMin),m_WorldZMin,m_WorldZMax,(m_WorldYMax-m_WorldYMin),m_WorldYMin,m_WorldYMax);
			m_maph2DPlaneHits[key]->GetXaxis()->SetTitle("Z(mm)");
			m_maph2DPlaneHits[key]->GetYaxis()->SetTitle("Y(mm)");
			m_maph2DPlaneHits[key]->SetLineColor(m_mapColor[PDGcode]);
			m_maph2DPlaneHits[key]->SetMarkerStyle(7);
			m_maph2DPlaneHits[key]->SetMarkerColor(m_mapColor[PDGcode]);	
			
			m_mapHitsNTuples[key]->Branch("NIntersectPoints",&m_NIntersectPoints,"m_NIntersectPoints/I");
			m_mapHitsNTuples[key]->Branch("zImpacts",&m_zImpacts);
			m_mapHitsNTuples[key]->Branch("yImpacts",&m_yImpacts);
			m_mapHitsNTuples[key]->Branch("ThetaImpacts",&m_ThetaImpacts);
			m_mapHitsNTuples[key]->Branch("PhiImpacts",&m_PhiImpacts);

		}
		else if(keyMapPosPlane.find("Z=")!=string::npos){
			ROOT::Math::Plane3D::Vector n(0,0,1);
			ROOT::Math::Plane3D::Point p(0,0,it->second);
			m_mapPlanes[key] = ROOT::Math::Plane3D(n,p);
			
			m_maph2DPlaneHits[key] = new TH2D(key.c_str(),key.c_str(),m_WorldXMax-m_WorldXMin,m_WorldXMin,m_WorldXMax,m_WorldYMax-m_WorldYMin,m_WorldYMin,m_WorldYMax);
			m_maph2DPlaneHits[key]->GetXaxis()->SetTitle("X(mm)");
			m_maph2DPlaneHits[key]->GetYaxis()->SetTitle("Y(mm)");
			m_maph2DPlaneHits[key]->SetMarkerStyle(7);
			m_maph2DPlaneHits[key]->SetMarkerColor(m_mapColor[PDGcode]);
			
			m_mapHitsNTuples[key]->Branch("NIntersectPoints",&m_NIntersectPoints,"m_NIntersectPoints/I");
			m_mapHitsNTuples[key]->Branch("xImpacts",&m_xImpacts);
			m_mapHitsNTuples[key]->Branch("yImpacts",&m_yImpacts);
			m_mapHitsNTuples[key]->Branch("ThetaImpacts",&m_ThetaImpacts);
			m_mapHitsNTuples[key]->Branch("PhiImpacts",&m_PhiImpacts);
		}
	}
}



if(m_option.find("ParticleGun ") != std::string::npos)
	{
	TH1D* hEPGunIP = new TH1D("hEPGunIP","",1000,0.,2000.);
	}

if(NEntries==-1) NEntries=m_tree->GetEntries();

for(int entry=0;entry<NEntries;entry++){
	cout << "Event#" << entry << endl;
	m_tree->GetEntry(entry);
	
	Initialize();
	
	eTotPrimaries=0;
	eTotExitPipe=0;
	eTotEntersZDD=0;
	eGammasEntersZDD=0;
	eElectronsEntersZDD=0.;
	ePositronsEntersZDD=0.;
	eElectronsPositronsEntersZDD=0;
	
	eDepBeforeZDD=0;
	
	nHitsZDD=0;
	nHitsGammasZDD=0;
	nHitsElectronsZDD=0;
	nHitsPositronsZDD=0;
	nHitsZDDEast=0;
	nHitsZDDWest=0;
	
	nHitsPrimaryZDD=0;
	nHitsPrimaryZDDEast=0;
	nHitsPrimaryZDDWest=0;
	
	nExitWindow=0;
	
	//printf("evtID = %d \n",m_evtID);
	//printf("evtID = %d m_vecXPreStep->size() = %d m_zdd_energy = %f \n",m_evtID, m_vecXPreStep->size(),m_zdd_energy);
	for(int step=0;step<m_vecXPreStep->size();step++){
		//printf("m_vecXPreStep->at(step) = %f (m_vecVolPostStep->at(step)).c_str() = %s \n",m_vecXPreStep->at(step),(m_vecVolPostStep->at(step)).c_str());
		bool changeTrack= step>0 && m_vectrackIDStep->at(step-1)!=m_vectrackIDStep->at(step);
		int trackID = m_vectrackIDStep->at(step);
		int parentID = m_vecparentIDStep->at(step);
		int PDG = m_vecPDGCodeStep->at(step);	
		if(pdg->GetParticle(PDG)==NULL) continue; //PDG=1000822050 (Pb205) and 1000070150 (N15) appears in NTuple and is not included in TDatabasePDG
		double mass = 	pdg->GetParticle(PDG)->Mass()*1e3;	

		TVector3 vPreStep(m_vecXPreStep->at(step),m_vecYPreStep->at(step),m_vecZPreStep->at(step));	
		TVector3 vPostStep(m_vecXPostStep->at(step),m_vecYPostStep->at(step),m_vecZPostStep->at(step));
		TVector3 vPreStepMom(m_vecPxPreStep->at(step),m_vecPyPreStep->at(step),m_vecPzPreStep->at(step));
		TLorentzVector lvPreStep;
		lvPreStep.SetVectM(vPreStepMom,mass);
		string volPreStep = m_vecVolPreStep->at(step);
		string volPostStep = m_vecVolPostStep->at(step);

		double ETotPreStep = m_vecETotPreStep->at(step);
		double ETotPostStep = m_vecETotPostStep->at(step);

		double EDepStep = m_vecEDepStep->at(step);

		bool isPrimary = parentID==0;
		//bool fromIP=vPreStep.x()==0. && vPreStep.y()==0. && vPreStep.z()==0.;
		bool fromIP=step==0 && vPreStep.x()==0. && vPreStep.y()==0. && vPreStep.z()==0.;

		if(isPrimary && fromIP)
			{
				//eTotPrimaries=eTotPrimaries+lvPreStep.E();
				if(m_option.find("BBBREM ") != std::string::npos){
					if(PDG==22 || PDG==11) eTotPrimaries=eTotPrimaries+ETotPreStep;					
					}				
				else eTotPrimaries=eTotPrimaries+ETotPreStep ;
				//m_maplvPrimaries[trackID];
			}
		
		
		/*
		bool preStepInTwoBeamPipe = volPreStep=="physicalTwoBeamPipe" || volPreStep=="physicalTwoBeamPipe1"
		|| volPreStep=="phycical_endplate_twoholes_square1" || volPreStep=="phycical_endplate_twoholes_square2"
		|| volPreStep=="phycical_endplate_twoholes_arc1" || volPreStep=="phycical_endplate_twoholes_arc2" || volPreStep=="phycical_endplate_twoholes_arc3" || volPreStep=="phycical_endplate_twoholes_arc4"
		||volPreStep=="phycical_endplate_twoholes_tshape1" || volPreStep=="phycical_endplate_twoholes_tshape2"
		;
		*/

		//bool preStepInBeamWindow = volPreStep=="physical_window";
		//bool preStepInBeamWindow = volPreStep=="physical_window" || volPreStep=="physicalTwoBeamPipe" || volPreStep=="physicalTwoBeamPipe1";  
		//bool preStepInBeamWindow = volPreStep=="physical_window" || volPreStep=="physical_outgoing_top" || volPreStep=="physical_outgoing_bottom";
		bool preStepInBeamWindow = volPreStep=="physical_window" || volPreStep=="physical_outgoing_top" || volPreStep=="physical_outgoing_bottom" || volPreStep=="physical_prewindow" || volPreStep=="physical_afterwindow" || volPreStep=="physical_ispb";

		bool exitWindow = preStepInBeamWindow && volPreStep!=volPostStep;
		if(exitWindow)
		//if(volPreStep!=volPostStep && (volPreStep=="physical_window" || volPreStep=="physicalTwoBeamPipe" || volPreStep=="physicalTwoBeamPipe1")&& fabs(vPreStep.Z())>3000.)
			{
			nExitWindow++;
			eTotExitPipe=eTotExitPipe+lvPreStep.E();
			}

		if(vPostStep.Z()<3349.95) eDepBeforeZDD=eDepBeforeZDD+EDepStep;

		bool postStepInZDD=vPostStep.Z()>=3350. && vPostStep.Z()<=3490. && vPostStep.X()>=0 && vPostStep.X()<=40. && fabs(vPostStep.Y())>=5 && fabs(vPostStep.Y())<=35;

		//bool entersZDD = volPreStep=="physicWorld" && (volPreStep!=volPostStep);
		//bool entersZDD = volPreStep=="physicWorld" && volPostStep.find("crystal_") != std::string::npos;
		//bool entersZDD = volPreStep=="physicWorld" && m_zdd_energy>0;
		//bool entersZDD = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && volPostStep.find("physicZdd") == std::string::npos && m_zdd_energy>0;
		//bool entersZDD = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && vPostStep.Z()<3350.0005 && m_zdd_energy>0;
		//bool entersZDD = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && fabs(vPostStep.Z())<3350.0005 && m_zdd_energy>0;			
		//bool entersZDD = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && fabs(vPreStep.Z())<3349.995 && m_zdd_energy>0;

		bool entersZDD = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && fabs(vPreStep.Z())<3350.000 && m_zdd_energy>0;

		//bool entersZDD = volPreStep!=volPostStep && (volPostStep.find("crystal_") || volPostStep.find("physicZdd") ) && fabs(vPreStep.Z())<3350. && m_zdd_energy>0;			
		//bool entersZDD = volPreStep!=volPostStep && (volPostStep.find("crystal_")) &&  postStepInZDD && m_zdd_energy>0;			
		//bool entersZDD = vPostStep.Z()>=3300. && vPostStep.Z()<3490. && vPostStep.X()>=0 && vPostStep.X()<=40. && fabs(vPostStep.Y())>=5 && fabs(vPostStep.Y())<=35;

		if(entersZDD) 
			{nHitsZDD++;
			//eTotEntersZDD=eTotEntersZDD+lvPreStep.E();
			eTotEntersZDD=eTotEntersZDD+ETotPreStep;				
			hPDGHitsZDD->Fill(PDG);

			if(PDG==22) {nHitsGammasZDD++;eGammasEntersZDD=eGammasEntersZDD+lvPreStep.E();}
			if(PDG==11) {nHitsElectronsZDD++;eElectronsEntersZDD=eElectronsEntersZDD+lvPreStep.E();}
			if(PDG==-11) {nHitsPositronsZDD++;ePositronsEntersZDD=ePositronsEntersZDD+lvPreStep.E();}
			if(PDG==11 || PDG==-11){eElectronsPositronsEntersZDD=eElectronsPositronsEntersZDD+lvPreStep.E();}
			//eElectronsPositronsEntersZDD=eElectronsPositronsEntersZDD+(eElectronsEntersZDD+ePositronsEntersZDD);

			if(isPrimary) 
				{nHitsPrimaryZDD++;
				hPDGHitsPrimaryZDD->Fill(PDG);
				}
			}

		//bool entersZDDEast = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && vPostStep.Z()>-3350.0005 && m_zddLeft_energy>0;
		bool entersZDDEast= entersZDD && vPreStep.Z()>-3350.000 && m_zddLeft_energy>0;

		if(entersZDDEast)
			{
			nHitsZDDEast++;

			hPDGHitsZDDEast->Fill(PDG);	
			if(isPrimary) 
				{nHitsPrimaryZDDEast++;
				hPDGHitsPrimaryZDDEast->Fill(PDG);
				}
			}

		//bool entersZDDWest = volPreStep!=volPostStep && volPostStep.find("crystal_") != std::string::npos && vPostStep.Z()<3350.0005 && m_zddRight_energy>0;
		bool entersZDDWest = entersZDD && vPreStep.Z()<3350.000 && m_zddRight_energy>0;

		if(entersZDDWest)
			{
			nHitsZDDWest++;

			hPDGHitsZDDWest->Fill(PDG);	
			if(isPrimary) 
				{nHitsPrimaryZDDWest++;
				hPDGHitsPrimaryZDDWest->Fill(PDG);
				}
			}
		
		//Complete the previous trajectory with its last point at postStep
		if(changeTrack){
			int prevTrackID=m_vectrackIDStep->at(step-1);
			TVector3 vPrevPostStep(m_vecXPostStep->at(step-1),m_vecYPostStep->at(step-1),m_vecZPostStep->at(step-1));
			TVector3 vPrevPostStepMom(m_vecPxPostStep->at(step-1),m_vecPyPostStep->at(step-1),m_vecPzPostStep->at(step-1));
			
			bool inWorld=	(vPrevPostStep.X()>=m_WorldXMin && vPrevPostStep.X()<=m_WorldXMax)
			&& (vPrevPostStep.Y()>=m_WorldYMin && vPrevPostStep.Y()<=m_WorldYMax)
			&& (vPrevPostStep.Z()>=m_WorldZMin && vPrevPostStep.Z()<=m_WorldZMax)
			; 
		
			if(inWorld){
				m_mapTrackPoints[prevTrackID].push_back(vPrevPostStep);
				m_mapTrackMomentum[prevTrackID].push_back(vPrevPostStepMom);
			}
		}
		
		bool inWorld=	(vPreStep.X()>=m_WorldXMin && vPreStep.X()<=m_WorldXMax)
		&& (vPreStep.Y()>=m_WorldYMin && vPreStep.Y()<=m_WorldYMax)
		&& (vPreStep.Z()>=m_WorldZMin && vPreStep.Z()<=m_WorldZMax)
		; 

		if(inWorld)
			{
			//AddPointToTrajectory(trackID,vPreStep);
			m_mapTrackPoints[trackID].push_back(vPreStep);
			m_mapTrackMomentum[trackID].push_back(lvPreStep.Vect());
			m_mapTrackPDG[trackID]=PDG;
			m_mapTrackPrimary[trackID]=isPrimary;
			}
		
		
		/*
		//Check if last point of trajectory 
		TVector3 pointTraj(0,0,0);
		TVector3 momTraj(0,0,0);
		if(changeTrack){
			pointTraj=TVector3(m_vecXPostStep->at(step-1),m_vecYPostStep->at(step-1),m_vecZPostStep->at(step-1));
		}
		else {
			pointTraj=vPreStep;
			momTraj=vPreStepMom;
		}
		
		bool inWorld=	(vPreStep.X()>=m_WorldXMin && vPreStep.X()<=m_WorldXMax)
		&& (vPreStep.Y()>=m_WorldYMin && vPreStep.Y()<=m_WorldYMax)
		&& (vPreStep.Z()>=m_WorldZMin && vPreStep.Z()<=m_WorldZMax)
		; 
		*/
	}
		
		double Abs3MomBeam = sqrt((pow(eTotPrimaries,2)/2-pow(m_dMasselec,2))/(1-cos(TMath::Pi()-m_dCrossingAngle)));
		double TotEngBeam = sqrt(pow(m_dMasselec,2)+pow(Abs3MomBeam,2));
		
		//cout << "m_mapTrackPoints.size()" << m_mapTrackPoints.size() << endl;
		for (std::map<int,std::vector<TVector3> > ::iterator it=m_mapTrackPoints.begin(); it!=m_mapTrackPoints.end(); ++it)
		{
			int trackIndex = it->first;
			std::vector<TVector3> traj=it->second;
			
			if(m_option.find("HorizontalZ ") != std::string::npos) for(int k=0;k<traj.size();k++) traj[k].RotateX(-TMath::Pi()/2);
			
			TPolyLine3D poly3D = MakeTPolyLine3D(traj);
						
			m_vTrajectories.push_back(poly3D);		
			m_vTrajPDGs.push_back(m_mapTrackPDG[trackIndex]);
			m_vTrajPrimary.push_back(m_mapTrackPrimary[trackIndex]);
			
			TVector3 momIni = m_mapTrackMomentum[trackIndex][0];			
			double mass=pdg->GetParticle(m_vTrajPDGs.back())->Mass()*1e3;
			printf("momIni.Mag() %f  mass %f \n",momIni.Mag(),mass);
			TLorentzVector lv;
			lv.SetVectM(momIni,mass);
			m_vIniLV.push_back(lv);
			
			//double x[2]={0,100};
			//double y[2]={0,0};
			//double z[2]={0,0};
			//TPolyLine3D poly3D(2,x,y,z);
			
			//double x[2]={0,0};
			//double y[2]={0,0};
			//double z[2]={0,3500};
			//TPolyLine3D poly3D(2,x,y,z);
			
			/*
			TPolyLine3D* poly3D;
			if(m_mapTrackPDG[trackIndex]==-11){
			//double x[2]={0,0};double y[2]={0,0};double z[2]={0,3500};
			double x[2]={0,-100};double y[2]={0,0};double z[2]={0,3500};
			poly3D= new TPolyLine3D(2,x,y,z);
			}
			else if(m_mapTrackPDG[trackIndex]==11){
			//double x[2]={0,0};double y[2]={0,0};double z[2]={0,-3500};
			double x[2]={0,-100};double y[2]={0,0};double z[2]={0,-3500};
			poly3D= new TPolyLine3D(2,x,y,z);		
			}
			else{
			//double x[2]={0,0};double y[2]={0,0};double z[2]={0,0};
			double x[2]={0,-100};double y[2]={0,0};double z[2]={0,0};
			poly3D= new TPolyLine3D(2,x,y,z);			
			}
			m_vTrajectories.push_back(*poly3D);	
			m_vTrajPDGs.push_back(m_mapTrackPDG[trackIndex]);
			m_vTrajPrimary.push_back(m_mapTrackPrimary[trackIndex]);
			*/
			
			//cout << "traj.size() " << traj.size() << endl;
			//cout << "m_vTrajPDGs.back() " << m_vTrajPDGs.back() << endl;
			//cout << "m_vTrajPrimary.back() " << m_vTrajPrimary.back() << endl;			
		}
		
		
		/*	
		for(int i=0;i<m_vTrajectories.size();i++){
			if(m_vTrajPrimary[i]!=1) continue;
			//if(m_vTrajPDGs[i]!=-11) continue;
			//if(m_vTrajPDGs[i]!=11) continue;
			cout << "m_vTrajPDGs[i]= " << m_vTrajPDGs[i] << endl;
			//cout << "m_vTrajPrimary[i]= " << m_vTrajPrimary[i] << endl;
			
			//cout << m_vTrajPDGs[i] << endl;
			TPolyLine3D poly3D=m_vTrajectories[i];
			PrintTrajectory(poly3D);
			
			for(map<string,ROOT::Math::Plane3D>::iterator it=m_mapPlanes.begin(); it!=m_mapPlanes.end(); ++it){
				string key = it->first;				
				string PDGcondition="PDG="+intToStr(m_vTrajPDGs[i]);
				//cout << key << endl;
				//cout << PDGcondition << endl;
				if(!(key.find(PDGcondition)!=string::npos)) continue;
				//cout << "Condition passed" << endl;				
				ROOT::Math::Plane3D plane3D=it->second;
				vector<TVector3> vIntersectPoints = getIntersectPolyLinePlane(plane3D,poly3D);
				
				if(vIntersectPoints.size()==0) continue;				
				cout << key << endl;
				cout << "vIntersectPoints.size() " << vIntersectPoints.size() << endl;
				Print(vIntersectPoints);
				//m_NIntersectPoints=vIntersectPoints.size();
				for(int j=0;j<vIntersectPoints.size();j++){
					if(key.find("X=")!=string::npos){
						m_maph2DPlaneHits[key]->Fill(vIntersectPoints[j].Z(),vIntersectPoints[j].Y());
						
						//m_zImpacts.push_back(vIntersectPoints[j].Z());
						//m_yImpacts.push_back(vIntersectPoints[j].Y());
					}
					else if (key.find("Z=")!=string::npos){
						m_maph2DPlaneHits[key]->Fill(vIntersectPoints[j].X(),vIntersectPoints[j].Y());
						//m_xImpacts.push_back(vIntersectPoints[j].X());
						//m_yImpacts.push_back(vIntersectPoints[j].Y());
					}
				}
				
				//m_mapHitsNTuples[key]->Fill();
				//m_xImpacts.clear();
				//m_yImpacts.clear();
				//m_zImpacts.clear();	
			}
		}
		*/
		
		for(map<string,ROOT::Math::Plane3D>::iterator it=m_mapPlanes.begin(); it!=m_mapPlanes.end(); ++it){
			string key = it->first;				
			for(int i=0;i<m_vTrajectories.size();i++){
				if(m_vTrajPrimary[i]!=1) continue;
				string PDGcondition="PDG="+intToStr(m_vTrajPDGs[i]);
				if(!(key.find(PDGcondition)!=string::npos)) continue;
				TPolyLine3D poly3D=m_vTrajectories[i];
				
				ROOT::Math::Plane3D plane3D=it->second;
				vector<TVector3> vIntersectPoints = getIntersectPolyLinePlane(plane3D,poly3D);
				
				if(vIntersectPoints.size()==0) continue;				
				cout << key << endl;
				cout << "vIntersectPoints.size() " << vIntersectPoints.size() << endl;
				Print(vIntersectPoints);
				m_NIntersectPoints=vIntersectPoints.size();
				for(int j=0;j<vIntersectPoints.size();j++){
					if(key.find("X=")!=string::npos){
						m_maph2DPlaneHits[key]->Fill(vIntersectPoints[j].Z(),vIntersectPoints[j].Y());						
						m_zImpacts.push_back(vIntersectPoints[j].Z());
						m_yImpacts.push_back(vIntersectPoints[j].Y());
					}
					else if (key.find("Z=")!=string::npos){
						m_maph2DPlaneHits[key]->Fill(vIntersectPoints[j].X(),vIntersectPoints[j].Y());
						m_xImpacts.push_back(vIntersectPoints[j].X());
						m_yImpacts.push_back(vIntersectPoints[j].Y());
					}
					m_ThetaImpacts.push_back(m_vIniLV[i].Theta());
					m_PhiImpacts.push_back(Phi0to2Pi(m_vIniLV[i].Phi()));
				}				
			}
			
			m_mapHitsNTuples[key]->Fill();
			m_xImpacts.clear();
			m_yImpacts.clear();
			m_zImpacts.clear();	
			m_ThetaImpacts.clear();
			m_PhiImpacts.clear();	
		}
		
		
		//cout << endl;
		hETotPrimaries->Fill(eTotPrimaries);
		
		hNHitsZDD->Fill(nHitsZDD);
		hNHitsGammasZDD->Fill(nHitsGammasZDD);
		hNHitsElectronsZDD->Fill(nHitsElectronsZDD);
		hNHitsPositronsZDD->Fill(nHitsPositronsZDD);
		hNHitsZDDEast->Fill(nHitsZDDEast);	
		hNHitsZDDWest->Fill(nHitsZDDWest);
		
		hNHitsPrimaryZDD->Fill(nHitsPrimaryZDD);
		hNHitsPrimaryZDDEast->Fill(nHitsPrimaryZDDEast);
		hNHitsPrimaryZDDWest->Fill(nHitsPrimaryZDDWest);
		
		hETotExitPipe->Fill(eTotExitPipe);
		
		hETotEntersZDD->Fill(eTotEntersZDD);
		if(eTotEntersZDD>0)hEGammasEntersZDDOverETotEntersZDD->Fill(eGammasEntersZDD/eTotEntersZDD);
		if(eTotEntersZDD>0)hEElectronsEntersZDDOverETotEntersZDD->Fill(eElectronsEntersZDD/eTotEntersZDD);
		if(eTotEntersZDD>0)hEPositronsEntersZDDOverETotEntersZDD->Fill(ePositronsEntersZDD/eTotEntersZDD);
		hETotEntersZDDOverETotPrimaries->Fill(eTotEntersZDD/eTotPrimaries);
		
		hEGammasEntersZDDOverETotPrimaries->Fill(eGammasEntersZDD/eTotPrimaries);
		hEElectronsEntersZDDOverETotPrimaries->Fill(eElectronsEntersZDD/eTotPrimaries);
		hEPositronsEntersZDDOverETotPrimaries->Fill(ePositronsEntersZDD/eTotPrimaries);
		hEElectronsPositronsEntersZDDOverETotPrimaries->Fill(eElectronsPositronsEntersZDD/eTotPrimaries);
		
		if(eTotExitPipe>0) hETotEntersZDDOverETotExitPipe->Fill(eTotEntersZDD/eTotExitPipe);
		
		hEDepZDD->Fill(m_zdd_energy);
		hEDepZDDOverETotPrimaries->Fill(m_zdd_energy/eTotPrimaries);
		
		if(eTotEntersZDD>0) hEDepZDDOverETotEntersZDD ->Fill(m_zdd_energy/eTotEntersZDD);
		
	if(m_option.find("DrawTraj ") != std::string::npos)	
		{
		//if(entry==1) DrawTrajectories();
		gStyle->SetOptTitle(0);
		DrawTrajectories();
		}
	//cout << "Energy Deposited before ZDD = " << eDepBeforeZDD << endl;
	//cout << "Energy entering ZDD = "<<eTotEntersZDD << endl;
	//cout << "Energy deposited in ZDD = " << m_zdd_energy << endl;
	}

hETotPrimaries->Scale(100*1/((double)hETotPrimaries->GetEntries()));
hETotPrimaries->GetYaxis()->SetTitle("\%");

hPDGHitsZDD->Scale(100*1/((double)hPDGHitsZDD->GetEntries()));
hPDGHitsZDD->GetYaxis()->SetTitle("\%");

hNHitsZDD->Scale(100*1/((double)hNHitsZDD->GetEntries()));
hNHitsZDD->GetYaxis()->SetTitle("\%");

hNHitsGammasZDD->Scale(100*1/((double)hNHitsGammasZDD->GetEntries()));
hNHitsGammasZDD->GetYaxis()->SetTitle("\%");

hNHitsElectronsZDD->Scale(100*1/((double)hNHitsElectronsZDD->GetEntries()));
hNHitsElectronsZDD->GetYaxis()->SetTitle("\%");

hNHitsPositronsZDD->Scale(100*1/((double)hNHitsPositronsZDD->GetEntries()));
hNHitsPositronsZDD->GetYaxis()->SetTitle("\%");

hPDGHitsZDDEast->Scale(100*1/((double)hPDGHitsZDDEast->GetEntries()));
hPDGHitsZDDEast->GetYaxis()->SetTitle("\%");

hPDGHitsZDDWest->Scale(100*1/((double)hPDGHitsZDDWest->GetEntries()));
hPDGHitsZDDWest->GetYaxis()->SetTitle("\%");

	
hPDGHitsPrimaryZDD->Scale(100*1/((double)hPDGHitsPrimaryZDD->GetEntries()));
hPDGHitsPrimaryZDD->GetYaxis()->SetTitle("\%");

hPDGHitsPrimaryZDDEast->Scale(100*1/((double)hPDGHitsPrimaryZDDEast->GetEntries()));
hPDGHitsPrimaryZDDEast->GetYaxis()->SetTitle("\%");

hPDGHitsPrimaryZDDWest->Scale(100*1/((double)hPDGHitsPrimaryZDDWest->GetEntries()));
hPDGHitsPrimaryZDDWest->GetYaxis()->SetTitle("\%");

hETotExitPipe->Scale(100*1/((double)hETotExitPipe->GetEntries()));
hETotExitPipe->GetYaxis()->SetTitle("\%");

hETotEntersZDD->Scale(100*1/((double)hETotEntersZDD->GetEntries()));
hETotEntersZDD->GetYaxis()->SetTitle("\%");


hEGammasEntersZDDOverETotEntersZDD->Scale(100*1/((double)hEGammasEntersZDDOverETotEntersZDD->GetEntries()));
hEGammasEntersZDDOverETotEntersZDD->GetYaxis()->SetTitle("\%");

hEElectronsEntersZDDOverETotEntersZDD->Scale(100*1/((double)hEElectronsEntersZDDOverETotEntersZDD->GetEntries()));
hEElectronsEntersZDDOverETotEntersZDD->GetYaxis()->SetTitle("\%");

hEPositronsEntersZDDOverETotEntersZDD->Scale(100*1/((double)hEPositronsEntersZDDOverETotEntersZDD->GetEntries()));
hEPositronsEntersZDDOverETotEntersZDD->GetYaxis()->SetTitle("\%");

hETotEntersZDDOverETotPrimaries->Scale(100*1/((double)hETotEntersZDDOverETotPrimaries->GetEntries()));
hETotEntersZDDOverETotPrimaries->GetYaxis()->SetTitle("\%");

hEGammasEntersZDDOverETotPrimaries->Scale(100*1/((double)hEGammasEntersZDDOverETotPrimaries->GetEntries()));
hEElectronsEntersZDDOverETotPrimaries->Scale(100*1/((double)hEElectronsEntersZDDOverETotPrimaries->GetEntries()));
hEElectronsEntersZDDOverETotPrimaries->SetTitle("\%");
hEPositronsEntersZDDOverETotPrimaries->Scale(100*1/((double)hEPositronsEntersZDDOverETotPrimaries->GetEntries()));
hEPositronsEntersZDDOverETotPrimaries->SetTitle("\%");			hEElectronsPositronsEntersZDDOverETotPrimaries->Scale(100*1/((double)hEElectronsPositronsEntersZDDOverETotPrimaries->GetEntries()));
hEElectronsPositronsEntersZDDOverETotPrimaries->SetTitle("\%");      

hETotEntersZDDOverETotExitPipe->Scale(100*1/((double)hETotEntersZDDOverETotExitPipe->GetEntries()));
hETotEntersZDDOverETotExitPipe->GetYaxis()->SetTitle("\%");
		
hEDepZDD->Scale(100*1/((double)hEDepZDD->GetEntries()));
hEDepZDD->GetYaxis()->SetTitle("\%");		

hEDepZDDOverETotPrimaries->Scale(100*1/((double)hEDepZDDOverETotPrimaries->GetEntries()));
hEDepZDDOverETotPrimaries->GetYaxis()->SetTitle("\%");


//Print in .pdf file
gStyle->SetOptTitle(0);
gStyle->SetOptStat(0);
TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str()));	
c->Divide(2,3);
c->cd(1);
m_maph2DPlaneHits["Primary, PDG=-11, Z=-2900mm"]->Draw("");
m_maph2DPlaneHits["Primary, PDG=11, Z=-2900mm"]->Draw("same");
m_maph2DPlaneHits["Primary, PDG=22, Z=-2900mm"]->Draw("same");
DrawBes("Z=-2900mm");
DrawTitle("Primary, Z=-2900mm (ISPB East exit)");

c->cd(2);
m_maph2DPlaneHits["Primary, PDG=11, Z=-3350mm"]->Draw(" ");
m_maph2DPlaneHits["Primary, PDG=-11, Z=-3350mm"]->Draw("same");
m_maph2DPlaneHits["Primary, PDG=22, Z=-3350mm"]->Draw("same");
DrawBes("Z=-3350mm");
DrawTitle("Primary, Z=-3350mm (ZDD East entrance)");

c->cd(3);
m_maph2DPlaneHits["Primary, PDG=-11, Z=2900mm"]->Draw("");
m_maph2DPlaneHits["Primary, PDG=11, Z=2900mm"]->Draw("same");
m_maph2DPlaneHits["Primary, PDG=22, Z=2900mm"]->Draw("same");
DrawBes("Z=2900mm");
DrawTitle("Primary, Z=2900mm (ISPB West exit)");

c->cd(4);
m_maph2DPlaneHits["Primary, PDG=-11, Z=3350mm"]->Draw("");
m_maph2DPlaneHits["Primary, PDG=11, Z=3350mm"]->Draw("same");
m_maph2DPlaneHits["Primary, PDG=22, Z=3350mm"]->Draw("same");
DrawBes("Z=3350mm");
DrawTitle("Primary, Z=3350mm (ZDD West entrance)");

c->cd(5);
m_maph2DPlaneHits["Primary, PDG=-11, X=100mm"]->Draw("");
m_maph2DPlaneHits["Primary, PDG=-11, X=100mm"]->GetXaxis()->SetRangeUser(-3500,-2900);
m_maph2DPlaneHits["Primary, PDG=11, X=100mm"]->Draw("same");
DrawTitle("Primary, X=100mm, Z<-2900mm");

c->cd(6);

m_maph2DPlaneHits["Primary, PDG=-11, X=100mm"]->DrawClone("");
m_maph2DPlaneHits["Primary, PDG=-11, X=100mm"]->GetXaxis()->SetRangeUser(2900,3500);
m_maph2DPlaneHits["Primary, PDG=11, X=100mm"]->Draw("same");
DrawTitle("Primary, X=100mm, Z>2900mm");

c->Print(Form("%s",outps.c_str()));

c->Clear();
vector<double> limbinTheta;
vector<double> limbinPhi;

limbinTheta=makeLimBinVec(0.,0.3,4);
limbinPhi=makeLimBinVec(0.,2*TMath::Pi(),4);
c=makeHitDistThetaPhi("Primary, PDG=-11, Z=2900mm",limbinTheta,limbinPhi);
c->Print(Form("%s",outps.c_str()));

c->Clear();
limbinTheta=makeLimBinVec(TMath::Pi()-0.3,TMath::Pi(),4);
limbinPhi=makeLimBinVec(0.,2*TMath::Pi(),4);
c=makeHitDistThetaPhi("Primary, PDG=11, Z=-2900mm",limbinTheta,limbinPhi);
c->Print(Form("%s",outps.c_str()));

c->Print(Form("%s]",outps.c_str()));

//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
	
outfile->Write();

//Modify the .gif to make it loops
if(m_option.find("DrawTraj ") != std::string::npos)	{
//sprintf(cmd,"convert -loop 0 %s.gif %s.gif",outpath.c_str(),outpath.c_str());
sprintf(cmd,"convert -rotate 90 -loop 0 %s.gif %s.gif",outpath.c_str(),outpath.c_str());
//sprintf(cmd,"convert -rotate 90 %s.gif %s.gif",outpath.c_str(),outpath.c_str());
cout<< cmd << endl;
system(cmd);
}


}

void InitSystVect()
{

m_crystalList.resize(12);
m_crystalList[0]="104";
m_crystalList[1]="103";
m_crystalList[2]="102";
m_crystalList[3]="101";
m_crystalList[4]="114";
m_crystalList[5]="113";
m_crystalList[6]="112";
m_crystalList[7]="111";
m_crystalList[8]="124";
m_crystalList[9]="123";
m_crystalList[10]="122";
m_crystalList[11]="121";


/*
m_crystalList.resize(13);
m_crystalList[0]="0deg";
m_crystalList[1]="104";
m_crystalList[2]="103";
m_crystalList[3]="102";
m_crystalList[4]="101";
m_crystalList[5]="114";
m_crystalList[6]="113";
m_crystalList[7]="112";
m_crystalList[8]="111";
m_crystalList[9]="124";
m_crystalList[10]="123";
m_crystalList[11]="122";
m_crystalList[12]="121";
*/
m_EnergyList.resize(7);
m_EnergyList[0]="50";
m_EnergyList[1]="100";
m_EnergyList[2]="200";
m_EnergyList[3]="500";
m_EnergyList[4]="1000";
m_EnergyList[5]="1500";
m_EnergyList[6]="2000";

}

void MultiAnaBesSteps(string inputpath, string outpath)
{

InitSystVect();


for(int i=0;i<m_EnergyList.size();i++)
	{
	
	for(int j=0;j<m_crystalList.size();j++)
		{

		string infilename="Impact_reco_particleGun_"+m_crystalList[j]+"_"+m_EnergyList[i]+"MeV_Pipe.root";
		string infilepath=inputpath+"/"+infilename;
		cout<< infilepath << endl;
		
		string outfilename="AnaBesSteps_particleGun_"+m_crystalList[j]+"_"+m_EnergyList[i]+"MeV_Pipe";
		string outfilepath=outpath+"/"+outfilename;
		cout << outfilepath << endl;
		cout << endl;
		//AnaBesSteps(infilepath, "", outfilepath, 10);
		AnaBesSteps(infilepath, "", outfilepath);
		}
	}

}

TH2D* MakeTH2FromTH1Slices(std::vector<TH1D*> histos)
{
TH2D* h2D;
h2D = new TH2D((histos[0]->GetName()),"",histos.size(),0.,4.,histos[0]->GetNbinsX(),histos[0]->GetXaxis()->GetXmin(),histos[0]->GetXaxis()->GetXmax());

for(int k=0;k<histos.size();k++)
	{
	for(int biny=1;biny<=histos[k]->GetNbinsX();biny++)
		{
		//h2D->SetBinContent(k,biny,histos[k]->GetBinContent(biny));
		h2D->SetBinContent(k+1,biny,histos[k]->GetBinContent(biny));
		}
	h2D->GetXaxis()->SetBinLabel(k+1,histos[k]->GetTitle());
	}

return h2D;
}

void MergeAnaBesSteps(string inputpath, string outpath)
{

InitSystVect();

//std::map<std::vector<string>,std::vector<TH1D> > m_mapHistos;
string outname=outpath+".root";
TFile* outfile = new TFile(outname.c_str(),"RECREATE");
/*
for(int i=0;i<m_EnergyList.size();i++)
	{
	std::vector<TH1D*> m_mapHistos;
	for(int j=0;j<m_crystalList.size();j++)
		{
		string infilename=inputpath+"/AnaBesSteps_particleGun_"+m_crystalList[j]+"_"+m_EnergyList[i]+"MeV_Pipe.root";
		cout << infilename << endl;
		TFile* file = TFile::Open(infilename.c_str());
		if(file==NULL) continue;
		TH1D* histo=(TH1D*) file->Get("hEDepZDDOverETotEntersZDD");
		if(histo==NULL) continue;
		histo->SetTitle(m_crystalList[j].c_str());
		m_mapHistos.push_back(histo);	
		
		}
	outfile->mkdir(m_EnergyList[i].c_str());
	outfile->cd(m_EnergyList[i].c_str());	
	TH2D* h2D = MakeTH2FromTH1Slices(m_mapHistos);
	char hname[256];
	char htitle[256];
	//h2D->SetTitle(m_EnergyList[i].c_str());
	//outfile->cd();
	h2D->GetXaxis()->SetTitle("ZDD crystal #");
	h2D->GetYaxis()->SetTitle(m_mapHistos[0]->GetXaxis()->GetTitle());
	h2D->Write();
	outfile->cd();
	}
outfile->Close();
*/

int counter=0;
vector<const char*> vechname;
for(int i=0;i<m_EnergyList.size();i++)
	{
	vector< vector<TH1D*> > m_mapHistos;
	for(int j=0;j<m_crystalList.size();j++)
		{
		string infilename=inputpath+"/AnaBesSteps_particleGun_"+m_crystalList[j]+"_"+m_EnergyList[i]+"MeV_Pipe.root";
		cout << infilename << endl;
		TFile* file = TFile::Open(infilename.c_str());
		if(file==NULL) continue;
		
		vector<TH1D*> vechistos;
			TIter nextkey(file->GetListOfKeys());
			TKey *key;
			while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      //cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;				
			if (obj->InheritsFrom("TH1D")) {
			//TH1D* histo= (TH1D*)key->ReadObj();
			TH1D* histo= (TH1D*)file->Get(obj->GetName());
			histo->SetTitle(m_crystalList[j].c_str());
			//if(strcmp(obj->GetName(),"hEDepZDD")==0)vechistos.push_back(histo);
			vechistos.push_back(histo);
			}	
		}
		m_mapHistos.push_back(vechistos);			
	}
	outfile->mkdir(m_EnergyList[i].c_str());
	outfile->cd(m_EnergyList[i].c_str());	
	
	for(int iHist=0;iHist<m_mapHistos[0].size();iHist++)
		{
		vector <TH1D*> vechistos;
			for(int iCrystal=0;iCrystal<m_mapHistos.size();iCrystal++)
			{
			vechistos.push_back(m_mapHistos[iCrystal][iHist]);
			cout << (vechistos.back())->GetName() << endl;
			}
		TH2D* h2D = MakeTH2FromTH1Slices(vechistos);
		h2D->GetXaxis()->SetTitle("ZDD crystal #");
	  h2D->GetYaxis()->SetTitle(vechistos[0]->GetXaxis()->GetTitle());
		h2D->Write();	
		}
	outfile->cd();		
	}
outfile->Close();
		
				
		
}

void DrawProjection1DvsEnergy(string inputpath, string hname, string crystal, string outpath)
{
TFile* file = TFile::Open(inputpath.c_str());

InitSystVect();

TCanvas *c = new TCanvas();
for(int k=0;k<m_EnergyList.size();k++)
{
string path=m_EnergyList[k]+"/"+hname;
//file->cd(path.c_str());
TH2D* h2D = (TH2D*)file->Get(path.c_str());
string hprojname="hproj_crystal"+crystal+"_E"+m_EnergyList[k];
int binnb=h2D->GetXaxis()->FindBin(crystal.c_str());

TH1D* histo = (TH1D*) h2D->ProjectionY(hprojname.c_str(),binnb,binnb);
histo->SetTitle(m_EnergyList[k].c_str());

histo->SetLineColor(kBlue+k);
if(k==0)histo->Draw();
else histo->Draw("same");

}
c->BuildLegend();


}