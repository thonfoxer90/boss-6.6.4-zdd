#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()
    
using namespace std;

TFile* file;

vector<double > vecRate_cZDD104;
vector<double > vecDeltaRate_cZDD104;

vector<string> split(const string& str, const string& delim)
{
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == string::npos) pos = str.length();
        string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}

//int RateZDD_BBBREM(string BBBREMlog, string inputfile, double instlum)
//int RateZDD_BBBREM(string BBBREMlog, string inputfile,string option, double instlum, string outputname)
int RateZDD_BBBREM(string BBBREMlog, string inputfile,string option, string outputname)
{

gStyle->SetOptStat(0);

vector<string> optionsplit=split(option," ");
vector<string> optiontag;
vector<string> optionval;
for(int k=0;k<optionsplit.size();k++){
	string  strtmp=optionsplit[k];
	vector<string> strtmpsplit = split(strtmp,"=");
	if(strtmpsplit.size()==2){
		optiontag.push_back(strtmpsplit[0]);
		optionval.push_back(strtmpsplit[1]);
	}
}

//double instlum=8e32;
double instlum=1e33;
//Geometry for one ZDD block (upper or lower)
double crystalDX=1;
double crystalDY=1;
int NCrystalsX=4;
int NCrystalsY=3;
double XEdgeOrigTop=0;
double gap=1;
double minEDep=0.;

string outps=outputname+".ps";
string outpdf=outputname+".pdf";

for(int k=0;k<optiontag.size();k++){
	cout << optiontag[k] << " " << optionval[k] <<endl;
	if(optiontag[k]=="lum"){instlum=atof(optionval[k].c_str());}
	if(optiontag[k]=="minEDep"){minEDep=atof(optionval[k].c_str());}
	if(optiontag[k]=="ZddGap"){gap=atof(optionval[k].c_str());}
	if(optiontag[k]=="ZddEdgeOrig"){XEdgeOrigTop=atof(optionval[k].c_str());}
}

printf("instlum=%.2e \n", instlum);

double w0=0;
if(BBBREMlog.find(".log") != string::npos){
	ifstream readLog(BBBREMlog.c_str());
	if(readLog.is_open()){
		while(readLog.good()){
			string strline;
			getline(readLog,strline);
			if(strline.find(" total # of events") != string::npos){
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, ' ')) {
 						strsplit.push_back(strtmp);
						}
						
					//for(int k=0;k<strsplit.size();k++) cout << strsplit[k] << endl;
						
					w0= atof((strsplit.back()).c_str());	
					
			}
		}	
	}
}
else if(BBBREMlog.find(".txt") != string::npos){
	ifstream readList(BBBREMlog.c_str());
	if(readList.is_open()){
		while(readList.good()){
			string strline;
			getline(readList,strline);
			ifstream readLog(strline.c_str());
			if(readLog.is_open()){
				while(readLog.good()){
					string strline;
					getline(readLog,strline);
					if(strline.find(" total # of events") != string::npos){
						stringstream ss;
						ss.str(strline);					
						string strtmp;
						vector <string> strsplit;
						while (getline(ss, strtmp, ' ')) {
 							strsplit.push_back(strtmp);
						}
						
						//for(int k=0;k<strsplit.size();k++) cout << strsplit[k] << endl;
						
						w0= w0+ atof((strsplit.back()).c_str());	
					
				}
			}	
		}
 	}
 }
}

printf("w0 = %f \n", w0); sleep(5);

TTree* allMCtree;
TTree* ZDDTree;

if(inputfile.find(".root") != string::npos){
	file = TFile::Open(inputfile.c_str());
	allMCtree = (TTree*) file->Get("AllMCTruthTree");
	ZDDTree = (TTree*) file->Get("BhabhaTree");
}
else if(inputfile.find(".txt") != string::npos){
	TChain* allMCChain = new TChain("AllMCTruthTree");
	TChain* ZDDChain = new TChain("BhabhaTree");
	ifstream readList(inputfile.c_str());
	if(readList.is_open()){
		while(readList.good()){
		string strline;
		getline(readList,strline);
		allMCChain->Add(strline.c_str());
		ZDDChain->Add(strline.c_str());
		}
	}
allMCtree = (TTree*) allMCChain;
ZDDTree = (TTree*) ZDDChain; 
}

double w1=0;		

//Define the list of branch names from NTuple to be read	
vector<string> allMCVarNameList;
for(int k=0;k<(allMCtree->GetListOfBranches())->GetEntries();k++)
		{
		TBranch* br = (TBranch*) (allMCtree->GetListOfBranches())->At(k);
		TLeaf* lf = br->GetLeaf(br->GetName());
		// Leaf name = branch name only if branch has only one leaf.
		
		string lfname(lf->GetName());
		string lftypename(lf->GetTypeName());
		//if(lftypename=="Double_t") allMCVarNameList.push_back(lfname);
		//Crashes. Seems like some leafs variable are not working fine
		//Maybe array multiple entry per event leaf (ie P[nTracks]) is causing trouble ?
		if(lftypename=="Double_t") 
			{
			if(lfname.find("eDep") != string::npos){
				allMCVarNameList.push_back(lfname);
				}
			if(lfname.find("weight") != string::npos){
				allMCVarNameList.push_back(lfname);
				}	
			}
		
		}


	//Assign for each branch an internal variable where its value will be stored.
	//The internal variable is labeled using the corresponding branch name.
	//Its value can be retrieved as the following ;
	// double variable = allMCVarList[Name of variable in NTuple]
	map<string, double> allMCVarList;
	for(int k=0;k<allMCVarNameList.size();k++)
		{
		string varname = allMCVarNameList[k];
		allMCVarList[varname]=0;
				
		double& var=(allMCVarList[varname]);
		allMCtree->SetBranchAddress(varname.c_str(),&var);
		
		}

	//Read the Ntuple and fill histograms
	//printf("All MC Truth Total Entries = %d \n",allMCtree->GetEntries());
	//sleep(3);
	for(int entry=0;entry<allMCtree->GetEntries();entry++)
		{
		allMCtree->GetEntry(entry);
		double weight=allMCVarList["weight"];
		//printf("entry #%d weight = %.2f \n", entry, weight);
		w1=w1+weight;	
		}

//Define the list of branch names from NTuple to be read	
vector<string> ZDDVarNameList;
for(int k=0;k<(ZDDTree->GetListOfBranches())->GetEntries();k++)
		{
		TBranch* br = (TBranch*) (ZDDTree->GetListOfBranches())->At(k);
		TLeaf* lf = br->GetLeaf(br->GetName());
		// Leaf name = branch name only if branch has only one leaf.		
		string lfname(lf->GetName());
		string lftypename(lf->GetTypeName());
		if(lftypename=="Double_t") ZDDVarNameList.push_back(lfname);
		}

//Assign for each branch an internal variable where its value will be stored.
	//The internal variable is labeled using the corresponding branch name.
	//Its value can be retrieved as the following ;
	// double variable = ZDDVarList[Name of variable in NTuple]
	map<string, double> ZDDVarList;
	for(int k=0;k<ZDDVarNameList.size();k++)
		{
		string varname = ZDDVarNameList[k];
		ZDDVarList[varname]=0;
				
		double& var=(ZDDVarList[varname]);
		ZDDTree->SetBranchAddress(varname.c_str(),&var);
		
		}
				
//Initialize variables describing rate for each ZDD crystal
map<string,double > sumWeightsList; //Associate a sum of weight variable to string key (key is crystal number)
map<string,double > sumWeights2List; //Associate a sum of weight**2 variable to string key (key is crystal number)
map<string,double > smcList; //Associate the cross section to string key (key is crystal number)
map<string,double > rateList; //Associate a rate variable (MHz) to string key (key is crystal number)
map<string,double > deltaRateList; //Associate an error on rate variable (MHz) to string key (key is crystal number)

for(int k=0;k<ZDDVarNameList.size();k++)
		{
		string key = ZDDVarNameList[k];
		if(key.find("eDep") != string::npos){
			//If variable name contains "eDep"
			sumWeightsList[key]=0.;
			sumWeights2List[key]=0.;
			rateList[key]=0.;
			deltaRateList[key]=0.;
			}
		if(key.find("zddLeft_energy") != string::npos){
			sumWeightsList[key]=0.;
			sumWeights2List[key]=0.;
			rateList[key]=0.;
			deltaRateList[key]=0.;
			}
		if(key.find("zddRight_energy") != string::npos){
			sumWeightsList[key]=0.;
			sumWeights2List[key]=0.;
			rateList[key]=0.;
			deltaRateList[key]=0.;
			}
		if(key.find("zdd_energy") != string::npos){
			sumWeightsList[key]=0.;
			sumWeights2List[key]=0.;
			rateList[key]=0.;
			deltaRateList[key]=0.;
			}
								
		}	

//Delcare histograms
char htitle[256];
TH1D* hEdep = new TH1D("hEDep","E_{Dep total}^{ZDD Left} (MeV)",1000,0.,1000.);
TH2D* hFrame;
TH2D* histo;

//Fill sum of weight variables

for(int entry=0;entry<ZDDTree->GetEntries();entry++){
	ZDDTree->GetEntry(entry);
	double weight=ZDDVarList["weight"];
	double EDepLeftZdd=ZDDVarList["zddLeft_energy"];
	//printf("entry #%d weight = %.2f \n", entry, weight);
	hEdep->Fill(EDepLeftZdd);
		for(std::map<string,double>::iterator it = sumWeightsList.begin();it!=sumWeightsList.end();++it)
			{
			string key = it->first;			
			//if(ZDDVarList[key]>0) sumWeightsList[key]=sumWeightsList[key]+weight;
			if(ZDDVarList[key]>minEDep) 
				{sumWeightsList[key]=sumWeightsList[key]+weight;
				sumWeights2List[key]=sumWeights2List[key]+weight*weight;
				}
			//if(EDepLeftZdd>minEDep) sumWeightsList[key]=sumWeightsList[key]+weight;
			}
	}

//Compute the rate for each crystal
for(std::map<string,double>::iterator it = sumWeightsList.begin();it!=sumWeightsList.end();++it){
string key = it->first;
double smc=sumWeightsList[key]/w0;
double deltasmc=sqrt(sumWeights2List[key]-pow(sumWeightsList[key],2)/w0)/w0;
smcList[key]=smc;
rateList[key]=smc*1e-3*1e-24*instlum*1e-6;
printf("key : %s sumWeightsList[key] = %f rateList[key] = %f \n",key.c_str(),sumWeightsList[key],rateList[key]);
deltaRateList[key]=deltasmc*1e-3*1e-24*instlum*1e-6;
}

//Draw rate values for left ZDD crystal
vector<double> stlVLimBinsX;
vector<double> stlVLimBinsY;

//X coordinates
for(int k=0;k<NCrystalsX+1;k++)
	{
	double x=XEdgeOrigTop+k*crystalDX;
	stlVLimBinsX.push_back(x);
	}

//Y coordinates
	//Lower block
	for(int k=0;k<NCrystalsY+1;k++)
	{
	double y=0;
	y=k*crystalDY-(NCrystalsY*crystalDY+gap/2);
	stlVLimBinsY.push_back(y);
	}	
	//gap
	stlVLimBinsY.push_back(gap/2);
	//Upper block
	for(int k=1;k<NCrystalsY+1;k++)
	{
	double y=0;
	y=k*crystalDY+gap/2;
	stlVLimBinsY.push_back(y);

	}	

//for(int k=0;k<stlVLimBinsY.size();k++) cout << stlVLimBinsY[k] << endl;

sprintf(htitle,"E_{Dep total}^{ZDD crystal}> %.1f (MeV); X(cm);Y(cm);f(MHz)",minEDep);
hFrame = new TH2D("hframe",htitle,1,-1.,5.,1,-5.,5.);

histo = new TH2D("histo","",stlVLimBinsX.size()-1,&(stlVLimBinsX[0]), stlVLimBinsY.size()-1, &(stlVLimBinsY[0]));

//map<string,int*> mapCrystalNbvsXY;
 
for(int binx=1;binx<=(histo->GetNbinsX());binx++){
		for(int biny=1;biny<=(histo->GetNbinsY());biny++){
		int bin = histo->GetBin(binx,biny);
		double xmean=histo->GetXaxis()->GetBinCenter(binx);
		double ymean=histo->GetYaxis()->GetBinCenter(biny);
		printf("binx = %d biny = %d bin = %d xmean = %f y mean= %f \n", binx,biny, bin, xmean,ymean);
		
		string key;
		if(biny==4) continue;
		if(binx==1 && biny==1) key="eDep221Left";
		if(binx==1 && biny==2) key="eDep211Left";
		if(binx==1 && biny==3) key="eDep201Left";
		if(binx==1 && biny==5) key="eDep101Left";
		if(binx==1 && biny==6) key="eDep111Left";
		if(binx==1 && biny==7) key="eDep121Left";
		
		if(binx==2 && biny==1) key="eDep222Left";
		if(binx==2 && biny==2) key="eDep212Left";
		if(binx==2 && biny==3) key="eDep202Left";
		if(binx==2 && biny==5) key="eDep102Left";
		if(binx==2 && biny==6) key="eDep112Left";
		if(binx==2 && biny==7) key="eDep122Left";
		
		if(binx==3 && biny==1) key="eDep223Left";
		if(binx==3 && biny==2) key="eDep213Left";
		if(binx==3 && biny==3) key="eDep203Left";
		if(binx==3 && biny==5) key="eDep103Left";
		if(binx==3 && biny==6) key="eDep113Left";
		if(binx==3 && biny==7) key="eDep123Left";
		
		if(binx==4 && biny==1) key="eDep224Left";
		if(binx==4 && biny==2) key="eDep214Left";
		if(binx==4 && biny==3) key="eDep204Left";
		if(binx==4 && biny==5) key="eDep104Left";
		if(binx==4 && biny==6) key="eDep114Left";
		if(binx==4 && biny==7) key="eDep124Left";
		
		double val=rateList[key];
		double err=deltaRateList[key];
		histo->SetBinContent(binx,biny,val);
		histo->SetBinError(binx,biny,err);
		if(key=="eDep104Left") 
			{vecRate_cZDD104.push_back(val);
			vecDeltaRate_cZDD104.push_back(err);
			}
		}	
	}

//Associate crystal number to their X,Y position
//histo->Fill(0.,1.,1);


TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str()));

hEdep->Draw();
TLine* lEDep = new TLine(minEDep,hEdep->GetMinimum(),minEDep,hEdep->GetMaximum());
lEDep->SetLineColor(2);
lEDep->Draw();
c->SetLogy(1);
c->Print(Form("%s",outps.c_str()));
c->SetLogy(0);
c->Clear();

hFrame->Draw();
gStyle->SetPaintTextFormat("5.2f MHz"); 
histo->Draw("same colz text E");
c->Print(Form("%s",outps.c_str()));
c->Clear();

c->Print(Form("%s]",outps.c_str()));
delete c;
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
		
return 0;
}

int SystZDDRateEcut(string BBBREMlog, string inputfile,string genopt, string prefix)
{

vector<double> vecEMin;
vecEMin.push_back(0.);vecEMin.push_back(10.);
vecEMin.push_back(20.); vecEMin.push_back(50.);
vecEMin.push_back(70.); vecEMin.push_back(100.);
vecEMin.push_back(150.);vecEMin.push_back(200.);

vector<double> vecDeltaEMin;
for(int k=0;k<vecEMin.size();k++) vecDeltaEMin.push_back(0.);

vecRate_cZDD104.clear();
vecDeltaRate_cZDD104.clear();

for(int k=0;k<vecEMin.size();k++)
	{
	string outname=prefix+".minEDep="+SSTR(vecEMin[k]);
	string option=genopt+" "+"minEDep="+SSTR(vecEMin[k]);
	cout << option << endl; sleep(2);
	RateZDD_BBBREM(BBBREMlog, inputfile,option, outname);
	}


printf("vecRate_cZDD104.size() = %d", vecRate_cZDD104.size());
printf("vecEMin.size() = %d", vecEMin.size());

for(int k=0;k<vecRate_cZDD104.size();k++)
	{
	printf("vecRate_cZDD104[k] = %f", vecRate_cZDD104[k]);
	}

string outps=prefix+".fZDD104vsEmin"+".ps";
string outpdf=prefix+".fZDD104vsEmin"+".pdf";

TCanvas* c = new TCanvas();	
TGraphErrors* graph= new TGraphErrors(vecRate_cZDD104.size(),&vecEMin[0],&vecRate_cZDD104[0],&vecDeltaEMin[0],&vecDeltaRate_cZDD104[0]);
graph->SetTitle(";E_{min}(MeV);f_{ZDD 104} (MHz)");
graph->SetMarkerStyle(8);
graph->Draw("AP");

c->Print(Form("%s",outps.c_str()));
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

return 0;
}