#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TText.h>
#include <TMath.h>
#include <TLorentzVector.h>
#include <TVector3.h>

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <utility> //std::pair

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace std;
//Define crystal center positions
double crystalDX=1;
double crystalDY=1;
int NCrystalsX=4;
int NCrystalsY=3;
double XEdgeOrigTop=0;
double gap=1;
double minEDep=0.;

void DrawZDDcrystals(vector<double> vecX, vector<double> vecY)
{
vector<TLine*> linesY;
vector<TLine*> linesX;
//Y=cst Lines
for(int i=0;i<vecY.size();i++)
	{
	double x0=vecX[0];
	double y0=vecY[i];
	double x1=vecX.back();
	double y1=vecY[i];
	linesY.push_back(new TLine(x0,y0,x1,y1));
	linesY.back()->Draw();
	}
	
//X=cst Lines
for(int i=0;i<vecX.size();i++)
	{
	double x0,y0,x1,y1;
	x0=vecX[i];
	y0=vecY[0];
	x1=vecX[i];
	y1=vecY[NCrystalsY];
	linesX.push_back(new TLine(x0,y0,x1,y1));
	linesX.back()->Draw();
	
	y0=vecY[NCrystalsY+1];
	y1=vecY.back();
	linesX.push_back(new TLine(x0,y0,x1,y1));
	linesX.back()->Draw();
	}
	
}

void DrawZddHitRecXY(string inputfile, string outpath, string option){
gStyle->SetNumberContours(20);
TFile* file = TFile::Open(inputfile.c_str());
TTree* tree = (TTree*) file->Get("4PiGamISRTagged");

tree->Draw("YZddRecHit:XZddRecHit>>histo(64,-1.,5.,64,-5.,5.)","zdd_energy>10","goff");

TH2D* histo = (TH2D*) gROOT->FindObject("histo");
histo->Draw("contz");
//histo->Draw("colz");
gPad->SetLogz();

vector<double> stlVLimBinsX;
vector<double> stlVLimBinsY;

//X coordinates
for(int k=0;k<NCrystalsX+1;k++)
	{
	double x=XEdgeOrigTop+k*crystalDX;
	stlVLimBinsX.push_back(x);
	}

//Y coordinates
	//Lower block
	for(int k=0;k<NCrystalsY+1;k++)
	{
	double y=0;
	y=k*crystalDY-(NCrystalsY*crystalDY+gap/2);
	stlVLimBinsY.push_back(y);
	}	
	//gap
	stlVLimBinsY.push_back(gap/2);
	//Upper block
	for(int k=1;k<NCrystalsY+1;k++)
	{
	double y=0;
	y=k*crystalDY+gap/2;
	stlVLimBinsY.push_back(y);

	}
DrawZDDcrystals(stlVLimBinsX, stlVLimBinsY);		
return;
}