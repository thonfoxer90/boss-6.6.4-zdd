#include "$VERTEXFITROOT/share/jobOptions_VertexDbSvc.txt"
ApplicationMgr.DLLs += {"analysis_cZDD"};
ApplicationMgr.TopAlg += { "analysis_cZDD" };

analysis_cZDD.Vr0cut = 1.0;
analysis_cZDD.Vz0cut = 5.0;

analysis_cZDD.EnergyThreshold = 0.04;
analysis_cZDD.GammaPhiCut = 20.0;
analysis_cZDD.GammaThetaCut = 20.0;
analysis_cZDD.GammaAngleCut = 20.0;

analysis_cZDD.Test4C = 1;
analysis_cZDD.Test5C = 1;
analysis_cZDD.CheckDedx = 1;				
analysis_cZDD.CheckTof = 1;
