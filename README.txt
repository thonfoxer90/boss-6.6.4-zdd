////////////////////////////////////////////////////////////////////////////////////////////////
//				BESIII+crystal Zero Degree Detector(cZDD) Simulation (BOSS 6.6.4.p01)																												
//
//This folder contains the packages needed to perform simulations. 
//of the BESIII detector and the crystal Zero Degree Detector (cZDD).
//The cZDD is made of 2 calorimeters located at both two ends of BESIII detector(z=+/- 335cm), 
//close to the beam pipe.
//Its main purpose is to measure Initial State Radiation (ISR) photons 
//emitted close to the e+/e- beam direction.
//The cZDD is implemented into the Geant 4 based BesIII Offline Software System (BOSS) 
//version 6.6.4.p01.
//It have been originally developed by Marcel Werner (Universit�t Giessen).
//
//
//Active authors : 
//Brice Garillon, Institut f�r Kernphysik (Mainz), bgarillo@uni-mainz.de
//Leonard Koch, Giessen Universit�t, leonard.koch@physik.uni-giessen.de
///////////////////////////////////////////////////////////////////////////////////////////////

////// INSTALLATION \\\\\\
-Pre-requisite : BOSS 6.6.4.p01 framework
-Environment setting
-Compilation

////// DESCRIPTION \\\\\

////// USAGE \\\\\\

////// Git \\\\\\


////// Change log \\\\\\

Early July : 
Analysis/Physics/analysis_cZDD/analysis_cZDD-00-00-01/src/analysis_cZDD.cxx : "briceTree" NTuple added

22-06-2016 :
-ZDDsim package is compiled near the end of the compiling chain (just before BesSim) 

-DetectorDescription/Identifier/Identifier-00-02-12/src/HltID.cxx : 
#include <stdint.h> added

-/home/bgarillo/boss-6.6.4-ZDD/Simulation/BOOST/ZddSim/ZddSim-00-00-01/src/BesZddSD.cc:
Changed from
#include "home/lkoch/wa/Simulation/BOOST/BesSim/BesSim-00-01-20/BesSim/BesSteppingAction.hh"
to
#include "/home/bgarillo/boss-6.6.4-ZDD/Simulation/BOOST/BesSim/BesSim-00-01-20/BesSim/BesSteppingAction.hh"

-DetectorDescription/Identifier/src/HPtID.cxx : #include<stdint.h> added 

-DetectorDescription/GdmlManagement/*.gdml:
"/home/bes/boss_6.6.4_p01/packages/BesGDML/2.8.0/GDMLSchema/gdml.xsd"-> "/cluster/bes3/packages/BesGDML/2.8.0/GDMLSchema/gdml.xsd" 

Early June :
Simulation package provided by Leonard Koch from Giessen.
Y-Type Crotch and inner SCPB vertical wall implemented (L.Koch).
cZDD shifted to x=x+1cm .cZDD now centered at x=2 (Werner, x=1).
Simulation/BOOST/ZddSim/BesZddDigitizer : output Impact_reco.root file added
